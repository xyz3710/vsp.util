﻿/**********************************************************************************************************************/
/*	Domain		:	MakeShortcut.Program
/*	Creator		:	KIMKIWON\xyz37
/*	Create		:	2007년 11월 5일 월요일 오후 8:55
/*	Purpose		:	
/*	Modifier	:	
/*	Update		:	
/*	Changes		:	
/*	Comment		:	참조->COM 참조->Windows Script Host Object Model을 추가하면 된다.
 *					http://www.codeproject.com/dotnet/shelllink.asp 참조
/**********************************************************************************************************************/

using System;
using System.Collections.Generic;
using System.Text;
using IWshRuntimeLibrary;
using System.Windows.Forms;

namespace MakeShortcut
{
	class Program
	{
		static void Main(string[] args)
		{
			string appPath = @"C:\Program Files\iDASiT\Framework.Win\iDASiT.Framework.Win.Login.exe";
			string workingFolder = @"C:\Program Files\iDASiT\Framework.Win";
			string shortcutName = "PCS 로그인";
			string shortcutDesc = "설명";
			string targetPath = Environment.GetFolderPath(System.Environment.SpecialFolder.DesktopDirectory);
			string shortcutPath = string.Format("{0}\\{1}.lnk", targetPath, shortcutName);
			int iconNum = 0;		// 0번째 Icon(MultipleIcon 샘플은 Resources\MultipleIcons 참조)
			WshShell shell = new WshShell();
			IWshShortcut shortcut = (IWshShortcut)shell.CreateShortcut(shortcutPath);
			
			shortcut.TargetPath = appPath;
			shortcut.Description = shortcutDesc;
			shortcut.WindowStyle = (int)ShortcutWindowStyle.WshMaximizedFocus;
			shortcut.WorkingDirectory = workingFolder;
			shortcut.IconLocation = string.Format("{0},{1}", appPath, iconNum);
			shortcut.Save();
		}
	}
}
