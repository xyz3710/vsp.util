@ECHO OFF
:: http://stackoverflow.com/questions/5598668/valid-parameters-for-msdeploy-via-msbuild
::-DeployOnBuild -True
:: -False
:: 
::-DeployTarget -MsDeployPublish
:: -Package
:: 
::-Configuration -Name of a valid solution configuration
:: 
::-CreatePackageOnPublish -True
:: -False
:: 
::-DeployIisAppPath -<Web Site Name>/<Folder>
:: 
::-MsDeployServiceUrl -Location of MSDeploy installation you want to use
:: 
::-MsDeployPublishMethod -WMSVC (Web Management Service)
:: -RemoteAgent
:: 
::-AllowUntrustedCertificate (used with self-signed SSL certificates) -True
:: -False
:: 
::-UserName
::-Password
SETLOCAL

IF EXIST "%SystemRoot%\Microsoft.NET\Framework\v2.0.50727" SET FXPath="%SystemRoot%\Microsoft.NET\Framework\v2.0.50727"
IF EXIST "%SystemRoot%\Microsoft.NET\Framework\v3.5" SET FXPath="%SystemRoot%\Microsoft.NET\Framework\v3.5"
IF EXIST "%SystemRoot%\Microsoft.NET\Framework\v4.0.30319" SET FXPath="%SystemRoot%\Microsoft.NET\Framework\v4.0.30319"

SET targetFile=.\SendScheduledMail\SendScheduledMail.csproj
SET configuration=Release
SET msDeployServiceUrl=https://x10.iptime.org:8172/MsDeploy.axd
SET msDeploySite="WebSite\Batch"
SET userName="WebDeploy"
SET password=%USERNAME%
SET platform=AnyCPU
SET msbuild=%FXPath%\MSBuild.exe /MaxCpuCount:%NUMBER_OF_PROCESSORS% /clp:ShowCommandLine

%MSBuild% %targetFile% /p:configuration=%configuration%;Platform=%platform% /p:DeployOnBuild=True /p:DeployTarget=MsDeployPublish /p:CreatePackageOnPublish=False /p:DeployIISAppPath=%msDeploySite% /p:MSDeployPublishMethod=WMSVC /p:MsDeployServiceUrl=%msDeployServiceUrl% /p:AllowUntrustedCertificate=True /p:UserName=%USERNAME% /p:Password=%password% /p:SkipExtraFilesOnServer=True /p:VisualStudioVersion=12.0

IF NOT "%ERRORLEVEL%"=="0" PAUSE 
ENDLOCAL
