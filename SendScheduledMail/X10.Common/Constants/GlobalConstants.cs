﻿/**********************************************************************************************************************/
/*	Domain		:	X10.Common.GlobalConstants
/*	Creator		:	X10\xyz37(김기원)
/*	Create		:	2011년 8월 14일 일요일 오후 3:37
/*	Purpose		:	Website에서 사용되는 공통 상수를 정의 합니다.
/*--------------------------------------------------------------------------------------------------------------------*/
/*	Modifier	:	
/*	Update		:	
/*	Changes		:	
/*--------------------------------------------------------------------------------------------------------------------*/
/*	Comment		:	상호 참조가 안돼서 <see/>로 XML Doc을 작성할 수 없음
/*--------------------------------------------------------------------------------------------------------------------*/
/*	Reviewer	:	Kim Ki Won
/*	Rev. Date	:	
/**********************************************************************************************************************/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace X10.Common
{
	/// <summary>
	/// Framework에서 사용되는 공통 상수를 정의 합니다.
	/// </summary>
	public class GlobalConstants
	{
		#region 51 Degrees Browser keys
		/// <summary>
		/// 51 degrees에서 Wireless device인지 검사하는 브라우저키 입니다.
		/// </summary>
		public const string FIFTYONE_IS_WIRELESS_DEVICE_KEY = "is_wireless_device";
		/// <summary>
		/// 51 degrees에서 Tablet device인지 검사하는 브라우저키 입니다.
		/// </summary>
		public const string FIFTYONE_IS_TABLET_KEY = "is_tablet";
		/// <summary>
		/// 51 degrees에서 Brand name을 검사하는 브라우저키 입니다.
		/// </summary>
		public const string FIFTYONE_BRAND_NAME_KEY = "brand_name";
		/// <summary>
		/// 51 degrees에서 Model name을 검사하는 브라우저키 입니다.
		/// </summary>
		public const string FIFTYONE_MODEL_NAME_KEY = "model_name";
		#endregion
	}
}