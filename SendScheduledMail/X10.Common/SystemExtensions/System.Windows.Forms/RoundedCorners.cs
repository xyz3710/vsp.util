﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace System.Windows.Forms
{
	/// <summary>
	/// 모서리가 둥근 여역을 나열합니다.
	/// </summary>
	public enum RoundedCorners
	{
		/// <summary>
		/// The none
		/// </summary>
		None = 0,
		/// <summary>
		/// The top left
		/// </summary>
		TopLeft = 1,
		/// <summary>
		/// The top right
		/// </summary>
		TopRight = 2,
		/// <summary>
		/// The bottom left
		/// </summary>
		BottomLeft = 4,
		/// <summary>
		/// The bottom right
		/// </summary>
		BottomRight = 8,
		/// <summary>
		/// All
		/// </summary>
		All = TopLeft | TopRight | BottomLeft | BottomRight
	}
}
