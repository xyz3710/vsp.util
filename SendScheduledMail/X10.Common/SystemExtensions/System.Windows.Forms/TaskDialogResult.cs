using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.InteropServices;

namespace System.Windows.Forms
{
	/// <summary>
	/// TaskDialog 결과를 나열합니다.
	/// </summary>
	[Flags]
	public enum TaskDialogResult : int
	{
		/// <summary>
		/// ID OK
		/// </summary>
		IDOK = 1,
		/// <summary>
		/// ID Cancel
		/// </summary>
		IDCANCEL = 2,
		/// <summary>
		/// ID Retry
		/// </summary>
		IDRETRY = 4,
		/// <summary>
		/// ID Yes
		/// </summary>
		IDYES = 6,
		/// <summary>
		/// ID No
		/// </summary>
		IDNO = 7,
		/// <summary>
		/// ID Close
		/// </summary>
		IDCLOSE = 8,
		/// <summary>
		/// ID None
		/// </summary>
		NONE = 0
	}
}

