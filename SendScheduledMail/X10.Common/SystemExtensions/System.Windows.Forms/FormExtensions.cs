﻿// ****************************************************************************************************************** //
//	Domain		:	System.Windows.Forms.FormExtensions
//	Creator		:	X10-MOBILE\xyz37(Kim Ki Won)
//	Create		:	Wednesday, April 09, 2014 11:24 AM
//	Purpose		:	Windows Form의 확장 기능을 제공합니다.
// ------------------------------------------------------------------------------------------------------------------ //
//	Modifier	:	
//	Update		:	
//	Changes		:	
// ------------------------------------------------------------------------------------------------------------------ //
//	Comment		:	
// ------------------------------------------------------------------------------------------------------------------ //
//	Reviewer	:	
//	Rev. Date	:	
//	Comment		:	
// ------------------------------------------------------------------------------------------------------------------ //
//	<copyright file="FormExtensions.cs" company="(주)가치소프트">
//		Copyright (c) 2014. (주)가치소프트. All rights reserved.
//	</copyright>
// <summary></summary>
// ****************************************************************************************************************** //

using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace System.Windows.Forms
{
	/// <summary>
	/// Windows Form의 확장 기능을 제공합니다.
	/// </summary>
	public static class FormExtensions
	{
		/// <summary>
		/// 폼의 타이틀 바 높이를 구합니다.
		/// </summary>
		/// <param name="form">구하려는 폼.</param>
		/// <returns>폼의 높이를 반환합니다.</returns>
		public static int GetTitleBarHeight(this Form form)
		{
			Rectangle screenRectangle = form.RectangleToScreen(form.ClientRectangle);
			int result = screenRectangle.Top - form.Top;

			return result;
		}

		/// <summary>
		/// 화면을 잠근것 같은 효과를 줘서 나머지 컨트롤에 접근할 수 없는 창을 만듭니다.
		/// </summary>
		/// <param name="owner">The owner.</param>
		/// <param name="opacity">The opacity.</param>
		/// <param name="exceptAreaControls">The except area controls.</param>
		/// <returns>Form.</returns>
		public static Form LockScreen(this Form owner, double opacity = 0.3, params Control[] exceptAreaControls)
		{
			var transparentColor = Color.Fuchsia;
			Form overlay = new Form
			{
				TopMost = true,
				ShowInTaskbar = false,
				FormBorderStyle = FormBorderStyle.None,
				BackColor = transparentColor,
				TransparencyKey = transparentColor,
				Opacity = opacity,
				Location = owner.Location,
				Size = owner.Size,
				StartPosition = FormStartPosition.Manual,
			};

			overlay.Top += owner.GetTitleBarHeight();

			if (exceptAreaControls != null)
			{
				foreach (Control control in exceptAreaControls)
				{
					Panel panel = new Panel
					{
						BackColor = transparentColor,
						Location = control.GetLocationOnForm(),
						Size = control.Size,
					};

					overlay.Controls.Add(panel);
				}
			}

			overlay.Paint += (sender, ea) =>
			{
				var rc = new Rectangle(0, 0, owner.ClientSize.Width, owner.ClientSize.Height);
				using (var brush = new LinearGradientBrush(rc, Color.Gray, Color.White, 90f))
				{
					ea.Graphics.FillRectangle(brush, rc);
				}
			};

			overlay.Show();

			return overlay;
		}
	}
}
