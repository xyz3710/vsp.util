﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace System.Windows.Forms
{
	/// <summary>
	/// TaskDialog 아이콘을 나열합니다.
	/// </summary>
	[Flags]
	public enum TaskDialogIcon
	{
		/// <summary>
		/// Information
		/// </summary>
		Information = UInt16.MaxValue - 2,
		/// <summary>
		/// Warning
		/// </summary>
		Warning = UInt16.MaxValue,
		/// <summary>
		/// Stop
		/// </summary>
		Stop = UInt16.MaxValue - 1,
		/// <summary>
		/// Question
		/// </summary>
		Question = 0,
		/// <summary>
		/// Security Warning 
		/// </summary>
		SecurityWarning = UInt16.MaxValue - 5,
		/// <summary>
		/// Security Error
		/// </summary>
		SecurityError = UInt16.MaxValue - 6,
		/// <summary>
		/// Security Success
		/// </summary>
		SecuritySuccess = UInt16.MaxValue - 7,
		/// <summary>
		/// Security Shield
		/// </summary>
		SecurityShield = UInt16.MaxValue - 3,
		/// <summary>
		/// Security Shield Blue
		/// </summary>
		SecurityShieldBlue = UInt16.MaxValue - 4,
		/// <summary>
		/// Security Shield Gray
		/// </summary>
		SecurityShieldGray = UInt16.MaxValue - 8
	}
}
