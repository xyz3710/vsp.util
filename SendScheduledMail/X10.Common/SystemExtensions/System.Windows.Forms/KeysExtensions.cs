﻿/**********************************************************************************************************************/
/*	Domain		:	X10.Common.SystemExtensions.System.Windows.Forms.KeysExtensions
/*	Creator		:	KIM-KIWON\xyz37(김기원)
/*	Create		:	2012년 10월 30일 화요일 오후 2:45
/*	Purpose		:	WinForm의 Key 처리와 관련한 확장 기능을 제공합니다.
/*--------------------------------------------------------------------------------------------------------------------*/
/*	Modifier	:	
/*	Update		:	
/*	Changes		:	
/*--------------------------------------------------------------------------------------------------------------------*/
/*	Comment		:	
/*--------------------------------------------------------------------------------------------------------------------*/
/*	Reviewer	:	Kim Ki Won
/*	Rev. Date	:	
/**********************************************************************************************************************/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;

namespace System.Windows.Forms
{
	/// <summary>
	/// WinForm의 Key 처리와 관련한 확장 기능을 제공합니다.
	/// </summary>
	public static class KeysExtensions
	{
		[System.Runtime.InteropServices.DllImport("user32.dll")]
		private static extern int MapVirtualKey(uint uCode, uint uMapType);

		/// <summary>
		/// ProcessCmdKey에서 처리한 keyData의 매핑된 char를 구합니다.
		/// </summary>
		/// <param name="keyData">WM_KEYDOWN(0x100)된 이후의 keyData</param>
		/// <returns></returns>
		public static char GetMappedChar(this Keys keyData)
		{
			// http://stackoverflow.com/questions/318777/c-sharp-how-to-translate-virtual-keycode-to-char
			int nonVirtualKey = MapVirtualKey((uint)keyData, 2);	// 2 is used to translate into an unshifted character value

			return Convert.ToChar(nonVirtualKey);
		}

		/// <summary>
		/// 2개 이상의 Control, Alt, Shift의 조합된 단축키 문자열을 <see cref="System.Windows.Forms.Keys"/>로 분리합니다.
		/// </summary>
		/// <param name="compositeKeys">조합된 단축키 문자열(Ex: Ctrl+A, Control+A, Ctrl+Alt+A)</param>
		/// <param name="defaultCompositeKeys">compositeKeys가 해석할 수 없는 문자열일 때 반환할 기본키</param>
		/// <param name="keyDelimiter">특수 문자를 구별짓는 구분자</param>
		/// <returns></returns>
		public static Keys ParseShortcut(string compositeKeys, Keys defaultCompositeKeys = Keys.None, string keyDelimiter = "+")
		{
			if (string.IsNullOrEmpty(compositeKeys) == true)
				return defaultCompositeKeys;

			var keyMap = compositeKeys.Split(new string[] { keyDelimiter }, StringSplitOptions.RemoveEmptyEntries);

			if (keyMap.Length < 2)
				return defaultCompositeKeys;

			if (keyMap[0] == "Ctrl")
				keyMap[0] = "Control";

			Keys result = Keys.None;

			foreach (var key in keyMap)
				if (Enum.IsDefined(typeof(Keys), key) == true)
					result |= (Keys)Enum.Parse(typeof(Keys), key);

			return result;
		}
	}
}
