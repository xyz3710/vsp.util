﻿/**********************************************************************************************************************/
/*	Domain		:	System.Windows.Forms.ControlExtensions
/*	Creator		:	KIM-KIWON\xyz37(김기원)
/*	Create		:	2012년 9월 19일 수요일 오후 1:01
/*	Purpose		:	System.Windows.Forms.Control의 확장 기능을 지원합니다.
/*--------------------------------------------------------------------------------------------------------------------*/
/*	Modifier	:	
/*	Update		:	
/*	Changes		:	
/*--------------------------------------------------------------------------------------------------------------------*/
/*	Comment		:	
/*--------------------------------------------------------------------------------------------------------------------*/
/*	Reviewer	:	Kim Ki Won
/*	Rev. Date	:	
/**********************************************************************************************************************/

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace System.Windows.Forms
{
	using System.Drawing;
	using System.Drawing.Drawing2D;

	/// <summary>
	/// <see cref="System.Windows.Forms.Control"/>의 확장 기능을 지원합니다.
	/// </summary>
	public static class ControlExtensions
	{
		/// <summary>
		/// Control이 놓인 폼에서의 상대 위치를 구합니다.
		/// </summary>
		/// <param name="control">구하고자 하는 컨트롤</param>
		/// <returns>폼의 상대 위치를 반환합니다.</returns>
		/// <exception cref="System.InvalidOperationException">control's parent control is null.</exception>
		public static Point GetLocationOnForm(this Control control)
		{
			if (control.Parent == null)
			{
				throw new InvalidOperationException("control's parent control is null.");
			}

			Point locationOnForm = Point.Empty;

			if (control.FindForm() != null)
			{
				locationOnForm = control.FindForm().PointToClient(control.Parent.PointToScreen(control.Location));
			}

			return locationOnForm;
		}

		/// <summary>
		/// 컨트롤이 놓여있는 절대 위치를 구합니다.
		/// </summary>
		/// <param name="control">구하고자 하는 컨트롤.</param>
		/// <returns>화면상의 절대 위치를 반환합니다.</returns>
		public static Point GetAbsoluteLocation(this Control control)
		{
			return control.PointToScreen(Point.Empty);
		}

		/// <summary>
		/// control.Text의 값을 control의 넓이에 맞게 폰트 크기를 자동으로 조절합니다.
		/// </summary>
		/// <param name="control">The control.</param>
		/// <param name="text">The text.</param>
		public static void SetTextAutoSizedFont(this Control control, string text = "")
		{
			control.Text = text;
			control.Update();
			/*
			Size measureText = TextRenderer.MeasureText(text, control.Font);

			if (measureText.Width > control.Width)
			{
				float fontSize = (float)(control.Width - control.Margin.Left - control.Margin.Right) / (text.Length == 0 ? 1f : (float)text.Length);
				var floor = (float)Math.Floor(fontSize);		// fontSize는 .5pt 단위로 잘라야 한다.

				if (floor < fontSize)
					fontSize = floor + (Math.Round(fontSize - floor) == 1d ? 0.5f : 0f);

				control.Font = new Font(control.Font.FontFamily, fontSize);
			}
			else
				control.Font = control.Parent.Font;

			control.Text = text;
			control.Update();
			*/
		}

		/// <summary>
		/// 컨트롤의 내부 창 핸들이 들어 있는 스레드에서 지정된 대리자를 실행합니다.
		/// </summary>
		/// <param name="control">컨트롤</param>
		/// <param name="action">컨트롤의 스레드 컨텍스트에서 호출될 캡슐화 된 메서드</param>
		public static void Invoke(this Control control, Action action)
		{
			if (control.InvokeRequired == true)
			{
				control.Invoke(new MethodInvoker(action));
			}
			else
			{
				action.Invoke();
			}
		}

		/// <summary>
		/// 컨트롤의 내부 창 핸들이 들어 있는 스레드에서 Text 속성을 업데이트 합니다.
		/// </summary>
		/// <param name="control">컨트롤</param>
		/// <param name="text">컨트롤에 업데이트 할 텍스트</param>
		public static void InvokeText(this Control control, string text)
		{
			control.Invoke(() =>
			{
				control.Text = text;
				control.Update();
			});
		}


		[System.Runtime.InteropServices.DllImport("Gdi32.dll", EntryPoint = "CreateRoundRectRgn")]
		private static extern IntPtr CreateRoundRectRgn
		(
			int leftRect, // x-coordinate of upper-left corner
			int topRect, // y-coordinate of upper-left corner
			int rightRect, // x-coordinate of lower-right corner
			int bottomRect, // y-coordinate of lower-right corner
			int widthEllipse, // height of ellipse
			int heightEllipse // width of ellipse
		);

		/// <summary>
		/// 컨트롤을 모서리가 둥근 영역으로 설정 합니다..
		/// </summary>
		/// <param name="control">설정 하려는 컨트롤</param>
		/// <param name="padding">패딩</param>
		/// <param name="radius">모서리 반지름</param>
		/// <param name="borderColor">테두리 색(없으면 검정색)</param>
		/// <returns>Region.</returns>
		public static Region SetRoundedRegion(this Control control, int padding = 2, int radius = 3, Color? borderColor = null)
		{
			// http://www.techno-soft.com/index.php?/how-to-round-the-edges-of-a-control-in-c
			int controlWidth = control.Width;
			int controlHeight = control.Height;

			var region = Region.FromHrgn(CreateRoundRectRgn(
											 padding,
											 padding,
											 controlWidth - padding,
											 controlHeight - padding,
											 radius,
											 radius));

			control.Region = region;

			Color bc = Color.Empty;

			if (borderColor.HasValue == false)
			{
				bc = Color.Black;
			}
			else if (borderColor.Value == Color.Transparent || borderColor.Value == Color.Empty)
			{
				bc = Color.Empty;
			}
			else
			{
				bc = borderColor.Value;
			}

			if (bc != Color.Empty)
			{
				using (var g = control.CreateGraphics())
				{
					using (Pen pen = new Pen(new SolidBrush(bc), 1))
					{
						var bounds = region.GetBounds(g);
						g.DrawRoundedRectangle(pen, bounds, radius);
					}
				}
			}

			return region;
		}

		/// <summary>
		/// <seealso cref="GraphicsPath"/>로 모서리가 둥근 사각형을 그립니다.
		/// </summary>
		/// <param name="graphic">그래픽 객체</param>
		/// <param name="pen">펜</param>
		/// <param name="bounds">테두리 영역</param>
		/// <param name="radius">모서리 반지름</param>
		/// <param name="corners">둥근 모서리를 만들 위치</param>
		public static void DrawRoundedRectangle(
			this Graphics graphic,
			Pen pen,
			RectangleF bounds,
			float radius,
			RoundedCorners corners = RoundedCorners.All)
		{
			using (GraphicsPath gp = new GraphicsPath())
			{
				if (radius <= 0)
					gp.AddRectangle(bounds);
				else
				{
					if ((corners & RoundedCorners.TopLeft) == RoundedCorners.TopLeft)
						gp.AddArc(bounds.Left + 0.5f, bounds.Top, radius, radius, 180, 90);
					else
						gp.AddLine(bounds.Left, bounds.Top, bounds.Left, bounds.Top);

					if ((corners & RoundedCorners.TopRight) == RoundedCorners.TopRight)
						gp.AddArc(bounds.Right - radius - 2f, bounds.Top, radius, radius, 270, 90);
					else
						gp.AddLine(bounds.Right, bounds.Top, bounds.Right, bounds.Top);

					if ((corners & RoundedCorners.BottomRight) == RoundedCorners.BottomRight)
						gp.AddArc(bounds.Right - radius - 2f, bounds.Bottom - radius - 2f, radius, radius, 0, 90);
					else
						gp.AddLine(bounds.Right, bounds.Bottom, bounds.Right, bounds.Bottom);

					if ((corners & RoundedCorners.BottomLeft) == RoundedCorners.BottomLeft)
						gp.AddArc(bounds.Left + 0.5f, bounds.Bottom - radius - 2f, radius, radius, 90, 90);
					else
						gp.AddLine(bounds.Left, bounds.Bottom, bounds.Left, bounds.Bottom);
				}

				gp.CloseFigure();
				graphic.DrawPath(pen, gp);
			}
		}

		/// <summary>
		/// control의 하위 컨트롤을 탐색하면서 특정한 작업을 합니다.
		/// </summary>
		/// <param name="control">The control.</param>
		/// <param name="action">The action.</param>
		public static void RecursiveAction(this Control control, Action<Control> action)
		{
			var queue = new Queue<System.Windows.Forms.Control.ControlCollection>();

			queue.Enqueue(control.Controls);

			while (queue.Count > 0)
			{
				var controls = queue.Dequeue();

				if (controls == null || controls.Count == 0)
				{
					continue;
				}

				foreach (Control item in controls)
				{
					action(item);

					queue.Enqueue(item.Controls);
				}
			}
		}
	}
}
