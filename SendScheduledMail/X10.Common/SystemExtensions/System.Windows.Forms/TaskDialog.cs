﻿/**********************************************************************************************************************/
/*	Domain		:	System.Windows.Forms.TaskDialog
/*	Creator		:	KIM-KIWON\xyz37(김기원)
/*	Create		:	2012년 9월 26일 수요일 오후 2:05
/*	Purpose		:	Windows Vista 이상의 Task 대화 상자 기능을 제공합니다.
/*--------------------------------------------------------------------------------------------------------------------*/
/*	Modifier	:	
/*	Update		:	
/*	Changes		:	
/*--------------------------------------------------------------------------------------------------------------------*/
/*	Comment		:	http://www.dotnetthoughts.net/how-to-use-taskdialog-api-in-c/
/*--------------------------------------------------------------------------------------------------------------------*/
/*	Reviewer	:	Kim Ki Won
/*	Rev. Date	:	
/**********************************************************************************************************************/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.InteropServices;

namespace System.Windows.Forms
{
	/// <summary>
	/// Windows Vista 이상의 Task 대화 상자 기능을 제공합니다.
	/// </summary>
	public class TaskDialog
	{
		#region P/Invoke TaskDialog
		/// <summary>
		/// 
		/// </summary>
		/// <param name="hWndParent">윈도우 핸들</param>
		/// <param name="hInstance"></param>
		/// <param name="pszWindowTitle"></param>
		/// <param name="pszMainInstruction"></param>
		/// <param name="pszContent"></param>
		/// <param name="dwCommonButtons"></param>
		/// <param name="pszIcon"></param>
		/// <param name="pnButton"></param>
		/// <returns></returns>
		[DllImport("comctl32.dll", CharSet = CharSet.Unicode, EntryPoint = "TaskDialog")]
		internal static extern int InternalTaskDialog(
			IntPtr hWndParent,
			IntPtr hInstance,
			String pszWindowTitle,
			String pszMainInstruction,
			String pszContent,
			int dwCommonButtons,
			IntPtr pszIcon,
			out TaskDialogResult pnButton);
		#endregion

		private TaskDialog()
		{
			
		}

		/// <summary>
		/// Shows the specified handle.
		/// </summary>
		/// <param name="handle">폼이나 컨트롤의 handle(ex. Form.Handle)</param>
		/// <param name="messageTitle">The message title.</param>
		/// <param name="mainTitle">The main title.</param>
		/// <param name="mainContent">Content of the main.</param>
		/// <param name="taskDialogButtons">The task dialog buttons.</param>
		/// <param name="taskDialogIcon">The task dialog icon.</param>
		/// <returns>TaskDialogResult.</returns>
		public static TaskDialogResult Show(
			IntPtr handle,
			string messageTitle,
			string mainTitle,
			string mainContent,
			TaskDialogButtons taskDialogButtons,
			TaskDialogIcon taskDialogIcon)
		{
			TaskDialogResult buttonClicked = TaskDialogResult.IDCANCEL;

			InternalTaskDialog(
				handle,
				IntPtr.Zero,
				messageTitle,
				mainTitle,
				mainContent,
				(int)taskDialogButtons,
				(IntPtr)(int)taskDialogIcon,
				out buttonClicked);

			return buttonClicked;
		}
	}
}
