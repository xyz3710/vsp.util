﻿/**********************************************************************************************************************/
/*	Domain		:	System.Windows.Forms.WindowNativeExtensions
/*	Creator		:	KIM-KIWON\xyz37(김기원)
/*	Create		:	Tuesday, January 08, 2013 3:19 PM
/*	Purpose		:	Handle을 이용하여 Window의 보여주기 상태를 제어 합니다.
/*--------------------------------------------------------------------------------------------------------------------*/
/*	Modifier	:	
/*	Update		:	
/*	Changes		:	
/*--------------------------------------------------------------------------------------------------------------------*/
/*	Comment		:	
/*--------------------------------------------------------------------------------------------------------------------*/
/*	Reviewer	:	Kim Ki Won
/*	Rev. Date	:	
/**********************************************************************************************************************/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.InteropServices;

namespace System.Windows.Forms
{
	/// <summary>
	/// Handle을 이용하여 Window의 보여주기 상태를 제어 합니다.
	/// </summary>
	public static class WindowNativeExtensions
	{
		#region Constants
		private const string USER32_INVOKE_FILE_NAME = "user32.dll";
		private const int SW_SHOWNOACTIVATE = 4;
		private const int HWND_TOPMOST = -1;
		private const uint SWP_NOACTIVATE = 0x0010;
		#endregion

		/// <summary>
		/// Shows the window.
		/// </summary>
		/// <param name="hWnd">The h WND.</param>
		/// <param name="nCmdShow">The n CMD show.</param>
		[DllImport(USER32_INVOKE_FILE_NAME)]
		[return: MarshalAs(UnmanagedType.Bool)]
		public extern static bool ShowWindow(IntPtr hWnd, WindowShowStyles nCmdShow);

		/// <summary>
		/// Sets the window pos.
		/// </summary>
		/// <param name="hWnd">The h WND.</param>
		/// <param name="hWndInsertAfter">The h WND insert after.</param>
		/// <param name="X">The X.</param>
		/// <param name="Y">The Y.</param>
		/// <param name="cx">The cx.</param>
		/// <param name="cy">The cy.</param>
		/// <param name="uFlags">The u flags.</param>
		/// <returns><c>true</c> if XXXX, <c>false</c> otherwise</returns>
		[DllImport("user32.dll", EntryPoint = "SetWindowPos")]
		public static extern bool SetWindowPos(
			 int hWnd,           // window handle
			 int hWndInsertAfter,    // placement-order handle
			 int X,          // horizontal position
			 int Y,          // vertical position
			 int cx,         // width
			 int cy,         // height
			 uint uFlags);       // window positioning flags

		/// <summary>
		/// Use this code within a class to show a topmost form without giving focus to it.
		/// </summary>
		/// <param name="form"></param>
		public static void ShowInactiveTopmost(Form form)
		{
			ShowWindow(form.Handle, WindowShowStyles.ShowNoActivate);
			SetWindowPos(form.Handle.ToInt32(), HWND_TOPMOST,
			form.Left, form.Top, form.Width, form.Height,
			SWP_NOACTIVATE);
		}

		/// <summary>
		/// Finds the window.
		/// </summary>
		/// <param name="lpClassName">Name of the lp class.</param>
		/// <param name="lpWindowName">Name of the lp window.</param>
		/// <returns>IntPtr.</returns>
		[DllImport(USER32_INVOKE_FILE_NAME, CharSet = CharSet.Unicode)]
		public static extern IntPtr FindWindow(String lpClassName, String lpWindowName);

		/// <summary>
		/// Gets the focus.
		/// </summary>
		/// <returns>IntPtr.</returns>
		[DllImport("user32.dll")]
		public static extern IntPtr GetFocus();

		/// <summary>
		/// 현재 포커싱 된 컨트롤을 구합니다.
		/// </summary>
		/// <remarks>DevExpress의 TextEdit 같은 경우는 구해진 Control의 parent를 구해야 한다.</remarks>
		/// <returns></returns>
		public static Control GetFocusedControl()
		{
			return Control.FromHandle(WindowNativeExtensions.GetFocus());
		}

		/// <summary>
		/// Sets the foreground window.
		/// </summary>
		/// <param name="hWnd">The h WND.</param>
		/// <returns><c>true</c> if XXXX, <c>false</c> otherwise</returns>
		[DllImport("USER32.DLL")]
		public static extern bool SetForegroundWindow(IntPtr hWnd);

		/// <summary>
		/// Brings the window to front.
		/// </summary>
		/// <param name="windowTitle">The window title.</param>
		public static void BringWindowToFront(string windowTitle)
		{
			// Get a handle to the application.
			IntPtr handle = FindWindow(null, windowTitle);

			// Verify that app is a running process.
			if (handle == IntPtr.Zero)
				return;

			// With this line you have changed window status from example, minimized to normal
			ShowWindow(handle, WindowShowStyles.ShowDefault);

			// Make app the foreground application
			SetForegroundWindow(handle);
		}
	}
}
