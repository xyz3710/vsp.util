using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace System.Windows.Forms
{
	/// <summary>
	/// TaskDialogButtons을 나열합니다.
	/// </summary>
	[Flags]
	public enum TaskDialogButtons : int
	{
		/// <summary>
		/// Ok
		/// </summary>
		Ok = 0x0001,
		/// <summary>
		/// Cancel
		/// </summary>
		Cancel = 0x0008,
		/// <summary>
		/// Yes
		/// </summary>
		Yes = 0x0002,
		/// <summary>
		/// No
		/// </summary>
		No = 0x0004,
		/// <summary>
		/// Retry
		/// </summary>
		Retry = 0x0010,
		/// <summary>
		/// Close
		/// </summary>
		Close = 0x0020,
	}
}

