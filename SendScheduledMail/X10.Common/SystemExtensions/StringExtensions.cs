﻿/**********************************************************************************************************************/
/*	Domain		:	System.StringExtensions
/*	Creator		:	KIMKIWON\xyz37(김기원)
/*	Create		:	2011년 7월 12일 화요일 오후 9:17
/*	Purpose		:	문자열에 적용되는 확장 기능을 지원합니다.
/*--------------------------------------------------------------------------------------------------------------------*/
/*	Modifier	:	
/*	Update		:	
/*	Changes		:	
/*--------------------------------------------------------------------------------------------------------------------*/
/*	Comment		:	
/*--------------------------------------------------------------------------------------------------------------------*/
/*	Reviewer	:	Kim Ki Won
/*	Rev. Date	:	2011년 10월 14일 금요일 오후 5:04
/**********************************************************************************************************************/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace System
{
	/// <summary>
	/// 문자열에 적용되는 확장 기능을 지원합니다.
	/// </summary>
	public static class StringExtensions
	{
		#region Constants
		/// <summary>
		/// 날짜 시간에 사용되는 yyyy-MM-dd HH:mm:ss:fff 의 23자리 문자열입니다.
		/// </summary>
		public const string DATE_TIME_FORMATSTRING23 = "yyyy-MM-dd HH:mm:ss:fff";
		/// <summary>
		/// 날짜 시간에 사용되는 yyyy-MM-dd HH:mm:ss 의 19자리 문자열입니다.
		/// </summary>
		public const string DATE_TIME_FORMATSTRING19 = "yyyy-MM-dd HH:mm:ss";
		/// <summary>
		/// 날짜 시간에 사용되는 yyyyMMddHHmmss 의 14자리 문자열입니다.
		/// </summary>
		public const string DATE_TIME_FORMATSTRING14 = "yyyyMMddHHmmss";
		/// <summary>
		/// 날짜에 사용되는 yyyy-MM-dd 의 10자리 문자열입니다.
		/// </summary>
		public const string DATE_FORMATSTRING10 = "yyyy-MM-dd";
		/// <summary>
		/// 날짜에 사용되는 yyyyMMdd 의 8자리 문자열입니다.
		/// </summary>
		public const string DATE_FORMATSTRING8 = "yyyyMMdd";
		#endregion

		#region String
		/// <summary>
		/// yyyyMMdd 또는 (yyyy-MM-dd) 형식의 날짜 문자열을 날짜형으로 변환합니다.
		/// </summary>
		/// <param name="dateString"></param>
		/// <returns></returns>
		public static DateTime ConvertToDate(this string dateString)
		{
			DateTime outDateTime = DateTime.MinValue;

			if (string.IsNullOrEmpty(dateString) == false)
			{
				if (dateString.Contains("-") == true)
					DateTime.TryParseExact(dateString, DATE_FORMATSTRING10, null, System.Globalization.DateTimeStyles.None, out outDateTime);
				else
					DateTime.TryParseExact(dateString, DATE_FORMATSTRING8, null, System.Globalization.DateTimeStyles.None, out outDateTime);
			}

			return outDateTime;
		}

		/// <summary>
		/// yyyyMMdd 또는 yyyy-MM-dd 형식의 날짜 문자열을 날짜형 문자열로 변환합니다.
		/// </summary>
		/// <param name="dateString"></param>
		/// <returns></returns>
		public static string ConvertToDateString(this string dateString)
		{
			DateTime outDateTime = DateTime.MinValue;

			if (string.IsNullOrEmpty(dateString) == false)
			{
				if (dateString.Contains("-") == true)
					DateTime.TryParseExact(dateString, DATE_FORMATSTRING10, null, System.Globalization.DateTimeStyles.None, out outDateTime);
				else
					DateTime.TryParseExact(dateString, DATE_FORMATSTRING8, null, System.Globalization.DateTimeStyles.None, out outDateTime);
			}

			return string.Format("{0:d}", outDateTime);
		}

		/// <summary>
		/// yyyyMMddhhmmss 또는 yyyy-MM-dd HH:mm:ss 형식의 날짜 문자열을 날짜형으로 변환합니다.
		/// </summary>
		/// <param name="dateTimeString"></param>
		/// <returns></returns>
		public static DateTime ConvertToDateTime(this string dateTimeString)
		{
			DateTime outDateTime = DateTime.MinValue;

			if (string.IsNullOrEmpty(dateTimeString) == false)
			{
				if (dateTimeString.Contains("-") == true)
					DateTime.TryParseExact(dateTimeString, DATE_TIME_FORMATSTRING19, null, System.Globalization.DateTimeStyles.None, out outDateTime);
				else
					DateTime.TryParseExact(dateTimeString, DATE_TIME_FORMATSTRING14, null, System.Globalization.DateTimeStyles.None, out outDateTime);
			}

			return outDateTime;
		}

		/// <summary>
		/// yyyyMMddhhmmss 또는 yyyy-MM-dd HH:mm:ss 형식의 날짜 문자열을 날짜형 문자열로 변환합니다.
		/// </summary>
		/// <param name="dateTimeString"></param>
		/// <returns>"2011년 7월 25일 오전 11:27" 형태를 반환 합니다.</returns>
		public static string ConvertToDateTimeString(this string dateTimeString)
		{
			DateTime outDateTime = DateTime.MinValue;

			if (string.IsNullOrEmpty(dateTimeString) == false)
			{
				if (dateTimeString.Contains("-") == true)
					DateTime.TryParseExact(dateTimeString, DATE_TIME_FORMATSTRING19, null, System.Globalization.DateTimeStyles.None, out outDateTime);
				else
					DateTime.TryParseExact(dateTimeString, DATE_TIME_FORMATSTRING14, null, System.Globalization.DateTimeStyles.None, out outDateTime);
			}

			return string.Format("{0:g}", outDateTime);
		}

		/// <summary>
		/// yyyyMMddhhmmss 또는 yyyy-MM-dd HH:mm:ss 형식의 날짜 문자열을 날짜형 문자열로 변환합니다.
		/// </summary>
		/// <param name="dateTimeString"></param>
		/// <returns>"2011년 7월 25일 오전 11:27:30" 형태를 반환 합니다.</returns>
		public static string ConvertToDateTimeDetailString(this string dateTimeString)
		{
			DateTime outDateTime = DateTime.MinValue;

			if (string.IsNullOrEmpty(dateTimeString) == false)
			{
				if (dateTimeString.Contains("-") == true)
					DateTime.TryParseExact(dateTimeString, DATE_TIME_FORMATSTRING19, null, System.Globalization.DateTimeStyles.None, out outDateTime);
				else
					DateTime.TryParseExact(dateTimeString, DATE_TIME_FORMATSTRING14, null, System.Globalization.DateTimeStyles.None, out outDateTime);
			}

			return string.Format("{0:G}", outDateTime);
		}

		/// <summary>
		/// 개행문자를 웹에서 사용할 수 있는 break line(<br />)로 대체합니다.
		/// </summary>
		/// <param name="value"></param>
		/// <returns></returns>
		public static MvcHtmlString ReplaceNewLineToBreakLine(this StringBuilder value)
		{
			return value.ToString().ReplaceNewLineToBreakLine();
		}

		/// <summary>
		/// 개행문자를 웹에서 사용할 수 있는 break line(<br />)로 대체합니다.
		/// </summary>
		/// <param name="value"></param>
		/// <returns></returns>
		public static MvcHtmlString ReplaceNewLineToBreakLine(this string value)
		{
			return MvcHtmlString.Create(value.Replace(Environment.NewLine, "<br />"));
		}

		/// <summary>
		/// delimiter를 이용하여 split 시킨 뒤 IEnumerable&lt;T&gt;를 구합니다.
		/// </summary>
		/// <param name="value">구별할 값입니다.</param>
		/// <param name="delimiter">기본 구분자는 (:) 입니다.</param>
		/// <returns></returns>
		public static IEnumerable<string> SplitDelimiter(this string value, string delimiter = ":")
		{
			return value.SplitDelimiter<string>(delimiter);
		}

		/// <summary>
		/// delimiter를 이용하여 split 시킨 뒤 IEnumerable&lt;T&gt;를 구합니다.
		/// </summary>
		/// <typeparam name="T">string, int 타입만 가능합니다.</typeparam>
		/// <param name="value">구별할 값입니다.</param>
		/// <param name="delimiter">기본 구분자는 (:) 입니다.</param>
		/// <returns></returns>
		public static IEnumerable<T> SplitDelimiter<T>(this string value, string delimiter = ":")
		{
			if (string.IsNullOrEmpty(value) == true)
				return new T[0];

			if (typeof(T) != typeof(int) && typeof(T) != typeof(string))
				throw new ArgumentException("T의 타입이 string이나 int 형이여야 합니다.");

			string[] splitValues = value.Split(new string[] { delimiter }, StringSplitOptions.RemoveEmptyEntries);

			if (typeof(T) == typeof(string))
				return splitValues.Cast<T>();
			else if (typeof(T) == typeof(int))
			{
				int oneValue = 0;
				List<int> result = new List<int>();

				foreach (string stringValue in splitValues)
				{
					if (int.TryParse(stringValue, out oneValue) == true)
						result.Add(oneValue);
				}

				return result.Cast<T>();
			}

			return new T[0];
		}

		/// <summary>
		/// 지정된 문자열이 null 이거나 System.String.Empty 문자열인지 여부를 나타냅니다.
		/// </summary>
		/// <param name="value">테스트할 문자열입니다.</param>
		/// <returns>value 매개 변수가 null이거나 빈 문자열("")이면 true 이고, 그렇지 않으면 false입니다.</returns>
		public static bool IsNullOrEmpty(this string value)
		{
			return string.IsNullOrEmpty(value);
		}

		/// <summary>
		/// 지정된 문자열이 null이거나 비어 있거나 공백 문자로만 구성되어 있는지 여부를 나타냅니다.
		/// </summary>
		/// <param name="value">테스트할 문자열입니다.</param>
		/// <returns>value 매개 변수가 null 또는 System.String.Empty이거나, value가 모두 공백 문자로 구성되어 있으면 true입니다.</returns>
		public static bool IsNullOrWhiteSpace(this string value)
		{
			return string.IsNullOrWhiteSpace(value);
		}

		/// <summary>
		/// Gets the string by pointer.
		/// </summary>
		/// <param name="stringPointer">The string pointer.</param>
		/// <returns>변환된 문자열이나 string.Empty를 반환 합니다.</returns>
		public static string GetString(this IntPtr stringPointer)
		{
			string interResult = System.Runtime.InteropServices.Marshal.PtrToStringAnsi(stringPointer);
			System.Runtime.InteropServices.Marshal.FreeHGlobal(stringPointer);

			return interResult ?? string.Empty;
		}

		/// <summary>
		/// 현재 인스턴스의 지정된 문자열이 지정된 다른 문자열로 첫번째만 바뀌는 새 문자열을 반환합니다.
		/// </summary>
		/// <param name="text">변경하려는 문자열 입니다.</param>
		/// <param name="oldValue">바꿀 문자열입니다.</param>
		/// <param name="newValue">oldValue를 바꿀 문자열입니다.</param>
		/// <returns>oldValue의 첫번째 인스턴스를 newValue로 바꾼다는 점을 제외하고 현재 문자열과 동일한 문자열입니다.</returns>
		/// <exception cref="System.ArgumentNullException">oldValue가 null인 경우</exception>
		/// <exception cref="System.ArgumentException">oldValue가 빈 문자열("")인 경우</exception>
		public static string ReplaceFirst(this string text, string oldValue, string newValue)
		{
			if (oldValue == null)
			{
				throw new System.ArgumentNullException(oldValue);
			}

			if (oldValue == string.Empty)
			{
				throw new System.ArgumentException(oldValue);
			}

			int pos = text.IndexOf(oldValue);

			if (pos < 0)
			{
				return text;
			}

			return string.Format("{0}{1}{2}", text.Substring(0, pos), newValue, text.Substring(pos + oldValue.Length));
		}

		/// <summary>
		/// text 에서 공백을 모두 제거합니다
		/// </summary>
		/// <param name="text">공백을 제거하려는 문자열</param>
		/// <returns>모든 공백이 제거된 문자열</returns>
		/// <exception cref="System.ArgumentNullException">text</exception>
		public static string TrimAll(this string text)
		{
			if (text == null)
			{
				throw new ArgumentNullException("text");
			}

			return text.Replace(" ", string.Empty);
		}

		/// <summary>
		/// 숫자의 문자열 표현을 해당하는 32비트 부호 있는 정수로 변환합니다.
		/// </summary>
		/// <param name="value">변환할 숫자가 들어 있는 문자열입니다.</param>
		/// <param name="defaultValue">변환에 실패 했을 때.</param>
		/// <returns>변환에 성공하면 32비트 부호 있는 정수를 반환하고, 그렇지 않으면 defaultValue를 반환합니다.</returns>
		public static int Parse(this string value, int defaultValue = int.MinValue)
		{
			int result = defaultValue;

			int.TryParse(value, out result);

			return result;
		}
		#endregion

		#region StringBuilder
		/// <summary>
		/// 서식 항목이 0개 이상 들어 있는 복합 서식 문자열을 처리하여 반환된 문자열을 이 인스턴스에 추가합니다.<br/>
		/// 각 서식 항목이 매개 변수 배열에서 해당하는 인수의 문자열 표현으로 바뀝니다.<br/>
		/// 현재 System.Text.StringBuilder 개체의 끝에 지정한 문자열의 복사본과 기본 줄 종결자를 차례로 추가합니다.
		/// </summary>
		/// <param name="builder"></param>
		/// <param name="format">복합 서식 문자열입니다.</param>
		/// <param name="args">서식을 지정할 개체의 배열입니다.</param>
		/// <returns>format이 추가된 이 인스턴스에 대한 참조입니다. format의 각 서식 항목이 해당하는 개체 인수의 문자열 표현으로 바뀝니다.</returns>
		/// <exception cref="System.ArgumentNullException">format 또는 args가 null인 경우</exception>
		/// <exception cref="System.FormatException">format이 잘못된 경우 -또는-형식 항목의 인덱스가 0보다 작거나 args 배열의 길이보다 크거나 같은 경우</exception>
		/// <exception cref="System.ArgumentOutOfRangeException">확장된 문자열 길이는 System.Text.StringBuilder.MaxCapacity를 초과합니다.</exception>
		public static StringBuilder AppendFormatLine(this StringBuilder builder, string format, params object[] args)
		{
			return builder.AppendFormat(format + Environment.NewLine, args);
		}
		#endregion
	}
}
