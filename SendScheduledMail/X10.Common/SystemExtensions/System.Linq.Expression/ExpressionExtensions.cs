﻿// ****************************************************************************************************************** //
//	Domain		:	System.Linq.Expressions.ExpressionExtensions
//	Creator		:	X10-MOBILE\xyz37(Kim Ki Won)
//	Create		:	Wednesday, September 3, 2014 1:04 PM
//	Purpose		:	
// ------------------------------------------------------------------------------------------------------------------ //
//	Modifier	:	
//	Update		:	
//	Changes		:	
// ------------------------------------------------------------------------------------------------------------------ //
//	Comment		:	
// ------------------------------------------------------------------------------------------------------------------ //
//	Reviewer	:	
//	Rev. Date	:	
//	Comment		:	
// ------------------------------------------------------------------------------------------------------------------ //
//	<copyright file="ExpressionExtensions.cs" company="Usix">
//		Copyright (c) 2014. Usix. All rights reserved.
//	</copyright>
// <summary></summary>
// ****************************************************************************************************************** //

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace System.Linq.Expressions
{
	/// <summary>
	/// Class ExpressionExtensions.
	/// </summary>
	public static class ExpressionExtensions
	{
		/// <summary>
		/// Ands the also.
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="left">The left.</param>
		/// <param name="right">The right.</param>
		/// <returns>Expression{Func{``0System.Boolean}}.</returns>
		public static Expression<Func<T, bool>> AndAlso<T>(this Expression<Func<T, bool>> left, Expression<Func<T, bool>> right)
		{
			var param = Expression.Parameter(typeof(T), "x");
			var body = Expression.AndAlso(
					Expression.Invoke(left, param),
					Expression.Invoke(right, param)
				);
			var lambda = Expression.Lambda<Func<T, bool>>(body, param);

			return lambda;
		}

		/// <summary>
		/// Combines the specified expressions.
		/// </summary>
		/// <param name="expressions">The expressions.</param>
		/// <returns>Expression{Func{TSystem.Boolean}}.</returns>
		public static Expression<Func<T, bool>> Combine<T>(this List<Expression<Func<T, bool>>> expressions)
		{
			if (expressions == null)
			{
				throw new ArgumentNullException("expressions");
			}

			Expression<Func<T, bool>> result = null;

			if (expressions.Count == 0)
			{
				result = x => true;

				return result;
			}

			result = expressions[0];

			for (int i = 1; i < expressions.Count - 1; i++)
			{
				result.AndAlso(expressions[i]);
			}

			return result;
		}
	}
}
