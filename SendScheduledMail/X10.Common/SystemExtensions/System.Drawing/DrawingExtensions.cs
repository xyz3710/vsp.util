﻿/**********************************************************************************************************************/
/*	Domain		:	System.Drawing.DrawingExtensions
/*	Creator		:	ejpark-PC\ejpark(박은종)
/*	Create		:	Friday, November 30, 2012 10:45 AM
/*	Purpose		:	System.Drawing 객체에 확장 기능을 지원합니다.
/*--------------------------------------------------------------------------------------------------------------------*/
/*	Modifier	:	
/*	Update		:	
/*	Changes		:	
/*--------------------------------------------------------------------------------------------------------------------*/
/*	Comment		:	
/*--------------------------------------------------------------------------------------------------------------------*/
/*	Reviewer	:	Kim Ki Won
/*	Rev. Date	:	
/**********************************************************************************************************************/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing.Imaging;
using System.IO;
using System.Runtime.InteropServices;

namespace System.Drawing
{
	/// <summary>
	/// System.Drawing 객체에 확장 기능을 지원합니다.
	/// </summary>
	public static class DrawingExtensions
	{
		/// <summary>
		/// Bitmap 이미지를 얕은 복사를 실행합니다.
		/// </summary>
		/// <param name="source"></param>
		/// <param name="rectangle"></param>
		/// <returns></returns>
		public static Bitmap ToBitmapClone(this Bitmap source, Rectangle rectangle)
		{
			return source.Clone(rectangle, source.PixelFormat);
		}

		/// <summary>
		/// Bitmap 이미지를 얕은 복사를 실행합니다.
		/// </summary>
		/// <param name="source"></param>
		/// <param name="point"></param>
		/// <returns></returns>
		public static Bitmap ToBitmapClone(this Bitmap source, Point point)
		{
			return source.ToBitmapClone(new Rectangle(point.X, point.Y, source.Width, source.Height));
		}

		/// <summary>
		/// 0, 0 에서 시작해서 Bitmap 이미지를 얕은 복사를 실행합니다.
		/// </summary>
		/// <param name="source"></param>
		/// <returns></returns>
		public static Bitmap ToBitmapClone(this Bitmap source)
		{
			return ToBitmapClone(source, new Point(0, 0));
		}

		/// <summary>
		/// Converts the to24bpp.
		/// </summary>
		/// <param name="image">The image.</param>
		/// <returns>Bitmap.</returns>
		public static Bitmap ConvertTo24bpp(this Image image)
		{
			var bitmap = new Bitmap(image.Width, image.Height, System.Drawing.Imaging.PixelFormat.Format24bppRgb);

			using (var g = Graphics.FromImage(bitmap))
			{
				g.DrawImage(image, new Rectangle(0, 0, image.Width, image.Height));
			}

			return bitmap;
		}

		#region ConvertToGrayscale3
		/// <summary>
		/// Bitmap 이미지를 Gray scale(32bit)로 변환합니다.
		/// </summary>
		/// <param name="source">The source.</param>
		/// <returns>새로운 Bitmap 객체.</returns>
		public static Bitmap ConvertToGrayscale3(this Bitmap source)
		{
			Bitmap target = new Bitmap(source.Width, source.Height, PixelFormat.Format8bppIndexed);

			source.ConvertToGrayscale3(target);

			return target;
		}

		/// <summary>
		/// Bitmap 이미지를 Gray scale로 변환합니다.
		/// </summary>
		/// <param name="source">The source.</param>
		/// <param name="target">The target.</param>
		public static void ConvertToGrayscale3(this Bitmap source, Bitmap target)
		{
			// refer: http://stackoverflow.com/questions/2265910/c-convert-image-to-grayscale
			// refer: http://www.switchonthecode.com/tutorials/csharp-tutorial-convert-a-color-image-to-grayscale
			Graphics g = Graphics.FromImage(target);		//get a graphics object from the new image			
			ColorMatrix colorMatrix = new ColorMatrix(		//create the grayscale ColorMatrix
			   new float[][] 
			   {
				 new float[] {.3f, .3f, .3f, 0, 0},
				 new float[] {.59f, .59f, .59f, 0, 0},
				 new float[] {.11f, .11f, .11f, 0, 0},
				 new float[] {0, 0, 0, 1, 0},
				 new float[] {0, 0, 0, 0, 1}
			   });

			ImageAttributes attributes = new ImageAttributes();		//create some image attributes

			attributes.SetColorMatrix(colorMatrix);		//set the color matrix attribute
			//draw the original image on the new image
			//using the grayscale color matrix
			g.DrawImage(
				source,
				new Rectangle(0, 0, source.Width, source.Height),
				0,
				0,
				source.Width,
				source.Height,
				GraphicsUnit.Pixel,
				attributes);

			//dispose the Graphics object
			g.Dispose();
		}
		#endregion

		#region ConvertToGrayscale4
		/*
		/// <summary>
		/// Bitmap 이미지를 Gray scale로 변환합니다.
		/// </summary>
		/// <param name="source"></param>
		/// <returns></returns>
		public static Bitmap ConvertToGrayscale4(this Bitmap source)
		{
			// refer: http://social.msdn.microsoft.com/Forums/en/csharpgeneral/thread/1a856813-a746-4c1a-ac15-a4314f6eb349
			int w = source.Width,
				h = source.Height,
				r, ic, oc, bmpStride, outputStride, bytesPerPixel;

			PixelFormat pfIn = source.PixelFormat;
			ColorPalette palette;
			Bitmap output;
			BitmapData bmpData, outputData;

			//Create the new bitmap
			output = new Bitmap(w, h, PixelFormat.Format8bppIndexed);

			//Build a grayscale color Palette
			palette = output.Palette;
			for (int i = 0; i < 256; i++)
			{
				Color tmp = Color.FromArgb(255, i, i, i);
				palette.Entries[i] = Color.FromArgb(255, i, i, i);
			}
			output.Palette = palette;

			//No need to convert formats if already in 8 bit
			if (pfIn == PixelFormat.Format8bppIndexed)
			{
				output = (Bitmap)source.Clone();

				//Make sure the palette is a grayscale palette and not some other
				//8-bit indexed palette
				output.Palette = palette;

				return output;
			}

			//Get the number of bytes per pixel
			switch (pfIn)
			{
				case PixelFormat.Format24bppRgb:
					bytesPerPixel = 3;
					break;
				case PixelFormat.Format32bppArgb:
					bytesPerPixel = 4;
					break;
				case PixelFormat.Format32bppRgb:
					bytesPerPixel = 4;
					break;
				default:
					throw new InvalidOperationException("Image format not supported");
			}

			//Lock the images
			bmpData = source.LockBits(new Rectangle(0, 0, w, h), ImageLockMode.ReadOnly,
								   pfIn);
			outputData = output.LockBits(new Rectangle(0, 0, w, h), ImageLockMode.WriteOnly,
										 PixelFormat.Format8bppIndexed);
			bmpStride = bmpData.Stride;
			outputStride = outputData.Stride;

			//Traverse each pixel of the image
			unsafe
			{
				byte* bmpPtr = (byte*)bmpData.Scan0.ToPointer(),
				outputPtr = (byte*)outputData.Scan0.ToPointer();

				if (bytesPerPixel == 3)
				{
					//Convert the pixel to it's luminance using the formula:
					// L = .299*R + .587*G + .114*B
					//Note that ic is the input column and oc is the output column
					for (r = 0; r < h; r++)
						for (ic = oc = 0; oc < w; ic += 3, ++oc)
							outputPtr[r * outputStride + oc] = (byte)(int)
								(0.299f * bmpPtr[r * bmpStride + ic] +
								 0.587f * bmpPtr[r * bmpStride + ic + 1] +
								 0.114f * bmpPtr[r * bmpStride + ic + 2]);
				}
				else //bytesPerPixel == 4
				{
					//Convert the pixel to it's luminance using the formula:
					// L = alpha * (.299*R + .587*G + .114*B)
					//Note that ic is the input column and oc is the output column
					for (r = 0; r < h; r++)
						for (ic = oc = 0; oc < w; ic += 4, ++oc)
							outputPtr[r * outputStride + oc] = (byte)(int)
								((bmpPtr[r * bmpStride + ic] / 255.0f) *
								(0.299f * bmpPtr[r * bmpStride + ic + 1] +
								 0.587f * bmpPtr[r * bmpStride + ic + 2] +
								 0.114f * bmpPtr[r * bmpStride + ic + 3]));
				}
			}

			//Unlock the images
			source.UnlockBits(bmpData);
			output.UnlockBits(outputData);

			return output;
		}
		*/
		#endregion

		/// <summary>
		/// Bitmap 이미지를 깊은 복사를 수행합니다.
		/// </summary>
		/// <param name="source">The source.</param>
		/// <returns>Bitmap.</returns>
		public static Bitmap DeepCopy(this Bitmap source)
		{
			Bitmap result;
			MemoryStream stream = new MemoryStream();

			source.Save(stream, ImageFormat.Bmp);
			stream.Seek(0, SeekOrigin.Begin);
			result = new Bitmap(stream);

			return result;
		}

		/// <summary>
		/// 깊은 복사를 수행하면서 지정된 영역을 잘라내어 Bitmap이미지를 만들어 냅니다.
		/// </summary>
		/// <param name="source">The source.</param>
		/// <param name="cropArea">남겨질 영역.</param>
		/// <returns>Bitmap.</returns>
		public static Bitmap DeepCopy(this Bitmap source, Rectangle cropArea)
		{
			Bitmap result;
			Bitmap buf = source.ToBitmapClone(cropArea);
			MemoryStream stream = new MemoryStream();

			buf.Save(stream, ImageFormat.Bmp);
			stream.Seek(0, SeekOrigin.Begin);
			result = new Bitmap(stream);

			return result;
		}

		/// <summary>
		/// source를 특정 영역으로 자른 뒤 새로운 이미지를 반환합니다.
		/// </summary>
		/// <param name="source">The source.</param>
		/// <param name="selection">The selection.</param>
		/// <returns>잘려진 새로운 Bitmap.</returns>
		/// <exception cref="System.ArgumentNullException">source</exception>
		/// <exception cref="System.ArgumentOutOfRangeException">범위가 벗어났습니다. selection을 수정하십시오.</exception>
		public static Bitmap Crop(this Bitmap source, Rectangle selection)
		{
			CheckCropCondition(source, selection);

			return source.Clone(selection, source.PixelFormat);
		}

		private static void CheckCropCondition(Bitmap source, Rectangle selection)
		{
			if (source == null)
			{
				throw new ArgumentNullException("source");
			}

			// 범위 지정이 잘못되면 OutOfMemoryException이 발생한다.
			if (source.Width < selection.Width + selection.X || source.Height < selection.Height + selection.Y)
			{
				throw new ArgumentOutOfRangeException("범위가 벗어났습니다. selection을 수정하십시오.");
			}
		}

		#region With Raw Data
		/// <summary>
		/// 이미지에서 Stride 값을 구합니다.
		/// </summary>
		/// <param name="source">The source.</param>
		/// <returns>System.Int32.</returns>
		public static int GetStride(this Image source)
		{
			int bitsPerPixel = ((int)source.PixelFormat & 0xff00) >> 8;
			int bytesPerPixel = (bitsPerPixel + 7) / 8;
			int stride = 4 * ((source.Width * bytesPerPixel + 3) / 4);

			return stride;
		}

		/// <summary>
		/// Stride 값을 구합니다.
		/// </summary>
		/// <param name="pixelFormat">The pixel format.</param>
		/// <param name="width">The width.</param>
		/// <returns>System.Int32.</returns>
		public static int GetStride(this PixelFormat pixelFormat, int width)
		{
			int bitsPerPixel = ((int)pixelFormat & 0xff00) >> 8;
			int bytesPerPixel = (bitsPerPixel + 7) / 8;
			int stride = 4 * ((width * bytesPerPixel + 3) / 4);

			return stride;
		}

		/// <summary>
		/// source Bitmap의 raw data 크기를 구합니다.
		/// </summary>
		/// <param name="source">The source.</param>
		/// <param name="startIndex">The start index.</param>
		/// <returns>raw data 크기</returns>
		/// <exception cref="System.ArgumentNullException">source</exception>
		public static int GetRawDataLength(this Bitmap source, int startIndex = 0)
		{
			if (source == null)
			{
				throw new ArgumentNullException("source");
			}

			int stride = source.GetStride();

			return stride * (source.Height - startIndex);
		}

		/// <summary>
		/// Bitmap에서 raw data를 구합니다.
		/// </summary>
		/// <param name="source">The source.</param>
		/// <param name="rawData">The raw data.</param>
		/// <param name="startIndex">The start index.</param>
		/// <exception cref="System.ArgumentNullException">rawData</exception>
		/// <exception cref="System.ArgumentException">rawData의 길이는 stride * height 길이와 같아야 합니다.</exception>
		public static void GetRawData(this Bitmap source, byte[] rawData, int startIndex = 0)
		{
			if (rawData == null)
			{
				throw new ArgumentNullException("rawData");
			}

			int width = source.Width;
			int height = source.Height - startIndex;
			Rectangle rectangle = new Rectangle(0, 0, width, height);
			BitmapData data = source.LockBits(rectangle, ImageLockMode.ReadOnly, source.PixelFormat);
			var strideSize = data.Stride;

			if (rawData.Length < strideSize * height)
			{
				throw new ArgumentException("rawData의 길이는 stride * height 길이와 같아야 합니다.");
			}

			IntPtr ptr = data.Scan0 + (startIndex * strideSize);		// 복사 시작할 포인터 지정
			int copySize = strideSize * height;

			// 배열에 픽셀값 복사
			Marshal.Copy(ptr, rawData, 0, copySize);
			// 반드시 lock된 이미지를 풀어줘야 함.
			source.UnlockBits(data);
		}

		/// <summary>
		/// Bitmap에서 raw data를 구합니다.(raw data를 새로 할당합니다.)
		/// </summary>
		/// <param name="source">The source.</param>
		/// <param name="startIndex">The start index.</param>
		/// <exception cref="System.ArgumentNullException">rawData</exception>
		/// <exception cref="System.ArgumentException">rawData의 길이는 stride * height 길이와 같아야 합니다.</exception>
		public static byte[] GetRawData(this Bitmap source, int startIndex = 0)
		{
			byte[] rawData = new byte[source.GetRawDataLength()];

			GetRawData(source, rawData, startIndex);

			return rawData;
		}

		/// <summary>
		/// Bitmap raw data에서 Bitmap 객체를 구합니다.
		/// </summary>
		/// <param name="rawData">The raw data.</param>
		/// <param name="target">구하려는 bitmap 객체(Width, Height, PixcelFormat이 지정되어야 합니다.)</param>
		/// <param name="startIndex">The start index.</param>
		/// <exception cref="System.ArgumentNullException">rawData</exception>
		public static void ConvertToBitmap(this byte[] rawData, Bitmap target, int startIndex = 0)
		{
			if (rawData == null)
			{
				throw new ArgumentNullException("rawData");
			}

			int width = target.Width;
			int height = target.Height;
			int strideSize = target.GetStride();
			int copySize = strideSize * height;
			var pixelFormat = target.PixelFormat;
			BitmapData data = target.LockBits(
								  new Rectangle(0, 0, width, height),
								  ImageLockMode.ReadWrite,
								  pixelFormat);
			IntPtr destPtr = data.Scan0;
			Marshal.Copy(rawData, strideSize * startIndex, destPtr, copySize);

			target.UnlockBits(data);
		}

		/// <summary>
		/// Bitmap raw data에서 새로운 Bitmap 객체를 생성합니다.
		/// </summary>
		/// <param name="rawData">The raw data.</param>
		/// <param name="width">The width.</param>
		/// <param name="pixelFormat">The pixel format.</param>
		/// <param name="startIndex">The start index.</param>
		/// <returns>Bitmap.</returns>
		/// <exception cref="System.ArgumentNullException">rawData</exception>
		public static Bitmap ConvertToBitmap(this byte[] rawData, int width, PixelFormat pixelFormat, int startIndex = 0)
		{
			if (rawData == null)
			{
				throw new ArgumentNullException("rawData");
			}

			int stride = pixelFormat.GetStride(width);
			int height = rawData.Length / stride - startIndex;
			Bitmap result = new Bitmap(width, height, pixelFormat);

			rawData.ConvertToBitmap(result);

			return result;
		}
		#endregion
	}
}
