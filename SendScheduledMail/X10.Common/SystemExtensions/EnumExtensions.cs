﻿// ****************************************************************************************************************** //
//	Domain		:	System.EnumExtensions
//	Creator		:	KIMKIWON\xyz37(Kim Ki Won)
//	Create		:	Thursday, July 31, 2014 4:32 PM
//	Purpose		:	Enum에 적용되는 확장 기능을 지원합니다.
// ------------------------------------------------------------------------------------------------------------------ //
//	Modifier	:	
//	Update		:	
//	Changes		:	
// ------------------------------------------------------------------------------------------------------------------ //
//	Comment		:	
// ------------------------------------------------------------------------------------------------------------------ //
//	Reviewer	:	
//	Rev. Date	:	
//	Comment		:	
// ------------------------------------------------------------------------------------------------------------------ //
//	<copyright file="EnumExtensions.cs" company="(주)가치소프트">
//		Copyright (c) 2014. (주)가치소프트. All rights reserved.
//	</copyright>
// <summary></summary>
// ****************************************************************************************************************** //

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace System
{
	/// <summary>
	/// Enum에 적용되는 확장 기능을 지원합니다.
	/// </summary>
	public static class EnumExtensions
	{
		/// <summary>
		/// T type의 Enum을 반복하며 action을 처리 합니다.
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="enum">The enum.</param>
		/// <param name="action">The action.</param>
		/// <exception cref="System.ArgumentException">T 형식은 enum 이어야만 합니다.</exception>
		public static void ForEach<T>(this Enum @enum, Action<T> action)
		{
			if (typeof(T).IsEnum == false)
			{
				throw new ArgumentException("T 형식은 enum 이어야만 합니다.");
			}

			foreach (T t in (T[])Enum.GetValues(typeof(T)))
			{
				action(t);
			}
		}
	}
}
