using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using System.Reflection;

namespace System
{
	/// <summary>
	/// 특정 Property에 연결된 Attribute 정보를 제공합니다.
	/// </summary>
	/// <typeparam name="T"></typeparam>
	public class PropertyAttribute<T>
		where T : class
	{
		/// <summary>
		/// PropertyInfo를 구하거나 설정합니다.
		/// </summary>
		/// <value>PropertyInfo를 반환합니다.</value>
		public PropertyInfo Property
		{
			get;
			set;
		}

		/// <summary>
		/// Attribute를 구하거나 설정합니다.
		/// </summary>
		/// <value>Attribute를 반환합니다.</value>
		public T Attribute
		{
			get;
			set;
		}
	}
}
