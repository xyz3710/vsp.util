﻿/**********************************************************************************************************************/
/*	Domain		:	System.ByteExtensions
/*	Creator		:	KIMKIWON\xyz37(김기원)
/*	Create		:	2012년 1월 4일 수요일 오후 2:33
/*	Purpose		:	Byte 형에 적용되는 확장 기능을 지원합니다.
/*--------------------------------------------------------------------------------------------------------------------*/
/*	Modifier	:	
/*	Update		:	
/*	Changes		:	
/*--------------------------------------------------------------------------------------------------------------------*/
/*	Comment		:	
/*--------------------------------------------------------------------------------------------------------------------*/
/*	Reviewer	:	Kim Ki Won
/*	Rev. Date	:	
/**********************************************************************************************************************/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace System
{
	/// <summary>
	/// Byte 형에 적용되는 확장 기능을 지원합니다.
	/// </summary>
	public static class ByteExtensions
	{
		#region Rotate
		/// <summary>
		/// original 값을 왼쪽으로 bits 만큼 Rotate 시킵니다.(bits가 7 이상은 지원하지 않습니다.)
		/// </summary>
		/// <param name="original">rotate 하고자 하는 값입니다.</param>
		/// <param name="bits">이동하려는 bit 숫자 입니다.</param>
		/// <returns></returns>
		public static byte RotateLeft(this byte original, int bits)
		{
			return (byte)((byte)(original << bits) | (byte)((original & 0xff) >> (8 - bits)));
		}

		/// <summary>
		/// original 값을 오른쪽으로 bits 만큼 Rotate 시킵니다.(bits가 7 이상은 지원하지 않습니다.)
		/// </summary>
		/// <param name="original">rotate 하고자 하는 값입니다.</param>
		/// <param name="bits">이동하려는 bit 숫자 입니다.</param>
		/// <returns></returns>
		public static byte RotateRight(this byte original, int bits)
		{
			return (byte)((byte)((original & 0xff) >> bits) | (byte)(original << (8 - bits)));
		}
		#endregion

		#region ToInt32
		/// <summary>
		/// 기본 Big-Endian 방식으로 4바이트에서 변환된 32비트 부호 있는 정수(numberBytes)에 대한 <seealso cref="System.Int32"/> 값을 구합니다.
		/// <remarks><seealso cref="System.BitConverter.ToInt32" />는 <seealso cref="System.BitConverter.IsLittleEndian" />에 의해 구합니다.</remarks>
		/// </summary>
		/// <param name="numberBytes">변환할 숫자가 있는 바이트 배열 입니다.</param>
		/// <param name="isLittleEndian"><seealso cref="System.BitConverter.IsLittleEndian"/></param>
		/// <returns>4바이트로 형성된 32비트 부호 있는 정수입니다.</returns>
		public static int ToInt32(this byte[] numberBytes, bool isLittleEndian = false)
		{
			int result = 0;

			if (isLittleEndian == false)
			{
				result = (numberBytes[0] << 24) | (numberBytes[1] << 16) | (numberBytes[2] << 8) | (numberBytes[3]);
			}
			else
			{
				result = (numberBytes[0]) | (numberBytes[1] << 8) | (numberBytes[2] << 16) | (numberBytes[3] << 24);
			}

			return result;
		}
		#endregion
	}
}
