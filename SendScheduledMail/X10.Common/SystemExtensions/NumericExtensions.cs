﻿/**********************************************************************************************************************/
/*	Domain		:	System.NumericExtensions
/*	Creator		:	KIMKIWON\xyz37(김기원)
/*	Create		:	2012년 1월 4일 수요일 오후 2:33
/*	Purpose		:	숫자형에 적용되는 확장 기능을 지원합니다.
/*--------------------------------------------------------------------------------------------------------------------*/
/*	Modifier	:	
/*	Update		:	
/*	Changes		:	
/*--------------------------------------------------------------------------------------------------------------------*/
/*	Comment		:	
/*--------------------------------------------------------------------------------------------------------------------*/
/*	Reviewer	:	Kim Ki Won
/*	Rev. Date	:	
/**********************************************************************************************************************/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace System
{
	/// <summary>
	/// 숫자형에 적용되는 확장 기능을 지원합니다.
	/// </summary>
	public static class NumericExtensions
	{
		#region Common methods
		/// <summary>
		/// number의 자리수를 구합니다.
		/// </summary>
		/// <param name="number">자리수를 구할 숫자</param>
		/// <returns>구해진 자리수</returns>
		private static int GetDigitCore(dynamic number)
		{
			dynamic digit = 0;

			number = Math.Abs(number);

			while ((dynamic)Math.Pow(10, digit++) <= number)
			{
				;
			}

			return digit - 1;
		}

		/// <summary>
		/// original 값을 왼쪽으로 bits 만큼 Rotate 시킵니다.
		/// </summary>
		/// <param name="original">rotate 하고자 하는 값입니다.</param>
		/// <param name="bits">이동하려는 bit 숫자 입니다.</param>
		/// <returns></returns>
		public static int RotateLeftCore(dynamic original, int bits)
		{
			return (int)((original << bits) | (original >> (32 - bits)));
		}

		/// <summary>
		/// original 값을 오른쪽으로 bits 만큼 Rotate 시킵니다.
		/// </summary>
		/// <param name="original">rotate 하고자 하는 값입니다.</param>
		/// <param name="bits">이동하려는 bit 숫자 입니다.</param>
		/// <returns></returns>
		public static int RotateRightCore(dynamic original, int bits)
		{
			return (int)((original >> bits) | (original << (32 - bits)));
		}
		#endregion

		#region RotateRight
		/// <summary>
		/// original 값을 오른쪽으로 bits 만큼 Rotate 시킵니다.
		/// </summary>
		/// <param name="original">rotate 하고자 하는 값입니다.</param>
		/// <param name="bits">이동하려는 bit 숫자 입니다.</param>
		/// <returns></returns>
		public static int RotateRight(this int original, int bits)
		{
			return RotateRightCore(original, bits);
		}

		/// <summary>
		/// original 값을 오른쪽으로 bits 만큼 Rotate 시킵니다.
		/// </summary>
		/// <param name="original">rotate 하고자 하는 값입니다.</param>
		/// <param name="bits">이동하려는 bit 숫자 입니다.</param>
		/// <returns></returns>
		public static int RotateRight(this uint original, int bits)
		{
			return RotateRightCore(original, bits);
		}

		/// <summary>
		/// original 값을 오른쪽으로 bits 만큼 Rotate 시킵니다.
		/// </summary>
		/// <param name="original">rotate 하고자 하는 값입니다.</param>
		/// <param name="bits">이동하려는 bit 숫자 입니다.</param>
		/// <returns></returns>
		public static int RotateRight(this long original, int bits)
		{
			return RotateRightCore(original, bits);
		}

		/// <summary>
		/// original 값을 오른쪽으로 bits 만큼 Rotate 시킵니다.
		/// </summary>
		/// <param name="original">rotate 하고자 하는 값입니다.</param>
		/// <param name="bits">이동하려는 bit 숫자 입니다.</param>
		/// <returns></returns>
		public static int RotateRight(this ulong original, int bits)
		{
			return RotateRightCore(original, bits);
		}
		#endregion

		#region RotateLeft
		/// <summary>
		/// original 값을 왼쪽으로 bits 만큼 Rotate 시킵니다.
		/// </summary>
		/// <param name="original">rotate 하고자 하는 값입니다.</param>
		/// <param name="bits">이동하려는 bit 숫자 입니다.</param>
		/// <returns></returns>
		public static int RotateLeft(this int original, int bits)
		{
			return RotateLeftCore(original, bits);
		}

		/// <summary>
		/// original 값을 왼쪽으로 bits 만큼 Rotate 시킵니다.
		/// </summary>
		/// <param name="original">rotate 하고자 하는 값입니다.</param>
		/// <param name="bits">이동하려는 bit 숫자 입니다.</param>
		/// <returns></returns>
		public static int RotateLeft(this uint original, int bits)
		{
			return RotateLeftCore(original, bits);
		}

		/// <summary>
		/// original 값을 왼쪽으로 bits 만큼 Rotate 시킵니다.
		/// </summary>
		/// <param name="original">rotate 하고자 하는 값입니다.</param>
		/// <param name="bits">이동하려는 bit 숫자 입니다.</param>
		/// <returns></returns>
		public static int RotateLeft(this long original, int bits)
		{
			return RotateLeftCore(original, bits);
		}

		/// <summary>
		/// original 값을 왼쪽으로 bits 만큼 Rotate 시킵니다.
		/// </summary>
		/// <param name="original">rotate 하고자 하는 값입니다.</param>
		/// <param name="bits">이동하려는 bit 숫자 입니다.</param>
		/// <returns></returns>
		public static int RotateLeft(this ulong original, int bits)
		{
			return RotateLeftCore(original, bits);
		}
		#endregion

		#region GetDigit
		/// <summary>
		/// number의 자리수를 구합니다.
		/// </summary>
		/// <param name="number">자리수를 구할 숫자</param>
		/// <returns>구해진 자리수</returns>
		public static int GetDigit(this int number)
		{
			return GetDigitCore(number);
		}

		/// <summary>
		/// number의 자리수를 구합니다.
		/// </summary>
		/// <param name="number">자리수를 구할 숫자</param>
		/// <returns>구해진 자리수</returns>
		public static int GetDigit(this uint number)
		{
			return GetDigitCore(number);
		}

		/// <summary>
		/// number의 자리수를 구합니다.
		/// </summary>
		/// <param name="number">자리수를 구할 숫자</param>
		/// <returns>구해진 자리수</returns>
		public static int GetDigit(this long number)
		{
			return GetDigitCore(number);
		}

		/// <summary>
		/// number의 자리수를 구합니다.
		/// </summary>
		/// <param name="number">자리수를 구할 숫자</param>
		/// <returns>구해진 자리수</returns>
		public static int GetDigit(this ulong number)
		{
			return GetDigitCore(number);
		}
		#endregion

		#region GetBytes
		/// <summary>
		/// 기본 Big-Endian 방식으로 number에 대한 4 바이트 배열 값을 구합니다.
		/// <remarks><seealso cref="System.BitConverter.GetBytes(int)" />는 <seealso cref="System.BitConverter.IsLittleEndian" />에 의해 구합니다.</remarks>
		/// </summary>
		/// <param name="number">변환할 숫자입니다.</param>
		/// <param name="isLittleEndian"><seealso cref="System.BitConverter.IsLittleEndian"/></param>
		/// <returns>길이가 4인 바이트 배열입니다.</returns>
		public static byte[] GetBytes(this int number, bool isLittleEndian = false)
		{
			byte[] result = new byte[4];
			Func<int, int> action = null;

			if (isLittleEndian == false)
			{
				action = x => 3 - x;
			}
			else
			{
				action = x => x;
			}

			// Big-Endian like this
			// result[0] = (byte)(number >> 24);
			// result[1] = (byte)(number >> 16);
			// result[2] = (byte)(number >> 8);
			// result[3] = (byte)(number);

			for (int i = 0; i < result.Length; i++)
			{
				result[i] = (byte)(number >> (action(i) * 8));
			}

			return result;
		}
		#endregion
	}
}
