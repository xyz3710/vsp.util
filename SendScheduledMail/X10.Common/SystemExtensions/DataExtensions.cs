﻿/**********************************************************************************************************************/
/*	Domain		:	System.Data.DataExtensions
/*	Creator		:	KIMKIWON\xyz37(김기원)
/*	Create		:	2011년 4월 26일 화요일 오후 11:22
/*	Purpose		:	System.Data의 확장 기능을 제공합니다.
/*--------------------------------------------------------------------------------------------------------------------*/
/*	Modifier	:	
/*	Update		:	
/*	Changes		:	
/*--------------------------------------------------------------------------------------------------------------------*/
/*	Comment		:	
/*--------------------------------------------------------------------------------------------------------------------*/
/*	Reviewer	:	Kim Ki Won
/*	Rev. Date	:	2011년 8월 18일 목요일 오후 10:06
/**********************************************************************************************************************/

using System;
using System.Linq;
using System.Text;
using System.Reflection;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;

namespace System.Data
{
	/// <summary>
	/// System.Data의 확장 기능을 제공합니다.
	/// </summary>
	public static class DataExtensions
	{
		#region ConvertToDataTable<F>
		/// <summary>
		/// INotifyPropertyChanging를 구현한 EntityType의 클래스를 <see cref="System.Data.DataTable"/>로 columns 목록의 컬럼만 변환합니다.
		/// </summary>
		/// <param name="fromType">INotifyPropertyChanging를 구현한 클래스</param>
		/// <param name="columns">변환하려는 컬럼 목록</param>
		/// <returns>dataTable 이름의 <see cref="System.Data.DataTable"/> 을 반환합니다.</returns>
		public static DataTable ConvertToDataTable<F>(this F fromType, params string[] columns)
				where F : INotifyPropertyChanging
		{
			string dataTableName = "dataTable";

			return ConvertToDataTable<F>(fromType, dataTableName, columns);
		}

		/// <summary>
		/// INotifyPropertyChanging를 구현한 EntityType의 클래스를 <see cref="System.Data.DataTable"/>로 columns 목록의 컬럼만 변환합니다.
		/// </summary>
		/// <param name="fromType">INotifyPropertyChanging를 구현한 클래스</param>
		/// <param name="dataTableName"><see cref="System.Data.DataTable"/> 이름</param>
		/// <param name="columns">변환하려는 컬럼 목록</param>
		/// <returns>dataTableName 이름의 <see cref="System.Data.DataTable"/> 을 반환합니다.</returns>
		public static DataTable ConvertToDataTable<F>(this F fromType, string dataTableName, params string[] columns)
			where F : INotifyPropertyChanging
		{
			DataTable result = new DataTable(dataTableName);

			if (fromType == null)
				return result;

			Type fromType2 = fromType.GetType();
			List<object> row = new List<object>();

			foreach (PropertyInfo pi in fromType2.GetProperties())
			{
				if (columns.Contains(pi.Name) == false)
					continue;

				Type fromPropType = pi.PropertyType.GetNullableUnderlyingType() ?? pi.PropertyType;

				result.Columns.Add(pi.Name, fromPropType);
				row.Add(pi.GetValue(fromType, null));
			}

			result.Rows.Add(row.ToArray());

			return result;
		}
		#endregion

		#region HasData
		/// <summary>
		/// DataSet이 값이 있는지 검사합니다.
		/// </summary>
		/// <param name="dataSet"></param>
		/// <returns></returns>
		public static bool HasData(this DataSet dataSet)
		{
			if (dataSet == null)
			{
				StackTrace stackTrace = new StackTrace();

				throw new ArgumentException(string.Format("HasData의 DataSet이 null 입니다.\r\nIn {0}", stackTrace.GetFrame(1).GetMethod().ToString()));
			}

			if (dataSet.Tables == null || dataSet.Tables.Count == 0)
				return false;
			else
			{
				foreach (DataTable dataTable in dataSet.Tables)
				{
					if (dataTable.HasData() == true)
						return true;
				}

				return false;
			}
		}

		/// <summary>
		/// DataTable의 값이 있는지 검사합니다.
		/// </summary>
		/// <param name="dataTable"></param>
		/// <returns></returns>
		public static bool HasData(this DataTable dataTable)
		{
			if (dataTable == null)
			{
				StackTrace stackTrace = new StackTrace();

				throw new ArgumentException(string.Format("HasData의 DataTable이 null 입니다.\r\nIn {0}", stackTrace.GetFrame(1).GetMethod().ToString()));
			}

			return (dataTable.Rows.Count > 0);
		}
		#endregion
	}
}
