﻿// ****************************************************************************************************************** //
//	Domain		:	System.Diagnostics.PerformanceCounterExtensions
//	Creator		:	X10-MOBILE\xyz37(Kim Ki Won)
//	Create		:	Wednesday, July 23, 2014 10:10 PM
//	Purpose		:	PerformanceCounter의 확장 기능을 제공합니다.
// ------------------------------------------------------------------------------------------------------------------ //
//	Modifier	:	
//	Update		:	
//	Changes		:	
// ------------------------------------------------------------------------------------------------------------------ //
//	Comment		:	
// ------------------------------------------------------------------------------------------------------------------ //
//	Reviewer	:	
//	Rev. Date	:	
//	Comment		:	
// ------------------------------------------------------------------------------------------------------------------ //
//	<copyright file="PerformanceCounterExtensions.cs" company="(주)가치소프트">
//		Copyright (c) 2014. (주)가치소프트. All rights reserved.
//	</copyright>
// <summary></summary>
// ****************************************************************************************************************** //

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using X10.Common.Utility;

namespace System.Diagnostics
{
	/// <summary>
	/// PerformanceCounter의 확장 기능을 제공합니다.
	/// </summary>
	public static class PerformanceCounterExtensions
	{
		#region Constants
		private const string DEFAULT_PROCESS_CATEGORY = "Process";
		#endregion

		#region Process Category
		private static PerformanceCounter SetProcessCategory(this PerformanceCounter performanceCounter, string processName = "")
		{
			if (performanceCounter == null)
			{
				throw new ArgumentNullException("performanceCounter");
			}

			if (string.IsNullOrEmpty(processName) == true)
			{
				processName = Process.GetCurrentProcess().ProcessName;
			}

			performanceCounter.CategoryName = DEFAULT_PROCESS_CATEGORY;
			performanceCounter.InstanceName = processName;

			return performanceCounter;
		}

		/// <summary>
		/// processName의 사용 시간(% Processor Time)을 구합니다.
		/// </summary>
		/// <param name="performanceCounter">The performance counter.</param>
		/// <param name="processName">프로세스 이름, 없을 경우 현재 프로세스 이름을 사용.</param>
		/// <returns>System.Int64.</returns>
		/// <exception cref="System.ArgumentNullException">performanceCounter</exception>
		public static string GetProcessProcessorTime(
			this PerformanceCounter performanceCounter,
			string processName = "")
		{
			performanceCounter.SetProcessCategory(processName).CounterName = "% Processor Time";
			var span = TimeSpan.FromTicks(performanceCounter.NextSample().TimeStamp);

			return span.ToString(@"hh\:mm\:ss");
		}

		/// <summary>
		/// processName의 전용 사용 메모리(Working Set - Private)를 구합니다.
		/// </summary>
		/// <remarks>Working Set - Private는 다른 프로세서가 공유하거나 공유할 수 있는 작업 집합이 아니라 이 프로세서만 사용하고 있는 작업 집합의 크기(바이트)입니다.</remarks>
		/// <param name="performanceCounter">The performance counter.</param>
		/// <param name="processName">프로세스 이름, 없을 경우 현재 프로세스 이름을 사용.</param>
		/// <returns>System.Int64.</returns>
		/// <exception cref="System.ArgumentNullException">performanceCounter</exception>
		public static long GetProcessPrivateWorkingSet(
			this PerformanceCounter performanceCounter,
			string processName = "")
		{
			performanceCounter.SetProcessCategory(processName).CounterName = "Working Set - Private";

			return (long)performanceCounter.NextValue();
		}

		/// <summary>
		/// processName의 전용 사용 메모리(Working Set - Private)를 구합니다.
		/// </summary>
		/// <remarks>Working Set - Private는 다른 프로세서가 공유하거나 공유할 수 있는 작업 집합이 아니라 이 프로세서만 사용하고 있는 작업 집합의 크기(바이트)입니다.</remarks>
		/// <param name="performanceCounter">The performance counter.</param>
		/// <param name="memorySizeType"><seealso cref="X10.Common.Utility.MemorySizeTypes"/>.</param>
		/// <param name="processName">프로세스 이름, 없을 경우 현재 프로세스 이름을 사용.</param>
		/// <returns>System.String.</returns>
		/// <exception cref="System.ArgumentNullException">performanceCounter</exception>
		public static string GetProcessPrivateWorkingSet(
			this PerformanceCounter performanceCounter,
			MemorySizeTypes memorySizeType = MemorySizeTypes.KiloByteShort,
			string processName = "")
		{
			return Utilities.GetMemorySizedTypeString(memorySizeType, performanceCounter.GetProcessPrivateWorkingSet(processName));
		}

		/// <summary>
		/// processName의 기본 사용 메모리(Working Set)를 구합니다.
		/// </summary>
		/// <remarks>
		/// Working Set는 이 프로세스에 대한 작업 집합의 현재 크기(바이트)입니다. 
		/// 작업 집합은 프로세스의 스레드가 최근에 작업한 메모리 페이지의 집합입니다. 
		/// 컴퓨터에 있는 빈 메모리가 한계를 초과하면 페이지는 사용 중이 아니라도 프로세스의 작업 집합에 남아 있습니다. 
		/// 빈 메모리가 한계 미만이면 페이지는 작업 집합에서 지워집니다. 
		/// 이 페이지가 필요하면 주 메모리에서 없어지기 전에 소프트 오류 처리되어 다시 작업 집합에 있게 됩니다.
		/// </remarks>
		/// <param name="performanceCounter">The performance counter.</param>
		/// <param name="processName">프로세스 이름, 없을 경우 현재 프로세스 이름을 사용.</param>
		/// <returns>System.Int64.</returns>
		/// <exception cref="System.ArgumentNullException">performanceCounter</exception>
		public static long GetProcessWorkingSet(
			this PerformanceCounter performanceCounter,
			string processName = "")
		{
			performanceCounter.SetProcessCategory(processName).CounterName = "Working Set";

			return (long)performanceCounter.NextValue();
		}

		/// <summary>
		/// processName의 기본 사용 메모리(Working Set)를 구합니다.
		/// </summary>
		/// <remarks>
		/// Working Set는 이 프로세스에 대한 작업 집합의 현재 크기(바이트)입니다. 
		/// 작업 집합은 프로세스의 스레드가 최근에 작업한 메모리 페이지의 집합입니다. 
		/// 컴퓨터에 있는 빈 메모리가 한계를 초과하면 페이지는 사용 중이 아니라도 프로세스의 작업 집합에 남아 있습니다. 
		/// 빈 메모리가 한계 미만이면 페이지는 작업 집합에서 지워집니다. 
		/// 이 페이지가 필요하면 주 메모리에서 없어지기 전에 소프트 오류 처리되어 다시 작업 집합에 있게 됩니다.
		/// </remarks>
		/// <param name="performanceCounter">The performance counter.</param>
		/// <param name="memorySizeType"><seealso cref="X10.Common.Utility.MemorySizeTypes"/>.</param>
		/// <param name="processName">프로세스 이름, 없을 경우 현재 프로세스 이름을 사용.</param>
		/// <returns>System.String.</returns>
		/// <exception cref="System.ArgumentNullException">performanceCounter</exception>
		public static string GetProcessWorkingSet(
			this PerformanceCounter performanceCounter,
			MemorySizeTypes memorySizeType = MemorySizeTypes.KiloByteShort,
			string processName = "")
		{
			return Utilities.GetMemorySizedTypeString(memorySizeType, performanceCounter.GetProcessWorkingSet(processName));
		}

		/// <summary>
		/// processName의 기본 사용 메모리 최고값(Working Set Peak)을 구합니다.
		/// </summary>
		/// <remarks>
		/// Working Set Peak는 한 시점에서 이 프로세스에 대한 작업 집합의 최대 크기(바이트)입니다. 
		/// 작업 집합은 프로세스의 스레드가 최근에 작업한 메모리 페이지의 집합입니다. 
		/// 컴퓨터에 있는 빈 메모리가 한계를 초과하면 페이지는 사용 중이 아니라도 프로세스의 작업 집합에 남아 있습니다. 
		/// 빈 메모리가 한계 미만이면 페이지는 작업 집합에서 지워집니다. 
		/// 이 페이지가 필요하면 주 메모리에서 없어지기 전에 소프트 오류 처리되어 다시 작업 집합에 있게 됩니다.
		/// </remarks>
		/// <param name="performanceCounter">The performance counter.</param>
		/// <param name="processName">프로세스 이름, 없을 경우 현재 프로세스 이름을 사용.</param>
		/// <returns>System.Int64.</returns>
		/// <exception cref="System.ArgumentNullException">performanceCounter</exception>
		public static long GetProcessWorkingSetPeak(
			this PerformanceCounter performanceCounter,
			string processName = "")
		{
			performanceCounter.SetProcessCategory(processName).CounterName = "Working Set Peak";

			return (long)performanceCounter.NextValue();
		}

		/// <summary>
		/// processName의 기본 사용 메모리 최고값(Working Set Peak)을 구합니다.
		/// </summary>
		/// <remarks>
		/// Working Set Peak는 한 시점에서 이 프로세스에 대한 작업 집합의 최대 크기(바이트)입니다. 
		/// 작업 집합은 프로세스의 스레드가 최근에 작업한 메모리 페이지의 집합입니다. 
		/// 컴퓨터에 있는 빈 메모리가 한계를 초과하면 페이지는 사용 중이 아니라도 프로세스의 작업 집합에 남아 있습니다. 
		/// 빈 메모리가 한계 미만이면 페이지는 작업 집합에서 지워집니다. 
		/// 이 페이지가 필요하면 주 메모리에서 없어지기 전에 소프트 오류 처리되어 다시 작업 집합에 있게 됩니다.
		/// </remarks>
		/// <param name="performanceCounter">The performance counter.</param>
		/// <param name="memorySizeType"><seealso cref="X10.Common.Utility.MemorySizeTypes"/>.</param>
		/// <param name="processName">프로세스 이름, 없을 경우 현재 프로세스 이름을 사용.</param>
		/// <returns>System.String.</returns>
		/// <exception cref="System.ArgumentNullException">performanceCounter</exception>
		public static string GetProcessWorkingSetPeak(
			this PerformanceCounter performanceCounter,
			MemorySizeTypes memorySizeType = MemorySizeTypes.KiloByteShort,
			string processName = "")
		{
			return Utilities.GetMemorySizedTypeString(memorySizeType, performanceCounter.GetProcessWorkingSetPeak(processName));
		}

		/// <summary>
		/// processName의 페이징 파일에서 사용하기 위해 예약한 가상 메모리의 현재 바이트 수(Page File Bytes)를 구합니다.
		/// </summary>
		/// <remarks>
		/// Page File Bytes는 이 프로세스가 페이징 파일에서 사용하기 위해 예약한 가상 메모리의 현재 바이트 수입니다.
		/// </remarks>
		/// <param name="performanceCounter">The performance counter.</param>
		/// <param name="processName">프로세스 이름, 없을 경우 현재 프로세스 이름을 사용.</param>
		/// <returns>System.Int64.</returns>
		/// <exception cref="System.ArgumentNullException">performanceCounter</exception>
		public static long GetProcessPageFileBytes(
			this PerformanceCounter performanceCounter,
			string processName = "")
		{
			performanceCounter.SetProcessCategory(processName).CounterName = "Page File Bytes";

			return (long)performanceCounter.NextValue();
		}

		/// <summary>
		/// processName의 페이징 파일에서 사용하기 위해 예약한 가상 메모리의 현재 바이트 수(Page File Bytes)를 구합니다.
		/// </summary>
		/// <remarks>
		/// Page File Bytes는 이 프로세스가 페이징 파일에서 사용하기 위해 예약한 가상 메모리의 현재 바이트 수입니다.
		/// </remarks>
		/// <param name="performanceCounter">The performance counter.</param>
		/// <param name="memorySizeType"><seealso cref="X10.Common.Utility.MemorySizeTypes"/>.</param>
		/// <param name="processName">프로세스 이름, 없을 경우 현재 프로세스 이름을 사용.</param>
		/// <returns>System.String.</returns>
		/// <exception cref="System.ArgumentNullException">performanceCounter</exception>
		public static string GetProcessPageFileBytes(
			this PerformanceCounter performanceCounter,
			MemorySizeTypes memorySizeType = MemorySizeTypes.KiloByteShort,
			string processName = "")
		{
			return Utilities.GetMemorySizedTypeString(memorySizeType, performanceCounter.GetProcessPageFileBytes(processName));
		}

		/// <summary>
		/// processName의 페이징 파일에서 사용하기 위해 예약한 가상 메모리의 최대 바이트 수(Page File Bytes Peak)를 구합니다.
		/// </summary>
		/// <remarks>
		/// Page File Bytes Peak는 이 프로세스가 페이징 파일에서 사용하기 위해 예약한 가상 메모리의 최대 바이트 수입니다.
		/// </remarks>
		/// <param name="performanceCounter">The performance counter.</param>
		/// <param name="processName">프로세스 이름, 없을 경우 현재 프로세스 이름을 사용.</param>
		/// <returns>System.Int64.</returns>
		/// <exception cref="System.ArgumentNullException">performanceCounter</exception>
		public static long GetProcessPageFileBytesPeak(
			this PerformanceCounter performanceCounter,
			string processName = "")
		{
			performanceCounter.SetProcessCategory(processName).CounterName = "Page File Bytes Peak";

			return (long)performanceCounter.NextValue();
		}

		/// <summary>
		/// processName의 페이징 파일에서 사용하기 위해 예약한 가상 메모리의 최대 바이트 수(Page File Bytes Peak)를 구합니다.
		/// </summary>
		/// <remarks>
		/// Page File Bytes Peak는 이 프로세스가 페이징 파일에서 사용하기 위해 예약한 가상 메모리의 최대 바이트 수입니다.
		/// </remarks>
		/// <param name="performanceCounter">The performance counter.</param>
		/// <param name="memorySizeType"><seealso cref="X10.Common.Utility.MemorySizeTypes"/>.</param>
		/// <param name="processName">프로세스 이름, 없을 경우 현재 프로세스 이름을 사용.</param>
		/// <returns>System.String.</returns>
		/// <exception cref="System.ArgumentNullException">performanceCounter</exception>
		public static string GetProcessPageFileBytesPeak(
			this PerformanceCounter performanceCounter,
			MemorySizeTypes memorySizeType = MemorySizeTypes.KiloByteShort,
			string processName = "")
		{
			return Utilities.GetMemorySizedTypeString(memorySizeType, performanceCounter.GetProcessPageFileBytesPeak(processName));
		}

		/// <summary>
		/// processName의 프로세스가 사용하고 있는 가상 주소 공간의 바이트 수(Virtual Bytes)를 구합니다.
		/// </summary>
		/// <remarks>
		/// Virtual Bytes는 프로세스가 사용하고 있는 가상 주소 공간의 바이트 수입니다. 
		/// 가상 주소 공간의 사용이 해당 디스크 또는 주 메모리 페이지를 반드시 사용함을 뜻하지는 않습니다. 
		/// 그러나 가상 공간이 한정되어 있으므로 프로세스가 라이브러리를 로드하는 데 제한을 받을 수 있습니다.
		/// </remarks>
		/// <param name="performanceCounter">The performance counter.</param>
		/// <param name="processName">프로세스 이름, 없을 경우 현재 프로세스 이름을 사용.</param>
		/// <returns>System.Int64.</returns>
		/// <exception cref="System.ArgumentNullException">performanceCounter</exception>
		public static long GetProcessVirtualBytes(
			this PerformanceCounter performanceCounter,
			string processName = "")
		{
			performanceCounter.SetProcessCategory(processName).CounterName = "Virtual Bytes";

			return (long)performanceCounter.NextValue();
		}

		/// <summary>
		/// processName의 프로세스가 사용하고 있는 가상 주소 공간의 바이트 수(Virtual Bytes)를 구합니다.
		/// </summary>
		/// <remarks>
		/// Virtual Bytes는 프로세스가 사용하고 있는 가상 주소 공간의 바이트 수입니다. 
		/// 가상 주소 공간의 사용이 해당 디스크 또는 주 메모리 페이지를 반드시 사용함을 뜻하지는 않습니다. 
		/// 그러나 가상 공간이 한정되어 있으므로 프로세스가 라이브러리를 로드하는 데 제한을 받을 수 있습니다.
		/// </remarks>
		/// <param name="performanceCounter">The performance counter.</param>
		/// <param name="memorySizeType"><seealso cref="X10.Common.Utility.MemorySizeTypes"/>.</param>
		/// <param name="processName">프로세스 이름, 없을 경우 현재 프로세스 이름을 사용.</param>
		/// <returns>System.String.</returns>
		/// <exception cref="System.ArgumentNullException">performanceCounter</exception>
		public static string GetProcessVirtualBytes(
			this PerformanceCounter performanceCounter,
			MemorySizeTypes memorySizeType = MemorySizeTypes.KiloByteShort,
			string processName = "")
		{
			return Utilities.GetMemorySizedTypeString(memorySizeType, performanceCounter.GetProcessVirtualBytes(processName));
		}

		/// <summary>
		/// processName의 한 시점에서 프로세스가 사용한 가상 주소 공간의 최대 크기(Virtual Bytes Peak)를 구합니다.
		/// </summary>
		/// <remarks>
		/// Virtual Bytes Peak는 한 시점에서 프로세스가 사용한 가상 주소 공간의 최대 크기(바이트)입니다.
		/// 가상 주소 공간의 사용이 해당 디스크 또는 주 메모리 페이지를 반드시 사용함을 뜻하지는 않습니다. 
		/// 그러나 가상 공간이 한정되어 있으므로 프로세스가 라이브러리를 로드하는 데 제한을 받을 수 있습니다.
		/// </remarks>
		/// <param name="performanceCounter">The performance counter.</param>
		/// <param name="processName">프로세스 이름, 없을 경우 현재 프로세스 이름을 사용.</param>
		/// <returns>System.Int64.</returns>
		/// <exception cref="System.ArgumentNullException">performanceCounter</exception>
		public static long GetProcessVirtualBytesPeak(
			this PerformanceCounter performanceCounter,
			string processName = "")
		{
			performanceCounter.SetProcessCategory(processName).CounterName = "Virtual Bytes Peak";

			return (long)performanceCounter.NextValue();
		}

		/// <summary>
		/// processName의 한 시점에서 프로세스가 사용한 가상 주소 공간의 최대 크기(Virtual Bytes Peak)를 구합니다.
		/// </summary>
		/// <remarks>
		/// Virtual Bytes Peak는 한 시점에서 프로세스가 사용한 가상 주소 공간의 최대 크기(바이트)입니다.
		/// 가상 주소 공간의 사용이 해당 디스크 또는 주 메모리 페이지를 반드시 사용함을 뜻하지는 않습니다. 
		/// 그러나 가상 공간이 한정되어 있으므로 프로세스가 라이브러리를 로드하는 데 제한을 받을 수 있습니다.
		/// </remarks>
		/// <param name="performanceCounter">The performance counter.</param>
		/// <param name="memorySizeType"><seealso cref="X10.Common.Utility.MemorySizeTypes"/>.</param>
		/// <param name="processName">프로세스 이름, 없을 경우 현재 프로세스 이름을 사용.</param>
		/// <returns>System.String.</returns>
		/// <exception cref="System.ArgumentNullException">performanceCounter</exception>
		public static string GetProcessVirtualBytesPeak(
			this PerformanceCounter performanceCounter,
			MemorySizeTypes memorySizeType = MemorySizeTypes.KiloByteShort,
			string processName = "")
		{
			return Utilities.GetMemorySizedTypeString(memorySizeType, performanceCounter.GetProcessVirtualBytesPeak(processName));
		}
		#endregion
	}
}
