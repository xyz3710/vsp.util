﻿/**********************************************************************************************************************/
/*	Domain		:	System.XmlStringExtensions
/*	Creator		:	KIMKIWON\xyz37(김기원)
/*	Create		:	2011년 7월 12일 화요일 오후 9:17
/*	Purpose		:	Xml 문자열에 적용되는 확장 기능을 지원합니다.
/*--------------------------------------------------------------------------------------------------------------------*/
/*	Modifier	:	
/*	Update		:	
/*	Changes		:	
/*--------------------------------------------------------------------------------------------------------------------*/
/*	Comment		:	
/*--------------------------------------------------------------------------------------------------------------------*/
/*	Reviewer	:	Kim Ki Won
/*	Rev. Date	:	2011년 10월 14일 금요일 오후 5:04
/**********************************************************************************************************************/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using System.Threading.Tasks;

namespace System.Xml
{
	/// <summary>
	/// Xml 문자열에 적용되는 확장 기능을 지원합니다.
	/// </summary>
	public static class XmlStringExtensions
	{
		/// <summary>
		/// Xml 태그 값의 문자열 변경 Rule을 적용 합니다.
		/// </summary>
		/// <param name="xmlString">rule을 적용받을 Xml 문자열</param>
		/// <returns>변경 rule이 적용된 Xml 문자열</returns>
		/// <example>
		/// <code>
		/// &amp; =&gt; &amp;amp;
		/// &gt; =&gt; &amp;gt;
		/// &lt; =&gt; &amp;lt;
		/// &quot; =&gt; &amp;quot;
		/// &apos; =&gt; &amp;apos;
		/// </code>
		/// </example>
		public static string ReplaceByXmlRule(this string xmlString)
		{
			return String.IsNullOrEmpty(xmlString) == true ?
					   string.Empty :
					   xmlString.Replace("&", "&amp;")
						.Replace(">", "&gt;")
						.Replace("<", "&lt;")
						.Replace("\"", "&quot;")
						.Replace("'", "&apos;");
		}
	}
}
