﻿// ****************************************************************************************************************** //
//	Domain		:	System.ExceptionExtensions
//	Creator		:	KIMKIWON\xyz37(Kim Ki Won)
//	Create		:	2014년 3월 13일 목요일 오후 4:10
//	Purpose		:	Exception의 확장 기능을 제공 합니다.
// ------------------------------------------------------------------------------------------------------------------ //
//	Modifier	:	
//	Update		:	
//	Changes		:	
// ------------------------------------------------------------------------------------------------------------------ //
//	Comment		:	
// ------------------------------------------------------------------------------------------------------------------ //
//	Reviewer	:	
//	Rev. Date	:	
//	Comment		:	
// ------------------------------------------------------------------------------------------------------------------ //
//	<copyright file="ExceptionExtensions.cs" company="(주)가치소프트">
//		Copyright (c) 2014. (주)가치소프트. All rights reserved.
//	</copyright>
// <summary></summary>
// ****************************************************************************************************************** //

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace System
{
	/// <summary>
	/// Exception의 확장 기능을 제공 합니다.
	/// </summary>
	public static class ExceptionExtensions
	{
		/// <summary>
		/// Gets the flatten messages.
		/// </summary>
		/// <param name="aggregateException">The aggregate exception.</param>
		/// <returns>System.String.</returns>
		/// <exception cref="System.ArgumentException">aggregateException is null</exception>
		public static string GetFlattenMessages(this AggregateException aggregateException)
		{
			if (aggregateException == null)
			{
				throw new ArgumentException("aggregateException");
			}

			List<string[]> result = new List<string[]>();

			foreach (Exception innerException in aggregateException.Flatten().InnerExceptions)
			{
				result.AddRange(GetInnerExceptionMessages(innerException));
			}

			if (result.Count == 0)
			{
				return string.Empty;
			}

			string message = result.Aggregate<string[], string>(
										string.Empty,
										(acc, next) =>
										{
											string nextValue = next[0].ToString();

											if (acc == string.Empty)
												return nextValue;

											return string.Format("{0}{1}{2}", acc, Environment.NewLine, nextValue);
										});
			string stackTrace = result.Aggregate<string[], string>(
										string.Empty,
										(acc, next) =>
										{
											string nextValue = next[1].ToString();

											if (acc == string.Empty)
												return nextValue;

											return string.Format("{0}{1}{2}", acc, Environment.NewLine, nextValue);
										});
#if DEBUG
			return string.Format(
					   "{1}{0}{2}{0}{3}",
					   Environment.NewLine,
					   message,
					   string.Empty.PadRight(40, '='),
					   stackTrace);
#else
			return message;
#endif
		}

		/// <summary>
		/// InnerException 메세지를 평면화하여 구합니다.
		/// </summary>
		/// <param name="exception"></param>
		/// <returns></returns>
		public static StringBuilder GetFlattenInnerMessages(this Exception exception)
		{
			StringBuilder sb = new StringBuilder();
			Queue<Exception> queue = new Queue<Exception>();

			if (exception == null)
				return sb;

			sb.AppendLine(exception.Message);

			if (exception.InnerException != null)
			{
				queue.Enqueue(exception.InnerException);
			}

			while (queue.Count > 0)
			{
				var innerEx = queue.Dequeue();

				sb.AppendLine(innerEx.Message);

				if (innerEx.InnerException == null)
				{
					continue;
				}

				queue.Enqueue(innerEx.InnerException);
			}

			return sb;
		}

		/// <summary>
		/// InnerException 메세지를 평면화하여 구합니다.
		/// </summary>
		/// <param name="exception"></param>
		/// <param name="reverseMessage">마지막 메세지를 처음으로 나타나게 할지 여부</param>
		/// <returns></returns>
		public static string GetFlattenInnerMessages(this Exception exception, bool reverseMessage = false)
		{
			StringBuilder sb = exception.GetFlattenInnerMessages();

			if (reverseMessage == true)
				return sb.ToString().SplitDelimiter(Environment.NewLine).Reverse().MergeDelimiter(Environment.NewLine);

			return sb.ToString(0, sb.Length - 2);
		}

		private static List<string[]> GetInnerExceptionMessages(Exception exception)
		{
			List<string[]> result = new List<string[]>();
			string[] stackTrace = new string[2];
			Queue<Exception> queue = new Queue<Exception>();

			queue.Enqueue(exception);

			while (queue.Count > 0)
			{
				var inner = queue.Dequeue();

				if (inner == null)
				{
					continue;
				}

				stackTrace[0] = inner.Message;
				stackTrace[1] = inner.StackTrace;
				result.Add(stackTrace);
				queue.Enqueue(inner.InnerException);
			}

			return result;
		}

		/// <summary>
		/// 평면화 된 InnerException의 마지막 메세지를 구합니다.
		/// </summary>
		/// <param name="exception"></param>
		/// <returns></returns>
		public static string GetLastInnerMessages(this Exception exception)
		{
			StringBuilder sb = exception.GetFlattenInnerMessages();
			var messages = sb.ToString().SplitDelimiter(Environment.NewLine).ToArray();

			return messages[messages.Length - 1];
		}
	}
}
