﻿/**********************************************************************************************************************/
/*	Domain		:	System.TypeExtensions
/*	Creator		:	KIMKIWON\xyz37(김기원)
/*	Create		:	2011년 7월 12일 화요일 오후 9:17
/*	Purpose		:	타입에 적용되는 확장 기능을 지원합니다.
/*--------------------------------------------------------------------------------------------------------------------*/
/*	Modifier	:	
/*	Update		:	
/*	Changes		:	
/*--------------------------------------------------------------------------------------------------------------------*/
/*	Comment		:	
/*--------------------------------------------------------------------------------------------------------------------*/
/*	Reviewer	:	Kim Ki Won
/*	Rev. Date	:	2011년 7월 12일 화요일 오후 9:21
/**********************************************************************************************************************/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;

namespace System
{
	/// <summary>
	/// 타입에 적용되는 확장 기능을 지원합니다.
	/// </summary>
	public static class TypeExtensions
	{
		#region Nullable
		/// <summary>
		/// Null이나 0일 경우 defaultValue로 설정합니다.
		/// </summary>
		/// <param name="value"></param>
		/// <param name="defaultValue"></param>
		/// <returns></returns>
		public static int NullOrZeroToValue(this int? value, int defaultValue)
		{
			int result = defaultValue;

			if (value == null || value == 0)
				result = defaultValue;

			return result;
		}

		/// <summary>
		/// Nullable type인지 검사합니다.
		/// </summary>
		/// <param name="checkedType">검사하려는 type</param>
		/// <returns></returns>
		public static bool IsNullableType(this Type checkedType)
		{
			return checkedType.IsGenericType
				&& checkedType.GetGenericTypeDefinition() == (typeof(Nullable<>));
		}

		/// <summary>
		/// <see cref="NullableConverter"/>를 이용하여 NullableType의 내부 형식을 가져옵니다.
		/// </summary>
		/// <param name="nullableType">Nullable type</param>
		/// <returns>Nullable type이면 내부 형식을 반환하고, 그렇지 않으면 null을 반환 합니다.</returns>
		public static Type GetNullableUnderlyingType<T>(this T? nullableType)
			where T : struct
		{
			return GetNullableUnderlyingType(nullableType.GetType());
		}

		/// <summary>
		/// <see cref="NullableConverter"/>를 이용하여 NullableType의 내부 형식을 가져옵니다.
		/// </summary>
		/// <param name="nullableType">Nullable type</param>
		/// <returns>Nullable type이면 내부 형식을 반환하고, 그렇지 않으면 null을 반환 합니다.</returns>
		public static Type GetNullableUnderlyingType(this Type nullableType)
		{
			if (nullableType != null && nullableType.IsNullableType() == true)
			{
				NullableConverter nullableConverter = new NullableConverter(nullableType);

				return nullableConverter.UnderlyingType;
			}
			else
				return null;
		}
		#endregion

		#region IsDerivedBaseType
		/// <summary>
		/// Type이 지정된 base type에서 상속 받았는지를 <see cref="Type.FullName"/> 일부로 검사합니다.
		/// </summary>
		/// <param name="checkedType">검사하려는 type</param>
		/// <param name="partOfBaseTypeFullName">base type <see cref="Type.FullName"/>의 일부 단어</param>
		/// <returns>지정된 Type으로 부터 상속 파생되었으면 true를 반환합니다.</returns>
		/// <seealso cref="IsDerivedBaseType(Type, string, bool)"/>
		public static bool IsDerivedBaseType(this Type checkedType, string partOfBaseTypeFullName)
		{
			bool exactlyMatchFullName = false;

			return IsDerivedBaseType(checkedType, partOfBaseTypeFullName, exactlyMatchFullName);
		}

		/// <summary>
		/// Type이 지정된 base type에서 상속 받았는지를 검사합니다.
		/// </summary>
		/// <param name="checkedType">검사하려는 type</param>
		/// <param name="baseTypeFullName">base type의 <see cref="Type.FullName"/></param>
		/// <param name="exactlyMatchFullName">baseTypeFullName을 정확하게 일치하는 단어로 검사할지 여부</param>
		/// <returns>지정된 Type으로 부터 상속 파생되었으면 true를 반환합니다.</returns>
		/// <seealso cref="IsDerivedBaseType(Type, string)"/>
		public static bool IsDerivedBaseType(this Type checkedType, string baseTypeFullName, bool exactlyMatchFullName)
		{
			bool checkBaseTypeResult = false;

			if (checkedType == null || baseTypeFullName == string.Empty)
				return checkBaseTypeResult;

			Queue<Type> checkTypeQueue = new Queue<Type>();

			checkTypeQueue.Enqueue(checkedType.BaseType);

			while (checkTypeQueue.Count > 0)
			{
				Type baseType = checkTypeQueue.Dequeue();

				if (baseType == null)
					continue;

				if (exactlyMatchFullName == false)
				{
					if (baseType.FullName.Contains(baseTypeFullName) == true)
						return true;
				}
				else
				{
					if (baseType.FullName == baseTypeFullName)
						return true;
				}

				checkTypeQueue.Enqueue(baseType.BaseType);
			}

			return checkBaseTypeResult;
		}
		#endregion

		#region GetPropertiesOfAttribute<T>
		/// <summary>
		/// 지정된 타입에서 지정한 T 타입의 Attribute를 사용한 Property를 구합니다.
		/// <remarks>T 형식은 ~~Attribute 이름 까지 지정해야 합니다.</remarks>
		/// </summary>
		/// <typeparam name="T">검색할 특성의 형식입니다. 이 형식에 할당할 수 있는 특성만 반환됩니다.</typeparam>
		/// <param name="type">The type.</param>
		/// <param name="inherit">특성을 찾기 위해 이 멤버의 상속 체인을 검색하려면 true로 설정하고,
		/// 그렇지 않으면 false로 설정합니다. 이 매개 변수는 속성과 이벤트의 경우 무시됩니다. 설명 부분을 참조하십시오.</param>
		/// <returns>IEnumerable{PropertyAttribute{``0}}.</returns>
		/// <exception cref="System.ArgumentNullException">type</exception>
		public static IEnumerable<PropertyAttribute<T>> GetPropertiesOfAttribute<T>(this Type type, bool inherit = false)
			where T : class
		{
			if (type == null)
			{
				throw new ArgumentNullException("type");
			}

			return from p in type.GetProperties()
				   let attr = p.GetCustomAttributes(typeof(T), inherit)
				   where attr.Length == 1
				   select new PropertyAttribute<T>
				   {
					   Property = p,
					   Attribute = (T)attr.First(),
				   };
		}
		#endregion

		#region DefaultValue
		/// <summary>
		/// 지정한 타입의 기본값을 구합니다.
		/// </summary>
		/// <param name="type">The type.</param>
		/// <returns>지정한 타입의 기본값을 반환 합니다.</returns>
		public static dynamic DefaultValue(this Type type)
		{
			if (type.IsValueType && Nullable.GetUnderlyingType(type) == null)
			{
				return Activator.CreateInstance(type);
			}
			else
			{
				return null;
			}
		}
		#endregion
	}
}
