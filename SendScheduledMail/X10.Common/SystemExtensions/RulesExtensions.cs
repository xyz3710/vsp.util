﻿/**********************************************************************************************************************/
/*	Domain		:	System.RulesExtension
/*	Creator		:	KIM-KIWON\xyz37(김기원)
/*	Create		:	2012년 10월 24일 수요일 오후 2:16
/*	Purpose		:	파일 이름 등 특정 규칙에 대한 확장 기능을 지원합니다.
/*--------------------------------------------------------------------------------------------------------------------*/
/*	Modifier	:	
/*	Update		:	
/*	Changes		:	
/*--------------------------------------------------------------------------------------------------------------------*/
/*	Comment		:	
/*--------------------------------------------------------------------------------------------------------------------*/
/*	Reviewer	:	Kim Ki Won
/*	Rev. Date	:	
/**********************************************************************************************************************/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace System
{
	/// <summary>
	/// 파일 이름 등 특정 규칙에 대한 확장 기능을 지원합니다.
	/// </summary>
	public static class RulesExtension
	{
		/// <summary>
		/// 파일이름 규칙을 적용합니다.
		/// </summary>
		/// <param name="fileName">규칙을 적용할 파일 이름</param>
		/// <param name="replaceString">위배되는 규칙을 변경할 문자</param>
		public static string SetFileNameRule(this string fileName, string replaceString = ",")
		{
			if (string.IsNullOrEmpty(fileName) == true)
				return string.Empty;

			string[] excludedCharaters = new string[] { "\\", "/", ":", "*", "?", "\"", "<", ">", "|", };

			Parallel.ForEach(excludedCharaters, x =>
			{
				fileName = fileName.Replace(x, replaceString);
			});

			return fileName;
		}
	}
}
