﻿// ****************************************************************************************************************** //
//	Domain		:	System.Collections.Generic.KeyValuePairExtensions
//	Creator		:	X10-MOBILE\xyz37(Kim Ki Won)
//	Create		:	Thursday, August 28, 2014 9:32 AM
//	Purpose		:	KeyValuePair에 확장 기능을 제공합니다.
// ------------------------------------------------------------------------------------------------------------------ //
//	Modifier	:	
//	Update		:	
//	Changes		:	
// ------------------------------------------------------------------------------------------------------------------ //
//	Comment		:	
// ------------------------------------------------------------------------------------------------------------------ //
//	Reviewer	:	Kim Ki Won
//	Rev. Date	:	Thursday, August 28, 2014 9:33 AM
// ****************************************************************************************************************** //

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace System.Collections.Generic
{
	/// <summary>
	/// KeyValuePair에 확장 기능을 제공합니다.
	/// </summary>
	public static class KeyValuePairExtensions
	{
		/// <summary>
		/// 특정 키에 값을 할당한다.
		/// </summary>
		/// <param name="key">문자열 키값</param>
		/// <param name="value">키에 따른 값</param>
		/// <returns>새로 생성된 KeyValuePair&lt;string, dynamic&gt; 값을 반환합니다.</returns>
		public static KeyValuePair<string, dynamic> With(this string key, dynamic value)
		{
			return new KeyValuePair<string, dynamic>(key, value);
		}
	}
}
