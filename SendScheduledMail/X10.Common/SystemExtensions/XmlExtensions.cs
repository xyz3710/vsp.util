﻿// ****************************************************************************************************************** //
//	Domain		:	System.Xml.XmlExtensions
//	Creator		:	X10-MOBILE\xyz37(Kim Ki Won)
//	Create		:	Sunday, August 24, 2014 9:31 PM
//	Purpose		:	Xml에 적용되는 확장 기능을 지원합니다.
// ------------------------------------------------------------------------------------------------------------------ //
//	Modifier	:	
//	Update		:	
//	Changes		:	
// ------------------------------------------------------------------------------------------------------------------ //
//	Comment		:	
// ------------------------------------------------------------------------------------------------------------------ //
//	Reviewer	:	Kim Ki Won
//	Rev. Date	:	Monday, August 25, 2014 7:08 AM
//	Comment		:	
// ****************************************************************************************************************** //

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace System.Xml
{
	/// <summary>
	/// Xml에 적용되는 확장 기능을 지원합니다.
	/// </summary>
	public static class XmlExtensions
	{
		/// <summary>
		/// Xml 노드 목록 중 지정된 이름이 있는 노드를 찾습니다.
		/// </summary>
		/// <param name="nodes">The nodes.</param>
		/// <param name="name">The node name.</param>
		/// <param name="stringComparison">문자열 비교 규칙(기본값으론 현재 문화권에 대해대/소문자를 구별하지 않습니다.)</param>
		/// <returns>찾을 경우 해당 XmlNode를 그렇지 않으면 null을 반환 합니다..</returns>
		public static XmlNode Find(
			this XmlNodeList nodes,
			string name,
			StringComparison stringComparison = StringComparison.CurrentCultureIgnoreCase)
		{
			return nodes.Cast<XmlNode>().SingleOrDefault(x => string.Compare(x.Name, name, stringComparison) == 0);
		}

		/// <summary>
		/// Xml 노드 목록 중 지정된 이름이 있는 노드의 인덱스를 찾습니다.
		/// </summary>
		/// <param name="nodes">The nodes.</param>
		/// <param name="name">The name.</param>
		/// <param name="stringComparison">문자열 비교 규칙(기본값으론 현재 문화권에 대해대/소문자를 구별하지 않습니다.)</param>
		/// <returns>찾을 경우 해당 XmlNode를 그렇지 않으면 null을 반환 합니다..</returns>
		/// <returns>System.Int32.</returns>
		public static int IndexOf(
			this XmlNodeList nodes,
			string name,
			StringComparison stringComparison = StringComparison.CurrentCultureIgnoreCase)
		{
			var node = nodes.Cast<XmlNode>()
				.Select((x, index) => new
				{
					Node = x,
					Index = index,
				})
				.SingleOrDefault(x => string.Compare(x.Node.Name, name, stringComparison) == 0);

			if (node == null)
			{
				return -1;
			}

			return node.Index;
		}

		/// <summary>
		/// Xml 노드 목록 중 InnerText에 특정 단어가 있는 노드를 찾습니다.
		/// </summary>
		/// <param name="nodes">The nodes.</param>
		/// <param name="innerText">The inner text.</param>
		/// <param name="stringComparison">문자열 비교 규칙(기본값으론 현재 문화권에 대해대/소문자를 구별하지 않습니다.)</param>
		/// <returns>찾을 경우 해당 XmlNode를 그렇지 않으면 null을 반환 합니다..</returns>
		public static XmlNode FindInnerText(
			this XmlNodeList nodes,
			string innerText,
			StringComparison stringComparison = StringComparison.CurrentCultureIgnoreCase)
		{
			return nodes.Cast<XmlNode>().SingleOrDefault(x => string.Compare(x.InnerText, innerText, stringComparison) == 0);
		}
	}
}
