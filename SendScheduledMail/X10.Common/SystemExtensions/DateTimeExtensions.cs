﻿/**********************************************************************************************************************/
/*	Domain		:	System.TypeExtensions
/*	Creator		:	KIMKIWON\xyz37(김기원)
/*	Create		:	2011년 7월 12일 화요일 오후 9:17
/*	Purpose		:	날자형에 적용되는 확장 기능을 지원합니다.
/*--------------------------------------------------------------------------------------------------------------------*/
/*	Modifier	:	
/*	Update		:	
/*	Changes		:	
/*--------------------------------------------------------------------------------------------------------------------*/
/*	Comment		:	
/*--------------------------------------------------------------------------------------------------------------------*/
/*	Reviewer	:	Kim Ki Won
/*	Rev. Date	:	2011년 10월 14일 금요일 오후 5:05
/**********************************************************************************************************************/

using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;

namespace System
{
	/// <summary>
	/// 날짜형 적용되는 확장 기능을 지원합니다.
	/// </summary>
	public static class DateTimeExtensions
	{
		/// <summary>
		/// 날짜 시간에 사용되는 yyyy-MM-dd HH:mm:ss:fff 의 23자리 문자열을 구합니다.
		/// </summary>
		/// <param name="value"></param>
		/// <returns></returns>
		public static string To23DateTimeString(this DateTime value)
		{
			return value.ToString(StringExtensions.DATE_TIME_FORMATSTRING23);
		}

		/// <summary>
		/// 날짜 시간에 사용되는 yyyy-MM-dd HH:mm:ss:fff 의 23자리 문자열을 구합니다.
		/// </summary>
		/// <param name="value"></param>
		/// <returns></returns>
		public static string To23DateTimeString(this DateTime? value)
		{
			return value == null ? string.Empty : ((DateTime)value).To23DateTimeString();
		}

		/// <summary>
		/// 날짜 시간에 사용되는 yyyy-MM-dd HH:mm:ss 의 19자리 문자열을 구합니다.
		/// </summary>
		/// <param name="value"></param>
		/// <returns></returns>
		public static string To19DateTimeString(this DateTime value)
		{
			return value.ToString(StringExtensions.DATE_TIME_FORMATSTRING19);
		}

		/// <summary>
		/// 날짜 시간에 사용되는 yyyy-MM-dd HH:mm:ss 의 19자리 문자열을 구합니다.
		/// </summary>
		/// <param name="value"></param>
		/// <returns></returns>
		public static string To19DateTimeString(this DateTime? value)
		{
			return value == null ? string.Empty : ((DateTime)value).To19DateTimeString();
		}

		/// <summary>
		/// 날짜 시간에 사용되는 yyyyMMddHHmmss 의 14자리 문자열을 구합니다.
		/// </summary>
		/// <param name="value"></param>
		/// <returns></returns>
		public static string To14DateTimeString(this DateTime value)
		{
			return value.ToString(StringExtensions.DATE_TIME_FORMATSTRING14);
		}

		/// <summary>
		/// 날짜 시간에 사용되는 yyyyMMddHHmmss 의 14자리 문자열을 구합니다.
		/// </summary>
		/// <param name="value"></param>
		/// <returns></returns>
		public static string To14DateTimeString(this DateTime? value)
		{
			return value == null ? string.Empty : ((DateTime)value).To14DateTimeString();
		}

		/// <summary>
		/// 날짜에 사용되는 yyyy-MM-dd 의 10자리 문자열을 구합니다.
		/// </summary>
		/// <param name="value"></param>
		/// <returns></returns>
		public static string To10DateString(this DateTime value)
		{
			return value.ToString(StringExtensions.DATE_FORMATSTRING10);
		}

		/// <summary>
		/// 날짜에 사용되는 yyyy-MM-dd 의 10자리 문자열을 구합니다.
		/// </summary>
		/// <param name="value"></param>
		/// <returns></returns>
		public static string To10DateString(this DateTime? value)
		{
			return value == null ? string.Empty : ((DateTime)value).To10DateString();
		}

		/// <summary>
		/// 날짜에 사용되는 yyyyMMdd 의 8자리 문자열을 구합니다.
		/// </summary>
		/// <param name="value"></param>
		/// <returns></returns>
		public static string To8DateString(this DateTime value)
		{
			return value.ToString(StringExtensions.DATE_FORMATSTRING8);
		}

		/// <summary>
		/// 날짜에 사용되는 yyyyMMdd 의 8자리 문자열을 구합니다.
		/// </summary>
		/// <param name="value"></param>
		/// <returns></returns>
		public static string To8DateString(this DateTime? value)
		{
			return value == null ? string.Empty : ((DateTime)value).To8DateString();
		}

		/// <summary>
		/// 해당 일자의 첫번째 요일을 구합니다.(일요일을 요일의 시작으로 합니다.)
		/// </summary>
		/// <param name="value">The value.</param>
		/// <returns></returns>
		public static DateTime GetFirstDayOfWeek(this DateTime value)
		{
			return value.AddDays(0 - (int)value.DayOfWeek);
		}

		/// <summary>
		/// 해당 월의 1일의 날짜 개체를 구합니다.
		/// </summary>
		/// <param name="value"></param>
		/// <returns></returns>
		public static DateTime GetFirstDayOfMonth(this DateTime value)
		{
			return new DateTime(value.Year, value.Month, 1);
		}

		/// <summary>
		/// 해당 년도의 1월 1일의 날짜 개체를 구합니다.
		/// </summary>
		/// <param name="value"></param>
		/// <returns></returns>
		public static DateTime GetFirstDayOfYear(this DateTime value)
		{
			return new DateTime(value.Year, 1, 1);
		}

		/// <summary>
		/// 해당일자가 1월 1일로 부터 몇번째 주인지를 구합니다.
		/// </summary>
		/// <param name="value">몇째주인지를 구하고자 하는 일자입니다.</param>
		/// <returns></returns>
		public static int GetWeekOfYear(this DateTime value)
		{
			GregorianCalendar gregorianCalendar = new GregorianCalendar();

			return gregorianCalendar.GetWeekOfYear(value, CalendarWeekRule.FirstFourDayWeek, DayOfWeek.Monday);
		}

		/// <summary>
		/// Telerik UI에서 사용되는 빈 DateTime(1900년 1월 1일 0시 0분 0초)을 구합니다.
		/// </summary>
		/// <param name="dateTime"></param>
		/// <returns></returns>
		public static DateTime Empty(this DateTime dateTime)
		{
			return new DateTime(1900, 1, 1, 0, 0, 0);
		}

		/// <summary>
		/// dateTime이 <seealso cref="Empty"/> 이면 null을 그렇지 않으면 dateTime의 format으로 서식화 된 문자열을 반환합니다.
		/// </summary>
		/// <param name="dateTime"></param>
		/// <param name="format"></param>
		/// <returns></returns>
		public static string GetEmptyIsNull(this DateTime dateTime, string format = StringExtensions.DATE_TIME_FORMATSTRING19)
		{
			return dateTime == dateTime.Empty() ? string.Empty : dateTime.ToString(format);
		}

		/// <summary>
		/// dateTime이 null 이면 string.Empty를 그렇지 않으면 dateTime의 format으로 서식화 된 문자열을 반환합니다.
		/// </summary>
		/// <param name="dateTime"></param>
		/// <param name="format"></param>
		/// <returns></returns>
		public static string GetEmptyIsNull(this DateTime? dateTime, string format = StringExtensions.DATE_TIME_FORMATSTRING19)
		{
			return dateTime == null ? string.Empty : ((DateTime)dateTime).ToString(format);
		}
	}
}
