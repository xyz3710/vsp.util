﻿/**********************************************************************************************************************/
/*	Domain		:	System.IO.IOExtensions
/*	Creator		:	X10\xyz37(김기원)
/*	Create		:	2012년 12월 16일 일요일 오후 7:53
/*	Purpose		:	IO에 대한 확장 기능을 제공합니다.
/*--------------------------------------------------------------------------------------------------------------------*/
/*	Modifier	:	
/*	Update		:	
/*	Changes		:	
/*--------------------------------------------------------------------------------------------------------------------*/
/*	Comment		:	
/*--------------------------------------------------------------------------------------------------------------------*/
/*	Reviewer	:	Kim Ki Won
/*	Rev. Date	:	
/**********************************************************************************************************************/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace System.IO
{
	/// <summary>
	/// IO에 대한 확장 기능을 제공합니다.
	/// </summary>
	public static class IOExtensions
	{
		/// <summary>
		///지정된 경로 문자열에 대한 디렉터리 이름을 구합니다.
		/// </summary>
		/// <example>C:\Windows\System32\cmd.exe 일 경우 System32반 반환합니다.</example>
		/// <param name="path">파일 또는 디렉터리의 경로입니다.</param>
		/// <remarks>확장자를 구별하는 .이 있으면 파일로 인식합니다.</remarks>
		/// <returns></returns>
		/// <exception cref="System.ArgumentException">path 매개 변수가 유효하지 않은 문자를 포함하거나, 비어 있거나, 공백만 포함하는 경우</exception>
		public static string GetDirectoryNameOnly(this string path)
		{
			if (string.IsNullOrEmpty(path) == true)
				throw new ArgumentException("path");

			char separatorChar = path.Contains(Path.DirectorySeparatorChar) == true ? Path.DirectorySeparatorChar : Path.AltDirectorySeparatorChar;

			return path.Split(separatorChar).LastOrDefault(x => x.Contains(".") == false);
		}

		/// <summary>
		/// path에서 /(AltDirectorySeparatorChar)를 \(DirectorySeparatorChar)로 변경합니다.
		/// </summary>
		/// <param name="path">파일 또는 디렉터리의 경로입니다.</param>
		/// <returns></returns>
		/// <exception cref="System.ArgumentException">path 매개 변수가 유효하지 않은 문자를 포함하거나, 비어 있거나, 공백만 포함하는 경우</exception>
		public static string ReplaceAltDirectorySeparatorChar(this string path)
		{
			if (string.IsNullOrEmpty(path) == true)
				throw new ArgumentException("path");

			return path.Replace(Path.AltDirectorySeparatorChar, Path.DirectorySeparatorChar);
		}

		/// <summary>
		/// ; 로 구별한 searchPattern의 모든 파일 목록을 구합니다.
		/// </summary>
		/// <param name="path"></param>
		/// <param name="searchPattern"></param>
		/// <param name="searchOption"></param>
		/// <returns></returns>
		public static string[] GetFiles(this string path, string searchPattern, SearchOption searchOption = SearchOption.TopDirectoryOnly)
		{
			var patterns = searchPattern.Split(';');
			List<string> files = new List<string>();

			Parallel.ForEach(patterns, pattern =>
			{
				files.AddRange(Directory.GetFiles(path, pattern, searchOption));
			});

			return files.ToArray();
		}

		/// <summary>
		/// 파일 길이를 GB, MB, KB, bytes 자동으로 단위를 붙여서 보여줍니다.
		/// </summary>
		/// <param name="fileLength">Length of the file.</param>
		/// <returns>System.String.</returns>
		public static string FormatFileLength(this long fileLength)
		{
			int[] limits = new int[] { 1024 * 1024 * 1024, 1024 * 1024, 1024 };
			string[] units = new string[] { "GB", "MB", "KB" };

			for (int i = 0; i < limits.Length; i++)
			{
				if (fileLength >= limits[i])
					return String.Format("{0:#,##0.##} " + units[i], ((double)fileLength / limits[i]));
			}

			return String.Format("{0} bytes", fileLength);
		}

		/// <summary>
		/// path의 폴더가 없으면 생성한 뒤 path를 반환합니다.
		/// </summary>
		/// <param name="path">The path.</param>
		/// <returns>System.String.</returns>
		public static string CheckDirectory(this string path)
		{
			if (Directory.Exists(path) == false)
			{
				try
				{
					Directory.CreateDirectory(path);
				}
				catch (IOException ex)
				{
					throw ex;
				}
			}

			return path;
		}
	}
}
