﻿namespace X10.Common.Utility
{
	partial class ProgressForm
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ProgressForm));
			this.tlpMain = new System.Windows.Forms.TableLayoutPanel();
			this.pnlBottom = new System.Windows.Forms.Panel();
			this.pbWait = new System.Windows.Forms.ProgressBar();
			this.pnlTop = new System.Windows.Forms.Panel();
			this.lblTitle = new System.Windows.Forms.Label();
			this.pnlMiddle = new System.Windows.Forms.Panel();
			this.lblMessage = new System.Windows.Forms.Label();
			this.pnlMain = new System.Windows.Forms.Panel();
			this.tlpMain.SuspendLayout();
			this.pnlBottom.SuspendLayout();
			this.pnlTop.SuspendLayout();
			this.pnlMiddle.SuspendLayout();
			this.pnlMain.SuspendLayout();
			this.SuspendLayout();
			// 
			// tlpMain
			// 
			this.tlpMain.ColumnCount = 1;
			this.tlpMain.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
			this.tlpMain.Controls.Add(this.pnlBottom, 0, 2);
			this.tlpMain.Controls.Add(this.pnlTop, 0, 0);
			this.tlpMain.Controls.Add(this.pnlMiddle, 0, 1);
			this.tlpMain.Dock = System.Windows.Forms.DockStyle.Fill;
			this.tlpMain.Location = new System.Drawing.Point(0, 0);
			this.tlpMain.Margin = new System.Windows.Forms.Padding(0);
			this.tlpMain.Name = "tlpMain";
			this.tlpMain.RowCount = 3;
			this.tlpMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
			this.tlpMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
			this.tlpMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
			this.tlpMain.Size = new System.Drawing.Size(498, 131);
			this.tlpMain.TabIndex = 1;
			// 
			// pnlBottom
			// 
			this.pnlBottom.BackColor = System.Drawing.Color.White;
			this.pnlBottom.Controls.Add(this.pbWait);
			this.pnlBottom.Dock = System.Windows.Forms.DockStyle.Fill;
			this.pnlBottom.Location = new System.Drawing.Point(0, 91);
			this.pnlBottom.Margin = new System.Windows.Forms.Padding(0);
			this.pnlBottom.Name = "pnlBottom";
			this.pnlBottom.Padding = new System.Windows.Forms.Padding(6, 10, 6, 10);
			this.pnlBottom.Size = new System.Drawing.Size(498, 40);
			this.pnlBottom.TabIndex = 4;
			// 
			// pbWait
			// 
			this.pbWait.Dock = System.Windows.Forms.DockStyle.Fill;
			this.pbWait.Location = new System.Drawing.Point(6, 10);
			this.pbWait.Margin = new System.Windows.Forms.Padding(0);
			this.pbWait.Name = "pbWait";
			this.pbWait.Size = new System.Drawing.Size(486, 20);
			this.pbWait.Step = 1;
			this.pbWait.Style = System.Windows.Forms.ProgressBarStyle.Continuous;
			this.pbWait.TabIndex = 0;
			this.pbWait.Value = 50;
			// 
			// pnlTop
			// 
			this.pnlTop.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(207)))), ((int)(((byte)(221)))), ((int)(((byte)(208)))));
			this.pnlTop.Controls.Add(this.lblTitle);
			this.pnlTop.Dock = System.Windows.Forms.DockStyle.Fill;
			this.pnlTop.Location = new System.Drawing.Point(0, 0);
			this.pnlTop.Margin = new System.Windows.Forms.Padding(0, 0, 0, 1);
			this.pnlTop.Name = "pnlTop";
			this.pnlTop.Size = new System.Drawing.Size(498, 39);
			this.pnlTop.TabIndex = 1;
			// 
			// lblTitle
			// 
			this.lblTitle.BackColor = System.Drawing.Color.Transparent;
			this.lblTitle.Dock = System.Windows.Forms.DockStyle.Fill;
			this.lblTitle.Location = new System.Drawing.Point(0, 0);
			this.lblTitle.Name = "lblTitle";
			this.lblTitle.Padding = new System.Windows.Forms.Padding(6, 0, 0, 0);
			this.lblTitle.Size = new System.Drawing.Size(498, 39);
			this.lblTitle.TabIndex = 3;
			this.lblTitle.Text = "Title";
			this.lblTitle.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// pnlMiddle
			// 
			this.pnlMiddle.BackColor = System.Drawing.Color.White;
			this.pnlMiddle.Controls.Add(this.lblMessage);
			this.pnlMiddle.Dock = System.Windows.Forms.DockStyle.Fill;
			this.pnlMiddle.Location = new System.Drawing.Point(0, 40);
			this.pnlMiddle.Margin = new System.Windows.Forms.Padding(0);
			this.pnlMiddle.Name = "pnlMiddle";
			this.pnlMiddle.Size = new System.Drawing.Size(498, 51);
			this.pnlMiddle.TabIndex = 3;
			// 
			// lblMessage
			// 
			this.lblMessage.BackColor = System.Drawing.Color.Transparent;
			this.lblMessage.Dock = System.Windows.Forms.DockStyle.Fill;
			this.lblMessage.Location = new System.Drawing.Point(0, 0);
			this.lblMessage.Name = "lblMessage";
			this.lblMessage.Padding = new System.Windows.Forms.Padding(6, 0, 0, 0);
			this.lblMessage.Size = new System.Drawing.Size(498, 51);
			this.lblMessage.TabIndex = 3;
			this.lblMessage.Text = "Message\r\nMessage";
			this.lblMessage.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// pnlMain
			// 
			this.pnlMain.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.pnlMain.Controls.Add(this.tlpMain);
			this.pnlMain.Dock = System.Windows.Forms.DockStyle.Fill;
			this.pnlMain.Location = new System.Drawing.Point(0, 0);
			this.pnlMain.Margin = new System.Windows.Forms.Padding(0);
			this.pnlMain.Name = "pnlMain";
			this.pnlMain.Size = new System.Drawing.Size(500, 133);
			this.pnlMain.TabIndex = 2;
			// 
			// ProgressForm
			// 
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
			this.ClientSize = new System.Drawing.Size(500, 133);
			this.ControlBox = false;
			this.Controls.Add(this.pnlMain);
			this.Font = new System.Drawing.Font("맑은 고딕", 11F);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
			this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
			this.Margin = new System.Windows.Forms.Padding(4);
			this.Name = "ProgressForm";
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.TopMost = true;
			this.tlpMain.ResumeLayout(false);
			this.pnlBottom.ResumeLayout(false);
			this.pnlTop.ResumeLayout(false);
			this.pnlMiddle.ResumeLayout(false);
			this.pnlMain.ResumeLayout(false);
			this.ResumeLayout(false);

		}

		#endregion

		private System.Windows.Forms.TableLayoutPanel tlpMain;
		private System.Windows.Forms.Panel pnlBottom;
		private System.Windows.Forms.ProgressBar pbWait;
		private System.Windows.Forms.Panel pnlTop;
		private System.Windows.Forms.Label lblTitle;
		private System.Windows.Forms.Panel pnlMiddle;
		private System.Windows.Forms.Label lblMessage;
		private System.Windows.Forms.Panel pnlMain;



	}
}