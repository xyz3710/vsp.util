﻿// ****************************************************************************************************************** //
//	Domain		:	X10.Common.Utility.TaskbarHelper
//	Creator		:	KIMKIWON\xyz37(Kim Ki Won)
//	Create		:	Thursday, May 15, 2014 6:01 PM
//	Purpose		:	작업 표시줄과 관련된 기능을 제공합니다.
// ------------------------------------------------------------------------------------------------------------------ //
//	Modifier	:	
//	Update		:	
//	Changes		:	
// ------------------------------------------------------------------------------------------------------------------ //
//	Comment		:	
// ------------------------------------------------------------------------------------------------------------------ //
//	Reviewer	:	
//	Rev. Date	:	
//	Comment		:	
// ------------------------------------------------------------------------------------------------------------------ //
//	<copyright file="TaskbarHelper.cs" company="(주)가치소프트">
//		Copyright (c) 2014. (주)가치소프트. All rights reserved.
//	</copyright>
// <summary></summary>
// ****************************************************************************************************************** //

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace X10.Common.Utility
{
	/// <summary>
	/// 작업 표시줄과 관련된 기능을 제공합니다.
	/// </summary>
	public static class TaskbarHelper
	{
		/// <summary>
		/// Windows XP 에서 프로그램이 실행되는지 여부를 구합니다.
		/// </summary>
		public static bool RunningOnXP
		{
			get
			{
				return Environment.OSVersion.Platform == PlatformID.Win32NT &&
					Environment.OSVersion.Version.Major >= 5;
			}
		}

		/// <summary>
		/// Windows Vista 에서 프로그램이 실행되는지 여부를 구합니다.
		/// </summary>
		public static bool RunningOnVista
		{
			get
			{
				return Environment.OSVersion.Version.Major >= 6;
			}
		}

		/// <summary>
		/// Windows 7 에서 프로그램이 실행되는지 여부를 구합니다.
		/// </summary>
		public static bool RunningOnWin7
		{
			get
			{
				// Verifies that OS version is 6.1 or greater, and the Platform is WinNT.
				return Environment.OSVersion.Platform == PlatformID.Win32NT &&
					Environment.OSVersion.Version.CompareTo(new Version(6, 1)) >= 0;
			}
		}

		/// <summary>
		/// 작업 표시줄에 해당 프로그램을 고정하거나 해제 합니다.
		/// </summary>
		/// <param name="targetPath">대상 프로그램 전체 경로(실행 파일이나 바로 가기도 가능)</param>
		/// <param name="pin">true면 작업 표시줄에 고정합니다.</param>
		/// <returns>작업 표시줄에 고정되거나 해제 되면 true를 반환합니다.</returns>
		/// <exception cref="System.PlatformNotSupportedException">Windows 7을 지원하지 않아서 작업 표시줄에 고정할 수 없습니다.</exception>
		public static bool PinToTaskbar(string targetPath, bool pin = true)
		{
			string path = Path.GetDirectoryName(targetPath);
			string targetFile = Path.GetFileName(targetPath);

			return PinToTaskbar(path, targetFile, pin);
		}

		/// <summary>
		/// 작업 표시줄에 해당 프로그램을 고정하거나 해제 합니다.
		/// </summary>
		/// <param name="path">대상 프로그램 경로</param>
		/// <param name="targetFile">대상 프로그램(실행 파일이나 바로 가기도 가능)</param>
		/// <param name="pin">true면 작업 표시줄에 고정합니다.</param>
		/// <returns>작업 표시줄에 고정되거나 해제 되면 true를 반환합니다.</returns>
		/// <exception cref="System.PlatformNotSupportedException">Windows 7을 지원하지 않아서 작업 표시줄에 고정할 수 없습니다.</exception>
		public static bool PinToTaskbar(
			string path,
			string targetFile,
			bool pin = true)
		{
			if (RunningOnWin7 == false)
			{
				throw new PlatformNotSupportedException("Windows 7을 지원하지 않아서 작업 표시줄에 고정할 수 없습니다.");
			}

			const string SHELL_APPLICATION_NAME = "Shell.Application";
			Type shellAppType = Type.GetTypeFromProgID(SHELL_APPLICATION_NAME);
			object shellApp = Activator.CreateInstance(shellAppType);
			// dynamic folder = shellApp.NameSpace(path);		// NameSpace를 못 찾아서 아래 처럼 구현
			dynamic folder = shellAppType.InvokeMethod("NameSpace", shellApp, path);
			dynamic link = folder.ParseName(targetFile);
			dynamic verbs = link.Verbs();
			string doPinString = string.Empty;
			string doUnpinString = string.Empty;
			bool result = false;

			switch (System.Threading.Thread.CurrentThread.CurrentCulture.LCID)
			{
				case 1042:
					doPinString = "작업 표시줄에 고정(&K)";
					doUnpinString = "작업 표시줄에서 제거(&K)";

					break;
				default:
					doPinString = "Pin to Tas&kbar";
					doUnpinString = "Unpin from Tas&kbar";

					break;
			}

			foreach (dynamic verb in verbs)
			{
				string verbName = verb.Name;

				if ((pin && verbName.Equals(doPinString, StringComparison.CurrentCultureIgnoreCase) ||
					(!pin && verbName.Equals(doUnpinString, StringComparison.CurrentCultureIgnoreCase))))
				{
					verb.DoIt();
					result = true;

					break;
				}
			}

			return result;
		}

		private static object InvokeMethod(this Type type, string methodName, object targetInstance, params object[] arguments)
		{
			return type.InvokeMember(
				methodName,
				BindingFlags.Public | BindingFlags.Instance | BindingFlags.InvokeMethod,
				null,
				targetInstance,
				arguments);
		}
	}
}
