﻿// ****************************************************************************************************************** //
//	Domain		:	X10.Common.Utility.Win32NativeMethods
//	Creator		:	KIMKIWON\xyz37(Kim Ki Won)
//	Create		:	2014년 3월 27일 목요일 오후 5:58
//	Purpose		:	Win32 NativeMethods
// ------------------------------------------------------------------------------------------------------------------ //
//	Modifier	:	
//	Update		:	
//	Changes		:	
// ------------------------------------------------------------------------------------------------------------------ //
//	Comment		:	
// ------------------------------------------------------------------------------------------------------------------ //
//	Reviewer	:	
//	Rev. Date	:	
//	Comment		:	
// ------------------------------------------------------------------------------------------------------------------ //
//	<copyright file="Win32NativeMethods.cs" company="(주)가치소프트">
//		Copyright (c) 2014. (주)가치소프트. All rights reserved.
//	</copyright>
// <summary></summary>
// ****************************************************************************************************************** //

using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace X10.Common.Utility
{
	/// <summary>
	/// Win32 NativeMethods.
	/// </summary>
	public class Win32NativeMethods
	{
		#region Constants
		private const string USER32_DLL = "user32.dll";
		#endregion

		/// <summary>
		/// 지정한 핸들의 윈도우를 활성화 상태로 만듭니다.
		/// </summary>
		/// <param name="hWnd">The h WND.</param>
		/// <returns>IntPtr.</returns>
		[DllImport(USER32_DLL, CharSet = CharSet.Auto, ExactSpelling = true)]
		public static extern IntPtr SetForegroundWindow(HandleRef hWnd);

		/// <summary>
		/// 지정한 핸들의 윈도우를 활성화 상태로 만듭니다.
		/// </summary>
		/// <param name="hWnd">The h WND.</param>
		/// <returns><c>true</c> if XXXX, <c>false</c> otherwise.</returns>
		[DllImport(USER32_DLL, CharSet = CharSet.Auto, ExactSpelling = true)]
		public static extern bool SetForegroundWindow(IntPtr hWnd);
	}
}
