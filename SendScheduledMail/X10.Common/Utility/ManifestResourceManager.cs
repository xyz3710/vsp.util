﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace X10.Common.Utility
{
	/// <summary>
	/// 어셈블리에 포함되는 리소스를 관리합니다.
	/// </summary>
	public class ManifestResourceManager
	{
		/// <summary>
		/// 지정된 namespace를 사용한 Resource 이름에 의해 resourceIncludedAssembly에서 Resource stream을 구합니다.
		/// </summary>
		/// <param name="resourceIncludedAssembly">resource가 포함된 assembly</param>
		/// <param name="nameSpace">nameSpace</param>
		/// <param name="resourceName">NameSpace를 제외한 Resource의 이름</param>
		/// <returns></returns>
		public static Stream GetStreamByName(Assembly resourceIncludedAssembly, string nameSpace, string resourceName)
		{
			if (string.IsNullOrEmpty(nameSpace) == true || string.IsNullOrEmpty(resourceName) == true)
				throw new InvalidOperationException("nameSpace나 resourceName이 string.Empty 일 수 없습니다.");

			Stream stream = null;

			if (resourceIncludedAssembly != null)
				stream = resourceIncludedAssembly.GetManifestResourceStream(string.Format("{0}.{1}", nameSpace, resourceName));

			return stream;
		}

		private static bool GetFileFromResourceAction(Stream resourceStream, string targetPath)
		{
			if (string.IsNullOrEmpty(targetPath) == true)
				throw new InvalidOperationException("targetPath 인자는 null이거나 string.Empty일 수 없습니다.");

			string streamPath = Path.GetDirectoryName(targetPath);
			bool result = false;

			if (Directory.Exists(streamPath) == false)
				Directory.CreateDirectory(streamPath);

			using (Stream resStream = resourceStream)
			{
				if (resStream != null)
				{
					byte[] buffer = new byte[resStream.Length];
					int readByte = resStream.Read(buffer, 0, (int)resStream.Length);

					using (FileStream fs = new FileStream(targetPath, FileMode.Create))
					{
						fs.Write(buffer, 0, readByte);
						fs.Flush();
						result = true;
					}
				}
			}

			return result;
		}

		/// <summary>
		/// 해당 type에 포함된 Resource를 파일 형태로 구합니다.
		/// </summary>
		/// <param name="type">포함된 resource를 구할 type(type 정보에는 namespace와 리소스 포함된 assembly 정보를 포함)</param>
		/// <param name="resourceFileName"></param>
		/// <param name="targetPath"></param>
		public static bool GetFileFromResource(Type type, string resourceFileName, string targetPath)
		{
			return GetFileFromResourceAction(GetStreamByName(type.Assembly, type.Namespace, resourceFileName), targetPath);
		}

		/// <summary>
		/// 해당 type에 포함된 Resource를 파일 형태로 구합니다.
		/// </summary>
		/// <param name="resourceIncludedAssembly">리소스가 포함된 어셈블리.</param>
		/// <param name="nameSpace">리소스가 포함된 어셈블리의 네임스페이스.</param>
		/// <param name="resourceFileName">포함된 리소스 파일 이름.</param>
		/// <param name="targetPath">저장될 경로.</param>
		/// <returns>파일에 저장이 되면 true를 반환합니다.</returns>
		public static bool GetFileFromResource(Assembly resourceIncludedAssembly, string nameSpace, string resourceFileName, string targetPath)
		{
			return GetFileFromResourceAction(GetStreamByName(resourceIncludedAssembly, nameSpace, resourceFileName), targetPath);
		}
	}
}
