﻿/**********************************************************************************************************************/
/*	Domain		:	X10.Common.Utility.PropertyForm
/*	Creator		:	KIM-KIWON\xyz37(김기원)
/*	Create		:	2012년 9월 19일 수요일 오후 1:15
/*	Purpose		:	PropertyGrid 컨트롤을 특정 컨트롤에 바인딩 하여 런타임시 컨트롤의 속성을 변경할 수 있는 기능을 제공합니다.
/*--------------------------------------------------------------------------------------------------------------------*/
/*	Modifier	:	
/*	Update		:	
/*	Changes		:	
/*--------------------------------------------------------------------------------------------------------------------*/
/*	Comment		:	
/*--------------------------------------------------------------------------------------------------------------------*/
/*	Reviewer	:	Kim Ki Won
/*	Rev. Date	:	
/**********************************************************************************************************************/

using System;
using System.Linq;
using System.Text;
using System.Data;
using System.Drawing;
using System.Windows.Forms;
using System.ComponentModel;
using System.Collections.Generic;

namespace X10.Common.Utility
{
	/// <summary>
	/// PropertyGrid 컨트롤을 특정 컨트롤에 바인딩 하여 런타임시 컨트롤의 속성을 변경할 수 있는 기능을 제공합니다.
	/// <see cref="Show"/>
	/// </summary>
	public partial class PropertyForm : Form
	{
		private static PropertyForm _form;

		private PropertyForm()
		{
			InitializeComponent();
		}

		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.PropertyGrid = new System.Windows.Forms.PropertyGrid();
			this.SuspendLayout();
			// 
			// propertyGrid
			// 
			this.PropertyGrid.Dock = System.Windows.Forms.DockStyle.Fill;
			this.PropertyGrid.Location = new System.Drawing.Point(0, 0);
			this.PropertyGrid.Name = "propertyGrid";
			this.PropertyGrid.Size = new System.Drawing.Size(484, 562);
			this.PropertyGrid.TabIndex = 0;
			// 
			// PropertyForm
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 14F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(484, 562);
			this.Controls.Add(this.PropertyGrid);
			this.Font = new System.Drawing.Font("Tahoma", 9F);
			this.Name = "PropertyForm";
			this.ShowInTaskbar = false;
			this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
			this.Text = "PropertyForm";
			this.TopMost = true;
			this.ResumeLayout(false);

		}

		#endregion

		/// <summary>
		/// 컨트롤을 바인딩 할 PropertyGrid 입니다.
		/// </summary>
		public System.Windows.Forms.PropertyGrid PropertyGrid;

		/// <summary>
		/// <see cref="E:System.Windows.Forms.Form.Load" /> 이벤트를 발생시킵니다.
		/// </summary>
		/// <param name="e">이벤트 데이터가 들어 있는 <see cref="T:System.EventArgs" />입니다.</param>
		protected override void OnLoad(EventArgs e)
		{
			base.OnLoad(e);
			var wa = Screen.PrimaryScreen.WorkingArea;

			Location = new Point(wa.Right - Size.Width, 0);
			Size = new Size(Size.Width, wa.Bottom - wa.Top);
		}

		/// <summary>
		/// 지정된 컨트롤의 속성을 보여주는 창을 엽니다.
		/// </summary>
		/// <param name="selectedObject">표로 속성을 표시하는 개체</param>
		public static void Show(object selectedObject)
		{
			if (_form == null)
				_form = new PropertyForm();

			_form.PropertyGrid.SelectedObject = selectedObject;
			_form.Show();
		}
	}
}
