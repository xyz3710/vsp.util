using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using ICSharpCode.SharpZipLib.Core;
using ICSharpCode.SharpZipLib.Zip;
using ICSharpCode.SharpZipLib.Zip.Compression;

namespace X10.Common.Utility
{
	public static class SharpZipHelperExtensions
	{
		/// <summary>
		/// Rename content rename in zip file.
		/// </summary>
		/// <param name="zipFile">The zip file.</param>
		/// <param name="fileName">The filename to rename</param>
		/// <param name="beforeNames">The before names.</param>
		/// <param name="afterNames">The after names.</param>
		/// <returns>True if the entry was found and renamed; false otherwise.</returns>
		/// <exception cref="System.ArgumentNullException">fileName</exception>
		/// <exception cref="System.ArgumentException">beforeNames, afterNames parameter length was not matched.</exception>
		/// <exception cref="ICSharpCode.SharpZipLib.Zip.ZipException"></exception>
		public static bool Rename(
			this ZipFile zipFile,
			string fileName,
			string[] beforeNames,
			string[] afterNames)
		{
			if (fileName == null)
			{
				throw new ArgumentNullException("fileName");
			}

			if (beforeNames.Length != afterNames.Length)
			{
				throw new ArgumentException("beforeNames, afterNames parameter length was not matched.");
			}

			bool result = false;
			zipFile.BeginUpdate();

			foreach (ZipEntry entry in zipFile)
			{
				//ZipUpdate renamedZipUpdate = new ZipUpdate(entry);
				//renamedZipUpdate.Entry.Name = afterName;
				//renamedZipUpdate.OutEntry.Name = afterName;
			}

			/*
			for (int bCount = 0; bCount < beforeNames.Length; bCount++)
			{
				string beforeName = beforeNames[bCount].Replace(@"\", "/");
				string afterName = afterNames[bCount].Replace(@"\", "/");

				if (updateIndex_.ContainsKey(beforeName) == true)
				{
					ArrayList removedIndex = new ArrayList(beforeName.Length);
					Hashtable renamedZip = new Hashtable(beforeName.Length);

					foreach (string key in updateIndex_.Keys)
					{
						object updateIndex = updateIndex_[key];

						if (key == beforeName)
						{
							ZipUpdate renamedZipUpdate = updates_[(int)updateIndex] as ZipUpdate;
							renamedZipUpdate.Entry.Name = afterName;
							renamedZipUpdate.OutEntry.Name = afterName;

							removedIndex.Add(key);
							renamedZip.Add(afterName, updateIndex);
						}
					}

					if (removedIndex.Count > 0)
					{
						for (int i = 0; i < removedIndex.Count; i++)
							updateIndex_.Remove(removedIndex[i]);

						foreach (object key in renamedZip.Keys)
							updateIndex_.Add(key, renamedZip[key]);

						removedIndex.Clear();
						removedIndex = null;
						renamedZip.Clear();
						renamedZip = null;
					}

					//int index = FindExistingUpdate(afterNames[bCount]);

					//if ((index >= 0) && (updates_[index] != null))
					//{
					//	result |= true;
					//	contentsEdited_ |= true;
					//}
					//else
					//	throw new ZipException(string.Format("Cannot find entry to rename: {0}", beforeNames[bCount]));
				}
			}
			*/

			return result;
		}

		/// <summary>
		/// Sort content name in zip file.
		/// </summary>
		/// <param name="fileName">The filename to rename</param>
		/// <param name="sortDirectoryFirst"></param>
		public static void SortFilenames(this ZipFile zipFile, string fileName, bool sortDirectoryFirst = true)
		{
			/*
			if (fileName == null)
				throw new ArgumentNullException("fileName");

			CheckUpdating();

			SortedList sortedList = new SortedList();
			SortedList sortedFolderList = new SortedList();

			foreach (string key in updateIndex_.Keys)
			{
				object updateIndex = updateIndex_[key];
				ZipUpdate zipUpdate = updates_[(int)updateIndex] as ZipUpdate;

				if (key.Contains("/") == true)
					sortedFolderList.Add(key, zipUpdate);
				else
					sortedList.Add(key, zipUpdate);
			}

			updates_.Clear();

			if (sortDirectoryFirst == true)
			{
				updates_ = new ArrayList(sortedFolderList.Values);
				updates_.AddRange(sortedList.Values);
			}
			else
			{
				updates_ = new ArrayList(sortedList.Values);
				updates_.AddRange(sortedFolderList.Values);
			}

			contentsEdited_ = true;
			*/
		}
	}

	class ZipUpdate
	{
		#region Constructors
		public ZipUpdate(string fileName, ZipEntry entry)
		{
			command_ = UpdateCommand.Add;
			entry_ = entry;
			filename_ = fileName;
		}

		public ZipUpdate(IStaticDataSource dataSource, ZipEntry entry)
		{
			command_ = UpdateCommand.Add;
			entry_ = entry;
			dataSource_ = dataSource;
		}

		public ZipUpdate(ZipEntry original, ZipEntry updated)
		{
			throw new ZipException("Modify not currently supported");
			/*
				command_ = UpdateCommand.Modify;
				entry_ = ( ZipEntry )original.Clone();
				outEntry_ = ( ZipEntry )updated.Clone();
			*/
		}

		public ZipUpdate(UpdateCommand command, ZipEntry entry)
		{
			command_ = command;
			entry_ = (ZipEntry)entry.Clone();
		}


		/// <summary>
		/// Copy an existing entry.
		/// </summary>
		/// <param name="entry">The existing entry to copy.</param>
		public ZipUpdate(ZipEntry entry)
			: this(UpdateCommand.Copy, entry)
		{
			// Do nothing.
		}
		#endregion

		/// <summary>
		/// Get the <see cref="ZipEntry"/> for this update.
		/// </summary>
		/// <remarks>This is the source or original entry.</remarks>
		public ZipEntry Entry
		{
			get
			{
				return entry_;
			}
		}

		/// <summary>
		/// Get the <see cref="ZipEntry"/> that will be written to the updated/new file.
		/// </summary>
		public ZipEntry OutEntry
		{
			get
			{
				if (outEntry_ == null)
				{
					outEntry_ = (ZipEntry)entry_.Clone();
				}

				return outEntry_;
			}
		}

		/// <summary>
		/// Get the command for this update.
		/// </summary>
		public UpdateCommand Command
		{
			get
			{
				return command_;
			}
		}

		/// <summary>
		/// Get the filename if any for this update.  Null if none exists.
		/// </summary>
		public string Filename
		{
			get
			{
				return filename_;
			}
		}

		/// <summary>
		/// Get/set the location of the size patch for this update.
		/// </summary>
		public long SizePatchOffset
		{
			get
			{
				return sizePatchOffset_;
			}
			set
			{
				sizePatchOffset_ = value;
			}
		}

		/// <summary>
		/// Get /set the location of the crc patch for this update.
		/// </summary>
		public long CrcPatchOffset
		{
			get
			{
				return crcPatchOffset_;
			}
			set
			{
				crcPatchOffset_ = value;
			}
		}

		/// <summary>
		/// Get/set the size calculated by offset.
		/// Specifically, the difference between this and next entry's starting offset.
		/// </summary>
		public long OffsetBasedSize
		{
			get
			{
				return _offsetBasedSize;
			}
			set
			{
				_offsetBasedSize = value;
			}
		}

		public Stream GetSource()
		{
			Stream result = null;
			if (dataSource_ != null)
			{
				result = dataSource_.GetSource();
			}

			return result;
		}

		#region Instance Fields
		ZipEntry entry_;
		ZipEntry outEntry_;
		UpdateCommand command_;
		IStaticDataSource dataSource_;
		string filename_;
		long sizePatchOffset_ = -1;
		long crcPatchOffset_ = -1;
		long _offsetBasedSize = -1;
		#endregion

		public enum UpdateCommand
		{
			Copy,       // Copy original file contents.
			Modify,     // Change encryption, compression, attributes, name, time etc, of an existing file.
			Add,        // Add a new file to the archive.
		}
	}
}

