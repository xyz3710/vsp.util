﻿// ****************************************************************************************************************** //
//	Domain		:	X10.Common.Utility.ProcessUtility
//	Creator		:	KIMKIWON\xyz37(Kim Ki Won)
//	Create		:	Thursday, May 8, 2014 11:02 AM
//	Purpose		:	Process 관련 유용한 기능을 제공합니다.
// ------------------------------------------------------------------------------------------------------------------ //
//	Modifier	:	
//	Update		:	
//	Changes		:	
// ------------------------------------------------------------------------------------------------------------------ //
//	Comment		:	
// ------------------------------------------------------------------------------------------------------------------ //
//	Reviewer	:	
//	Rev. Date	:	
//	Comment		:	
// ------------------------------------------------------------------------------------------------------------------ //
//	<copyright file="ProcessUtility.cs" company="(주)가치소프트">
//		Copyright (c) 2014. (주)가치소프트. All rights reserved.
//	</copyright>
// <summary></summary>
// ****************************************************************************************************************** //

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Threading;

namespace X10.Common.Utility
{
	/// <summary>
	/// Process 관련 유용한 기능을 제공합니다.
	/// </summary>
	public class ProcessUtility
	{
		/// <summary>
		/// Calls the specified jump to process path.
		/// </summary>
		/// <param name="jumpToProcessPath">호출해서 이동할 프로세스의 경로</param>
		/// <param name="afterKillProcess">호출한 뒤 자기 자신을 제거 할지 여부</param>
		/// <param name="delayJumpToMilliseconds">다음 프로세스를 호출할 때까지 대기시간</param>
		public static void Call(string jumpToProcessPath, bool afterKillProcess = true, int delayJumpToMilliseconds = 1500)
		{
			ProcessStartInfo psi = new ProcessStartInfo(jumpToProcessPath)
			{
				WorkingDirectory = Application.StartupPath,
				ErrorDialog = true,
				Arguments = afterKillProcess == true ? Process.GetCurrentProcess().Id.ToString() : string.Empty,
			};

			Process.Start(psi);

			if (afterKillProcess == true)
			{
				Thread.Sleep(delayJumpToMilliseconds);
				Process.GetCurrentProcess().Kill();
			}
		}

		/// <summary>
		/// 해당 processId의 폼을 제거 합니다.
		/// </summary>
		/// <param name="processId">processId 또는 processName</param>
		public static void Kill(string processId = "")
		{
			Process callProcess = null;

			if (processId == string.Empty)
			{
				processId = Process.GetCurrentProcess().Id.ToString();
			}

			try
			{
				callProcess = Process.GetProcessById(Convert.ToInt32(processId));
			}
			catch
			{
				if (Process.GetProcessesByName(processId).Length > 0)
					callProcess = Process.GetProcessesByName(processId)[0];
			}

			if (callProcess != null)
				callProcess.Kill();
		}
	}
}
