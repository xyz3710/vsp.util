﻿/**********************************************************************************************************************/
/*	Domain		:	X10.Common.Utility.MemorySizeTypes
/*	Creator		:	KIM-KIWON\xyz37(김기원)
/*	Create		:	2012년 9월 14일 금요일 오후 1:10
/*	Purpose		:	메모리 크기 종류를 나열합니다.
/*--------------------------------------------------------------------------------------------------------------------*/
/*	Modifier	:	
/*	Update		:	
/*	Changes		:	
/*--------------------------------------------------------------------------------------------------------------------*/
/*	Comment		:	
/*--------------------------------------------------------------------------------------------------------------------*/
/*	Reviewer	:	
/*	Rev. Date	:	
/**********************************************************************************************************************/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace X10.Common.Utility
{
	/// <summary>
	/// 메모리 크기 종류를 나열합니다.
	/// </summary>
	public enum MemorySizeTypes
	{
		/// <summary>
		/// Byte
		/// </summary>
		Byte = 0,
		/// <summary>
		/// Kilo Byte
		/// </summary>
		KiloByte,
		/// <summary>
		/// Mega Byte
		/// </summary>
		MegaByte,
		/// <summary>
		/// Giga Byte
		/// </summary>
		GigaByte,
		/// <summary>
		/// Tera Byte
		/// </summary>
		TeraByte,
		/// <summary>
		/// B
		/// </summary>
		ByteShort = 11,
		/// <summary>
		/// KB
		/// </summary>
		KiloByteShort,
		/// <summary>
		/// MB
		/// </summary>
		MegaByteShort,
		/// <summary>
		/// GB
		/// </summary>
		GigaByteShort,
		/// <summary>
		/// TB
		/// </summary>
		TeraByteShort,
	}
}
