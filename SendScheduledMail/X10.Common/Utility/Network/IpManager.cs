﻿// ****************************************************************************************************************** //
//	Domain		:	X10.Common.Utility.Network.IpManager
//	Creator		:	KIMKIWON\xyz37(Kim Ki Won)
//	Create		:	2014년 4월 4일 금요일 오전 11:48
//	Purpose		:	IP Address를 관리하는 class입니다.
// ------------------------------------------------------------------------------------------------------------------ //
//	Modifier	:	
//	Update		:	
//	Changes		:	
// ------------------------------------------------------------------------------------------------------------------ //
//	Comment		:	
// ------------------------------------------------------------------------------------------------------------------ //
//	Reviewer	:	
//	Rev. Date	:	
//	Comment		:	
// ------------------------------------------------------------------------------------------------------------------ //
//	<copyright file="IpManager.cs" company="(주)가치소프트">
//		Copyright (c) 2014. (주)가치소프트. All rights reserved.
//	</copyright>
// <summary></summary>
// ****************************************************************************************************************** //

using System;
using System.Collections.Generic;
using System.Text;
using System.Net;
using System.Net.Sockets;

namespace X10.Common.Utility.Network
{
	/// <summary>
	/// IP Address를 관리하는 class입니다.
	/// </summary>
	public class IpManager
	{
		/// <summary>
		/// 현재 실행중인 컴퓨터의 Local IP Address를 구합니다.
		/// </summary>
		public static string LocalIp
		{
			get
			{
				// Get local ip address
				IPHostEntry ipEntry = Dns.GetHostEntry(Dns.GetHostName());
				string localIpAddress = string.Empty;

				foreach (IPAddress ipAddr in ipEntry.AddressList)
				{
					byte ipHeader = ipAddr.GetAddressBytes()[0];

					if (ipHeader == 0 || ipHeader == 127 || ipHeader == 169
						|| ipAddr.AddressFamily != AddressFamily.InterNetwork)
						continue;

					localIpAddress = Convert.ToString(ipAddr);

					if (localIpAddress != string.Empty)
						break;
				}

				return localIpAddress;
			}
		}
	}
}
