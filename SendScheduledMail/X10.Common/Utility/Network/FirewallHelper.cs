﻿/**********************************************************************************************************************/
/*	Domain		:	X10.Common.Utility.Network.FirewallHelper
/*	Creator		:	KIM-KIWON\xyz37(김기원)
/*	Create		:	Thursday, December 13, 2012 11:30 AM
/*	Purpose		:	방화벽 기능을 제공합니다.
/*--------------------------------------------------------------------------------------------------------------------*/
/*	Modifier	:	
/*	Update		:	
/*	Changes		:	
/*--------------------------------------------------------------------------------------------------------------------*/
/*	Comment		:	Vista 이하의 방화벽은 프로그램 경로 기반이고, Vista 이상의 Advance 방화벽은 이름 기반
/*--------------------------------------------------------------------------------------------------------------------*/
/*	Reviewer	:	Kim Ki Won
/*	Rev. Date	:	
/**********************************************************************************************************************/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;
using System.Reflection;

namespace X10.Common.Utility.Network
{
	/// <summary>
	/// 방화벽 기능을 제공합니다.
	/// </summary>
	/// <remarks>Vista 이하의 방화벽은 프로그램 경로 기반이고, Vista 이상의 Advance 방화벽은 이름 기반으로 작동합니다.</remarks>
	public class FirewallHelper
	{
		private const int VISTA_MAJOR_VERSION = 6;

		private enum RuleTypes
		{
			/// <summary>
			/// 새로운 룰을 추가 합니다.
			/// </summary>
			Add,
			/// <summary>
			/// 기존 룰에 새로운 값을 설정합니다.
			/// </summary>
			Set,
			/// <summary>
			/// 룰을 삭제합니다.
			/// </summary>
			Delete,
			/// <summary>
			/// 룰을 표시합니다.
			/// </summary>
			Show,
			/// <summary>
			/// 룰을 사용할 수 없게 합니다.
			/// </summary>
			Block,
			/// <summary>
			/// 룰을 사용할 수 있게 합니다.
			/// </summary>
			Allow,
		}

		private static bool UseAdvanceFirewall
		{
			get
			{
				return System.Environment.OSVersion.Version.Major >= VISTA_MAJOR_VERSION;
			}
		}

		private static string GetFirewallCommand(RuleTypes ruleTypes, string name, string fullPath, string comment = "")
		{
			string result = string.Empty;

			switch (ruleTypes)
			{
				case RuleTypes.Add:
					if (UseAdvanceFirewall == true)
						result = string.Format("advfirewall firewall add rule name=\"{0}\" dir=in action=allow program=\"{1}\" description=\"{2}\" enable=yes", name, fullPath, comment);
					else
						result = string.Format("firewall add allowedprogram name=\"{0}\" program=\"{1}\" ENABLE", name, fullPath);

					break;
				case RuleTypes.Set:
					if (UseAdvanceFirewall == true)
						result = string.Format("advfirewall firewall set rule name=\"{0}\" new program=\"{1}\" description=\"{2}\"", name, fullPath, comment);
					else
						result = string.Format("firewall set allowedprogram name=\"{0}\" program=\"{1}\"", name, fullPath);

					break;
				case RuleTypes.Delete:
					if (UseAdvanceFirewall == true)
						result = string.Format("advfirewall firewall delete rule name=\"{0}\"", name);
					else
						result = string.Format("firewall delete allowedprogram program=\"{0}\"", fullPath);	// 프로그램 이름(순서 주의)

					break;
				case RuleTypes.Show:
					if (UseAdvanceFirewall == true)
						result = string.Format("advfirewall firewall show rule name=\"{0}\"", name);
					else
						result = string.Format("firewall show allowedprogram");

					break;
				case RuleTypes.Block:
					if (UseAdvanceFirewall == true)
						result = string.Format("advfirewall firewall set rule name=\"{0}\" new action=block", name);
					else
						result = string.Format("firewall set allowedprogram program=\"{1}\" name=\"{0}\" DISABLE", fullPath, name);	// 프로그램 이름(순서 주의)

					break;
				case RuleTypes.Allow:
					if (UseAdvanceFirewall == true)
						result = "advfirewall firewall set rule name=\"{0}\" new action=allow";
					else
						result = string.Format("firewall set allowedprogram program=\"{1}\" name=\"{0}\" ENABLE", fullPath, name);	// 프로그램 이름(순서 주의)

					break;
			}

			return result;
		}
		#region ShellResult class
		[DebuggerDisplay("ErrorLevel:{ErrorLevel}, ErrorMessage:{ErrorMessage}, Result:{Result}", Name = "ShellResult")]
		private class ShellResult
		{
			public ShellResult()
			{
				ErrorLevel = 0;
				ErrorMessage = string.Empty;
				Result = string.Empty;
			}

			public int ErrorLevel
			{
				get;
				set;
			}

			public string ErrorMessage
			{
				get;
				set;
			}

			public string Result
			{
				get;
				set;
			}
		}
		#endregion

		private static ShellResult InvokeNetShell(string arguments)
		{
			ShellResult result = new ShellResult();
			ProcessStartInfo startInfo = new ProcessStartInfo()
			{
				CreateNoWindow = true,
				FileName = Environment.SystemDirectory + @"\netsh.exe",
				Arguments = arguments,
				UseShellExecute = false,
				RedirectStandardOutput = true,
				RedirectStandardError = true
			};
			Process process = new Process()
			{
				EnableRaisingEvents = false,
				StartInfo = startInfo
			};

			process.Start();

			result.Result = process.StandardOutput.ReadToEnd();
			result.ErrorMessage = process.StandardError.ReadToEnd();

			process.WaitForExit();
			result.ErrorLevel = process.ExitCode;
			process.Close();

			return result;
		}

		private static void CheckName(ref string name, Assembly callingAsm)
		{
			if (string.IsNullOrEmpty(name) == true)
				name = callingAsm.GetAssemblyTitle();
		}

		private static void CheckFullPath(ref string fullPath, Assembly assembly)
		{
			if (string.IsNullOrEmpty(fullPath) == true)
				fullPath = assembly.Location;
		}

		private static void CheckComment(ref string comments, Assembly assembly)
		{
			if (string.IsNullOrEmpty(comments) == true)
				comments = assembly.GetAssemblyDescription();
		}

		/// <summary>
		/// 방화벽에 프로그램을 추가합니다.
		/// </summary>
		/// <param name="name">추가될 이름(지정하지 않으면 어셈블리 이름)</param>
		/// <param name="fullPath">프로그램 전체 경로(지정하지 않으면 현재 어셈블리 경로)</param>
		/// <param name="comment">추가할 설명</param>
		/// <param name="inBoundDir">InBound에 추가하려면 true, OutBound에 추가하려면 false</param>
		/// <returns>추가되면 <c>true</c>, 그렇지 않으면 <c>false</c>를 반환합니다.</returns>
		public static bool AddRule(string name = "", string fullPath = "", string comment = "", bool inBoundDir = true)
		{
			Assembly callingAsm = Assembly.GetCallingAssembly();

			CheckName(ref name, callingAsm);
			CheckFullPath(ref fullPath, callingAsm);
			CheckComment(ref comment, callingAsm);

			try
			{
				string command = GetFirewallCommand(RuleTypes.Add, name, fullPath, comment);

				if (inBoundDir == false)
					command = command.Replace(" dir=in ", " dir=out ");

				var result = InvokeNetShell(command);

				return result.ErrorLevel == 0;
			}
			catch
			{
				return false;
			}
		}

		/// <summary>
		/// 방화벽에 프로그램을 수정합니다.
		/// </summary>
		/// <param name="name">추가될 이름(지정하지 않으면 어셈블리 이름)</param>
		/// <param name="fullPath">프로그램 전체 경로(지정하지 않으면 현재 어셈블리 경로)</param>
		/// <param name="comment">추가할 설명</param>
		/// <returns>추가되면 <c>true</c>, 그렇지 않으면 <c>false</c>를 반환합니다.</returns>
		public static bool UpdateRule(string name = "", string fullPath = "", string comment = "")
		{
			Assembly callingAsm = Assembly.GetCallingAssembly();

			CheckName(ref name, callingAsm);
			CheckFullPath(ref fullPath, callingAsm);
			CheckComment(ref comment, callingAsm);

			try
			{
				string command = GetFirewallCommand(RuleTypes.Set, name, fullPath, comment);
				var result = InvokeNetShell(command);

				return result.ErrorLevel == 0;
			}
			catch
			{
				return false;
			}
		}

		/// <summary>
		/// 방화벽에 있는 프로그램 룰을 삭제합니다.
		/// </summary>
		/// <param name="name">검사될 이름(지정하지 않으면 어셈블리 이름)</param>
		/// <param name="fullPath">프로그램 전체 경로(지정하지 않으면 현재 어셈블리 경로)</param>
		/// <returns>추가되면 <c>true</c>, 그렇지 않으면 <c>false</c>를 반환합니다.</returns>
		public static bool DeleteRule(string name = "", string fullPath = "")
		{
			Assembly callingAsm = Assembly.GetCallingAssembly();

			CheckName(ref name, callingAsm);
			CheckFullPath(ref fullPath, callingAsm);

			try
			{
				string command = GetFirewallCommand(RuleTypes.Delete, name, fullPath);
				var result = InvokeNetShell(command);

				return result.ErrorLevel == 0;
			}
			catch
			{
				return false;
			}
		}

		/// <summary>
		/// 방화벽에 프로그램 룰이 있는지 검사합니다.
		/// </summary>
		/// <param name="name">검사될 이름(지정하지 않으면 어셈블리 이름)</param>
		/// <param name="fullPath">프로그램 전체 경로(지정하지 않으면 현재 어셈블리 경로)</param>
		/// <returns>추가되면 <c>true</c>, 그렇지 않으면 <c>false</c>를 반환합니다.</returns>
		public static bool ExistRule(string name = "", string fullPath = "")
		{
			Assembly callingAsm = Assembly.GetCallingAssembly();

			CheckName(ref name, callingAsm);
			CheckFullPath(ref fullPath, callingAsm);

			try
			{
				string command = GetFirewallCommand(RuleTypes.Show, name, fullPath);
				var result = new ShellResult();

				if (UseAdvanceFirewall == true)
					result = InvokeNetShell(command);
				else
				{
					string key = string.Format("{0} / ", name);

					result = InvokeNetShell(command);

					return result.Result.IndexOf(key) > 0;
				}

				return result.ErrorLevel == 0;
			}
			catch
			{
				return false;
			}
		}

		/// <summary>
		/// 방화벽에 프로그램 룰을 사용할 수 있도록 합니다.
		/// </summary>
		/// <param name="name">검사될 이름(지정하지 않으면 어셈블리 이름)</param>
		/// <param name="fullPath">프로그램 전체 경로(지정하지 않으면 현재 어셈블리 경로)</param>
		/// <returns>추가되면 <c>true</c>, 그렇지 않으면 <c>false</c>를 반환합니다.</returns>
		public static bool AllowRule(string name = "", string fullPath = "")
		{
			Assembly callingAsm = Assembly.GetCallingAssembly();

			CheckName(ref name, callingAsm);
			CheckFullPath(ref fullPath, callingAsm);

			try
			{
				string command = GetFirewallCommand(RuleTypes.Allow, name, fullPath);
				var result = InvokeNetShell(command);

				return result.ErrorLevel == 0;
			}
			catch
			{
				return false;
			}
		}

		/// <summary>
		/// 방화벽에 프로그램 룰을 사용할 수 없도록 합니다.
		/// </summary>
		/// <param name="name">검사될 이름(지정하지 않으면 어셈블리 이름)</param>
		/// <param name="fullPath">프로그램 전체 경로(지정하지 않으면 현재 어셈블리 경로)</param>
		/// <returns>추가되면 <c>true</c>, 그렇지 않으면 <c>false</c>를 반환합니다.</returns>
		public static bool BlockRule(string name = "", string fullPath = "")
		{
			Assembly callingAsm = Assembly.GetCallingAssembly();

			CheckName(ref name, callingAsm);
			CheckFullPath(ref fullPath, callingAsm);

			try
			{
				string command = GetFirewallCommand(RuleTypes.Block, name, fullPath);
				var result = InvokeNetShell(command);

				return result.ErrorLevel == 0;
			}
			catch
			{
				return false;
			}
		}
	}
}
