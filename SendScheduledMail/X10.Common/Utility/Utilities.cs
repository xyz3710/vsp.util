﻿/**********************************************************************************************************************/
/*	Domain		:	X10.Common.Utility.Utilities
/*	Creator		:	KIM-KIWON\xyz37(김기원)
/*	Create		:	2012년 9월 14일 금요일 오후 1:09
/*	Purpose		:	유용한 기능의 유틸리티를 제공합니다.
/*--------------------------------------------------------------------------------------------------------------------*/
/*	Modifier	:	
/*	Update		:	
/*	Changes		:	
/*--------------------------------------------------------------------------------------------------------------------*/
/*	Comment		:	
/*--------------------------------------------------------------------------------------------------------------------*/
/*	Reviewer	:	
/*	Rev. Date	:	
/**********************************************************************************************************************/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;
using System.Windows.Forms;
using System.Management;

namespace X10.Common.Utility
{
	/// <summary>
	/// 유용한 기능의 유틸리티를 제공합니다.
	/// </summary>
	public static class Utilities
	{
		#region Memory 조회 관련
		/// <summary>
		/// 사용된 실제 메모리를 구합니다.
		/// </summary>
		/// <param name="memorySizeType">보고자 하는 메모리 타입, 지정하지 않으면 KiloByteShort</param>
		/// <param name="process">확인하려는 프로세스, 지정하지 않으면 현재 프로세스</param>
		/// <returns></returns>
		public static string GetUsedMemorySize(MemorySizeTypes memorySizeType = MemorySizeTypes.KiloByteShort, Process process = null)
		{
			if (process == null)
				process = Process.GetCurrentProcess();

			Int64 usageMemory = (Int64)process.WorkingSet64;

			return GetMemorySizedTypeString(memorySizeType, usageMemory);
		}

		/// <summary>
		/// 최대로 사용된 실제 메모리를 구합니다.
		/// </summary>
		/// <param name="memorySizeType">보고자 하는 메모리 타입, 지정하지 않으면 KiloByteShort</param>
		/// <param name="process">확인하려는 프로세스, 지정하지 않으면 현재 프로세스</param>
		/// <returns></returns>
		public static string GetPeakUsedMemorySize(MemorySizeTypes memorySizeType = MemorySizeTypes.KiloByteShort, Process process = null)
		{
			if (process == null)
				process = Process.GetCurrentProcess();

			Int64 usageMemory = (Int64)process.PeakWorkingSet64;

			return GetMemorySizedTypeString(memorySizeType, usageMemory);
		}

		/// <summary>
		/// Gets the memory sized type string.
		/// </summary>
		/// <param name="memorySizeType">Type of the memory size.</param>
		/// <param name="usageMemory">The usage memory.</param>
		/// <param name="format">The string format.</param>
		/// <returns>System.String.</returns>
		public static string GetMemorySizedTypeString(MemorySizeTypes memorySizeType, Int64 usageMemory, string format = "{0:#,##0}{1}")
		{
			usageMemory = (Int64)(usageMemory / ((Int64)1 << (10 * (GetMemorySizeIncresemental(memorySizeType)))));
			string memorySizeTypeString = GetMemorySizedUnitString(memorySizeType);

			return string.Format(format, usageMemory, memorySizeTypeString);
		}

		private static int GetMemorySizeIncresemental(MemorySizeTypes memorySizeType)
		{
			if (memorySizeType.ToString().Contains("Short") == true)
			{
				return (int)memorySizeType - 10;
			}

			return (int)memorySizeType + 1;
		}

		/// <summary>
		/// Gets the memory sized type string.
		/// </summary>
		/// <param name="memorySizeType">Type of the memory size.</param>
		/// <returns>System.String.</returns>
		public static string GetMemorySizedUnitString(MemorySizeTypes memorySizeType)
		{
			string memorySizeTypeString = string.Empty;

			switch (memorySizeType)
			{
				case MemorySizeTypes.Byte:
					memorySizeTypeString = "Byte";

					break;
				case MemorySizeTypes.ByteShort:
					memorySizeTypeString = "B";

					break;
				case MemorySizeTypes.KiloByte:
					memorySizeTypeString = "KiloByte";

					break;
				case MemorySizeTypes.KiloByteShort:
					memorySizeTypeString = "KB";

					break;
				case MemorySizeTypes.MegaByte:
					memorySizeTypeString = "MegaByte";

					break;
				case MemorySizeTypes.MegaByteShort:
					memorySizeTypeString = "MB";

					break;
				case MemorySizeTypes.GigaByte:
					memorySizeTypeString = "GigaByte";

					break;
				case MemorySizeTypes.GigaByteShort:
					memorySizeTypeString = "GB";

					break;
				case MemorySizeTypes.TeraByte:
					memorySizeTypeString = "TeraByte";

					break;
				case MemorySizeTypes.TeraByteShort:
					memorySizeTypeString = "TB";

					break;
			}

			return memorySizeTypeString;
		}

		/// <summary>
		/// Text 속성이 있는 컨트롤에 특정 프로세스의 메모리 사용량을 나타냅니다.
		/// <remarks><seealso cref="System.Diagnostics.PerformanceCounter" />의 속성을 사용합니다.</remarks>
		/// </summary>
		/// <param name="textPropertiedControl">Text 속성이 있는 컨트롤</param>
		/// <param name="processId">확인하려는 프로세스 Id, 지정하지 않으면 현재 프로세스</param>
		/// <param name="memorySizeType">보고자 하는 메모리 타입, 지정하지 않으면 KiloByte</param>
		/// <param name="interval">메세지 갱신주기(ms)</param>
		/// <param name="showDetailInfo">자세한 정보를 보여줄 지 여부, false일 경우 PrivateWorkingSet, WorkingSetPeak, PageFileBytes, PageFileBytesPeak 4가지 정보만 보여준다.</param>
		/// <returns>System.Windows.Forms.Timer.</returns>
		public static System.Windows.Forms.Timer ShowMemories(
			dynamic textPropertiedControl,
			int processId = 0,
			MemorySizeTypes memorySizeType = MemorySizeTypes.KiloByteShort,
			int interval = 500,
			bool showDetailInfo = true)
		{

			System.Windows.Forms.Timer timer = new System.Windows.Forms.Timer
			{
				Interval = interval,
			};
			Process process = null;

			if (processId == 0)
			{
				process = Process.GetCurrentProcess();
			}
			else
			{
				process = Process.GetProcessById(processId);
			}

			PerformanceCounter performanceCounter = new PerformanceCounter();
			string processName = process.ProcessName;
			string format = string.Empty;

			if (showDetailInfo == true)
			{
				format = "Mem: {0}, Peak: {1}, Paged: {2}, PeakPaged: {3}, Vir: {4}, PeakVir: {5}, CPUTime: {6}, Id: {7}";
			}
			else
			{
				format = "Mem: {0}, Peak: {1}, Paged: {2}, PeakPaged: {3}, CPUTime: {4}";
			}

			timer.Tick += (sender, ea) =>
			{
				textPropertiedControl.Text = string.Format(format,
					performanceCounter.GetProcessPrivateWorkingSet(memorySizeType, processName),
					performanceCounter.GetProcessWorkingSetPeak(memorySizeType, processName),
					performanceCounter.GetProcessPageFileBytes(memorySizeType, processName),
					performanceCounter.GetProcessPageFileBytesPeak(memorySizeType, processName),
					showDetailInfo == true ? performanceCounter.GetProcessVirtualBytes(memorySizeType, processName) : performanceCounter.GetProcessProcessorTime(processName),
					showDetailInfo == true ? performanceCounter.GetProcessVirtualBytesPeak(memorySizeType, processName) : string.Empty,
					showDetailInfo == true ? performanceCounter.GetProcessProcessorTime(processName) : string.Empty,
					showDetailInfo == true ? process.Id : 0
				);
			};

			return timer;
		}
		#endregion

		/// <summary>
		/// Timer Tick 이벤트를 발생하는 핸들러를 등록하여 간단히 테스트 할 수 있습니다.
		/// </summary>
		/// <param name="action">The action.</param>
		/// <param name="interval">The interval.</param>
		/// <param name="autoStart">if set to <c>true</c> [automatic start].</param>
		/// <returns>System.Windows.Forms.Timer.</returns>
		public static System.Windows.Forms.Timer TimerTickGenerate(Action action, int interval = 100, bool autoStart = true)
		{
			System.Windows.Forms.Timer timer = new System.Windows.Forms.Timer();

			timer.Interval = interval;
			timer.Tick += (senderObject, ea) =>
			{
				action();
			};

			if (autoStart == true)
			{
				timer.Start();
			}

			return timer;
		}

		/// <summary>
		/// 현재 로그인한 사용자가 userName일 경우에 true를 반환 합니다.
		/// </summary>
		/// <param name="userName">Name of the user.</param>
		/// <returns><c>true</c> if XXXX, <c>false</c> otherwise.</returns>
		public static bool ItsMe(string userName = "xyz37")
		{
			return Environment.GetEnvironmentVariable("UserName").Contains(userName);
		}
	}
}
