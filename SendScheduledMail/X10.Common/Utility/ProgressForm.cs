﻿/**********************************************************************************************************************/
/*	Domain		:	X10.Common.Utility.ProgressForm
/*	Creator		:	KIM-KIWON\xyz37(김기원)
/*	Create		:	Monday, October 28, 2013 6:01 PM
/*	Purpose		:	
/*--------------------------------------------------------------------------------------------------------------------*/
/*	Modifier	:	
/*	Update		:	
/*	Changes		:	
/*--------------------------------------------------------------------------------------------------------------------*/
/*	Comment		:	
/*--------------------------------------------------------------------------------------------------------------------*/
/*	Reviewer	:	Kim Ki Won
/*	Rev. Date	:	
/**********************************************************************************************************************/

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using X10.Common;
using System.Diagnostics;
using System.Threading;

namespace X10.Common.Utility
{
	/// <summary>
	/// Class ProgressForm
	/// </summary>
	public partial class ProgressForm : Form
	{
		#region ProgressEnd Event
		/// <summary>
		/// ProgressEnd 이벤트를 처리할 메서드를 나타냅니다.
		/// </summary>
		/// <param name="sender">이벤트 소스입니다.</param>
		/// <param name="e">이벤트 데이터가 들어 있는 EventArgs 입니다.</param>
		public delegate void ProgressEndEventHandler(object sender, EventArgs e);

		/// <summary>
		/// ProgressEnd를 하면 발생합니다.
		/// </summary>
		public event ProgressEndEventHandler ProgressEnd;

		/// <summary>
		/// ProgressEnd 이벤트를 발생시킵니다.
		/// </summary>
		/// <param name="e">이벤트 데이터가 들어 있는 EventArgs 입니다.</param>
		protected void OnProgressEnd(EventArgs e)
		{
			if (ProgressEnd != null)
				ProgressEnd(this, e);
		}
		#endregion

		private const int DEFAULT_MOVING_INTERVAL = 10;

		/// <summary>
		/// Initializes a new instance of the <see cref="ProgressForm"/> class.
		/// </summary>
		public ProgressForm()
		{
			InitializeComponent();

			StartProgress = true;
			InitFormEvents();
		}

		/// <summary>
		/// Initializes a new instance of the ProgressForm class.
		/// </summary>
		/// <param name="owner">이 폼을 소유할 최상위 창을 나타내는 개체</param>
		/// <param name="action">프로그래스바가 움직이는 동안 처리할 동기 액션</param>
		/// <param name="title">타이틀</param>
		/// <param name="message">대기 메세지</param>
		/// <param name="movingInterval">프로그래스바의 이동 간격(ms)</param>
		/// <param name="closeToHide">폼이 종료될 때 Hide 할지 Close 시킬지 여부</param>
		/// <exception cref="System.ArgumentException">action이 null이면 발생합니다.</exception>
		public ProgressForm(
			Form owner,
			Action action,
			string title = "작업 대기 중입니다.",
			string message = "로드 중입니다.",
			int movingInterval = DEFAULT_MOVING_INTERVAL,
			bool closeToHide = false)
			: this()
		{
			SetShowAction(owner, action, title, message, movingInterval, closeToHide);
		}

		private void SetShowAction(
			Form owner,
			Action action,
			string title,
			string message,
			int movingInterval,
			bool closeToHide)
		{
			if (action == null)
				throw new ArgumentException("action");

			if (owner != null)
			{
				Text = owner.Text;
				Icon = owner.Icon;
			}
			else
				Text = title;

			Action = action;
			Title = title;
			Message = message;
			MovingInterval = movingInterval;
			CloseToHide = closeToHide;
		}

		private void InitFormEvents()
		{
			Load += (sender, ee) =>
			{
				SetTitle(Title);
				SetMessage(Message);
			};
			Shown += (sender, ee) =>
			{
				StartProgressBar();
				InnerTask = Task.Factory.StartNew(Action);

				if (CloseToHide == true)
					Hide();
				else
					Close();
			};
			FormClosing += (sender, ee) =>
			{
				while (InnerTask.IsCompleted == false)
				{
					Thread.Sleep(MovingInterval);
					Application.DoEvents();
				}

				OnProgressEnd(new EventArgs());
				StartProgress = false;
			};
		}

		/// <summary>
		/// 프로그래스 바 진행시 처리할 액션을 구하거나 설정합니다.
		/// </summary>
		public Action Action
		{
			get;
			set;
		}

		/// <summary>
		/// Progress를 진행할지 여부를 구하거나 설정합니다.
		/// </summary>
		public bool StartProgress
		{
			get;
			set;
		}

		/// <summary>
		/// 프로그래스 바의 이동 속도를 구하거나 설정합니다.(ms)
		/// </summary>
		/// <value>The moving interval.</value>
		public int MovingInterval
		{
			get;
			set;
		}

		/// <summary>
		/// 폼이 종료될 때 Hide 할지 Close 시킬지 여부를 구하거나 설정합니다.
		/// </summary>
		public bool CloseToHide
		{
			get;
			set;
		}

		/// <summary>
		/// 비동기 작업을 진행할 내부 Task를 구하거나 설정합니다.
		/// </summary>
		private Task InnerTask
		{
			get;
			set;
		}

		/// <summary>
		/// Title를 구하거나 설정합니다.
		/// </summary>
		private string Title
		{
			get;
			set;
		}

		/// <summary>
		/// Message를 구하거나 설정합니다.
		/// </summary>
		private string Message
		{
			get;
			set;
		}

		private int Increase(int value)
		{
			return value + 1;
		}

		private int Decrease(int value)
		{
			return value - 1;
		}

		/// <summary>
		/// Starts the progress bar.
		/// </summary>
		private void StartProgressBar()
		{
			Task.Factory.StartNew(() =>
			{
				int start = Minimum;
				int end = Maximum;
				int index = start;
				Func<int, int> action = Increase;

				while (StartProgress == true)
				{
					if (index == end - 1)
						action = Decrease;
					else if (index == start)
						action = Increase;

					index = action(index);
					SetProgressBar(index);
					Thread.Sleep(MovingInterval);
					Application.DoEvents();
				}
			});
		}

		/// <param name="owner">이 폼을 소유할 최상위 창을 나타내는 개체</param>
		/// <param name="action">프로그래스바가 움직이는 동안 처리할 동기 액션</param>
		/// <param name="title">타이틀</param>
		/// <param name="message">대기 메세지</param>
		/// <param name="movingInterval">프로그래스바의 이동 간격(ms)</param>
		/// <param name="closeToHide">폼이 종료될 때 Hide 할지 Close 시킬지 여부</param>
		/// <exception cref="System.ArgumentException">action이 null이면 발생합니다.</exception>
		public void Show(Form owner,
			Action action,
			string title = "작업 대기 중입니다.",
			string message = "로드 중입니다.",
			int movingInterval = DEFAULT_MOVING_INTERVAL,
			bool closeToHide = false)
		{
			SetShowAction(owner, action, title, message, movingInterval, closeToHide);

			if (owner == null)
				Show();
			else
				Show(owner);
		}

		#region Other Controls
		/// <summary>
		/// 프로그래스 바의 Minimum를 구하거나 설정합니다.
		/// </summary>
		public int Minimum
		{
			get
			{
				return pbWait.Minimum;
			}
			set
			{
				Invoke(new MethodInvoker(() =>
				{
					pbWait.Minimum = value;
					pbWait.Update();
				}));
			}
		}

		/// <summary>
		/// 프로그래스 바의 Maximum를 구하거나 설정합니다.
		/// </summary>
		public int Maximum
		{
			get
			{
				return pbWait.Maximum;
			}
			set
			{
				Invoke(new MethodInvoker(() =>
				{
					pbWait.Maximum = value;
					pbWait.Update();
				}));
			}
		}

		/// <summary>
		/// Sets the title.
		/// </summary>
		/// <param name="title">The title.</param>
		public void SetTitle(string title)
		{
			Invoke(new MethodInvoker(() =>
			{
				lblTitle.Text = title;
				lblTitle.Refresh();
			}));
		}

		/// <summary>
		/// Sets the message.
		/// </summary>
		/// <param name="message">The message.</param>
		public void SetMessage(string message)
		{
			Invoke(new MethodInvoker(() =>
			{
				lblMessage.Text = message;
				lblMessage.Refresh();

				int newLineCount = lblMessage.Text.Split('\n').Length;

				if (newLineCount > 2)
				{
					Height = 132 + (18 * newLineCount);
					Refresh();
				}
			}));
		}

		/// <summary>
		/// 프로그래스 바의 위치를 이동합니다.
		/// </summary>
		/// <param name="value">The value.</param>
		public void SetProgressBar(int value)
		{
			Invoke(new MethodInvoker(() =>
			{
				pbWait.Value = value;
				pbWait.Text = value.ToString();
				pbWait.Update();
			}));
		}
		#endregion
	}
}
