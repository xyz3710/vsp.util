/**********************************************************************************************************************/
/*	Domain		:	X10.Common.Utilitys.SharpZipHelper
/*	Creator		:	KIMKIWON\xyz37(김기원)
/*	Create		:	2011??12??20???�요???�전 10:36
/*	Purpose		:	SharpZip library�??�순?�게 ?�용?????�는 기능???�공?�니??
/*--------------------------------------------------------------------------------------------------------------------*/
/*	Modifier	:	
/*	Update		:	
/*	Changes		:	
/*--------------------------------------------------------------------------------------------------------------------*/
/*	Comment		:	
/*--------------------------------------------------------------------------------------------------------------------*/
/*	Reviewer	:	Kim Ki Won
/*	Rev. Date	:	2011??12??20???�요???�후 4:37
/**********************************************************************************************************************/

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using ICSharpCode.SharpZipLib.Core;
using ICSharpCode.SharpZipLib.Zip;
using ICSharpCode.SharpZipLib.Zip.Compression;

namespace X10.Common.Utility
{
	/// <summary>
	/// SharpZip library�??�순?�게 ?�용?????�는 기능???�공?�니??
	/// </summary>
	public class SharpZipHelper
	{
		private const int DEFAULT_COMPRESSION_LEVEL = Deflater.NO_COMPRESSION;

		#region Fields
		private byte[] _buffer;
		private string _removablePathPrefix;
		private ZipOutputStream _outputStream;
		#endregion

		#region Constructor
		/// <summary>
		/// SharpZipHelper ?�래?�의 ???�스?�스�?초기???�니??
		/// </summary>
		public SharpZipHelper()
		{
			_buffer = null;
			AddEmptyDirectoryEntries = true;
			RelativePathInfo = true;
		}

		/// <summary>
		/// SharpZipHelper ?�래?�의 ???�스?�스�?초기???�니??
		/// </summary>
		public static SharpZipHelper NewInstance
		{
			get
			{
				return new SharpZipHelper();
			}
		}
		#endregion

		#region Properties
		/// <summary>
		/// BufferSize�?구하거나 ?�정?�니??
		/// </summary>
		public int BufferSize
		{
			get;
			set;
		}

		/// <summary>
		/// CompressionLevel�?구하거나 ?�정?�니??
		/// </summary>
		public int CompressionLevel
		{
			get;
			set;
		}

		/// <summary>
		/// CompressionMethod�?구하거나 ?�정?�니??
		/// </summary>
		public CompressionMethod CompressionMethod
		{
			get;
			set;
		}

		/// <summary>
		/// 비어?�는 ?�더�?추�??��? ?��?�?구하거나 ?�정?�니??
		/// </summary>
		public bool AddEmptyDirectoryEntries
		{
			get;
			set;
		}

		/// <summary>
		/// ?��? 경로�??�?�할지 ?��?�?구하거나 ?�정?�니??
		/// </summary>
		public bool RelativePathInfo
		{
			get;
			set;
		}
		#endregion

		#region SetFiles
		/// <summary>
		/// ?�라미터 ?�식???�일??추�? ?�니??
		/// </summary>
		/// <param name="files"></param>
		/// <returns></returns>
		[DebuggerStepThrough()]
		public static string[] SetFiles(params string[] files)
		{
			return files;
		}
		#endregion

		#region Private methods
		/// <summary>
		/// Compress contents of folder
		/// </summary>
		/// <param name="basePath">The folder to compress</param>
		/// <param name="recursiveSearch">If true process recursively</param>
		/// <param name="searchPattern">Pattern to match for files</param>
		/// <returns>Number of entries added</returns>
		private int CompressFolder(string basePath, bool recursiveSearch, string searchPattern)
		{
			int result = 0;
			string[] names = Directory.GetFiles(basePath, searchPattern);

			foreach (string fileName in names)
			{
				AddFile(fileName);
				++result;
			}

			if (names.Length == 0 && AddEmptyDirectoryEntries)
			{
				AddFolder(basePath);
				++result;
			}

			if (recursiveSearch == true)
			{
				names = Directory.GetDirectories(basePath);

				foreach (string folderName in names)
				{
					result += CompressFolder(folderName, true, searchPattern);
				}
			}
			return result;
		}

		private void AddFolder(string folderName)
		{
			folderName = CookZipEntryName(folderName);

			if (folderName.Length == 0 || folderName[folderName.Length - 1] != '/')
			{
				folderName = folderName + '/';
			}

			ZipEntry zipEntry = new ZipEntry(folderName);
			_outputStream.PutNextEntry(zipEntry);
		}

		private bool AddFile(string fileName)
		{
			bool result = false;

			if (File.Exists(fileName) == true)
			{
				string entryName = CookZipEntryName(fileName);

				AddFileSeekableOutput(fileName, entryName);
			}

			return result;
		}

		private string CookZipEntryName(string name)
		{
			return CookZipEntryName(name, _removablePathPrefix, RelativePathInfo);
		}

		private string CookZipEntryName(string name, string stripPrefix, bool relativePath)
		{
#if TEST
					Console.WriteLine("Cooking '{0}' prefix is '{1}'", name, stripPrefix);
#endif
			if (name == null)
				return string.Empty;

			if (stripPrefix != null && stripPrefix.Length > 0 && name.IndexOf(stripPrefix, 0) == 0)
				name = name.Substring(stripPrefix.Length);

			if (Path.IsPathRooted(name) == true)
			{
				// NOTE:
				// for UNC names...  \\machine\share\zoom\beet.txt gives \zoom\beet.txt
				name = name.Substring(Path.GetPathRoot(name).Length);
#if TEST
						Console.WriteLine("Removing root info {0}", name);
#endif
			}

			name = name.Replace(@"\", "/");

			if (relativePath == true)
			{
				if (name.Length > 0 && (name[0] == Path.AltDirectorySeparatorChar || name[0] == Path.DirectorySeparatorChar))
					name = name.Remove(0, 1);
			}
			else
			{
				if (name.Length > 0 && name[0] != Path.AltDirectorySeparatorChar && name[0] != Path.DirectorySeparatorChar)
					name = name.Insert(0, "/");
			}
#if TEST
					Console.WriteLine("Cooked value '{0}'", name);
#endif
			return name;
		}

		private void AddFileSeekableOutput(string file, string entryPath)
		{
			FileInfo fileInfo = new FileInfo(file);
			ZipEntry entry = new ZipEntry(entryPath)
			{
				DateTime = fileInfo.LastWriteTime,
				ExternalFileAttributes = (int)fileInfo.Attributes,
				Size = fileInfo.Length,
				CompressionMethod = CompressionMethod,
			};

			using (FileStream fileStream = File.OpenRead(file))
			{
				_outputStream.PutNextEntry(entry);
				StreamUtils.Copy(fileStream, _outputStream, GetBuffer());
			}
		}

		private int GetCompressionRatio(ZipEntry zipEntry)
		{
			int result = 0;

			if (zipEntry == null)
			{
				return result;
			}

			long packedSize = zipEntry.CompressedSize;
			long unpackedSize = zipEntry.Size;

			if (unpackedSize > 0 && unpackedSize >= packedSize)
			{
				result = (int)Math.Round((1d - ((double)packedSize / (double)unpackedSize)) * 100d);
			}

			return result;
		}

		private string InterpretExternalAttributes(ZipEntry zipEntry)
		{
			if (zipEntry == null)
			{
				return "-------";
			}

			int operatingSystem = zipEntry.HostSystem;
			int attributes = zipEntry.ExternalFileAttributes;
			string result = string.Empty;

			if ((operatingSystem == 0) || (operatingSystem == 10))
			{
				// Directory
				if ((attributes & 0x10) != 0)
				{
					result = result + "D";
				}
				else
				{
					{
						result = result + "-";
					}
				}

				// Volume
				if ((attributes & 0x08) != 0)
				{
					result = result + "V";
				}
				else
				{
					result = result + "-";
				}

				// Read-only
				if ((attributes & 0x01) != 0)
				{
					result = result + "r";
				}
				else
				{
					result = result + "-";
				}

				// Archive
				if ((attributes & 0x20) != 0)
				{
					result = result + "a";
				}
				else
				{
					result = result + "-";
				}

				// System
				if ((attributes & 0x04) != 0)
				{
					result = result + "s";
				}
				else
				{
					result = result + "-";
				}

				// Hidden
				if ((attributes & 0x02) != 0)
				{
					result = result + "h";
				}
				else
				{
					result = result + "-";
				}

				// Device
				if ((attributes & 0x4) != 0)
				{
					result = result + "d";
				}
				else
				{
					result = result + "-";
				}

				// OS is NTFS
				if (operatingSystem == 10)
				{
					// Encrypted
					if ((attributes & 0x4000) != 0)
					{
						result += "E";
					}
					else
					{
						result += "-";
					}

					// Not content indexed
					if ((attributes & 0x2000) != 0)
					{
						result += "n";
					}
					else
					{
						result += "-";
					}

					// Offline
					if ((attributes & 0x1000) != 0)
					{
						result += "O";
					}
					else
					{
						result += "-";
					}

					// Compressed
					if ((attributes & 0x0800) != 0)
					{
						result += "C";
					}
					else
					{
						result += "-";
					}

					// Reparse point
					if ((attributes & 0x0400) != 0)
					{
						result += "R";
					}
					else
					{
						result += "-";
					}

					// Sparse
					if ((attributes & 0x0200) != 0)
					{
						result += "S";
					}
					else
					{
						result += "-";
					}

					// Temporary
					if ((attributes & 0x0100) != 0)
					{
						result += "T";
					}
					else
					{
						result += "-";
					}
				}
			}

			return result;
		}

		private byte[] GetBuffer()
		{
			return _buffer ?? new byte[BufferSize];
		}

		private bool DecompressFile(
			string fileName,
			string targetDir,
			bool overwriteFiles = true,
			bool restoreDateTime = true,
			string password = null,
			int bufferSize = 8192)
		{
			bool result = true;

			using (ZipInputStream inputStream = new ZipInputStream(File.OpenRead(fileName)))
			{
				if (string.IsNullOrWhiteSpace(password) == false)
				{
					inputStream.Password = password;
				}

				ZipEntry theEntry;

				while ((theEntry = inputStream.GetNextEntry()) != null)
				{
					if (theEntry.IsFile == true)
					{
						ExtractFile(inputStream, theEntry, targetDir, overwriteFiles, restoreDateTime, password, bufferSize);
					}
					else if (theEntry.IsDirectory == true)
					{
						ExtractDirectory(inputStream, theEntry, targetDir, overwriteFiles, restoreDateTime, password, bufferSize);
					}
				}
			}

			return result;
		}

		private bool ExtractFile(
			ZipInputStream inputStream,
			ZipEntry theEntry,
			string targetDir,
			bool overwriteFiles = true,
			bool restoreDateTime = true,
			string password = null,
			int bufferSize = 8192)
		{
			// try and sort out the correct place to save this entry
			string entryFileName = string.Empty;

			if (Path.IsPathRooted(theEntry.Name))
			{
				string workName = Path.GetPathRoot(theEntry.Name);

				workName = theEntry.Name.Substring(workName.Length);
				entryFileName = Path.Combine(Path.GetDirectoryName(workName), Path.GetFileName(theEntry.Name));
			}
			else
			{
				entryFileName = theEntry.Name;
			}

			string targetName = Path.Combine(targetDir, entryFileName);
			string fullPath = Path.GetDirectoryName(Path.GetFullPath(targetName));

			// Could be an option or parameter to allow failure or try creation
			if (Directory.Exists(fullPath) == false)
			{
				try
				{
					Directory.CreateDirectory(fullPath);
				}
				catch
				{
					return false;
				}
			}
			else if (overwriteFiles == false)
			{
				return true;
			}

			if (entryFileName.Length > 0)
			{
				using (FileStream streamWriter = File.Create(targetName))
				{
					byte[] data = new byte[bufferSize];
					int size = 0;

					do
					{
						size = inputStream.Read(data, 0, data.Length);
						streamWriter.Write(data, 0, size);
					} while (size > 0);
				}
#if !NETCF
				if (restoreDateTime == true)
				{
					File.SetLastWriteTime(targetName, theEntry.DateTime);
				}
#endif
			}

			return true;
		}

		private void ExtractDirectory(
			ZipInputStream inputStream,
			ZipEntry theEntry,
			string targetDir,
			bool overwriteFiles = true,
			bool restoreDateTime = true,
			string password = null,
			int bufferSize = 8192)
		{
			// For now do nothing.
		}

		private void CheckExtension(string zipFilename)
		{
			if (Path.GetExtension(zipFilename).Length == 0)
			{
				zipFilename = Path.ChangeExtension(zipFilename, ".zip");
			}
		}
		#endregion

		#region Public methods
		/// <summary>
		/// Zip ?�일???�성?�니??
		/// </summary>
		/// <param name="zipFilename"></param>
		/// <param name="archiveFiles"><see cref="SetFiles"/>�??�용?�여 Zip ?�일???�성???�일??추�??�니??</param>
		/// <param name="recursive">?�위 ?�더???�축?�여 Zip ?�일???�성?��? ?��?�??�택?�니??</param>
		/// <param name="bufferSize"></param>
		/// <param name="compressionLevel"></param>
		/// <param name="password"></param>
		/// <returns></returns>
		public int CreateArchive(
			string zipFilename,
			string[] archiveFiles,
			bool recursive = true,
			int compressionLevel = DEFAULT_COMPRESSION_LEVEL,
			string password = null,
			int bufferSize = 8192)
		{
			BufferSize = bufferSize;
			CompressionLevel = compressionLevel;
			UseZip64 useZip64 = UseZip64.Off;
			int totalEntries = 0;

			CheckExtension(zipFilename);

			if (compressionLevel == Deflater.NO_COMPRESSION)
			{
				CompressionMethod = ICSharpCode.SharpZipLib.Zip.CompressionMethod.Stored;
			}

			using (FileStream stream = File.Create(zipFilename))
			{
				using (_outputStream = new ZipOutputStream(stream))
				{
					if (string.IsNullOrWhiteSpace(password) == false)
					{
						_outputStream.Password = password;
					}

					_outputStream.UseZip64 = useZip64;
					_outputStream.SetLevel(compressionLevel);

					foreach (string archiveFile in archiveFiles)
					{
						string fileName = Path.GetFileName(archiveFile);
						string pathName = Path.GetDirectoryName(archiveFile);

						if (pathName == null || pathName.Length == 0)
						{
							pathName = Path.GetFullPath(".");

							if (RelativePathInfo == true)
								_removablePathPrefix = pathName;
						}
						else
						{
							pathName = Path.GetFullPath(pathName);

							// TODO: for paths like ./txt/*.txt the prefix should be fullpath for .
							// for z:txt/*.txt should be fullpath for z:.
							if (RelativePathInfo == true)
								_removablePathPrefix = pathName;
						}

						if (recursive == true || fileName.IndexOf('*') >= 0 || fileName.IndexOf('?') >= 0)
						{
							totalEntries += CompressFolder(pathName, recursive, fileName);
						}
						else
						{
							AddFile(pathName + @"\" + fileName);
							++totalEntries;
						}
					}
				}
			}

			return totalEntries;
		}

		/// <summary>
		/// ?�일???�는 Zip ?�일???�성?�니??
		/// </summary>
		/// <param name="zipFilename"></param>
		/// <param name="compressionLevel"></param>
		/// <param name="password"></param>
		/// <returns></returns>
		public void CreateEmptyArchive(
			string zipFilename,
			int compressionLevel = DEFAULT_COMPRESSION_LEVEL,
			string password = null)
		{
			BufferSize = 8192;
			CompressionLevel = compressionLevel;
			UseZip64 useZip64 = UseZip64.Off;

			CheckExtension(zipFilename);

			if (compressionLevel == Deflater.NO_COMPRESSION)
			{
				CompressionMethod = ICSharpCode.SharpZipLib.Zip.CompressionMethod.Stored;
			}

			using (FileStream stream = File.Create(zipFilename))
			{
				using (_outputStream = new ZipOutputStream(stream))
				{
					if (string.IsNullOrWhiteSpace(password) == false)
					{
						_outputStream.Password = password;
					}

					_outputStream.UseZip64 = useZip64;
					_outputStream.SetLevel(compressionLevel);
				}
			}
		}

		/// <summary>
		/// Zip ?�일???�축 ?�제 ?�니??
		/// </summary>
		/// <param name="zipFilename">?�?�드 카드�??�용?????�습?�다.</param>
		/// <param name="targetOutputDirectory">?�축???�제???�더�?지?�할 ???�습?�다.</param>
		/// <param name="usezipFilenameFolder">targetOutputDirectory�?지?�하지 ?�을 경우 zipFilename???�더?�름?�로 ?�축???�제 ?�니??</param>
		/// <param name="overwriteFiles"></param>
		/// <param name="restoreDateTime"></param>
		/// <param name="password"></param>
		/// <param name="bufferSize"></param>
		public void ExtractFromArchive(
			string zipFilename,
			string targetOutputDirectory = "",
			bool usezipFilenameFolder = true,
			bool overwriteFiles = true,
			bool restoreDateTime = true,
			string password = null,
			int bufferSize = 8192)
		{
			string[] names = null;

			if (string.IsNullOrWhiteSpace(targetOutputDirectory) == true)
			{
				if (usezipFilenameFolder == true)
				{
					targetOutputDirectory = Path.GetFileNameWithoutExtension(zipFilename);
				}
				else
				{
					targetOutputDirectory = @".\";
				}
			}

			if (zipFilename.IndexOf('*') >= 0 || zipFilename.IndexOf('?') >= 0)
			{
				string pathName = Path.GetDirectoryName(zipFilename);

				if (pathName == null || pathName.Length == 0)
					pathName = @".\";

				names = Directory.GetFiles(pathName, Path.GetFileName(zipFilename));
			}
			else
			{
				names = new string[] { zipFilename };
			}

			foreach (string fileName in names)
			{
				if (File.Exists(fileName) == true)
				{
					DecompressFile(fileName, targetOutputDirectory, overwriteFiles, restoreDateTime, password, bufferSize);
				}
			}
		}

		/// <summary>
		/// Zip ?�일??목록??구합?�다.
		/// </summary>
		/// <param name="zipFilename"></param>
		/// <param name="password"></param>
		/// <returns>?�일???�용???�어??null??반환 ?��? ?�습?�다.</returns>
		public List<ZipEntry> ListArchive(string zipFilename, string password = null)
		{
			List<ZipEntry> result = new List<ZipEntry>();
			FileInfo fileInfo = new FileInfo(zipFilename);

			if (fileInfo.Exists == false)
			{
				return result;
			}

			using (ZipFile zipFile = new ZipFile(zipFilename))
			{
				if (string.IsNullOrWhiteSpace(password) == false)
				{
					zipFile.Password = password;
				}

				result = zipFile.Cast<ZipEntry>().ToList();
			}

			return result;
		}

		/// <summary>
		/// 기존??Zip ?�일???�규 ?�일??추�??�니??
		/// </summary>
		/// <param name="zipFilename">The zip filename.</param>
		/// <param name="addFiles">The add files.</param>
		/// <param name="recursive">if set to <c>true</c> [recursive].</param>
		/// <param name="password">기존 Zip ?�일???�호?� ?��?경우 추�??�는 ?�일�?별도 ?�호�??�?�합?�다.</param>
		/// <remarks>기존 ?�일???�을 경우 기본 ?�션?�로 Zip ?�일???�성?�니??</remarks>
		public void AddToArchive(
			string zipFilename,
			string[] addFiles,
			bool recursive = false,
			string password = null)
		{
			CheckExtension(zipFilename);
			zipFilename = Path.GetFullPath(zipFilename);

			if (File.Exists(zipFilename) == false)
			{
				CreateArchive(zipFilename, addFiles, password: password, recursive: recursive);
			}
			else
			{
				using (ZipFile zipFile = new ZipFile(zipFilename))
				{
					zipFile.BeginUpdate();

					if (string.IsNullOrWhiteSpace(password) == false)
					{
						zipFile.Password = password;
					}

					for (int i = 0; i < addFiles.Length; ++i)
					{
						zipFile.Add(addFiles[i]);
					}

					zipFile.CommitUpdate();
				}
			}
		}

		/// <summary>
		/// 기존??Zip ?�일???�규 ?�일???�동?�여 추�??�니??
		/// </summary>
		/// <param name="zipFilename">The zip filename.</param>
		/// <param name="moveFiles">The move files.</param>
		/// <param name="recursive">if set to <c>true</c> [recursive].</param>
		/// <param name="password">기존 Zip ?�일???�호?� ?��?경우 추�??�는 ?�일�?별도 ?�호�??�?�합?�다.</param>
		/// <remarks>기존 ?�일???�을 경우 기본 ?�션?�로 Zip ?�일???�성?�니??</remarks>
		public void MoveToArchive(
			string zipFilename,
			string[] moveFiles,
			bool recursive = false,
			string password = null)
		{
			try
			{
				AddToArchive(zipFilename, moveFiles, recursive, password);

				foreach (string moveFile in moveFiles)
				{
					string fileName = Path.GetFileName(moveFile);
					string pathName = Path.GetFullPath(Path.GetDirectoryName(moveFile));
					string deleteFileFullPath = Path.Combine(pathName, fileName);

					try
					{
						File.Delete(deleteFileFullPath);
					}
					catch
					{
					}

					try
					{
						if (Directory.GetFiles(pathName).Count() == 0)
						{
							Directory.Delete(pathName, recursive);
						}
					}
					catch
					{
					}
				}
			}
			catch (ZipException ex)
			{
				throw ex;
			}
		}

		/// <summary>
		/// 기존??Zip ?�일?�서 지?�된 ?�일????��?�니??
		/// </summary>
		/// <param name="zipFilename"></param>
		/// <param name="deleteFiles"></param>
		/// <param name="password"></param>
		/// <param name="deleteFileIfEmtpy">Zip ?�일???�일?????�상 ?�다�??�당 zip ?�일????��?��? ?��?</param>
		public void DeleteFromArchive(
			string zipFilename,
			string[] deleteFiles,
			string password = null,
			bool deleteFileIfEmtpy = true)
		{
			CheckExtension(zipFilename);
			zipFilename = Path.GetFullPath(zipFilename);

			long zipFileCount = 0L;

			using (ZipFile zipFile = new ZipFile(zipFilename))
			{
				zipFile.BeginUpdate();

				if (string.IsNullOrWhiteSpace(password) == false)
				{
					zipFile.Password = password;
				}

				for (int i = 0; i < deleteFiles.Length; ++i)
				{
					zipFile.Delete(deleteFiles[i]);
				}

				zipFile.CommitUpdate();
				zipFileCount = zipFile.Count;
			}

			if (deleteFileIfEmtpy == true && zipFileCount == 0L)
			{
				try
				{
					File.Delete(zipFilename);
				}
				catch
				{
				}
			}
		}

		/// <summary>
		/// 기존??Zip ?�일??무결?�과 ?�효?�을 검?�합?�다.
		/// </summary>
		/// <param name="zipFilename"></param>
		/// <param name="throwException">?�류 발생??exception??발생 ?�킬지 ?��?�??�정?�니??</param>
		/// <returns></returns>
		public bool TestArchive(string zipFilename, bool throwException = false)
		{
			CheckExtension(zipFilename);
			zipFilename = Path.GetFullPath(zipFilename);

			bool result = false;

			if (File.Exists(zipFilename) == false)
			{
				if (throwException == true)
				{
					throw new InvalidDataException(string.Format("{0} file was not found.", Path.GetFileName(zipFilename)));
				}
				else
				{
					return false;
				}
			}

			try
			{
				using (ZipFile zipFile = new ZipFile(zipFilename))
				{
					result = zipFile.TestArchive(true);
				}
			}
			catch (ZipException)
			{
			}

			return result;
		}

		/// <summary>
		/// Zip ?�일 ?�의 ?�일 ?�름??변경합?�다.
		/// </summary>
		/// <param name="zipFilename"></param>
		/// <param name="beforeNames"></param>
		/// <param name="afterNames"></param>
		/// <param name="sortAfterRename">?�름 변�????�렉?�리 ?�름??먼�? ?�오?�록 ?�렬?�니??</param>
		/// <param name="password"></param>
		/// <returns></returns>
		private bool RenameContents(
			string zipFilename,
			string[] beforeNames,
			string[] afterNames,
			bool sortAfterRename = true,
			string password = null)
		{
			bool result = true;

			CheckExtension(zipFilename);
			zipFilename = Path.GetFullPath(zipFilename);

			using (ZipFile zipFile = new ZipFile(zipFilename))
			{
				zipFile.BeginUpdate();

				if (string.IsNullOrWhiteSpace(password) == false)
				{
					zipFile.Password = password;
				}

				// UNDONE: 
				// by KIMKIWON\xyz37(김기원) in 2014??9??29???�요???�후 12:29
				//result &= zipFile.Rename(zipFilename, beforeNames, afterNames);

				if (result == true)
				{
					if (sortAfterRename == true)
					{
						// UNDONE: 
						// by KIMKIWON\xyz37(김기원) in 2014??9??29???�요???�후 12:29
						//zipFile.SortFilenames(zipFilename, true);
					}

					zipFile.CommitUpdate();
				}

				zipFile.CommitUpdate();
			}

			return result;
		}

		/// <summary>
		/// Zip ?�일 ?�의 ?�일 ?�름 ?�으�??�렬 ?�니??
		/// </summary>
		/// <param name="zipFilename"></param>
		/// <param name="sortDirectoryFirst">?�렉?�리가 ?�함?�었??경우 ?�렉?�리�?먼�? 배치??지 ?��?�?결정?�니??</param>
		/// <param name="password"></param>
		private void SortFilenames(string zipFilename, bool sortDirectoryFirst = true, string password = null)
		{
			CheckExtension(zipFilename);
			zipFilename = Path.GetFullPath(zipFilename);

			using (ZipFile zipFile = new ZipFile(zipFilename))
			{
				zipFile.BeginUpdate();

				if (string.IsNullOrWhiteSpace(password) == false)
				{
					zipFile.Password = password;
				}

				// UNDONE: 
				// by KIMKIWON\xyz37(김기원) in 2014??9??29???�요???�후 12:29
				//zipFile.SortFilenames(zipFilename, sortDirectoryFirst);
				zipFile.CommitUpdate();
			}
		}
		#endregion
	}
}