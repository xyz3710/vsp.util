﻿// ****************************************************************************************************************** //
//	Domain		:	X10.Common.Utility.Log.LogManager
//	Creator		:	KIMKIWON\xyz37(Kim Ki Won)
//	Create		:	2014년 4월 4일 금요일 오전 11:46
//	Purpose		:	Debugging용 Log를 관리하는 class입니다.
// ------------------------------------------------------------------------------------------------------------------ //
//	Modifier	:	
//	Update		:	
//	Changes		:	
// ------------------------------------------------------------------------------------------------------------------ //
//	Comment		:	
// ------------------------------------------------------------------------------------------------------------------ //
//	Reviewer	:	
//	Rev. Date	:	
//	Comment		:	
// ------------------------------------------------------------------------------------------------------------------ //
//	<copyright file="LogManager.cs" company="(주)가치소프트">
//		Copyright (c) 2014. (주)가치소프트. All rights reserved.
//	</copyright>
// <summary></summary>
// ****************************************************************************************************************** //

using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Xml;
using System.Xml.Linq;
using System.Windows.Forms;
using X10.Common.Utility.Network;
using System.Configuration;

namespace X10.Common.Utility.Log
{
	/// <summary>
	/// Debugging용 Log를 관리하는 class입니다.
	/// </summary>
	public partial class LogManager
	{
		#region Use Singleton Pattern
		private static LogManager _newInstance;

		#region Constructor for Single Ton Pattern
		/// <summary>
		/// LogManager 클래스의 Single Ton Pattern을 위한 생성자 입니다.
		/// </summary>
		/// <param name="logPath">The default log path.</param>
		private LogManager(string logPath)
		{
			_newInstance = null;

			if (string.IsNullOrEmpty(logPath) == true)
			{
				LogPath = GetDefaultLogPath();
			}
			else
			{
				LogPath = logPath;
			}
		}
		#endregion

		#region Instance
		/// <summary>
		/// LogManager 클래스의 Single Ton Pattern을 위한 새 인스턴스를 초기화 합니다.
		/// </summary>
		/// <param name="logPath">The default log path.(지정하지 않을 경우 실행 프로그램 경로의 Logs 라는 폴더로 지정 됩니다.</param>
		/// <returns>유일한 LogManager instance</returns>
		public static LogManager RegisterLogPath(string logPath = "")
		{
			if (_newInstance == null)
			{
				if (string.IsNullOrEmpty(logPath) == true)
				{
					logPath = GetDefaultLogPath();
				}

				_newInstance = new LogManager(logPath);
			}
			else
			{
				if (string.IsNullOrEmpty(logPath) == false)
				{
					LogPath = logPath;
				}
			}

			return _newInstance;
		}

		/// <summary>
		/// LogManager 클래스의 Single Ton Pattern을 위한 새 인스턴스를 구합니다.
		/// </summary>
		/// <returns>유일한 LogManager instance</returns>
		public static LogManager Instance
		{
			get
			{
				if (_newInstance == null)
					_newInstance = new LogManager(LogPath);

				return _newInstance;
			}
		}

		private static string GetDefaultLogPath()
		{
			return Path.Combine(Application.StartupPath, "Logs");
		}

		#endregion
		#endregion

		/// <summary>
		/// DefaultLogPath를 구하거나 설정합니다.
		/// </summary>
		/// <value>DefaultLogPath를 반환합니다.</value>
		public static string LogPath
		{
			get;
			set;
		}

		#region Write with string message
		/// <summary>
		/// Debugging용 로그를 지정된  경로로 write 합니다.
		/// <remarks>파일 형식 : &lt;LogTypeName Folder&gt;\yyyyMMdd_HHmmss_LocalIp_additionalFilename</remarks>
		/// </summary>
		/// <param name="message">로그에 저장되는 내용</param>
		/// <returns></returns>
		public bool Write(string message)
		{
			LogTypes logType = LogTypes.Debug;

			return Write(logType, message);
		}

		/// <summary>
		/// Debugging용 로그를 지정된 경로로 write 합니다.
		/// <remarks>파일 형식 : &lt;LogTypeName Folder&gt;\yyyyMMdd_HHmmss_LocalIp_additionalFilename</remarks>
		/// </summary>
		/// <param name="logType">로깅 하려는 종류</param>
		/// <param name="message">로그에 저장되는 내용</param>
		/// <returns></returns>
		public bool Write(LogTypes logType, string message)
		{
			string additionalFilename = String.Empty;

			return Write(logType, additionalFilename, message);
		}

		/// <summary>
		/// Debugging용 로그를 지정된 경로로 write 합니다.
		/// <remarks>파일 형식 : &lt;LogTypeName Folder&gt;\yyyyMMdd_HHmmss_LocalIp_additionalFilename</remarks>
		/// </summary>
		/// <param name="logType">로깅 하려는 종류</param>
		/// <param name="additionalFilename">추가적인 file 이름</param>
		/// <param name="message">로그에 저장되는 내용</param>
		/// <returns><c>true</c> if XXXX, <c>false</c> otherwise.</returns>
		public bool Write(LogTypes logType, string additionalFilename, string message)
		{
			DateTime now = DateTime.Now;
			string localIp = IpManager.LocalIp;
			string fileName = string.Format("{0}_{1}_{2}{3}.log",
							   now.ToString("yyyyMMdd"),
							   now.ToString("HHmmss"),
							   localIp,
							   additionalFilename == string.Empty ? string.Empty : "_" + additionalFilename);

			return InternalWriteLog(logType, LogLevels.Debug, fileName, message);
		}

		/// <summary>
		/// Debugging용 로그를 지정된 경로로 저장 합니다.
		/// <remarks>customFileName으로 파일 이름을 지정할 수 있습니다.</remarks>
		/// </summary>
		/// <param name="customFileName">저장하려는 파일 이름</param>
		/// <param name="message">로그에 저장되는 내용</param>
		/// <returns></returns>
		public bool Write(string customFileName, string message)
		{
			return InternalWriteLog(LogTypes.Custom, LogLevels.None, customFileName, message);
		}

		/// <summary>
		/// Debugging용 로그를 지정된 경로로 저장 합니다.
		/// <remarks>customFileName으로 파일 이름을 지정할 수 있습니다.</remarks>
		/// </summary>
		/// <param name="customFileName">저장하려는 파일 이름</param>
		/// <param name="format">복합 형식 문자열입니다</param>
		/// <param name="args">형식 항목을 args에 있는 해당 개체의 문자열 표현으로 바꾼 format의 복사본입니다.</param>
		/// <returns></returns>
		/// <exception cref="System.ArgumentNullException">format 또는 args가 null인 경우</exception>
		/// <exception cref="System.FormatException">format 또는 args가 null인 경우</exception>
		public bool Write(string customFileName, string format, params object[] args)
		{
			if (format == null || args == null)
			{
				throw new ArgumentNullException("format 또는 args가 null인 경우");
			}

			return InternalWriteLog(LogTypes.Custom, LogLevels.None, customFileName, string.Format(format, args));
		}

		/// <summary>
		/// 로그를 지정된 경로로 저장 합니다.
		/// <remarks>customFileName으로 파일 이름을 지정할 수 있습니다.</remarks>
		/// </summary>
		/// <param name="logLevel">로그 수준</param>
		/// <param name="customFileName">저장하려는 파일 이름</param>
		/// <param name="message">로그에 저장되는 내용</param>
		/// <returns></returns>
		public bool Write(LogLevels logLevel, string customFileName, string message)
		{
			return InternalWriteLog(LogTypes.Custom, logLevel, customFileName, message);
		}

		/// <summary>
		/// Debugging용 로그를 지정된 경로로 개행 문자를 마지막에 추가하여 저장 합니다.
		/// <remarks>customFileName으로 파일 이름을 지정할 수 있습니다.</remarks>
		/// </summary>
		/// <param name="customFileName">저장하려는 파일 이름</param>
		/// <param name="message">로그에 저장되는 내용</param>
		/// <returns></returns>
		/// <exception cref="System.ArgumentNullException">format 또는 args가 null인 경우</exception>
		/// <exception cref="System.FormatException">format이 잘못된 경우 또는 형식 항목의 인덱스가 0보다 작거나 args 배열의 길이보다 크거나 같은 경우</exception>
		public bool WriteLine(string customFileName, string message)
		{
			return Write(customFileName, message + Environment.NewLine);
		}

		/// <summary>
		/// Debugging용 로그를 지정된 경로로 개행 문자를 마지막에 추가하여 저장 합니다.
		/// <remarks>customFileName으로 파일 이름을 지정할 수 있습니다.</remarks>
		/// </summary>
		/// <param name="customFileName">저장하려는 파일 이름</param>
		/// <param name="format">복합 형식 문자열입니다</param>
		/// <param name="args">형식 항목을 args에 있는 해당 개체의 문자열 표현으로 바꾼 format의 복사본입니다.</param>
		/// <returns></returns>
		/// <exception cref="System.ArgumentNullException">format 또는 args가 null인 경우</exception>
		/// <exception cref="System.FormatException">format이 잘못된 경우 또는 형식 항목의 인덱스가 0보다 작거나 args 배열의 길이보다 크거나 같은 경우</exception>
		public bool WriteLine(string customFileName, string format, params object[] args)
		{
			return WriteLine(customFileName, string.Format(format, args));
		}

		/// <summary>
		/// 로그를 지정된 경로로 개행 문자를 마지막에 추가하여 저장 합니다.
		/// <remarks>customFileName으로 파일 이름을 지정할 수 있습니다.</remarks>
		/// </summary>
		/// <param name="logLevel">로그 수준</param>
		/// <param name="customFileName">저장하려는 파일 이름</param>
		/// <param name="message">로그에 저장되는 내용</param>
		/// <returns></returns>
		public bool WriteLine(LogLevels logLevel, string customFileName, string message)
		{
			return Write(logLevel, customFileName, message + Environment.NewLine);
		}
		#endregion

		#region Write with Xml message
		/// <summary>
		/// Debugging용 로그를 지정된 경로로 write 합니다.
		/// <remarks>파일 형식 : &lt;LogTypeName Folder&gt;\yyyyMMdd_HHmmss_LocalIp_additionalFilename</remarks>
		/// </summary>
		/// <param name="message">로그에 저장되는 내용</param>
		/// <returns></returns>
		public bool Write(XContainer message)
		{
			LogTypes logType = LogTypes.Debug;

			return Write(logType, message);
		}

		/// <summary>
		/// Debugging용 로그를 지정된 경로로 write 합니다.
		/// <remarks>파일 형식 : &lt;LogTypeName Folder&gt;\yyyyMMdd_HHmmss_LocalIp_additionalFilename</remarks>
		/// </summary>
		/// <param name="logType">로깅 하려는 종류</param>
		/// <param name="message">로그에 저장되는 내용</param>
		/// <returns></returns>
		public bool Write(LogTypes logType, XContainer message)
		{
			string additionalFilename = String.Empty;

			return Write(logType, additionalFilename, message);
		}

		/// <summary>
		/// Debugging용 로그를 지정된 경로로 write 합니다.
		/// <remarks>파일 형식 : &lt;LogTypeName Folder&gt;\yyyyMMdd_HHmmss_LocalIp_additionalFilename</remarks>
		/// </summary>
		/// <param name="logType">로깅 하려는 종류</param>
		/// <param name="additionalFilename">추가적인 file 이름</param>
		/// <param name="message">로그에 저장되는 내용</param>
		/// <returns></returns>
		public bool Write(LogTypes logType, string additionalFilename, XContainer message)
		{
			DateTime now = DateTime.Now;
			string localIp = IpManager.LocalIp;
			string fileName = string.Format("{0}_{1}_{2}{3}.xml",
							   now.ToString("yyyyMMdd"),
							   now.ToString("HHmmss"),
							   localIp,
							   additionalFilename == string.Empty ? string.Empty : "_" + additionalFilename);

			return InternalWriteLog(logType, fileName, message);
		}

		/// <summary>
		/// Debugging용 로그를 지정된 경로로 write 합니다.
		/// <remarks>customFileName으로 파일 이름을 지정할 수 있습니다.</remarks>
		/// </summary>
		/// <param name="customFileName">저장하려는 파일 이름</param>
		/// <param name="message">로그에 저장되는 내용</param>
		/// <returns></returns>
		public bool Write(string customFileName, XContainer message)
		{
			return InternalWriteLog(LogTypes.Custom, customFileName, message);
		}
		#endregion

		#region ClearLogFolder
		/// <summary>
		/// 지정된 LogFolder의 내용을 제거합니다.(전체 내용을 삭제 합니다.)
		/// </summary>
		public void ClearLogFolder()
		{
			LogTypes logType = LogTypes.None;

			ClearLogFolder(logType);
		}

		/// <summary>
		/// 지정된 LogFolder의 내용을 제거합니다.
		/// </summary>
		/// <param name="logType">제거할 <seealso cref="LogTypes"/>을 지정합니다.</param>
		public void ClearLogFolder(LogTypes logType)
		{
			string[] files = null;

			switch (logType)
			{
				case LogTypes.None:
					files = Directory.GetFiles(LogPath, "*.log", SearchOption.AllDirectories);

					break;
				default:
					files = Directory.GetFiles(
								LogPath,
								string.Format(@"{0}\*.log", logType),
								SearchOption.TopDirectoryOnly);
					break;
			}

			if (files != null)
			{
				foreach (string fileName in files)
				{
					try
					{
						File.Delete(fileName);
					}
					catch
					{
						continue;
					}
				}
			}
		}
		#endregion

		#region Private methods
		private string PrepareToWriteLog(LogTypes logType, string fileName)
		{
			string logPath = string.Empty;

			switch (logType)
			{
				case LogTypes.None:
					logPath = LogPath;

					break;
				default:
					logPath = Path.Combine(LogPath, logType.ToString());

					break;
			}

			string filePath = Path.Combine(logPath, fileName);

			if (Directory.Exists(logPath) == false)
				Directory.CreateDirectory(logPath);

			return filePath;
		}

		/// <summary>
		/// Log를 지정된 경로로 저장 합니다.
		/// </summary>
		/// <param name="logType">로깅 하려는 종류</param>
		/// <param name="fileName">저장하려는 파일 이름</param>
		/// <param name="xContainer">로그에 저장되는 <see cref="XDocument"/>, <see cref="XElement"/>와 같은 <see cref="XContainer"/></param>
		/// <returns></returns>
		private bool InternalWriteLog(LogTypes logType, string fileName, XContainer xContainer)
		{
			bool result = true;
			string filePath = PrepareToWriteLog(logType, fileName);

			XmlWriterSettings xws = new XmlWriterSettings()
			{
				Indent = true,
				IndentChars = "\t",
				ConformanceLevel = ConformanceLevel.Document,
				Encoding = Encoding.UTF8,
			};

			using (XmlWriter xw = XmlWriter.Create(filePath, xws))
			{
				try
				{
					xContainer.WriteTo(xw);
				}
				catch
				{
					result = false;
					throw;
				}
			}

			return result;
		}

		/// <summary>
		/// Log를 지정된 경로로 저장 합니다.
		/// </summary>
		/// <param name="logType">로깅 하려는 종류</param>
		/// <param name="logLevel">로깅하려는 수준</param>
		/// <param name="fileName">저장하려는 파일 이름</param>
		/// <param name="message">로그에 저장되는 내용</param>
		/// <returns><c>true</c> if XXXX, <c>false</c> otherwise.</returns>
		private bool InternalWriteLog(LogTypes logType, LogLevels logLevel, string fileName, string message)
		{
			var logLevelConfig = ConfigurationManager.AppSettings["logLevel"];

			if (logLevel != LogLevels.None)
			{
				if (string.IsNullOrEmpty(logLevelConfig) == true)
				{
					return false;
				}

				LogLevels logLevelInConfig = LogLevels.None;

				if (Enum.IsDefined(typeof(LogLevels), logLevelConfig) == true)
				{
					logLevelInConfig = (LogLevels)Enum.Parse(typeof(LogLevels), logLevelConfig);

					if (logLevelInConfig != logLevel && logLevelInConfig != LogLevels.All)
					{
						return false;
					}
				}
			}

			bool result = false;
			string filePath = PrepareToWriteLog(logType, fileName);

			using (StreamWriter streamWriter = new StreamWriter(filePath, true, Encoding.Unicode))
			{
				try
				{
					streamWriter.Write(message);
					streamWriter.Flush();
					streamWriter.Close();
					result = true;
				}
				catch
				{
					result = false;
					throw;
				}
				finally
				{
					if (streamWriter != null)
						streamWriter.Close();
				}
			}

			return result;
		}
		#endregion
	}
}
