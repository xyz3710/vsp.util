﻿// ****************************************************************************************************************** //
//	Domain		:	X10.Common.Utility.Log.LogTypes
//	Creator		:	KIMKIWON\xyz37(Kim Ki Won)
//	Create		:	2014년 4월 4일 금요일 오전 11:46
//	Purpose		:	Log를 남기는 Type을 결정합니다.
// ------------------------------------------------------------------------------------------------------------------ //
//	Modifier	:	
//	Update		:	
//	Changes		:	
// ------------------------------------------------------------------------------------------------------------------ //
//	Comment		:	
// ------------------------------------------------------------------------------------------------------------------ //
//	Reviewer	:	
//	Rev. Date	:	
//	Comment		:	
// ------------------------------------------------------------------------------------------------------------------ //
//	<copyright file="LogTypes.cs" company="(주)가치소프트">
//		Copyright (c) 2014. (주)가치소프트. All rights reserved.
//	</copyright>
// <summary></summary>
// ****************************************************************************************************************** //

using System;
using System.Collections.Generic;
using System.Text;

namespace X10.Common.Utility.Log
{
	/// <summary>
	/// Log를 남기는 Type을 결정합니다.
	/// </summary>
	public enum LogTypes
	{
		/// <summary>
		/// Type을 지정하지 않습니다.
		/// </summary>
		None = 0,
		/// <summary>
		/// Exception
		/// </summary>
		Exception,
		/// <summary>
		/// Updater용 Log
		/// </summary>
		Updater,
		/// <summary>
		/// Debugging용 Log
		/// </summary>
		Debug,
		/// <summary>
		/// 개발자가 변경할 수 있도록 합니다.
		/// </summary>
		Custom,
	}
}
