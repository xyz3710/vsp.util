﻿/**********************************************************************************************************************/
/*	Domain		:	X10.Common.Utility.Log.FileLogger
/*	Creator		:	KIM-KIWON\xyz37(김기원)
/*	Create		:	2012년 11월 14일 수요일 오후 2:23
/*	Purpose		:	파일에 로깅 메세지 처리를 지원합니다.
/*--------------------------------------------------------------------------------------------------------------------*/
/*	Modifier	:	
/*	Update		:	
/*	Changes		:	
/*--------------------------------------------------------------------------------------------------------------------*/
/*	Comment		:	
/*--------------------------------------------------------------------------------------------------------------------*/
/*	Reviewer	:	Kim Ki Won
/*	Rev. Date	:	
/**********************************************************************************************************************/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Runtime.CompilerServices;

namespace X10.Common.Utility.Log
{
	/// <summary>
	/// 파일에 로깅 메세지 처리를 지원합니다.
	/// </summary>
	public class FileLogger
	{
		#region Constructors
		/// <summary>
		/// FileLogger class의 새 인스턴스를 초기화 합니다.
		/// </summary>
		/// <param name="path">로그가 저장될 경로</param>
		/// <param name="fileName">로그가 저장될 파일 이름</param>
		public FileLogger(string path, string fileName = "log.txt")
		{
			LogPath = path;
			FileName = fileName;
		}
		#endregion

		#region Properties
		/// <summary>
		/// Path를 구하거나 설정합니다.
		/// </summary>
		public string LogPath
		{
			get;
			set;
		}

		/// <summary>
		/// FileName를 구하거나 설정합니다.
		/// </summary>
		public string FileName
		{
			get;
			set;
		}

		/// <summary>
		/// FilePath를 구합니다.
		/// </summary>
		public string LogFilePath
		{
			get
			{
				return System.IO.Path.Combine(LogPath, FileName);
			}
		}
		#endregion

		#region Public methods
		/// <summary>
		/// 파일에 로깅을 합니다.(지정된 파일은 append 모드로 열립니다.)
		/// </summary>
		/// <param name="format">로그메세지 항목에 대한 복합 형식 문자열</param>
		/// <param name="logMessages">로그 메세지</param>
		/// <returns></returns>
		public bool Write(string format, params object[] logMessages)
		{
			return Write(string.Format(format, logMessages), LogPath, FileName);
		}

		/// <summary>
		/// 파일에 라인 단위로 로깅을 합니다.(지정된 파일은 append 모드로 열립니다.)
		/// </summary>
		/// <param name="logMessage">로그 메세지</param>
		/// <returns></returns>
		public bool Write(string logMessage)
		{
			return Write(logMessage, LogPath, FileName);
		}

		/// <summary>
		/// 파일에 라인 단위로 로깅을 합니다.(지정된 파일은 append 모드로 열립니다.)
		/// </summary>
		/// <param name="format">로그메세지 항목에 대한 복합 형식 문자열</param>
		/// <param name="logMessages">로그 메세지</param>
		/// <returns></returns>
		public bool WriteLine(string format, params object[] logMessages)
		{
			return WriteLine(string.Format(format, logMessages), LogPath, FileName);
		}

		/// <summary>
		/// 파일에 로깅을 합니다.(지정된 파일은 append 모드로 열립니다.)
		/// </summary>
		/// <param name="logMessage">로그 메세지</param>
		/// <param name="insertDateTime">행의 처음에 일시를 기록 할지 여부(일시 뒤에 탭이 삽입됩니다.)</param>
		/// <param name="insertCallerInfo">호출자 정보를 기록할지 여부(true로 지정할 경우 memberName, filePath, lineNumber는 자동으로 구합니다.</param>
		/// <param name="memberName">Name of the member.</param>
		/// <param name="filePath">The file path.</param>
		/// <param name="lineNumber">The line number.</param>
		/// <returns></returns>
		public bool WriteLine(
			string logMessage,
			bool insertDateTime = false,
			bool insertCallerInfo = true,
			[CallerMemberName] string memberName = null,
			[CallerFilePath] string filePath = null,
			[CallerLineNumber] int lineNumber = 0)
		{
			return WriteLine(logMessage, LogPath, FileName, insertDateTime, insertCallerInfo, memberName, filePath, lineNumber);
		}
		#endregion

		#region Static methods
		/// <summary>
		/// 파일에 로깅을 합니다.(지정된 파일은 append 모드로 열립니다.)
		/// </summary>
		/// <param name="logMessage">로그 메세지</param>
		/// <param name="path">로그가 저장될 경로</param>
		/// <param name="fileName">로그가 저장될 파일 이름</param>
		/// <returns></returns>
		public static bool Write(string logMessage, string path, string fileName = "log.txt")
		{
			bool result = false;
			string filePath = System.IO.Path.Combine(path, fileName);

			if (Directory.Exists(path) == false)
				Directory.CreateDirectory(path);

			using (StreamWriter streamWriter = new StreamWriter(filePath, true, Encoding.Unicode))
			{
				try
				{
					streamWriter.Write(logMessage);
					streamWriter.Flush();
					streamWriter.Close();
					result = true;
				}
				catch
				{
					result = false;
					throw;
				}
				finally
				{
					if (streamWriter != null)
						streamWriter.Close();
				}
			}

			return result;
		}

		/// <summary>
		/// 파일에 라인 단위로 로깅을 합니다.(지정된 파일은 append 모드로 열립니다.)
		/// </summary>
		/// <param name="logMessage">로그 메세지</param>
		/// <param name="path">로그가 저장될 경로</param>
		/// <param name="fileName">로그가 저장될 파일 이름</param>
		/// <param name="insertDateTime">행의 처음에 일시를 기록 할지 여부(일시 뒤에 탭이 삽입됩니다.)</param>
		/// <param name="insertCallerInfo">호출자 정보를 기록할지 여부(true로 지정할 경우 memberName, filePath, lineNumber는 자동으로 구합니다.</param>
		/// <param name="memberName">Name of the member.</param>
		/// <param name="filePath">The file path.</param>
		/// <param name="lineNumber">The line number.</param>
		/// <returns><c>true</c> if XXXX, <c>false</c> otherwise</returns>
		public static bool WriteLine(
			string logMessage,
			string path,
			string fileName = "log.txt",
			bool insertDateTime = false,
			bool insertCallerInfo = true,
			[CallerMemberName] string memberName = null,
			[CallerFilePath] string filePath = null,
			[CallerLineNumber] int lineNumber = 0)
		{
			string callerMsg = string.Format("\r\n\t\t\t[{0} in {1} {2} line]", memberName, filePath, lineNumber);

			return Write(string.Format(
									   "{0}{1}{2}{3}",
									   insertDateTime == true ? DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss:fff") + "\t" : string.Empty,
									   logMessage,
									   insertCallerInfo == true ? callerMsg : string.Empty,
									   Environment.NewLine),
								   path,
								   fileName);
		}
		#endregion
	}
}
