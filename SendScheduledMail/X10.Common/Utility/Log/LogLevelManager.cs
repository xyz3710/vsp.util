// ****************************************************************************************************************** //
//	Domain		:	X10.Common.Utility.Log.LogLevelManager
//	Creator		:	KIMKIWON\xyz37(Kim Ki Won)
//	Create		:	Friday, July 18, 2014 11:08 AM
//	Purpose		:	appSettings의 logLevel 에 따라서 로깅 하는 기능을 제공합니다.
// ------------------------------------------------------------------------------------------------------------------ //
//	Modifier	:	
//	Update		:	
//	Changes		:	
// ------------------------------------------------------------------------------------------------------------------ //
//	Comment		:	현재 실행프로그램 하위의 기본 경로(Logs)에 저장합니다.
// ------------------------------------------------------------------------------------------------------------------ //
//	Reviewer	:	
//	Rev. Date	:	
//	Comment		:	
// ------------------------------------------------------------------------------------------------------------------ //
//	<copyright file="LogLevelManager.cs" company="(주)가치소프트">
//		Copyright (c) 2014. (주)가치소프트. All rights reserved.
//	</copyright>
// <summary></summary>
// ****************************************************************************************************************** //

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace X10.Common.Utility.Log
{
	/// <summary>
	/// appSettings의 logLevel 에 따라서 로깅 하는 기능을 제공합니다.
	/// </summary>
	public class LogLevelManager
	{
		private static LogManager _logManager;

		#region Use Singleton Pattern
		private static LogLevelManager _newInstance;

		#region Constructor for Single Ton Pattern
		/// <summary>
		/// LogLevelManager 클래스의 Single Ton Pattern을 위한 생성자 입니다.
		/// </summary>
		private LogLevelManager()
		{
			_newInstance = null;
			_logManager = LogManager.Instance;
		}
		#endregion

		/// <summary>
		/// LogLevelManager 클래스의 Single Ton Pattern을 위한 새 인스턴스를 구합니다.
		/// </summary>
		/// <returns>유일한 LogLevelManager instance</returns>
		public static LogLevelManager Instance
		{
			get
			{
				if (_newInstance == null)
					_newInstance = new LogLevelManager();

				return _newInstance;
			}
		}
		#endregion

		/// <summary>
		/// customFileName에 Debug 레벨의 로그를 남깁니다.
		/// </summary>
		/// <param name="customFileName">저장하려는 파일 이름</param>
		/// <param name="message">로그에 저장되는 내용</param>
		/// <returns></returns>
		public bool Debug(string customFileName, string message)
		{
			return _logManager.WriteLine(LogLevels.Debug, customFileName, message);
		}

		/// <summary>
		/// customFileName에 Debug 레벨의 로그를 남깁니다.
		/// </summary>
		/// <param name="customFileName">저장하려는 파일 이름</param>
		/// <param name="format">복합 형식 문자열입니다</param>
		/// <param name="args">형식 항목을 args에 있는 해당 개체의 문자열 표현으로 바꾼 format의 복사본입니다.</param>
		/// <returns></returns>
		/// <exception cref="System.ArgumentNullException">format 또는 args가 null인 경우</exception>
		/// <exception cref="System.FormatException">format이 잘못된 경우 또는 형식 항목의 인덱스가 0보다 작거나 args 배열의 길이보다 크거나 같은 경우</exception>
		public bool Debug(string customFileName, string format, params object[] args)
		{
			return _logManager.WriteLine(LogLevels.Debug, customFileName, string.Format(format, args));
		}

		/// <summary>
		/// customFileName에 Info 레벨의 로그를 남깁니다.
		/// </summary>
		/// <param name="customFileName">저장하려는 파일 이름</param>
		/// <param name="message">로그에 저장되는 내용</param>
		/// <returns></returns>
		public bool Info(string customFileName, string message)
		{
			return _logManager.WriteLine(LogLevels.Info, customFileName, message);
		}

		/// <summary>
		/// customFileName에 Info 레벨의 로그를 남깁니다.
		/// </summary>
		/// <param name="customFileName">저장하려는 파일 이름</param>
		/// <param name="format">복합 형식 문자열입니다</param>
		/// <param name="args">형식 항목을 args에 있는 해당 개체의 문자열 표현으로 바꾼 format의 복사본입니다.</param>
		/// <returns></returns>
		/// <exception cref="System.ArgumentNullException">format 또는 args가 null인 경우</exception>
		/// <exception cref="System.FormatException">format이 잘못된 경우 또는 형식 항목의 인덱스가 0보다 작거나 args 배열의 길이보다 크거나 같은 경우</exception>
		public bool Info(string customFileName, string format, params object[] args)
		{
			return _logManager.WriteLine(LogLevels.Info, customFileName, string.Format(format, args));
		}

		/// <summary>
		/// customFileName에 Warn 레벨의 로그를 남깁니다.
		/// </summary>
		/// <param name="customFileName">저장하려는 파일 이름</param>
		/// <param name="message">로그에 저장되는 내용</param>
		/// <returns></returns>
		public bool Warn(string customFileName, string message)
		{
			return _logManager.WriteLine(LogLevels.Warn, customFileName, message);
		}

		/// <summary>
		/// customFileName에 Warn 레벨의 로그를 남깁니다.
		/// </summary>
		/// <param name="customFileName">저장하려는 파일 이름</param>
		/// <param name="format">복합 형식 문자열입니다</param>
		/// <param name="args">형식 항목을 args에 있는 해당 개체의 문자열 표현으로 바꾼 format의 복사본입니다.</param>
		/// <returns></returns>
		/// <exception cref="System.ArgumentNullException">format 또는 args가 null인 경우</exception>
		/// <exception cref="System.FormatException">format이 잘못된 경우 또는 형식 항목의 인덱스가 0보다 작거나 args 배열의 길이보다 크거나 같은 경우</exception>
		public bool Warn(string customFileName, string format, params object[] args)
		{
			return _logManager.WriteLine(LogLevels.Warn, customFileName, string.Format(format, args));
		}

		/// <summary>
		/// customFileName에 Error 레벨의 로그를 남깁니다.
		/// </summary>
		/// <param name="customFileName">저장하려는 파일 이름</param>
		/// <param name="message">로그에 저장되는 내용</param>
		/// <returns></returns>
		public bool Error(string customFileName, string message)
		{
			return _logManager.WriteLine(LogLevels.Error, customFileName, message);
		}

		/// <summary>
		/// customFileName에 Error 레벨의 로그를 남깁니다.
		/// </summary>
		/// <param name="customFileName">저장하려는 파일 이름</param>
		/// <param name="format">복합 형식 문자열입니다</param>
		/// <param name="args">형식 항목을 args에 있는 해당 개체의 문자열 표현으로 바꾼 format의 복사본입니다.</param>
		/// <returns></returns>
		/// <exception cref="System.ArgumentNullException">format 또는 args가 null인 경우</exception>
		/// <exception cref="System.FormatException">format이 잘못된 경우 또는 형식 항목의 인덱스가 0보다 작거나 args 배열의 길이보다 크거나 같은 경우</exception>
		public bool Error(string customFileName, string format, params object[] args)
		{
			return _logManager.WriteLine(LogLevels.Error, customFileName, string.Format(format, args));
		}

		/// <summary>
		/// customFileName에 Fatal 레벨의 로그를 남깁니다.
		/// </summary>
		/// <param name="customFileName">저장하려는 파일 이름</param>
		/// <param name="message">로그에 저장되는 내용</param>
		/// <returns></returns>
		public bool Fatal(string customFileName, string message)
		{
			return _logManager.WriteLine(LogLevels.Fatal, customFileName, message);
		}

		/// <summary>
		/// customFileName에 Fatal 레벨의 로그를 남깁니다.
		/// </summary>
		/// <param name="customFileName">저장하려는 파일 이름</param>
		/// <param name="format">복합 형식 문자열입니다</param>
		/// <param name="args">형식 항목을 args에 있는 해당 개체의 문자열 표현으로 바꾼 format의 복사본입니다.</param>
		/// <returns></returns>
		/// <exception cref="System.ArgumentNullException">format 또는 args가 null인 경우</exception>
		/// <exception cref="System.FormatException">format이 잘못된 경우 또는 형식 항목의 인덱스가 0보다 작거나 args 배열의 길이보다 크거나 같은 경우</exception>
		public bool Fatal(string customFileName, string format, params object[] args)
		{
			return _logManager.WriteLine(LogLevels.Fatal, customFileName, string.Format(format, args));
		}
	}
}
