﻿// ****************************************************************************************************************** //
//	Domain		:	X10.Common.Utility.Log.LogLevel
//	Creator		:	KIMKIWON\xyz37(Kim Ki Won)
//	Create		:	Friday, July 18, 2014 10:31 AM
//	Purpose		:	Logging Level을 나열합니다.
// ------------------------------------------------------------------------------------------------------------------ //
//	Modifier	:	
//	Update		:	
//	Changes		:	
// ------------------------------------------------------------------------------------------------------------------ //
//	Comment		:	TODO: 추후에 Log4Net으로 대체 한다.
// ------------------------------------------------------------------------------------------------------------------ //
//	Reviewer	:	
//	Rev. Date	:	
//	Comment		:	
// ------------------------------------------------------------------------------------------------------------------ //
//	<copyright file="LogLevel.cs" company="(주)가치소프트">
//		Copyright (c) 2014. (주)가치소프트. All rights reserved.
//	</copyright>
// <summary></summary>
// ****************************************************************************************************************** //

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace X10.Common.Utility.Log
{
	/// <summary>
	/// Logging Level을 나열합니다.
	/// </summary>
	public enum LogLevels
	{
		/// <summary>
		/// 로깅 레벨을 지정하지 않습니다.
		/// </summary>
		None = 0,
		/// <summary>
		/// All
		/// </summary>
		All,
		/// <summary>
		/// Debugging용 Log
		/// </summary>
		Debug,
		/// <summary>
		/// The information
		/// </summary>
		Info,
		/// <summary>
		/// The warnning
		/// </summary>
		Warn,
		/// <summary>
		/// The error
		/// </summary>
		Error,
		/// <summary>
		/// The fatal
		/// </summary>
		Fatal,
	}
}
