﻿// ****************************************************************************************************************** //
//	Domain		:	GS.Win.Common.Utility.ShortcutHelper
//	Creator		:	KIMKIWON\xyz37(Kim Ki Won)
//	Create		:	Thursday, May 15, 2014 3:47 PM
//	Purpose		:	바로 가기와 관련된 기능을 제공합니다.
// ------------------------------------------------------------------------------------------------------------------ //
//	Modifier	:	
//	Update		:	
//	Changes		:	
// ------------------------------------------------------------------------------------------------------------------ //
//	Comment		:	
// ------------------------------------------------------------------------------------------------------------------ //
//	Reviewer	:	
//	Rev. Date	:	
//	Comment		:	
// ------------------------------------------------------------------------------------------------------------------ //
//	<copyright file="ShortcutHelper.cs" company="(주)가치소프트">
//		Copyright (c) 2014. (주)가치소프트. All rights reserved.
//	</copyright>
// <summary></summary>
// ****************************************************************************************************************** //

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;

namespace GS.Win.Common.Utility
{
	/// <summary>
	/// 바로 가기와 관련된 기능을 제공합니다.
	/// </summary>
	public static class ShortcutHelper
	{
		#region Constants
		/// <summary>
		/// 기본 바로 가기 확장자입니다.
		/// </summary>
		public const string DEFAULT_SHORTCUT_EXTENSION = ".lnk";

		private const string WSCRIPT_SHELL_NAME = "WScript.Shell";
		#endregion

		/// <summary>
		/// 바로 가기를 생성합니다.
		/// </summary>
		/// <param name="linkFileName">바로 가기 파일 이름(.lnk 확장자를 포함 할 수 있습니다.)</param>
		/// <param name="targetPath">바로 가기 대상</param>
		/// <param name="workingDirectory">시작 위치</param>
		/// <param name="arguments">대상에 전달할 인자값</param>
		/// <param name="hotkey">바로 가기 키</param>
		/// <param name="shortcutWindowStyle">실행 창 형태</param>
		/// <param name="description">바로 가기 설명</param>
		/// <param name="iconNumber">바로 가기 대상에 포함된 아이콘 인덱스(0부터 시작)</param>
		/// <returns>바로 가기 파일 경로를 반환 합니다.</returns>
		/// <exception cref="System.IO.FileNotFoundException">바로가기 대상이 없을 때 발생</exception>
		public static string CreateShortcut(
			string linkFileName,
			string targetPath,
			string workingDirectory = "",
			string arguments = "",
			string hotkey = "",
			ShortcutWindowStyles shortcutWindowStyle = ShortcutWindowStyles.WshNormalFocus,
			string description = "",
			int iconNumber = 0)
		{
			if (linkFileName.Contains(DEFAULT_SHORTCUT_EXTENSION) == false)
			{
				linkFileName = string.Format("{0}{1}", linkFileName, DEFAULT_SHORTCUT_EXTENSION);
			}

			if (File.Exists(targetPath) == false)
			{
				throw new FileNotFoundException(targetPath);
			}

			if (workingDirectory == string.Empty)
			{
				workingDirectory = Path.GetDirectoryName(targetPath);
			}

			string iconLocation = string.Format("{0},{1}", targetPath, iconNumber);

			if (Environment.Version.Major >= 4)
			{
				Type shellType = Type.GetTypeFromProgID(WSCRIPT_SHELL_NAME);
				dynamic shell = Activator.CreateInstance(shellType);
				dynamic shortcut = shell.CreateShortcut(linkFileName);

				shortcut.TargetPath = targetPath;
				shortcut.WorkingDirectory = workingDirectory;
				shortcut.Arguments = arguments;
				shortcut.Hotkey = hotkey;
				shortcut.WindowStyle = shortcutWindowStyle;
				shortcut.Description = description;
				shortcut.IconLocation = iconLocation;

				shortcut.Save();
			}
			else
			{
				Type shellType = Type.GetTypeFromProgID(WSCRIPT_SHELL_NAME);
				object shell = Activator.CreateInstance(shellType);
				object shortcut = shellType.InvokeMethod("CreateShortcut", shell, linkFileName);
				Type shortcutType = shortcut.GetType();

				shortcutType.InvokeSetMember("TargetPath", shortcut, targetPath);
				shortcutType.InvokeSetMember("WorkingDirectory", shortcut, workingDirectory);
				shortcutType.InvokeSetMember("Arguments", shortcut, arguments);
				shortcutType.InvokeSetMember("Hotkey", shortcut, hotkey);
				shortcutType.InvokeSetMember("WindowStyle", shortcut, shortcutWindowStyle);
				shortcutType.InvokeSetMember("Description", shortcut, description);
				shortcutType.InvokeSetMember("IconLocation", shortcut, iconLocation);

				shortcutType.InvokeMethod("Save", shortcut);
			}

			return Path.Combine(workingDirectory, linkFileName);
		}

		private static object InvokeSetMember(this Type type, string methodName, object targetInstance, params object[] arguments)
		{
			return type.InvokeMember(
				methodName,
				BindingFlags.Public | BindingFlags.Instance | BindingFlags.SetProperty,
				null,
				targetInstance,
				arguments);
		}

		private static object InvokeMethod(this Type type, string methodName, object targetInstance, params object[] arguments)
		{
			return type.InvokeMember(
				methodName,
				BindingFlags.Public | BindingFlags.Instance | BindingFlags.InvokeMethod,
				null,
				targetInstance,
				arguments);
		}

		/// <summary>
		/// 바로 가기 창의 종류를 나열합니다.
		/// </summary>
		public enum ShortcutWindowStyles
		{
			/// <summary>
			/// Hide
			/// </summary>
			WshHide = 0,
			/// <summary>
			/// NormalFocus
			/// </summary>
			WshNormalFocus = 1,
			/// <summary>
			/// MinimizedFocus
			/// </summary>
			WshMinimizedFocus = 2,
			/// <summary>
			/// MaximizedFocus
			/// </summary>
			WshMaximizedFocus = 3,
			/// <summary>
			/// NormalNoFocus
			/// </summary>
			WshNormalNoFocus = 4,
			/// <summary>
			/// MinimizedNoFocus
			/// </summary>
			WshMinimizedNoFocus = 6,
		}
	}
}
