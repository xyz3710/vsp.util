﻿/**********************************************************************************************************************/
/*	Domain		:	X10.Common.Utility.VisualStudioHelper
/*	Creator		:	KIM-KIWON2\hsjeon(김기원)
/*	Create		:	2012년 9월 12일 수요일 오전 10:48
/*	Purpose		:	Visual Studio의 추가 기능을 제공합니다.
/*--------------------------------------------------------------------------------------------------------------------*/
/*	Modifier	:	
/*	Update		:	
/*	Changes		:	
/*--------------------------------------------------------------------------------------------------------------------*/
/*	Comment		:	EnvDTE, EnvDTE80, EnvDTE100를 참조 추가해야 합니다.
/*--------------------------------------------------------------------------------------------------------------------*/
/*	Reviewer	:	Kim Ki Won
/*	Rev. Date	:	
/**********************************************************************************************************************/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.InteropServices;

namespace X10.Common.Utility
{
#if DEBUG && false
	/// <summary>
	/// Visual Studio의 추가 기능을 제공합니다.
	/// </summary>
	public class VisualStudioHelper
	{
		/// <summary>
		/// 출력 창을 삭제합니다.(디버그 시에 유용합니다.)
		/// </summary>
		public static void ClearOutput()
		{
			EnvDTE80.DTE2 ide = (EnvDTE80.DTE2)Marshal.GetActiveObject("VisualStudio.DTE.10.0");

			try
			{
				ide.ToolWindows.OutputWindow.ActivePane.Clear();
			}
			catch
			{
			}
		}
	}
#endif
}
