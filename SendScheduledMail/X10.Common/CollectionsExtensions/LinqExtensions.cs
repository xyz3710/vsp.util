﻿/**********************************************************************************************************************/
/*	Domain		:	System.Linq.LinqExtensions
/*	Creator		:	KIMKIWON\xyz37(김기원)
/*	Create		:	2011년 4월 1일 금요일 오후 1:03
/*	Purpose		:	네임스페이스 내에서 사용할 공통 확장 메서드를 제공합니다.
/*--------------------------------------------------------------------------------------------------------------------*/
/*	Modifier	:	
/*	Update		:	
/*	Changes		:	
/*--------------------------------------------------------------------------------------------------------------------*/
/*	Comment		:	
/*--------------------------------------------------------------------------------------------------------------------*/
/*	Reviewer	:	Kim Ki Won
/*	Rev. Date	:	2011년 8월 18일 목요일 오후 10:19
/**********************************************************************************************************************/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace System.Linq
{
	/// <summary>
	/// 네임스페이스 내에서 사용할 공통 확장 메서드를 제공합니다.
	/// </summary>
	public static class LinqExtensions
	{
		/// <summary>
		/// 시퀀스의 유일한 요소를 반환하거나 시퀀스가 비어 있으면 기본 개체를 반환합니다. 
		/// 시퀀스에 요소가 둘 이상 있으면 예외를 throw합니다.
		/// </summary>
		/// <typeparam name="TSource">source 요소의 형식입니다.</typeparam>
		/// <param name="source">단일 요소를 반환할 System.Collections.Generic.IEnumerable&lt;T&gt; 입니다.</param>
		/// <param name="predicate">요소를 조건에 대해 테스트하는 함수입니다.</param>
		/// <returns>입력 시퀀스의 단일 요소이거나, 시퀀스에 요소가 없으면 default(TSource)입니다.</returns>
		/// <exception cref="System.ArgumentNullException">source이 null입니다.</exception>
		/// <exception cref="System.InvalidOperationException">입력 시퀀스에 요소가 둘 이상 있는 경우</exception>
		public static TSource SingleOrNew<TSource>(this IQueryable<TSource> source, Func<TSource, bool> predicate)
			where TSource : new()
		{
			TSource t = new TSource();

			if (source == null)
				return t;

			t = source.SingleOrDefault(predicate);

			return t != null ? t : new TSource();
		}

		/// <summary>
		/// 컬렉션에서 지정된 pageNo의 요소를 itemsPerPage 만큼 구합니다.
		/// </summary>
		/// <typeparam name="TSource">source 요소의 형식입니다.</typeparam>
		/// <param name="source">요소를 반환할 <see cref="System.Collections.Generic.IEnumerable&lt;T&gt;"/>입니다.</param>
		/// <param name="pageNo">값을 구할 페이지 번호입니다.(Index 0 부터 시작합니다.)</param>
		/// <param name="itemsPerPage">페이지당 최대 항목의 개수 입니다.</param>
		/// <returns></returns>
		public static IEnumerable<TSource> PageOf<TSource>(this IEnumerable<TSource> source, int pageNo, int itemsPerPage)
			where TSource : new()
		{
			return source.Skip(pageNo * itemsPerPage).Take(itemsPerPage);
		}
	}
}
