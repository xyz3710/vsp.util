﻿/**********************************************************************************************************************/
/*	Domain		:	System.Collections.Generic.DictionaryExtensions`2
/*	Creator		:	KIMKIWON\xyz37(김기원)
/*	Create		:	2011년 4월 21일 목요일 오후 5:45
/*	Purpose		:	Dictionary에 확장 기능을 지원합니다.
/*--------------------------------------------------------------------------------------------------------------------*/
/*	Modifier	:	
/*	Update		:	
/*	Changes		:	
/*--------------------------------------------------------------------------------------------------------------------*/
/*	Comment		:	
/*--------------------------------------------------------------------------------------------------------------------*/
/*	Reviewer	:	Kim Ki Won
/*	Rev. Date	:	2011년 8월 18일 목요일 오후 10:02
/**********************************************************************************************************************/

using System;
using System.Linq;
using System.Text;

namespace System.Collections.Generic
{
	/// <summary>
	/// Dictionary에 확장 기능을 지원합니다.
	/// </summary>
	public static class DictionaryExtensions
	{
		/// <summary>
		/// 지정한 키가 사전에 있는지 검사 후 없으면 키와 값을 사전에 추가합니다.
		/// </summary>
		/// <param name="collection">추가하려는 컬렉션입니다.</param>
		/// <param name="key">추가할 요소의 키입니다.</param>
		/// <param name="value">추가할 요소의 값입니다.참조 형식에 대해 값은 null이 될 수 있습니다.</param>
		/// <exception cref="System.ArgumentNullException">key이 null입니다.</exception>
		public static void AddIfEmpty<TKey, TValue>(this Dictionary<TKey, TValue> collection, TKey key, TValue value)
		{
			if (collection.ContainsKey(key) == false)
			{
				collection.Add(key, value);
			}
		}

		/// <summary>
		/// 지정한 키가 사전에 있는지 검사 후 있으면 기존 값을 수정 하고, 없으면 키와 값을 사전에 추가합니다.
		/// </summary>
		/// <param name="collection">추가하려는 컬렉션입니다.</param>
		/// <param name="key">추가할 요소의 키입니다.</param>
		/// <param name="value">추가할 요소의 값입니다.참조 형식에 대해 값은 null이 될 수 있습니다.</param>
		/// <exception cref="System.ArgumentNullException">key이 null입니다.</exception>
		public static void AddOrUpdate<TKey, TValue>(this Dictionary<TKey, TValue> collection, TKey key, TValue value)
		{
			if (collection.ContainsKey(key) == false)
			{
				collection.Add(key, value);
			}
			else
			{
				collection[key] = value;
			}
		}

		/// <summary>
		/// 지정한 키가 사전에 있는지 검사 후 있으면 있으면 값을 구하고, 없으면 TValue 타입의 기본값을 구합니다.
		/// </summary>
		/// <param name="collection">추가하려는 컬렉션입니다.</param>
		/// <param name="key">추가할 요소의 키입니다.</param>
		/// <exception cref="System.ArgumentNullException">key이 null입니다.</exception>
		public static TValue GetValue<TKey, TValue>(this Dictionary<TKey, TValue> collection, TKey key)
		{
			if (collection.ContainsKey(key) == true)
			{
				return collection[key];
			}
			else
			{
				return default(TValue);
			}
		}

		/// <summary>
		/// 지정한 키를 변경합니다.
		/// </summary>
		/// <typeparam name="TKey">The type of the t key.</typeparam>
		/// <typeparam name="TValue">The type of the t value.</typeparam>
		/// <param name="dictionary">The dictionary.</param>
		/// <param name="oldKey">The old key.</param>
		/// <param name="newKey">The new key.</param>
		/// <returns><c>true</c> if XXXX, <c>false</c> otherwise.</returns>
		public static bool ChangeKey<TKey, TValue>(this IDictionary<TKey, TValue> dictionary, TKey oldKey, TKey newKey)
		{
			TValue value;

			if (dictionary.TryGetValue(oldKey, out value) == false)
				return false;

			dictionary.Remove(oldKey);
			dictionary[newKey] = value;

			return true;
		}
	}
}
