﻿// ****************************************************************************************************************** //
//	Domain		:	System.Collections.Generic.ItemPool`1
//	Creator		:	KIMKIWON\xyz37(Kim Ki Won)
//	Create		:	Thursday, July 10, 2014 5:09 PM
//	Purpose		:	T 타입의 Pool 기능을 제공합니다.
// ------------------------------------------------------------------------------------------------------------------ //
//	Modifier	:	
//	Update		:	
//	Changes		:	
// ------------------------------------------------------------------------------------------------------------------ //
//	Comment		:	
// ------------------------------------------------------------------------------------------------------------------ //
//	Reviewer	:	
//	Rev. Date	:	
//	Comment		:	
// ------------------------------------------------------------------------------------------------------------------ //
//	<copyright file="ItemPool.cs" company="(주)가치소프트">
//		Copyright (c) 2014. (주)가치소프트. All rights reserved.
//	</copyright>
// <summary></summary>
// ****************************************************************************************************************** //

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Diagnostics;

namespace System.Collections.Generic
{
	/// <summary>
	/// T 타입의 Pool 기능을 제공합니다.
	/// </summary>
	/// <typeparam name="T"></typeparam>
	[DebuggerDisplay("MaxCount:{MaxCount}, Count:{Count}, Capacity:{Capacity}, Available:{Available}", Name = "ItemPool")]
	public class ItemPool<T> : IDisposable
		where T : IDisposable, new()
	{
		#region Constants
		private const int MINIUM_CAPACITY = 1;
		/// <summary>
		/// Capacity가 최소 개수보다 작습니다.
		/// </summary>
		private const string CAPACITY_UNDER_MIN_COUNT = "Capacity가 1 보다 작습니다.";
		/// <summary>
		/// Capacity가 MaxCount 보다 큽니다.
		/// </summary>
		private const string CAPACITY_OVER_MAXCOUNT = "Capacity가 MaxCount 보다 큽니다.";
		/// <summary>
		/// Capacity가 최대치를 초과했습니다.
		/// </summary>
		private const string CAPACITY_EXCESS_MAXCOUNT = "Capacity가 최대치를 초과했습니다.";
		#endregion

		#region ItemChanged Event
		/// <summary>
		/// ItemChanged 이벤트를 처리할 메서드를 나타냅니다.
		/// </summary>
		/// <param name="sender">이벤트 소스입니다.</param>
		/// <param name="e">이벤트 데이터가 들어 있는 ItemChanged EventArgs 입니다.</param>
		public delegate void ItemChangedEventHandler(ItemPool<T> sender, EventArgs e);

		/// <summary>
		/// ItemChanged를 하면 발생합니다.
		/// </summary>
		public event ItemChangedEventHandler ItemChanged;

		/// <summary>
		/// ItemChanged 이벤트를 발생시킵니다.
		/// </summary>
		/// <param name="e">이벤트 데이터가 들어 있는 EventArgs 입니다.</param>
		protected void OnItemChanged(EventArgs e)
		{
			if (ItemChanged != null)
			{
				ItemChanged(this, e);
			}
		}
		#endregion

		#region Use Singleton Pattern
		private static ItemPool<T> _newInstance;

		#region Constructor for Single Ton Pattern
		/// <summary>
		/// AllocatedBitmapPool 클래스의 Single Ton Pattern을 위한 생성자 입니다.
		/// </summary>
		/// <param name="maxCount">AllocatedBitmapPool에서 사용할 수 있는 최대 개수</param>
		/// <param name="capacity">AllocatedBitmapPool에 포함되는 초기 요소수</param>
		private ItemPool(int maxCount, int capacity)
		{
			_newInstance = null;
			Pool = new List<PoolItem<T>>(capacity);
			MaxCount = maxCount;
			IncreaseCapacity(capacity);
		}
		#endregion

		#region Instance
		/// <summary>
		/// AllocatedBitmapPool 클래스의 Single Ton Pattern을 위한 새 인스턴스를 초기화 합니다.
		/// </summary>
		/// <returns>유일한 AllocatedBitmapPool instance</returns>
		public static ItemPool<T> Initialize(int maxCount, int capacity)
		{
			if (_newInstance == null)
				_newInstance = new ItemPool<T>(maxCount, capacity);

			return _newInstance;
		}

		/// <summary>
		/// AllocatedBitmapPool 클래스의 Single Ton Pattern을 위한 새 인스턴스를 구합니다.
		/// </summary>
		/// <returns>유일한 AllocatedBitmapPool instance</returns>
		public static ItemPool<T> Default
		{
			get
			{
				if (_newInstance == null)
					throw new InvalidOperationException("Initialize 되지 않았습니다..");

				return _newInstance;
			}
		}
		#endregion
		#endregion

		/// <summary>
		/// Pool에서 사용할 수 있는 개수를 구합니다.
		/// </summary>
		/// <value>최대 개수를 반환합니다.</value>
		public int MaxCount
		{
			get;
			private set;
		}

		/// <summary>
		/// 현재 사용된 항목의 개수를 구합니다.
		/// </summary>
		/// <value>현재 사용된 항목의 개수를 반환합니다.</value>
		public int Count
		{
			get
			{
				return Pool.Count;
			}
		}

		/// <summary>
		/// 허용 항목의 개수를 구합니다.
		/// </summary>
		/// <value>허용 항목의 개수를 반환합니다.</value>
		public int Capacity
		{
			get;
			private set;
		}

		/// <summary>
		/// 사용 가능한 최대 항목를 구합니다.
		/// </summary>
		/// <remarks>Capacity - Count가 아니고 실제 있는 (ReleaseItem이 호출되지 않은) 객체의 수를 반환 합니다.</remarks>
		/// <value>Available를 반환합니다.</value>
		public int Available
		{
			get
			{
				return Pool.Count(x => x.Available == true);
			}
		}

		private List<PoolItem<T>> Pool
		{
			get;
			set;
		}

		private bool AutoRelease
		{
			get;
			set;
		}

		private int AutoReleaseCount
		{
			get;
			set;
		}

		private bool ForceRelease
		{
			get;
			set;
		}

		private int ForceReleaseCount
		{
			get;
			set;
		}

		/// <summary>
		/// Pool의 허용 요소수를 증가 시킵니다.
		/// </summary>
		/// <param name="capacity">증가 시킬려는 요소수</param>
		/// <returns>추가된 첫번째 항목을 반환합니다.</returns>
		/// <exception cref="System.ArgumentOutOfRangeException">Capacity가 MaxCount 보다 큽니다.</exception>
		/// <exception cref="System.OverflowException">Capacity가 최대치를 초과했습니다.</exception>
		private PoolItem<T> IncreaseCapacity(int capacity)
		{
			if (capacity > MaxCount)
			{
				throw new ArgumentOutOfRangeException(CAPACITY_OVER_MAXCOUNT);
			}

			if ((Capacity + capacity) > MaxCount)
			{
				throw new OverflowException(CAPACITY_EXCESS_MAXCOUNT);
			}

			Capacity += capacity;
			var first = new PoolItem<T>();

			for (int i = 0; i < capacity; i++)
			{
				var item = AddNewItem();

				if (i == 0)
				{
					first = item;
				}
			}

			return first;
		}

		/// <summary>
		/// Pool의 허용 요소수를 감소 시킵니다.
		/// </summary>
		/// <param name="capacity">감소 시킬려는 요소수</param>
		/// <returns>감소된 요소수를 반환합니다.</returns>
		/// <exception cref="System.ArgumentOutOfRangeException">Capacity가 1 보다 작습니다.</exception>
		private int DecreaseCapacity(int capacity)
		{
			if (capacity < MINIUM_CAPACITY)
			{
				throw new ArgumentOutOfRangeException(CAPACITY_UNDER_MIN_COUNT);
			}

			if ((Capacity - capacity) < MINIUM_CAPACITY)
			{
				throw new ArgumentOutOfRangeException(CAPACITY_UNDER_MIN_COUNT);
			}

			Capacity -= capacity;

			return Capacity;
		}

		private PoolItem<T> AddNewItem()
		{
			var item = new PoolItem<T>
			{
				Available = true,
				Item = new T(),
			};

			Pool.Add(item);
			OnItemChanged(EventArgs.Empty);

			return item;
		}

		private PoolItem<T> GetContainedPoolItem(T item)
		{
			return Pool.SingleOrDefault(x => x.Item.Equals(item) == true);
		}

		private PoolItem<T> GetAvailable()
		{
			return Pool.FirstOrDefault(x => x.Available == true);
		}

		/// <summary>
		/// 사용 가능한 항목을 구합니다. 없을 경우 지정된 capacity씩 증가 시킵니다.
		/// </summary>
		/// <param name="autoIncreaseCapacity">The automatic increase capacity.</param>
		/// <returns>사용가능한 T 객체</returns>
		/// <exception cref="System.ArgumentOutOfRangeException">capacity가 MaxCount 보다 큽니다.</exception>
		/// <exception cref="System.InvalidOperationException">Capacity가 최대치를 초과했습니다.</exception>
		public T GetAvailableItem(int autoIncreaseCapacity = 1)
		{
			var availableItem = GetAvailable();

			if (availableItem != null)
			{
				availableItem.Available = false;
				OnItemChanged(EventArgs.Empty);

				return availableItem.Item;
			}
			else
			{
				if (Capacity - Count > 0)
				{
					var newItem = AddNewItem();

					newItem.Available = false;

					return newItem.Item;
				}

				var increasedItem = IncreaseCapacity(autoIncreaseCapacity);

				increasedItem.Available = false;
				OnItemChanged(EventArgs.Empty);

				return increasedItem.Item;
			}
		}

		/// <summary>
		/// 항목을 반환하여 재사용할 수 있도록 합니다.
		/// </summary>
		/// <param name="item">The item.</param>
		/// <returns><c>true</c> if XXXX, <c>false</c> otherwise.</returns>
		public bool ItemReturn(T item)
		{
			var containedPoolItem = GetContainedPoolItem(item);

			if (containedPoolItem != null)
			{
				containedPoolItem.Available = true;
				OnItemChanged(EventArgs.Empty);

				if (AutoRelease == true)
				{
					// 최대 Capacity / 2의 값에서 정해진 AutoReleaseCount 만큼 반복했을 때
					// ForceRelease가 true이면 PoolItem을 하나씩 Dispose 한다.
					ForceRelease = Available >= Math.Floor(Capacity / 2d);

					if (ForceRelease == true)
					{
						if (ForceReleaseCount++ >= AutoReleaseCount && Capacity > Math.Floor(MaxCount / 2d))
						{
							if (ReleaseItem(item) == true)
							{
								if (Capacity > Math.Floor(MaxCount / 2d))
								{
									DecreaseCapacity(1);
								}
								else
								{
									ForceReleaseCount = 1;
									ForceRelease = false;
								}
							}
						}
					}
				}

				return true;
			}
			else
			{
				return false;
			}
		}

		/// <summary>
		/// 사용된 item의 리소스를 해제 합니다.
		/// </summary>
		/// <param name="item">The item.</param>
		/// <returns>성공적으로 해제하면 true를 반환합니다.</returns>
		/// <exception cref="System.ArgumentNullException">item</exception>
		public bool ReleaseItem(T item)
		{
			bool result = false;

			if (item == null)
			{
				throw new ArgumentNullException("item");
			}

			var availableItem = GetContainedPoolItem(item);

			if (availableItem != null)
			{
				Pool.Remove(availableItem);
				availableItem.Item.Dispose();
				GC.Collect();
				result = true;
			}

			return result;
		}

		/// <summary>
		/// 사용된 모든 item의 리소스를 해제 합니다.
		/// </summary>
		public void ReleaseItems()
		{
			Pool.ForEach(x => x.Item.Dispose());
			Pool.Clear();
		}

		/// <summary>
		/// Available &gt;= Math.Floor(Capacity / 2) 의 조건이 계속해서 <seealso cref="AutoReleaseCount"/> 만큼 반복되면 ItemReturn시 해당 자원을 Release 시킵니다.
		/// </summary>
		/// <param name="autoRelease">자동으로 Release 할지 여부</param>
		/// <param name="autoReleaseCount">자동 해제 조건이 반복되는 횟수</param>
		public void SetAutoRelease(bool autoRelease = true, int autoReleaseCount = 10)
		{
			AutoRelease = autoRelease;
			AutoReleaseCount = autoReleaseCount;
			ForceRelease = autoRelease;
			ForceReleaseCount = 1;
		}

		#region IDisposable 멤버

		/// <summary>
		/// 관리되지 않는 리소스의 확보, 해제 또는 다시 설정과 관련된 응용 프로그램 정의 작업을 수행합니다.
		/// </summary>
		public void Dispose()
		{
			Pool.Clear();
			Pool = null;
		}

		#endregion

		#region PoolItem<TItem>
		[DebuggerDisplay("Available:{Available}, Item:{Item}", Name = "PoolItem")]
		private class PoolItem<TItem>
			where TItem : IDisposable
		{
			public bool Available
			{
				get;
				set;
			}

			public TItem Item
			{
				get;
				set;
			}
		}
		#endregion
	}
}
