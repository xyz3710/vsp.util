/**********************************************************************************************************************/
/*	Domain		:	X10.Common.Extensions.AppSettingKeysExtension
/*	Creator		:	X10\xyz37(김기원)
/*	Create		:	2012년 9월 5일 수요일 오전 6:30
/*	Purpose		:	설정 파일에서 AppSetting Key의 값을 읽거나 저장하는 확장 기능을 제공합니다.
/*--------------------------------------------------------------------------------------------------------------------*/
/*	Modifier	:	
/*	Update		:	
/*	Changes		:	
/*--------------------------------------------------------------------------------------------------------------------*/
/*	Comment		:	
/*--------------------------------------------------------------------------------------------------------------------*/
/*	Reviewer	:	Kim Ki Won
/*	Rev. Date	:	
/**********************************************************************************************************************/

using System;
using System.Data;
using System.Linq;
using System.Configuration;

namespace X10.Common.Extensions
{
	/// <summary>
	/// 설정 파일에서 AppSetting Key의 값을 읽거나 저장하는 확장 기능을 제공합니다.
	/// </summary>
	public static class AppSettingKeysExtension
	{
		private static void RefreshAppSettings()
		{
			ConfigurationManager.RefreshSection("appSettings");
		}

		/// <summary>
		/// Enum 타입의 설정값에서 값을 구합니다.
		/// </summary>
		/// <param name="settings">The settings.</param>
		/// <returns></returns>
		public static string GetValue(this Enum settings)
		{
			RefreshAppSettings();

			string value = ConfigurationManager.AppSettings[settings.ToString()];

			return value;
		}

		/// <summary>
		/// Enum 타입을 설정값에서 값을 구합니다.
		/// </summary>
		/// <param name="settings">The settings.</param>
		/// <returns></returns>
		public static T GetValueEnum<T>(this Enum settings)
		{
			if (typeof(T).IsEnum == false)
				throw new ArgumentException("T 타입이 enum이 아닙니다.");

			var value = settings.GetValue();
			T t = default(T);

			if (Enum.IsDefined(typeof(T), value) == true)
				t = (T)Enum.Parse(typeof(T), value);

			return t;
		}

		/// <summary>
		/// 설정값에 값을 저장합니다.
		/// </summary>
		/// <param name="settings">The settings.</param>
		/// <param name="value">The value.</param>
		public static void SetValue(this Enum settings, string value)
		{
			Configuration currentConfig = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
			string key = settings.ToString();
			var valueType = value.GetType();
			KeyValueConfigurationCollection keySettings = currentConfig.AppSettings.Settings;

			// Key 파일이 camelCase 형식으로 되어있어서 대문자로 시작하는 키의 경우에는 값을 계속 추가한다.
			if (keySettings.AllKeys.Contains(key, StringComparer.CurrentCultureIgnoreCase) == true)
			{
				keySettings[key].Value = value;
			}
			else
			{
				keySettings.Add(key, value);
			}

			currentConfig.Save();
			RefreshAppSettings();
		}
	}
}

