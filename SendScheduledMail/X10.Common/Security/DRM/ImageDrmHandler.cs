﻿/**********************************************************************************************************************/
/*	Domain		:	X10.Common.Security.DrmStreamBase
/*	Creator		:	KIMKIWON\xyz37(김기원)
/*	Create		:	2012년 1월 6일 금요일 오후 5:15
/*	Purpose		:	이미지 파일의 DRM을 처리하는 기본 클래스 입니다.
/*--------------------------------------------------------------------------------------------------------------------*/
/*	Modifier	:	
/*	Update		:	
/*	Changes		:	
/*--------------------------------------------------------------------------------------------------------------------*/
/*	Comment		:	
/*--------------------------------------------------------------------------------------------------------------------*/
/*	Reviewer	:	Kim Ki Won
/*	Rev. Date	:	2012년 1월 6일 금요일 오후 5:42
/**********************************************************************************************************************/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace X10.Common.Security
{
	/// <summary>
	/// 이미지 파일의 DRM을 처리하는 기본 클래스 입니다.
	/// </summary>
	public class ImageDrmHandler : DrmHandlerBase
	{
		private const string ENCRYPTION_SEED = "efactory_image_0101enc";
		private const int ENCRYTION_ROTATE_BITS = 4;
		private const int ENCRYPTED_DATA_MULTIPLY = 8;

		#region Use Singleton Pattern
		private static ImageDrmHandler _newInstance;

		#region Constructor for Single Ton Pattern
		/// <summary>
		/// ImageDrmHandler 클래스의 Single Ton Pattern을 위한 생성자 입니다.
		/// </summary>
		/// <param name="bufferSize">DRM을 처리하는데 필요한 buffer 크기 입니다.</param>
		private ImageDrmHandler(int bufferSize = 8196)
			: base(DrmMagicByte.ImageDRM, ENCRYPTION_SEED, ENCRYTION_ROTATE_BITS, ENCRYPTED_DATA_MULTIPLY, bufferSize)
		{
			_newInstance = null;

		}
		#endregion

		#region Instance
		/// <summary>
		/// ImageDrmHandler 클래스의 Single Ton Pattern을 위한 새 인스턴스를 초기화 합니다.
		/// </summary>
		/// <param name="bufferSize">DRM을 처리하는데 필요한 buffer 크기 입니다.</param>
		/// <returns>유일한 ImageDrmHandler instance</returns>
		public static ImageDrmHandler GetInstance(int bufferSize = 8196)
		{
			if (_newInstance == null)
				_newInstance = new ImageDrmHandler();

			return _newInstance;
		}

		/// <summary>
		/// ImageDrmHandler 클래스의 Single Ton Pattern을 위한 새 인스턴스를 구합니다.
		/// </summary>
		/// <returns>bufferSize가 8196 byte인 유일한 ImageDrmHandler instance</returns>
		public static ImageDrmHandler Instance
		{
			get
			{
				if (_newInstance == null)
					_newInstance = new ImageDrmHandler();

				return _newInstance;
			}
		}
		#endregion
		#endregion
	}
}
