﻿/**********************************************************************************************************************/
/*	Domain		:	X10.Common.Security.DrmExceptionCodes
/*	Creator		:	KIMKIWON\xyz37(김기원)
/*	Create		:	2012년 1월 6일 금요일 오후 3:06
/*	Purpose		:	DRM 처리중 나타나는 오류 코드를 나타납니다.
/*--------------------------------------------------------------------------------------------------------------------*/
/*	Modifier	:	
/*	Update		:	
/*	Changes		:	
/*--------------------------------------------------------------------------------------------------------------------*/
/*	Comment		:	
/*--------------------------------------------------------------------------------------------------------------------*/
/*	Reviewer	:	Kim Ki Won
/*	Rev. Date	:	2012년 1월 6일 금요일 오후 5:13
/**********************************************************************************************************************/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace X10.Common.Security
{
	/// <summary>
	/// DRM 처리중 나타나는 오류 코드를 나타납니다.
	/// </summary>
	public enum DrmExceptionCodes
	{
		/// <summary>
		/// Magic byte가 틀립니다.
		/// </summary>
		InvalidMagicByte = 0,
		/// <summary>
		/// DRM이 이미 적용되었습니다.
		/// </summary>
		DrmAlreadyAdapted,
		/// <summary>
		/// Header 정보가 유효하지 않습니다.
		/// </summary>
		InvalidHeader,
		/// <summary>
		/// EncryptionSeed의 길이가 255를 넘습니다.
		/// </summary>
		OverflowEncryptionSeedLength,
	}
}
