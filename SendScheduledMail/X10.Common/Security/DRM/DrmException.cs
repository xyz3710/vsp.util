﻿/**********************************************************************************************************************/
/*	Domain		:	X10.Common.Security.DrmException
/*	Creator		:	KIMKIWON\xyz37(김기원)
/*	Create		:	2012년 1월 6일 금요일 오후 3:03
/*	Purpose		:	DRM 처리중 나타나는 오류를 나타냅니다.
/*--------------------------------------------------------------------------------------------------------------------*/
/*	Modifier	:	
/*	Update		:	
/*	Changes		:	
/*--------------------------------------------------------------------------------------------------------------------*/
/*	Comment		:	
/*--------------------------------------------------------------------------------------------------------------------*/
/*	Reviewer	:	Kim Ki Won
/*	Rev. Date	:	2012년 1월 6일 금요일 오후 3:09
/**********************************************************************************************************************/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace X10.Common.Security
{
	/// <summary>
	/// DRM 처리중 나타나는 오류를 나타냅니다.
	/// </summary>
	public class DrmException : Exception
	{
		#region Constructor
		/// <summary>
		/// DrmException 클래스의 새 인스턴스를 초기화 합니다.
		/// </summary>
		public DrmException()
		{
		}

		/// <summary>
		/// DrmException class의 새 인스턴스를 초기화 합니다.
		/// </summary>
		/// <param name="drmExceptionCode">DRM 처리시 나타나는 오류코드</param>
		public DrmException(DrmExceptionCodes drmExceptionCode)
			: this()
		{
			DrmExceptionCode = drmExceptionCode;
		}
		#endregion

		/// <summary>
		/// DrmExceptionCode를 구하거나 설정합니다.
		/// </summary>
		public DrmExceptionCodes DrmExceptionCode
		{
			get;
			set;
		}
	}
}
