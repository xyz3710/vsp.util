﻿/**********************************************************************************************************************/
/*	Domain		:	X10.Common.Security.DrmCore
/*	Creator		:	KIMKIWON\xyz37(김기원)
/*	Create		:	2012년 1월 4일 수요일 오후 2:01
/*	Purpose		:	DRM을 처리 알고리즘을 제공하는 클래스 입니다.
/*--------------------------------------------------------------------------------------------------------------------*/
/*	Modifier	:	
/*	Update		:	
/*	Changes		:	
/*--------------------------------------------------------------------------------------------------------------------*/
/*	Comment		:	Encrypt
 *					 1. GenerateEncryptionHeader()로 헤더 정보 생성
 *					 2. 적용할 스트림에서 파일에서 1번에서 반환한 encryptionDataLength 만큼 읽어들인 뒤
 *					 3. Encrypt()로 encryption
 *					Decrypt
 *					 1. ReadEncryptedDataLength()에서 encryption된 길이와 데이터를 구한뒤
 *					 2. Encryption 된 데이터를 읽어서 Decrypt()로 decryption
/*--------------------------------------------------------------------------------------------------------------------*/
/*	Reviewer	:	Kim Ki Won
/*	Rev. Date	:	2012년 1월 6일 금요일 오후 4:45
/**********************************************************************************************************************/

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace X10.Common.Security
{
	/// <summary>
	/// DRM을 처리 알고리즘을 제공하는 클래스 입니다.
	/// </summary>
	public class DrmCore : IDisposable
	{
		private const byte BLANK_BYTE = 0x00;
		private const byte MAX_PSEUDO_DATA_LENGTH = 255;
		private const byte USE_ENC_CODE = 0x22;
		private const byte NOT_USE_ENC_CODE = 0x73;

		private byte _magicByte;
		private static byte[] _pseudoDataMask = { 0x16, 0xCB, 0x29, 0x4C };

		#region Constructor
		/// <summary>
		/// DrmCore 클래스의 새 인스턴스를 초기화 합니다.
		/// </summary>
		/// <param name="magicByte">DRM 을 구별해주는 매직 바이트 입니다.</param>
		/// <param name="encryptionSeed">Encryption에 사용되는 Seed 문자열입니다.</param>
		/// <param name="encrytionRotateBits">암호화 Rotate에 사용하는 bit 수 입니다.</param>
		/// <param name="encryptedDataMultiply">암호화된 데이터를 구하기 위한 배수입니다.</param>
		public DrmCore(
			DrmMagicByte magicByte,
			string encryptionSeed,
			int encrytionRotateBits,
			int encryptedDataMultiply)
		{
			_magicByte = (byte)magicByte;
			EncryptionSeed = encryptionSeed;
			EncrytionRotateBits = encrytionRotateBits;
			EncryptedDataMultiply = encryptedDataMultiply;
		}
		#endregion

		#region Public methods
		/// <summary>
		/// Encryption에 사용되는 Seed 문자열을 구합니다.
		/// </summary>
		public string EncryptionSeed
		{
			get;
			private set;
		}

		/// <summary>
		/// 암호화 Rotate에 사용하는 bit를 구합니다.
		/// </summary>
		public int EncrytionRotateBits
		{
			get;
			private set;
		}

		/// <summary>
		/// 암호화된 데이터를 구하기 위한 배수를 구합니다.
		/// </summary>
		public int EncryptedDataMultiply
		{
			get;
			private set;
		}
		#endregion

		#region Private methods
		private byte[] CreateBasePseudoData()
		{
			byte[] fakeData = Encoding.UTF8.GetBytes(EncryptionSeed);
			const int basicFakeDataRotateBits = 2;

			Parallel.For(0, fakeData.Length, i =>
			{
				fakeData[i] = Convert.ToByte(fakeData[i] ^ _pseudoDataMask[i % _pseudoDataMask.Length]).RotateRight(basicFakeDataRotateBits);
			});

			return fakeData;
		}

		private byte[] CreatePseudoData(byte[] basePseudoData, string password)
		{
			if (string.IsNullOrWhiteSpace(password) == true)
			{
				basePseudoData[basePseudoData.Length - 1] = NOT_USE_ENC_CODE;

				return basePseudoData;
			}
			else
			{
				byte[] encPassword = Encoding.UTF8.GetBytes(password);
				byte[] pseudoData = new byte[basePseudoData.Length + encPassword.Length];

				Array.Copy(basePseudoData, 0, pseudoData, 0, basePseudoData.Length);
				pseudoData[basePseudoData.Length - 1] = USE_ENC_CODE;
				Array.Copy(encPassword, 0, pseudoData, basePseudoData.Length, encPassword.Length);

				return pseudoData;
			}
		}
		#endregion

		#region Encryption
		/// <summary>
		/// 암호화할 헤더 정보를 생성합니다.
		/// </summary>
		/// <param name="password">암호</param>
		/// <param name="encryptedPseudoData">암호와 결합된 허위 데이터</param>
		/// <param name="encryptionDataLength">암호화 할 데이터의 길이입니다.</param>
		/// <returns>암호화할 헤더 정보와 암호와 결합된 허위 데이터 및 암호화할 데이터의 길이를 반환합니다.</returns>
		/// <exception cref="X10.Common.Security.DrmException">정해진 규칙을 만족 하지 못할 경우</exception>
		/// <exception cref="System.IO.IOException">I/O 오류가 발생하는 경우</exception>
		/// <exception cref="System.NotSupportedException">스트림이 읽기를 지원하지 않는 경우</exception>
		/// <exception cref="System.ObjectDisposedException">스트림이 닫힌 후 메서드가 호출된 경우</exception>
		public byte[] GenerateEncryptionHeader(
			string password,
			out byte[] encryptedPseudoData,
			out int encryptionDataLength)
		{
			byte[] basePseudoData = CreateBasePseudoData();
			encryptedPseudoData = CreatePseudoData(basePseudoData, password);
			encryptionDataLength = encryptedPseudoData.Length * EncryptedDataMultiply;

			int basePseudoDataLength = basePseudoData.Length;

			if (basePseudoDataLength > MAX_PSEUDO_DATA_LENGTH)
				throw new DrmException(DrmExceptionCodes.OverflowEncryptionSeedLength);

			const int headerLength = 3;		// MAGIC_BYTE + BLANK_BYTE + basePseudoDataLength byte
			byte[] header = new byte[headerLength + basePseudoDataLength];

			header[0] = _magicByte;
			header[1] = BLANK_BYTE;
			header[2] = (byte)basePseudoDataLength;

			// basePseudoData의 마지막 바이트는 암호 사용 여부로 저장되어야 한다.
			basePseudoData[basePseudoDataLength - 1] = string.IsNullOrWhiteSpace(password) == true ? NOT_USE_ENC_CODE : USE_ENC_CODE;
			Array.Copy(basePseudoData, 0, header, headerLength, basePseudoDataLength);

			return header;
		}

		/// <summary>
		/// 데이터를 암호화 합니다.
		/// </summary>
		/// <param name="data">암호화할 데이터</param>
		/// <param name="pseudoData">암호화 하기 위한 허위 데이터</param>
		/// <returns>암호화된 데이터를 반환합니다.</returns>
		public byte[] Encrypt(byte[] data, byte[] pseudoData)
		{
			int pseudoDataLength = pseudoData.Length;

			Parallel.For(0, data.Length, i =>
			{
				data[i] = (byte)(data[i].RotateLeft(EncrytionRotateBits) ^ pseudoData[i % pseudoDataLength]);
			});

			return data;
		}
		#endregion

		#region Decryption
		/// <summary>
		/// inputStream에서 읽어서 암호화된 데이터의 길이를 구합니다.
		/// </summary>
		/// <param name="inputStream">암호화된 데이터를 읽어들일 스트림</param>
		/// <param name="password">암호</param>
		/// <param name="encryptedPseudoData">암호와 결합된 허위 데이터</param>
		/// <returns>암호화된 데이터의 길이와 암호와 결합된 허위 데이터를 반환합니다.</returns>
		/// <exception cref="X10.Common.Security.DrmException">정해진 규칙을 만족 하지 못할 경우</exception>
		/// <exception cref="System.IO.IOException">I/O 오류가 발생하는 경우</exception>
		/// <exception cref="System.NotSupportedException">스트림이 읽기를 지원하지 않는 경우</exception>
		/// <exception cref="System.ObjectDisposedException">스트림이 닫힌 후 메서드가 호출된 경우</exception>
		public int ReadEncryptedDataLength(
			Stream inputStream,
			string password,
			out byte[] encryptedPseudoData)
		{
			if (inputStream.ReadByte() != _magicByte)
				throw new DrmException(DrmExceptionCodes.InvalidMagicByte);

			if (inputStream.ReadByte() != BLANK_BYTE)
				throw new DrmException(DrmExceptionCodes.InvalidHeader);

			int pseudoDataLength = inputStream.ReadByte();
			byte[] pseudoData = new byte[pseudoDataLength];
			int readLength = inputStream.Read(pseudoData, 0, pseudoDataLength);

			encryptedPseudoData = CreatePseudoData(pseudoData, password);

			return encryptedPseudoData.Length * EncryptedDataMultiply;
		}

		/// <summary>
		/// 암호화된 데이터를 복호화 합니다.
		/// </summary>
		/// <param name="encyptedData">암호화된 데이터</param>
		/// <param name="pseudoData">복호화 하기 위한 허위 데이터</param>
		/// <returns>복호화된 데이터를 반환합니다.</returns>
		public byte[] Decrypt(byte[] encyptedData, byte[] pseudoData)
		{
			int pseudoDataLength = pseudoData.Length;

			Parallel.For(0, encyptedData.Length, i =>
			{
				encyptedData[i] = ((byte)(encyptedData[i] ^ pseudoData[i % pseudoDataLength])).RotateRight(EncrytionRotateBits);
			});

			return encyptedData;
		}
		#endregion

		/// <summary>
		/// 지정된 Magic byte로 Header를 검사하여 DRM이 적용 되었는지 여부를 구합니다.
		/// </summary>
		/// <param name="inputStream">검사하려는 stream</param>
		/// <returns>DRM이 적용 되었다면 true를, 그렇지 않다면 false를 반환합니다.</returns>
		public bool CheckDrmAdapted(Stream inputStream)
		{
			long currentPosition = inputStream.Position;
			byte[] partialHeader = new byte[2];
			
			inputStream.Position = 0;
			int readLength = inputStream.Read(partialHeader, 0, partialHeader.Length);

			inputStream.Position = currentPosition;

			return readLength == partialHeader.Length
				&& partialHeader[0] == _magicByte
				&& partialHeader[1] == BLANK_BYTE;
		}

		#region IDisposable 멤버

		/// <summary>
		/// DrmCore의 관리되지 않는 리소스의 확보, 해제 또는 다시 설정과 관련된 응용 프로그램 정의 작업을 수행합니다.
		/// </summary>
		public void Dispose()
		{
			_pseudoDataMask = null;
		}

		#endregion
	}
}
