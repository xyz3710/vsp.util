﻿/**********************************************************************************************************************/
/*	Domain		:	X10.Common.Security.DrmStreamBase
/*	Creator		:	KIMKIWON\xyz37(김기원)
/*	Create		:	2012년 1월 5일 목요일 오후 3:44
/*	Purpose		:	DRM을 처리하는 기본 클래스 입니다.
/*--------------------------------------------------------------------------------------------------------------------*/
/*	Modifier	:	
/*	Update		:	
/*	Changes		:	
/*--------------------------------------------------------------------------------------------------------------------*/
/*	Comment		:	
/*--------------------------------------------------------------------------------------------------------------------*/
/*	Reviewer	:	Kim Ki Won
/*	Rev. Date	:	2012년 1월 6일 금요일 오후 5:12
/**********************************************************************************************************************/

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace X10.Common.Security
{
	/// <summary>
	/// DRM을 처리하는 기본 클래스 입니다.
	/// </summary>
	public class DrmHandlerBase : IDisposable
	{
		private DrmCore _drmCore;

		#region Constructor
		/// <summary>
		/// DrmHandlerBase 클래스의 새 인스턴스를 초기화 합니다.
		/// </summary>
		/// <param name="magicByte">DRM 을 구별해주는 매직 바이트 입니다.</param>
		/// <param name="encryptionSeed">Encryption에 사용되는 Seed 문자열입니다.</param>
		/// <param name="encrytionRotateBits">암호화 Rotate에 사용하는 bit 수 입니다.</param>
		/// <param name="encryptedDataMultiply">암호화된 데이터를 구하기 위한 배수입니다.</param>
		/// <param name="bufferSize">DRM을 처리하는데 필요한 buffer 크기 입니다.</param>
		public DrmHandlerBase(
			DrmMagicByte magicByte,
			string encryptionSeed,
			int encrytionRotateBits,
			int encryptedDataMultiply,
			int bufferSize = 8196)
		{
			_drmCore = new DrmCore(magicByte, encryptionSeed, encrytionRotateBits, encryptedDataMultiply);
			BufferSize = bufferSize;
		}
		#endregion

		/// <summary>
		/// DRM을 처리하는데 필요한 buffer 크기 입니다
		/// </summary>
		protected int BufferSize
		{
			get;
			set;
		}

		/// <summary>
		/// filename에서 DRM이 적용되었다면 파일의 내용을 읽어서 filename에 기록 합니다.
		/// </summary>
		/// <param name="filename">DRM이 적용된 파일</param>
		/// <param name="password">암호</param>
		/// <returns>outFilename의 길이를 반환합니다.</returns>
		/// <exception cref="X10.Common.Security.DrmException">정해진 규칙을 만족 하지 못할 경우</exception>
		public long Read(string filename, string password = "")
		{
			string outFilename = Path.GetTempFileName();
			long length = Read(filename, outFilename, password);

			try
			{
				File.Copy(outFilename, filename, true);
				File.Delete(outFilename);
			}
			catch
			{
			}

			return length;
		}

		/// <summary>
		/// inFilename이 DRM이 적용되었다면 파일을 읽어서 outFilename에 기록 합니다.
		/// </summary>
		/// <param name="inFilename">DRM이 적용된 파일</param>
		/// <param name="outFilename">DRM이 해제된 파일</param>
		/// <param name="password">암호</param>
		/// <returns>outFilename의 길이를 반환합니다.</returns>
		/// <exception cref="X10.Common.Security.DrmException">정해진 규칙을 만족 하지 못할 경우</exception>
		public long Read(string inFilename, string outFilename, string password = "")
		{
			long length = 0L;

			using (FileStream inputStream = File.Open(inFilename, FileMode.Open, FileAccess.Read, FileShare.ReadWrite | FileShare.Delete))
			{
				using (FileStream outputStream = new FileStream(outFilename, FileMode.Create))
				{
					length = Read(inputStream, outputStream, password);
					outputStream.Flush();
				}
			}

			return length;
		}

		/// <summary>
		/// DRM이 적용된 스트림을 읽어서 outStream에 기록 합니다.
		/// </summary>
		/// <remarks>
		/// inStream.Position은 초기화 되고,
		/// inStream과 outStream은 사용 후 Close를 해줘야 합니다.
		/// </remarks>
		/// <param name="inStream">DRM이 적용된 스트림</param>
		/// <param name="outStream">DRM이 해제된 스트림</param>
		/// <param name="password">암호</param>
		/// <returns>outStream의 길이를 반환합니다.</returns>
		/// <exception cref="X10.Common.Security.DrmException">정해진 규칙을 만족 하지 못할 경우</exception>
		public long Read(Stream inStream, Stream outStream, string password = "")
		{
			int readLength = 0;
			byte[] pseudoData = null;
			int encLength = _drmCore.ReadEncryptedDataLength(inStream, password, out pseudoData);
			byte[] encData = new byte[encLength];

			// Encryption 된 데이터를 읽어서 Decryption 시킨다.
			readLength = inStream.Read(encData, 0, encLength);

			byte[] decData = _drmCore.Decrypt(encData, pseudoData);

			outStream.Write(decData, 0, encLength);

			do
			{
				byte[] buffer = new byte[BufferSize];

				readLength = inStream.Read(buffer, 0, BufferSize);
				outStream.Write(buffer, 0, readLength);

			} while (readLength > 0);

			inStream.Position = 0;

			return outStream.Length;
		}

		/// <summary>
		/// filename의 내용에 DRM을 적용 후 filename에 기록 합니다.
		/// </summary>
		/// <param name="filename">DRM을 적용할 파일</param>
		/// <param name="password">암호</param>
		/// <param name="duplicateDrm">이미 DRM이 적용되었을 경우 다시 DRM을 적용할지 여부</param>
		/// <returns>filename의 길이를 반환합니다.</returns>
		/// <exception cref="X10.Common.Security.DrmException">정해진 규칙을 만족 하지 못할 경우</exception>
		public long Write(string filename, string password = "", bool duplicateDrm = false)
		{
			string outFilename = Path.GetTempFileName();
			long length = Write(filename, outFilename, password, duplicateDrm);

			try
			{
				File.Copy(outFilename, filename, true);
				File.Delete(outFilename);
			}
			catch
			{
			}

			return length;
		}

		/// <summary>
		/// inFilename을 읽어서 DRM을 적용 후 outFilename에 기록 합니다.
		/// </summary>
		/// <param name="inFilename">DRM을 적용할 파일</param>
		/// <param name="outFilename">DRM이 적용된 파일</param>
		/// <param name="password">암호</param>
		/// <param name="duplicateDrm">이미 DRM이 적용되었을 경우 다시 DRM을 적용할지 여부</param>
		/// <returns>outFilename의 길이를 반환합니다.</returns>
		/// <exception cref="X10.Common.Security.DrmException">정해진 규칙을 만족 하지 못할 경우</exception>
		public long Write(string inFilename, string outFilename, string password = "", bool duplicateDrm = false)
		{
			long length = 0L;

			using (FileStream inputStream = File.Open(inFilename, FileMode.Open, FileAccess.Read, FileShare.ReadWrite | FileShare.Delete))
			{
				using (FileStream outputStream = new FileStream(outFilename, FileMode.Create))
				{
					length = Write(inputStream, outputStream, password);
					outputStream.Flush();
				}
			}

			return length;
		}

		/// <summary>
		/// inStream을 스트림을 읽어서 DRM을 적용 후 outStream에 기록 합니다.
		/// </summary>
		/// <remarks>
		/// inStream.Position은 초기화 되고,
		/// inStream과 outStream은 사용 후 Close를 해줘야 합니다.
		/// </remarks>
		/// <param name="inStream">DRM을 적용할 스트림</param>
		/// <param name="outStream">DRM이 적용된 스트림</param>
		/// <param name="password">암호</param>
		/// <param name="duplicateDrm">이미 DRM이 적용되었을 경우 다시 DRM을 적용할지 여부</param>
		/// <returns>outStream의 길이를 반환합니다.</returns>
		/// <exception cref="X10.Common.Security.DrmException">정해진 규칙을 만족 하지 못할 경우</exception>
		public long Write(Stream inStream, Stream outStream, string password = "", bool duplicateDrm = false)
		{
			byte[] pseudoData = null;
			int encryptionDataLength = 0;

			if (duplicateDrm == false && _drmCore.CheckDrmAdapted(inStream) == true)
				throw new DrmException(DrmExceptionCodes.DrmAlreadyAdapted);

			byte[] header = _drmCore.GenerateEncryptionHeader(password, out pseudoData, out encryptionDataLength);
			byte[] toEncryptionHeader = new byte[encryptionDataLength];

			int readLength = inStream.Read(toEncryptionHeader, 0, encryptionDataLength);
			byte[] encryptedData = _drmCore.Encrypt(toEncryptionHeader, pseudoData);

			outStream.Write(header, 0, header.Length);
			outStream.Write(encryptedData, 0, encryptedData.Length);

			do
			{
				byte[] buffer = new byte[BufferSize];

				readLength = inStream.Read(buffer, 0, BufferSize);
				outStream.Write(buffer, 0, readLength);

			} while (readLength > 0);

			inStream.Position = 0;

			return outStream.Length;
		}

		/// <summary>
		/// 지정된 Magic byte로 Header를 검사하여 DRM이 적용 되었는지 여부를 구합니다.
		/// </summary>
		/// <param name="filename">검사하려는 파일</param>
		/// <returns>DRM이 적용 되었다면 true를, 그렇지 않다면 false를 반환합니다.</returns>
		public bool CheckDrmAdapted(string filename)
		{
			bool result = false;

			using (FileStream inputStream = File.Open(filename, FileMode.Open, FileAccess.Read, FileShare.ReadWrite | FileShare.Delete))
			{
				result = CheckDrmAdapted(inputStream);
			}

			return result;
		}

		/// <summary>
		/// 지정된 Magic byte로 Header를 검사하여 DRM이 적용 되었는지 여부를 구합니다.
		/// </summary>
		/// <param name="inputStream">검사하려는 stream</param>
		/// <returns>DRM이 적용 되었다면 true를, 그렇지 않다면 false를 반환합니다.</returns>
		public bool CheckDrmAdapted(Stream inputStream)
		{
			return _drmCore.CheckDrmAdapted(inputStream);
		}

		#region IDisposable 멤버

		/// <summary>
		/// DrmHandlerBase의 관리되지 않는 리소스의 확보, 해제 또는 다시 설정과 관련된 응용 프로그램 정의 작업을 수행합니다.
		/// </summary>
		public void Dispose()
		{
			if (_drmCore == null)
				_drmCore.Dispose();
		}
		#endregion
	}
}
