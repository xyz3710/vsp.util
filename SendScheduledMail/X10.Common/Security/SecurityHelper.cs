/**********************************************************************************************************************/
/*	Domain		:	X10.Common.Security.SecurityHelper
/*	Creator		:	KIMKIWON\xyz37(김기원)
/*	Create		:	2011년 10월 11일 화요일 오후 5:04
/*	Purpose		:	보안관련된 내용을 지원합니다.
/*--------------------------------------------------------------------------------------------------------------------*/
/*	Modifier	:	
/*	Update		:	
/*	Changes		:	
/*--------------------------------------------------------------------------------------------------------------------*/
/*	Comment		:	
/*--------------------------------------------------------------------------------------------------------------------*/
/*	Reviewer	:	Kim Ki Won
/*	Rev. Date	:	
/**********************************************************************************************************************/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Helpers;
using System.Security.Cryptography;
using System.IO;

namespace X10.Common.Security
{
	/// <summary>
	/// 보안관련된 내용을 지원합니다.
	/// </summary>
	public class SecurityHelper
	{
		private const string EncryptionKey = "sMarTauDioeNcryPtionkEy";

		/// <summary>
		/// 해쉬화된 암호를 구합니다.
		/// </summary>
		/// <param name="newPassword"></param>
		/// <returns></returns>
		public static string HashPassword(string newPassword)
		{
			return Crypto.HashPassword(newPassword);
		}

		/// <summary>
		/// 토큰을 생성합니다.
		/// </summary>
		/// <returns></returns>
		public static string GenerateToken()
		{
			byte[] data = new byte[0x10];

			using (RNGCryptoServiceProvider provider = new RNGCryptoServiceProvider())
			{
				provider.GetBytes(data);

				return Convert.ToBase64String(data);
			}
		}

		/// <summary>
		/// 암호화된 텍스트를 구합니다.
		/// </summary>
		/// <param name="plainText">암호화할 문자열</param>
		/// <param name="encryptionPrivateKey">암호화에 사용될 개인키로 16자 이상이어야 합니다.</param>
		/// <param name="useUrl">Url로 사용할 경우 UrlEncoding을 한다.</param>
		/// <returns></returns>
		/// <exception cref="ArgumentException">encryptionPrivateKey가 16 자리 이하면 발생합니다.</exception>
		public static string EncryptText(string plainText, string encryptionPrivateKey = "", bool useUrl = false)
		{
			if (string.IsNullOrEmpty(plainText))
			{
				return plainText;
			}

			if (string.IsNullOrEmpty(encryptionPrivateKey) == true)
			{
				encryptionPrivateKey = EncryptionKey;
			}

			var tDESalg = new TripleDESCryptoServiceProvider();
			var encoding = Encoding.UTF8;//new ASCIIEncoding();
			tDESalg.Key = encoding.GetBytes(encryptionPrivateKey.Substring(0, 16));
			tDESalg.IV = encoding.GetBytes(encryptionPrivateKey.Substring(8, 8));

			byte[] encryptedBinary = EncryptTextToMemory(plainText, tDESalg.Key, tDESalg.IV);
			string result = Convert.ToBase64String(encryptedBinary);

			if (useUrl == true)
			{
				return System.Web.HttpUtility.UrlEncode(result);
			}
			else
			{
				return result;
			}
		}

		/// <summary>
		/// 복호화된 텍스트를 구합니다.
		/// </summary>
		/// <param name="cipherText">암호화된 문자열</param>
		/// <param name="encryptionPrivateKey">암호화에 사용된 개인키로 16자 이상이어야 합니다.</param>
		/// <param name="useUrl">Url로 사용된 경우 UrlDecoding을 한다.</param>
		/// <returns></returns>
		/// <exception cref="ArgumentException">encryptionPrivateKey가 16 자리 이하면 발생합니다.</exception>
		public static string DecryptText(string cipherText, string encryptionPrivateKey = "", bool useUrl = false)
		{
			if (String.IsNullOrEmpty(cipherText))
			{
				return cipherText;
			}

			if (String.IsNullOrEmpty(encryptionPrivateKey) == true)
			{
				encryptionPrivateKey = EncryptionKey;
			}

			if (useUrl == true)
			{
				cipherText = System.Web.HttpUtility.UrlDecode(cipherText);
			}

			var tDESalg = new TripleDESCryptoServiceProvider();
			var encoding = Encoding.UTF8; //new ASCIIEncoding();
			tDESalg.Key = encoding.GetBytes(encryptionPrivateKey.Substring(0, 16));
			tDESalg.IV = encoding.GetBytes(encryptionPrivateKey.Substring(8, 8));

			byte[] buffer = Convert.FromBase64String(cipherText);

			return DecryptTextFromMemory(buffer, tDESalg.Key, tDESalg.IV);
		}

		/// <summary>
		/// 암호화된 Base64 문자열인지 여부를 구합니다.
		/// </summary>
		/// <param name="cipherText">검사하려는 문자열</param>
		/// <returns>
		///   <c>true</c> if [is base64 string] [the specified s]; otherwise, <c>false</c>.
		/// </returns>
		public static bool IsBase64String(string cipherText)
		{
			if (string.IsNullOrEmpty(cipherText) == true || (cipherText.Length % 4) != 0)
				return false;

			try
			{
				Convert.FromBase64String(cipherText);

				return true;
			}
			catch (FormatException)
			{
				return false;
			}
		}

		private static byte[] EncryptTextToMemory(string data, byte[] key, byte[] iv)
		{
			using (var ms = new MemoryStream())
			{
				using (var cs = new CryptoStream(ms, new TripleDESCryptoServiceProvider().CreateEncryptor(key, iv), CryptoStreamMode.Write))
				{
					byte[] toEncrypt = new UnicodeEncoding().GetBytes(data);
					cs.Write(toEncrypt, 0, toEncrypt.Length);
					cs.FlushFinalBlock();
				}

				return ms.ToArray();
			}
		}

		private static string DecryptTextFromMemory(byte[] data, byte[] key, byte[] iv)
		{
			using (var ms = new MemoryStream(data))
			{
				using (var cs = new CryptoStream(ms, new TripleDESCryptoServiceProvider().CreateDecryptor(key, iv), CryptoStreamMode.Read))
				{
					var sr = new StreamReader(cs, new UnicodeEncoding());

					return sr.ReadLine();
				}
			}
		}

		/// <summary>
		/// X10에서 호출할 때 사용할 Interface Key를 생성합니다.
		/// </summary>
		/// <param name="fromDate">현재 주간의 일요일</param>
		/// <param name="toDate">현재 주간의 토요일</param>
		/// <returns></returns>
		public static string GenerateInterfaceKey(string fromDate, string toDate)
		{
			string dateTimeKey = string.Format("{0}{1}", fromDate, toDate);

			return EncryptText(dateTimeKey, useUrl: true);
		}
	}
}
