﻿/**********************************************************************************************************************/
/*	Domain		:	X10.Common.Security.SecurityHelperToJava
/*	Creator		:	KIMKIWON\xyz37(김기원)
/*	Create		:	2012년 8월 10일 금요일 오후 5:18
/*	Purpose		:	Java와 Interface를 위한 Encryption/Decryption 모듈을 제공합니다.
/*--------------------------------------------------------------------------------------------------------------------*/
/*	Modifier	:	
/*	Update		:	
/*	Changes		:	
/*--------------------------------------------------------------------------------------------------------------------*/
/*	Comment		:	http://lamahashim.blogspot.kr/2009/08/encyptiondecryption-in-c-and-java.html
 *					Java와 URL에서 데이터를 주고 받을 경우
 *					HttpContext.Current.Server.UrlEncode, HttpContext.Current.Server.UrlDecode를 사용한다.
 *					Java의 URL Encoding 방법은 SecurityHelperToJava.txt나	위 링크의 댓글을 확인한다.
/*--------------------------------------------------------------------------------------------------------------------*/
/*	Reviewer	:	Kim Ki Won
/*	Rev. Date	:	
/**********************************************************************************************************************/

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Web.Helpers;

namespace X10.Common.Security
{
	/// <summary>
	/// Java와 Interface를 위한 Encryption/Decryption 모듈을 제공합니다.
	/// </summary>
	public class SecurityHelperToJava
	{
		private const string PRIVATE_KEY = "X10#$inTfaC$*e";

		/// <summary>
		/// 암호화된 텍스트를 구합니다.
		/// </summary>
		/// <param name="plainText">암호화할 문자열</param>
		/// <param name="privateKey">암호화에 사용할 개인키</param>
		/// <returns></returns>
		public static string EncryptText(string plainText, string privateKey = PRIVATE_KEY)
		{
			RijndaelManaged rijndaelCipher = new RijndaelManaged();
			rijndaelCipher.Mode = CipherMode.CBC;
			rijndaelCipher.Padding = PaddingMode.PKCS7;
			rijndaelCipher.KeySize = 0x80;
			rijndaelCipher.BlockSize = 0x80;

			byte[] pwdBytes = Encoding.UTF8.GetBytes(privateKey);
			byte[] keyBytes = new byte[0x10];
			int len = pwdBytes.Length;

			if (len > keyBytes.Length)
				len = keyBytes.Length;

			Array.Copy(pwdBytes, keyBytes, len);

			rijndaelCipher.Key = keyBytes;
			rijndaelCipher.IV = keyBytes;

			ICryptoTransform transform = rijndaelCipher.CreateEncryptor();
			byte[] innerPlainText = Encoding.UTF8.GetBytes(plainText);

			return Convert.ToBase64String(transform.TransformFinalBlock(innerPlainText, 0, innerPlainText.Length));
		}

		/// <summary>
		/// 복호화된 텍스트를 구합니다.
		/// </summary>
		/// <param name="cipherText">암호화된 문자열</param>
		/// <param name="privateKey">암호화에 사용된 개인키</param>
		/// <returns></returns>
		public static string DecryptText(string cipherText, string privateKey = PRIVATE_KEY)
		{
			RijndaelManaged rijndaelCipher = new RijndaelManaged();

			rijndaelCipher.Mode = CipherMode.CBC;
			rijndaelCipher.Padding = PaddingMode.PKCS7;
			rijndaelCipher.KeySize = 0x80;
			rijndaelCipher.BlockSize = 0x80;

			byte[] encryptedData = Convert.FromBase64String(cipherText);
			byte[] pwdBytes = Encoding.UTF8.GetBytes(privateKey);
			byte[] keyBytes = new byte[0x10];
			int len = pwdBytes.Length;

			if (len > keyBytes.Length)
				len = keyBytes.Length;

			Array.Copy(pwdBytes, keyBytes, len);

			rijndaelCipher.Key = keyBytes;
			rijndaelCipher.IV = keyBytes;

			byte[] plainText = rijndaelCipher.CreateDecryptor().TransformFinalBlock(encryptedData, 0, encryptedData.Length);

			return Encoding.UTF8.GetString(plainText);
		}
	}
}
