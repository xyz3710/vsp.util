﻿/**********************************************************************************************************************/
/*	Domain		:	System.Web.RazorViewExtensions
/*	Creator		:	KIM-KIWON\xyz37(김기원)
/*	Create		:	2012년 7월 18일 수요일 오후 2:02
/*	Purpose		:	Razor View에서 확장 기능을 지원합니다.
/*--------------------------------------------------------------------------------------------------------------------*/
/*	Modifier	:	
/*	Update		:	
/*	Changes		:	
/*--------------------------------------------------------------------------------------------------------------------*/
/*	Comment		:	
/*--------------------------------------------------------------------------------------------------------------------*/
/*	Reviewer	:	Kim Ki Won
/*	Rev. Date	:	
/**********************************************************************************************************************/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using X10.Common;

namespace System.Web
{
	/// <summary>
	/// Razor View에서 확장 기능을 지원합니다.
	/// </summary>
	public static class RazorViewExtensions
	{
		/// <summary>
		/// 현재 view의 controller 이름을 구합니다.
		/// </summary>
		/// <param name="webViewPage">The web view page.</param>
		/// <returns></returns>
		public static string GetControllerName(this WebViewPage webViewPage)
		{
			return Convert.ToString(webViewPage.ViewContext.Controller.ValueProvider.GetValue("controller").RawValue);
		}

		/// <summary>
		/// 현재 view의 action 이름을 구합니다.
		/// </summary>
		/// <param name="webViewPage">The web view page.</param>
		/// <returns></returns>
		public static string GetActionName(this WebViewPage webViewPage)
		{
			return Convert.ToString(webViewPage.ViewContext.Controller.ValueProvider.GetValue("action").RawValue);
		}

		/// <summary>
		/// 현재 Controller Context의 controller 이름을 구합니다.
		/// </summary>
		/// <param name="controllerContext">Controller context</param>
		/// <returns></returns>
		public static string GetControllerName(this ControllerContext controllerContext)
		{
			return Convert.ToString(controllerContext.RouteData.Values["controller"]);
		}

		/// <summary>
		/// 현재 Controller Context의 action 이름을 구합니다.
		/// </summary>
		/// <param name="controllerContext">Controller context</param>
		/// <returns></returns>
		public static string GetActionName(this ControllerContext controllerContext)
		{
			return Convert.ToString(controllerContext.RouteData.Values["action"]);
		}

		/// <summary>
		/// 현재 Controller의 controller 이름을 구합니다.
		/// </summary>
		/// <param name="controller">Controller</param>
		/// <returns></returns>
		public static string GetControllerName(this Controller controller)
		{
			return Convert.ToString(controller.ControllerContext.RouteData.Values["controller"]);
		}

		/// <summary>
		/// 현재 Controller의 action 이름을 구합니다.
		/// </summary>
		/// <param name="controller">Controller</param>
		/// <returns></returns>
		public static string GetActionName(this Controller controller)
		{
			return Convert.ToString(controller.ControllerContext.RouteData.Values["action"]);
		}
	}
}
