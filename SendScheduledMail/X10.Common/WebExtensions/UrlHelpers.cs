﻿/**********************************************************************************************************************/
/*	Domain		:	System.Web.Mvc.UrlHelpers
/*	Creator		:	KIMKIWON\xyz37(김기원)
/*	Create		:	2011년 9월 26일 월요일 오후 9:57
/*	Purpose		:	Url과 관련된 확장 기능을 제공합니다.
/*--------------------------------------------------------------------------------------------------------------------*/
/*	Modifier	:	
/*	Update		:	
/*	Changes		:	
/*--------------------------------------------------------------------------------------------------------------------*/
/*	Comment		:	
/*--------------------------------------------------------------------------------------------------------------------*/
/*	Reviewer	:	Kim Ki Won
/*	Rev. Date	:	
/**********************************************************************************************************************/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using System.Text;
using System.Configuration;

namespace System.Web.Mvc
{
	/// <summary>
	/// Url과 관련된 확장 기능을 제공합니다.
	/// </summary>
	public static class UrlHelpers
	{
		/// <summary>
		/// 현재 접속이 로컬에서 접속했는지 여부를 구합니다.
		/// </summary>
		/// <param name="url"></param>
		/// <returns></returns>
		public static bool IsLocalUrl(this UrlHelper url)
		{
			//return url.IsLocalUrl(HttpContext.Current.Request.Url.ToString());
			return HttpContext.Current.IsLocalUrl();
		}

		/// <summary>
		/// 현재 Url이 "Debug" 라우트를 사용했는지 여부를 구합니다.
		/// </summary>
		/// <param name="url"></param>
		/// <returns></returns>
		public static bool IsDebugRoute(this UrlHelper url)
		{
			return url.GetRouteName() == "debug";
		}

		/// <summary>
		/// 라우트 이름을 구합니다.
		/// </summary>
		/// <param name="url"></param>
		/// <returns></returns>
		public static string GetRouteName(this UrlHelper url)
		{
			return url.RouteCollection.GetRouteData(url.RequestContext.HttpContext).GetRouteName();
		}

		/// <summary>
		/// Url에서 지정된 식별자와 값을 검색합니다.
		/// </summary>
		/// <param name="url"></param>
		/// <param name="valueName">검색할 값의 키입니다.</param>
		/// <returns>해당 키가 valueName과 일치하는 System.Web.Routing.RouteData.Values 속성의 요소입니다.</returns>
		/// <exception cref="System.InvalidOperationException">valueName에 대한 값이 존재하지 않습니다.</exception>
		public static string GetRequiredString(this UrlHelper url, string valueName)
		{
			return url.RouteCollection.GetRouteData(url.RequestContext.HttpContext).GetRequiredString(valueName);
		}

		/// <summary>
		/// Url에서 지정된 식별자와 값을 검색합니다.
		/// </summary>
		/// <param name="url"></param>
		/// <param name="valueName">검색할 값의 키입니다.</param>
		/// <returns>해당 키가 valueName과 일치하는 System.Web.Routing.RouteData.Values 속성의 요소입니다.</returns>
		/// <exception cref="System.InvalidOperationException">valueName에 대한 값이 존재하지 않습니다.</exception>
		public static int GetRequiredInt32(this UrlHelper url, string valueName)
		{
			string intString = url.GetRequiredString(valueName);
			int result = 0;

			int.TryParse(intString, out result);

			return result;
		}
	}
}