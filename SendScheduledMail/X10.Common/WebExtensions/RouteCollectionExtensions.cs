/**********************************************************************************************************************/
/*	Domain		:	System.Web.Mvc.RouteCollectionExtensions
/*	Creator		:	X10\xyz37(김기원)
/*	Create		:	2011년 9월 29일 목요일 오후 4:34
/*	Purpose		:	RouteCollection의 확장기능을 제공합니다.
/*--------------------------------------------------------------------------------------------------------------------*/
/*	Modifier	:	
/*	Update		:	
/*	Changes		:	
/*--------------------------------------------------------------------------------------------------------------------*/
/*	Comment		:	
/*--------------------------------------------------------------------------------------------------------------------*/
/*	Reviewer	:	Kim Ki Won
/*	Rev. Date	:	
/**********************************************************************************************************************/

using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using System.Web.Security;

namespace System.Web.Mvc
{
	/// <summary>
	/// RouteCollection의 확장기능을 제공합니다.
	/// </summary>
	public static class RouteCollectionExtensions
	{
		#region Constants
		private const string ROUTE_NAME_KEY = "RouteName";
		#endregion

		/// <summary>
		/// DataToken에 라우트 이름을 넣은 라우트를 생성합니다.
		/// </summary>
		/// <param name="routes">응용 프로그램에 대한 경로의 컬렉션입니다.</param>
		/// <param name="name">매핑할 경로(라우트)의 이름입니다.</param>
		/// <param name="url">경로의 URL 패턴입니다.</param>
		/// <param name="defaults">기본 경로 값을 포함하는 개체입니다.</param>
		/// <returns>매핑된 경로에 대한 참조입니다.</returns>
		/// <exception cref="System.ArgumentNullException">routes 또는 url 매개 변수가 null입니다.</exception>
		public static Route MapRouteWithName(
			this RouteCollection routes,
			string name,
			string url,
			object defaults)
		{
			object constraints = null;

			return MapRouteWithName(routes, name, url, defaults, constraints);
		}

		/// <summary>
		/// DataToken에 라우트 이름을 넣은 라우트를 생성합니다.
		/// </summary>
		/// <param name="routes">응용 프로그램에 대한 경로의 컬렉션입니다.</param>
		/// <param name="name">매핑할 경로(라우트)의 이름입니다.</param>
		/// <param name="url">경로의 URL 패턴입니다.</param>
		/// <param name="defaults">기본 경로 값을 포함하는 개체입니다.</param>
		/// <param name="constraints">url 매개 변수에 대한 값을 지정하는 식의 집합입니다.</param>
		/// <returns>매핑된 경로에 대한 참조입니다.</returns>
		/// <exception cref="System.ArgumentNullException">routes 또는 url 매개 변수가 null입니다.</exception>
		public static Route MapRouteWithName(
			this RouteCollection routes,
			string name,
			string url,
			object defaults,
			object constraints)
		{
			string[] namespaces = null;

			return MapRouteWithName(routes, name, url, defaults, constraints, namespaces);
		}

		/// <summary>
		/// DataToken에 라우트 이름을 넣은 라우트를 생성합니다.
		/// </summary>
		/// <param name="routes">응용 프로그램에 대한 경로의 컬렉션입니다.</param>
		/// <param name="name">매핑할 경로(라우트)의 이름입니다.</param>
		/// <param name="url">경로의 URL 패턴입니다.</param>
		/// <param name="defaults">기본 경로 값을 포함하는 개체입니다.</param>
		/// <param name="constraints">url 매개 변수에 대한 값을 지정하는 식의 집합입니다.</param>
		/// <param name="namespaces">응용 프로그램의 네임스페이스 집합입니다.</param>
		/// <returns>매핑된 경로에 대한 참조입니다.</returns>
		/// <exception cref="System.ArgumentNullException">routes 또는 url 매개 변수가 null입니다.</exception>
		public static Route MapRouteWithName(
			this RouteCollection routes,
			string name,
			string url,
			object defaults,
			object constraints,
			string[] namespaces)
		{
			Route route = routes.MapRoute(name, url, defaults, constraints, namespaces);

			route.DataTokens = new RouteValueDictionary();
			route.DataTokens.Add(ROUTE_NAME_KEY, name);

			return route;
		}

		/// <summary>
		/// 라우트 이름을 구합니다.
		/// </summary>
		/// <remarks>View에서만 사용합니다.<br/>Controller에서는 <see cref="System.Web.Mvc.UrlHelpers.GetRouteName"/>을 사용합니다.</remarks>
		/// <param name="route"></param>
		/// <returns></returns>
		public static string GetRouteName(this Route route)
		{
			if (route == null)
				return null;

			return route.DataTokens.GetRouteName();
		}

		/// <summary>
		/// 라우트 이름을 구합니다.
		/// <remarks>View에서만 사용합니다.<br/>Controller에서는 <see cref="System.Web.Mvc.UrlHelpers.GetRouteName"/>을 사용합니다.</remarks>
		/// </summary>
		/// <param name="routeData"></param>
		/// <returns></returns>
		public static string GetRouteName(this RouteData routeData)
		{
			if (routeData == null)
				return null;

			return routeData.DataTokens.GetRouteName();
		}

		/// <summary>
		/// 라우트 이름을 구합니다.
		/// <remarks>View에서만 사용합니다.<br/>Controller에서는 <see cref="System.Web.Mvc.UrlHelpers.GetRouteName"/>을 사용합니다.</remarks>
		/// </summary>
		/// <param name="routeValues"></param>
		/// <returns></returns>
		public static string GetRouteName(this RouteValueDictionary routeValues)
		{
			if (routeValues == null)
				return null;

			object routeName = null;

			routeValues.TryGetValue(ROUTE_NAME_KEY, out routeName);

			return routeName as string;
		}
	}
}

