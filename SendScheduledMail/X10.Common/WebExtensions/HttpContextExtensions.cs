﻿/**********************************************************************************************************************/
/*	Domain		:	System.Web.HttpContextExtensions
/*	Creator		:	KIMKIWON\xyz37(김기원)
/*	Create		:	2011년 8월 18일 목요일 오후 9:30
/*	Purpose		:	HttpContext에 확장 기능을 지원합니다.
/*--------------------------------------------------------------------------------------------------------------------*/
/*	Modifier	:	
/*	Update		:	
/*	Changes		:	
/*--------------------------------------------------------------------------------------------------------------------*/
/*	Comment		:	http://msdn.microsoft.com/ko-kr/library/ms178194(v=vs.100).aspx
/*--------------------------------------------------------------------------------------------------------------------*/
/*	Reviewer	:	Kim Ki Won
/*	Rev. Date	:	2011년 8월 18일 목요일 오후 9:47
/**********************************************************************************************************************/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using X10.Common;

namespace System.Web
{
	/// <summary>
	/// HttpContext에 확장 기능을 지원합니다.
	/// </summary>
	public static class HttpContextExtensions
	{
		/// <summary>
		/// 현재 접속이 로컬에서 접속했는지 여부를 구합니다.
		/// </summary>
		/// <param name="context"></param>
		/// <returns></returns>
		public static bool IsLocalUrl(this HttpContext context)
		{
			return context.Request.Url.Host == "localhost";
		}

		#region ConvertToHttpContextBase
		/// <summary>
		/// HttpContextWrapper를 사용해서 HttpContextBase 개체를 생성합니다.
		/// </summary>
		/// <param name="httpContext"></param>
		/// <returns></returns>
		public static HttpContextBase GetHttpContextBase(this HttpContext httpContext)
		{
			return (new HttpContextWrapper(httpContext));
		}

		#endregion

		#region GetCookie / SetCookie
		/// <summary>
		/// 지정된 키에서 쿠키 값을 구합니다.
		/// </summary>
		/// <param name="request"></param>
		/// <param name="cookieKey">쿠키의 키</param>
		/// <returns></returns>
		public static string GetCookie(this HttpRequestBase request, string cookieKey)
		{
			if (request == null)
				throw new InvalidOperationException("request가 null 입니다.");

			if (string.IsNullOrEmpty(cookieKey) == true)
				throw new InvalidOperationException("cookieKey가 지정되지 않았습니다.");

			return request.Cookies[cookieKey] == null ?
				string.Empty :
				request.Cookies[cookieKey].Value;
		}

		/// <summary>
		/// 지정된 키에서 쿠키 값을 구합니다.
		/// </summary>
		/// <param name="httpContext"></param>
		/// <param name="cookieKey">쿠키의 키</param>
		/// <returns></returns>
		public static string GetCookie(this HttpContextBase httpContext, string cookieKey)
		{
			if (httpContext == null)
				throw new InvalidOperationException("httpContext가 null 입니다.");

			return GetCookie(httpContext.Request, cookieKey);
		}

		/// <summary>
		/// 지정된 키에서 쿠키 값을 구합니다.
		/// </summary>
		/// <param name="httpContext"></param>
		/// <param name="cookieKey">쿠키의 키</param>
		/// <returns></returns>
		public static string GetCookie(this HttpContext httpContext, string cookieKey)
		{
			return httpContext.GetHttpContextBase().GetCookie(cookieKey);
		}

		/// <summary>
		/// 지정된 키에서 쿠키 값을 구합니다.
		/// </summary>
		/// <param name="controller"></param>
		/// <param name="cookieKey">쿠키의 키</param>
		/// <returns></returns>
		public static string GetCookie(this ControllerBase controller, string cookieKey)
		{
			if (controller == null)
				throw new InvalidOperationException("controller가 null 입니다.");
			if (controller.ControllerContext == null)
				throw new InvalidOperationException("controller.ControllerContext가 null 입니다.");

			return GetCookie(controller.ControllerContext.HttpContext, cookieKey);
		}

		/// <summary>
		/// 지정된 키로 쿠키를 생성 합니다.
		/// </summary>
		/// <param name="response"></param>
		/// <param name="cookieKey">쿠키의 키</param>
		/// <param name="value">쿠키의 값</param>
		/// <param name="expires">만료 일자</param>
		public static void SetCookie(this HttpResponseBase response, string cookieKey, string value, DateTime? expires)
		{
			if (response == null)
				throw new InvalidOperationException("reponse가 null 입니다.");

			if (string.IsNullOrEmpty(cookieKey) == true)
				throw new InvalidOperationException("cookieKey가 지정되지 않았습니다.");

			response.Cookies[cookieKey].Value = value;

			if (expires != null)
				response.Cookies[cookieKey].Expires = (DateTime)expires;
		}

		/// <summary>
		/// 지정된 키로 쿠키를 생성 합니다.
		/// </summary>
		/// <param name="httpContext"></param>
		/// <param name="cookieKey">쿠키의 키</param>
		/// <param name="value">쿠키의 값</param>
		/// <param name="expires">만료 일자</param>
		public static void SetCookie(this HttpContextBase httpContext, string cookieKey, string value, DateTime? expires)
		{
			if (httpContext == null)
				throw new InvalidOperationException("httpContext가 null 입니다.");

			SetCookie(httpContext.Response, cookieKey, value, expires);
		}

		/// <summary>
		/// 지정된 키로 쿠키를 생성 합니다.
		/// </summary>
		/// <param name="httpContext"></param>
		/// <param name="cookieKey">쿠키의 키</param>
		/// <param name="value">쿠키의 값</param>
		/// <param name="expires">만료 일자</param>
		public static void SetCookie(this HttpContext httpContext, string cookieKey, string value, DateTime? expires)
		{
			if (httpContext == null)
				throw new InvalidOperationException("httpContext가 null 입니다.");

			httpContext.GetHttpContextBase().SetCookie(cookieKey, value, expires);
		}

		/// <summary>
		/// 지정된 키로 쿠키를 생성 합니다.
		/// </summary>
		/// <param name="controller"></param>
		/// <param name="cookieKey">쿠키의 키</param>
		/// <param name="value">쿠키의 값</param>
		/// <param name="expires">만료 일자</param>
		public static void SetCookie(this ControllerBase controller, string cookieKey, string value, DateTime? expires)
		{
			if (controller == null)
				throw new InvalidOperationException("controller가 null 입니다.");

			if (controller.ControllerContext == null)
				throw new InvalidOperationException("controller.ControllerContext가 null 입니다.");

			SetCookie(controller.ControllerContext.HttpContext, cookieKey, value, expires);
		}

		/// <summary>
		/// 지정된 키로 브라우저가 열려 있는 기간만 사용되는 쿠키를 생성 합니다.
		/// </summary>
		/// <param name="response"></param>
		/// <param name="cookieKey">쿠키의 키</param>
		/// <param name="value">쿠키의 값</param>
		public static void SetCookie(this HttpResponseBase response, string cookieKey, string value)
		{
			DateTime? expires = null;

			SetCookie(response, cookieKey, value, expires);
		}

		/// <summary>
		/// 지정된 키로 브라우저가 열려 있는 기간만 사용되는 쿠키를 생성 합니다.
		/// </summary>
		/// <param name="httpContext"></param>
		/// <param name="cookieKey">쿠키의 키</param>
		/// <param name="value">쿠키의 값</param>
		public static void SetCookie(this HttpContextBase httpContext, string cookieKey, string value)
		{
			DateTime? expires = null;

			SetCookie(httpContext, cookieKey, value, expires);
		}

		/// <summary>
		/// 지정된 키로 브라우저가 열려 있는 기간만 사용되는 쿠키를 생성 합니다.
		/// </summary>
		/// <param name="httpContext"></param>
		/// <param name="cookieKey">쿠키의 키</param>
		/// <param name="value">쿠키의 값</param>
		public static void SetCookie(this HttpContext httpContext, string cookieKey, string value)
		{
			if (httpContext == null)
				throw new InvalidOperationException("httpContext가 null 입니다.");

			httpContext.GetHttpContextBase().SetCookie(cookieKey, value);
		}

		/// <summary>
		/// 지정된 키로 브라우저가 열려 있는 기간만 사용되는 쿠키를 생성 합니다.
		/// </summary>
		/// <param name="controller"></param>
		/// <param name="cookieKey">쿠키의 키</param>
		/// <param name="value">쿠키의 값</param>
		public static void SetCookie(this ControllerBase controller, string cookieKey, string value)
		{
			DateTime? expires = null;

			SetCookie(controller, cookieKey, value, expires);
		}

		#endregion

		#region RemoveCookie
		/// <summary>
		/// 지정된 키의 쿠기를 삭제합니다.
		/// </summary>
		/// <param name="response"></param>
		/// <param name="cookieKey">쿠키의 키</param>
		public static void RemoveCookie(this HttpResponseBase response, string cookieKey)
		{
			if (response == null)
				throw new InvalidOperationException("reponse가 null 입니다.");

			if (string.IsNullOrEmpty(cookieKey) == true)
				throw new InvalidOperationException("cookieKey가 지정되지 않았습니다.");

			if (response.StatusCode == 200)
			{
				response.Cookies[cookieKey].Expires = DateTime.Today.AddDays(-1);
				response.Cookies.Remove(cookieKey);
			}
		}

		/// <summary>
		/// 지정된 키의 쿠기를 삭제합니다.
		/// </summary>
		/// <param name="httpContext"></param>
		/// <param name="cookieKey">쿠키의 키</param>
		public static void RemoveCookie(this HttpContextBase httpContext, string cookieKey)
		{
			if (httpContext == null)
				throw new InvalidOperationException("httpContext가 null 입니다.");

			RemoveCookie(httpContext.Response, cookieKey);

			//if (httpContext.Request.Cookies[cookieKey] != null)
			//{
			//    httpContext.Request.Cookies[cookieKey].Expires = DateTime.Today.AddDays(-1);
			//    httpContext.Request.Cookies.Remove(cookieKey);
			//}
		}

		/// <summary>
		/// 지정된 키의 쿠기를 삭제합니다.
		/// </summary>
		/// <param name="httpContext"></param>
		/// <param name="cookieKey">쿠키의 키</param>
		public static void RemoveCookie(this HttpContext httpContext, string cookieKey)
		{
			if (httpContext == null)
				throw new InvalidOperationException("httpContext가 null 입니다.");

			httpContext.GetHttpContextBase().RemoveCookie(cookieKey);
		}

		/// <summary>
		/// 지정된 키의 쿠기를 삭제합니다.
		/// </summary>
		/// <param name="controller"></param>
		/// <param name="cookieKey">쿠키의 키</param>
		public static void RemoveCookie(this ControllerBase controller, string cookieKey)
		{
			if (controller == null)
				throw new InvalidOperationException("controller가 null 입니다.");

			if (controller.ControllerContext == null)
				throw new InvalidOperationException("controller.ControllerContext가 null 입니다.");

			RemoveCookie(controller.ControllerContext.HttpContext, cookieKey);
		}

		#endregion

		#region WriteToPage, WriteLineToPage
		/// <summary>
		/// HttpContext 객체의 Response.Write를 이용하여 간단한 로그를 페이지에 표시합니다.
		/// </summary>
		/// <param name="httpContext"></param>
		/// <param name="message"></param>
		public static void WriteToPage(this HttpContextBase httpContext, string message)
		{
			httpContext.Response.Write(message);
		}

		/// <summary>
		/// HttpContext 객체의 Response.Write를 이용하여 간단한 로그를 페이지에 표시합니다.
		/// </summary>
		/// <param name="httpContext"></param>
		/// <param name="message"></param>
		public static void WriteToPage(this HttpContext httpContext, string message)
		{
			httpContext.GetHttpContextBase().WriteToPage(message);
		}

		/// <summary>
		/// HttpContext 객체의 Response.Write를 이용하여 간단한 로그를 페이지에 표시합니다.
		/// </summary>
		/// <param name="controller"></param>
		/// <param name="message"></param>
		public static void WriteToPage(this ControllerBase controller, string message)
		{
			if (controller == null)
				throw new InvalidOperationException("controller가 null 입니다.");

			if (controller.ControllerContext == null)
				throw new InvalidOperationException("controller.ControllerContext가 null 입니다.");

			controller.ControllerContext.HttpContext.WriteToPage(message);
		}

		/// <summary>
		/// HttpContext 객체의 Response.Write를 이용하여 간단한 로그를 페이지에 표시한 뒤 라인을 분리합니다.
		/// </summary>
		/// <param name="httpContext"></param>
		/// <param name="message"></param>
		public static void WriteLineToPage(this HttpContextBase httpContext, string message)
		{
			httpContext.WriteToPage(string.Format("{0}<br/>", message));
		}

		/// <summary>
		/// HttpContext 객체의 Response.Write를 이용하여 간단한 로그를 페이지에 표시한 뒤 라인을 분리합니다.
		/// </summary>
		/// <param name="httpContext"></param>
		/// <param name="message"></param>
		public static void WriteLineToPage(this HttpContext httpContext, string message)
		{
			httpContext.GetHttpContextBase().WriteLineToPage(message);
		}

		/// <summary>
		/// HttpContext 객체의 Response.Write를 이용하여 간단한 로그를 페이지에 표시한 뒤 라인을 분리합니다.
		/// </summary>
		/// <param name="controller"></param>
		/// <param name="message"></param>
		public static void WriteLineToPage(this ControllerBase controller, string message)
		{
			if (controller == null)
				throw new InvalidOperationException("controller가 null 입니다.");

			if (controller.ControllerContext == null)
				throw new InvalidOperationException("controller.ControllerContext가 null 입니다.");

			controller.ControllerContext.HttpContext.WriteLineToPage(message);
		}
		#endregion
	}
}
