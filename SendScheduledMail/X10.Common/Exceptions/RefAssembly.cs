﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GS.Common.Exceptions
{
	/// <summary>
	/// 참조 어셈블리 일부 정보를 제공 하는 클래스 입니다.
	/// </summary>
	public class RefAssembly
	{
		#region Constructors
		/// <summary>
		/// RefAssembly class의 새 인스턴스를 초기화 합니다.
		/// </summary>
		public RefAssembly(string name, string version)
		{
			Name = name;
			Version = version;
		}
		#endregion

		/// <summary>
		/// Name를 구하거나 설정합니다.
		/// </summary>
		public string Name
		{
			get;
			set;
		}

		/// <summary>
		/// Version를 구하거나 설정합니다.
		/// </summary>
		public string Version
		{
			get;
			set;
		}
	}
}
