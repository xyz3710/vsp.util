﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Reflection;
using System.Runtime.Serialization;
using System.Security.Permissions;
using System.Collections.Specialized;

namespace GS.Common.Exceptions
{
	/// <summary>
	/// 예외 처리를 지원하는 기본 클래스 입니다.
	/// </summary>
	[Serializable]
	public class ExceptionBase : Exception, ISerializable
	{
		#region Constants
		private IEnumerable<RefAssembly> _referencedAssemblies;
		/// <summary>
		/// 원본 소스 문자열을 나타냅니다.
		/// </summary>
		public const string ORIGINAL_SOURCE_STRING = "원본 : ";
		private const string EXCEPTIONBLOCK_FULLNAME_KEY = "ExceptionBlockFullName";
		private const string LOADED_ASSEMBLY_KEY = "LoadedAssembly";
		private const string REFERENCEDASSEMBLIES_KEY = "ReferencedAssemblies";
		#endregion

		#region Constructors
		/// <summary>
		/// ExceptionBase class의 새 인스턴스를 초기화 합니다.
		/// </summary>
		public ExceptionBase()
			: base()
		{
		}

		/// <summary>
		/// ExceptionBase class의 새 인스턴스를 초기화 합니다.
		/// </summary>
		/// <param name="message">예외 메세지</param>
		public ExceptionBase(string message)
			: base(message)
		{
		}

		/// <summary>
		/// ExceptionBase class의 새 인스턴스를 초기화 합니다.
		/// </summary>
		/// <param name="message">예외 메세지</param>
		/// <param name="innerException">발생한 내부 예외</param>
		public ExceptionBase(string message, Exception innerException)
			: base()
		{
		}

		/// <summary>
		/// ExceptionBase class의 새 인스턴스를 초기화 합니다.
		/// </summary>
		/// <param name="loadedAssembly">해당 Block의 assembly 정보</param>
		/// <param name="innerException">발생한 내부 예외</param>
		public ExceptionBase(Assembly loadedAssembly, Exception innerException)
			: this(loadedAssembly, String.Empty, innerException)
		{
		}

		/// <summary>
		/// ExceptionBase class의 새 인스턴스를 초기화 합니다.
		/// </summary>
		/// <param name="loadedAssembly">해당 Block의 assembly 정보</param>
		/// <param name="message">예외 메세지</param>
		/// <param name="innerException">발생한 내부 예외</param>
		public ExceptionBase(Assembly loadedAssembly, string message, Exception innerException)
			: base(message != string.Empty ?
					   message :
					   string.Format(
						   "{0}{1}",
						   ORIGINAL_SOURCE_STRING,
						   string.IsNullOrEmpty(innerException.Source) == true && innerException.InnerException != null ?
							   innerException.InnerException.Source :
							   innerException.Source)
				, innerException)
		{
			ExceptionBlockFullName = loadedAssembly.FullName;
			LoadedAssembly = loadedAssembly;
			innerException.HelpLink = loadedAssembly.GetName().Name;

			if (innerException.Data.Count > 0)
			{
				//innerException 에 추가한 Data를 현재 예외로 이동 시킨다.
				foreach (object key in innerException.Data.Keys)
					Data.Add(key, innerException.Data[key]);

				innerException.Data.Clear();
			}
		}

		/// <summary>
		/// ExceptionBase class의 새 인스턴스를 초기화 합니다.
		/// </summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected ExceptionBase(SerializationInfo info, StreamingContext context)
			: base(info, context)
		{
			ExceptionBlockFullName = info.GetString(EXCEPTIONBLOCK_FULLNAME_KEY);
			LoadedAssembly = info.GetValue(LOADED_ASSEMBLY_KEY, typeof(Assembly)) as Assembly;
			ReferencedAssemblies = info.GetValue(REFERENCEDASSEMBLIES_KEY, typeof(IEnumerable<RefAssembly>)) as IEnumerable<RefAssembly>;
		}
		#endregion

		/// <summary>
		/// ExceptionBlockFullName를 구합니다.
		/// </summary>
		public string ExceptionBlockFullName
		{
			get;
			private set;
		}

		/// <summary>
		/// Load된 Assembly를 구하거나 설정합니다.
		/// </summary>
		public Assembly LoadedAssembly
		{
			get;
			set;
		}

		/// <summary>
		/// LoadedAssembly에 참조된 어셈블리 중 지역 특화된 어셈블리만을 구합니다.
		/// </summary>
		public IEnumerable<RefAssembly> ReferencedAssemblies
		{
			get
			{
				if (_referencedAssemblies != null)
					return _referencedAssemblies;

				_referencedAssemblies = new RefAssembly[0];

				if (LoadedAssembly == null)
					return _referencedAssemblies;

				_referencedAssemblies = LoadedAssembly.GetReferencedAssemblies()
								   .Where(asm => asm.Name.StartsWith("System") == false
										&& asm.Name.StartsWith("Micrisoft") == false
										&& asm.Name.StartsWith("Infragistics") == false
										&& asm.Name.StartsWith("mscorlib") == false)
								   .Select(asm => new RefAssembly(asm.Name, asm.Version.ToString()))
								   .OrderBy(refAsm => refAsm.Name);

				return _referencedAssemblies;
			}
			private set
			{
				_referencedAssemblies = value;
			}
		}

		#region ToString
		/// <summary>
		/// ExceptionBase를 나타내는 String을 반환합니다. 
		/// </summary>
		/// <returns>전체 Property의 값</returns>
		public override string ToString()
		{
			return string.Format("ExceptionBlockFullName:{0}", ExceptionBlockFullName);
		}
		#endregion

		#region ISerializable 멤버

		[SecurityPermission(SecurityAction.Demand, SerializationFormatter = true)]
		void ISerializable.GetObjectData(SerializationInfo info, StreamingContext context)
		{
			// 상위 타입의 필드를 직렬화 한다.
			base.GetObjectData(info, context);

			// 필드를 직렬화 한다.
			info.AddValue(EXCEPTIONBLOCK_FULLNAME_KEY, ExceptionBlockFullName);
			info.AddValue(LOADED_ASSEMBLY_KEY, LoadedAssembly);
			info.AddValue(REFERENCEDASSEMBLIES_KEY, ReferencedAssemblies);
		}

		#endregion
	}
}
