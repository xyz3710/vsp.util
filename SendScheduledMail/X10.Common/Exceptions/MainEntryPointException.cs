﻿// ****************************************************************************************************************** //
//	Domain		:	GS.Common.Exceptions.MainEntryPointException
//	Creator		:	KIMKIWON\xyz37(Kim Ki Won)
//	Create		:	2014년 4월 4일 금요일 오후 12:01
//	Purpose		:	Main Entry Point Program의 예외 처리를 지원하는 클래스 입니다.
// ------------------------------------------------------------------------------------------------------------------ //
//	Modifier	:	
//	Update		:	
//	Changes		:	
// ------------------------------------------------------------------------------------------------------------------ //
//	Comment		:	
// ------------------------------------------------------------------------------------------------------------------ //
//	Reviewer	:	
//	Rev. Date	:	
//	Comment		:	
// ------------------------------------------------------------------------------------------------------------------ //
//	<copyright file="MainEntryPointException.cs" company="(주)가치소프트">
//		Copyright (c) 2014. (주)가치소프트. All rights reserved.
//	</copyright>
// <summary></summary>
// ****************************************************************************************************************** //

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Reflection;

namespace GS.Common.Exceptions
{
	/// <summary>
	/// Main Entry Point Program의 예외 처리를 지원하는 클래스 입니다.
	/// </summary>
	[Serializable]
	public class MainEntryPointException : ExceptionBase
	{
		/// <summary>
		/// ControlsException 클래스의 새 인스턴스를 초기화 합니다.
		/// </summary>
		/// <param name="innerException">내부 exception</param>
		public MainEntryPointException(Exception innerException)
			: base(Assembly.GetCallingAssembly(), innerException)
		{
		}

		/// <summary>
		/// ControlsException 클래스의 새 인스턴스를 초기화 합니다.
		/// </summary>
		/// <param name="loadedAssembly">해당 Block의 assembly 정보</param>
		/// <param name="innerException">내부 exception</param>
		public MainEntryPointException(Assembly loadedAssembly, Exception innerException)
			: base(loadedAssembly, innerException)
		{
		}

		/// <summary>
		/// ControlsException 클래스의 새 인스턴스를 초기화 합니다.
		/// </summary>
		/// <param name="loadedAssembly">해당 Block의 assembly 정보</param>
		/// <param name="message">예외 메세지</param>
		/// <param name="innerException">내부 exception</param>
		public MainEntryPointException(Assembly loadedAssembly, string message, Exception innerException)
			: base(loadedAssembly, message, innerException)
		{
		}
	}
}
