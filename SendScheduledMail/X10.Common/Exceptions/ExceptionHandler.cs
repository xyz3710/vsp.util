﻿/**********************************************************************************************************************/
/*	Domain		:	Okabc.FX.Win.ExceptionHandler
/*	Creator		:	KIMKIWON\xyz37
/*	Create		:	2007년 10월 22일 월요일 오후 4:16
/*	Purpose		:	Okabc WinForm Framework의 Exception을 Handling 합니다.
/*--------------------------------------------------------------------------------------------------------------------*/
/*	Modifier	:	Kim Ki Won
/*	Update		:	2010년 8월 5일 목요일 오전 10:40
/*	Changes		:	Block별 Exception 추가
/*--------------------------------------------------------------------------------------------------------------------*/
/*	Modifier	:	
/*	Update		:	
/*	Changes		:	
/*--------------------------------------------------------------------------------------------------------------------*/
/*	Comment		:	ExceptionBase의 하위 class를 추가하여 특별한 작업이나 
 *					추가 데이터를 처리할 경우 경우 ShowExceptionMessageBox에 추가해준다.
/*--------------------------------------------------------------------------------------------------------------------*/
/*	Reviewer	:	Kim Ki Won
/*	Rev. Date	:	2009년 10월 30일 금요일 오전 10:37
/**********************************************************************************************************************/

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data.Linq;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.Remoting.Messaging;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;
using System.Xml.Linq;
using GS.Common.Utility.Log;
using Microsoft.SqlServer.MessageBox;
using Microsoft.Win32;

namespace GS.Common.Exceptions
{
	/// <summary>
	/// Exception Mesage 처리를 담당하는 클래스 입니다.
	/// </summary>
	[Serializable]
	public class ExceptionHandler
	{
		#region Fields
		private static LogManager logManager = LogManager.RegisterLogPath();
		#endregion

		#region Constructors
		/// <summary>
		/// ExceptionHandler class의 인스턴스를 생성합니다.
		/// </summary>
		/// <param name="owner"></param>
		/// <remarks>ExceptionMessageBoxButtons.RetryCancel을 기본 버튼으로 합니다.</remarks>
		public ExceptionHandler(Form owner)
		{
			Owner = owner;
			ExceptionMessageBoxButtons = ExceptionMessageBoxButtons.RetryCancel;
			DefaultButton = ExceptionMessageBoxDefaultButton.Button2;
		}
		#endregion

		#region ExceptionMessageLoggingHandler
		/// <summary>
		/// ExceptionMessage 의 로깅을 처리할 메서드를 나타냅니다.
		/// </summary>
		/// <param name="e">예외 데이터가 들어 있는 ExceptinoBase 인스턴스입니다.</param>
		public delegate void ExceptionMessageLoggingHandler(ExceptionBase e);

		/// <summary>
		/// ExceptionMessageLogger를 구하거나 설정합니다.
		/// </summary>
		public ExceptionMessageLoggingHandler ExceptionMessageLogger
		{
			get;
			set;
		}
		#endregion

		#region Properties
		/// <summary>
		/// ExceptionHandler의 Owner를 구하거나 설정합니다.
		/// </summary>
		public Form Owner
		{
			get;
			set;
		}

		/// <summary>
		/// ExceptionMessageBoxDefaultButton을 구하거나 설정합니다.
		/// </summary>
		public ExceptionMessageBoxButtons ExceptionMessageBoxButtons
		{
			get;
			set;
		}

		/// <summary>
		/// ExceptionMessageBoxDefaultButton를 구하거나 설정합니다.
		/// </summary>
		public ExceptionMessageBoxDefaultButton DefaultButton
		{
			get;
			set;
		}

		/// <summary>
		/// 내부적으로 사용한 ExceptionMessageBox를 구합니다.
		/// </summary>
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public ExceptionMessageBox InternalExceptionMessageBox
		{
			get;
			private set;
		}

		/// <summary>
		/// MemberName를 구하거나 설정합니다.
		/// </summary>
		/// <value>MemberName를 반환합니다.</value>
		public string MemberName
		{
			get;
			set;
		}

		/// <summary>
		/// SourceFilePath를 구하거나 설정합니다.
		/// </summary>
		/// <value>SourceFilePath를 반환합니다.</value>
		public string SourceFilePath
		{
			get;
			set;
		}

		/// <summary>
		/// SourceLineNumber를 구하거나 설정합니다.
		/// </summary>
		/// <value>SourceLineNumber를 반환합니다.</value>
		public int SourceLineNumber
		{
			get;
			set;
		}
		#endregion

		#region DoException
		/// <summary>
		/// 예외 처리를 User-Friendly한 화면으로 처리합니다.<br/>
		/// </summary>
		/// <param name="exception">실제 프로그램에서 발생한 Exception입니다.</param>
		/// <returns>사용자가 선택한 <see cref="System.Windows.Forms.DialogResult"/>를 반환합니다.</returns>
		public DialogResult ExceptionHandling(Exception exception)
		{
			return ExceptionHandling(string.Empty, exception);
		}

		/// <summary>
		/// 예외 처리를 User-Friendly한 화면으로 처리합니다.<br />
		/// </summary>
		/// <param name="message">사용자가 처리할 수 있는 메세지를 입력합니다.</param>
		/// <param name="exception">실제 프로그램에서 발생한 Exception입니다.</param>
		/// <param name="memberName">Name of the member.</param>
		/// <param name="sourceFilePath">The source file path.</param>
		/// <param name="sourceLineNumber">The source line number.</param>
		/// <returns>사용자가 선택한 <see cref="System.Windows.Forms.DialogResult" />를 반환합니다.</returns>
		public DialogResult ExceptionHandling(
			string message,
			Exception exception,
			[CallerMemberName] string memberName = "",
			[CallerFilePath] string sourceFilePath = "",
			[CallerLineNumber] int sourceLineNumber = 0)
		{
			ExceptionBase exceptionBase = exception as ExceptionBase;

			if (exceptionBase == null)
				exceptionBase = new ExceptionBase(Assembly.GetCallingAssembly(), message, exception);

			if (ExceptionMessageLogger == null)
			{
				ExceptionMessageLogger = DefaultExceptionMessageXmlLogger;
			}
			AsyncCallback asyncCallback = new AsyncCallback(CallbackLogging);

			ExceptionMessageLogger.BeginInvoke(exceptionBase, asyncCallback, exceptionBase);

			return ShowExceptionMessageBox(exceptionBase);
		}

		private void CallbackLogging(IAsyncResult result)
		{
			ExceptionBase exceptionBase = result.AsyncState as ExceptionBase;

			try
			{
				AsyncResult asyncResult = result as AsyncResult;
				ExceptionMessageLoggingHandler loggingHandler = asyncResult.AsyncDelegate as ExceptionMessageLoggingHandler;

				loggingHandler.EndInvoke(asyncResult);
			}
			catch (Exception)
			{
				DefaultExceptionMessageXmlLogger(exceptionBase);
			}
		}
		#endregion

		#region ShowExceptionMessageBox
		private DialogResult ShowExceptionMessageBox(ExceptionBase exceptionBase)
		{
			DialogResult result = DialogResult.Cancel;
			bool shownDialogBox = false;

			if (InternalExceptionMessageBox == null)
				InternalExceptionMessageBox = new ExceptionMessageBox();

			InternalExceptionMessageBox.Message = exceptionBase.InnerException != null ? exceptionBase.InnerException : exceptionBase;
			InternalExceptionMessageBox.Message.Source = exceptionBase.GetType().ToString();
			InternalExceptionMessageBox.Buttons = ExceptionMessageBoxButtons.Custom;
			InternalExceptionMessageBox.Symbol = ExceptionMessageBoxSymbol.Exclamation;
			InternalExceptionMessageBox.DefaultButton = ExceptionMessageBoxDefaultButton.Button2;
			InternalExceptionMessageBox.SetButtonText("응용 프로그램 종료(&A)", "다시 시도(&R)");

			InternalExceptionMessageBox.Show(Owner);
			shownDialogBox = true;

			switch (InternalExceptionMessageBox.CustomDialogResult)
			{
				case ExceptionMessageBoxDialogResult.Button1:
					result = DialogResult.Abort;

					break;
				case ExceptionMessageBoxDialogResult.Button2:
					result = DialogResult.Retry;

					break;
				default:
					result = DialogResult.Cancel;
					break;
			}

			if (shownDialogBox == false)
			{
				result = InternalExceptionMessageBox.Show(Owner);
			}

			return result;
		}
		#endregion

		#region DefaultExceptionMessageLoggers
		/// <summary>
		/// 예외 메세지를 <see cref="LogManager"/>를 이용하여 file에 기본적으로 로깅하는 방법을 제공합니다.
		/// </summary>
		/// <param name="exception"></param>
		public static void DefaultExceptionMessageTextLogger(Exception exception)
		{
			if (exception == null)
				return;

			const string ORIGINAL_SOURCE_STRING = "원본   : ";
			const string MESSAGE_STRING = "메세지 : ";
			const string ADDITIONAL_DATA_STRING = "추가데이터 :";
			const string PROGRAM_LOCATION_STRING = "프로그램위치 :";
			Queue<Exception> queue = new Queue<Exception>();
			StringBuilder messages = new StringBuilder();
			string blockBar = Environment.NewLine.PadLeft(80, '=');
			string bar = Environment.NewLine.PadLeft(80, '-');
			string additionalInfo = string.Empty;
			int seq = 0;

			queue.Enqueue(exception);

			while (queue.Count > 0)
			{
				Exception ex = queue.Dequeue();

				if (ex == null)
					continue;

				//messages.Append(blockBar);
				messages.AppendFormat(string.Format("{0}({1:00})", ex.GetType().FullName, seq++).PadRight(80, '=') + Environment.NewLine);

				if (ex is ExceptionBase)
				{
					ExceptionBase exceptionBase = ex as ExceptionBase;

					//messages.AppendFormat("{0} {1}", ADDITIONAL_DATA_STRING, Environment.NewLine);
					messages.AppendFormat("\tBlockName       : {0}{1}", exceptionBase.ExceptionBlockFullName, Environment.NewLine);
					additionalInfo = exceptionBase.LoadedAssembly.GetName().Name;

					// ExceptionBase.Message에 ExceptionBase에서 만든 원본 문자열이 있을 경우 추가 하지 않는다.
					if (exceptionBase.Message.StartsWith(ExceptionBase.ORIGINAL_SOURCE_STRING) == false)
					{
						//messages.Append(blockBar);
						//messages.AppendFormat("{0}{1}{2}", ORIGINAL_SOURCE_STRING, exceptionBase.GetType().Name, Environment.NewLine);
						messages.AppendFormat("\t{0}{1}{2}", "메세지          : ", exceptionBase.Message, Environment.NewLine);
					}

					// 예외 발생시 참조된 어셈블리 정보를 구한다.
					if (exceptionBase.ReferencedAssemblies.Count() > 0)
					{
						messages.AppendLine("\tRefAssemblies   : ");

						foreach (RefAssembly refAssembly in exceptionBase.ReferencedAssemblies)
							messages.AppendFormat("\t   {0}, {1}{2}", refAssembly.Name, refAssembly.Version, Environment.NewLine);
					}

					if (ex.Data.Count > 0)
					{
						messages.Append(bar);

						//if (exceptionBase.Message.StartsWith(ExceptionBase.ORIGINAL_SOURCE_STRING) == false)
						messages.AppendFormat("{0}{1}", ADDITIONAL_DATA_STRING, Environment.NewLine);

						foreach (object key in ex.Data.Keys)
							messages.AppendFormat("\t{0}\t: {1}{2}", key, ex.Data[key], Environment.NewLine);
					}
				}
				else
				{
					messages.AppendFormat("{0}{1}{2}", ORIGINAL_SOURCE_STRING, ex.Source ?? ex.HelpLink, Environment.NewLine);
					messages.AppendFormat("{0}{1}{2}", MESSAGE_STRING, ex.Message, Environment.NewLine);

					if (ex.Data.Count > 0)
					{
						messages.Append(bar);
						messages.AppendFormat("{0}{1}", ADDITIONAL_DATA_STRING, Environment.NewLine);

						foreach (object key in ex.Data.Keys)
							messages.AppendFormat("\t{0}\t: {1}{2}", key, ex.Data[key], Environment.NewLine);
					}

					if (additionalInfo == string.Empty)
						additionalInfo = string.IsNullOrEmpty(ex.Source) == false ? ex.Source : exception.HelpLink;
					//else
					//	additionalInfo = ex.Source;
				}

				if (string.IsNullOrEmpty(ex.StackTrace) == false)
				{
					messages.Append(bar);
					messages.AppendFormat("{0}{1}", PROGRAM_LOCATION_STRING, Environment.NewLine);
					messages.AppendLine(ex.StackTrace);
					//messages.AppendFormat("{0}{1}", ex.StackTrace, Environment.NewLine);
				}

				if (ex.InnerException != null)
					queue.Enqueue(ex.InnerException);
			}

			logManager.Write(LogTypes.Exception, additionalInfo, messages.ToString());
		}

		/// <summary>
		/// 예외 메세지를 <see cref="LogManager"/>를 이용하여 Xml에 기본적으로 로깅하는 방법을 제공합니다.
		/// </summary>
		/// <param name="exception"></param>
		public static void DefaultExceptionMessageXmlLogger(Exception exception)
		{
			if (exception == null)
				return;

			const string BLOCKNAME_KEY = "blockName";
			const string REFERENCEDASSEMBLIES_KEY = "referencedAssemblies";
			const string SOURCE_KEY = "source";
			const string MESSAGE_KEY = "message";
			const string DATA_KEY = "data";
			const string PROGRAM_LOCATION_KEY = "programLocation";
			const string LOCATION_KEY = "location";
			const string TYPE_KEY = "type";
			const string SEQ_KEY = "seq";
			const string FILE_KEY = "file";
			const string LINE_KEY = "line";
			const string COLUMN_KEY = "column";
			string additionalInfo = string.Empty;
			int seq = 0;
			Queue<Exception> queue = new Queue<Exception>();
			XDocument messages = new XDocument(
					new XDeclaration("1.0", "UTF-8", "yes"),
						new XElement("exceptions")
			);

			queue.Enqueue(exception);

			while (queue.Count > 0)
			{
				Exception ex = queue.Dequeue();

				if (ex == null)
					continue;

				XElement exceptionXE = new XElement(
						ex.GetType().Name,
							new XAttribute(TYPE_KEY, ex.GetType()),
							new XAttribute(SEQ_KEY, seq++)
				);

				if (ex is ExceptionBase)
				{
					ExceptionBase exceptionBase = ex as ExceptionBase;

					exceptionXE.Add(new XElement(BLOCKNAME_KEY, exceptionBase.ExceptionBlockFullName));
					additionalInfo = exceptionBase.LoadedAssembly.GetName().Name;

					// ExceptionBase.Message에 ExceptionBase에서 만든 원본 문자열이 있을 경우 추가 하지 않는다.
					if (exceptionBase.Message.StartsWith(ExceptionBase.ORIGINAL_SOURCE_STRING) == false)
						exceptionXE.Add(new XElement(MESSAGE_KEY, exceptionBase.Message));

					// 예외 발생시 참조된 어셈블리 정보를 구한다.
					if (exceptionBase.ReferencedAssemblies.Count() > 0)
					{
						exceptionXE.Add(new XElement(REFERENCEDASSEMBLIES_KEY,
								exceptionBase.ReferencedAssemblies
									.Select(refAsm => new XElement(refAsm.Name, refAsm.Version))));
					}

					if (ex.Data.Count > 0)
					{
						XElement dataXE = new XElement(DATA_KEY);

						//if (exceptionBase.Message.StartsWith(ExceptionBase.ORIGINAL_SOURCE_STRING) == false)
						exceptionXE.Add(dataXE);

						foreach (object key in ex.Data.Keys)
							dataXE.Add(new XElement(key.ToString(), new XAttribute(TYPE_KEY, ex.Data[key].GetType()), ex.Data[key]));
					}
				}
				else
				{
					exceptionXE.Add(new XElement(SOURCE_KEY, ex.Source ?? ex.HelpLink));
					exceptionXE.Add(new XElement(MESSAGE_KEY, ex.Message));

					if (ex.Data.Count > 0)
					{
						XElement dataXE = new XElement(DATA_KEY);

						exceptionXE.Add(dataXE);

						foreach (object key in ex.Data.Keys)
							dataXE.Add(new XElement(key.ToString(), new XAttribute(TYPE_KEY, ex.Data[key].GetType()), ex.Data[key]));
					}

					if (additionalInfo == string.Empty)
						additionalInfo = string.IsNullOrEmpty(ex.Source) == false ? ex.Source : exception.HelpLink;
				}

				if (string.IsNullOrEmpty(ex.StackTrace) == false)
				{
					XElement programLocationXE = new XElement(PROGRAM_LOCATION_KEY);

					exceptionXE.Add(programLocationXE);

					StackTrace stackTrace = new StackTrace(ex, true);

					foreach (StackFrame stackFrame in stackTrace.GetFrames())
					{
						MethodBase method = stackFrame.GetMethod();
						string location = string.Empty;

						if (method.DeclaringType != null)
						{
							location = string.Format(
										"{0}.{1}({2})",
										method.DeclaringType.FullName,
										method.Name,
										method.GetParameters()
										.Aggregate<ParameterInfo, string>(
										string.Empty,
										(acc, next) =>
										{
											if (acc == string.Empty)
												return next.ToString();

											return string.Format(
													   "{0} {1}{2}{3}",
													   next.ParameterType.Name,
													   next.Name,
													   ", ",
													   next.ToString());
										})
									);
						}

						if (stackFrame.GetFileName() == null)
						{
							programLocationXE.Add(new XElement(LOCATION_KEY, location));
						}
						else
						{
							programLocationXE.Add(new XElement(LOCATION_KEY,
								new XAttribute(FILE_KEY, stackFrame.GetFileName()),
								new XAttribute(LINE_KEY, stackFrame.GetFileLineNumber()),
								new XAttribute(COLUMN_KEY, stackFrame.GetFileColumnNumber()),
									location));
						}
					}
				}

				if (ex.InnerException != null)
					queue.Enqueue(ex.InnerException);

				messages.Root.Add(exceptionXE);
			}

			logManager.Write(LogTypes.Exception, additionalInfo, messages);
		}

		#region GetXmlTag
		private static string GetXmlTag(string key, string value)
		{
			Dictionary<string, string> attributes = null;

			return GetXmlTag(key, value, attributes);
		}

		private static string GetXmlTag(string key, string value, string attributeKey, string attributeValue)
		{
			Dictionary<string, string> attributes = new Dictionary<string, string>();

			attributes.Add(attributeKey, attributeValue);

			return GetXmlTag(key, value, attributes);
		}

		private static string GetXmlTag(string key, string value, Dictionary<string, string> attributes)
		{
			if (attributes == null)
				return string.Format("<{0}>{1}</{0}>", key, value.ReplaceByXmlRule());
			else
			{
				StringBuilder attr = new StringBuilder();

				foreach (string attKey in attributes.Keys)
					attr.Append(string.Format("{0}=\"{1}\" ", attKey, attributes[attKey].ReplaceByXmlRule()));

				return string.Format(
						   "<{0} {2}>{1}</{0}>",
						   key,
						   value.ReplaceByXmlRule(),
						   attr.ToString(0, attr.Length - 1));
			}
		}
		#endregion
		#endregion

		#region DoException
		/// <summary>
		/// ControlsCustom block에서 발생한 예외를 처리합니다.
		/// </summary>
		/// <param name="exceptionBase">발생한 예외</param>
		/// <returns>사용자가 선택한 <see cref="System.Windows.Forms.DialogResult"/>를 반환합니다.</returns>
		public static DialogResult DoException(ExceptionBase exceptionBase)
		{
			ExceptionHandler exceptionHandler = new ExceptionHandler(null);

			return exceptionHandler.ExceptionHandling(exceptionBase.InnerException);
		}

		/// <summary>
		/// ControlsCustom block에서 발생한 예외를 처리합니다.
		/// </summary>
		/// <param name="exception">발생한 예외</param>
		/// <returns>사용자가 선택한 <see cref="System.Windows.Forms.DialogResult"/>를 반환합니다.</returns>
		public static DialogResult DoException(Exception exception)
		{
			string message = String.Empty;

			return ExceptionHandler.DoException(message, exception);
		}

		/// <summary>
		/// ControlsCustom block에서 발생한 예외를 처리합니다.
		/// </summary>
		/// <param name="message">예외 메세지</param>
		/// <param name="exception">발생한 예외</param>
		/// <returns>사용자가 선택한 <see cref="System.Windows.Forms.DialogResult"/>를 반환합니다.</returns>
		public static DialogResult DoException(string message, Exception exception)
		{
			return ExceptionHandler.DoException(new ExceptionBase(
											 Assembly.GetCallingAssembly(),
											 message,
											 exception));
		}
		#endregion

		#region MainEntryPointException(DoUnhandledException, DoApplicationThreadException)
		/// <summary>
		/// 처리되지 않은 Exception을 처리합니다.<br />
		/// 응용 프로그램 종료(Abort)와 취소(Cancel) 버튼이 나타납니다.
		/// </summary>
		/// <param name="exception">The exception.</param>
		/// <param name="memberName">Name of the member.</param>
		/// <param name="sourceFilePath">The source file path.</param>
		/// <param name="sourceLineNumber">The source line number.</param>
		/// <returns>응용 프로그램 종료를 선택하면 DialogResult.Abort를 그렇지 않으면 DialogResult.Cancel을 반환합니다.</returns>
		public static DialogResult DoUnhandledException(
			Exception exception,
			[CallerMemberName] string memberName = "",
			[CallerFilePath] string sourceFilePath = "",
			[CallerLineNumber] int sourceLineNumber = 0)
		{
			MainEntryPointException loaderException = new MainEntryPointException(
															  Assembly.GetCallingAssembly(),
															  "처리되지 않는 오류가 발생했습니다.(CurrentDomain.UnhandledException)",
															  exception);
			ExceptionHandler exceptionHandler = new ExceptionHandler(null)
			{
				InternalExceptionMessageBox = new ExceptionMessageBox(loaderException),
				ExceptionMessageBoxButtons = ExceptionMessageBoxButtons.Custom,
				DefaultButton = ExceptionMessageBoxDefaultButton.Button1,
				MemberName = memberName,
				SourceFilePath = sourceFilePath,
				SourceLineNumber = sourceLineNumber,
			};
			exceptionHandler.InternalExceptionMessageBox.SetButtonText("응용 프로그램 종료(&A)", "취소(&C)");

			DialogResult result = DialogResult.Cancel;

			exceptionHandler.ExceptionHandling(loaderException);

			if (exceptionHandler.InternalExceptionMessageBox.CustomDialogResult == ExceptionMessageBoxDialogResult.Button1)
				result = DialogResult.Abort;

			return result;
		}

		/// <summary>
		/// 처리되지 않은 Exception을 처리합니다.<br />
		/// 응용 프로그램 종료(Abort)와 계속(Retry) 버튼이 나타납니다.
		/// </summary>
		/// <param name="exception">The exception.</param>
		/// <param name="memberName">Name of the member.</param>
		/// <param name="sourceFilePath">The source file path.</param>
		/// <param name="sourceLineNumber">The source line number.</param>
		/// <returns>응용 프로그램 종료를 선택하면 DialogResult.Abort를 그렇지 않으면 DialogResult.Retry를 반환합니다.</returns>
		public static DialogResult DoApplicationThreadException(
			Exception exception,
			[CallerMemberName] string memberName = "",
			[CallerFilePath] string sourceFilePath = "",
			[CallerLineNumber] int sourceLineNumber = 0)
		{
			MainEntryPointException loaderException = new MainEntryPointException(
															  Assembly.GetCallingAssembly(),
															  "처리되지 않는 오류가 발생했습니다.(Application.ThreadException)",
															  exception);
			ExceptionHandler exceptionHandler = new ExceptionHandler(null)
			{
				InternalExceptionMessageBox = new ExceptionMessageBox(loaderException),
				ExceptionMessageBoxButtons = ExceptionMessageBoxButtons.Custom,
				DefaultButton = ExceptionMessageBoxDefaultButton.Button2,
				MemberName = memberName,
				SourceFilePath = sourceFilePath,
				SourceLineNumber = sourceLineNumber,
			};
			exceptionHandler.InternalExceptionMessageBox.SetButtonText("응용 프로그램 종료(&A)", "계속(&C)");

			DialogResult result = DialogResult.Retry;

			exceptionHandler.ExceptionHandling(loaderException);

			if (exceptionHandler.InternalExceptionMessageBox.CustomDialogResult == ExceptionMessageBoxDialogResult.Button1)
				result = DialogResult.Abort;

			return result;
		}
		#endregion
	}
}
