﻿/**********************************************************************************************************************/
/*	Domain		:	System.Data.Entity.IAttributeConvention
/*	Creator		:	KIM-KIWON\xyz37(김기원)
/*	Create		:	2012년 8월 14일 화요일 오전 11:10
/*	Purpose		:	
/*--------------------------------------------------------------------------------------------------------------------*/
/*	Modifier	:	
/*	Update		:	
/*	Changes		:	
/*--------------------------------------------------------------------------------------------------------------------*/
/*	Comment		:	현재 EF 5 CTP 버전에서는 AttributeConfigurationConvention class를 internal abstract로 선언되어서 꼼수로 해결
 *					http://dotnetspeak.com/index.php/2011/03/custom-conventions-in-entity-framework-code-first-v-4-1/
/*--------------------------------------------------------------------------------------------------------------------*/
/*	Reviewer	:	Kim Ki Won
/*	Rev. Date	:	
/**********************************************************************************************************************/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Reflection;
using System.Data.Entity.ModelConfiguration.Configuration;

namespace System.Data.Entity
{
	/// <summary>
	/// 
	/// </summary>
	public interface IAttributeConvention : IConvention
	{
		/// <summary>
		/// Applies the configuration.
		/// </summary>
		/// <param name="memberInfo">The member info.</param>
		/// <param name="propertyConfiguration">The property configuration.</param>
		/// <param name="attrribute">The attrribute.</param>
		void ApplyConfiguration(
			MemberInfo memberInfo,
			PrimitivePropertyConfiguration propertyConfiguration,
			Attribute attrribute);

		/// <summary>
		/// Gets the type of the property configuration.
		/// </summary>
		/// <value>
		/// The type of the property configuration.
		/// </value>
		Type PropertyConfigurationType
		{
			get;
		}

		/// <summary>
		/// Gets the type of the attribute.
		/// </summary>
		/// <value>
		/// The type of the attribute.
		/// </value>
		Type AttributeType
		{
			get;
		}
	}

	/// <summary>
	/// 
	/// </summary>
	/// <typeparam name="TMemberInfo">The type of the member info.</typeparam>
	/// <typeparam name="TPropertyConfiguration">The type of the property configuration.</typeparam>
	/// <typeparam name="TAttribute">The type of the attribute.</typeparam>
	public abstract class AttributeConfigurationConvention<TMemberInfo, TPropertyConfiguration, TAttribute>
		: IAttributeConvention
		where TMemberInfo : MemberInfo
		where TPropertyConfiguration : PrimitivePropertyConfiguration
		where TAttribute : Attribute
	{
		/// <summary>
		/// Applies the configuration.
		/// </summary>
		/// <param name="memberInfo">The member info.</param>
		/// <param name="propertyConfiguration">The property configuration.</param>
		/// <param name="attribute">The attribute.</param>
		public void ApplyConfiguration(
			MemberInfo memberInfo,
			PrimitivePropertyConfiguration propertyConfiguration,
			Attribute attribute)
		{
			Apply((TMemberInfo)memberInfo, (TPropertyConfiguration)propertyConfiguration, (TAttribute)attribute);
		}

		/// <summary>
		/// Applies the specified member info.
		/// </summary>
		/// <param name="memberInfo">The member info.</param>
		/// <param name="propertyConfiguration">The property configuration.</param>
		/// <param name="attrribute">The attrribute.</param>
		protected abstract void Apply(
			TMemberInfo memberInfo,
			TPropertyConfiguration propertyConfiguration,
			TAttribute attrribute);

		/// <summary>
		/// Gets the type of the property configuration.
		/// </summary>
		/// <value>
		/// The type of the property configuration.
		/// </value>
		public Type PropertyConfigurationType
		{
			get
			{
				return typeof(TPropertyConfiguration);
			}
		}

		/// <summary>
		/// Gets the type of the attribute.
		/// </summary>
		/// <value>
		/// The type of the attribute.
		/// </value>
		public Type AttributeType
		{
			get
			{
				return typeof(TAttribute);
			}
		}
	}
}
