﻿/**********************************************************************************************************************/
/*	Domain		:	System.Data.Entity.UnicodeAttribute
/*	Creator		:	KIM-KIWON\xyz37(김기원)
/*	Create		:	2012년 8월 14일 화요일 오전 11:11
/*	Purpose		:	Entity Framework의 컬럼에 유니코드를 설정합니다.
/*--------------------------------------------------------------------------------------------------------------------*/
/*	Modifier	:	
/*	Update		:	
/*	Changes		:	
/*--------------------------------------------------------------------------------------------------------------------*/
/*	Comment		:	
/*--------------------------------------------------------------------------------------------------------------------*/
/*	Reviewer	:	Kim Ki Won
/*	Rev. Date	:	
/**********************************************************************************************************************/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Reflection;
using System.Data.Entity.ModelConfiguration.Configuration;
using System.Data.Entity.ModelConfiguration.Conventions;

namespace System.Data.Entity
{
	/// <summary>
	/// Entity Framework의 컬럼에 유니코드를 설정합니다.
	/// </summary>
	[AttributeUsage(AttributeTargets.Property, AllowMultiple = false)]
	public class UnicodeAttribute : Attribute
	{
		/// <summary>
		/// Initializes a new instance of the <see cref="UnicodeAttribute"/> class.
		/// </summary>
		/// <param name="isUnicode">if set to <c>true</c> [is unicode].</param>
		public UnicodeAttribute(bool isUnicode)
		{
			IsUnicode = isUnicode;
		}

		/// <summary>
		/// 유니코드를 사용할지 여부를 구합니다.
		/// </summary>
		/// <value>
		/// 	<c>true</c> if this instance is unicode; otherwise, <c>false</c>.
		/// </value>
		public bool IsUnicode
		{
			get;
			private set;
		}
	}

	/// <summary>
	/// 
	/// </summary>
	public class UnicodeAttributeConvention : AttributeConfigurationConvention<PropertyInfo, StringPropertyConfiguration, UnicodeAttribute>
	{
		/// <summary>
		/// Applies the specified member info.
		/// </summary>
		/// <param name="memberInfo">The member info.</param>
		/// <param name="configuration">The configuration.</param>
		/// <param name="attribute">The attribute.</param>
		protected override void Apply(PropertyInfo memberInfo, StringPropertyConfiguration configuration, UnicodeAttribute attribute)
		{
			configuration.IsUnicode(attribute.IsUnicode);
		}
	}
}
