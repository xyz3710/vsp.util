/**********************************************************************************************************************/
/*	Domain		:	System.Data.Entity.UnicodeAttribute
/*	Creator		:	KIM-KIWON\xyz37(김기원)
/*	Create		:	2012년 8월 14일 화요일 오전 11:11
/*	Purpose		:	Entity Framework의 Decimal 컬럼에 정확도를 설정합니다.
/*--------------------------------------------------------------------------------------------------------------------*/
/*	Modifier	:	
/*	Update		:	
/*	Changes		:	
/*--------------------------------------------------------------------------------------------------------------------*/
/*	Comment		:	
/*--------------------------------------------------------------------------------------------------------------------*/
/*	Reviewer	:	Kim Ki Won
/*	Rev. Date	:	
/**********************************************************************************************************************/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Reflection;
using System.Data.Entity.ModelConfiguration.Configuration;
using System.Data.Entity.ModelConfiguration.Conventions;

namespace System.Data.Entity
{
	/// <summary>
	/// Entity Framework의 Decimal 컬럼에 정확도를 설정합니다.
	/// </summary>
	[AttributeUsage(AttributeTargets.Property, AllowMultiple = false)]
	public class DecimalPrecisionAttribute : Attribute
	{
		/// <summary>
		/// Initializes a new instance of the <see cref="DecimalPrecisionAttribute"/> class.
		/// </summary>
		/// <param name="precision">The precision.</param>
		/// <param name="scale">The scale.</param>
		public DecimalPrecisionAttribute(int precision, int scale)
		{
			Precision = precision;
			Scale = scale;
		}

		/// <summary>
		/// 정밀도를 구합니다.
		/// </summary>
		public int Precision
		{
			get;
			private set;
		}

		/// <summary>
		/// 소수점 이하 자리수를 구합니다.
		/// </summary>
		public int Scale
		{
			get;
			private set;
		}
	}

	/// <summary>
	/// 
	/// </summary>
	public class DecimalPrecisionAttributeConvention : AttributeConfigurationConvention<PropertyInfo, DecimalPropertyConfiguration, DecimalPrecisionAttribute>
	{
		/// <summary>
		/// Applies the specified member info.
		/// </summary>
		/// <param name="memberInfo">The member info.</param>
		/// <param name="configuration">The configuration.</param>
		/// <param name="attribute">The attribute.</param>
		protected override void Apply(PropertyInfo memberInfo, DecimalPropertyConfiguration configuration, DecimalPrecisionAttribute attribute)
		{
			configuration.HasPrecision(Convert.ToByte(attribute.Precision), Convert.ToByte(attribute.Scale));
		}
	}
	public class MyKeyConvention : IConfigurationConvention<PropertyInfo, EntityTypeConfiguration>
	{
		
	}
}
