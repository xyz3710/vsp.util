﻿/**********************************************************************************************************************/
/*	Domain		:	X10.Common.MobileHelper
/*	Creator		:	KIMKIWON\xyz37(김기원)
/*	Create		:	2011년 10월 13일 목요일 오후 1:26
/*	Purpose		:	웹의 추가적인 기능을 제공합니다.
/*--------------------------------------------------------------------------------------------------------------------*/
/*	Modifier	:	
/*	Update		:	
/*	Changes		:	
/*--------------------------------------------------------------------------------------------------------------------*/
/*	Comment		:	
/*--------------------------------------------------------------------------------------------------------------------*/
/*	Reviewer	:	Kim Ki Won
/*	Rev. Date	:	
/**********************************************************************************************************************/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;

namespace X10.Common
{
	/// <summary>
	/// 웹의 추가적인 기능을 제공합니다.
	/// </summary>
	public class MobileHelper
	{
		/// <summary>
		/// 현재의 HttpContext를 구합니다.
		/// </summary>
		public static HttpContext HttpContext
		{
			get
			{
				return HttpContext.Current;
			}
		}

		/// <summary>
		/// 현재의 HttpRequest를 구합니다.
		/// </summary>
		public static HttpRequest Request
		{
			get
			{
				return HttpContext.Request;
			}
		}

		/// <summary>
		/// 현재의 HttpResponse를 구합니다.
		/// </summary>
		public static HttpResponse Response
		{
			get
			{
				return HttpContext.Response;
			}
		}

		/// <summary>
		/// 51 Degrees를 사용해서 접속 장비가 MobileDevice 인지를 구별합니다.
		/// </summary>
		/// <returns></returns>
		public static bool IsMobileDevice
		{
			get
			{
				bool isMobileDevice = false;

				if (Request.UserAgent != null)
					isMobileDevice = Convert.ToBoolean(Request.Browser[GlobalConstants.FIFTYONE_IS_WIRELESS_DEVICE_KEY]);

				return isMobileDevice;
			}
		}

		/// <summary>
		/// 51 Degrees를 사용해서 접속 장비가 타블렛 인지를 구별합니다.
		/// </summary>
		/// <returns></returns>
		public static bool IsTablet
		{
			get
			{
				bool isTablet = false;

				if (Request.UserAgent != null)
					isTablet = Convert.ToBoolean(Request.Browser[GlobalConstants.FIFTYONE_IS_TABLET_KEY]);

				return isTablet;
			}
		}

		/// <summary>
		/// 51 Degrees를 사용해서 Mobile 장비의 브렌드 이름을 구합니다.
		/// </summary>
		/// <returns></returns>
		public static string MobileBrandName
		{
			get
			{
				string mobileBrandName = string.Empty;

				if (Request.UserAgent != null)
					mobileBrandName = Convert.ToString(Request.Browser[GlobalConstants.FIFTYONE_BRAND_NAME_KEY]);

				return mobileBrandName;
			}
		}

		/// <summary>
		/// 51 Degrees를 사용해서 Mobile 장비의 모델 이름을 구합니다.
		/// </summary>
		/// <returns></returns>
		public static string MobileModelName
		{
			get
			{
				string mobileModelName = string.Empty;

				if (Request.UserAgent != null)
					mobileModelName = Convert.ToString(Request.Browser[GlobalConstants.FIFTYONE_MODEL_NAME_KEY]);

				return mobileModelName;
			}
		}
	}
}
