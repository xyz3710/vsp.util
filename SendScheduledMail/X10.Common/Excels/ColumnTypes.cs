using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Diagnostics;
using NPOI.SS.UserModel;

namespace X10.Common.Excels
{
	/// <summary>
	/// Enum ColumnTypes
	/// </summary>
	public enum ColumnTypes
	{
		/// <summary>
		/// The string type
		/// </summary>
		String = 0,
		/// <summary>
		/// The decimal type
		/// </summary>
		Decimal,
		/// <summary>
		/// The number type
		/// </summary>
		Number,
		/// <summary>
		/// The boolean type
		/// </summary>
		Boolean,
		/// <summary>
		/// The date type
		/// </summary>
		Date,
	}
}

