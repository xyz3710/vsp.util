using System;
using System.Collections.Generic;
using System.Data;
using System.Data.OleDb;
using System.Drawing;
using System.IO;
using System.Linq;

namespace X10.Common.Excels
{
	/// <summary>
	/// Class SheetInfo.
	/// </summary>
	/// <typeparam name="T"></typeparam>
	public class SheetInfo<T>
	{
		/// <summary>
		/// Summary for SheetInfo
		/// </summary>
		/// <param name="useAlternativeRow">행의 색이 번갈아 나올지 여부</param>
		/// <param name="oddRowColorIndex">홀수행의 색(http://www.jakartaproject.com/upload/editor/200503141110805468928_jpg의 컬러 챠트 참고)</param>
		/// <param name="eventRowColorIndex">짝수행의 색(http://www.jakartaproject.com/upload/editor/200503141110805468928_jpg의 컬러 챠트 참고)</param>
		/// <param name="headerHeightMultiple">헤더의 높이 배수</param>
		/// <param name="headerWrapText">헤더의 텍스트를 여러행으로 나누어 표시할지 여부</param>
		/// <param name="cellWrapText">텍스트를 여러행으로 나누어 표시할지 여부</param>
		/// <param name="cellShrinkToFit">텍스트 크기를 셀에 맞출지 여부</param>
		/// <param name="autoSizeColumn">컬럼을 자동 조절할 지</param>
		public SheetInfo(
			bool useAlternativeRow = false,
			short oddRowColorIndex = 9,
			short eventRowColorIndex = 31,
			short headerHeightMultiple = 2,
			bool headerWrapText = false,
			bool cellWrapText = true,
			bool cellShrinkToFit = false,
			bool autoSizeColumn = false)
		{
			UseAlternativeRow = useAlternativeRow;
			OddRowColorIndex = oddRowColorIndex;
			EventRowColorIndex = eventRowColorIndex;
			HeaderHeightMultiple = headerHeightMultiple;
			HeaderWrapText = headerWrapText;
			CellWrapText = cellWrapText;
			CellShrinkToFit = cellShrinkToFit;
			AutoSizeColumn = autoSizeColumn;
		}

		/// <summary>
		/// 행의 색이 번갈아 나올지 여부
		/// </summary>
		public bool UseAlternativeRow
		{
			get;
			set;
		}

		/// <summary>
		/// 홀수행의 색(http://www.jakartaproject.com/upload/editor/200503141110805468928_jpg의 컬러 챠트 참고)
		/// </summary>
		public short OddRowColorIndex
		{
			get;
			set;
		}

		/// <summary>
		/// 짝수행의 색(http://www.jakartaproject.com/upload/editor/200503141110805468928_jpg의 컬러 챠트 참고)
		/// </summary>
		public short EventRowColorIndex
		{
			get;
			set;
		}

		/// <summary>
		/// 헤더의 높이 배수
		/// </summary>
		public short HeaderHeightMultiple
		{
			get;
			set;
		}

		/// <summary>
		/// 헤더의 텍스트를 여러행으로 나누어 표시할지 여부
		/// </summary>
		public bool HeaderWrapText
		{
			get;
			set;
		}

		/// <summary>
		/// 텍스트를 여러행으로 나누어 표시할지 여부
		/// </summary>
		public bool CellWrapText
		{
			get;
			set;
		}

		/// <summary>
		/// 텍스트 크기를 셀에 맞출지 여부
		/// </summary>
		public bool CellShrinkToFit
		{
			get;
			set;
		}

		/// <summary>
		/// 컬럼을 자동 조절할 지 
		/// </summary>
		public bool AutoSizeColumn
		{
			get;
			set;
		}
	}
}
