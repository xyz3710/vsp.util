﻿/**********************************************************************************************************************/
/*	Domain		:	X10.Common.Excels.ExcelColumnInfo
/*	Creator		:	KIM-KIWON\xyz37(김기원)
/*	Create		:	2012년 10월 18일 목요일 오후 7:51
/*	Purpose		:	Grid에서 컬럼을 구한 뒤 컬럼 정보를 전송하는 DTO 객체 입니다.
/*--------------------------------------------------------------------------------------------------------------------*/
/*	Modifier	:	
/*	Update		:	
/*	Changes		:	
/*--------------------------------------------------------------------------------------------------------------------*/
/*	Comment		:	
/*--------------------------------------------------------------------------------------------------------------------*/
/*	Reviewer	:	Kim Ki Won
/*	Rev. Date	:	
/**********************************************************************************************************************/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Diagnostics;
using NPOI.SS.UserModel;

namespace X10.Common.Excels
{
	/// <summary>
	/// Grid에서 컬럼을 구한 뒤 컬럼 정보를 전송하는 DTO 객체 입니다.
	/// </summary>
	public class ColumnCollection
	{
		/// <summary>
		/// 컬럼 항목을 구합니다.
		/// </summary>
		public IEnumerable<ExcelColumnInfo> Columns
		{
			get;
			set;
		}

		/// <summary>
		/// Dictionary를 구합니다.
		/// </summary>
		/// <param name="includeHiddenColumn"></param>
		public ExcelColumnInfo[] ConvertToArray(bool includeHiddenColumn = false)
		{
			if (includeHiddenColumn == true)
				return Columns.ToArray();
			else
				return Columns.Where(x => x.Hidden == false)
					.ToArray();
		}
	}

	/// <summary>
	/// 엑셀 익스포트용 컬럼 정보를 제공하는 DTO 객체 입니다.
	/// </summary>
	[DebuggerDisplay("{Key}, {TypeName}, Hidden:{Hidden}, {Width}px, Title:{Title}, Align:{Align}", Name = "ExcelColumn")]
	public class ExcelColumnInfo
	{
		#region Constructor
		/// <summary>
		/// ExcelColumnInfo class의 새 인스턴스를 초기화 합니다.
		/// </summary>
		public ExcelColumnInfo()
		{
			VerticalAlignment = NPOI.SS.UserModel.VerticalAlignment.Center;
			DataFormatString = string.Empty;
			Formula = string.Empty;
		}
		#endregion

		/// <summary>
		/// Id를 구하거나 설정합니다.
		/// </summary>
		public string Key
		{
			get;
			set;
		}

		/// <summary>
		/// TypeName를 구하거나 설정합니다.
		/// </summary>
		public string TypeName
		{
			get;
			set;
		}

		/// <summary>
		/// Title를 구하거나 설정합니다.
		/// </summary>
		public string Title
		{
			get;
			set;
		}

		/// <summary>
		/// Width를 구하거나 설정합니다.
		/// </summary>
		public int Width
		{
			get;
			set;
		}

		/// <summary>
		/// Hidden 여부를 구하거나 설정합니다.
		/// </summary>
		public bool Hidden
		{
			get;
			set;
		}

		/// <summary>
		/// Align를 구하거나 설정합니다.
		/// </summary>
		public string Align
		{
			get;
			set;
		}

		/// <summary>
		/// VAlign를 구하거나 설정합니다.
		/// </summary>
		public string VAlign
		{
			get;
			set;
		}

		/// <summary>
		/// Align 문자열로 HorizontalAlignment를 구하거나 설정합니다.
		/// </summary>
		public HorizontalAlignment HorizontalAlignment
		{
			get
			{
				HorizontalAlignment horizontalAlignment = HorizontalAlignment.Left;

				switch (Align)
				{
					case "center":
						horizontalAlignment = HorizontalAlignment.Center;

						break;
					case "right":
						horizontalAlignment = HorizontalAlignment.Right;

						break;
				}

				return horizontalAlignment;
			}
			set
			{
				Align = value.ToString().ToLower();
			}
		}

		/// <summary>
		/// VerticalAlignment를 구하거나 설정합니다.
		/// </summary>
		/// <value>VerticalAlignment를 반환합니다.</value>
		public VerticalAlignment VerticalAlignment
		{
			get;
			set;
		}

		/// <summary>
		/// IsLocked를 구하거나 설정합니다.
		/// </summary>
		/// <value>IsLocked를 반환합니다.</value>
		public bool IsLocked
		{
			get;
			set;
		}

		/// <summary>
		/// DataFormatString를 구하거나 설정합니다.
		/// <remarks>숫자등은 "[=0]0;#,###.0"로 표현한다.</remarks>
		/// </summary>
		/// <value>DataFormatString를 반환합니다.</value>
		public string DataFormatString
		{
			get;
			set;
		}

		/// <summary>
		/// Formula를 구하거나 설정합니다.
		/// </summary>
		/// <value>Formula를 반환합니다.</value>
		public string Formula
		{
			get;
			set;
		}

		#region ToString
		/// <summary>
		/// ExcelColumn를 나타내는 String을 반환합니다. 
		/// </summary>
		/// <returns>전체 Property의 값</returns>
		public override string ToString()
		{
			return string.Format("Key:{0}, TypeName:{1}, Title:{2}, Width:{3}, Hidden:{4}, Align:{5}, HorizontalAlignment:{6}",
					Key, TypeName, Title, Width, Hidden, Align, HorizontalAlignment);
		}
		#endregion

		/// <summary>
		/// Clones this instance.
		/// </summary>
		/// <returns>ExcelColumnInfo.</returns>
		public ExcelColumnInfo Clone()
		{
			return new ExcelColumnInfo
			{
				Key = Key,
				TypeName = TypeName,
				Title = Title,
				Width = Width,
				Hidden = Hidden,
				HorizontalAlignment = HorizontalAlignment,
				VerticalAlignment = VerticalAlignment,
			};
		}

		/// <summary>
		/// Defaults this instance.
		/// </summary>
		/// <param name="key">The key.</param>
		/// <param name="title">The title.</param>
		/// <param name="width">The width.</param>
		/// <param name="columnType">Type of the column.</param>
		/// <param name="hAlign">The horizontal alignment.</param>
		/// <param name="vAlign">The vertical alignment.</param>
		/// <param name="hidden">if set to <c>true</c> [hidden].</param>
		/// <returns>ExcelColumnInfo.</returns>
		public static ExcelColumnInfo Default(
			string key = "",
			string title = "",
			int width = 100,
			ColumnTypes columnType = ColumnTypes.String,
			HorizontalAlignment hAlign = HorizontalAlignment.Center,
			VerticalAlignment vAlign = VerticalAlignment.Center,
			bool hidden = false)
		{
			return new ExcelColumnInfo
			{
				Key = key,
				Title = title,
				TypeName = columnType.ToString(),
				Width = width,
				HorizontalAlignment = hAlign,
				VerticalAlignment = vAlign,
				Hidden = hidden,
			};
		}
	}
}