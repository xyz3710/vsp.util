﻿/**********************************************************************************************************************/
/*	Domain		:	X10.Common.Excels.ExcelHelper
/*	Creator		:	KIM-KIWON\xyz37(김기원)
/*	Create		:	2012년 8월 20일 월요일 오전 10:38
/*	Purpose		:	Server에서 Excel을 처리하는데 필요한 기능을 제공합니다.
/*--------------------------------------------------------------------------------------------------------------------*/
/*	Modifier	:	
/*	Update		:	
/*	Changes		:	
/*--------------------------------------------------------------------------------------------------------------------*/
/*	Comment		:	How to read in XLSX data for editing with NPOI
 *					(http://www.zachhunter.com/2010/05/read-xlsx-data-for-npoi/)
 *					* POI CellStyle: http://poi.apache.org/apidocs/org/apache/poi/ss/usermodel/CellStyle.html
 *					* Quick Guide: http://blog.naver.com/PostView.nhn?blogId=nanandayo&logNo=60173460177
 *					* Color & Cell style quick Guide: http://stackoverflow.com/questions/11416305/export-multiple-gridviews-to-excel-clientside
 *					* HSSFColor chart: http://webprogrammer.tistory.com/329
/*--------------------------------------------------------------------------------------------------------------------*/
/*	Reviewer	:	Kim Ki Won
/*	Rev. Date	:	
/**********************************************************************************************************************/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.OleDb;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using NPOI.HSSF.UserModel;
using NPOI.HSSF.Util;
using NPOI.SS.UserModel;
using NPOI.SS.Util;

namespace X10.Common.Excels
{
	/// <summary>
	/// Server에서 Excel을 처리하는데 필요한 기능을 제공합니다.
	/// </summary>
	public static class ExcelHelper
	{
		/// <summary>
		/// 엑셀 워크시트을 생성합니다.
		/// </summary>
		/// <typeparam name="T">모델 타입</typeparam>
		/// <param name="dataSource">데이터 소스</param>
		/// <param name="workbook">여러 워크시트를 생성할 경우 <see cref="HSSFWorkbook" />를 전달하고 그렇지 않으면 null이나 생성하여 전달</param>
		/// <param name="columns">워크 시트를 생성할 컬럼 정보</param>
		/// <param name="sheetName">워크시트 이름</param>
		/// <param name="sheetInfo">The sheet information.</param>
		/// <returns>생성된 워크시트를 반환합니다.</returns>
		public static HSSFWorkbook GenerateWorkSheet<T>(
			this IEnumerable<T> dataSource,
			HSSFWorkbook workbook,
			ExcelColumnInfo[] columns,
			string sheetName,
			SheetInfo<T> sheetInfo)
			where T : class
		{
			return GenerateWorkSheet<T>(
					   dataSource.ToArray(),
					   workbook,
					   columns,
					   sheetName,
					   sheetInfo);
		}

		/// <summary>
		/// 엑셀 워크시트을 생성합니다.
		/// </summary>
		/// <typeparam name="T">모델 타입</typeparam>
		/// <param name="dataSource">데이터 소스</param>
		/// <param name="workbook"><see cref="HSSFWorkbook" />로 없으면 생성</param>
		/// <param name="columns">워크 시트를 생성할 컬럼 정보</param>
		/// <param name="sheetName">워크시트 이름</param>
		/// <param name="sheetInfo">The sheet information.</param>
		/// <returns>생성된 워크북을 반환합니다.</returns>
		/// <exception cref="System.ArgumentNullException">columns가 null 입니다.</exception>
		public static HSSFWorkbook GenerateWorkSheet<T>(
			T[] dataSource,
			HSSFWorkbook workbook,
			ExcelColumnInfo[] columns,
			string sheetName,
			SheetInfo<T> sheetInfo)
		{
			if (workbook == null)
				workbook = new HSSFWorkbook();

			if (columns == null)
				throw new ArgumentNullException("columns가 null 입니다.");

			sheetName = sheetName.SetFileNameRule(", ");

			var sheet = workbook.CreateSheet(sheetName);

			sheet.SetWorkSheet<T>(dataSource, columns, sheetInfo);

			return workbook;
		}

		/// <summary>
		/// 엑셀 워크시트을 설정합니다.
		/// </summary>
		/// <typeparam name="T">모델 타입</typeparam>
		/// <param name="sheet">The sheet.</param>
		/// <param name="dataSource">데이터 소스</param>
		/// <param name="columns">워크 시트를 생성할 컬럼 정보</param>
		/// <param name="sheetInfo">The sheet information.</param>
		/// <returns>워크시트를 반환합니다.</returns>
		public static ISheet SetWorkSheet<T>(
			this  ISheet sheet,
			T[] dataSource,
			ExcelColumnInfo[] columns,
			SheetInfo<T> sheetInfo)
		{
			var headerRowCount = 0;
			var headerRow = sheet.CreateRow(headerRowCount++);		// 헤더열 생성
			var workbook = sheet.Workbook as HSSFWorkbook;
			IFont headerFont = workbook.CreateFont(12);
			IFont font = workbook.CreateFont(10);
			// 같은 Cell을 공유해서 아래처럼 새로운 인스턴스를 생성 후 처리
			// http://npoi.codeplex.com/discussions/259161 참고
			// CellStyle의 최대 갯수는 4000개다. 넘을 경우 아래 오류 발생
			// The maximum number of cell styles was exceeded. You can define up to 4000 styles in a .xls workbook
			var cellStyleCache = workbook.GetCellStyleDictionary();
			Regex arrayIndexRegex = new Regex(@"\[(\d*)\]\.", RegexOptions.Compiled | RegexOptions.Singleline);

			headerRow.Height *= sheetInfo.HeaderHeightMultiple;

			for (int i = 0; i < columns.Length; i++)
			{
				var headerCell = headerRow.CreateCell(i);

				headerCell.SetCellValue(columns[i].Title);	// 컬럼 타이틀
				headerCell.CellStyle.Alignment = NPOI.SS.UserModel.HorizontalAlignment.Center;
				headerCell.CellStyle.VerticalAlignment = AdjustVAlignment(NPOI.SS.UserModel.VerticalAlignment.Center);
				headerCell.CellStyle.WrapText = sheetInfo.HeaderWrapText;
				headerCell.CellStyle.SetFont(headerFont);
				headerCell.CellStyle.ShrinkToFit = true;

				if (sheetInfo.AutoSizeColumn == false)
				{
					sheet.SetColumnWidth(i, ConvertPixelToExcelSize(columns[i].Width));		// 컬럼 폭
				}

				SetCellStyle(workbook, columns[i].TypeName);		// cellStyleCache를 컬럼별로 구성한다.
			}

			sheet.CreateFreezePane(0, 1, 0, 1);		// 헤더열 고정
			//HSSFCellStyle oddStyle = GetOddCellStyle(workbook, oddRowColorIndex);
			//HSSFCellStyle evenStyle = GetEvenCellStyle(workbook, eventRowColorIndex);
			IDataFormat format = workbook.CreateDataFormat();
			BindingFlags propertyBindingFlags = BindingFlags.NonPublic | BindingFlags.Public | BindingFlags.Instance;
			var dataFormatDic = new Dictionary<string, short>();

			//Parallel.For(0, dataSource.Length, row =>
			for (int row = 0; row < dataSource.Length; row++)
			{	// 데이터로 sheet에 바인딩
				var xRow = sheet.CreateRow(row + headerRowCount);		// 새로운 행 생성
				var type = dataSource[row].GetType();

				xRow.Height = (short)(xRow.Height + xRow.Height * 0.3);

				for (int col = 0; col < columns.Length; col++)
				{
					var cell = xRow.CreateCell(col);
					var colInfo = columns[col];
					string columnKey = colInfo.Key;
					string columnTypeName = colInfo.TypeName;

					cell.CellStyle = cellStyleCache[colInfo.TypeName];	// 4,000개를 넘을 수 없으므로 캐쉬에서 가져온다.
					cell.CellStyle.WrapText = sheetInfo.CellWrapText;
					cell.CellStyle.ShrinkToFit = sheetInfo.CellShrinkToFit;

					if (colInfo.IsLocked == false)
					{
						var newCellStyle = SetCellStyle(workbook, colInfo.Key);

						newCellStyle.IsLocked = colInfo.IsLocked;
						newCellStyle.WrapText = sheetInfo.CellWrapText;
						newCellStyle.ShrinkToFit = sheetInfo.CellShrinkToFit;
						newCellStyle.FillForegroundColor = HSSFColor.LightGreen.Index;
						newCellStyle.FillPattern = FillPattern.SolidForeground;
						cell.CellStyle = newCellStyle;
					}

					if (colInfo.DataFormatString != string.Empty)
					{
						if (dataFormatDic.ContainsKey(columnKey) == false)
						{
							var formatIndex = workbook.CreateDataFormat().GetFormat(colInfo.DataFormatString);

							dataFormatDic.Add(columnKey, formatIndex);
						}

						cell.CellStyle.DataFormat = dataFormatDic[columnKey];
					}

					if (colInfo.Formula != string.Empty)
					{
						cell.SetCellType(CellType.Formula);
						string formula = colInfo.Formula.Replace("{ROW}", (xRow.RowNum + headerRowCount).ToString()).Replace("{COL}", (col + 1).ToString());

						cell.SetCellFormula(formula);
					}

					if (columnKey.Contains("]") == true)	// 배열
					{
						string arrayName = columnKey.Split(new string[] { "[" }, 2, StringSplitOptions.RemoveEmptyEntries)[0];
						var arrayValue = type.GetProperty(arrayName, propertyBindingFlags).GetValue(dataSource[row], null);

						if (arrayValue != null)
						{
							if (columnKey.Contains("].") == true)		// class 배열로 하위 속성이 있는 경우
							{
								string subPropertyName = columnKey.Split(new string[] { "]." }, 2, StringSplitOptions.RemoveEmptyEntries)[1];
								int index = Convert.ToInt32(arrayIndexRegex.Match(columnKey).Groups[1].Value);
								var arrayPropertyValue = (arrayValue as Array).GetValue(index);
								Type arrayPropertyType = arrayPropertyValue.GetType();
								var subArrayPropertyInfo = arrayPropertyType.GetProperty(subPropertyName, propertyBindingFlags);
								var subPrppertyValue = subArrayPropertyInfo.GetValue(arrayPropertyValue, null);

								SetCellValueByType(sheet, cell, columnKey, columnTypeName, col, subPrppertyValue);
							}
							else
							{
								SetCellValueByType(sheet, cell, columnKey, columnTypeName, col, arrayValue);
							}
						}
					}
					else
					{
						var pi = type.GetProperty(columnKey, propertyBindingFlags);

						if (pi != null)
						{
							// 같은 Cell을 공유해서 아래처럼 새로운 인스턴스를 생성 후 처리
							// http://npoi.codeplex.com/discussions/259161 참고
							//cell.CellStyle = workbook.CreateCellStyle(); cellStyleCache로 제공
							object propertyValue = pi.GetValue(dataSource[row], null);
							SetCellValueByType(sheet, cell, columnKey, columnTypeName, col, propertyValue);
						}
					}

					if (sheetInfo.UseAlternativeRow == true)
					{
						if (row % 2 == 0)
						{
							cell.CellStyle = GetEvenCellStyle(cell.CellStyle, sheetInfo.EventRowColorIndex);
						}
						else
						{
							cell.CellStyle = GetOddCellStyle(cell.CellStyle, sheetInfo.OddRowColorIndex);
						}
					}
					else
					{
						cell.CellStyle.VerticalAlignment = AdjustVAlignment(NPOI.SS.UserModel.VerticalAlignment.Center);
					}

					cell.CellStyle.Alignment = colInfo.HorizontalAlignment;
					cell.CellStyle.VerticalAlignment = AdjustVAlignment(colInfo.VerticalAlignment);
					cell.CellStyle.SetFont(font);
				}
			}

			if (sheetInfo.AutoSizeColumn == true)
			{
				for (int i = 0; i < columns.Length; i++)
				{
					sheet.AutoSizeColumn(i);
				}
			}

			return sheet;
		}

		/// <summary>
		/// Adjusts the vertical alignment.(현재 버전(2.0.6.0) 에서 수직 정렬값이 밀렸다 그래서 원래 값에서 1을 빼줘야 정상적으로 세팅 된다.)
		/// </summary>
		/// <param name="verticalAlignment">The vertical alignment.</param>
		/// <returns>VerticalAlignment.</returns>
		public static VerticalAlignment AdjustVAlignment(VerticalAlignment verticalAlignment)
		{
			// NOTICE: 현재 버전(2.0.6.0) 에서 수직 정렬값이 밀렸다 그래서 원래 값에서 1을 빼줘야 정상적으로 세팅 된다.
			// https://npoi.codeplex.com/workitem/13263
			// by KIMKIWON\xyz37(김기원) in 2014년 9월 26일 금요일 오전 9:54

			return verticalAlignment - 1;
		}

		/// <summary>
		/// 일반 pixcel을 POI에서 사용하는 excel pixcel 크기로 변환합니다.
		/// </summary>
		/// <param name="pixel"></param>
		/// <returns></returns>
		public static int ConvertPixelToExcelSize(int pixel)
		{
			// 절대적인 변환인데 폰트 크기에 따라서 변경될 수 있다.
			return pixel * 37;
		}

		/// <summary>
		/// 엑셀 컬럼 정보를 파싱합니다.
		/// </summary>
		/// <param name="excelColumns">columns 멤버이름을 포함하는 Json 객체</param>
		/// <returns></returns>
		public static ExcelColumnInfo[] ExcelColumnInfoParser(object excelColumns)
		{
			var jsonString = string.Format("{{ \"columns\": {0} }}", (excelColumns as string[])[0]);
			JavaScriptSerializer jsonSerializer = new JavaScriptSerializer();

			return jsonSerializer.Deserialize<ColumnCollection>(jsonString).ConvertToArray();
		}

		/// <summary>
		/// 컬럼 index로 Excel에서 사용하는 알파벳 문자로 변환해서 구합니다.
		/// </summary>
		/// <param name="colIndex">컬럼 인덱스(0에서 시작합니다.)</param>
		/// <returns>A~Z, AA~AZ, BA~BZ... 등을 구합니다.</returns>
		public static string GetAlphaCellName(int colIndex)
		{
			int share = colIndex / 26;
			int rest = colIndex % 26;
			string result = string.Empty;

			if (share > 0)
			{
				share = share - 1;
				result = string.Format("{0}{1}", (char)('A' + share), (char)('A' + rest));
			}
			else
				result = string.Format("{0}", (char)('A' + rest));

			return result;
		}

		/// <summary>
		/// Creates the default cell style.
		/// <remarks>내부적으로 관리하는 Syle Dictionary에 저장을 합니다.</remarks>
		/// </summary>
		/// <param name="workbook">The workbook.</param>
		/// <param name="styleName">workbook의 style dictionary에 사용되는 스타일 이름(키).</param>
		/// <param name="dataFormat">The data format.</param>
		/// <param name="horizontalAlignment">The horizontal alignment.</param>
		/// <param name="verticalAlignment">The vertical alignment.</param>
		/// <param name="font">The font.</param>
		/// <param name="allBorderStyle">상/하/좌/우의 border style.</param>
		/// <param name="borderColor">Color of the border.</param>
		/// <returns>ICellStyle.</returns>
		public static ICellStyle CreateDefaultCellStyle(
			this HSSFWorkbook workbook,
			string styleName,
			string dataFormat = "",
			HorizontalAlignment horizontalAlignment = HorizontalAlignment.Center,
			VerticalAlignment verticalAlignment = VerticalAlignment.Center,
			IFont font = null,
			BorderStyle allBorderStyle = BorderStyle.Thin,
			short borderColor = HSSFColor.Black.Index)
		{
			var dic = workbook.GetCellStyleDictionary();
			var cellStyle = workbook.CreateCellStyle();

			if (dataFormat != string.Empty)
			{
				cellStyle.DataFormat = workbook.CreateDataFormat().GetFormat(dataFormat);
			}

			cellStyle.Alignment = horizontalAlignment;
			cellStyle.VerticalAlignment = ExcelHelper.AdjustVAlignment(verticalAlignment);
			cellStyle.BorderLeft = allBorderStyle;
			cellStyle.BorderTop = allBorderStyle;
			cellStyle.BorderRight = allBorderStyle;
			cellStyle.BorderBottom = allBorderStyle;
			cellStyle.LeftBorderColor = borderColor;
			cellStyle.TopBorderColor = borderColor;
			cellStyle.RightBorderColor = borderColor;
			cellStyle.BottomBorderColor = borderColor;

			if (font != null)
			{
				cellStyle.SetFont(font);
			}

			dic.AddOrUpdate(styleName, cellStyle);

			return cellStyle;
		}

		private static Dictionary<string, Dictionary<string, ICellStyle>> _workbookStyleDictionary;

		/// <summary>
		/// Gets the cell style dictionary.
		/// <remarks>첫번째 이름이 Dictionary를 사용하는 이름으로 정의가 되어 있으면 해당 Style Dictionay를 반환 합니다.</remarks>
		/// </summary>
		/// <param name="workbook">The workbook.</param>
		/// <returns>Dictionary{System.StringICellStyle}.</returns>
		public static Dictionary<string, ICellStyle> GetCellStyleDictionary(this IWorkbook workbook)
		{
			const string DICTIONARY_PREFIX = "Dic:";
			var result = new Dictionary<string, ICellStyle>();

			if (_workbookStyleDictionary == null)
			{
				_workbookStyleDictionary = new Dictionary<string, Dictionary<string, ICellStyle>>();
			}

			if (workbook.NumberOfNames == 0)
			{
				workbook.CreateName().NameName = string.Format("{0}{1}", DICTIONARY_PREFIX, Guid.NewGuid());
			}

			var name = workbook.GetNameAt(0).NameName;

			if (name.StartsWith(DICTIONARY_PREFIX) == false)
			{
				return result;
			}

			if (_workbookStyleDictionary.ContainsKey(name) == true)
			{
				result = _workbookStyleDictionary[name];
			}
			else
			{
				_workbookStyleDictionary.Add(name, result);
			}

			return result;
		}

		/// <summary>
		/// Sets the cell style.
		/// </summary>
		/// <param name="workbook">The workbook.</param>
		/// <param name="typeName">Name of the type.</param>
		/// <param name="dataFormat">The data format.</param>
		/// <returns>ICellStyle.</returns>
		public static ICellStyle SetCellStyle(
			this HSSFWorkbook workbook,
			string typeName,
			string dataFormat = "")
		{
			var cellStyleCache = workbook.GetCellStyleDictionary();

			switch (typeName)
			{
				case "Number":
					if (cellStyleCache.ContainsKey(typeName) == false)
					{
						cellStyleCache.Add(typeName, workbook.CreateCellStyle());
					}

					var numberCellStyle = cellStyleCache[typeName];

					if (dataFormat == string.Empty)
					{
						dataFormat = "#,##0";
					}

					numberCellStyle.DataFormat = workbook.CreateDataFormat().GetFormat(dataFormat);

					break;
				case "Boolean":
					if (cellStyleCache.ContainsKey(typeName) == false)
					{
						cellStyleCache.Add(typeName, workbook.CreateCellStyle());
					}

					var booleanCellStyle = cellStyleCache[typeName];

					if (dataFormat == string.Empty)
					{
						dataFormat = "[=1]\"예\";[=0]\"아니오\"";
					}

					booleanCellStyle.DataFormat = workbook.CreateDataFormat().GetFormat(dataFormat);

					break;
				case "Decimal":
					if (cellStyleCache.ContainsKey(typeName) == false)
					{
						cellStyleCache.Add(typeName, workbook.CreateCellStyle());
					}

					var doubleCellStyle = cellStyleCache[typeName];

					if (dataFormat == string.Empty)
					{
						dataFormat = "[=0]0;#,###.0";
					}

					doubleCellStyle.DataFormat = workbook.CreateDataFormat().GetFormat(dataFormat);

					break;
				case "Date":
					if (cellStyleCache.ContainsKey(typeName) == false)
					{
						cellStyleCache.Add(typeName, workbook.CreateCellStyle());
					}

					var dateTimeCellStyle = cellStyleCache[typeName];

					if (dataFormat == string.Empty)
					{
						dataFormat = "[=-1]\"\";yyyy-mm-dd hh:mm:ss";
					}

					dateTimeCellStyle.DataFormat = workbook.CreateDataFormat().GetFormat(dataFormat);

					break;
				default:
					if (cellStyleCache.ContainsKey(typeName) == false)
					{
						workbook.CreateDefaultCellStyle(
							typeName,
							horizontalAlignment: HorizontalAlignment.General,
							allBorderStyle: BorderStyle.None);
					}

					if (dataFormat != string.Empty)
					{
						var defaultCellStyle = cellStyleCache[typeName];

						defaultCellStyle.DataFormat = workbook.CreateDataFormat().GetFormat(dataFormat);
					}

					break;
			}

			return cellStyleCache[typeName];
		}

		/// <summary>
		/// Sets the type of the cell value by.
		/// </summary>
		/// <param name="sheet">The sheet.</param>
		/// <param name="cell">The cell.</param>
		/// <param name="columnsKey">The columns key.</param>
		/// <param name="typeName">Name of the type.</param>
		/// <param name="columnIndex">Index of the column.</param>
		/// <param name="propertyValue">The property value.</param>
		public static void SetCellValueByType(
			this ISheet sheet,
			ICell cell,
			string columnsKey,
			string typeName,
			int columnIndex,
			object propertyValue)
		{
			switch (typeName)
			{
				case "Number":
					cell.SetCellValue(Convert.ToInt32(propertyValue));

					break;
				case "Boolean":
					cell.SetCellValue(Convert.ToInt32(propertyValue));

					break;
				case "Decimal":
					cell.SetCellValue(Convert.ToDouble(propertyValue));

					break;
				case "Date":
					cell.SetCellValue(Convert.ToDateTime(propertyValue));
					sheet.AutoSizeColumn(columnIndex);			// 날짜형은 초까지 나오기 때문에 셀 폭을 자동 조절 한다.

					break;
				default:
					cell.SetCellValue(Convert.ToString(propertyValue));

					if (columnsKey == "Name" || columnsKey == "Title")
						sheet.AutoSizeColumn(columnIndex);		// 제목, 이름은 셀 폭을 자동 조절 한다.

					break;
			}
		}

		/// <summary>
		/// 지정된 폰트로 fontSize에 맞는 IFont를 생성합니다.
		/// </summary>
		/// <param name="workbook">지정할 워크북</param>
		/// <param name="fontSize">폰트 크기</param>
		/// <param name="fontName">폰트 이름</param>
		/// <returns></returns>
		public static IFont CreateFont(
			this HSSFWorkbook workbook,
			short fontSize = 10,
			string fontName = "맑은 고딕")
		{
			IFont font = workbook.CreateFont();

			font.FontHeightInPoints = fontSize;
			font.FontName = fontName;
			font.Boldweight = 5;
			font.Color = NPOI.HSSF.Util.HSSFColor.Black.Index;

			return font;
		}

		/// <summary>
		/// sheet 지정된 숫자만큼의 행을 삽입합니다.
		/// </summary>
		/// <param name="sheet"></param>
		/// <param name="fromRowIndex"></param>
		/// <param name="rowCount"></param>
		public static void InsertRows(
			this HSSFSheet sheet,
			int fromRowIndex,
			int rowCount)
		{
			sheet.ShiftRows(fromRowIndex, sheet.LastRowNum, rowCount, true, false, true);

			for (int rowIndex = fromRowIndex; rowIndex < fromRowIndex + rowCount; rowIndex++)
			{
				var rowSource = sheet.GetRow(rowIndex + rowCount);
				var rowInsert = sheet.CreateRow(rowIndex);

				rowInsert.Height = rowSource.Height;

				for (int colIndex = 0; colIndex < rowSource.LastCellNum; colIndex++)
				{
					var cellSource = rowSource.GetCell(colIndex);
					var cellInsert = rowInsert.CreateCell(colIndex);

					if (cellSource != null)
						cellInsert.CellStyle = cellSource.CellStyle;
				}
			}
		}

		#region GetCellValue
		private static dynamic GetCellValueCore(ICell cell)
		{
			switch (cell.CellType)
			{
				case CellType.Numeric:

					return cell.NumericCellValue;
				case CellType.String:

					return cell.StringCellValue;
				case CellType.Formula:

					break;
				case CellType.Blank:

					return null;
				case CellType.Boolean:

					return cell.BooleanCellValue;
				case CellType.Error:

					return cell.ErrorCellValue;
			}

			return null;
		}

		/// <summary>
		/// 지정된 행/열에 지정된 값을 반환합니다.
		/// </summary>
		/// <param name="sheet">The sheet.</param>
		/// <param name="row">행</param>
		/// <param name="col">열</param>
		/// <returns>행과 열이 없으면 생성한 뒤 ICell을 반환합니다.</returns>
		public static dynamic GetCellValue(this ISheet sheet, int row, int col)
		{
			var cell = sheet.GetRow(row).Cells[col];

			return GetCellValueCore(cell);
		}

		/// <summary>
		/// 지정된 행/열에 지정된 값을 반환합니다.
		/// </summary>
		/// <param name="cell">The sheet.</param>
		/// <returns>값이 없으면 null을 반환합니다.</returns>
		public static dynamic GetCellValue(this ICell cell)
		{
			return GetCellValueCore(cell);
		}
		#endregion

		#region SetCellValue2
		/// <summary>
		/// 지정된 행/열에 지정된 값을 설정합니다.
		/// </summary>
		/// <param name="sheet">The sheet.</param>
		/// <param name="rowIndex">행</param>
		/// <param name="columnIndex">열</param>
		/// <param name="value">값을 지정하지 않으면 row와 col을 지정한 cell을 반환 합니다.</param>
		/// <param name="cellStyle">The cell style.</param>
		/// <returns>행과 열이 없으면 생성한 뒤 ICell을 반환합니다.</returns>
		/// <exception cref="System.NotSupportedException">value가 지정된 타입이 아닐경우</exception>
		public static ICell SetCellValue(
			this ISheet sheet,
			int rowIndex,
			int columnIndex,
			dynamic value,
			ICellStyle cellStyle)
		{
			CheckSetValueValueType(value);

			var activeCell = CreateCellCore(sheet, rowIndex, columnIndex);

			if (value == null)
			{
				value = string.Empty;
			}

			SetCellValue2(activeCell, value);

			if (cellStyle != null)
			{
				activeCell.CellStyle = cellStyle;
			}

			return activeCell;
		}

		/// <summary>
		/// 지정된 행/열에 지정된 값을 설정합니다.
		/// </summary>
		/// <param name="sheet">The sheet.</param>
		/// <param name="rowIndex">행</param>
		/// <param name="columnIndex">열</param>
		/// <param name="value">값을 지정하지 않으면 row와 col을 지정한 cell을 반환 합니다.</param>
		/// <param name="horizontalAlignment">The horizontal alignment.</param>
		/// <param name="verticalAlignment">The vertical alignment.</param>
		/// <param name="font">The font.</param>
		/// <param name="wrapText">if set to <c>true</c> [wrap text].</param>
		/// <returns>행과 열이 없으면 생성한 뒤 ICell을 반환합니다.</returns>
		/// <exception cref="System.NotSupportedException">value가 지정된 타입이 아닐경우</exception>
		public static ICell SetCellValue(
			this ISheet sheet,
			int rowIndex,
			int columnIndex,
			dynamic value = null,
			HorizontalAlignment horizontalAlignment = HorizontalAlignment.Center,
			VerticalAlignment verticalAlignment = VerticalAlignment.Center,
			IFont font = null,
			bool wrapText = true)
		{
			CheckSetValueValueType(value);

			var activeCell = CreateCellCore(sheet, rowIndex, columnIndex);

			if (value == null)
			{
				value = string.Empty;
			}

			SetCellValue2(activeCell, value);
			SetCellValueStyle(activeCell, horizontalAlignment, verticalAlignment, font, wrapText);

			return activeCell;
		}

		private static ICell CreateCellCore(ISheet sheet, int rowIndex, int columnIndex)
		{
			ICell activeCell = null;
			IRow row = sheet.GetRow(rowIndex);

			if (row == null)
			{
				row = sheet.CreateRow(rowIndex);
			}

			activeCell = row.GetCell(columnIndex);

			if (activeCell == null)
			{
				activeCell = row.CreateCell(columnIndex);
			}

			return activeCell;
		}

		private static ICell SetCellValueStyle(
			ICell cell,
			HorizontalAlignment horizontalAlignment,
			VerticalAlignment verticalAlignment,
			IFont font,
			bool wrapText)
		{
			cell.CellStyle.Alignment = horizontalAlignment;
			cell.CellStyle.VerticalAlignment = AdjustVAlignment(verticalAlignment);

			cell.CellStyle.WrapText = wrapText;

			if (font != null)
			{
				cell.CellStyle.SetFont(font);
			}

			return cell;
		}

		/// <summary>
		/// 셀의 값을 설정 합니다.
		/// </summary>
		/// <param name="cell">The cell.</param>
		/// <param name="value">The value.</param>
		/// <param name="cellStyle">The cell style.</param>
		/// <returns>ICell.</returns>
		/// <exception cref="System.NotSupportedException">value가 지정된 타입이 아닐경우</exception>
		public static ICell SetCellValue2(
			this ICell cell,
			dynamic value,
			ICellStyle cellStyle)
		{
			CheckSetValueValueType(value);

			if (value == null)
			{
				value = string.Empty;
			}

			cell.SetCellValue(value);

			if (cellStyle != null)
			{
				cell.CellStyle = cellStyle;
			}

			return cell;
		}

		private static void CheckSetValueValueType(dynamic value)
		{
			if (value == null)
			{
				return;
			}

			var typeName = Convert.ToString(value.GetType().Name);

			if ((typeName == "Boolean" || typeName == "DateTime" ||
								typeName == "Double" || typeName == "Decimal" || typeName == "Int32" ||
								typeName == "String" || typeName == "IRichTextString") == false)
			{
				throw new NotSupportedException(string.Format("value가 지정된 타입이 아닙니다.\r\nType: {0}", value.GetType().Name));
			}
		}

		/// <summary>
		/// 셀의 값을 설정 합니다.
		/// </summary>
		/// <param name="cell">The cell.</param>
		/// <param name="value">The value.</param>
		/// <param name="horizontalAlignment">The horizontal alignment.</param>
		/// <param name="verticalAlignment">The vertical alignment.</param>
		/// <param name="font">The font.</param>
		/// <param name="wrapText">if set to <c>true</c> [wrap text].</param>
		/// <returns>ICell.</returns>
		/// <exception cref="System.NotSupportedException">value가 지정된 타입이 아닐경우</exception>
		public static ICell SetCellValue2(
			this ICell cell,
			dynamic value,
			HorizontalAlignment horizontalAlignment = HorizontalAlignment.Center,
			VerticalAlignment verticalAlignment = VerticalAlignment.Center,
			IFont font = null,
			bool wrapText = true)
		{
			CheckSetValueValueType(value);

			if (value == null)
			{
				value = string.Empty;
			}

			cell.SetCellValue(value);

			return SetCellValueStyle(cell, horizontalAlignment, verticalAlignment, font, wrapText);
		}
		#endregion

		/// <summary>
		/// Creates the formula cell.
		/// </summary>
		/// <param name="row">The row.</param>
		/// <param name="column">The column.</param>
		/// <param name="formula">The formula. e.g. "SUM(B3:B4)" 와 같이 =를 없애고 함수 등을 사용한다.</param>
		/// <param name="cellStyle">The cell style.</param>
		/// <returns>ICell.</returns>
		public static ICell CreateFormulaCell(
			this IRow row,
			int column,
			string formula,
			ICellStyle cellStyle = null)
		{
			var cell = row.CreateCell(column, CellType.Formula);

			cell.SetCellFormula(formula);

			if (cellStyle != null)
			{
				cell.CellStyle = cellStyle;
			}

			return cell;
		}

		/// <summary>
		/// Sets the cell border.
		/// </summary>
		/// <param name="sheet">The sheet.</param>
		/// <param name="fromRowIndex">Index of from row.</param>
		/// <param name="toRowIndex">Index of to row.</param>
		/// <param name="fromColumnIndex">Index of from column.</param>
		/// <param name="toColumnIndex">Index of to column.</param>
		/// <param name="borderStyle">The border style.</param>
		/// <param name="borderColor">Color of the border.</param>
		public static void SetCellBorder(
			this ISheet sheet,
			int fromRowIndex,
			int toRowIndex,
			int fromColumnIndex,
			int toColumnIndex,
			BorderStyle borderStyle = BorderStyle.Thin,
			short borderColor = HSSFColor.Black.Index)
		{
			var region = new CellRangeAddress(fromRowIndex, toRowIndex, fromColumnIndex, toColumnIndex);
			var hssfSheet = sheet as HSSFSheet;
			var hssfWorkbook = sheet.Workbook as HSSFWorkbook;

			HSSFRegionUtil.SetLeftBorderColor(borderColor, region, hssfSheet, hssfWorkbook);
			HSSFRegionUtil.SetTopBorderColor(borderColor, region, hssfSheet, hssfWorkbook);
			HSSFRegionUtil.SetRightBorderColor(borderColor, region, hssfSheet, hssfWorkbook);
			HSSFRegionUtil.SetBottomBorderColor(borderColor, region, hssfSheet, hssfWorkbook);
			HSSFRegionUtil.SetBorderLeft(borderStyle, region, hssfSheet, hssfWorkbook);
			HSSFRegionUtil.SetBorderTop(borderStyle, region, hssfSheet, hssfWorkbook);
			HSSFRegionUtil.SetBorderRight(borderStyle, region, hssfSheet, hssfWorkbook);
			HSSFRegionUtil.SetBorderBottom(borderStyle, region, hssfSheet, hssfWorkbook);
		}

		#region AddMergedRegion
		/// <summary>
		/// Merge된 영역을 생성 후 firstRow, firstCol에 값을 할당 합니다.
		/// </summary>
		/// <param name="sheet">The sheet.</param>
		/// <param name="firstRow">첫번째 행</param>
		/// <param name="lastRow">마지막 행</param>
		/// <param name="firstCol">첫번째 열</param>
		/// <param name="lastCol">마지막 열</param>
		/// <param name="index">합쳐진 영역의 인덱스</param>
		/// <param name="value">값을 지정하지 않으면 row와 col을 지정한 cell을 반환 합니다.</param>
		/// <param name="cellStyle">The cell style.</param>
		/// <param name="borderStyle">The border style.</param>
		/// <param name="borderColor">Color of the border.</param>
		/// <returns>ICell.</returns>
		/// <exception cref="System.NotSupportedException">value가 지정된 타입이 아닐경우</exception>
		public static ICell AddMergedRegion(
			this ISheet sheet,
			int firstRow,
			int lastRow,
			int firstCol,
			int lastCol,
			out int index,
			dynamic value,
			ICellStyle cellStyle,
			BorderStyle borderStyle = BorderStyle.None,
			short borderColor = HSSFColor.Black.Index)
		{
			index = sheet.AddMergedRegion(new CellRangeAddress(firstRow, lastRow, firstCol, lastCol));

			if (borderStyle != BorderStyle.None)
			{
				sheet.SetCellBorder(firstRow, lastRow, firstCol, lastCol, borderStyle, borderColor);
			}

			return SetCellValue(sheet, firstRow, firstCol, value, cellStyle);
		}

		/// <summary>
		/// Merge된 영역을 생성 후 firstRow, firstCol에 값을 할당 합니다.
		/// </summary>
		/// <param name="sheet">The sheet.</param>
		/// <param name="firstRow">첫번째 행</param>
		/// <param name="lastRow">마지막 행</param>
		/// <param name="firstCol">첫번째 열</param>
		/// <param name="lastCol">마지막 열</param>
		/// <param name="index">합쳐진 영역의 인덱스</param>
		/// <param name="value">값을 지정하지 않으면 row와 col을 지정한 cell을 반환 합니다.</param>
		/// <param name="horizontalAlignment">The horizontal alignment.</param>
		/// <param name="verticalAlignment">The vertical alignment.</param>
		/// <param name="font">The font.</param>
		/// <param name="wrapText">if set to <c>true</c> [wrap text].</param>
		/// <param name="borderStyle">The border style.</param>
		/// <param name="borderColor">Color of the border.</param>
		/// <returns>ICell.</returns>
		/// <exception cref="System.NotSupportedException">value가 지정된 타입이 아닐경우</exception>
		public static ICell AddMergedRegion(
			this ISheet sheet,
			int firstRow,
			int lastRow,
			int firstCol,
			int lastCol,
			out int index,
			dynamic value,
			HorizontalAlignment horizontalAlignment = HorizontalAlignment.Center,
			VerticalAlignment verticalAlignment = VerticalAlignment.Center,
			IFont font = null,
			bool wrapText = true,
			BorderStyle borderStyle = BorderStyle.None,
			short borderColor = HSSFColor.Black.Index)
		{
			index = sheet.AddMergedRegion(new CellRangeAddress(firstRow, lastRow, firstCol, lastCol));

			if (borderStyle != BorderStyle.None)
			{
				sheet.SetCellBorder(firstRow, lastRow, firstCol, lastCol, borderStyle, borderColor);
			}

			return SetCellValue(sheet, firstRow, firstCol, value, horizontalAlignment, verticalAlignment, font, wrapText);
		}
		#endregion

		private static HSSFCellStyle GetOddCellStyle(ICellStyle oddStyle, short foreColor)
		{
			oddStyle.FillForegroundColor = foreColor;
			oddStyle.FillPattern = NPOI.SS.UserModel.FillPattern.SolidForeground;
			oddStyle.BorderBottom = BorderStyle.Thin;
			oddStyle.BottomBorderColor = NPOI.HSSF.Util.HSSFColor.White.Index;
			oddStyle.BorderRight = BorderStyle.Thin;
			oddStyle.RightBorderColor = NPOI.HSSF.Util.HSSFColor.White.Index;
			oddStyle.BorderLeft = BorderStyle.Thin;
			oddStyle.LeftBorderColor = NPOI.HSSF.Util.HSSFColor.White.Index;
			oddStyle.BorderTop = BorderStyle.Thin;
			oddStyle.TopBorderColor = NPOI.HSSF.Util.HSSFColor.White.Index;
			oddStyle.VerticalAlignment = VerticalAlignment.Center;

			return oddStyle as HSSFCellStyle;
		}

		private static HSSFCellStyle GetEvenCellStyle(ICellStyle evenStyle, short foreColor)
		{
			evenStyle.FillForegroundColor = foreColor;
			evenStyle.FillPattern = NPOI.SS.UserModel.FillPattern.SolidForeground;
			evenStyle.BorderBottom = BorderStyle.Thin;
			evenStyle.BottomBorderColor = NPOI.HSSF.Util.HSSFColor.White.Index;
			evenStyle.BorderRight = BorderStyle.Thin;
			evenStyle.RightBorderColor = NPOI.HSSF.Util.HSSFColor.White.Index;
			evenStyle.BorderLeft = BorderStyle.Thin;
			evenStyle.LeftBorderColor = NPOI.HSSF.Util.HSSFColor.White.Index;
			evenStyle.BorderTop = BorderStyle.Thin;
			evenStyle.TopBorderColor = NPOI.HSSF.Util.HSSFColor.White.Index;
			evenStyle.VerticalAlignment = VerticalAlignment.Center;

			return evenStyle as HSSFCellStyle;
		}

		/// <summary>
		/// Render a Excel 2007 (xlsx) Worksheet to NPOI Excel 2003 Worksheet, all excel formatting
		/// from XLSX will be lost when converted.  NPOI roadmap shows v1.6 will support Excel 2007 (xlsx).
		/// </summary>
		/// <param name="excelFileStream">XLSX FileStream</param>
		/// <param name="sheetName">Excel worksheet to convert</param>
		/// <returns>MemoryStream containing NPOI Excel workbook</returns>
		/// <remarks>
		/// NPOI Roadmap  : http://npoi.codeplex.com/wikipage?title=NPOI%20Road%20Map
		/// NPOI Homepage : http://npoi.codeplex.com/
		/// </remarks>
		public static Stream ConvertXLSXWorksheetToXLSWorksheet(Stream excelFileStream, string sheetName)
		{
			// Temp file name
			string tempFile = HttpContext.Current.Server.MapPath("~/uploads/" + HttpContext.Current.Session.LCID + ".tmp");

			// Temp data container (using DataTable to leverage existing RenderDataTableToExcel function)
			DataTable dt = new DataTable();

			try
			{
				// Create temp XLSX file
				FileStream fileStream = new FileStream(tempFile, FileMode.Create, FileAccess.Write);

				const int length = 256;
				Byte[] buffer = new Byte[length];
				int bytesRead = excelFileStream.Read(buffer, 0, length);

				while (bytesRead > 0)
				{
					fileStream.Write(buffer, 0, bytesRead);
					bytesRead = excelFileStream.Read(buffer, 0, length);
				}

				excelFileStream.Close();
				fileStream.Close();

				// Read temp XLSX file using OLEDB
				// Tested on Vista & Windows 2008 R2
				using (OleDbConnection con = new OleDbConnection(@"Provider=Microsoft.ACE.OLEDB.12.0; Extended Properties=Excel 12.0;Data Source=" + tempFile + @";Extended Properties=Excel 12.0;"))
				{
					con.Open();
					string sql = String.Format("SELECT * FROM [{0}$]", sheetName);
					OleDbDataAdapter da = new OleDbDataAdapter(sql, con);

					da.Fill(dt);
				}
			}
			finally
			{
				// Make sure temp file is deleted
				File.Delete(tempFile);
			}

			// Return a new POI Excel 2003 Workbook
			return RenderDataTableToExcel(dt);
		}

		/// <summary>
		/// Render DataTable to NPOI Excel 2003 MemoryStream
		/// NOTE:  Limitation of 65,536 rows suppored by XLS
		/// </summary>
		/// <param name="sourceTable">Source DataTable</param>
		/// <returns>MemoryStream containing NPOI Excel workbook</returns>
		public static Stream RenderDataTableToExcel(DataTable sourceTable)
		{
			HSSFWorkbook workbook = new HSSFWorkbook();
			MemoryStream memoryStream = new MemoryStream();
			// By default NPOI creates "Sheet0" which is inconsistent with Excel using "Sheet1"
			var sheet = workbook.CreateSheet("Sheet1");
			var headerRow = sheet.CreateRow(0);

			// Header Row
			foreach (DataColumn column in sourceTable.Columns)
				headerRow.CreateCell(column.Ordinal).SetCellValue(column.ColumnName);

			// Detail Rows
			int rowIndex = 1;

			foreach (DataRow row in sourceTable.Rows)
			{
				var dataRow = sheet.CreateRow(rowIndex);

				foreach (DataColumn column in sourceTable.Columns)
				{
					dataRow.CreateCell(column.Ordinal).SetCellValue(row[column].ToString());
				}

				rowIndex++;
			}

			workbook.Write(memoryStream);
			memoryStream.Flush();
			memoryStream.Position = 0;

			return memoryStream;
		}

		#region SetHeight
		/// <summary>
		/// 행의 높이를 지정 합니다.
		/// </summary>
		/// <param name="row">The row.</param>
		/// <param name="height">pixel이 아닌 엑셀에서 지정하는 높이</param>
		/// <returns>IRow.</returns>
		/// <exception cref="System.ArgumentNullException">row</exception>
		public static IRow SetHeight(this IRow row, double height)
		{
			if (row == null)
			{
				throw new ArgumentNullException("row");
			}

			row.Height = (short)(height * 20);

			return row;
		}
		#endregion
	}
}