﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using NPOI.HSSF.UserModel;
using X10.Common.Mail;
using System.Configuration;

namespace SendScheduledMail.Controllers
{
	public class HomeController : Controller
	{
		public ActionResult Index()
		{
			return View();
		}

		public ActionResult About()
		{
			ViewBag.Message = "Your application description page.";

			return View();
		}

		public ActionResult Contact()
		{
			ViewBag.Message = "Your contact page.";

			return View();
		}

		public async Task<ContentResult> SendScheduledMail()
		{
			string toMail = ConfigurationManager.AppSettings["toMail"];
			string fromMail = ConfigurationManager.AppSettings["fromMail"];
			string ccMail = ConfigurationManager.AppSettings["ccMail"];
			string templateFile = Server.MapPath("~/App_Data/LIG_카드수납요청서.xls");
			List<string> files = new List<string>();
			var today = DateTime.Today;

			ChangeExcelMonth(templateFile);
			files.Add(templateFile);

			await MailHelper.SendAsync(
				to: toMail,
				subject: string.Format("{0:0000}년 {1:00}월 보험료 카드 수납 요청서입니다.", today.Year, today.Month),
				body: "카드 수납 요청서 첨부 합니다.",
				from: fromMail,
				cc: ccMail,
				filesToAttach: files);

			return Content(string.Empty);
		}

		private void ChangeExcelMonth(string templateFile)
		{
			HSSFWorkbook workbook = null;

			using (var file = new FileStream(templateFile, FileMode.Open, FileAccess.Read, FileShare.ReadWrite))
			{
				workbook = new HSSFWorkbook(file);
				HSSFFormulaEvaluator.EvaluateAllFormulaCells(workbook);
			}

			using (var file = new FileStream(templateFile, FileMode.Create, FileAccess.Write, FileShare.ReadWrite))
			{
				workbook.Write(file);
			}
		}
	}
}