﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(SendScheduledMail.Startup))]
namespace SendScheduledMail
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
