@ECHO OFF
::********************************************************************************************************************
:: Purpose	:	매월 5일에 카드 수납 요청서 전송용
:: Creator	:	Kim Ki Won
:: Create	:	2017년 3월 7일 화요일 오전 10:48:14
:: Modifier	:	
:: Update	:	
:: Comment	:	
::				
::********************************************************************************************************************

::************************************************************************************************************** Label
SETLOCAL ENABLEEXTENSIONS ENABLEDELAYEDEXPANSION 
:START

:: 작업 스케줄러에 등록 한다.
CALL :__AddScheduler

SET curl=.\curl\curl

%curl% http://x10.iptime.org/Batch/SendScheduledMail

GOTO END

REM ************************************************ Inner  Batch ************************************************ Label
:__AddScheduler
SET Script=%~dpnx0
SET TaskName1=카드 수납 요청서
SET Version=
SET startTime=09:00:00

CALL :__GetOSVersion

IF %Version% geq 6 SCHTASKS /query /tn "%TaskName1%" > NUL
IF %Version% lss 6 SCHTASKS /QUERY /fo table | FINDSTR /L "%TaskName1%" > NUL

::SCHTASKS /query /tn "%TaskName1%" > NUL
::IF ERRORLEVEL == 0 SCHTASKS /DELETE /tn "%TaskName1%" /f

IF "%ERRORLEVEL%"=="0" GOTO :EOF

SCHTASKS /create /tn "%TaskName1%" /tr "\"%Script%"" /sc monthly /d 5 /st %startTime% /ed 2999/12/31 /ru "%USERDOMAIN%\%USERNAME%" /rl HIGHEST 
::SCHTASKS /create /tn "%TaskName1%" /tr "\"%Script%"" /sc daily /du 120 /ri 60 /ru "%USERDOMAIN%\%USERNAME%" /rl HIGHEST
::SCHTASKS /DELETE /tn %TaskName1% /f

GOTO :EOF
REM ********************************************************************************************************************

REM ************************************************ Inner  Batch ************************************************ Label
:__GetOSVersion
FOR /F "tokens=3,4* delims=[. " %%F in ('ver') DO SET Version=%%G
GOTO :EOF
REM ********************************************************************************************************************

::************************************************************************************************************** Label
:USAGE
ECHO.
ECHO  
ECHO.
ECHO    %~nx0 : ^< ^>
ECHO            
ECHO.
ECHO        ex) %~nx0 
ECHO.

::************************************************************************************************************** Label
:END
ENDLOCAL

::EXIT 
