﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Windows.Forms;

namespace FileMerger
{
	class Program
	{
		static int Main(string[] args)
		{
			if (args.Length < 2)
				return -1;

			string filter = args[0];
			string newFileName = args[1];

			string[] files = Directory.GetFiles(Application.StartupPath, filter, SearchOption.TopDirectoryOnly);

			FileStream fs = File.OpenWrite(newFileName);
			StreamWriter sw = new StreamWriter(fs);
			StreamReader sr = null;

			foreach (string file in files)
			{				
				try
				{
					sr = File.OpenText(file);

					sw.WriteLine();
					sw.WriteLine();
					sw.Write(sr.ReadToEnd());
				}
				catch (Exception ex)
				{
					Console.WriteLine(ex.Message);
				}
				finally
				{
					if (sr != null)
                    	sr.Close();
				}
			}

			if (sw != null)
			{
				sw.Flush();
				sw.Close();
			}

			return 0;
		}
	}
}
