﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Diagnostics;
using System.IO;
using System.Reflection;
using System.Security.AccessControl;

namespace GacManagement
{
	public class GacManager2
	{
		public static bool RegisterAssembly(string filename)
		{
			bool result = false;

			try
			{
				Assembly assembly = Assembly.ReflectionOnlyLoadFrom(filename);
				AssemblyName name = assembly.GetName();
				byte[] publicKeyToken = name.GetPublicKeyToken();

				if (publicKeyToken == null)
					return result;

				string gacRoot = string.Format(@"{0}\assembly\GAC_MSIL", Environment.GetEnvironmentVariable("SystemRoot"));
				string publicKeyTokenName = BitConverter.ToString(publicKeyToken).Replace("-", string.Empty).ToLower();
				string gacFolderName = string.Format("{0}__{1}", name.Version, publicKeyTokenName);
				string gacFolderPath = string.Format(@"{0}\{1}\{2}", gacRoot, name.Name, gacFolderName);
				string targetPath = string.Format(@"{0}\{1}", gacFolderPath, Path.GetFileName(filename));

				if (Directory.Exists(gacFolderPath) == false)
					Directory.CreateDirectory(gacFolderPath);

				File.Copy(filename, targetPath, true);
				result = true;
			}
			catch
			{
			}

			return result;
		}

		public static bool UnRegisterAssembly(string filename)
		{
			bool result = false;

			try
			{
				Assembly assembly = Assembly.ReflectionOnlyLoadFrom(filename);
				AssemblyName name = assembly.GetName();
				byte[] publicKeyToken = name.GetPublicKeyToken();

				if (publicKeyToken == null)
					return result;

				string gacRoot = string.Format(@"{0}\assembly\GAC_MSIL", Environment.GetEnvironmentVariable("SystemRoot"));
				string publicKeyTokenName = BitConverter.ToString(publicKeyToken).Replace("-", string.Empty).ToLower();
				string gacFolderName = string.Format("{0}__{1}", name.Version, publicKeyTokenName);
				string gacFolderPath = string.Format(@"{0}\{1}\{2}", gacRoot, name.Name, gacFolderName);
				string targetPath = string.Format(@"{0}\{1}", gacFolderPath, Path.GetFileName(filename));

				if (Directory.Exists(gacFolderPath) == true)
					Directory.Delete(gacFolderPath, true);

				result = true;
			}
			catch
			{
			}

			return result;
		}
	}
}
