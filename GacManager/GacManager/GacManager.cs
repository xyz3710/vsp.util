﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Diagnostics;
using System.IO;

namespace GacManagement
{
	public class GacManager
	{
		public static bool RegisterAssembly(string filename)
		{
			bool result = false;

			try
			{
				result = RegisterAssemblyCode(filename);
			}
			catch 
			{				
			}

			return result;
		}

		public static bool UnRegisterAssembly(string filename)
		{
			bool result = false;

			try
			{
				result = UnRegisterAssemblyCode(filename);
			}
			catch
			{
			}

			return result;
		}

		private static bool RegisterAssemblyCode(string fileName)
		{
			// Debug.Print("Registering .. " + fileName);

			string envName = "VS80COMNTOOLS"; // For VS 2003 We need to use "VS71COMNTOOLS". 
			string toolPath = Environment.GetEnvironmentVariable(envName);
			string vsCmdLinePath = Path.Combine(toolPath, "vsvars32.bat");
			Process process = new Process();

			using (StreamReader reader = new StreamReader(vsCmdLinePath))
			{
				string value = null;

				while (null != (value = reader.ReadLine()))
				{
					if (value.IndexOf("FrameworkSDKDir") != -1)
					{
						string sdkPath = value.Substring(value.IndexOf("=") + 1).Trim();
						string gacutilPath = Path.Combine(sdkPath, @"bin\gacutil.exe");
						string cmdLineArgument = String.Format(" -i \"{0}\" /f", fileName);

						// Debug.Print("Argument : "+ cmdLineArgument);

						process.StartInfo = new ProcessStartInfo(gacutilPath, cmdLineArgument);

						break;
					}
				}
			}

			process.StartInfo.CreateNoWindow = true;
			process.StartInfo.RedirectStandardOutput = true;
			process.StartInfo.UseShellExecute = false;
			process.Start();
			process.WaitForExit();

			if (process.ExitCode == 0)
				return true;
			else
				return false;
		}

		private static bool UnRegisterAssemblyCode(string fileName)
		{
			// Debug.Print("Unregistering .. " + fileName);

			string envName = "VS80COMNTOOLS"; // For VS 2003 We need to use "VS71COMNTOOLS". 
			string toolPath = Environment.GetEnvironmentVariable(envName);
			string vsCmdLinePath = Path.Combine(toolPath, "vsvars32.bat");
			Process process = new Process();

			using (StreamReader reader = new StreamReader(vsCmdLinePath))
			{
				string value = null;
				while (null != (value = reader.ReadLine()))
				{
					if (value.IndexOf("FrameworkSDKDir") != -1)
					{
						string sdkPath = value.Substring(value.IndexOf("=") + 1).Trim();
						string gacutilPath = Path.Combine(sdkPath, @"bin\gacutil.exe");
						string asmName = Path.GetFileName(fileName);

						if (asmName.ToLower().EndsWith(".dll"))
							asmName = asmName.Substring(0,asmName.ToLower().LastIndexOf(".dll"));

						string cmdLineArgument = " -u " + asmName;

						// Debug.Print("Argument : " + cmdLineArgument);

						process.StartInfo = new ProcessStartInfo(gacutilPath, cmdLineArgument);

						break;
					}
				}
			}

			process.StartInfo.CreateNoWindow = true;
			process.StartInfo.RedirectStandardOutput = true;
			process.StartInfo.UseShellExecute = false;
			process.Start();
			process.WaitForExit();

			if (process.ExitCode == 0)
				return true;
			else
				return false;
		}
	}
}
