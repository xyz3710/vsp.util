﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Reflection;
using System.IO;

namespace GacManagement
{
	class Program
	{
		static void Main(string[] args)
		{
			string assemblyPath = @"C:\Program Files\iSet-DA\Framework.Win\AnyFactory.FX.Win.dll";

			/*
			Console.WriteLine("UnRegister result : {0}", GacManager2.UnRegisterAssembly(assemblyPath));
			Console.WriteLine("등록하려면 엔터키를 누르세요.");			
			Console.ReadLine();
			Console.WriteLine("Register result : {0}", GacManager2.RegisterAssembly(assemblyPath));
			
			return;
			*/
			List<string> list = new List<string>(new string[]{ assemblyPath });

			//list.AddRange(Directory.GetFiles(Path.GetDirectoryName(assemblyPath), "*.dll"));
			//list.AddRange(Directory.GetFiles(Path.GetDirectoryName(assemblyPath), "*.exe"));
			
			foreach (string file in list)
			{
				string fileName = Path.GetFileNameWithoutExtension(file);

				try
				{
					Console.WriteLine("Unregister {0} from GAC result : {1}", fileName, GlobalAssemblyCache.Value.UninstallAssembly(fileName));
				}
				catch (Exception ex)
				{
					Console.WriteLine("\t{0}", ex.Message);
				}
			}

			AssemblyName assemblyName = AssemblyName.GetAssemblyName(assemblyPath);

			Console.WriteLine(GlobalAssemblyCache.Value.Contains(assemblyName));			

			foreach (string file in list)
			{
				string fileName = Path.GetFileNameWithoutExtension(file);

				try
				{
					Console.WriteLine("Register {0} from GAC result : {1}", fileName, GlobalAssemblyCache.Value.InstallAssembly(file));
				}
				catch (Exception ex)
				{
					Console.WriteLine("\t{0}", ex.Message);
				}				
			}

			Console.WriteLine(GlobalAssemblyCache.Value.Contains(assemblyName));			
		}
	}
}
