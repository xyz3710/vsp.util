using System;

namespace Tools.Network
{
	/// <summary>
	/// 
	/// </summary>
	public enum ImpersonationLevel
	{
		SecurityAnonymous = 0,
		SecurityIdentification = 1,
		SecurityImpersonation = 2,
		SecurityDelegation = 3
	}
}

