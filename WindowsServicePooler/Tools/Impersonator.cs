﻿/**********************************************************************************************************************/
/*	Domain		:	WindowsServicePooler.ImpersonationUtil
/*	Creator		:	KIMKIWON\xyz37(김기원)
/*	Create		:	2011년 11월 22일 화요일 오후 9:34
/*	Purpose		:	
/*--------------------------------------------------------------------------------------------------------------------*/
/*	Modifier	:	
/*	Update		:	
/*	Changes		:	
/*--------------------------------------------------------------------------------------------------------------------*/
/*	Comment		:	참조 : http://platinumdogs.wordpress.com/2008/10/30/net-c-impersonation-with-network-credentials/
/*--------------------------------------------------------------------------------------------------------------------*/
/*	Reviewer	:	Kim Ki Won
/*	Rev. Date	:	
/**********************************************************************************************************************/

using System;
using System.ComponentModel;
using System.Runtime.InteropServices;
using System.Security.Principal;

namespace Tools.Network
{
	/// <summary>
	/// Allows code to be executed under the security context of a specified user account.
	/// </summary>
	/// <remarks>
	///
	/// Implements IDispose, so can be used via a using-directive or method calls;
	///...
	///
	///var imp = new Impersonator( "myDomainname", "myUsername", "myPassword" );
	///imp.UndoImpersonation();
	///
	///...
	///
	/// var imp = new Impersonator();
	///imp.Impersonate("myDomainname", "myUsername", "myPassword");
	///imp.UndoImpersonation();
	///
	///...
	///
	///using ( new Impersonator( "myDomainname", "myUsername", "myPassword" ) )
	///{
	///...
	///[code that executes under the new context]
	///...
	///}
	///
	///...
	/// </remarks>
	public class Impersonator : IDisposable
	{
		private WindowsImpersonationContext _wic;

		/// <summary>
		/// Begins impersonation with the given credentials, Logon type and Logon provider.
		/// </summary>
		/// <param name="domainName">Name of the domain.</param>
		/// <param name="userName">Name of the user.</param>
		/// <param name="password">The password. <see cref="System.String"/></param>
		/// <param name="logonType">Type of the logon.</param>
		/// <param name="logonProvider">The logon provider. <see cref="LogonProvider"/></param>
		public Impersonator(string domainName, string userName, string password, LogonType logonType, LogonProvider logonProvider)
		{
			Impersonate(domainName, userName, password, logonType, logonProvider);
		}

		/// <summary>
		/// Begins impersonation with the given credentials.
		/// </summary>
		/// <param name="domainName">Name of the domain.</param>
		/// <param name="userName">Name of the user.</param>
		/// <param name="password">The password. <see cref="System.String"/></param>
		public Impersonator(string domainName, string userName, string password)
		{
			Impersonate(domainName, userName, password, LogonType.LOGON32_LOGON_INTERACTIVE, LogonProvider.LOGON32_PROVIDER_DEFAULT);
		}

		/// <summary>
		/// Initializes a new instance of the <see cref="Impersonator"/> class.
		/// </summary>
		public Impersonator()
		{
		}

		/// <summary>
		/// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
		/// </summary>
		public void Dispose()
		{
			UndoImpersonation();
		}

		/// <summary>
		/// Impersonates the specified user account.
		/// </summary>
		/// <param name="domainName">Name of the domain.</param>
		/// <param name="userName">Name of the user.</param>
		/// <param name="password">The password. <see cref="System.String"/></param>
		public void Impersonate(string domainName, string userName, string password)
		{
			Impersonate(domainName, userName, password, LogonType.LOGON32_LOGON_INTERACTIVE, LogonProvider.LOGON32_PROVIDER_DEFAULT);
		}

		/// <summary>
		/// Impersonates the specified user account.
		/// </summary>
		/// <param name="domainName">Name of the domain.</param>
		/// <param name="userName">Name of the user.</param>
		/// <param name="password">The password. <see cref="System.String"/></param>
		/// <param name="logonType">Type of the logon.</param>
		/// <param name="logonProvider">The logon provider. <see cref="Mit.Sharepoint.WebParts.EventLogQuery.Network.LogonProvider"/></param>
		public bool Impersonate(
			string domainName,
			string userName,
			string password,
			LogonType logonType,
			LogonProvider logonProvider)
		{
			UndoImpersonation();

			IntPtr logonToken = IntPtr.Zero;
			IntPtr logonTokenDuplicate = IntPtr.Zero;
			bool result = false;

			try
			{
				// revert to the application pool identity, saving the identity of the current requestor
				_wic = WindowsIdentity.Impersonate(IntPtr.Zero);

				// do logon & impersonate
				if (Win32NativeMethods.LogonUser(userName, domainName, password, (int)logonType, (int)logonProvider, ref logonToken) != 0)
				{
					if (Win32NativeMethods.DuplicateToken(logonToken, (int)ImpersonationLevel.SecurityImpersonation, ref logonTokenDuplicate) != 0)
					{
						var wi = new WindowsIdentity(logonTokenDuplicate);

						if (wi != null)
						{
							wi.Impersonate();// discard the returned identity context (which is the context of the application pool)
							result = true;
						}
					}
					else
						throw new Win32Exception(Marshal.GetLastWin32Error());
				}
				else
					throw new Win32Exception(Marshal.GetLastWin32Error());
			}
			finally
			{
				if (logonToken != IntPtr.Zero)
					Win32NativeMethods.CloseHandle(logonToken);

				if (logonTokenDuplicate != IntPtr.Zero)
					Win32NativeMethods.CloseHandle(logonTokenDuplicate);
			}

			return result;
		}

		/// <summary>
		/// Stops impersonation.
		/// </summary>
		public void UndoImpersonation()
		{
			// restore saved requestor identity
			if (_wic != null)
				_wic.Undo();

			_wic = null;
		}
	}
}