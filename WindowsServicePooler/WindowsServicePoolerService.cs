﻿/**********************************************************************************************************************/
/*	Domain		:	WindowsServicePooler.WindowsServicePoolerService
/*	Creator		:	KIMKIWON\xyz37(김기원)
/*	Create		:	2011년 11월 23일 수요일 오후 5:02
/*	Purpose		:	원격지 컴퓨터의 특정 서비스를 감시하여 항상 사용 가능 상태로 서비스 풀을 구성합니다.
/*--------------------------------------------------------------------------------------------------------------------*/
/*	Modifier	:	
/*	Update		:	
/*	Changes		:	
/*--------------------------------------------------------------------------------------------------------------------*/
/*	Comment		:	1. alternativeMode = true로 설정되면 isPrimary = true 서비스 부터 우선 시작 시키고
 *						실패시 등록된 순서에서 다음 목록의 서버를 isPrimary = false로 되어 있어도 강제로 시작 시킨다.
 *						모든 서버가 primary = false로 되어 있고 서비스 정보를 가져오는데 문제가 없다면 
 *						서비스는 자동으로 종료 된다.
 *					2. alternativeMode = false로 설정되면 isPrimary 속성은 무시되고 등록된 서버의 서비스를 모두 시작 시킨다.
 *					
 *					serviceProcessInstaller 권한 issue : 
 *						http://social.msdn.microsoft.com/Forums/en/sqlsecurity/thread/31d57870-1faa-4e14-8527-ce77b1ff40e4
/*--------------------------------------------------------------------------------------------------------------------*/
/*	Reviewer	:	Kim Ki Won
/*	Rev. Date	:	2011년 11월 23일 수요일 오후 5:06
/**********************************************************************************************************************/

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading;
using System.Windows.Forms;
using Tools.Network;
using WindowsServicePooler.Configuration;
using System.Security.Permissions;

namespace WindowsServicePooler
{
	/// <summary>
	/// 원격지 컴퓨터의 특정 서비스를 감시하여 항상 사용 가능 상태로 서비스 풀을 구성합니다.
	/// </summary>
	public partial class WindowsServicePoolerService : ServiceBase
	{
		private Impersonator _impersonators;
		private Thread _mainThread;
		private string _startedServerName;

		public WindowsServicePoolerService()
		{
			InitializeComponent();

			ReadConfiguration();
			_impersonators = new Impersonator();
		}

		private void ReadConfiguration()
		{
			EnabledLogging = ConfigWrapper.Instance.Section.EnableLogging;
			FilePath = ConfigWrapper.Instance.Section.LogFilePath;
			Interval = ConfigWrapper.Instance.Section.Interval;
			Services = ConfigWrapper.Instance.Section.Services
				.Cast<Service>().ToList();
		}

		#region Properties
		private bool EnabledLogging
		{
			get;
			set;
		}

		private string FilePath
		{
			get;
			set;
		}

		private int Interval
		{
			get;
			set;
		}

		private IList<Service> Services
		{
			get;
			set;
		}
		#endregion

		#region Logging
		private void WriteLog(string msg)
		{
			if (EnabledLogging == false)
				return;

			Logging(FilePath, msg);
		}

		private void WriteLog(Exception ex)
		{
			StringBuilder message = new StringBuilder();

			message.AppendLine("오류 발생");

			if (ex.Message != null)
				message.AppendLine(ex.Message);

			if (ex.InnerException != null)
				message.AppendLine(string.Format("Inner Exception : {0}", ex.InnerException.Message));

			WriteLog(message.ToString());
		}

		/// <summary>
		/// 일자별로 해당 폴더에 Log 파일을 생성
		/// </summary>
		/// <param name="logPath"></param>
		/// <param name="logMessage"></param>
		public static void Logging(string logPath, string logMessage)
		{
			FileStream fsLog = null;
			string logFolder = string.Empty;

			if (string.IsNullOrEmpty(logPath) == true)
			{

				logPath = string.Format(@"{0}\Log{1}.txt", Application.StartupPath, DateTime.Now.ToShortDateString());
				logFolder = Path.GetDirectoryName(logPath);
			}
			else
			{
				logFolder = Path.GetDirectoryName(logPath);
				logPath = string.Format(@"{0}\{1} {2}{3}",
							  logFolder,
							  Path.GetFileNameWithoutExtension(logPath),
							  DateTime.Now.ToShortDateString(),
							  Path.GetExtension(logPath));
			}

			if (logFolder != string.Empty && Directory.Exists(logFolder) == false)
				Directory.CreateDirectory(logFolder);

			try
			{
				fsLog = File.Open(logPath, FileMode.Append, FileAccess.Write);
				string context = string.Format("{0}\t{1}\r\n", DateTime.Now.ToString("yyyy-MM-dd hh:mm:ss"), logMessage);
				byte[] contextBytes = Encoding.Default.GetBytes(context);

				fsLog.Write(contextBytes, 0, contextBytes.Length);
				Console.WriteLine(context);
			}
			catch
			{
			}
			finally
			{
				if (fsLog != null)
					fsLog.Close();
			}
		}
		#endregion

		protected override void OnStart(string[] args)
		{
			// 서비스 시작시 설정 파일을 다시 읽어 들인다.
			ConfigWrapper.Instance.Refresh();
			ReadConfiguration();

			try
			{
				WriteLog("Pool Service Start");
				_mainThread = new Thread(PoolServiceStart);

				_mainThread.Start();
			}
			catch (Exception ex)
			{
				WriteLog(string.Format("OnStart : {0}", ex.Message));
			}
		}

		protected override void OnStop()
		{
			_mainThread.Abort();
			_impersonators.UndoImpersonation();
			WriteLog("Pool Service Stop");
		}

		private void PoolServiceStart()
		{
			do
			{
				ServiceSwiching();

				Thread.Sleep(Interval);
			}
			while (true);
		}

		private void ServiceSwiching()
		{
			foreach (Service service in Services)
				CheckServiceInMachine(service);
		}

		private void CheckServiceInMachine(Service service)
		{
			bool primaryServiceNotStarted = false;
			bool primaryServiceAlreadyStarted = false;		// alternativeMode일 경우 primary는 처음 1개만 실행 시킨다.
			string lastMachineName = service.MachinesInPool.Last().Name;

			foreach (MachineInPool mip in service.MachinesInPool)
			{
				ServiceController sc = null;

				#region 권한 설정
				// by KIMKIWON\xyz37 in 2011년 11월 24일 목요일 오후 3:11
				/*
				ServiceControllerPermission scp = new ServiceControllerPermission(ServiceControllerPermissionAccess.Control, mip.Name, service.Name);
				SecurityPermission sp = new SecurityPermission(PermissionState.Unrestricted);

				scp.Demand();
				sp.Demand();
				*/
				#endregion

				try
				{
					Impersonator impersonator = new Impersonator();

					if (impersonator.Impersonate(
							string.IsNullOrEmpty(mip.DomainName) == true ? mip.Name : mip.DomainName,
							mip.UserName,
							mip.Password,
							LogonType.LOGON32_LOGON_NEW_CREDENTIALS,
							LogonProvider.LOGON32_PROVIDER_DEFAULT) == true)
						sc = new ServiceController(service.Name, mip.Name);
					else
					{
						WriteLog(string.Format(
									 "No such account found, Impersonation failed. : {0}, {1}, {2}",
									 mip.DomainName,
									 mip.UserName,
									 mip.Password));

						continue;
					}
				}
				catch (InvalidOperationException ioe)
				{
					WriteLog(ioe);
				}
				catch (Win32Exception wex)
				{
					WriteLog(wex);
				}

				if (sc == null)
				{
					// 다음 서버로 이동
					WriteLog(string.Format("{0}: {1} service not found.", mip.Description, service.Name));

					continue;
				}

				try
				{
					if (sc.Status == ServiceControllerStatus.Running)
					{
						if (service.AlternativeMode == true)
						{
							// primary로 지정된 서비스를 우선 실행 하고 나머지는 Stop 시킨다.
							if (mip.IsPrimary == true)
							{
								if (primaryServiceAlreadyStarted == true)
								{
									SetStop(sc, service.Name, mip);
									
									continue;
								}

								primaryServiceAlreadyStarted = true;

								continue;
							}
							else
								SetStop(sc, service.Name, mip);
						}
					}
					else
					{
						if (service.AlternativeMode == true)
						{
							if ((mip.IsPrimary == true						// primary로 지정된 서비스를 우선 실행 하고 나머지는 Stop 시킨다.
								|| primaryServiceNotStarted == true)		// primary가 시작되지 않으면 시작 시킨다.
								&& primaryServiceAlreadyStarted == false)	// primary 서비스가 일단 시작하지 않아야 한다.
							{
								SetStart(sc, service.Name, mip);
								primaryServiceNotStarted = false;
							}
						}
						else
							SetStart(sc, service.Name, mip);
					}
				}
				catch (InvalidOperationException ex)
				{
					if (mip.IsPrimary == true)
						primaryServiceNotStarted = true;

					// 해당 서버의 서비스가 시작할 수 없으면 다음 서버로 이동
					WriteLog(string.Format(
								 "{0}: {1} service could not start.\r\n\t{2}",
								 mip.Description,
								 service.Name,
								 ex.Message));

					continue;
				}
			}
		}

		private void SetStop(ServiceController sc, string serviceName, MachineInPool mip)
		{
			sc.Stop();
			sc.WaitForStatus(ServiceControllerStatus.Stopped);
			WriteLog(string.Format("{0}: {1} service force stoped.", mip.Description, serviceName));
		}

		private void SetStart(ServiceController sc, string serviceName, MachineInPool mip)
		{
			sc.Start();
			sc.WaitForStatus(ServiceControllerStatus.Running);
			_startedServerName = mip.Name;
			WriteLog(string.Format("{0}: {1} service force started.", mip.Description, serviceName));
		}

		internal void TestStart()
		{
			//OnStart(null);
			ServiceSwiching();
		}
	}
}
