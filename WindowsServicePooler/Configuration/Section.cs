﻿/**********************************************************************************************************************/
/*	Domain		:	WindowsServicePooler.Configuration.Section
/*	Creator		:	KIMKIWON\xyz37(김기원)
/*	Create		:	2011년 11월 23일 수요일 오후 1:42
/*	Purpose		:	WindowsServicePooler의 섹션에 관련된 구성 정보를 제공합니다.
/*--------------------------------------------------------------------------------------------------------------------*/
/*	Modifier	:	
/*	Update		:	
/*	Changes		:	
/*--------------------------------------------------------------------------------------------------------------------*/
/*	Comment		:	
/*--------------------------------------------------------------------------------------------------------------------*/
/*	Reviewer	:	Kim Ki Won
/*	Rev. Date	:	2011년 11월 24일 목요일 오전 10:19
/**********************************************************************************************************************/

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Text;

namespace WindowsServicePooler.Configuration
{
	/// <summary>
	/// WindowsServicePooler의 섹션에 관련된 구성 정보를 제공합니다.
	/// </summary>
	public class Section : ConfigurationSection
	{
		/// <summary>
		/// WindowsServicePooler의 SectionName string을 구합니다.
		/// </summary>
		public const string SectionNameString = "windowsServicePooler";

		/// <summary>
		/// Section class의 새 인스턴스를 초기화합니다.
		/// </summary>
		public Section()
		{
		}

		/// <summary>
		/// EnableLogging를 구하거나 설정합니다.
		/// </summary>
		[ConfigurationProperty("enableLogging", DefaultValue = "true", IsRequired = true)]
		[Description("EnableLogging를 구하거나 설정합니다.")]
		public bool EnableLogging
		{
			get
			{
				return (bool)this["enableLogging"];
			}
			set
			{
				this["enableLogging"] = value;
			}
		}

		/// <summary>
		/// LogFilePath를 구하거나 설정합니다.
		/// </summary>
		/// <remarks>해당 경로에는 Local Service 권한이 있어야 로깅이 가능합니다.</remarks>
		[ConfigurationProperty("logFilePath", DefaultValue = @"C:\Program Files\Services\WindowsServicePooler.log", IsRequired = true)]
		[Description("LogFilePath를 구하거나 설정합니다.")]
		public string LogFilePath
		{
			get
			{
				return (string)this["logFilePath"];
			}
			set
			{
				this["logFilePath"] = value;
			}
		}

		/// <summary>
		/// Interval를 구하거나 설정합니다.
		/// </summary>
		[ConfigurationProperty("interval", DefaultValue = "5000", IsRequired = true)]
		[Description("Interval를 구하거나 설정합니다.")]
		public int Interval
		{
			get
			{
				return (int)this["interval"];
			}
			set
			{
				this["interval"] = value;
			}
		}

		/// <summary>
		/// Service 정보를 구하거나 설정합니다.
		/// </summary>
		[ConfigurationProperty("services", IsRequired = true)]
		[Description("Service 정보를 구하거나 설정합니다.")]
		public ServiceCollection Services
		{
			get
			{
				return (ServiceCollection)this["services"];
			}
			set
			{
				this["services"] = value;
			}
		}
	}
}
