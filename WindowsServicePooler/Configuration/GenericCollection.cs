﻿/**********************************************************************************************************************/
/*	Domain		:	WindowsServicePooler.Configuration.GenericCollection<T>
/*	Creator		:	KIMKIWON\xyz37(김기원)
/*	Create		:	2011년 11월 24일 목요일 오전 9:05
/*	Purpose		:	
/*--------------------------------------------------------------------------------------------------------------------*/
/*	Modifier	:	
/*	Update		:	
/*	Changes		:	
/*--------------------------------------------------------------------------------------------------------------------*/
/*	Comment		:	
/*--------------------------------------------------------------------------------------------------------------------*/
/*	Reviewer	:	Kim Ki Won
/*	Rev. Date	:	
/**********************************************************************************************************************/

using System;
using System.Collections.Generic;
using System.Text;
using System.Configuration;
using System.Collections;

namespace WindowsServicePooler.Configuration
{
	/// <summary>
	/// 자식 요소의 컬렉션을 포함하는 GenericCollection 구성 요소를 나타냅니다.<br/>
	/// <remarks>T element의 IsRequired=true, IsKey=true 속성이 적용된 Property만 Key로 인식 합니다.</remarks>
	/// </summary>
	[ConfigurationCollection(typeof(ConfigurationElement), CollectionType = ConfigurationElementCollectionType.AddRemoveClearMap)]
	public class GenericCollection<T> : ConfigurationElementCollection, IEnumerable<T>
		where T : ConfigurationElement, new()
	{
		#region Constructors
		/// <summary>
		/// 자식 요소의 컬렉션을 포함하는 <seealso cref="AnyFactory.FX.Win.Configuration.Handler.GenericCollection&lt;T&gt;"/>의 새 인스턴스를 초기화합니다.
		/// </summary>
		public GenericCollection()
		{
		}
		#endregion

		#region Protected methods
		/// <summary>
		/// 파생 클래스에서 재정의 될 때 T type이 인스턴스를 생성합니다.
		/// </summary>
		/// <returns></returns>
		protected override ConfigurationElement CreateNewElement()
		{
			return new T();
		}

		/// <summary>
		/// 파생 클래스에서 재정의 될 때 지정된 구성 요소의 요소 키를 가져옵니다.<br/>
		/// <remarks>T element의 IsRequired=true, IsKey=true 속성이 적용된 Property만 Key로 인식 합니다.</remarks>
		/// </summary>
		/// <param name="element"></param>
		/// <returns></returns>
		protected override object GetElementKey(ConfigurationElement element)
		{
			if (element == null)
				return null;

			object value = null;

			foreach (PropertyInformation pi in element.ElementInformation.Properties)
				if (pi.IsKey == true && pi.IsRequired == true)
					value = pi.Value;

			return value;
		}

		/// <summary>
		/// 구성 요소를 구성 요소 컬렉션에 추가합니다.
		/// </summary>
		/// <param name="index">지정된 T type을 추가할 인덱스 위치 입니다.</param>
		/// <param name="element">추가할 T type의 <seealso cref="System.Configuration.ConfigurationElement"/>입니다.</param>
		protected override void BaseAdd(int index, ConfigurationElement element)
		{
			base.BaseAdd(element, false);
		}
		#endregion

		#region Properties
		/// <summary>
		/// <seealso cref="AnyFactory.FX.Win.Configuration.Handler.GenericCollection&lt;T&gt;"/>의 추가 작업과 연결할 이름을 가져오거나 설정합니다.
		/// </summary>
		public new string AddElementName
		{
			get
			{
				return base.AddElementName;
			}
			set
			{
				base.AddElementName = value;
			}
		}

		/// <summary>
		/// <seealso cref="AnyFactory.FX.Win.Configuration.Handler.GenericCollection&lt;T&gt;"/>의 지우기 작업과 연결할 이름을 가져오거나 설정합니다.
		/// </summary>
		public new string ClearElementName
		{
			get
			{
				return base.ClearElementName;
			}
			set
			{
				base.ClearElementName = value;
			}
		}

		/// <summary>
		/// <seealso cref="AnyFactory.FX.Win.Configuration.Handler.GenericCollection&lt;T&gt;"/>의 제거 작업과 연결할 이름을 가져오거나 설정합니다.
		/// </summary>
		public new string RemoveElementName
		{
			get
			{
				return base.RemoveElementName;
			}
			set
			{
				base.RemoveElementName = value;
			}
		}
		#endregion

		#region Public methods
		/// <summary>
		/// 인덱스를 통하여 T type을 가져오거나 설정합니다.
		/// </summary>
		/// <param name="index">액세스할 T type의 인덱스 입니다.</param>
		/// <returns></returns>
		public T this[int index]
		{
			get
			{
				return (T)base.BaseGet(index);
			}
			set
			{
				if (base.BaseGet(index) != null)
					base.BaseRemoveAt(index);

				BaseAdd(index, value);
			}
		}

		/// <summary>
		/// T를 Key로 구하거나 설정합니다.
		/// </summary>
		/// <param name="keyName">액세스할 T type의 Key Property 이름입니다.</param>
		/// <returns></returns>
		public new T this[string keyName]
		{
			get
			{
				return (T)base.BaseGet(keyName);
			}
			set
			{
				ConfigurationElement element = base.BaseGet(keyName);

				if (element == null)
					BaseAdd(value, false);
				else
				{
					int index = BaseIndexOf(element);

					if (base.BaseGet(index) != null)
						base.BaseRemoveAt(index);

					BaseAdd(index, value);
				}
			}
		}

		/// <summary>
		/// 지정된 T type의 인덱스입니다.
		/// </summary>
		/// <param name="t">지정된 인덱스 위치에 대한 T Type 입니다.</param>
		/// <returns></returns>
		public int IndexOf(T t)
		{
			return base.BaseIndexOf(t);
		}

		/// <summary>
		/// 구성 요소를 구성 요소 컬렉션에 추가합니다.
		/// </summary>
		/// <param name="t">추가할 T type의 <seealso cref="System.Configuration.ConfigurationElement"/> 입니다.</param>
		public void Add(T t)
		{
			base.BaseAdd(t);
		}

		/// <summary>
		/// 컬렉션에서 T type의 <seealso cref="System.Configuration.ConfigurationElement"/>를 제거합니다.
		/// </summary>
		/// <param name="t">제거할 T type의 <seealso cref="System.Configuration.ConfigurationElement"/> 입니다.</param>
		public void Remove(T t)
		{
			if (BaseIndexOf(t) >= 0)
				base.BaseRemove(t.ToString());
		}

		/// <summary>
		/// 컬렉션에서 T type의 <seealso cref="System.Configuration.ConfigurationElement"/>를 제거합니다.
		/// </summary>
		/// <param name="key">제거할 T type의 <seealso cref="System.Configuration.ConfigurationElement"/>의 키입니다.</param>
		public void Remove(object key)
		{
			base.BaseRemove(key);
		}

		/// <summary>
		/// 지정된 인덱스 위치에서 T type의 <seealso cref="System.Configuration.ConfigurationElement"/>를 제거합니다.
		/// </summary>
		/// <param name="index">제거할 T type의 <seealso cref="System.Configuration.ConfigurationElement"/>의 인덱스 위치입니다.</param>
		public void RemoveAt(int index)
		{
			base.BaseRemoveAt(index);
		}

		/// <summary>
		/// 컬렉션에서 구성 요소 개체를 모두 제거 합니다.
		/// </summary>
		public void Clear()
		{
			base.BaseClear();
		}
		#endregion

		#region IEnumerable<T> 멤버

		/// <summary>
		/// T 형태의 IEnumerator를 반환합니다.
		/// </summary>
		/// <returns></returns>
		public new IEnumerator<T> GetEnumerator()
		{
			foreach (T item in (ConfigurationElementCollection)this)
			{
				if (item == null)
					yield break;

				yield return item;
			}
		}

		#endregion
	}
}
