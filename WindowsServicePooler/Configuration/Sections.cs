﻿/**********************************************************************************************************************/
/*	Domain		:	WindowsServicePooler.Configuration.Sections
/*	Creator		:	KIMKIWON\xyz37(김기원)
/*	Create		:	2011년 11월 23일 수요일 오후 1:42
/*	Purpose		:	WindowsServicePooler의 섹션 그룹에 관련된 구성 정보를 제공합니다.
/*--------------------------------------------------------------------------------------------------------------------*/
/*	Modifier	:	
/*	Update		:	
/*	Changes		:	
/*--------------------------------------------------------------------------------------------------------------------*/
/*	Comment		:	
/*--------------------------------------------------------------------------------------------------------------------*/
/*	Reviewer	:	Kim Ki Won
/*	Rev. Date	:	2011년 11월 24일 목요일 오전 9:30
/**********************************************************************************************************************/

using System;
using System.Collections.Generic;
using System.Text;
using System.Configuration;
using System.ComponentModel;

namespace WindowsServicePooler.Configuration
{
	/// <summary>
	/// WindowsServicePooler의 섹션 그룹에 관련된 구성 정보를 제공합니다.
	/// </summary>
	internal class Sections : ConfigurationSectionGroup
	{
		/// <summary>
		/// SectionGroup을 구합니다.
		/// </summary>
		public const string SectionGroupNameString = "windowsServicePoolers";

		/// <summary>
		/// SectionGroup class의 새 인스턴스를 초기화합니다.
		/// </summary>
		public Sections()
		{
		}

		/// <summary>
		/// Section을 구하거나 설정합니다.
		/// </summary>
		[ConfigurationProperty("windowsServicePooler", DefaultValue = "windowsServicePooler", IsRequired = true)]
		[Description("WindowsServicePooler Section을 구하거나 설정합니다.")]
		public Section Section
		{
			get
			{
				return (Section)base.Sections["windowsServicePooler"];
			}
			set
			{
				base.Sections.Add("windowsServicePooler", value);
			}
		}
	}
}
