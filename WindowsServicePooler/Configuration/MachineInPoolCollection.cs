﻿/**********************************************************************************************************************/
/*	Domain		:	WindowsServicePooler.Configuration.MachineInPoolCollection
/*	Creator		:	KIMKIWON\xyz37(김기원)
/*	Create		:	2011년 11월 23일 수요일 오후 1:24
/*	Purpose		:	자식 요소의 컬렉션을 포함하는 MachineInPool 구성 요소를 나타냅니다.
/*--------------------------------------------------------------------------------------------------------------------*/
/*	Modifier	:	
/*	Update		:	
/*	Changes		:	
/*--------------------------------------------------------------------------------------------------------------------*/
/*	Comment		:	
/*--------------------------------------------------------------------------------------------------------------------*/
/*	Reviewer	:	Kim Ki Won
/*	Rev. Date	:	2011년 11월 24일 목요일 오전 9:18
/**********************************************************************************************************************/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;

namespace WindowsServicePooler.Configuration
{
	/// <summary>
	/// 자식 요소의 컬렉션을 포함하는 <seealso cref="WindowsServicePooler.MachineInPoolCollection"/> 구성 요소를 나타냅니다.
	/// </summary>
	[ConfigurationCollection(typeof(MachineInPool), CollectionType = ConfigurationElementCollectionType.AddRemoveClearMap, AddItemName = "machine")]
	public class MachineInPoolCollection : GenericCollection<MachineInPool>
	{
	}
}
