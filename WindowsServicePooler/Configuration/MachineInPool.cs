﻿/**********************************************************************************************************************/
/*	Domain		:	WindowsServicePooler.Configuration.MachineInPool
/*	Creator		:	KIMKIWON\xyz37(김기원)
/*	Create		:	2011년 11월 23일 수요일 오후 1:21
/*	Purpose		:	서비스 풀에서 작동할 대상 컴퓨터를 정보를 구성합니다.
/*--------------------------------------------------------------------------------------------------------------------*/
/*	Modifier	:	
/*	Update		:	
/*	Changes		:	
/*--------------------------------------------------------------------------------------------------------------------*/
/*	Comment		:	
/*--------------------------------------------------------------------------------------------------------------------*/
/*	Reviewer	:	Kim Ki Won
/*	Rev. Date	:	2011년 11월 24일 목요일 오전 9:20
/**********************************************************************************************************************/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;
using System.ComponentModel;
using System.Diagnostics;

namespace WindowsServicePooler.Configuration
{
	/// <summary>
	/// 서비스 풀에서 작동할 대상 컴퓨터를 정보를 구성합니다.
	/// </summary>
	[DebuggerDisplay("Name:{Name}, DomainName:{DomainName}, UserName:{UserName}, Password:{Password}, IsPrimary:{IsPrimary}, Description:{Description}", Name = "MachineInPool")]
	public class MachineInPool : ConfigurationElement
	{
		#region Constructors
		/// <summary>
		/// MachineInPool class의 새 인스턴스를 초기화 합니다.
		/// </summary>
		/// <param name="name"></param>
		/// <param name="domain"></param>
		/// <param name="userName"></param>
		/// <param name="password"></param>
		/// <param name="isPrimary"></param>
		/// <param name="description"></param>
		public MachineInPool(string name, string domain, string userName, string password, bool isPrimary, string description)
		{
			Name = name;
			DomainName = domain;
			UserName = userName;
			Password = password;
			IsPrimary = isPrimary;
			Description = description;
		}

		/// <summary>
		/// MachineInPool class의 새 인스턴스를 초기화 합니다.
		/// </summary>
		/// <param name="name"></param>
		/// <param name="domain"></param>
		/// <param name="userName"></param>
		/// <param name="password"></param>
		public MachineInPool(string name, string domain, string userName, string password)
			: this(name, String.Empty, userName, password, false, string.Empty)
		{
		}

		/// <summary>
		/// MachineInPool class의 새 인스턴스를 초기화 합니다.
		/// </summary>
		/// <param name="name"></param>
		/// <param name="userName"></param>
		/// <param name="password"></param>
		public MachineInPool(string name, string userName, string password)
			: this(name, String.Empty, userName, password)
		{
		}

		/// <summary>
		/// MachineInPool class의 새 인스턴스를 초기화 합니다.
		/// </summary>
		/// <param name="name"></param>
		public MachineInPool(string name)
			: this(name, String.Empty, String.Empty)
		{
		}

		/// <summary>
		/// MachineInPool 클래스의 새 인스턴스를 초기화 합니다.
		/// </summary>
		public MachineInPool()
		{
		}
		#endregion

		/// <summary>
		/// Name를 구하거나 설정합니다.
		/// </summary>
		[ConfigurationProperty("name", IsRequired = true, IsKey = true)]
		[Description("Name를 구하거나 설정합니다.")]
		public string Name
		{
			get
			{
				return (string)this["name"];
			}
			set
			{
				this["name"] = value;
			}
		}

		/// <summary>
		/// DomainName를 구하거나 설정합니다.
		/// </summary>
		[ConfigurationProperty("domainName", IsRequired = false)]
		[Description("DomainName를 구하거나 설정합니다.")]
		public string DomainName
		{
			get
			{
				return (string)this["domainName"];
			}
			set
			{
				this["domainName"] = value;
			}
		}

		/// <summary>
		/// UserName를 구하거나 설정합니다.
		/// </summary>
		[ConfigurationProperty("userName", IsRequired = true)]
		[Description("UserName를 구하거나 설정합니다.")]
		public string UserName
		{
			get
			{
				return (string)this["userName"];
			}
			set
			{
				this["userName"] = value;
			}
		}

		/// <summary>
		/// Password를 구하거나 설정합니다.
		/// </summary>
		[ConfigurationProperty("password", IsRequired = true)]
		[Description("Password를 구하거나 설정합니다.")]
		public string Password
		{
			get
			{
				return (string)this["password"];
			}
			set
			{
				this["password"] = value;
			}
		}

		/// <summary>
		/// IsPrimary를 구하거나 설정합니다.
		/// </summary>
		[ConfigurationProperty("isPrimary", DefaultValue = "false", IsRequired = false)]
		[Description("IsPrimary를 구하거나 설정합니다.")]
		public bool IsPrimary
		{
			get
			{
				return (bool)this["isPrimary"];
			}
			set
			{
				this["isPrimary"] = value;
			}
		}

		/// <summary>
		/// Description를 구하거나 설정합니다.
		/// </summary>
		[ConfigurationProperty("description", IsRequired = false)]
		[Description("Description를 구하거나 설정합니다.")]
		public string Description
		{
			get
			{
				return (string)this["description"];
			}
			set
			{
				this["description"] = value;
			}
		}
	}
}
