﻿/**********************************************************************************************************************/
/*	Domain		:	WindowsServicePooler.Configuration.ConfigWrapperBase
/*	Creator		:	KIMKIWON\xyz37(김기원)
/*	Create		:	2011년 11월 24일 목요일 오전 9:33
/*	Purpose		:	설정 파일의 Wrapper에 관련된 기본 클래스 입니다.
/*--------------------------------------------------------------------------------------------------------------------*/
/*	Modifier	:	
/*	Update		:	
/*	Changes		:	
/*--------------------------------------------------------------------------------------------------------------------*/
/*	Comment		:	
/*--------------------------------------------------------------------------------------------------------------------*/
/*	Reviewer	:	Kim Ki Won
/*	Rev. Date	:	2011년 11월 24일 목요일 오전 9:33
/**********************************************************************************************************************/

using System;
using System.Collections.Generic;
using System.Text;
using System.Configuration;
using System.Text.RegularExpressions;

namespace WindowsServicePooler.Configuration
{
	/// <summary>
	/// 설정 파일의 Wrapper에 관련된 기본 클래스 입니다.
	/// </summary>
	public abstract class ConfigWrapperBase
	{
		/// <summary>
		/// 설정 파일을 처리하는 Instance 입니다.
		/// </summary>
		protected System.Configuration.Configuration Config;
		/// <summary>
		/// 설정 파일이 읽은 Section의 이름입니다.
		/// </summary>
		protected abstract string SectionName
		{
			get;
		}

		/// <summary>
		/// Refresh 할 경우 해당 Section을 refresh할 방법을 결정합니다.
		/// </summary>
		/// <param name="config">다시 읽어들인 configuration</param>
		protected abstract void RefreshGroupMethod(System.Configuration.Configuration config);

		/// <summary>
		/// LoginWrapper class의 Single Ton BufferSize을 위한 생성자 입니다.
		/// </summary>
		/// <param name="exePath">실행 파일과 연결된 구성 파일에 대한 경로입니다.
		/// <remarks>string.Empty로 지정되면 실행파일명.config를 읽습니다.</remarks></param>
		/// <returns></returns>
		protected virtual System.Configuration.Configuration GetConfig(string exePath)
		{
			if (string.IsNullOrEmpty(exePath) == true)
				Config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
			else
			{
				string extension = System.IO.Path.GetExtension(exePath).Replace(".", string.Empty).ToLower();

				if (extension == "config")
				{
					ExeConfigurationFileMap configFile = new ExeConfigurationFileMap();

					configFile.ExeConfigFilename = exePath;

					Config = ConfigurationManager.OpenMappedExeConfiguration(configFile, ConfigurationUserLevel.None);
				}
				else
					Config = ConfigurationManager.OpenExeConfiguration(exePath);
			}

			return Config;
		}

		/// <summary>
		/// 읽어들인 Config의 FilePath를 구합니다.
		/// </summary>
		public string ConfigFilePath
		{
			get
			{
				return Config.FilePath;
			}
		}

		/// <summary>
		/// Config 파일에 변경 사항만을 저장합니다.
		/// </summary>		
		public void SaveModifed()
		{
			Config.Save(ConfigurationSaveMode.Modified);
		}

		/// <summary>
		/// Config 파일에 저장 합니다.
		/// </summary>
		public void Save()
		{
			Config.Save();
		}

		/// <summary>
		/// Config 파일의 해당 Section을 다시 읽어들입니다.
		/// </summary>
		/// <param name="sectionName">다시 읽어들일 section Name 입니다.</param>
		public void Refresh()
		{
			try
			{
				ConfigurationManager.RefreshSection(SectionName);
				RefreshGroupMethod(GetConfig(ConfigFilePath));
			}
			catch
			{
			}
		}
	}
}
