﻿//#define CREATE_CONFIG
/**********************************************************************************************************************/
/*	Domain		:	WindowsServicePooler.Configuration.ConfigWrapper
/*	Creator		:	KIMKIWON\xyz37(김기원)
/*	Create		:	2011년 11월 24일 목요일 오전 9:34
/*	Purpose		:	설정 파일에 관련된 Wrapper 클래스 입니다.
/*--------------------------------------------------------------------------------------------------------------------*/
/*	Modifier	:	
/*	Update		:	
/*	Changes		:	
/*--------------------------------------------------------------------------------------------------------------------*/
/*	Comment		:	
/*--------------------------------------------------------------------------------------------------------------------*/
/*	Reviewer	:	Kim Ki Won
/*	Rev. Date	:	2011년 11월 24일 목요일 오전 10:22
/**********************************************************************************************************************/

using System;
using System.Collections.Generic;
using System.Text;
using System.Configuration;
using System.Text.RegularExpressions;

namespace WindowsServicePooler.Configuration
{
	/// <summary>
	/// 설정 파일에 관련된 Wrapper 클래스 입니다.
	/// </summary>
	public class ConfigWrapper : ConfigWrapperBase
	{
		#region Fields
		private Section _section;
		#endregion

		#region Use Singleton Wrapper
		private static ConfigWrapper _newInstance;

		#region Constructor for Single Ton Wrapper
		/// <summary>
		/// Wrapper class의 Single Ton BufferSize을 위한 생성자 입니다.
		/// </summary>
		/// <returns></returns>
		private ConfigWrapper(string exePath)
		{
			_newInstance = null;
			Config = GetConfig(exePath);

			Sections group = Config.SectionGroups[Sections.SectionGroupNameString] as Sections;

			// SectionGroup 정보를 생성한다.
			if (group == null)
			{
				group = new Sections();

				Config.SectionGroups.Add(Sections.SectionGroupNameString, group);
			}

			_section = group.Section;

#if CREATE_CONFIG
			if (_section == null)
			{
				_section = new Section();
				group.Sections.Add(Section.SectionNameString, _section);

				_section.EnableLogging = true;
				_section.LogFilePath = @"C:\Program Files\Services\WindowsServicePooler.log";

				MachineInPoolCollection machinesInPool1 = new MachineInPoolCollection();
				MachineInPoolCollection machinesInPool2 = new MachineInPoolCollection();

				machinesInPool1.Add(new MachineInPool("192.168.0.1",
									string.Empty,
									"administrator",
									"1234",
									true,
									"Server1"));
				machinesInPool1.Add(new MachineInPool("192.168.0.2",
									string.Empty,
									"administrator",
									"4567",
									false,
									"Server2"));
				machinesInPool1.Add(new MachineInPool("192.168.0.3",
									string.Empty,
									"administrator",
									"1234",
									true,
									"Server3"));
				machinesInPool1.Add(new MachineInPool("192.168.0.4",
									string.Empty,
									"administrator",
									"4567",
									false,
									"Server4"));

				_section.Services.Add(new Service
				{
					Name = "ServiceName1",
					AlternativeMode = true,
					Interval = 5000,
					MachinesInPool = machinesInPool1,
				});
				_section.Services.Add(new Service
				{
					Name = "ServiceName2",
					AlternativeMode = true,
					Interval = 3000,
					MachinesInPool = machinesInPool2,
				});
			}

			SaveModifed();
#endif
		}

		/// <summary>
		/// Refresh 할 경우 해당 Section을 refresh할 방법을 결정합니다.
		/// </summary>
		/// <param name="config">다시 읽어들인 configuration</param>
		protected override void RefreshGroupMethod(System.Configuration.Configuration config)
		{
			Sections group = config.SectionGroups[Sections.SectionGroupNameString] as Sections;

			if (group != null)
				_section = group.Section;
		}
		#endregion

		#region GetInstance
		/// <summary>
		/// Wrapper class의 Single Ton을 위한 새 인스턴스를 초기화 합니다.
		/// 
		/// </summary>
		/// <returns></returns>
		public static ConfigWrapper GetInstance()
		{
			string exePath = String.Empty;

			return GetInstance(exePath);
		}

		/// <summary>
		/// Wrapper class의 Single Ton을 위한 새 인스턴스를 초기화 합니다.
		/// <param name="exePath">실행 파일과 연결된 구성 파일에 대한 경로입니다.</param>
		/// </summary>
		/// <returns></returns>
		public static ConfigWrapper GetInstance(string exePath)
		{
			if (_newInstance == null)
				_newInstance = new ConfigWrapper(exePath);

			return _newInstance;
		}

		/// <summary>
		/// Wrapper class의 초기화된 인스턴스를 구합니다.
		/// <remarks>기본 config는 현재 실행 파일이름.config 입니다.</remarks>
		/// </summary>
		public static ConfigWrapper Instance
		{
			get
			{
				if (_newInstance == null)
					GetInstance();

				return _newInstance;
			}
		}

		/// <summary>
		/// WindowsService Pooler의 설정 section을 구합니다.
		/// </summary>
		public Section Section
		{
			get
			{
				return Instance._section;
			}
		}
		#endregion
		#endregion

		/// <summary>
		/// 설정 파일이 읽은 Section의 이름입니다.
		/// </summary>
		protected override string SectionName
		{
			get
			{
				return Section.SectionNameString;
			}
		}
	}
}
