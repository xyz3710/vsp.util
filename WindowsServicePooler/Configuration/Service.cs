﻿/**********************************************************************************************************************/
/*	Domain		:	WindowsServicePooler.Configuration.Service
/*	Creator		:	KIMKIWON\xyz37(김기원)
/*	Create		:	2011년 11월 24일 목요일 오전 9:16
/*	Purpose		:	서비스 Pool을 구성할 서비스 정보를 구성합니다.
/*--------------------------------------------------------------------------------------------------------------------*/
/*	Modifier	:	
/*	Update		:	
/*	Changes		:	
/*--------------------------------------------------------------------------------------------------------------------*/
/*	Comment		:	
/*--------------------------------------------------------------------------------------------------------------------*/
/*	Reviewer	:	Kim Ki Won
/*	Rev. Date	:	2011년 11월 24일 목요일 오전 10:19
/**********************************************************************************************************************/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;
using System.ComponentModel;
using System.Diagnostics;

namespace WindowsServicePooler.Configuration
{
	/// <summary>
	/// 서비스 Pool을 구성할 서비스 정보를 구성합니다.
	/// </summary>
	[DebuggerDisplay("Name:{Name}, AlternativeMode:{AlternativeMode}, MachinesInPool:{MachinesInPool}", Name = "Service")]
	public class Service : ConfigurationElement
	{
		#region Constructors
		/// <summary>
		/// Service class의 새 인스턴스를 초기화 합니다.
		/// </summary>
		/// <param name="name"></param>
		/// <param name="alternativeMode"></param>
		public Service(string name, bool alternativeMode)
		{
			Name = name;
			AlternativeMode = alternativeMode;
		}

		/// <summary>
		/// Service class의 새 인스턴스를 초기화 합니다.
		/// </summary>
		/// <param name="name"></param>
		public Service(string name)
			: this(name, false)
		{
		}

		/// <summary>
		/// Service class의 새 인스턴스를 초기화 합니다.
		/// </summary>
		public Service()
		{
		}
		#endregion

		/// <summary>
		/// 서비스 이름이나 표시 이름을 구하거나 설정합니다.
		/// </summary>
		[ConfigurationProperty("name", IsRequired = true, IsKey = true)]
		[Description("서비스 이름이나 표시 이름을 구하거나 설정합니다.")]
		public string Name
		{
			get
			{
				return (string)this["name"];
			}
			set
			{
				this["name"] = value;
			}
		}

		/// <summary>
		/// 한개의 서비스만 번갈아가면서 실행할지 여부를 구하거나 설정합니다.
		/// </summary>
		[ConfigurationProperty("alternativeMode", DefaultValue = "true", IsRequired = false)]
		[Description("AlternativeMode를 구하거나 설정합니다.")]
		public bool AlternativeMode
		{
			get
			{
				return (bool)this["alternativeMode"];
			}
			set
			{
				this["alternativeMode"] = value;
			}
		}

		/// <summary>
		/// MachinesInPool를 구하거나 설정합니다.
		/// </summary>
		[ConfigurationProperty("machinesInPool", IsRequired = true)]
		[Description("MachinesInPool를 구하거나 설정합니다.")]
		public MachineInPoolCollection MachinesInPool
		{
			get
			{
				return (MachineInPoolCollection)this["machinesInPool"];
			}
			set
			{
				this["machinesInPool"] = value;
			}
		}
	}
}
