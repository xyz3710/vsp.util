﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Reflection;
using System.IO;
using System.Text.RegularExpressions;

namespace DiagnosticSource
{
	internal class Program
	{
		#region Constants
		private const string BAR_TEXT = "========================================";
		private const string LOG_FILE = "DiagnosticSource.txt";
		private const string CLASS_NAME_KEY = "ClassName";
		private const string BASECLASS_NAME_KEY = "BaseClass";
		#endregion

		private static Regex _regexClassName;
		private static Regex _regexLineCount;

		static void Main(string[] args)
		{
			long totalServerCount = 0L;
			long totalServerLines = 0L;
			long totalClientCount = 0L;
			long totalClientLines = 0L;

			const string ROOT_PATH = @"D:\iSet-DA.ESG";
			List<string> serverFolders = new List<string>();
			List<string> clientFolders = new List<string>();

			serverFolders.Add("Framework.iDoIt");
			serverFolders.Add("Framework.Server");

			clientFolders.Add("Framework.Win");
			clientFolders.Add("Win");

			_regexClassName = new Regex(String.Format(@"[\w\t]*(?:class|interface|enum)[\s]+(?<{0}>[\w\d]*)[ :]*(?<{1}>[\w\d,\<\> ]*)\s*\{{",
														 CLASS_NAME_KEY,
														 BASECLASS_NAME_KEY), RegexOptions.Compiled);
			_regexLineCount = new Regex(@"\r\n", RegexOptions.Compiled);

			if (File.Exists(Path.GetFullPath(LOG_FILE)) == true)
				File.Delete(Path.GetFullPath(LOG_FILE));

			ShowTitle();

			foreach (string serverFolder in serverFolders)
			{
				string path = Path.Combine(ROOT_PATH, serverFolder);
				
				CheckSource(ref totalServerCount, ref totalServerLines, null, path);
			}

			List<string> exceptFiles = new List<string>();

			exceptFiles.Add("Resources.Designer.cs");
			exceptFiles.Add("Resource.cs");
			exceptFiles.Add("Settings.Designer.cs");
			exceptFiles.Add("AssemblyInfo.cs");

			foreach (string clientFolder in clientFolders)
			{
				string path = Path.Combine(ROOT_PATH, clientFolder);

				CheckSource(ref totalClientCount, ref totalClientLines, null, path);
			}
			
			StringBuilder diagnosticsMessage = new StringBuilder();

			diagnosticsMessage.AppendLine(BAR_TEXT);
			diagnosticsMessage.AppendLine(string.Format("Server : {0:#,##0} .cs files, {1:#,##0} lines", 
                                              totalServerCount, 
                                              totalServerLines));
			diagnosticsMessage.AppendLine(string.Format("Client : {0:#,##0} .cs files, {1:#,##0} lines", 
                                              totalClientCount, 
                                              totalClientLines));
			diagnosticsMessage.AppendLine(BAR_TEXT);
			diagnosticsMessage.AppendLine(string.Format("Total : {0:#,##0} .cs files, {1:#,##0} lines", 
                                              totalServerCount + totalClientCount, 
                                              totalServerLines + totalClientLines));
			diagnosticsMessage.AppendLine(BAR_TEXT);

			Logging(diagnosticsMessage.ToString());
		}

		private static void Logging(string log)
		{
			Console.WriteLine(log);

			FileStream fsLog = null;
			string logFilePath = Path.GetFullPath(LOG_FILE);

			try
			{
				fsLog = File.Open(logFilePath, FileMode.Append, FileAccess.Write, FileShare.ReadWrite);

				if (fsLog != null)
				{
					lock (fsLog)
					{
						string context = string.Format("{0}\r\n", log);
						byte[] contextBytes = Encoding.Default.GetBytes(context);

						fsLog.Write(contextBytes, 0, contextBytes.Length);
					}
				}
			}
			catch
			{
			}
			finally
			{
				if (fsLog != null)
				{
					fsLog.Flush();
					fsLog.Close();
				}
			}
		}

		private static void CheckSource(ref long totalCount, ref long totalLines, List<string> exceptFiles, string path)
		{
			foreach (String file in Directory.GetFiles(path, "*.cs", SearchOption.AllDirectories))
			{
				string fileName = Path.GetFileName(file);
				string className = string.Empty;
				string baseClass = string.Empty;
				int lineCount = 0;
				StringBuilder context = new StringBuilder();

				if (exceptFiles != null && CheckExceptFile(exceptFiles, fileName) == true)
					continue;

				string source = GetSource(file);

				if (_regexClassName.IsMatch(source) == false)
					continue;

				className = _regexClassName.Match(source).Groups[CLASS_NAME_KEY].Value;
				baseClass = _regexClassName.Match(source).Groups[BASECLASS_NAME_KEY].Value;
				lineCount = _regexLineCount.Matches(source).Count;
				totalLines += lineCount;

				context.AppendFormat("{0:0000000} {1}\r\n", ++totalCount, fileName);
				context.AppendFormat("\t{0}, {1}{2}",
					className,
					lineCount,
					baseClass != string.Empty ?
		                        string.Format(", {0}", baseClass) :
		                        string.Empty);

				Logging(context.ToString());
			}
		}

		private static void ShowTitle()
		{
			StringBuilder title = new StringBuilder();

			title.AppendLine(BAR_TEXT);
			title.AppendLine("0000000 <Filename>");
			title.AppendLine("<Class Name>, <Line Count>, <Base Class>");
			title.AppendLine(BAR_TEXT);

			Logging(title.ToString());
		}

		private static string GetSource(string file)
		{
			FileStream fileStream = null;
			StreamReader streamReader = null;
			string context = string.Empty;

			try
			{
				fileStream = new FileStream(file, FileMode.Open, FileAccess.Read, FileShare.Read);

				if (fileStream != null)
				{
					streamReader = new StreamReader(fileStream);

					if (streamReader != null)
						context = streamReader.ReadToEnd();
				}
			}
			finally
			{
				if (streamReader != null)
					streamReader.Close();

				if (fileStream != null)
					fileStream.Close();
			}

			return context;
		}

		private static bool CheckExceptFile(List<string> exceptFiles, string fileName)
		{
			return exceptFiles.Exists(delegate(string list)
			{
				return (list == fileName);
			});
		}
	}
}
