﻿namespace ReplaceText
{
	partial class Form1
	{
		/// <summary>
		/// 필수 디자이너 변수입니다.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// 사용 중인 모든 리소스를 정리합니다.
		/// </summary>
		/// <param name="disposing">관리되는 리소스를 삭제해야 하면 true이고, 그렇지 않으면 false입니다.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form 디자이너에서 생성한 코드

		/// <summary>
		/// 디자이너 지원에 필요한 메서드입니다.
		/// 이 메서드의 내용을 코드 편집기로 수정하지 마십시오.
		/// </summary>
		private void InitializeComponent()
		{
			this.panel1 = new System.Windows.Forms.Panel();
			this.chkSubfolder = new System.Windows.Forms.CheckBox();
			this.btnReplace = new System.Windows.Forms.Button();
			this.btnPath = new System.Windows.Forms.Button();
			this.btnSearch = new System.Windows.Forms.Button();
			this.txtReplace = new System.Windows.Forms.TextBox();
			this.lblCnt = new System.Windows.Forms.Label();
			this.label5 = new System.Windows.Forms.Label();
			this.label4 = new System.Windows.Forms.Label();
			this.label2 = new System.Windows.Forms.Label();
			this.txtPath = new System.Windows.Forms.TextBox();
			this.txtExt = new System.Windows.Forms.TextBox();
			this.txtSearch = new System.Windows.Forms.TextBox();
			this.label3 = new System.Windows.Forms.Label();
			this.label1 = new System.Windows.Forms.Label();
			this.panel2 = new System.Windows.Forms.Panel();
			this.lstResult = new System.Windows.Forms.ListBox();
			this.fbdPath = new System.Windows.Forms.FolderBrowserDialog();
			this.panel1.SuspendLayout();
			this.panel2.SuspendLayout();
			this.SuspendLayout();
			// 
			// panel1
			// 
			this.panel1.Controls.Add(this.chkSubfolder);
			this.panel1.Controls.Add(this.btnReplace);
			this.panel1.Controls.Add(this.btnPath);
			this.panel1.Controls.Add(this.btnSearch);
			this.panel1.Controls.Add(this.txtReplace);
			this.panel1.Controls.Add(this.lblCnt);
			this.panel1.Controls.Add(this.label5);
			this.panel1.Controls.Add(this.label4);
			this.panel1.Controls.Add(this.label2);
			this.panel1.Controls.Add(this.txtPath);
			this.panel1.Controls.Add(this.txtExt);
			this.panel1.Controls.Add(this.txtSearch);
			this.panel1.Controls.Add(this.label3);
			this.panel1.Controls.Add(this.label1);
			this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
			this.panel1.Location = new System.Drawing.Point(0, 0);
			this.panel1.Name = "panel1";
			this.panel1.Size = new System.Drawing.Size(1016, 103);
			this.panel1.TabIndex = 4;
			// 
			// chkSubfolder
			// 
			this.chkSubfolder.AutoSize = true;
			this.chkSubfolder.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
			this.chkSubfolder.Checked = true;
			this.chkSubfolder.CheckState = System.Windows.Forms.CheckState.Checked;
			this.chkSubfolder.Location = new System.Drawing.Point(216, 47);
			this.chkSubfolder.Name = "chkSubfolder";
			this.chkSubfolder.Size = new System.Drawing.Size(118, 16);
			this.chkSubfolder.TabIndex = 7;
			this.chkSubfolder.Text = "하위폴더(&S) 포함";
			this.chkSubfolder.UseVisualStyleBackColor = true;
			// 
			// btnReplace
			// 
			this.btnReplace.Location = new System.Drawing.Point(920, 71);
			this.btnReplace.Name = "btnReplace";
			this.btnReplace.Size = new System.Drawing.Size(84, 23);
			this.btnReplace.TabIndex = 5;
			this.btnReplace.Text = "바꾸기(&H)";
			this.btnReplace.UseVisualStyleBackColor = true;
			this.btnReplace.Click += new System.EventHandler(this.btnReplace_Click);
			// 
			// btnPath
			// 
			this.btnPath.Location = new System.Drawing.Point(920, 12);
			this.btnPath.Name = "btnPath";
			this.btnPath.Size = new System.Drawing.Size(84, 23);
			this.btnPath.TabIndex = 0;
			this.btnPath.Text = "경로찾기(&P)";
			this.btnPath.UseVisualStyleBackColor = true;
			this.btnPath.Click += new System.EventHandler(this.btnPath_Click);
			// 
			// btnSearch
			// 
			this.btnSearch.Location = new System.Drawing.Point(920, 44);
			this.btnSearch.Name = "btnSearch";
			this.btnSearch.Size = new System.Drawing.Size(84, 23);
			this.btnSearch.TabIndex = 3;
			this.btnSearch.Text = "찾기(&F)";
			this.btnSearch.UseVisualStyleBackColor = true;
			this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click);
			// 
			// txtReplace
			// 
			this.txtReplace.Location = new System.Drawing.Point(447, 71);
			this.txtReplace.Name = "txtReplace";
			this.txtReplace.Size = new System.Drawing.Size(467, 21);
			this.txtReplace.TabIndex = 4;
			// 
			// lblCnt
			// 
			this.lblCnt.AutoSize = true;
			this.lblCnt.Location = new System.Drawing.Point(98, 74);
			this.lblCnt.Name = "lblCnt";
			this.lblCnt.Size = new System.Drawing.Size(27, 12);
			this.lblCnt.TabIndex = 4;
			this.lblCnt.Text = "0 건";
			// 
			// label5
			// 
			this.label5.AutoSize = true;
			this.label5.Location = new System.Drawing.Point(11, 74);
			this.label5.Name = "label5";
			this.label5.Size = new System.Drawing.Size(85, 12);
			this.label5.TabIndex = 4;
			this.label5.Text = "찾은 파일 수 : ";
			// 
			// label4
			// 
			this.label4.AutoSize = true;
			this.label4.Location = new System.Drawing.Point(11, 45);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(83, 12);
			this.label4.TabIndex = 4;
			this.label4.Text = "화일 종류(&E) :";
			this.label4.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
			// 
			// label2
			// 
			this.label2.AutoSize = true;
			this.label2.Location = new System.Drawing.Point(347, 74);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(94, 12);
			this.label2.TabIndex = 4;
			this.label2.Text = "바꿀 문자열(&L) :";
			// 
			// txtPath
			// 
			this.txtPath.Location = new System.Drawing.Point(100, 12);
			this.txtPath.Name = "txtPath";
			this.txtPath.Size = new System.Drawing.Size(814, 21);
			this.txtPath.TabIndex = 6;
			this.txtPath.TextChanged += new System.EventHandler(this.txtPath_TextChanged);
			// 
			// txtExt
			// 
			this.txtExt.Location = new System.Drawing.Point(100, 42);
			this.txtExt.Name = "txtExt";
			this.txtExt.Size = new System.Drawing.Size(100, 21);
			this.txtExt.TabIndex = 1;
			this.txtExt.Text = "*.*";
			// 
			// txtSearch
			// 
			this.txtSearch.Location = new System.Drawing.Point(447, 44);
			this.txtSearch.Name = "txtSearch";
			this.txtSearch.Size = new System.Drawing.Size(467, 21);
			this.txtSearch.TabIndex = 2;
			// 
			// label3
			// 
			this.label3.AccessibleDescription = "m m ";
			this.label3.AutoSize = true;
			this.label3.Location = new System.Drawing.Point(11, 15);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(84, 12);
			this.label3.TabIndex = 3;
			this.label3.Text = "찾을 경로(&O) :";
			// 
			// label1
			// 
			this.label1.AutoSize = true;
			this.label1.Location = new System.Drawing.Point(347, 47);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(96, 12);
			this.label1.TabIndex = 3;
			this.label1.Text = "찾을 문자열(&N) :";
			// 
			// panel2
			// 
			this.panel2.Controls.Add(this.lstResult);
			this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
			this.panel2.Location = new System.Drawing.Point(0, 103);
			this.panel2.Name = "panel2";
			this.panel2.Size = new System.Drawing.Size(1016, 616);
			this.panel2.TabIndex = 5;
			// 
			// lstResult
			// 
			this.lstResult.Dock = System.Windows.Forms.DockStyle.Fill;
			this.lstResult.FormattingEnabled = true;
			this.lstResult.ItemHeight = 12;
			this.lstResult.Location = new System.Drawing.Point(0, 0);
			this.lstResult.Name = "lstResult";
			this.lstResult.ScrollAlwaysVisible = true;
			this.lstResult.Size = new System.Drawing.Size(1016, 616);
			this.lstResult.TabIndex = 0;
			// 
			// fbdPath
			// 
			this.fbdPath.ShowNewFolderButton = false;
			// 
			// Form1
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(1016, 719);
			this.Controls.Add(this.panel2);
			this.Controls.Add(this.panel1);
			this.Name = "Form1";
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Text = "Replace Text";
			this.Load += new System.EventHandler(this.Form1_Load);
			this.panel1.ResumeLayout(false);
			this.panel1.PerformLayout();
			this.panel2.ResumeLayout(false);
			this.ResumeLayout(false);

		}

		#endregion

		private System.Windows.Forms.Panel panel1;
		private System.Windows.Forms.Button btnReplace;
		private System.Windows.Forms.Button btnSearch;
		private System.Windows.Forms.TextBox txtReplace;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.TextBox txtSearch;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Panel panel2;
		private System.Windows.Forms.ListBox lstResult;
		private System.Windows.Forms.Button btnPath;
		private System.Windows.Forms.TextBox txtPath;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.CheckBox chkSubfolder;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.TextBox txtExt;
		private System.Windows.Forms.Label lblCnt;
		private System.Windows.Forms.Label label5;
		private System.Windows.Forms.FolderBrowserDialog fbdPath;
	}
}

