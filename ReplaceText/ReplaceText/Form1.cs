﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Collections;

namespace ReplaceText
{
	public partial class Form1 : Form
	{
		private ArrayList _alFileContext = new ArrayList();
		private byte[] _utf8 = new byte[] { 0xff, 0xfe };
		private byte[] _signedUTF8 = new byte[] { 0xef, 0xbb, 0xbf };

		public Form1()
		{
			InitializeComponent();
		}

		private void btnPath_Click(object sender, EventArgs e)
		{
			DialogResult result = fbdPath.ShowDialog();

			if (result == DialogResult.OK)
				txtPath.Text = fbdPath.SelectedPath;

			if (fbdPath.SelectedPath != string.Empty)
				btnSearch.Enabled = true;

			txtSearch.Focus();
		}

		private void btnSearch_Click(object sender, EventArgs e)
		{
			string path = txtPath.Text;
			try
			{
				string[] files = Directory.GetFiles(path, txtExt.Text, chkSubfolder.Checked == true? SearchOption.AllDirectories : SearchOption.TopDirectoryOnly);

				lstResult.Items.Clear();
				lblCnt.Text = "0 건";

				for (int fileCnt = 0; fileCnt < files.Length; fileCnt++)
				{
					using (FileStream fSteam = File.OpenRead(files[fileCnt]))
					{
						try
						{
							long streamLength = fSteam.Length;
							byte[] readData = new byte[streamLength];
							int readLength = fSteam.Read(readData, 0, (int)streamLength);
							
							if (readLength > 0)
							{
								string text = Encoding.UTF8.GetString(readData);

								if (text.IndexOf(txtSearch.Text) > 0)
									lstResult.Items.Add(files[fileCnt]);
							}
						}
						finally
						{
							fSteam.Close();
						}
					};
				}
			}
			catch (Exception ex)
			{
				MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);

				return;
			}

			lblCnt.Text = string.Format("{0} 건", lstResult.Items.Count.ToString());

			if (lstResult.Items.Count > 0)
				btnReplace.Enabled = true;
			else
			{
				btnReplace.Enabled = false;
				MessageBox.Show("찾은 파일이 없습니다.", "File not found", MessageBoxButtons.OK, MessageBoxIcon.Information);
			}
		}

		private void btnReplace_Click(object sender, EventArgs e)
		{
			for (int fileCnt = 0; fileCnt < lstResult.Items.Count; fileCnt++)
			{
				using (FileStream fStream = File.Open(lstResult.Items[fileCnt].ToString(), FileMode.OpenOrCreate, FileAccess.ReadWrite))
				{
					bool result = false;
					
					try
					{
						string text = string.Empty;
						int streamLength = (int)fStream.Length;
						byte[] readData = new byte[streamLength];
						int readLength = fStream.Read(readData, 0, streamLength);
						int unicodeHeaderLength = getUTF8HeaderLength(readData);

						readLength -= unicodeHeaderLength;

						if (readLength > 0)
							text = Encoding.UTF8.GetString(readData, unicodeHeaderLength, readLength);
						else
							throw new IOException();

						if (text.IndexOf(txtSearch.Text) > 0 && text.Length == readLength)
						{
							string data = text.Replace(@txtSearch.Text, @txtReplace.Text);
							byte[] writedData = Encoding.UTF8.GetBytes(data);

							try
							{
								fStream.Seek(0, SeekOrigin.Begin);

								if (unicodeHeaderLength > 0)
									fStream.Write(readData, 0, unicodeHeaderLength);

								fStream.Write(writedData, 0, data.Length);
								fStream.SetLength(unicodeHeaderLength + data.Length);
								fStream.Flush();

								result = true;
							}
							catch (Exception ex)
							{
								txtPath.Text = ex.Message;
								result = false;
							}
						}
					}
					finally
					{
						string filePath = lstResult.Items[fileCnt].ToString();

						lstResult.Items.RemoveAt(fileCnt);
						lstResult.Items.Insert(fileCnt, string.Format("[{0}]:{1}", result == true ? "OK     " : "Failure", filePath));

						fStream.Close();
					}
				};
			}

			btnReplace.Enabled = false;
		}

		private int getUTF8HeaderLength(byte[] data)
		{
			bool check = false;
			int length = 0;

			for (int i = 0; i < _utf8.Length; i++)
			{
				if (data[i] == _utf8[i])
					check = true;
				else
					check = false;
			}

			if (check == true)
				length = _utf8.Length;

			for (int i = 0; i < _signedUTF8.Length; i++)
			{
				if (data[i] == _signedUTF8[i])
					check = true;
				else
					check = false;
			}

			if (check == true)
				length = _signedUTF8.Length;

			return length; 
		}

		private void Form1_Load(object sender, EventArgs e)
		{
			btnSearch.Enabled = false;
			btnReplace.Enabled = false;

			txtPath.Text = @"G:\";
			txtExt.Text = "*.cs";
			txtSearch.Text = "Deploy";
			txtReplace.Text = "";
		}

		private void txtPath_TextChanged(object sender, EventArgs e)
		{
			if (txtPath.Text != string.Empty)
				btnSearch.Enabled = true;
		}
	}
}