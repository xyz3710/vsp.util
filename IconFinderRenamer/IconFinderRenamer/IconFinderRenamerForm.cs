﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Xml.Linq;
using System.Xml;
using System.Data.Linq;
using System.Threading;
using System.Threading.Tasks;
namespace IconFinderRenamer
{
	public partial class IconFinderRenamerForm : Form
	{
		public IconFinderRenamerForm()
		{
			InitializeComponent();
			tbLocation.Text = @"E:\IconFinder";
		}

		private void btnFolder_Click(object sender, EventArgs e)
		{
			if (fbdFolder.ShowDialog(this) == System.Windows.Forms.DialogResult.OK)
			{
				tbLocation.Text = fbdFolder.SelectedPath;
			}
		}

		private void btnStart_Click(object sender, EventArgs e)
		{
			var sourceFolder = tbLocation.Text;
			var files = Directory.GetFiles(sourceFolder);
			progressBar.Maximum = files.Length;
			var textInfo = new System.Globalization.CultureInfo("ko-KR", false).TextInfo;

			for (int i = 0; i < files.Length; i++)
			{
				var file = files[i];
				var ext = Path.GetExtension(file);
				var name = Path.GetFileNameWithoutExtension(file);
				var fileName = string.Format("{0}{1}", name, ext.ToLower());

				/*
				if (ext.ToLower() != ".ic")
				{
					continue;
				}

				var name = Path.GetFileNameWithoutExtension(file);
				var fileName = string.Empty;

				using (var bitmap = new Bitmap(file))
				{
					fileName = string.Format("{0}{1}{2}", name, bitmap.Height, ext);
				}
				*/

				try
				{
					WriteLog(string.Format("{0}\t{1}", name, fileName));
					File.Move(file, Path.Combine(sourceFolder, fileName));
				}
				catch (Exception ex)
				{
					WriteLog(string.Format("\t{0}", ex.Message));
				}
				progressBar.Value = i;
			}
			/*
			await Task.Factory.StartNew(() =>
			{
				for (int i = 0; i < files.Length; i++)
				{
					var file = files[i];
					var names = file.Split('_');
					var targetFilename = names.Skip(1).Aggregate((acc, next) =>
					{
						var str = string.Format("{0}_{1}", textInfo.ToTitleCase(acc), textInfo.ToTitleCase(next));

						return str;
					});

					try
					{
						WriteLog(string.Format("{0}\t{1}", Path.GetFileName(file), targetFilename));
						File.Move(file, Path.Combine(sourceFolder, targetFilename));
					}
					catch (Exception ex)
					{
						WriteLog(string.Format("\t{0}", ex.Message));
					}
				}
			});
			*/
		}

		private void WriteLog(string messgae)
		{
			Invoke(new MethodInvoker(() =>
			{
				tbLog.AppendText(messgae + Environment.NewLine);
			}));
		}
	}
}
