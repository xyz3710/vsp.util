﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using System.IO;
using System.Drawing.Imaging;


namespace RServer
{
	class ThServer
	{
		// Fields
		private Bitmap cimg;
		private Graphics g;
		private string IP;
		private TcpListener listener;
		private int MAX;
		private string port;
		private Size sz;
		private Thread td;
		// Methods
		public ThServer()
		{
			this.MAX = 0x2800;
		}
		public void Cap_data(Bitmap cimg, Graphics g, Size sz, string IP, string port)
		{
			this.cimg = cimg;
			this.g = g;
			this.sz = sz;
			this.IP = IP;
			this.port = port;
		}
		public void listener_stop()
		{
			this.listener.Stop();
		}
		public void Rc_Thread(Socket s)
		{
			RevClient client = new RevClient();
			client.Client_Rev_init(s);
			ThreadStart start = new ThreadStart(client.Client_Rev);
			this.td = new Thread(start);
			this.td.Start();
		}
		public byte[] Screen_Capture()
		{
			MemoryStream stream = new MemoryStream();
			this.g.CopyFromScreen(0, 0, 0, 0, this.sz);
			this.cimg.Save(stream, ImageFormat.Gif);
			byte[] buffer = new byte[stream.Length];
			stream.Position = 0L;
			stream.Read(buffer, 0, (int)stream.Length);
			string s = buffer.Length.ToString();
			string str2 = null;
			if (s.Length <= 8)
			{
				for (int i = 0; i < (8 - s.Length); i++)
				{
					str2 = str2 + "0";
				}
				s = str2 + s;
			}
			byte[] bytes = Encoding.ASCII.GetBytes(s);
			byte[] dst = new byte[bytes.Length + buffer.Length];
			Buffer.BlockCopy(bytes, 0, dst, 0, 8);
			Buffer.BlockCopy(buffer, 0, dst, 8, buffer.Length);
			dst[dst.Length - 1] = 3;
			return dst;
		}
		public void Screen_Capture_Image_Send(byte[] data, Socket s)
		{
			int num = 0;
			int num2 = 0;
			int num3 = 0;
			num = data.Length / this.MAX;
			int num4 = data.Length % this.MAX;
			if (num4 > 0)
			{
				num3 = num + 1;
			}
			else
			{
				num3 = num;
			}
			int offset = 0;
			int num6 = 0;
			int mAX = this.MAX;
			while (true)
			{
				if (num3 == num2)
				{
					num2 = 0;
					return;
				}
				offset = num6;
				num6 = offset + this.MAX;
				if ((num == num2) && (num4 > 0))
				{
					num6 = offset + (data.Length - (num * this.MAX));
					mAX = data.Length - (num * this.MAX);
				}
				if (num3 > num2)
				{
					s.Send(data, offset, mAX, SocketFlags.None);
				}
				num2++;
			}
		}
		private void Server()
		{
			this.listener.Start();
			Socket s = this.listener.AcceptSocket();
			this.Rc_Thread(s);
			try
			{
				byte[] buffer;
				bool flag;
				goto Label_003E;
Label_0024:
				buffer = this.Screen_Capture();
				this.Screen_Capture_Image_Send(buffer, s);
				Thread.Sleep(60);
Label_003E:
				flag = true;
				goto Label_0024;
			}
			catch (Exception)
			{
				try
				{
					s.Close();
					this.listener.Stop();
					this.g.Dispose();
					MessageBox.Show("연결이 종료되었습니다.");
				}
				catch (Exception)
				{
				}
			}
		}
		public void Sock_init()
		{
			IPAddress localaddr = IPAddress.Parse(this.IP);
			this.listener = new TcpListener(localaddr, short.Parse(this.port));
			this.Server();
		}
	}
}
