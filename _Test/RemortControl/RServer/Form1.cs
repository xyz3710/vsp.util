﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using System.IO;
using System.Runtime.InteropServices;

namespace RServer
{
	public partial class Form1 : Form
	{
 
	 
		// Fields

		private string IP;

		private Graphics g;

		private TcpListener listener;
		private MemoryStream ms;
		private Thread thread;




		private int s_heigh;
		private int s_width;
		private Size sz;
		private ThServer T;



		// Methods
		public Form1()
		{
			this.ms = new MemoryStream();
			this.T = new ThServer();
			this.IP = null;
			this.components = null;
			this.InitializeComponent();
		}
		private void button1_Click(object sender, EventArgs e)
		{
			try
			{
				this.listener.Stop();
				this.thread.Abort();
				this.button2.Enabled = true;
			}
			catch (Exception)
			{
			}
		}
		private void button2_Click(object sender, EventArgs e)
		{
			string text = this.IPBox.Text;
			if (text == "")
			{
				MessageBox.Show("IP를 입력하세요");
				this.IPBox.Focus();
			}
			else
			{
				string[] strArray = text.Split(new char[] { '.' });
				if (strArray.Length != 4)
				{
					MessageBox.Show("IP를 정확히 입력하세요");
					this.IPBox.Focus();
				}
				else
				{
					byte[] bytes = Encoding.ASCII.GetBytes(strArray[0]);
					byte[] ip = Encoding.ASCII.GetBytes(strArray[1]);
					byte[] buffer3 = Encoding.ASCII.GetBytes(strArray[2]);
					byte[] buffer4 = Encoding.ASCII.GetBytes(strArray[3]);
					if ((((bytes.Length > 3) || (ip.Length > 3)) || (buffer3.Length > 3)) || (buffer4.Length > 3))
					{
						MessageBox.Show("IP를 정확히 입력하세요");
						this.IPBox.Focus();
					}
					else if ((this.num_chk(bytes, "IP") && this.num_chk(ip, "IP")) && (this.num_chk(buffer3, "IP") && this.num_chk(buffer4, "IP")))
					{
						string s = this.textBox1.Text;
						if (s == "")
						{
							MessageBox.Show("PORT를 입력하세요");
							this.textBox1.Focus();
						}
						else
						{
							byte[] buffer5 = Encoding.ASCII.GetBytes(s);
							if (this.num_chk(buffer5, "PORT"))
							{
								this.T.Cap_data(this.cimg, this.g, this.sz, text, s);
								ThreadStart start = new ThreadStart(this.T.Sock_init);
								this.thread = new Thread(start);
								this.thread.Start();
								this.button2.Enabled = false;
							}
						}
					}
				}
			}
		}
	 
		private void exitToolStripMenuItem_Click(object sender, EventArgs e)
		{
			base.Close();
		}
		private void Form1_FormClosed(object sender, FormClosedEventArgs e)
		{
			try
			{
				this.thread.Abort();
				this.T.listener_stop();
			}
			catch (Exception)
			{
			}
		}
		private void Form1_Load(object sender, EventArgs e)
		{
			this.s_width = Screen.AllScreens[0].Bounds.Width / 1;
			this.s_heigh = Screen.AllScreens[0].Bounds.Height / 1;
			this.sz = new Size(this.s_width, this.s_heigh);
			this.cimg = new Bitmap(this.s_width, this.s_heigh);
			this.g = Graphics.FromImage(this.cimg);
		}
	
		private bool num_chk(byte[] ip, string msg)
		{
			for (int i = 0; i < ip.Length; i++)
			{
				if ((ip[i] < 0x30) || (ip[i] > 0x39))
				{
					MessageBox.Show(msg + "를 정확히 입력하세요");
					return false;
				}
			}
			return true;
		}
		[DllImport("gdi32")]
		public static extern int SetStretchBltMode(ref int hDC, int nStretchMode);
		private void 이프로그램은ToolStripMenuItem_Click(object sender, EventArgs e)
		{
			new info().ShowDialog();
		}
	}
	
}
 
