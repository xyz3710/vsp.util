﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using System.IO;


namespace RServer
{
	partial class Form1
	{
		/// <summary>
		/// 필수 디자이너 변수입니다.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// 사용 중인 모든 리소스를 정리합니다.
		/// </summary>
		/// <param name="disposing">관리되는 리소스를 삭제해야 하면 true이고, 그렇지 않으면 false입니다.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form 디자이너에서 생성한 코드

		/// <summary>
		/// 디자이너 지원에 필요한 메서드입니다.
		/// 이 메서드의 내용을 코드 편집기로 수정하지 마십시오.
		/// </summary>
		public void InitializeComponent()
		{
			this.button2 = new Button();
			this.IPBox = new TextBox();
			this.textBox1 = new TextBox();
			this.groupBox1 = new GroupBox();
			this.label1 = new Label();
			this.label2 = new Label();
			this.button1 = new Button();
			this.menuStrip1 = new MenuStrip();
			this.테스트버전ToolStripMenuItem = new ToolStripMenuItem();
			this.이프로그램은ToolStripMenuItem = new ToolStripMenuItem();
			this.exitToolStripMenuItem = new ToolStripMenuItem();
			this.groupBox1.SuspendLayout();
			this.menuStrip1.SuspendLayout();
			base.SuspendLayout();
			this.button2.Location = new Point(0xdb, 20);
			this.button2.Name = "button2";
			this.button2.Size = new Size(0x4b, 0x3e);
			this.button2.TabIndex = 2;
			this.button2.Text = "Start";
			this.button2.UseVisualStyleBackColor=(true);
			this.button2.Click += new EventHandler(this.button2_Click);
			this.IPBox.Location = new Point(0x39, 0x16);
			this.IPBox.Name = "IPBox";
			this.IPBox.Size = new Size(100, 0x15);
			this.IPBox.TabIndex = 3;
			this.textBox1.Location = new Point(0x39, 0x3d);
			this.textBox1.Name = "textBox1";
			this.textBox1.Size = new Size(100, 0x15);
			this.textBox1.TabIndex = 4;
			this.groupBox1.Controls.Add(this.button1);
			this.groupBox1.Controls.Add(this.button2);
			this.groupBox1.Controls.Add(this.label2);
			this.groupBox1.Controls.Add(this.label1);
			this.groupBox1.Controls.Add(this.textBox1);
			this.groupBox1.Controls.Add(this.IPBox);
			this.groupBox1.Location = new Point(0x10, 0x26);
			this.groupBox1.Name = "groupBox1";
			this.groupBox1.Size = new Size(320, 0x61);
			this.groupBox1.TabIndex = 5;
			this.groupBox1.TabStop = false;
			this.groupBox1.Text = "IP/PORT 입력";
			this.label1.AutoSize=(true);
			this.label1.Location = new Point(13, 0x1f);
			this.label1.Name = "label1";
			this.label1.Size = new Size(0x10, 12);
			this.label1.TabIndex = 5;
			this.label1.Text = "IP";
			this.label2.AutoSize=(true);
			this.label2.Location = new Point(13, 70);
			this.label2.Name = "label2";
			this.label2.Size = new Size(0x26, 12);
			this.label2.TabIndex = 6;
			this.label2.Text = "PORT";
			this.button1.Location = new Point(0xdb, 0x3b);
			this.button1.Name = "button1";
			this.button1.Size = new Size(0x4b, 0x17);
			this.button1.TabIndex = 7;
			this.button1.Text = "Stop";
			this.button1.UseVisualStyleBackColor=(true);
			this.button1.Visible = false;
			this.button1.Click += new EventHandler(this.button1_Click);
			this.menuStrip1.Items.AddRange(new ToolStripItem[] { this.테스트버전ToolStripMenuItem });
			this.menuStrip1.Location = new Point(0, 0);
			this.menuStrip1.Name = "menuStrip1";
			this.menuStrip1.Size = new Size(0x17e, 0x18);
			this.menuStrip1.TabIndex = 6;
			this.menuStrip1.Text = "menuStrip1";
			this.테스트버전ToolStripMenuItem.DropDownItems.AddRange(new ToolStripItem[] { this.이프로그램은ToolStripMenuItem, this.exitToolStripMenuItem });
			this.테스트버전ToolStripMenuItem.Name=("테스트버전ToolStripMenuItem");
			this.테스트버전ToolStripMenuItem.Size=(new Size(0xa5, 20));
			this.테스트버전ToolStripMenuItem.Text=("원격제어[테스트용 입니다]");
			this.이프로그램은ToolStripMenuItem.Name=("이프로그램은ToolStripMenuItem");
			this.이프로그램은ToolStripMenuItem.Size=(new Size(0x9c, 0x16));
			this.이프로그램은ToolStripMenuItem.Text=("이프로그램은..");
			this.이프로그램은ToolStripMenuItem.Click+=(new EventHandler(this.이프로그램은ToolStripMenuItem_Click));
			this.exitToolStripMenuItem.Name=("exitToolStripMenuItem");
			this.exitToolStripMenuItem.Size=(new Size(0x9c, 0x16));
			this.exitToolStripMenuItem.Text=("Exit");
			this.exitToolStripMenuItem.Click+=(new EventHandler(this.exitToolStripMenuItem_Click));
			base.AutoScaleDimensions=(new SizeF(7f, 12f));
			base.AutoScaleMode=AutoScaleMode.None;
			base.ClientSize = new Size(0x17e, 0x97);
			base.Controls.Add(this.groupBox1);
			base.Controls.Add(this.menuStrip1);
			base.MainMenuStrip=(this.menuStrip1);
			base.Name = "Form1";
			base.FormClosed +=(new FormClosedEventHandler(this.Form1_FormClosed));
			base.Load += new EventHandler(this.Form1_Load);
			this.groupBox1.ResumeLayout(false);
			this.groupBox1.PerformLayout();
			this.menuStrip1.ResumeLayout(false);
			this.menuStrip1.PerformLayout();
			base.ResumeLayout(false);
			base.PerformLayout();
		}

		#endregion


		private Button button1;
		private Button button2;
		private Bitmap cimg;
		private ToolStripMenuItem exitToolStripMenuItem;

		private GroupBox groupBox1;
		private TextBox IPBox;
		private Label label1;
		private Label label2;



		private MenuStrip menuStrip1;
		private TextBox textBox1;

		private ToolStripMenuItem 이프로그램은ToolStripMenuItem;
		private ToolStripMenuItem 테스트버전ToolStripMenuItem;
	}
}

