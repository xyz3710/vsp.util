﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using System.IO;


namespace RServer
{
	partial class info
	{
		/// <summary>
		/// 필수 디자이너 변수입니다.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// 사용 중인 모든 리소스를 정리합니다.
		/// </summary>
		/// <param name="disposing">관리되는 리소스를 삭제해야 하면 true이고, 그렇지 않으면 false입니다.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form 디자이너에서 생성한 코드

		/// <summary>
		/// 디자이너 지원에 필요한 메서드입니다.
		/// 이 메서드의 내용을 코드 편집기로 수정하지 마십시오.
		/// </summary>

		private void InitializeComponent()
		{
			this.groupBox1 = new GroupBox();
			this.label1 = new Label();
			this.button1 = new Button();
			this.groupBox1.SuspendLayout();
			base.SuspendLayout();
			this.groupBox1.Controls.Add(this.label1);
			this.groupBox1.Location = new Point(12, 2);
			this.groupBox1.Name = "groupBox1";
			this.groupBox1.Size = new Size(0x11f, 0x6d);
			this.groupBox1.TabIndex = 0;
			this.groupBox1.TabStop = false;
			this.groupBox1.Text = "이프로그램은..";
			this.label1.AutoSize=(true);
			this.label1.Location = new Point(6, 0x11);
			this.label1.Name = "label1";
			this.label1.Size = new Size(0x111, 0x54);
			this.label1.TabIndex = 0;
			this.label1.Text = "C# 공부하다가  만들어본 프로그램입니다. \r\n기능은 마우스 제어 (원쪽/오른쪽 버튼 및 드래그)\r\n및 키보드 제어  정도라고 할까요 ^^~\r\n\r\n\r\n\r\n                                  vision12@naver.com\r\n";
			this.button1.Location = new Point(0x131, 0x43);
			this.button1.Name = "button1";
			this.button1.Size = new Size(0x4b, 0x2c);
			this.button1.TabIndex = 1;
			this.button1.Text = "확 인";
			this.button1.UseVisualStyleBackColor=(true);
			this.button1.Click += new EventHandler(this.button1_Click);
			base.AutoScaleDimensions=(new SizeF(7f, 12f));
			base.AutoScaleMode=AutoScaleMode.None;
			base.ClientSize = new Size(0x182, 0x73);
			base.Controls.Add(this.button1);
			base.Controls.Add(this.groupBox1);
			base.FormBorderStyle = FormBorderStyle.FixedToolWindow;
			base.Name = "Form2";
			this.groupBox1.ResumeLayout(false);
			this.groupBox1.PerformLayout();
			base.ResumeLayout(false);
		}

		#endregion



		// Fields
		private Button button1;
		private GroupBox groupBox1;
		private Label label1;
		// Methods
	 
	}
}