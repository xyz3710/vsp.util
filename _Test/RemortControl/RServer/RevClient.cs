﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using System.IO;
using System.Runtime.InteropServices;

namespace RServer
{
	public partial class RevClient : Form
	{
		public RevClient()
		{
			this.ms = new MemoryStream();
			//InitializeComponent();
		}

		// Fields
		public TextBox IPBox1;
		public const int MOUSEEVENTF_LEFTDOWN = 2;
		public const int MOUSEEVENTF_LEFTUP = 4;
		public const int MOUSEEVENTF_RIGHTDOWN = 8;
		public const int MOUSEEVENTF_RIGHTUP = 0x10;
		private MemoryStream ms;
		private Socket s2;
		private int xp;
		private int yp;
		// Methods
	 
		public void Client_Rev()
		{
			MemoryStream stream = new MemoryStream();
			Point point = new Point();
			byte[] dst = new byte[20];
			byte[] buffer = new byte[20];
			int count = 0;
			int num2 = 0;
			int num3 = 20;
			int num4 = 0;
			int num5 = 0;
			byte[] src = new byte[num5];
			bool flag = false;
			int width = Screen.AllScreens[0].Bounds.Width;
			int height = Screen.AllScreens[0].Bounds.Height;
			try
			{
				try
				{
					bool flag3;
					goto Label_030F;
Label_0072:
					;
					try
					{
						if (flag)
						{
							count = num5;
							Buffer.BlockCopy(src, 0, dst, 0, count);
							flag = false;
						}
						goto Label_0116;
Label_0097:
						count += num2;
						if (num3 > count)
						{
							num2 = this.s2.Receive(buffer);
							try
							{
								Buffer.BlockCopy(buffer, 0, dst, count, num2);
							}
							catch (Exception)
							{
								num4 = num3 - count;
								num5 = num2 - num4;
								src = new byte[num5];
								Buffer.BlockCopy(buffer, 0, dst, count, num4);
								Buffer.BlockCopy(buffer, num4, src, 0, num5);
								flag = true;
							}
						}
						else
						{
							count = 0;
							num2 = 0;
							goto Label_011E;
						}
Label_0116:
						flag3 = true;
						goto Label_0097;
Label_011E:
						;
						string[] strArray = Encoding.ASCII.GetString(dst).Split(new char[] { ',' });
						this.xp = int.Parse(strArray[0]);
						this.yp = int.Parse(strArray[1]);
						double num6 = (((float)width) / 1000f) * this.xp;
						double num7 = (((float)height) / 720f) * this.yp;
						point.X = (int)num6;
						point.Y = (int)num7;
						Cursor.Position = point;
						string str2 = strArray[2];
						string s = strArray[3];
						string str4 = strArray[4];
						switch (str2)
						{
							case "L":
								mouse_event(2, 0, 0, 0, GetMessageExtraInfo());
								break;
							case "l":
								mouse_event(4, 0, 0, 0, GetMessageExtraInfo());
								break;
							case "R":
								mouse_event(8, 0, 0, 0, GetMessageExtraInfo());
								mouse_event(0x10, 0, 0, 0, GetMessageExtraInfo());
								break;
						}
						if (s != "")
						{
							IntPtr ptr;
							int num10 = int.Parse(s);
							if (str4 != "")
							{
								ptr = new IntPtr();
								keybd_event(0x10, 0, 0, ptr);
								ptr = new IntPtr();
								keybd_event((byte)num10, 0, 0, ptr);
								ptr = new IntPtr();
								keybd_event((byte)num10, 0, 2, ptr);
								ptr = new IntPtr();
								keybd_event(0x10, 0, 2, ptr);
							}
							else
							{
								ptr = new IntPtr();
								keybd_event((byte)num10, 0, 0, ptr);
								ptr = new IntPtr();
								keybd_event((byte)num10, 0, 2, ptr);
							}
						}
					}
					catch (Exception)
					{
						this.s2.Close();
						return;
					}
Label_030F:
					flag3 = true;
					goto Label_0072;
				}
				catch (SocketException)
				{
					this.s2.Close();
				}
			}
			finally
			{
				this.s2.Close();
			}
		}
		public void Client_Rev_init(Socket s)
		{
			this.s2 = s;
		}
		[DllImport("User32.dll")]
		public static extern int GetMessageExtraInfo();
		[DllImport("User32.dll")]
		public static extern void keybd_event(byte bVk, byte bScan, int dwFlags, IntPtr dwExtraInfo);
		[DllImport("User32.dll")]
		public static extern int mouse_event(int dwFlags, int dx, int dy, int cButtons, int dwExtraInfo);
	}
}