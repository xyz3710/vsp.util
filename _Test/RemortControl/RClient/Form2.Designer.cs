﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Net;
using System.Net.Sockets;
using System.Threading;

namespace Rclient
{
	partial class Form2
	{
		/// <summary>
		/// 필수 디자이너 변수입니다.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// 사용 중인 모든 리소스를 정리합니다.
		/// </summary>
		/// <param name="disposing">관리되는 리소스를 삭제해야 하면 true이고, 그렇지 않으면 false입니다.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form 디자이너에서 생성한 코드

		/// <summary>
		/// 디자이너 지원에 필요한 메서드입니다.
		/// 이 메서드의 내용을 코드 편집기로 수정하지 마십시오.
		/// </summary>
		private void InitializeComponent()
		{
			this.textBox1 = new TextBox();
			this.button1 = new Button();
			this.textBox2 = new TextBox();
			this.groupBox1 = new GroupBox();
			this.label1 = new Label();
			this.label2 = new Label();
			this.groupBox1.SuspendLayout();
			base.SuspendLayout();
			this.textBox1.Location = new Point(50, 20);
			this.textBox1.Name = "textBox1";
			this.textBox1.Size = new Size(100, 0x15);
			this.textBox1.TabIndex = 0;
			this.button1.Location = new Point(0xb2, 60);
			this.button1.Name = "button1";
			this.button1.Size = new Size(0x4b, 0x17);
			this.button1.TabIndex = 1;
			this.button1.Text = "입력";
			this.button1.UseVisualStyleBackColor=(true);
			this.button1.Click += new EventHandler(this.button1_Click);
			this.textBox2.Location = new Point(50, 60);
			this.textBox2.Name = "textBox2";
			this.textBox2.Size = new Size(100, 0x15);
			this.textBox2.TabIndex = 2;
			this.groupBox1.Controls.Add(this.label2);
			this.groupBox1.Controls.Add(this.label1);
			this.groupBox1.Controls.Add(this.textBox1);
			this.groupBox1.Controls.Add(this.textBox2);
			this.groupBox1.Controls.Add(this.button1);
			this.groupBox1.Location = new Point(0x16, 12);
			this.groupBox1.Name = "groupBox1";
			this.groupBox1.Size = new Size(0x178, 110);
			this.groupBox1.TabIndex = 3;
			this.groupBox1.TabStop = false;
			this.groupBox1.Text = "IP / PORT 입력";
			this.label1.AutoSize=(true);
			this.label1.Location = new Point(11, 0x18);
			this.label1.Name = "label1";
			this.label1.Size = new Size(0x10, 12);
			this.label1.TabIndex = 3;
			this.label1.Text = "IP";
			this.label2.AutoSize=(true);
			this.label2.Location = new Point(11, 0x42);
			this.label2.Name = "label2";
			this.label2.Size = new Size(0x26, 12);
			this.label2.TabIndex = 4;
			this.label2.Text = "PORT";
			base.AutoScaleDimensions=(new SizeF(7f, 12f));
			base.AutoScaleMode= AutoScaleMode.None;
			base.ClientSize = new Size(0x1a5, 0x8b);
			base.Controls.Add(this.groupBox1);
			base.FormBorderStyle = FormBorderStyle.FixedToolWindow;
			base.Location = new Point(100, 100);
			base.Name = "Form2";
			this.Text = "IP/PORT 입력";
			this.groupBox1.ResumeLayout(false);
			this.groupBox1.PerformLayout();
			base.ResumeLayout(false);
		}

	
		#endregion
		private Button button1;

		private GroupBox groupBox1;
		private Label label1;
		private Label label2;
		public TextBox textBox1;
		public TextBox textBox2;
	
	}
}