﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using System.IO;


namespace Rclient
{
	public partial class Form2 : Form
	{
 

		// Fields

		// Methods
		public Form2()
		{
			this.InitializeComponent();
			this.textBox1.Focus();
		}
		private void button1_Click(object sender, EventArgs e)
		{
			string text = this.textBox1.Text;
			if (text == "")
			{
				MessageBox.Show("IP를 입력하세요");
				this.textBox1.Focus();
			}
			else
			{
				string[] strArray = text.Split(new char[] { '.' });
				if (strArray.Length != 4)
				{
					MessageBox.Show("IP를 정확히 입력하세요");
					this.textBox1.Focus();
				}
				else
				{
					byte[] bytes = Encoding.ASCII.GetBytes(strArray[0]);
					byte[] ip = Encoding.ASCII.GetBytes(strArray[1]);
					byte[] buffer3 = Encoding.ASCII.GetBytes(strArray[2]);
					byte[] buffer4 = Encoding.ASCII.GetBytes(strArray[3]);
					if (((bytes.Length > 3) || (ip.Length > 3)) || ((buffer3.Length > 3) || (buffer4.Length > 3)))
					{
						MessageBox.Show("IP를 정확히 입력하세요");
						this.textBox1.Focus();
					}
					else if (this.num_chk(bytes, "IP") && ((this.num_chk(ip, "IP") && this.num_chk(buffer3, "IP")) && this.num_chk(buffer4, "IP")))
					{
						string s = this.textBox2.Text;
						if (s == "")
						{
							MessageBox.Show("PORT를 입력하세요");
							this.textBox2.Focus();
						}
						else
						{
							byte[] buffer5 = Encoding.ASCII.GetBytes(s);
							if (this.num_chk(buffer5, "PORT"))
							{
								base.Hide();
							}
						}
					}
				}
			}
		}


		private bool num_chk(byte[] ip, string msg)
		{
			for (int i = 0; i < ip.Length; i++)
			{
				if ((ip[i] < 0x30) || (ip[i] > 0x39))
				{
					MessageBox.Show(msg + "를 정확히 입력하세요");
					return false;
				}
			}
			return true;
		}
	}
}