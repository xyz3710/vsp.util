﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Net;
using System.Net.Sockets;
using System.Threading;

namespace Rclient
{
	partial class Form1
	{
		/// <summary>
		/// 필수 디자이너 변수입니다.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// 사용 중인 모든 리소스를 정리합니다.
		/// </summary>
		/// <param name="disposing">관리되는 리소스를 삭제해야 하면 true이고, 그렇지 않으면 false입니다.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form 디자이너에서 생성한 코드

		/// <summary>
		/// 디자이너 지원에 필요한 메서드입니다.
		/// 이 메서드의 내용을 코드 편집기로 수정하지 마십시오.
		/// </summary>
		private void InitializeComponent()
		{
			ComponentResourceManager manager = new ComponentResourceManager(typeof(Form1));
			this.pictureBox1 = new PictureBox();
			this.원격제어ToolStripMenuItem = new ToolStripMenuItem();
			this.startToolStripMenuItem = new ToolStripMenuItem();
			this.endToolStripMenuItem = new ToolStripMenuItem();
			this.menuStrip1 = new MenuStrip();
			this.정보ToolStripMenuItem = new ToolStripMenuItem();
			this.이프로그램은ToolStripMenuItem = new ToolStripMenuItem();
			this.exitToolStripMenuItem = new ToolStripMenuItem();
			//this.pictureBox1.BeginInit();
			this.menuStrip1.SuspendLayout();
			base.SuspendLayout();
			this.pictureBox1.Location = new Point(0, 0x1a);
			this.pictureBox1.Name = "pictureBox1";
			this.pictureBox1.Size = new Size(0x3e8, 720);
			this.pictureBox1.SizeMode = PictureBoxSizeMode.AutoSize;
			this.pictureBox1.TabIndex = 1;
			this.pictureBox1.TabStop = false;
			this.pictureBox1.MouseDown += new MouseEventHandler(this.pictureBox1_MouseDown);
			this.pictureBox1.MouseMove += new MouseEventHandler(this.pictureBox1_MouseMove);
			this.pictureBox1.LoadCompleted+=(new AsyncCompletedEventHandler(this.pictureBox1_LoadCompleted));
			this.pictureBox1.MouseUp += new MouseEventHandler(this.pictureBox1_MouseUp);
			this.원격제어ToolStripMenuItem.DropDownItems.AddRange(new ToolStripItem[] { this.startToolStripMenuItem, this.endToolStripMenuItem });
			this.원격제어ToolStripMenuItem.Font=(new Font("굴림", 9f, FontStyle.Regular, GraphicsUnit.Point, 0x81));
			this.원격제어ToolStripMenuItem.Name=("원격제어ToolStripMenuItem");
			this.원격제어ToolStripMenuItem.Size=(new Size(0x41, 20));
			this.원격제어ToolStripMenuItem.Text=("원격제어");
			this.startToolStripMenuItem.Image=((Image)manager.GetObject("startToolStripMenuItem.Image"));
			this.startToolStripMenuItem.Name=("startToolStripMenuItem");
			this.startToolStripMenuItem.Size=(new Size(0x98, 0x16));
			this.startToolStripMenuItem.Text=("Start");
			this.startToolStripMenuItem.Click+=(new EventHandler(this.startToolStripMenuItem_Click));
			this.endToolStripMenuItem.Name=("endToolStripMenuItem");
			this.endToolStripMenuItem.Size=(new Size(0x98, 0x16));
			this.endToolStripMenuItem.Text=("End");
			this.endToolStripMenuItem.Click+=(new EventHandler(this.endToolStripMenuItem_Click));
			this.menuStrip1.Items.AddRange(new ToolStripItem[] { this.원격제어ToolStripMenuItem, this.정보ToolStripMenuItem });
			this.menuStrip1.Location = new Point(0, 0);
			this.menuStrip1.Name = "menuStrip1";
			this.menuStrip1.RenderMode = ToolStripRenderMode.System;
			this.menuStrip1.Size = new Size(0x3ea, 0x18);
			this.menuStrip1.TabIndex = 5;
			this.menuStrip1.Text = "menuStrip1";
			this.정보ToolStripMenuItem.DropDownItems.AddRange(new ToolStripItem[] { this.이프로그램은ToolStripMenuItem, this.exitToolStripMenuItem });
			this.정보ToolStripMenuItem.Name=("정보ToolStripMenuItem");
			this.정보ToolStripMenuItem.Size=(new Size(0x29, 20));
			this.정보ToolStripMenuItem.Text=("정보");
			this.이프로그램은ToolStripMenuItem.Name=("이프로그램은ToolStripMenuItem");
			this.이프로그램은ToolStripMenuItem.Size=(new Size(0x9c, 0x16));
			this.이프로그램은ToolStripMenuItem.Text=("이프로그램은..");
			this.이프로그램은ToolStripMenuItem.Click+=(new EventHandler(this.이프로그램은ToolStripMenuItem_Click));
			this.exitToolStripMenuItem.Name=("exitToolStripMenuItem");
			this.exitToolStripMenuItem.Size=(new Size(0x9c, 0x16));
			this.exitToolStripMenuItem.Text=("Exit");
			this.exitToolStripMenuItem.Click+=(new EventHandler(this.exitToolStripMenuItem_Click));
			base.AutoScaleDimensions=(new SizeF(7f, 12f));
			base.AutoScaleMode=AutoScaleMode.Inherit;
			base.ClientSize = new Size(0x3ea, 750);
			base.Controls.Add(this.pictureBox1);
			base.Controls.Add(this.menuStrip1);
			base.Name = "Form1";
			this.Text = "RC";
			base.FormClosed+=(new FormClosedEventHandler(this.Form1_FormClosed));
			base.Load += new EventHandler(this.Form1_Load);
			//this.pictureBox1.EndInit();
			this.menuStrip1.ResumeLayout(false);
			this.menuStrip1.PerformLayout();
			base.ResumeLayout(false);
			base.PerformLayout();
		}

		#endregion

	 

		private ToolStripMenuItem endToolStripMenuItem;
		private ToolStripMenuItem exitToolStripMenuItem;

		private MenuStrip menuStrip1;
		private PictureBox pictureBox1;

		private ToolStripMenuItem startToolStripMenuItem;

		private ToolStripMenuItem 원격제어ToolStripMenuItem;
		private ToolStripMenuItem 이프로그램은ToolStripMenuItem;
		private ToolStripMenuItem 정보ToolStripMenuItem;
	}
}

