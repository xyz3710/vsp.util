﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Net;
using System.Net.Sockets;
using System.Threading;

namespace Rclient
{
 

	public partial class Form1 : Form
	{
		// Fields

		private Csend cs;
		private IPEndPoint endPoint;
		private Graphics g;
		private IPAddress ip;
        		private Socket s;
				private Thread thread;
				private Thread thread2;


		// Methods
		public Form1()
		{
			this.cs = new Csend();
			this.InitializeComponent();
		}
 
		private void endToolStripMenuItem_Click(object sender, EventArgs e)
		{
			try
			{
				this.startToolStripMenuItem.Enabled=(true);
				this.thread.Abort();
				this.s.Close();
			}
			catch (Exception)
			{
			}
		}
		private void exitToolStripMenuItem_Click(object sender, EventArgs e)
		{
			base.Close();
		}
		private void Form1_FormClosed(object sender, FormClosedEventArgs e)
		{
			if (this.thread != null)
			{
				this.thread.Abort();
			}
		}
		private void Form1_Load(object sender, EventArgs e)
		{
			base.SetStyle(ControlStyles.DoubleBuffer, true);
			base.SetStyle(ControlStyles.AllPaintingInWmPaint, true);
			base.SetStyle(ControlStyles.UserPaint, true);
		}
	
		private void pictureBox1_LoadCompleted(object sender, AsyncCompletedEventArgs e)
		{
			base.SetStyle(ControlStyles.DoubleBuffer, true);
			base.SetStyle(ControlStyles.AllPaintingInWmPaint, true);
			base.SetStyle(ControlStyles.UserPaint, true);
		}
		public void pictureBox1_MouseDown(object sender, MouseEventArgs e)
		{
			this.cs.Csend_init2(e);
		}
		private void pictureBox1_MouseMove(object sender, MouseEventArgs e)
		{
			this.cs.Csend_init4(e);
		}
		private void pictureBox1_MouseUp(object sender, MouseEventArgs e)
		{
			this.cs.Csend_init3(e);
		}
		private bool Start_Sock()
		{
			Form2 form = new Form2();
			form.ShowDialog();
			string text = form.textBox1.Text;
			string s = form.textBox2.Text;
			if (text == "")
			{
				MessageBox.Show("아이피를 입력하세요");
				return false;
			}
			if (s == "")
			{
				MessageBox.Show("포트를 입력하세요");
				return false;
			}
			try
			{
				this.ip = IPAddress.Parse(text);
				this.endPoint = new IPEndPoint(this.ip, int.Parse(s));
				this.s = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
				this.s.Connect(this.endPoint);
			}
			catch (Exception exception)
			{
				MessageBox.Show(" " + exception.Message);
			}
			return true;
		}
		private void startToolStripMenuItem_Click(object sender, EventArgs e)
		{
			if (this.Start_Sock())
			{
				this.startToolStripMenuItem.Enabled=(false);
				this.g = this.pictureBox1.CreateGraphics();
				Creceiv creceiv = new Creceiv();
				creceiv.Creceiv_init(this.s, this.pictureBox1, this.g, this.cs);
				ThreadStart start = new ThreadStart(creceiv.Server_Rev);
				this.thread = new Thread(start);
				this.thread.Start();
				this.cs.Server_Send_init(this.s);
				ThreadStart start2 = new ThreadStart(this.cs.Server_Send);
				this.thread2 = new Thread(start2);
				this.cs.Csend_init(this.thread2);
				this.thread2.Start();
			}
		}
		private void toolStripTextBox1_Click(object sender, EventArgs e)
		{
		}
		private void 이프로그램은ToolStripMenuItem_Click(object sender, EventArgs e)
		{
			new Form3().ShowDialog();
		}
	}
 
 
 
}
 
