﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using System.IO;
using System.Drawing.Drawing2D;

namespace Rclient
{
	class Creceiv
	{
		// Fields
		private Bitmap cimg;
		private Csend cs;
		private byte[] data;
		private Graphics g;
		private byte[] img_data;
		private int MAX;
		private MemoryStream ms;
		private PictureBox pb;
		private Socket s;
		// Methods
		public Creceiv()
		{
			this.MAX = 0x5000;
			this.data = new byte[8];
			this.img_data = new byte[0x30d40];
		}
		public int Capture_Size_Rev(Socket s)
		{
			int num = 0;
			s.Receive(this.data);
			num = int.Parse(Encoding.ASCII.GetString(this.data));
			this.img_data = new byte[num];
			return num;
		}
		public void Cre_Main()
		{
			int num = 0;
			byte[] buffer = new byte[this.MAX];
			int count = 0;
			int num3 = 0;
			bool flag = false;
			int num4 = 0;
			int num5 = 0;
			byte[] src = new byte[num5];
			byte[] dst = new byte[num];
			try
			{
Label_002C:
				if (!flag)
				{
					num = this.Capture_Size_Rev(this.s);
					dst = new byte[num];
					num3 = 0;
					count = 0;
				}
				else
				{
					num = int.Parse(Encoding.ASCII.GetString(this.data));
					dst = new byte[num];
					count = num5;
					Buffer.BlockCopy(src, 0, dst, 0, count);
					flag = false;
				}
Label_007F:
				count += num3;
				if (num > count)
				{
					num3 = this.s.Receive(buffer);
					try
					{
						Buffer.BlockCopy(buffer, 0, dst, count, num3);
					}
					catch (Exception)
					{
						num4 = num - count;
						num5 = (num3 - num4) - 8;
						if (num5 < 0)
						{
							num5 = num3 - num4;
						}
						if ((num3 - num4) >= 8)
						{
							src = new byte[num5];
							Buffer.BlockCopy(buffer, 0, dst, count, num4);
							Buffer.BlockCopy(buffer, num4, this.data, 0, 8);
							Buffer.BlockCopy(buffer, num4 + 8, src, 0, num5);
						}
						else
						{
							Buffer.BlockCopy(buffer, 0, dst, count, num4);
							Buffer.BlockCopy(buffer, num4, this.data, 0, num5);
							num3 = this.s.Receive(buffer);
							src = new byte[num3];
							Buffer.BlockCopy(buffer, 0, this.data, num5, 8 - num5);
							Buffer.BlockCopy(buffer, 8 - num5, src, 0, num3 - (8 - num5));
							num5 = num3 - (8 - num5);
							flag = true;
							num3 = 0;
							goto Label_016B;
						}
						flag = true;
					}
					goto Label_007F;
				}
				num3 = 0;
Label_016B:
				count = 0;
				this.ms = new MemoryStream(dst);
				this.cimg = new Bitmap(this.ms);
				this.g.InterpolationMode = InterpolationMode.HighQualityBicubic;
				this.g.DrawImage(this.cimg, new Rectangle(0, 0, 0x3e8, 720));
				goto Label_002C;
			}
			catch (Exception)
			{
				MessageBox.Show("작업이 중지되었습니다.");
				this.s.Close();
			}
		}
		public void Creceiv_init(Socket s, PictureBox pb, Graphics g, Csend cs)
		{
			this.s = s;
			this.pb = pb;
			this.g = g;
			this.cs = cs;
		}
		public void Server_Rev()
		{
			this.Cre_Main();
		}
	}
}
