﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using System.IO;


namespace Rclient
{
	internal class Csend : IMessageFilter
	{
		// Fields
		private static string keyOut;
		private MouseEventArgs mdn;
		private MouseEventArgs mpoint;
		private MouseEventArgs mup;
		private Socket socket2;
		private Thread thread;
		private const int WM_KEYDOWN = 0x100;
		private const int WM_KEYUP = 0x101;
		// Methods
		public Csend()
		{
		}
		public void Csend_c()
		{
			string str = null;
			try
			{
				string str2;
Label_0002:
				if (this.mdn == null)
				{
					goto Label_0042;
				}
				switch (this.mdn.Button)
				{
					case MouseButtons.Left:
						str = "L";
						break;
					case MouseButtons.Right:
						str = "R";
						goto Label_0039;
				}
Label_0039:
				this.mdn = null;
				goto Label_0088;
Label_0042:
				if (this.mup == null)
				{
					goto Label_0082;
				}
				MouseButtons button = this.mup.Button;
				if (button == MouseButtons.Left)
				{
					str = "l";
				}
				else if (button == MouseButtons.Right)
				{
					goto Label_0073;
				}
				goto Label_0079;
Label_0073:
				str = "r";
Label_0079:
				this.mup = null;
				goto Label_0088;
Label_0082:
				str = "N";
Label_0088:
				str2 = this.mpoint.X.ToString();
				string str3 = this.mpoint.Y.ToString();
				string str4 = null;
				string s = str2 + "," + str3 + "," + str + "," + keyOut;
				keyOut = "";
				if (s.Length < 20)
				{
					for (int i = 0; i < (0x13 - s.Length); i++)
					{
						str4 = str4 + "0";
					}
					s = s + "," + str4;
				}
				byte[] bytes = Encoding.ASCII.GetBytes(s);
				if (bytes.Length == 20)
				{
					this.socket2.Send(bytes);
				}
				Thread.Sleep(1);
				goto Label_0002;
			}
			catch (Exception)
			{
				this.thread.Abort();
				this.socket2.Close();
			}
		}
		public void Csend_init(Thread thread)
		{
			this.thread = thread;
		}
		public void Csend_init2(MouseEventArgs mdn)
		{
			this.mdn = mdn;
		}
		public void Csend_init3(MouseEventArgs mup)
		{
			this.mup = mup;
		}
		public void Csend_init4(MouseEventArgs mpoint)
		{
			this.mpoint = mpoint;
		}
		public bool PreFilterMessage(ref Message m)
		{
			if ((m.Msg == 0x100) && (Control.ModifierKeys == Keys.Shift))
			{
				keyOut = m.WParam.ToString() + ",16";
				return false;
			}
			if (m.Msg == 0x100)
			{
				keyOut = m.WParam.ToString() + ",";
				return false;
			}
			return false;
		}
		public void Server_Send()
		{
			this.Csend_c();
		}
		public void Server_Send_init(Socket s)
		{
			this.socket2 = s;
		}
	}
}
