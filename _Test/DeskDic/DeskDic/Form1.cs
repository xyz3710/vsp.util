﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Runtime.InteropServices;

namespace DeskDic
{
	public partial class Form1 : Form
	{
		#region Invoke method
		[DllImport("user32.dll", EntryPoint="FindWindow", SetLastError=true)]
		private static extern IntPtr FindWindow(string lpClassName, string lpWindowName);

		[DllImport("user32.dll", EntryPoint="FindWindowByCaption", SetLastError=true)]
		private static extern IntPtr FindWindowByCaption(IntPtr ZeroOnly, string lpWindowName);

		[DllImport("user32.dll", CharSet=CharSet.Auto, SetLastError=false)]
		private static extern int SendMessage(IntPtr hWnd, UInt32 Msg, Int32 wParam, Int32 lParam);

		[DllImport("user32.dll", CharSet=CharSet.Auto, SetLastError=false)]
		private static extern int SendMessage(IntPtr hWnd, UInt32 Msg, Int32 wParam, string lParam);

		[DllImport("user32.dll", CharSet=CharSet.Auto, SetLastError=false)]
		private static extern IntPtr SetActiveWindow(IntPtr hWnd);

		[DllImport("user32.dll", SetLastError=true)]
		private static extern IntPtr FindWindowEx(IntPtr hwndParent, IntPtr hwndChildAfter, string lpszClass, string lpszWindow);

		[DllImport("user32.dll", SetLastError=true)]
		private static extern IntPtr FindWindowEx(IntPtr parentHandle, IntPtr childAfter, string className, IntPtr windowTitle);

		[DllImport("user32.dll", EntryPoint="PostMessage", SetLastError=true)]
		private static extern uint PostMessage(IntPtr hwnd, uint wMsg, uint wParam, uint lParam);

		[DllImport("user32.dll", EntryPoint="WindowFromPoint", SetLastError=true)]
		private static extern IntPtr WindowFromPoint(int xPoint, int yPoint);

		[DllImport("user32.dll", CharSet=CharSet.Auto)]
		private static extern int GetClassName(IntPtr hWnd, StringBuilder lpClassName, int nMaxCount);
	
		[DllImport("user32.dll", CharSet = CharSet.Auto, SetLastError = true)]
		private static extern int GetWindowText(IntPtr hWnd, StringBuilder lpString, int nMaxCount);

		[DllImport("user32.dll", CharSet = CharSet.Auto, SetLastError = true)]
		private static extern int InternalGetWindowText(IntPtr hWnd, StringBuilder lpString, int nMaxCount);

		[DllImport("user32.dll", SetLastError=true, CharSet=CharSet.Auto)]
		static extern int GetWindowTextLength(IntPtr hWnd);
		#endregion
		
		#region Constants
		private const uint WM_GETTEXT = 0xD;
		private const uint WM_GETTEXTLENGTH = 0xE;
		//private const uint WM_GETTEXT = 13;
		//private const uint WM_GETTEXTLENGTH = 14;
		#endregion

		public Form1()
		{
			InitializeComponent();
		}

		private string GetTextUnderMouse()
		{
			string result = String.Empty;
			Point point = new Point();

			
			/*
			const object WM_GETTEXT = 13;
			const object WM_GETTEXTLENGTH = 14;

			POINTAPI P;
			long lRet;
			long hHandle;
			string aText;
			long lTextlen;
			string aText;
			lRet = GetCursorPos(P);
			hHandle = WindowFromPoint(P.x, P.y);
			lTextlen = SendMessage(hHandle, WM_GETTEXTLENGTH, 0, 0);
			if (lTextlen)
			{
				 if (lTextlen > 1024) lTextlen = 1024; 
				 lTextlen += 1
				 aText = Space(lTextlen);
				 lRet = SendMessage(hHandle, WM_GETTEXT, lTextlen, aText);
				 aText = aText.Substring(lRet);
			}
				}
        	
			*/
			return result;
		}

		private void button1_Click(object sender, EventArgs e)
		{
			GetTextUnderMouse();
		}

		private void Form1_Load(object sender, EventArgs e)
		{
			GetTextUnderMouse();
			Timer timer = new Timer()
			{
				Interval = 50,
			};
						
			timer.Tick += new EventHandler(timer_Tick);
			timer.Start();
		}

		public string GetText(IntPtr hWnd)
		{
			// Allocate correct string length first
			int length = GetWindowTextLength(hWnd);
			StringBuilder sb = new StringBuilder(length + 1);

			GetWindowText(hWnd, sb, sb.Capacity);

			return sb.ToString();
		}

		void timer_Tick(object sender, EventArgs e)
		{
			Point point = Control.MousePosition;
			//Text = string.Format("x : {0}, y : {1}", point.X, point.Y);

			IntPtr handle = WindowFromPoint(point.X, point.Y);
			StringBuilder className = new StringBuilder(100);            
			int res = GetClassName(handle, className, className.Capacity);

			textBox1.Text = GetText(handle);
			label1.Text = className.ToString();
			
						int textLen = SendMessage(handle, WM_GETTEXTLENGTH, 0, 0);
						button1.Text = textLen.ToString();

						//if (textLen > 0)
						{
							textLen = Math.Min(textLen, 1024);
							textLen += 1;
							string text = string.Empty.PadRight(textLen);
							int ret = SendMessage(handle, WM_GETTEXT, textLen, text);
						}

		}
	}
}
