﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Reflection;

namespace PinToTaskbar
{
	class Program
	{
		static int Main(string[] args)
		{
			if (args.Length < 1)
			{
				string name = Assembly.GetExecutingAssembly().GetName().Name;

				Console.WriteLine();
				Console.WriteLine("Usages : ");
				Console.WriteLine("\t{0} <Path> <pin status>", name);
				Console.WriteLine();
				Console.WriteLine(@"        {0} ""C:\text.lnk"" 1", name);
				Console.WriteLine(@"        {0} ""C:\text.txt"" 0", name);
				Console.WriteLine();

				return -1;
			}

			string path = string.Empty;
			string targetFile = string.Empty;
			bool pin = true;

			if (args.Length >= 1)
			{
				string targetPath = args[0];

				if (File.Exists(targetPath) == false)
				{
					Console.WriteLine("File not found.\r\n{0}", targetPath);

					return -99;
				}

				path = Path.GetDirectoryName(targetPath);
				targetFile = Path.GetFileName(targetPath);
			}

			if (args.Length == 2)
			{
				pin = Convert.ToBoolean(Convert.ToInt32(args[1]));
			}

			PinToTaskbar(path, targetFile, pin);

			return 0;
		}

		/// <summary>
		/// Determines if the application is running on Windows 7
		/// </summary>
		public static bool RunningOnWin7
		{
			get
			{
				// Verifies that OS version is 6.1 or greater, and the Platform is WinNT.
				return Environment.OSVersion.Platform == PlatformID.Win32NT &&
					Environment.OSVersion.Version.CompareTo(new Version(6, 1)) >= 0;
			}
		}

		/// <summary>
		/// 작업 표시줄에 해당 프로그램을 고정하거나 해제 합니다.
		/// </summary>
		/// <param name="path">대상 프로그램 경로</param>
		/// <param name="targetFile">대상 프로그램(실행 파일이나 바로 가기도 가능)</param>
		/// <param name="pin">true면 작업 표시줄에 고정합니다.</param>
		/// <exception cref="System.PlatformNotSupportedException">Windows 7을 지원하지 않아서 작업 표시줄에 고정할 수 없습니다.</exception>
		public static void PinToTaskbar(string path, string targetFile, bool pin = true)
		{
			if (RunningOnWin7 == false)
			{
				throw new PlatformNotSupportedException("Windows 7을 지원하지 않아서 작업 표시줄에 고정할 수 없습니다.");
			}

			const string SHELL_APPLICATION_NAME = "Shell.Application";
			Type shellAppType = Type.GetTypeFromProgID(SHELL_APPLICATION_NAME);
			dynamic shellApp = Activator.CreateInstance(shellAppType);
			dynamic folder = shellApp.NameSpace(path);
			dynamic link = folder.ParseName(targetFile);
			dynamic verbs = link.Verbs();
			string doPinString = string.Empty;
			string doUnpinString = string.Empty;

			switch (System.Threading.Thread.CurrentThread.CurrentCulture.LCID)
			{
				case 1042:
					doPinString = "작업 표시줄에 고정(&K)";
					doUnpinString = "작업 표시줄에서 제거(&K)";

					break;
				default:
					doPinString = "Pin to Tas&kbar";
					doUnpinString = "Unpin from Tas&kbar";

					break;
			}

			foreach (dynamic verb in verbs)
			{
				string verbName = verb.Name;

				if ((pin && verbName.Equals(doPinString, StringComparison.CurrentCultureIgnoreCase) ||
					(!pin && verbName.Equals(doUnpinString, StringComparison.CurrentCultureIgnoreCase))))
				{
					verb.DoIt();

					break;
				}

				System.Diagnostics.Debug.WriteLine(verbName);
			}
		}
	}
}
