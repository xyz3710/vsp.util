﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Configuration;

namespace VncAddressBook.Handler
{
	class ServerChangerTestDriver
	{
		static void Main(string[] args)
		{
			Configuration config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
			SectionGroup group = config.SectionGroups[SectionGroup.SectionGroupNameString] as SectionGroup;

			// SectionGroup 정보를 생성한다.
			if (group == null)
			{
				group = new SectionGroup();

				config.SectionGroups.Add(SectionGroup.SectionGroupNameString, group);
			}

			VncAddressBookSection vncAddressBookSection = group.VncAddressBook;

			if (vncAddressBookSection == null)
			{
				vncAddressBookSection = new VncAddressBookSection();
				group.Sections.Add(VncAddressBookSection.SectionNameString, vncAddressBookSection);
			}

			ConnectionCollection connections = vncAddressBookSection.Connections;

			if (connections == null)
			{
				connections = new ConnectionCollection();

				connections.Add(new Connection("192.168.25.28", "dbd83cfd727a1458"));
			}
            
			OptionCollection options = new OptionCollection();

			if (options == null)
			{
				options = new OptionCollection();

				options.Add(new Option("UseLocalCursor", "1"));
				options.Add(new Option("UseDesktopResize", "1"));
				options.Add(new Option("FullScreen", "0"));
				options.Add(new Option("FullScreenChangeResolution", "0"));
				options.Add(new Option("UseAllMonitors", "0"));
				options.Add(new Option("RelativePtr", "0"));
				options.Add(new Option("FullColour", "1"));
				options.Add(new Option("LowColourLevel", "2"));
				options.Add(new Option("PreferredEncoding", "hextile"));
				options.Add(new Option("AutoSelect", "1"));
				options.Add(new Option("Shared", "0"));
				options.Add(new Option("SendPtrEvents", "1"));
				options.Add(new Option("SendKeyEvents", "1"));
				options.Add(new Option("SendCutText", "1"));
				options.Add(new Option("AcceptCutText", "1"));
				options.Add(new Option("ShareFiles", "1"));
				options.Add(new Option("DisableWinKeys", "1"));
				options.Add(new Option("Emulate3", "0"));
				options.Add(new Option("PointerEventInterval", "0"));
				options.Add(new Option("Monitor", @"\\.\DISPLAY2"));
				options.Add(new Option("Scaling", "None"));
				options.Add(new Option("MenuKey", "F8"));
				options.Add(new Option("SuppressIME", "1"));
				options.Add(new Option("AutoReconnect", "1"));
				options.Add(new Option("VerifyId", "2"));
				options.Add(new Option("HTTP", "0"));
				options.Add(new Option("ProxySetting", "Direct"));
				options.Add(new Option("SameProxy", "1"));
				options.Add(new Option("HTTPProxy", string.Empty));
				options.Add(new Option("HTTPSProxy", string.Empty));
				options.Add(new Option("ProxyConfig", string.Empty));
			}
            
			config.Save(ConfigurationSaveMode.Modified);
		}
	}
}
