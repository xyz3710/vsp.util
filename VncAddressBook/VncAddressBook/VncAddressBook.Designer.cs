﻿namespace VncAddressBook
{
	partial class VncAddressBook
	{
		/// <summary>
		/// 필수 디자이너 변수입니다.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// 사용 중인 모든 리소스를 정리합니다.
		/// </summary>
		/// <param name="disposing">관리되는 리소스를 삭제해야 하면 true이고, 그렇지 않으면 false입니다.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form 디자이너에서 생성한 코드

		/// <summary>
		/// 디자이너 지원에 필요한 메서드입니다.
		/// 이 메서드의 내용을 코드 편집기로 수정하지 마십시오.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(VncAddressBook));
			this.lstHosts = new System.Windows.Forms.ListBox();
			this.SuspendLayout();
			// 
			// lstHosts
			// 
			this.lstHosts.Dock = System.Windows.Forms.DockStyle.Fill;
			this.lstHosts.FormattingEnabled = true;
			this.lstHosts.ItemHeight = 15;
			this.lstHosts.Items.AddRange(new object[] {
            "192.168.255.288:5900\tTest Server(VM)",
            "192.168.25.36\tCVD Room",
            "192.168.25.36\t1234567",
            "192.168.25.36\tBreaking",
            "192.168.25.36\tCCE제거"});
			this.lstHosts.Location = new System.Drawing.Point(0, 0);
			this.lstHosts.Margin = new System.Windows.Forms.Padding(2);
			this.lstHosts.Name = "lstHosts";
			this.lstHosts.Size = new System.Drawing.Size(257, 94);
			this.lstHosts.TabIndex = 0;
			this.lstHosts.DoubleClick += new System.EventHandler(this.lstHosts_DoubleClick);
			this.lstHosts.KeyDown += new System.Windows.Forms.KeyEventHandler(this.lstHosts_KeyDown);
			// 
			// VncAddressBook
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 15F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(257, 97);
			this.Controls.Add(this.lstHosts);
			this.Font = new System.Drawing.Font("굴림체", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
			this.ForeColor = System.Drawing.Color.RoyalBlue;
			this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
			this.KeyPreview = true;
			this.Margin = new System.Windows.Forms.Padding(2);
			this.MaximizeBox = false;
			this.MinimizeBox = false;
			this.Name = "VncAddressBook";
			this.Opacity = 0.95;
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Text = "Vnc Auto Caller";
			this.TopMost = true;
			this.SizeChanged += new System.EventHandler(this.VncAddressBook_SizeChanged);
			this.Shown += new System.EventHandler(this.VncAddressBook_Shown);
			this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.VncAddressBook_KeyDown);
			this.ResumeLayout(false);

		}

		#endregion

		private System.Windows.Forms.ListBox lstHosts;

	}
}

