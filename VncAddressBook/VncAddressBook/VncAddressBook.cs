﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using VncAddressBook.Handler;
using System.IO;
using System.Reflection;
using System.Diagnostics;

namespace VncAddressBook
{
	public partial class VncAddressBook : Form
	{
		private const string DELIMETER = " ";
		private string _targetFile = "remote.vnc";
        
		public VncAddressBook()
		{
			InitializeComponent();

			SuspendLayout();
			Font = Wrapper.GetInstance().Font;
			ResumeLayout();
		}

		private void VncAddressBook_Shown(object sender, EventArgs e)
		{
			SuspendLayout();

			AssemblyName assemblyGetName = Assembly.GetExecutingAssembly().GetName();

			Text = string.Format("{0} Ver. {1}", 
                       FileVersionInfo.GetVersionInfo(assemblyGetName.CodeBase.Replace("file:///", string.Empty)).FileDescription, 
                       assemblyGetName.Version.ToString());

			Wrapper wrapper = Wrapper.GetInstance();
			lstHosts.Items.Clear();
			lstHosts.BeginUpdate();

			// 해당 host item중에서 가장 큰 길이를 구한다.
			int maxHostLen = 0;
			int maxAliasLen = 0;
			Dictionary<string, string> hosts = new Dictionary<string, string>();
			
			foreach (string alias in wrapper.Connections.Keys)
			{
				ConnectInfo wrapperConnections = wrapper.Connections[alias].ConnectInfo;
				
				maxHostLen = Math.Max(wrapperConnections.Host.Length, maxHostLen);
				maxAliasLen = Math.Max(GetRealLength(alias), maxAliasLen);
			}

			foreach (string alias in wrapper.Connections.Keys)
			{
				ConnectInfo wrapperConnections = wrapper.Connections[alias].ConnectInfo;

				string format = string.Format("{{0,-{0}}}{1}{{1,{2}}}", 
                                    maxHostLen, 
                                    DELIMETER, 
                                    maxAliasLen - GetHangulLength(alias));
				string item = string.Format(format, wrapperConnections.Host, alias);

				lstHosts.Items.Add(item);
				hosts.Add(wrapperConnections.Host, alias);
			}

			lstHosts.Tag = hosts;
			lstHosts.EndUpdate();

			Size = new Size((int)((maxHostLen + maxAliasLen) * Font.SizeInPoints - (maxHostLen + maxAliasLen)), 
				(lstHosts.Items.Count + 1) * lstHosts.ItemHeight + 32);
			ResumeLayout();
		}

		private int GetRealLength(string alias)
		{
			if (alias.Length == 0)
				return 0;

			int result = 0;

			foreach (char character in alias)
			{
				if (character > 31 && character < 129)
					result++;
				else
					result += 2;
			}

			return result;
		}

		private int GetHangulLength(string alias)
		{
			if (alias.Length == 0)
				return 0;
			
			int result = 0;

			foreach (char character in alias)
				if (character <= 31 || character >= 129)
					result++;

			return result;
		}

		private void VncAddressBook_SizeChanged(object sender, EventArgs e)
		{
			SuspendLayout();
			Location = new Point(Location.X, (Screen.PrimaryScreen.WorkingArea.Height - Size.Height) / 2);
			ResumeLayout();
		}

		private void VncAddressBook_KeyDown(object sender, KeyEventArgs e)
		{
			if (e.KeyCode == Keys.Escape)
				Close();
		}

		private void lstHosts_DoubleClick(object sender, EventArgs e)
		{
			StartVnc(lstHosts.SelectedItem as string);
		}

		private void lstHosts_KeyDown(object sender, KeyEventArgs e)
		{
			if (e.KeyCode == Keys.Enter)
				StartVnc(lstHosts.SelectedItem as string);
		}

		private void StartVnc(string selectedItem)
		{
			string alias = string.Empty;

			if (string.IsNullOrEmpty(selectedItem) == false)
			{
				string[] aliases = selectedItem.Split(new string[] { DELIMETER }, StringSplitOptions.RemoveEmptyEntries);
				
				if (aliases.Length >= 2)
				{
					Dictionary<string, string> hosts = lstHosts.Tag as Dictionary<string, string>;

					if (hosts != null)
						alias = hosts[aliases[0]];
					else
						alias = aliases[1].Trim();
				}
			}

			if (CreateFile(alias) == true)
			{
				Process process = new Process();

				string vncViewer = Wrapper.GetInstance().VncViewer;
				ProcessStartInfo psi = new ProcessStartInfo(vncViewer, string.Format("{0} {1}", "-config", _targetFile));
				process.StartInfo = psi;

				process.Start();
			}
		}

		private bool CreateFile(string alias)
		{
			StreamWriter sw = new StreamWriter(_targetFile);
			Wrapper wrapper = Wrapper.GetInstance();
			bool result = false;

			try
			{
				sw.WriteLine("[Connection]");

				ConnectInfo ci = wrapper.Connections[alias].ConnectInfo;
				Type type = ci.GetType();

				foreach (PropertyInfo pi in type.GetProperties())
					sw.WriteLine(string.Format("{0}={1}", pi.Name, pi.GetValue(ci, null)));

				sw.WriteLine("\r\n[Options]");

				Dictionary<string, string> wrapperOptions = wrapper.Options;

				if (wrapper.Connections[alias].Options != null && wrapper.Connections[alias].Options.Count > 0)
				{
					foreach (Option option in wrapper.Connections[alias].Options)
					{
						string optionKey = option.Key;

						if (wrapperOptions.ContainsKey(optionKey) == true)
							wrapperOptions[optionKey] = option.Value;
						else
							wrapperOptions.Add(optionKey, option.Value);
					}
				}

				foreach (string key in wrapperOptions.Keys)
					sw.WriteLine(string.Format("{0}={1}", key, wrapper.Options[key]));

				result = true;
			}
			catch (Exception ex)
			{
				MessageBox.Show(string.Format("File을 저장할 수 없습니다.{0}Alias : {1}{0}{2}", 
                                    Environment.NewLine, 
									alias,
                                    ex.Message), 
                    Text, 
                    MessageBoxButtons.OK, 
                    MessageBoxIcon.Error);
			}
			finally
			{
				if (sw != null)
				{
					sw.Flush();
					sw.Close();
				}
			}

			return result;
		}
	}
}