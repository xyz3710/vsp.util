﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VncAddressBook.Handler
{
	public class ConnectInfo
	{
		#region Fields
		private string _host;
		private string _userName;
		private string _password;
		private string _identity;
		private string _encryption;
		private string _singleSignOn;
		private string _selectDesktop;
		#endregion

		#region Constructors
		/// <summary>
		/// ConnectInfo structure의 새 인스턴스를 초기화 합니다.
		/// </summary>
		/// <param name="host"></param>
		/// <param name="userName"></param>
		/// <param name="password"></param>
		/// <param name="identity"></param>
		/// <param name="encryption"></param>
		/// <param name="singleSignOn"></param>
		/// <param name="selectDesktop"></param>
		public ConnectInfo(string host, string userName, string password, string identity, string encryption, string singleSignOn, string selectDesktop)
		{
			_host = host;
			_userName = userName;
			_password = password;
			_identity = identity;
			_encryption = encryption;
			_singleSignOn = singleSignOn;
			_selectDesktop = selectDesktop;
		}
		#endregion

		#region Properties
		/// <summary>
		/// Host를 구하거나 설정합니다.
		/// </summary>
		public string Host
		{
			get
			{
				return _host;
			}
			set
			{
				_host = value;
			}
		}

		/// <summary>
        /// UserName를 구하거나 설정합니다.
        /// </summary>
        public string UserName
        {
        	get
        	{
        		return _userName;
        	}
        	set
        	{
        		_userName = value;
        	}
        }

		/// <summary>
        /// Password를 구하거나 설정합니다.
        /// </summary>
        public string Password
        {
        	get
        	{
        		return _password;
        	}
        	set
        	{
        		_password = value;
        	}
        }

		/// <summary>
        /// Identity를 구하거나 설정합니다.
        /// </summary>
        public string Identity
        {
        	get
        	{
        		return _identity;
        	}
        	set
        	{
        		_identity = value;
        	}
        }

		/// <summary>
        /// Encryption를 구하거나 설정합니다.
        /// </summary>
        public string Encryption
        {
        	get
        	{
        		return _encryption;
        	}
        	set
        	{
        		_encryption = value;
        	}
        }

		/// <summary>
        /// SingleSignOn를 구하거나 설정합니다.
        /// </summary>
		public string SingleSignOn
        {
        	get
        	{
        		return _singleSignOn;
        	}
        	set
        	{
        		_singleSignOn = value;
        	}
        }

		/// <summary>
        /// SelectDesktop를 구하거나 설정합니다.
        /// </summary>
        public string SelectDesktop
        {
        	get
        	{
        		return _selectDesktop;
        	}
        	set
        	{
        		_selectDesktop = value;
        	}
        }
        
        #endregion
        
	}
}
