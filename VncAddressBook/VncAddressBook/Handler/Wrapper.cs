﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Configuration;
using System.Drawing;

namespace VncAddressBook.Handler
{
	/// <summary>
	/// Updater의 설정 파일에 관련된 Wrapper 클래스 입니다.
	/// </summary>
	public class Wrapper
	{
		#region Fields
		private VncAddressBookSection _vncAddressBookSection;
		private ConnectionCollection _connectionCollection;
		private OptionCollection _optionCollection;
		private Dictionary<string, Connection> _connections;
		private Dictionary<string, string> _options;
		#endregion

		#region Use Singleton Wrapper
		private static Wrapper _newInstance;
		private static Configuration _config;

		#region Constructor for Single Ton Wrapper
		/// <summary>
        /// Wrapper class의 Single Ton BufferSize을 위한 생성자 입니다.
        /// </summary>
        /// <returns></returns>
        private Wrapper(string exePath)
        {
        	_newInstance = null;
        	
			if (string.IsNullOrEmpty(exePath) == true)
				_config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
			else
				_config = ConfigurationManager.OpenExeConfiguration(exePath);

			SectionGroup group = _config.SectionGroups[SectionGroup.SectionGroupNameString] as SectionGroup;

			// SectionGroup 정보를 생성한다.
			if (group == null)
			{
				group = new SectionGroup();

				_config.SectionGroups.Add(SectionGroup.SectionGroupNameString, group);
			}

			_vncAddressBookSection = group.VncAddressBook;

			if (_vncAddressBookSection == null)
			{
				_vncAddressBookSection = new VncAddressBookSection();
				group.Sections.Add(VncAddressBookSection.SectionNameString, _vncAddressBookSection);

				_vncAddressBookSection.VncViewer = @"C:\Program Files\Util\VNC4\VncViewer.exe";
				_vncAddressBookSection.Font = new Font("MV Boli", 11.25f);
			}

			_connectionCollection = _vncAddressBookSection.Connections;

			if (_connectionCollection == null || _connectionCollection.Count == 0)
			{
				_connectionCollection = new ConnectionCollection();

				Connection connection1 = new Connection("CVD Room", "192.168.25.36", "dbd83cfd727a1458");
				Connection connection2 = new Connection("Harvesting", "192.168.27.102", "dbd83cfd727a1458");
				Connection connection3 = new Connection("CCE 제거", "192.168.26.80", "dbd83cfd727a1458");
				Connection connection4 = new Connection("Breaking", "192.168.27.109", "dbd83cfd727a1458");
				Connection connection5 = new Connection("Etching(전)", "192.168.26.16", "dbd83cfd727a1458");
				Connection connection6 = new Connection("Etching(후)", "192.168.26.24", "dbd83cfd727a1458");
				Connection connection7 = new Connection("Boxing", "192.168.27.106", "dbd83cfd727a1458");
				Connection connection8 = new Connection("Palletizing", "192.168.26.56", "dbd83cfd727a1458");
				Connection connection9 = new Connection("출하실", "192.168.25.190", "dbd83cfd727a1458");
				Connection connection10 = new Connection("Test Server", "192.168.25.28", "dbd83cfd727a1458");
				Connection connection11 = new Connection("Real Server", "192.168.25.246", "dbd83cfd727a1458");
				Connection connection12 = new Connection("BO Server", "192.168.25.247", "dbd83cfd727a1458");

				_connectionCollection.Add(connection1);
				_connectionCollection.Add(connection2);
				_connectionCollection.Add(connection3);
				_connectionCollection.Add(connection4);
				_connectionCollection.Add(connection5);
				_connectionCollection.Add(connection6);
				_connectionCollection.Add(connection7);
				_connectionCollection.Add(connection8);
				_connectionCollection.Add(connection9);
				_connectionCollection.Add(connection10);
				_connectionCollection.Add(connection11);
				_connectionCollection.Add(connection12);

				connection10.Options.Add(new Option("FullScreen", "0"));
				connection10.Options.Add(new Option("Scaling", "None"));

				_vncAddressBookSection.Connections = _connectionCollection;
			}

			_optionCollection = _vncAddressBookSection.Options;

			if (_optionCollection == null || _optionCollection.Count == 0)
			{
				_optionCollection = new OptionCollection();

				_optionCollection.Add(new Option("UseLocalCursor", "1"));
				_optionCollection.Add(new Option("UseDesktopResize", "1"));
				_optionCollection.Add(new Option("FullScreen", "0"));
				_optionCollection.Add(new Option("FullScreenChangeResolution", "0"));
				_optionCollection.Add(new Option("UseAllMonitors", "0"));
				_optionCollection.Add(new Option("RelativePtr", "0"));
				_optionCollection.Add(new Option("FullColour", "1"));
				_optionCollection.Add(new Option("LowColourLevel", "2"));
				_optionCollection.Add(new Option("PreferredEncoding", "hextile"));
				_optionCollection.Add(new Option("AutoSelect", "1"));
				_optionCollection.Add(new Option("Shared", "0"));
				_optionCollection.Add(new Option("SendPtrEvents", "1"));
				_optionCollection.Add(new Option("SendKeyEvents", "1"));
				_optionCollection.Add(new Option("SendCutText", "1"));
				_optionCollection.Add(new Option("AcceptCutText", "1"));
				_optionCollection.Add(new Option("ShareFiles", "1"));
				_optionCollection.Add(new Option("DisableWinKeys", "1"));
				_optionCollection.Add(new Option("Emulate3", "0"));
				_optionCollection.Add(new Option("PointerEventInterval", "0"));
				_optionCollection.Add(new Option("Monitor", @"\\.\DISPLAY2"));
				_optionCollection.Add(new Option("Scaling", "None"));
				_optionCollection.Add(new Option("MenuKey", "F8"));
				_optionCollection.Add(new Option("SuppressIME", "1"));
				_optionCollection.Add(new Option("AutoReconnect", "1"));
				_optionCollection.Add(new Option("VerifyId", "2"));
				_optionCollection.Add(new Option("HTTP", "0"));
				_optionCollection.Add(new Option("ProxySetting", "Direct"));
				_optionCollection.Add(new Option("SameProxy", "1"));
				_optionCollection.Add(new Option("HTTPProxy", string.Empty));
				_optionCollection.Add(new Option("HTTPSProxy", string.Empty));
				_optionCollection.Add(new Option("ProxyConfig", string.Empty));

				_vncAddressBookSection.Options = _optionCollection;
			}

			_config.Save(ConfigurationSaveMode.Modified);
        }
        #endregion 
        
        #region GetInstance
		/// <summary>
		/// Wrapper class의 Single Ton BufferSize을 위한 새 인스턴스를 초기화 합니다.
		/// 
		/// </summary>
		/// <returns></returns>
		public static Wrapper GetInstance()
		{
			string exePath = String.Empty;

			return GetInstance(exePath);
		}
        
		/// <summary>
        /// Wrapper class의 Single Ton BufferSize을 위한 새 인스턴스를 초기화 합니다.
		/// <param name="exePath">실행 파일과 연결된 구성 파일에 대한 경로입니다.</param>
		/// </summary>
        /// <returns></returns>
		public static Wrapper GetInstance(string exePath)
        {
        	if (_newInstance == null)
				_newInstance = new Wrapper(exePath);
        
        	return _newInstance;
        }
        #endregion

        #endregion
                
		#region Properties
		/// <summary>
		/// VncViewer의 경로를 구하거나 설정합니다.
		/// </summary>
		public string VncViewer
		{
			get
			{
				return _vncAddressBookSection.VncViewer;
			}
			set
			{
				_vncAddressBookSection.VncViewer = value;
			}
		}

		public Font Font
		{
			get
			{
				return _vncAddressBookSection.Font;
			}
			set
			{
				_vncAddressBookSection.Font = value;
			}
		}
        /// <summary>
		/// Connections를 구하거나 설정합니다.<br/>
		/// <remarks>Connections를 추가나 삭제 모두 제거는 <seealso cref="ConnectionAdd"/>, <seealso cref="ConnectionRemove"/>, <seealso cref="ConnectionClear"/> 메서드를 사용하십시오.</remarks>
        /// </summary>
		public Dictionary<string, Connection> Connections
        {
        	get
        	{
				if (_connections == null)
				{
					_connections = new Dictionary<string, Connection>();

					foreach (Connection connection in _connectionCollection)
						_connections.Add(connection.Alias, connection);
				}
                
				return _connections;
        	}
        	set
        	{
				if (value.Count > 0)
				{
					_connectionCollection.Clear();

					foreach (string alias in value.Keys)
					{
						_connectionCollection.Add(new Connection(alias, _connectionCollection[alias].Host,
													  _connectionCollection[alias].UserName,
													  _connectionCollection[alias].Password,
													  _connectionCollection[alias].Identity,
													  _connectionCollection[alias].Encryption,
													  _connectionCollection[alias].SingleSingOn,
													  _connectionCollection[alias].SelectDesktop));
					}

					_connections = value;
				}				
        	}
        }

		/// <summary>
		/// Connections를 구하거나 설정합니다.<br/>
		/// <remarks>Connections를 추가나 삭제 모두 제거는 <seealso cref="ConnectionAdd"/>, <seealso cref="ConnectionRemove"/>, <seealso cref="ConnectionClear"/> 메서드를 사용하십시오.</remarks>
		/// </summary>
		public Dictionary<string, string> Options
		{
			get
			{
				if (_options == null)
				{
					_options = new Dictionary<string, string>();

					foreach (Option option in _optionCollection)
						_options.Add(option.Key, option.Value);
				}

				return _options;
			}
			set
			{
				if (value.Count > 0)
				{
					_optionCollection.Clear();

					foreach (string key in value.Keys)
						_optionCollection.Add(new Option(key, _options[key]));

					_options = value;
				}
			}
		}
		#endregion

		/// <summary>
		/// Upate Connection를 추가합니다.
		/// </summary>
		/// <param name="alias"></param>
		/// <param name="connectInfo"></param>
		public void ConnectionAdd(string alias, ConnectInfo connectInfo)
		{
			ConnectionAdd(alias, connectInfo, null);
		}

        /// <summary>
		/// Upate Connection를 추가합니다.
		/// </summary>
		/// <param name="alias"></param>
		/// <param name="connectInfo"></param>
		/// <param name="option"></param>
		public void ConnectionAdd(string alias, ConnectInfo connectInfo, OptionCollection options)
		{
			Connection connection = new Connection(alias, connectInfo);

			if (options != null && options.Count > 0)
				connection.Options = options;

			_connectionCollection.Add(connection);
		}

		/// <summary>
		/// Upate Connection를 제거합니다.
		/// </summary>
		/// <param name="alias">Connection alias(ex : Real)</param>
		public void ConnectionRemove(string alias)
		{
			_connectionCollection.Remove(alias);
		}

		/// <summary>
		/// Upate Connection 목록을 모두 제거 합니다.
		/// </summary>
		public void ConnectionClear()
		{
			_connectionCollection.Clear();
		}

		/// <summary>
		/// 대상 Filename을 추가합니다.
		/// </summary>
		/// <param name="key">추가할 filename</param>
		/// <param name="value"></param>
		public void OptionAdd(string key, string value)
		{
			_optionCollection.Add(new Option(key, value));
		}

		/// <summary>
		/// 대상 Filename을 제거합니다.
		/// </summary>
		/// <param name="key">제거할 filename</param>
		public void OptionRemove(string key)
		{
			_optionCollection.Remove(key);
		}

		/// <summary>
		/// 대상 Filename 목록을 모두 제거 합니다.
		/// </summary>
		public void OptionClear()
		{
			_optionCollection.Clear();
		}

		/// <summary>
		/// Config 파일에 변경 사항만을 저장합니다.
		/// </summary>		
		public void SaveModifed()
		{
			_config.Save(ConfigurationSaveMode.Modified);
		}

		/// <summary>
		/// Config 파일에 저장 합니다.
		/// </summary>
		public void Save()
		{
			_config.Save();
		}
	}
}
