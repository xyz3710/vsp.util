﻿/**********************************************************************************************************************/
/*	Domain		:	VncAddressBook.Handler.OptionCollection
/*	Creator		:	KIMKIWON\xyz37
/*	Create		:	2008년 6월 10일 화요일 오후 5:27
/*	Purpose		:	Option 컬렉션을 포함하는 OptionCollection 구성 요소를 나타냅니다.
/*	Modifier	:	
/*	Update		:	
/*	Changes		:	
/*	Comment		:	
/**********************************************************************************************************************/

using System;
using System.Collections.Generic;
using System.Text;
using System.Configuration;
using System.ComponentModel;

namespace VncAddressBook.Handler
{
	/// <summary>
	/// Option 컬렉션을 포함하는 OptionCollection 구성 요소를 나타냅니다.
	/// </summary>
	[ConfigurationCollection(typeof(Option), CollectionType=ConfigurationElementCollectionType.AddRemoveClearMap, AddItemName="option")]
	public class OptionCollection : GenericCollection<Option>
	{
		/*
		/// <summary>
		/// Host를 구하거나 설정합니다.
		/// </summary>
		[ConfigurationProperty("host", IsRequired=true)]
		[Description("Host를 구하거나 설정합니다.")]
		public Connection Host
		{
			get
			{
				return base.ElementInformation.Properties["host"].Value as Connection;
			}
			set
			{
				base.ElementInformation.Properties["host"].Value = value;
			}
		}
		*/
	}	
}
