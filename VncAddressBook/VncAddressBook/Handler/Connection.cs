﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Configuration;
using System.ComponentModel;

namespace VncAddressBook.Handler
{
	public class Connection : ConfigurationElement
	{
		private ConnectInfo _connectInfo;

		/// <summary>
		/// Connection class의 새 인스턴스를 초기화 합니다.
		/// </summary>
		public Connection()
		{
		}

		/// <summary>
		/// Connections class의 새 인스턴스를 초기화 합니다.
		/// </summary>
		/// <param name="alias"></param>
		/// <param name="connectInfo"></param>
		public Connection(string alias, ConnectInfo connectInfo)
		{
			Alias = alias;
			Host = connectInfo.Host;
			UserName = connectInfo.UserName;
			Password = connectInfo.Password;
			Identity = connectInfo.Identity;
			Encryption = connectInfo.Encryption;
			SingleSingOn = connectInfo.SingleSignOn;
			SelectDesktop = connectInfo.SelectDesktop;
			_connectInfo = connectInfo;
		}

        /// <summary>
		/// Connections class의 새 인스턴스를 초기화 합니다.
		/// </summary>
		/// <param name="alias"></param>
		/// <param name="host"></param>
		/// <param name="password"></param>
		public Connection(string alias, string host, string password)
			: this(alias, host, String.Empty, password)
		{
		}

		/// <summary>
		/// Connections class의 새 인스턴스를 초기화 합니다.
		/// </summary>
		/// <param name="alias"></param>
		/// <param name="host"></param>
		/// <param name="userName"></param>
		/// <param name="password"></param>
		public Connection(string alias, string host, string userName, string password)
			: this(alias, host, userName, password, string.Empty, "Server", "1", string.Empty)
		{
		}

		/// <summary>
		/// Connections class의 새 인스턴스를 초기화 합니다.
		/// </summary>
		/// <param name="alias"></param>
		/// <param name="host"></param>
		/// <param name="userName"></param>
		/// <param name="password"></param>
		/// <param name="identity"></param>
		/// <param name="encryption"></param>
		/// <param name="singleSignOn"></param>
		/// <param name="selectDesktop"></param>
		public Connection(string alias, string host, string userName, string password, string identity, string encryption, string singleSignOn, string selectDesktop)
		{
			Alias = alias;
			Host = host;
			UserName = userName;
			Password = password;
			Identity = identity;
			Encryption = encryption;
			SingleSingOn = singleSignOn;
			SelectDesktop = selectDesktop;
		}

		/// <summary>
        /// Alias를 구하거나 설정합니다.
        /// </summary>
        [ConfigurationProperty("alias", IsRequired=true, IsKey=true)]
        [Description("Alias를 구하거나 설정합니다.")]
        public string Alias
        {
        	get
        	{
        		return (string)this["alias"];
        	}
        	set
        	{
        		this["alias"] = value;
        	}
        }
        
		/// <summary>
        /// Host를 구하거나 설정합니다.
        /// </summary>
        [ConfigurationProperty("host", IsRequired=true)]
        [Description("Host를 구하거나 설정합니다.")]
        public string Host
        {
        	get
        	{
        		return (string)this["host"];
        	}
        	set
        	{
        		this["host"] = value;
        	}
        }
        
        /// <summary>
        /// UserName를 구하거나 설정합니다.
        /// </summary>
        [ConfigurationProperty("userName", IsRequired=false)]
        [Description("UserName를 구하거나 설정합니다.")]
        public string UserName
        {
        	get
        	{
        		return (string)this["userName"];
        	}
        	set
        	{
        		this["userName"] = value;
        	}
        }
        
        /// <summary>
        /// Password를 구하거나 설정합니다.
        /// </summary>
        [ConfigurationProperty("password", IsRequired=true)]
        [Description("Password를 구하거나 설정합니다.")]
        public string Password
        {
        	get
        	{
        		return (string)this["password"];
        	}
        	set
        	{
        		this["password"] = value;
        	}
        }
        
        /// <summary>
        /// Identity를 구하거나 설정합니다.
        /// </summary>
        [ConfigurationProperty("identity", IsRequired=false)]
        [Description("Identity를 구하거나 설정합니다.")]
        public string Identity
        {
        	get
        	{
        		return (string)this["identity"];
        	}
        	set
        	{
        		this["identity"] = value;
        	}
        }
        
        /// <summary>
        /// Encryption를 구하거나 설정합니다.
        /// </summary>
        [ConfigurationProperty("encryption", DefaultValue="Server", IsRequired=false)]
        [Description("Encryption를 구하거나 설정합니다.")]
        public string Encryption
        {
        	get
        	{
        		return (string)this["encryption"];
        	}
        	set
        	{
        		this["encryption"] = value;
        	}
        }
        
        /// <summary>
        /// SingleSingOn를 구하거나 설정합니다.
        /// </summary>
        [ConfigurationProperty("singleSingOn", DefaultValue="1", IsRequired=true)]
        [Description("SingleSingOn를 구하거나 설정합니다.")]
		public string SingleSingOn
        {
        	get
        	{
				return (string)this["singleSingOn"];
        	}
        	set
        	{
        		this["singleSingOn"] = value;
        	}
        }
        
        /// <summary>
        /// SelectDesktop를 구하거나 설정합니다.
        /// </summary>
        [ConfigurationProperty("selectDesktop", IsRequired=false)]
        [Description("SelectDesktop를 구하거나 설정합니다.")]
        public string SelectDesktop
        {
        	get
        	{
        		return (string)this["selectDesktop"];
        	}
        	set
        	{
        		this["selectDesktop"] = value;
        	}
        }

		/// <summary>
		/// Options를 구하거나 설정합니다.
		/// </summary>
		[ConfigurationProperty("options", IsRequired=false)]
		[Description("Options를 구하거나 설정합니다.")]
		public OptionCollection Options
		{
			get
			{
				return (OptionCollection)this["options"];
			}
			set
			{
				this["options"] = value;
			}
		}
		
		/// <summary>
		/// ConnectInfo를 구합니다.
		/// </summary>
		public ConnectInfo ConnectInfo
		{
			get
			{
				if (_connectInfo == null)
					_connectInfo = new ConnectInfo(Host,
										   UserName,
										   Password,
										   Identity,
										   Encryption,
										   SingleSingOn,
										   SelectDesktop);

				return _connectInfo;
			}
		}
	}
}
