﻿/**********************************************************************************************************************/
/*	Domain		:	VncAddressBook.Handler.ConnectionCollection
/*	Creator		:	KIMKIWON\xyz37
/*	Create		:	2008년 6월 10일 화요일 오후 5:27
/*	Purpose		:	Connection 컬렉션을 포함하는 ConnectionCollection 구성 요소를 나타냅니다.
/*	Modifier	:	
/*	Update		:	
/*	Changes		:	
/*	Comment		:	
/**********************************************************************************************************************/

using System;
using System.Configuration;
using VncAddressBook.Handler;
using System.ComponentModel;

namespace VncAddressBook.Handler
{
	/// <summary>
	/// Connection 컬렉션을 포함하는 ConnectionCollection 구성 요소를 나타냅니다.
	/// </summary>
	[ConfigurationCollection(typeof(Connection), CollectionType=ConfigurationElementCollectionType.AddRemoveClearMap, AddItemName="host")]
	internal class ConnectionCollection : GenericCollection<Connection>
	{
	}	
}
