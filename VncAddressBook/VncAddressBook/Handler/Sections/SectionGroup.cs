﻿/**********************************************************************************************************************/
/*	Domain		:	VncAddressBook.Handler.SectionGroup
/*	Creator		:	KIMKIWON\xyz37
/*	Create		:	2008년 6월 11일 수요일 오후 12:10
/*	Purpose		:	구성 파일에 있는 관련된 섹션의 그룹을 나타냅니다.
/*	Modifier	:	
/*	Update		:	
/*	Changes		:	
/*	Comment		:	
/**********************************************************************************************************************/

using System;
using System.Collections.Generic;
using System.Text;
using System.Configuration;
using System.ComponentModel;

namespace VncAddressBook.Handler
{
	/// <summary>
	/// 구성 파일에 있는 관련된 섹션의 그룹을 나타냅니다.
	/// </summary>
	internal class SectionGroup : ConfigurationSectionGroup
	{
		/// <summary>
		/// VncAddressBook.Handler의 SectionGroup을 구합니다.
		/// </summary>
		public const string SectionGroupNameString = "VncAddressBook.Handler.SectionGroup";

		/// <summary>
		/// SectionGroup class의 새 인스턴스를 초기화합니다.
		/// </summary>
		public SectionGroup()
		{
		}
		
		/// <summary>
		/// VncAddressBook Section을 구하거나 설정합니다.
        /// </summary>
		[ConfigurationProperty("VncAddressBook", DefaultValue="VncAddressBook", IsRequired=false)]
		[Description("VncAddressBook Section을 구하거나 설정합니다.")]
		public VncAddressBookSection VncAddressBook
        {
        	get
        	{
				return (VncAddressBookSection)base.Sections["VncAddressBook"];
        	}
        	set
        	{
				base.Sections.Add("VncAddressBook", value);
        	}
        }
	}
}
