﻿/**********************************************************************************************************************/
/*	Domain		:	VncAddressBook.Handler.VncAddressBookSection
/*	Creator		:	KIMKIWON\xyz37
/*	Create		:	2008년 6월 10일 화요일 오후 5:31
/*	Purpose		:	Updater Core Section에 관련된 설정 파일을 관리하는 클래스 입니다.
/*	Modifier	:	
/*	Update		:	
/*	Changes		:	
/*	Comment		:	
/**********************************************************************************************************************/

using System;
using System.Collections.Generic;
using System.Text;
using System.Configuration;
using System.ComponentModel;
using System.Drawing;

namespace VncAddressBook.Handler
{
	/// <summary>
	/// VncAddressBook Section에 관련된 설정 파일을 관리하는 클래스 입니다.
	/// </summary>
	internal class VncAddressBookSection : ConfigurationSection
	{
		/// <summary>
		/// VncAddressBook.Handler의 Updater SectionName string을 구합니다.
		/// </summary>
		public const string SectionNameString = "VncAddressBook";

		/// <summary>
		/// VncAddressBookSection class의 새 인스턴스를 초기화합니다.
		/// </summary>
		public VncAddressBookSection()
		{
		}

		/// <summary>
        /// VncViewer를 구하거나 설정합니다.
        /// </summary>
        [ConfigurationProperty("vncViewer", DefaultValue=@"C:\Program Files\Util\VNC4\VncViewer.exe", IsRequired=true)]
        [Description("VncViewer를 구하거나 설정합니다.")]
        public string VncViewer
        {
        	get
        	{
        		return (string)this["vncViewer"];
        	}
        	set
        	{
        		this["vncViewer"] = value;
        	}
        }
        
		/// <summary>
        /// Font를 구하거나 설정합니다.
        /// </summary>
        [ConfigurationProperty("font", DefaultValue="Tahoma", IsRequired=false)]
        [Description("Font를 구하거나 설정합니다.")]
        public Font Font
        {
        	get
        	{
        		return (Font)this["font"];
        	}
        	set
        	{
        		this["font"] = value;
        	}
        }
        
		/// <summary>
        /// Connections를 구하거나 설정합니다.
        /// </summary>
        [ConfigurationProperty("connections", IsRequired=true)]
        [Description("Connections를 구하거나 설정합니다.")]
        public ConnectionCollection Connections
        {
        	get
        	{
        		return (ConnectionCollection)this["connections"];
        	}
        	set
        	{
        		this["connections"] = value;
        	}
        }
        
        /// <summary>
        /// Options를 구하거나 설정합니다.
        /// </summary>
        [ConfigurationProperty("options", IsRequired=true)]
        [Description("Options를 구하거나 설정합니다.")]
        public OptionCollection Options
        {
        	get
        	{
        		return (OptionCollection)this["options"];
        	}
        	set
        	{
        		this["options"] = value;
        	}
        }
	}
}
