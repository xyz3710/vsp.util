﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Configuration;
using System.ComponentModel;

namespace VncAddressBook.Handler
{
	public class Option : ConfigurationElement
	{
		/// <summary>
		/// Option class의 새 인스턴스를 초기화 합니다.
		/// </summary>
		public Option()
		{
		}

		/// <summary>
		/// Option class의 새 인스턴스를 초기화 합니다.
		/// </summary>
		/// <param name="key"></param>
		public Option(string key)
			: this(key, String.Empty)
		{
		}

        /// <summary>
		/// Option class의 새 인스턴스를 초기화 합니다.
		/// </summary>
		/// <param name="key"></param>
		/// <param name="value"></param>
		public Option(string key, string value)
		{
			Key = key;
			Value = value;
		}

		/// <summary>
        /// Key를 구하거나 설정합니다.
        /// </summary>
        [ConfigurationProperty("key", IsRequired=true, IsKey=true)]
        [Description("Key를 구하거나 설정합니다.")]
        public string Key
        {
        	get
        	{
        		return (string)this["key"];
        	}
        	set
        	{
        		this["key"] = value;
        	}
        }
        
        /// <summary>
        /// Value를 구하거나 설정합니다.
        /// </summary>
        [ConfigurationProperty("value", IsRequired=true)]
        [Description("Value를 구하거나 설정합니다.")]
        public string Value
        {
        	get
        	{
        		return (string)this["value"];
        	}
        	set
        	{
        		this["value"] = value;
        	}
        }        
	}
}
