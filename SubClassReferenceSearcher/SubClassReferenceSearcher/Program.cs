﻿/**********************************************************************************************************************/
/*	Domain		:	SubClassReferenceSearcher.Program
/*	Creator		:	KIMKIWON\xyz37(김기원)
/*	Create		:	2012년 1월 11일 수요일 오전 10:47
/*	Purpose		:	특정 클래스에서 파생된 모든 클래스를 검사합니다.
/*--------------------------------------------------------------------------------------------------------------------*/
/*	Modifier	:	
/*	Update		:	
/*	Changes		:	
/*--------------------------------------------------------------------------------------------------------------------*/
/*	Comment		:	
/*--------------------------------------------------------------------------------------------------------------------*/
/*	Reviewer	:	Kim Ki Won
/*	Rev. Date	:	
/**********************************************************************************************************************/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Reflection;

namespace SubClassReferenceSearcher
{
	class Program
	{
		static void Main(string[] args)
		{
			string assemblyFolder = @"C:\Program Files (x86)\Microsoft ASP.NET\ASP.NET MVC 3\Assemblies";
			//string assemblyFolder = @"C:\Windows\Microsoft.NET\Framework\v4.0.30319";
			var assemblies = Directory.GetFiles(assemblyFolder, "System.Web.Mvc.dll", SearchOption.TopDirectoryOnly);
			string typeFullName = "System.Web.Mvc.ValidateInputAttribute";
			//string typeFullName = "System.Exception";
			Dictionary<string, List<string>> results = new Dictionary<string, List<string>>();

			foreach (string assemblyFilename in assemblies)
			{
				Assembly assembly = null;

				try
				{
					//assembly = Assembly.GetExecutingAssembly();
					assembly = Assembly.LoadFrom(assemblyFilename);
				}
				catch
				{
					continue;
				}

				var assemblyTypes = assembly.GetTypes();

				foreach (var type in assemblyTypes)
				{
					List<string> types = GetBaseType(typeFullName, type).ToList();

					if (types.Count == 0)
						continue;
					else if (types.Count > 0)
					{
						string resultTypeFullName = type.FullName;
						List<string> popTypes = new List<string>(types);

						// 마지막 값만 dictionary에 넣는다.
						if (results.ContainsKey(resultTypeFullName) == false)
						{
							string removeKey = string.Empty;

							foreach (string key in results.Keys)
							{
								string lastKey = results[key][results[key].Count - 1];

								// 중복을 피하기 위해 더 하위에 기존 타입이 있다면 기존 컬렉션에 제거 한다.

								if (key == lastKey)
								{
									removeKey = key;

									break;

								}
							}

							//if (removeKey != string.Empty)
							//	results.Remove(removeKey);

							results.Add(resultTypeFullName, popTypes.ToList());
						}
					}
				}
			}

			foreach (string key in results.Keys)
			{
				List<string> value = results[key];
								
				for (int i = 0; i < value.Count; i++)
					Console.WriteLine("{0}{1}", string.Empty.PadRight(i, '\t'), value[i]);
			}

			var typeGroup = results
				.GroupBy(key => key.Value[0],
					(key, fullName) => fullName);

			foreach (var item in typeGroup)
			{
				
			}
		}

		private static Stack<string> GetBaseType(string checkTypeFullName, Type type, Stack<string> baseTypeQueue = null)
		{
			Type baseType = type.BaseType;

			if (baseTypeQueue == null)
				baseTypeQueue = new Stack<string>();

			if (baseType == null)
				return baseTypeQueue;

			switch (baseType.FullName)
			{
				case "System.Object":
				case "System.ValueType":
				case "System.Enum":

					return baseTypeQueue;
			}

			baseTypeQueue.Push(type.FullName);

			if (baseType.FullName == checkTypeFullName)
			{
				baseTypeQueue.Push(baseType.FullName);

				return baseTypeQueue;
			}

			//baseTypeQueue.Push(baseType.FullName);

			return GetBaseType(checkTypeFullName, type.BaseType, baseTypeQueue);
		}
	}
}
