using System;
using System.Collections;
using System.Data;
using System.Text;
using System.IO;

using My.FtpClient;
//using My.Debug;

using MapFileTransfer.Table;
using MapFileTransfer.Common;

namespace MapFileTransfer
{
	#region MainClass
	/// <summary>
	/// 
	/// </summary>
	public class MainClass
	{
		/// <summary>
		/// 해당 응용 프로그램의 주 진입점입니다.
		/// </summary>
		[STAThread]
		public static void Main(string[] args)
		{
			MapFileTransferDriver mapFtpDriver = new MapFileTransferDriver();

			mapFtpDriver.Run();
		}
	}
	#endregion

	/// <summary>
	/// Map FTP driver class
	/// </summary>
	public class MapFileTransferDriver
	{
		private AsynchronousConnection _ftp;
		private string _vendor;
		private ArrayList _fileList;

		#region Constructor
		/// <summary>
		/// 기본 Vendor인 90001을 저장합니다.
		/// </summary>
		public MapFileTransferDriver()
		{
			if (_vendor == null)
				_vendor = "90001";
		}

		/// <summary>
		/// Vendor를 지정할 수 있습니다.
		/// </summary>
		/// <param name="vendor"></param>
		public MapFileTransferDriver(string vendor)
			: this()
		{
			_vendor = vendor;
		}
		#endregion

		#region Run routine
		/// <summary>
		/// Routine을 시작합니다.
		/// </summary>
		public void Run()
		{
			_fileList = getFileList();

			if (_fileList.Count < 1)
			{
				showMessage("FileList가 없습니다.");
				return;
			}

			try
			{
				setFtpOpen();

				for (int i = 0; i < _fileList.Count; i++)
				{
					// ArrayList에 Hashtable은 항상 1개만 들어간다.
					foreach (object key in ((Hashtable)_fileList[i]).Keys)
					{
						sendFile((string)key);
					}

					System.Threading.Thread.Sleep(500);
				}
			}
			catch (Exception ex)
			{
				Log.Write(ex.Message);

				string msg = string.Format(ex.Message);
			}
			finally
			{
				setFtpClose();
			}
		}
		#endregion

		#region Private methods
		private void setFtpOpen()
		{
			VendorFtpTable vendorFtp = new VendorFtpTable(_vendor);
			string remoteHost = vendorFtp.RemoteHost;
			string userId = vendorFtp.UserId;
			string password = vendorFtp.Password;

			_ftp = new AsynchronousConnection();

			_ftp.OnOpenning += new My.FtpClient.FtpBase.OpenEventHandler(_ftp_OnOpenning);
			_ftp.OnOpenned += new My.FtpClient.FtpBase.OpenEventHandler(_ftp_OnOpenned);
			_ftp.OnBeginPutFile += new My.FtpClient.FtpBase.TransferFileInfoEventHandler(_ftp_OnBeginPutFile);
			_ftp.OnTransferingPutFile += new My.FtpClient.FtpBase.TransferFileInfoEventHandler(_ftp_OnTransferingPutFile);
			_ftp.OnSuccessPutFile += new My.FtpClient.FtpBase.TransferFileInfoEventHandler(_ftp_OnSuccessPutFile);
			_ftp.OnFailurePutFile += new My.FtpClient.FtpBase.TransferFileInfoEventHandler(_ftp_OnFailurePutFile);
			_ftp.OnClosing += new My.FtpClient.FtpBase.OpenEventHandler(_ftp_OnClosing);
			_ftp.OnClosed += new My.FtpClient.FtpBase.OpenEventHandler(_ftp_OnClosed);

			_ftp.IsLogMessage = true;

			_ftp.Open(remoteHost, userId, password);
		}

		private void setFtpClose()
		{
			if (_ftp.IsOpenned == true)
				_ftp.Close();
		}

		private ArrayList getFileList()
		{
			WaferDataTable waferData = new WaferDataTable(_vendor);

			return waferData.FileList;
		}

		private void createEndFile(string endFilePath)
		{
			FileStream fsEndFile = null;

			try
			{
				fsEndFile = new FileStream(endFilePath, FileMode.Create);
			}
			catch (Exception ex)
			{
				showMessage(ex.Message);
			}
			finally
			{
				if (fsEndFile != null)
					fsEndFile.Close();
			}
		}

		private void sendFile(string filename)
		{
			string path = Constant.LOCAL_PATH + @"\";
			string endFilePath = path + Path.GetFileNameWithoutExtension(filename) + Constant.END_FILE_EXT;

			// 0 byte의 end file을 생성한다.
			createEndFile(endFilePath);

			if (_ftp.IsOpenned == true)
			{
				_ftp.PutFile(path + filename, TransferType.Binary);
				_ftp.PutFile(endFilePath, TransferType.Binary);
			}
		}

		private void updateTables(string filePath)
		{
			string fileExt = Path.GetExtension(filePath);

			if (fileExt == Constant.END_FILE_EXT)
				return;

			string lotId = string.Empty;
			string filename = Path.GetFileName(filePath);
			if (_fileList != null)
			{
				for (int i = 0; i < _fileList.Count; i++)
				{
					object lot = ((Hashtable)_fileList[i])[filename];

					if (lot != null)
					{
						lotId = (string)lot;

						break;
					}
				}
			}

			OriginalLotTable originalLot = new OriginalLotTable(_vendor, lotId, filename);

			if (originalLot.Update() > 0)
				showMessage(Constant.TABLE_ORIGINAL + " Update에 성공하였습니다.");
			else
				showMessage(Constant.TABLE_ORIGINAL + " Update에 실패하였습니다.");
			;


			TempLotTable tempLot = new TempLotTable(_vendor, lotId, filename);

			if (tempLot.Update() > 0)
				showMessage(Constant.TABLE_TEMPORARY + " Update에 성공하였습니다.");
			else
				showMessage(Constant.TABLE_TEMPORARY + " Update에 실패하였습니다.");
			;

			if (tempLot.Delete() > 0)
				showMessage(Constant.TABLE_TEMPORARY + " Delete에 성공하였습니다.");
			else
				showMessage(Constant.TABLE_TEMPORARY + " Delete에 실패하였습니다.");
			;
		}

		private void showMessage(string message)
		{
			string logString = string.Format("[{0}] {1}", DateTime.Now.ToString(), message);

			Console.WriteLine(logString);

			Log.Write(message);
		}

		#region FtpClient event handler
		private void _ftp_OnOpenning(object sender, OpenEventArgs e)
		{
			string msg = string.Format("{0}\t에 연결중입니다.", e.RemoteHost);

			showMessage(msg);
		}

		private void _ftp_OnOpenned(object sender, OpenEventArgs e)
		{
			string msg = string.Format("{0}\t에 연결되었습니다.", e.RemoteHost);

			showMessage(msg);
		}

		private void _ftp_OnBeginPutFile(object sender, TransferFileEventArgs e)
		{
			string msg = string.Format("{0}\t 전송(put) 준비중입니다.", e.LocalFileName);

			showMessage(msg);
		}

		private void _ftp_OnTransferingPutFile(object sender, TransferFileEventArgs e)
		{
			string msg = string.Format("\n[{0}] {1}\t 전송(put)중입니다.\t[{2,3:N2}]", DateTime.Now.ToString(), e.LocalFileName, e.TransferedRatio);

			Console.Write(msg);
		}

		private void _ftp_OnSuccessPutFile(object sender, TransferFileEventArgs e)
		{
			string msg = string.Format("{0}\t 전송(put) 성공하였습니다.\t[{1,12:N0} / {2,12:N0}] bytes",
				e.LocalFileName, e.TransferedStreamByte, e.TotalStreamByte);

			showMessage(msg);

			updateTables(e.LocalFileName);
		}

		private void _ftp_OnFailurePutFile(object sender, TransferFileEventArgs e)
		{
			string msg = string.Format("{0}\t 전송(put) 실패하였습니다.\t[{1,12:N0} / {2,12:N0}] bytes",
				e.LocalFileName, e.TransferedStreamByte, e.TotalStreamByte);

			showMessage(msg);
		}

		private void _ftp_OnClosing(object sender, OpenEventArgs e)
		{
			string msg = string.Format("{0}\t로 부터 종료 중입니다.", e.RemoteHost);

			showMessage(msg);
		}

		private void _ftp_OnClosed(object sender, OpenEventArgs e)
		{
			string msg = string.Format("{0}\t로 부터 종료 되었습니다.", e.RemoteHost);

			showMessage(msg);

			#region Print message
			if (_ftp.IsLogMessage == true)
			{
				Console.WriteLine();
				foreach (string str in _ftp.MessageList)
				{
					Console.WriteLine(str);
				}
			}
			#endregion

			string tmp = string.Empty;
			Log.Write(tmp.PadLeft(79, '='));
		}
		#endregion
		#endregion
	}
}
