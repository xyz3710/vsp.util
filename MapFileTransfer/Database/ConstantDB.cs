/**********************************************************************************************************************/
/*	Name		:	My.Database.ConstantDB
/*	Purpose		:	상수 목록을 위한 class
/*	Creator		:	Kim Ki Won
/*	Create		:	2006년 8월 22일 화요일 오후 9:36:06
/*	Modifier	:	
/*	Update		:	2006년 8월 22일 화요일
/*	Comment		:	
/**********************************************************************************************************************/

using System;

namespace My.Database
{
	/// <summary>
	/// 상수 목록 class 입니다.
	/// </summary>
	public class ConstantDB
	{
		public const string DEFAULT_USERID = "cmadm";
		public const string DEFAULT_PASSWORD = "cmadm";
		public const string DEFAULT_DATA_SOURCE = "CJCMS_10-20";

		public const string CONNECTION_COULD_NOT_OPEN = "Connection could not open.";
	}
}
