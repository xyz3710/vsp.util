/**********************************************************************************************************************/
/*	Name		:	MapFileTransfer.Database.DataManipulation
/*	Purpose		:	해당 테이블을 Insert/Update/Delete 및 Query 결과를 합니다.
/*	Creator		:	Kim Ki Won
/*	Create		:	2006년 8월 22일 화요일 오후 9:35:37
/*	Modifier	:	
/*	Update		:	2006년 9월 14일 목요일
/*	Comment		:	ExecuteNonQuery, ExecuteQuery의 Tablename parameter 제거
/**********************************************************************************************************************/

using System;
using System.Data;
using System.Data.OleDb;
using MapFileTransfer.Common;

namespace My.Database
{
	/// <summary>
	/// 해당 테이블을 Update/Delete 합니다.
	/// </summary>
	public class DataManipulation
	{
		#region Members
		private IDbConnection _connection;
		#endregion

		#region ExecuteNonQuery
		/// <summary>
		/// 테이블을 Insert/Update/Delete 합니다.
		/// </summary>
		/// <param name="commandText">command text</param>
		/// <returns>성공한 row count</returns>
		public int ExecuteNonQuery(string commandText)
		{
			IDbTransaction transaction = null;
			int result = -1;

			try
			{
#if DEBUG
				Log.Write(commandText);
#endif
				openDataConnection();

				IDbCommand command = _connection.CreateCommand();
				transaction = _connection.BeginTransaction(IsolationLevel.Serializable);

				command.Transaction = transaction;

				command.CommandText = commandText;
				result = command.ExecuteNonQuery();

				if (result != -1)
					transaction.Commit();
				else
				{
					transaction.Rollback();

					string message = string.Format("[{0}] {1} 작업에 실패 하였습니다.",
						getTablename(commandText), getOperation(commandText).ToString());

					throw new InvalidOperationException(message);
				}
			}
			catch (Exception ex)
			{
				Log.Write(ex.Message);

				if (transaction != null)
					transaction.Rollback();

				throw new InvalidOperationException(ex.Message);
			}
			finally
			{
				closeDataConnection();
			}

			return result;
		}
		#endregion

		#region ExecuteQuery
		/// <summary>
		/// 테이블의 Select 결과를 구합니다.
		/// </summary>
		/// <param name="commandText">command text</param>
		/// <returns></returns>
		public DataTable ExecuteQuery(string commandText)
		{
			DataTable dataTable = new DataTable();

			try
			{
#if DEBUG
				Log.Write(commandText);
#endif
				openDataConnection();

				OleDbDataAdapter adapter = new OleDbDataAdapter();
				DataSet ds = new DataSet();

				adapter.SelectCommand = new OleDbCommand(commandText, (OleDbConnection)_connection);
				adapter.Fill(ds);

				if (ds.Tables.Count > 0)
					dataTable = ds.Tables[0];
			}
			catch (Exception ex)
			{
				string message = string.Format("검색 결과를 가져오지 못했습니다.\r\n{0}", ex.Message);

				Log.Write(message);

				throw new InvalidOperationException(message);
			}
			finally
			{
				closeDataConnection();
			}

			return dataTable;
		}
		#endregion

		#region Private methods
		private void openDataConnection()
		{
			try
			{
				if (_connection == null)
					_connection = DataConnection.GetInstance().GetConncection();

				if (_connection.State != ConnectionState.Open)
					_connection.Open();

				if (_connection.State != ConnectionState.Open)
					throw new InvalidOperationException(ConstantDB.CONNECTION_COULD_NOT_OPEN);
			}
			catch (Exception ex)
			{
				Log.Write(ex.Message);

				throw new InvalidOperationException(ex.Message);
			}
		}

		private void closeDataConnection()
		{
			try
			{
				if (_connection != null)
					_connection.Close();
			}
			catch (Exception ex)
			{
				Log.Write(ex.Message);

				throw new InvalidOperationException(ex.Message);
			}
		}

		private string getTablename(string commandText)
		{
			string ret = string.Empty;
			string insertParsingKey = "INSERT INTO ";
			string updateParsingKey = "UPDATE ";
			string deleteParsingKey = "DELETE FROM ";
			string parsingKey = string.Empty;
			int index = 0;

			switch (getOperation(commandText))
			{
				case Operation.Insert:
					parsingKey = insertParsingKey;

					break;
				case Operation.Update:
					parsingKey = updateParsingKey;

					break;
				case Operation.Delete:
					parsingKey = deleteParsingKey;

					break;
			}

			index = commandText.ToUpper().IndexOf(parsingKey);

			if (index != -1)
				ret = commandText.Remove(index, parsingKey.Length).Split(' ')[0];

			return ret;
		}

		private Operation getOperation(string commandText)
		{
			Operation retOperation;
			string insertParsingKey = "INSERT";
			string updateParsingKey = "UPDATE";
			string parsingKey = string.Empty;

			if (commandText.ToUpper().IndexOf(insertParsingKey) != -1)
				retOperation = Operation.Insert;
			else if (commandText.ToUpper().IndexOf(updateParsingKey) != -1)
				retOperation = Operation.Update;
			else
				retOperation = Operation.Delete;

			return retOperation;
		}
		#endregion
	}
}
