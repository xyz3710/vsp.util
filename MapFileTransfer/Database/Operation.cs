/**********************************************************************************************************************/
/*	Name		:	MapFileTransfer.Database.Operation
/*	Purpose		:	Insert, Update, Delete 할 작업 종류 enum
/*	Creator		:	Kim Ki Won
/*	Create		:	2006년 8월 22일 화요일 오후 9:06:02
/*	Modifier	:	
/*	Update		:	2006년 8월 22일 화요일
/*	Comment		:	하나의 method를 command text만 변경하여 작업하기 위해 생성
/**********************************************************************************************************************/

using System;

namespace My.Database
{
	/// <summary>
	/// Update, Delete할 table 종류에 관한 enum
	/// </summary>
	public enum Operation
	{
		/// <summary>
		/// Insert 작업
		/// </summary>
		Insert,
		/// <summary>
		/// Update 작업
		/// </summary>
		Update,
		/// <summary>
		/// Delete 작업
		/// </summary>
		Delete,
	}
}
