/**********************************************************************************************************************/
/*	Name		:	My.Database.DataConnection
/*	Purpose		:	Data Connection을 위한 class 입니다.
/*	Creator		:	Kim Ki Won
/*	Create		:	2006년 8월 22일 화요일 오후 9:35:37
/*	Modifier	:	
/*	Update		:	
/*	Comment		:	
/**********************************************************************************************************************/

using System;
using System.Text;
using System.Data;
using System.Data.OleDb;

using My.Database;

namespace My.Database
{
	/// <summary>
	/// DataConnection을 위한 class입니다.
	/// </summary>
	public class DataConnection
	{
		#region Members
		private IDbConnection _connection;
		private string _connectionString;
		private string _userId;
		private string _password;
		private string _dataSource;
		#endregion

		#region Use Singleton Pattern
        
		private static DataConnection _newInstance;
        
		#region Constructor for Single Ton Pattern
        
		/// <summary>
		/// Constructor for Single Ton Pattern
		/// </summary>
		private DataConnection() 
		{
			_newInstance = null;

			if (_userId == null)
				_userId = ConstantDB.DEFAULT_USERID;

			if (_password == null)
				_password = ConstantDB.DEFAULT_PASSWORD;

			if (_dataSource == null)
				_dataSource = ConstantDB.DEFAULT_DATA_SOURCE;

			StringBuilder sb = new StringBuilder();

			sb.Append("Provider=OraOLEDB.Oracle; ");
			sb.Append("Unicode=True; ");
			sb.AppendFormat("User ID={0}; ", _userId);
			sb.AppendFormat("Password={0}; ", _password);
			sb.AppendFormat("Data Source={0}; ", _dataSource);

			_connectionString = sb.ToString();
		}
        
		/// <summary>
		/// Constructor for Single Ton Pattern
		/// </summary>
		/// <param name="userId">User ID(default : cmadm)</param>
		/// <param name="password">Password(default : cmadm)</param>
		/// <param name="dataSource">Data source(default : CJCMS_10-20)</param>
		public DataConnection(string userId, string password, string dataSource)
			: this()
		{
			_userId = userId;
			_password = password;
			_dataSource = dataSource;			
		}        
		#endregion 
        
		/// <summary>
		/// Create unique instance
		/// </summary>
		/// <returns></returns>
		public static DataConnection GetInstance()
		{
			if (_newInstance == null)
				_newInstance = new DataConnection();
        
			return _newInstance;
		}

		/// <summary>
		/// Create unique instance
		/// </summary>
		/// <returns></returns>
		/// <param name="userId">User ID(default : cmadm)</param>
		/// <param name="password">Password(default : cmadm)</param>
		/// <param name="dataSource">Data source(default : CJCMS_10-20)</param>
		public static DataConnection GetInstance(string userId, string password, string dataSource)
		{
			if (_newInstance == null)
				_newInstance = new DataConnection(userId, password, dataSource);
        
			return _newInstance;
		}
        
		#endregion
        
		#region Properties
		/// <summary>
		/// UserId를 가져옵니다.
		/// </summary>
		public string UserId
		{
			get
			{
				return _userId;
			}
		}
		
		/// <summary>
		/// Password를 가져옵니다.
		/// </summary>
		public string Password
		{
			get
			{
				return _password;
			}
		}
		
		/// <summary>
		/// DataSource를 가져옵니다.
		/// </summary>
		public string DataSource
		{
			get
			{
				return _dataSource;
			}
		}
		#endregion
		
		#region GetConncection
		/// <summary>
		/// Data Connection 정보를 구한다.
		/// </summary>
		/// <returns></returns>
		public IDbConnection GetConncection()
		{
			if (_connection == null)
				_connection = new OleDbConnection(_connectionString);

			return _connection;
		}
		#endregion
	}
}
