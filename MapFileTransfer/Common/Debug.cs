/**********************************************************************************************************************/
/*	Name		:	MapFileTransfer.Common.Log
/*	Purpose		:	MapFileTransfer의 log를 처리하는 class
/*	Creator		:	Kim Ki Won
/*	Create		:	2006년 8월 22일 화요일 오후 8:51:42
/*	Modifier	:	
/*	Update		:	2006년 8월 22일 화요일
/*	Comment		:	
/**********************************************************************************************************************/

using System;
using System.IO;

namespace MapFileTransfer.Common
{
	/// <summary>
	/// Debuging이나 Log에 필요한 정보를 생성하는 class 입니다.
	/// </summary>
	public class Log
	{
		/// <summary>
		/// 지정한 곳<seealso cref="ConstantBank.LOG_PATH"/>에 Log 기록을 남깁니다.
		/// </summary>
		/// <param name="text">Log text</param>
		public static void Write(string text)
		{
			try
			{
				string logPath = Path.GetDirectoryName(Constant.LOG_PATH);

				if (Directory.Exists(logPath) == false)
					Directory.CreateDirectory(logPath);

				FileStream fs = new FileStream(Constant.LOG_PATH, FileMode.Create | FileMode.Append);
				StreamWriter sw = new StreamWriter(fs, System.Text.Encoding.Default);

				sw.BaseStream.Seek(0, SeekOrigin.End);

				string logString = string.Format("[{0}] {1}", DateTime.Now.ToString(), text);
				sw.WriteLine(logString);

				sw.Flush();
				sw.Close();
			}
			catch (Exception ex)
			{
				Console.WriteLine(ex.Message);
			}
		}
	}
}
