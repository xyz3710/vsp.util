/**********************************************************************************************************************/
/*	Name		:	MapFileTransfer.Common.Constant
/*	Purpose		:	상수 목록을 위한 class
/*	Creator		:	Kim Ki Won
/*	Create		:	2006년 8월 22일 화요일 오후 9:36:06
/*	Modifier	:	
/*	Update		:	2006년 8월 22일 화요일
/*	Comment		:	
/**********************************************************************************************************************/

using System;

namespace MapFileTransfer.Common
{
	/// <summary>
	/// 상수 목록 class 입니다.
	/// </summary>
	public class Constant
	{
		public const string END_FILE_EXT = ".END";

		public const string LOCAL_PATH = @"D:\hicms-p.data\data\wafer";

		public const string TABLE_ORIGINAL = "CM_NOINK_LOT_WAFER";
		public const string TABLE_TEMPORARY = "TEMP_INKMAP_WAFER_DATA";
		public const string TABLE_VENDOR_FTP = "CM_VENDOR_FTP";

		public const string VENDOR_DOES_NOT_INPUTED = "Vendor를 먼저 지정해야 합니다.";

		public const string LOG_PATH = @"D:\hicms-p.data\data\wafer\Log\MapFileTransfer.log";
	}
}
