/**********************************************************************************************************************/
/*	Name		:	My.FtpClient.AsynchronousConnection
/*	Purpose		:	Ftp Client를 비연결 지향형 방식으로 연다
/*	Creator		:	Kim Ki Won
/*	Create		:	2006년 8월 16일 수요일 오후 7:34:05
/*	Modifier	:	
/*	Update		:	2006년 8월 16일 수요일
/*	Comment		:	
/**********************************************************************************************************************/

using System;
using System.Collections;
using System.Threading;
using System.IO;

namespace My.FtpClient
{
	/// <summary>
	/// Asynchronous connection FtpClient
	/// </summary>
	public class AsynchronousConnection : FtpBase
	{
		#region Current thread name prefix constant
		private const string THREAD_NAME_GET_FILE_PREFIX = "GetFileFromQueue";
		private const string THREAD_NAME_PUT_FILE_PREFIX = "PutFileFromQueue";
		private const string THREAD_NAME_DEL_FILE_PREFIX = "DeleteFileFromQueue";
		private const string THREAD_NAME_CD_PREFIX = "ChangeDirFromQueue";
		private const string THREAD_NAME_MD_PREFIX = "MakeDirFromQueue";
		private const string THREAD_NAME_RD_PREFIX = "RemoveDirFromQueue";
		#endregion

		#region Members
		private ArrayList _threadPool;
		private Queue _putFileTransfersQueue;
		private Queue _getFileTransfersQueue;
		private Queue _deleteFileQueue;
		private Queue _changeDirQueue;
		private Queue _makeDirQueue;
		private Queue _removeDirQueue;
		private System.Timers.Timer _timer;
		#endregion

		#region Constructor
		public AsynchronousConnection() 
			: base()
		{
			_threadPool = new ArrayList();
			_putFileTransfersQueue = new Queue();
			_getFileTransfersQueue = new Queue();
			_deleteFileQueue = new Queue();
			_changeDirQueue = new Queue();
			_makeDirQueue = new Queue();
			_removeDirQueue = new Queue();

			_timer = new System.Timers.Timer(100);
			_timer.Elapsed += new System.Timers.ElapsedEventHandler(manageThreads);
			_timer.Start();
		}
		#endregion

		#region Public Methods
		#region GetFile
		/// <summary>
		/// File을 가져온다.
		/// </summary>
		/// <param name="remoteFileName">가져올 remote filename</param>
		/// <param name="transferType">Transfer type</param>
		public new void GetFile(string remoteFileName, TransferType transferType)
		{
			GetFile(remoteFileName, Path.GetFileName(remoteFileName), transferType);
		}

		/// <summary>
		/// File을 가져온다.
		/// </summary>
		/// <param name="remoteFileName">가져올 remote filename</param>
		/// <param name="localFileName">저장하거나 변경할 local filename</param>
		/// <param name="transferType">Transfer type</param>
		public new void GetFile(string remoteFileName, string localFileName, TransferType transferType)
		{
			enqueueThread(createGetFileThread(remoteFileName, localFileName, transferType));
		}
		#endregion

		#region PutFile
		/// <summary>
		/// File을 전송합니다.
		/// </summary>
		/// <param name="localFileName">전송할 local filename</param>
		/// <param name="transferType">Transfer type</param>
		public new void PutFile(string localFileName, TransferType type)
		{
			PutFile(localFileName, Path.GetFileName(localFileName), type);
		}
		
		/// <summary>
		/// File을 전송합니다.
		/// </summary>
		/// <param name="localFileName">전송할 local filename</param>
		/// <param name="remoteFileName">변경하거나 저장할 remote filename</param>
		/// <param name="transferType">Transfer type</param>
		public new void PutFile(string localFileName, string remoteFileName, TransferType transferType)
		{
			enqueueThread(createPutFileThread(localFileName, remoteFileName, transferType));
		}
		#endregion

		#region DeleteFile
		/// <summary>
		/// Remote의 file을 삭제합니다.
		/// </summary>
		/// <param name="remoteFileName">삭제할 파일이름</param>
		public override void DeleteFile(string remoteFileName)
		{
			enqueueThread(createDeleteFileThread(remoteFileName));
		}
		#endregion

		#region ChangeDir
		/// <summary>
		/// 현재 directory 위치를 변경합니다.
		/// </summary>
		/// <param name="remotePath">변경할 remote path</param>
		public override void ChangeDir(string remotePath)
		{
			enqueueThread(createChangeDirThread(remotePath));
		}
		#endregion

		#region MakeDir
		/// <summary>
		/// 새로운 폴더를 생성합니다.
		/// </summary>
		/// <param name="folderName">생성할 폴더 이름</param>
		public override void MakeDir(string folderName)
		{
			enqueueThread(createMakeDirFromQueueThread(folderName));
		}
		#endregion

		#region RemoveDir
		/// <summary>
		/// 폴더를 제거합니다.
		/// </summary>
		/// <param name="folderName">제거할 폴더 이름</param>
		public override void RemoveDir(string folderName)
		{
			enqueueThread(createRemoveDirFromQueueThread(folderName));
		}
		#endregion

		#region Close
		/// <summary>
		/// Ftp 연결을 종료합니다.
		/// </summary>
		public override void Close()
		{
			waitAllThreads();

			base.Close();
		}
		#endregion
		#endregion

		#region Private Methods
		private Thread createGetFileThread(string remoteFileName, string localFileName, TransferType transferType)
		{
			FileTransferStruct ft = new FileTransferStruct();

			ft.LocalFileName = localFileName;
			ft.RemoteFileName = remoteFileName;
			ft.Type = transferType;
			
			_getFileTransfersQueue.Enqueue(ft);

			Thread thread = new Thread(new ThreadStart(getFileFromQueue));
			thread.Name = THREAD_NAME_GET_FILE_PREFIX + " " + remoteFileName + ", " + localFileName + ", " + transferType.ToString();;
			
			return thread;
		}

		private void getFileFromQueue()
		{
			FileTransferStruct ft = (FileTransferStruct)_getFileTransfersQueue.Dequeue();
			
			base.GetFile(ft.RemoteFileName, ft.LocalFileName, ft.Type);
		}

		private Thread createPutFileThread(string localFileName, string remoteFileName, TransferType transferType)
		{
			FileTransferStruct ft = new FileTransferStruct();
			
			ft.LocalFileName = localFileName;
			ft.RemoteFileName = remoteFileName;
			ft.Type = transferType;
			_putFileTransfersQueue.Enqueue(ft);

			Thread thread = new Thread(new ThreadStart(putFileFromQueue));
			thread.Name = THREAD_NAME_PUT_FILE_PREFIX + " " + localFileName + ", " + remoteFileName + ", " + transferType.ToString();;
			
			return thread;
		}

		private void putFileFromQueue()
		{
			FileTransferStruct ft = (FileTransferStruct)_putFileTransfersQueue.Dequeue();
			
			base.PutFile(ft.LocalFileName, ft.RemoteFileName, ft.Type);
		}

		private Thread createDeleteFileThread(string remoteFileName)
		{
			_deleteFileQueue.Enqueue(remoteFileName);

			Thread thread = new Thread(new ThreadStart(deleteFileFromQueue));
			thread.Name = THREAD_NAME_DEL_FILE_PREFIX +" " + remoteFileName;
			
			return thread;
		}
		
		private void deleteFileFromQueue()
		{
			base.DeleteFile((string)_deleteFileQueue.Dequeue());
		}

		private Thread createChangeDirThread(string remotePath)
		{
			_changeDirQueue.Enqueue(remotePath);

			Thread thread = new Thread(new ThreadStart(changeDirFromQueue));
			thread.Name = THREAD_NAME_CD_PREFIX +" " + remotePath;
			
			return thread;
		}

		private void changeDirFromQueue()
		{
			base.ChangeDir((string)_changeDirQueue.Dequeue());
		}

		private Thread createMakeDirFromQueueThread(string directoryName)
		{
			_makeDirQueue.Enqueue(directoryName);

			Thread thread = new Thread(new ThreadStart(makeDirFromQueue));
			thread.Name = THREAD_NAME_MD_PREFIX + " " + directoryName;
			
			return thread;
		}

		private void makeDirFromQueue()
		{
			base.MakeDir((string) _makeDirQueue.Dequeue());
		}
			
		private Thread createRemoveDirFromQueueThread(string directoryName)
		{
			_removeDirQueue.Enqueue(directoryName);

			Thread thread = new Thread(new ThreadStart(removeDirFromQueue));
			thread.Name = THREAD_NAME_RD_PREFIX + " " + directoryName;
			
			return thread;
		}

		private void removeDirFromQueue()
		{
			base.RemoveDir((string) _removeDirQueue.Dequeue());
		}

		private void manageThreads(Object state, System.Timers.ElapsedEventArgs e)
		{
			Thread thread;

			try
			{
				lockThreadPool();

				thread = peekThread();

				if (thread != null)
				{
					switch (thread.ThreadState)
					{
						case ThreadState.Unstarted:
							lockThreadPool();

							thread.Start();

							unlockThreadPool();

							break;
						case ThreadState.Stopped:
							lockThreadPool();

							dequeueThread();

							unlockThreadPool();

							break;
					}
				}

				unlockThreadPool();
			}
			catch
			{
				unlockThreadPool();
			}
		}
		
		private void waitAllThreads()
		{
			while(_threadPool.Count != 0)
			{
				Thread.Sleep(100);
			}
		}

		private void enqueueThread(Thread thread)
		{
			lockThreadPool();

			_threadPool.Add(thread);

			unlockThreadPool();
		}

		private Thread dequeueThread()
		{
			Thread thread;

			lockThreadPool();
			
			thread = (Thread)_threadPool[0];
			
			_threadPool.RemoveAt(0);
			
			unlockThreadPool();
			
			return thread;
		}

		private Thread peekThread()
		{
			Thread thread = null;

			lockThreadPool();
			
			if (_threadPool.Count > 0)
			{
				thread = (Thread)_threadPool[0];
			}
			
			unlockThreadPool();
			
			return thread;
		}

		private void lockThreadPool()
		{
			Monitor.Enter(_threadPool);
		}

		private void unlockThreadPool()
		{
			Monitor.Exit(_threadPool);
		}
		#endregion
	}

	#region Internal FileTransferStruct structure
	internal struct FileTransferStruct
	{
		public string RemoteFileName;
		public string LocalFileName;
		public TransferType Type;
	}
	#endregion
}
