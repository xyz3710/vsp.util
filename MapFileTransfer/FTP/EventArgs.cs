/**********************************************************************************************************************/
/*	Name		:	My.FtpClient.EventArgs
/*	Purpose		:	Event argument에 관련한 class
/*	Creator		:	Kim Ki Won
/*	Create		:	2006년 8월 22일 화요일 오후 8:47:48
/*	Modifier	:	
/*	Update		:	2006년 8월 22일 화요일
/*	Comment		:	
/**********************************************************************************************************************/

using System;
using System.Collections;
using System.Text;

namespace My.FtpClient
{
	#region OpenEventArgs
	/// <summary>
	/// Ftp가 연결될 때 필요한 정보를 담고 있는 Event arguement class
	/// </summary>
	public class OpenEventArgs : EventArgs
	{
		private string _remoteHost;
		private int _remotePort;
		private string _userId;
		private string _password;
		private ClientMode _clientMode;

		/// <summary>
		/// Ftp가 연결될 때 필요한 정보를 담고 있는 Event arguement 생성자
		/// </summary>
		/// <param name="remoteHost">Remote host</param>
		/// <param name="remotePort">Remote port</param>
		/// <param name="userId">User ID</param>
		/// <param name="password">Password</param>
		/// <param name="clientMode">Client mode</param>
		public OpenEventArgs(string remoteHost, int remotePort, string userId, string password, ClientMode clientMode)
		{
			_remoteHost = remoteHost;
			_remotePort = remotePort;
			_userId = userId;
			_password = password;
			_clientMode = clientMode;
		}

		/// <summary>
		/// Remote host를 구합니다.
		/// </summary>
		public string RemoteHost
		{
			get
			{
				return _remoteHost;
			}
		}

		/// <summary>
		/// Remote port 번호를 구합니다.
		/// </summary>
		public int RemotePort
		{
			get
			{
				return _remotePort;
			}
		}

		/// <summary>
		/// 접속한 User ID를 구합니다.
		/// </summary>
		public string UserId
		{
			get
			{
				return _userId;
			}
		}

		/// <summary>
		/// 접속한 User password를 구합니다.
		/// </summary>
		public string Password
		{
			get
			{
				return _password;
			}
		}

		/// <summary>
		/// 연결한 Clientmode를 구합니다.
		/// </summary>
		public ClientMode ClientMode
		{
			get
			{
				return _clientMode;
			}
		}            
	}        
	#endregion

	#region TransferFileEventArgs
	/// <summary>
	/// File put/get에 따른 정보를 담고 있는 Event arguement class
	/// </summary>
	public class TransferFileEventArgs : EventArgs
	{
		private string _localFileName;
		private string _remoteFileName;
		private TransferType _transferType;
		private long _totalStreamByte;
		private long _transferedStreamByte;
		private double _transferedRatio;

		/// <summary>
		/// File put/get에 따른 정보를 담고 있는 Event arguement 생성자
		/// </summary>
		/// <param name="localFileName">Local filename</param>
		/// <param name="remoteFileName">Remote filename</param>
		/// <param name="transferType">Transfer type</param>
		/// <param name="totalStreamByte">전체 전송할 stream byte</param>
		/// <param name="transferedStreamByte">현재 전송된 stream byte</param>
		public TransferFileEventArgs(string localFileName, string remoteFileName, TransferType transferType,
            long totalStreamByte, long transferedStreamByte)		
		{
			_localFileName = localFileName;
			_remoteFileName = remoteFileName;
			_transferType = transferType;
			_totalStreamByte = totalStreamByte;
			_transferedStreamByte = transferedStreamByte;
		}

		/// <summary>
		/// Local filename을 구합니다.
		/// </summary>
		public string LocalFileName
		{
			get
			{
				return _localFileName;
			}
		}

		/// <summary>
		/// Remote filename을 구합니다.
		/// </summary>
		public string RemoteFileName
		{
			get
			{
				return _remoteFileName;
			}
		}

		/// <summary>
		/// File TransferType을 구합니다.
		/// </summary>
		public TransferType TransferType
		{
			get
			{
				return _transferType;
			}
		}

		/// <summary>
		/// 전체 전송한 Stream의 길이를 byte 단위로 구합니다.
		/// </summary>
		public long TotalStreamByte
		{
			get
			{
				return _totalStreamByte;
			}
		}
            
		/// <summary>
		/// 현재 전송된 Stream의 길이를 byte 단위로 구합니다.
		/// </summary>
		public long TransferedStreamByte
		{
			get
			{
				return _transferedStreamByte;
			}
		}

		/// <summary>
		/// 전송된 Stream의 비율을 구합니다.(소수점 이하 2자리로 반올림 합니다.)
		/// </summary>
		public double TransferedRatio
		{
			get
			{
				if (_totalStreamByte != 0)
					_transferedRatio = Math.Round(((double)_transferedStreamByte / (double)_totalStreamByte) * 100, 2);

				return _transferedRatio;
			}
		}
            
	}
	#endregion

	#region DirInfoEventArgs
	/// <summary>
	/// Directory 관련 명령에 관한 정보를 담고 있는 Event arguement class
	/// </summary>
	public class DirInfoEventArgs : EventArgs
	{
		private string _foldername;

		/// <summary>
		/// Directory 관련 명령에 관한 정보를 담고 있는 Event arguement 생성자
		/// </summary>
		/// <param name="foldername">사용할 foldername</param>
		public DirInfoEventArgs(string foldername)
		{
			_foldername = foldername;
		}

		/// <summary>
		/// Directory 관련 명령을 수행할 Foldername을 구합니다.
		/// </summary>
		public string Foldername
		{
			get
			{
				return _foldername;
			}
		}
	}
	#endregion        

	#region FileInfoEventArgs
	/// <summary>
	/// File 관련 명령에 관한 정보를 담고 있는 Event arguement class
	/// </summary>
	public class FileInfoEventArgs : EventArgs
	{
		private string _fromFileName;
		private string _toFileName;

		/// <summary>
		/// File 관련 명령에 관한 정보를 담고 있는 Event arguement 생성자
		/// </summary>
		/// <param name="fromFileName">변경 전 filename</param>
		/// <param name="toFileName">변경 후 filename</param>
		public FileInfoEventArgs(string fromFileName, string toFileName)
		{
			_fromFileName = fromFileName;
			_toFileName = toFileName;
		}

		/// <summary>
		/// File 관련 명령을 수행할 변경전 filename을 구합니다.
		/// </summary>
		public string FromFileName
		{
			get
			{
				return _fromFileName;
			}
		}            

		/// <summary>
		/// File 관련 명령을 수행할 변경후 filename을 구합니다.
		/// <br>DelFile시 : 삭제한 foldername을 저장합니다.</br>
		/// <br>MoveFile시 : 이동할 File path를 저장합니다.</br>
		/// </summary>
		public string ToFileName
		{
			get
			{
				return _toFileName;
			}
		}            
	}
	#endregion
        
	/// <summary>
	/// Remote host에서 return하는 message 정보를 담고 있는 Event arguement class
	/// </summary>
	public class MessageUpEventArgs : EventArgs
	{
		private ArrayList _messages;

		/// <summary>
		/// Remote host에서 return하는 message 정보를 담고 있는 Event arguement class 생성자
		/// </summary>
		/// <param name="messages"></param>
		public MessageUpEventArgs(ArrayList messages)
		{
			_messages = messages;
		}

		/// <summary>
		/// Remote host에서 return하는 message 정보를 구합니다.
		/// </summary>
		public ArrayList Messages
		{
			get
			{
				return _messages;
			}
		}		

		/// <summary>
		/// Remote host에서 return하는 message 정보를 구합니다.
		/// <br>여러 Line이 있을 경우 <seealso cref="Environment.NewLine"/>으로 합쳐서 구합니다.</br>
		/// </summary>
		public string MessageList
		{
			get
			{
				string ret = string.Empty;
                
				if (_messages.Count > 1)
				{
					StringBuilder sb = new StringBuilder();
					
					for (int i = 0; i < _messages.Count - 1; i++)
					{
						sb.AppendFormat("{0}{1}", _messages[i], Environment.NewLine);
					}

					sb.Append(_messages[_messages.Count - 1]);

					ret = sb.ToString();					
				}
				else if (_messages.Count == 1)
				{
					ret = (string)_messages[0];
				}

				return ret;
			}
		}		
	}
        
}
