/**********************************************************************************************************************/
/*	Name		:	My.FtpClient.XDirColumns
/*	Purpose		:	XDir 명령에 filtering 하기 위한 enum
/*	Creator		:	Kim Ki Won
/*	Create		:	2006년 8월 22일 화요일 오후 8:48:27
/*	Modifier	:	
/*	Update		:	2006년 8월 22일 화요일
/*	Comment		:	
/**********************************************************************************************************************/

using System;

namespace My.FtpClient
{
	/// <summary>
	/// XDir에서 조회되는 파일 정보의 Column을 나열한다.
	/// </summary>
	public enum XDirColumns : int
	{
		/// <summary>
		/// File date column
		/// </summary>
		FileDate = 0,
		/// <summary>
		/// File time column
		/// </summary>
		FileTime,
		/// <summary>
		/// File size column
		/// </summary>
		FileSize,
		/// <summary>
		/// File name column
		/// </summary>
		FileName,
	}
}
