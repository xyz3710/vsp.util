/**********************************************************************************************************************/
/*	Name		:	My.FtpClient.TransferType enum
/*	Purpose		:	File 전송 형식을 지정합니다.
/*	Creator		:	Kim Ki Won
/*	Create		:	2006년 8월 16일 수요일 오후 3:51:53
/*	Modifier	:	
/*	Update		:	2006년 8월 16일 수요일
/*	Comment		:	
/**********************************************************************************************************************/

using System;

namespace My.FtpClient
{
	/// <summary>
	/// Ftp 내에 File 전송 형식을 지정합니다.
	/// </summary>
	public enum TransferType : int
	{
		/// <summary>
		/// Ascii type
		/// </summary>
		ASCII = 1,
		/// <summary>
		/// Binary type
		/// </summary>
		Binary = 2
	}
}
