/**********************************************************************************************************************/
/*	Name		:	My.FtpClient.ClientMode enum
/*	Purpose		:	Ftp Client의 연결 모드를 결정합니다.
/*	Creator		:	Kim Ki Won
/*	Create		:	2006년 8월 16일 수요일 오후 3:47:48
/*	Modifier	:	
/*	Update		:	2006년 8월 16일 수요일
/*	Comment		:	
/**********************************************************************************************************************/

using System;

namespace My.FtpClient
{
	/// <summary>
	/// Ftp Client의 연결 모드를 결정합니다.
	/// </summary>
	public enum ClientMode : int
	{
		/// <summary>
		/// Passive Mode
		/// </summary>
		Passive = 1,
		/// <summary>
		/// Active Mode
		/// </summary>
		Active = 2
	}
}
