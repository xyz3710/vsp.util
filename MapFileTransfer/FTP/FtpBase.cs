/**********************************************************************************************************************/
/*	Name		:	My.FtpClient.FtpBase
/*	Purpose		:	FtpClient의 Base Class
/*	Creator		:	Kim Ki Won
/*	Create		:	2006년 8월 16일 수요일 오후 7:04:45
/*	Modifier	:	
/*	Update		:	2006년 8월 16일 수요일
/*	Comment		:	Dir, XDir에 해당 폴더를 지정하여 바로 검색하지 않고 ChangeDir로 이동후 검색한다.(추후변경)
/**********************************************************************************************************************/

using System;
using System.Net;
using System.IO;
using System.Text;
using System.Net.Sockets;
using System.Collections;
using System.Data;
using System.Threading;

namespace My.FtpClient
{
	/// <summary>
	/// Ftp 연결과 관련된 기본 작업을 합니다.
	/// </summary>
	public class FtpBase
	{
		#region Constant
		private const int BLOCK_SIZE = 512;
		private const int DATA_PORT_RANGE_FROM = 1500;
		private const int DATA_PORT_RANGE_TO = 65000;
		private const int DEFAULT_REMOTE_PORT = 21;
		private const string DEFAULT_USER = "anonymous";
		private const string DEFAULT_PASSWORD = "anonymous@anonymous.com";
		private const string PATH_DELIMITER = "/";

		#region Command constant
		private const string COMMAND_STORE = "STOR";
		private const string COMMAND_NLST = "NLST";
		private const string COMMAND_LIST = "LIST";
		private const string COMMAND_RETRURN = "RETR";
		private const string COMMAND_DELETE = "DELE";
		private const string COMMAND_RENAMETO = "RNTO";
		private const string COMMAND_RENAMEFROM = "RNFR";
		private const string COMMAND_CWD = "CWD";
		private const string COMMAND_MAKEDIR = "MKD";
		private const string COMMAND_REMOVEDIR = "RMD";
		private const string COMMAND_PORT = "PORT";
		private const string COMMAND_PASV = "PASV";
		private const string COMMAND_USER = "USER";
		private const string COMMAND_PASS = "PASS";
		#endregion
		#endregion

		#region Members
		private TcpClient _tcpClient;
		private ClientMode _clientMode;
		private ArrayList _messageList;
		private int _activeConnectionsCount;
		private bool _isLogMessage;
		private string _remoteHost;
		private int _remotePort;
		private string _userId;
		private string _password;
		private bool _isOpenned;
		#endregion

		#region EventHandler
		/// <summary>
		/// Open/Close 이벤트 데이터가 없는 이벤트를 처리할 메서드를 나타냅니다.
		/// </summary>
		public delegate void OpenEventHandler(object sender, OpenEventArgs e);
		/// <summary>
		/// File 관련 이벤트 데이터가 없는 이벤트를 처리할 메서드를 나타냅니다.
		/// </summary>
		public delegate void TransferFileInfoEventHandler(object sender, TransferFileEventArgs e);
		/// <summary>
		/// Directory 관련 이벤트 데이터가 없는 이벤트를 처리할 메서드를 나타냅니다.
		/// </summary>
		public delegate void DirInfoEventHandler(object sender, DirInfoEventArgs e);
		/// <summary>
		/// File 관련 이벤트 데이터가 없는 이벤트를 처리할 메서드를 나타냅니다.
		/// </summary>
		public delegate void FileInfoEventHandler(object sender, FileInfoEventArgs e);
		/// <summary>
		/// MessageList에 값이 입력될 때 마다 이벤트 데이터가 없는 이벤트를 처리할 메서드를 나타냅니다.
		/// </summary>
		public delegate void MessageUpEventHandler(object sender, MessageUpEventArgs e);
		#endregion

		#region Event
		/// <summary>
		/// Ftp 연결하기 전에 발생합니다.
		/// </summary>
		public event OpenEventHandler OnOpenning;
		/// <summary>
		/// Ftp 연결이 완료되고 UserId, password등이 전달되어 접속 된 후 발생합니다.
		/// </summary>
		public event OpenEventHandler OnOpenned;
		/// <summary>
		/// Ftp 연결이 종료되기 전에 발생합니다.
		/// </summary>
		public event OpenEventHandler OnClosing;
		/// <summary>
		/// Ftp 연결이 종료된 후 발생합니다.
		/// </summary>
		public event OpenEventHandler OnClosed;
		/// <summary>
		/// Get 명령을 실행하기 전에 발생합니다.
		/// </summary>
		public event TransferFileInfoEventHandler OnBeginGetFile;
		/// <summary>
		/// Get 작업 도중 block 단위 전송을 할 때 마다 발생합니다.
		/// </summary>
		public event TransferFileInfoEventHandler OnTransferingGetFile;
		/// <summary>
		/// Get 작업이 성공적으로 완료되면 발생합니다.
		/// </summary>
		public event TransferFileInfoEventHandler OnSuccessGetFile;
		/// <summary>
		/// Get 작업이 실패할 경우 발생합니다.
		/// </summary>
		public event TransferFileInfoEventHandler OnFailureGetFile;
		/// <summary>
		/// Get 명령이 완료된 후 발생합니다.
		/// </summary>
		public event TransferFileInfoEventHandler OnEndGetFile;
		/// <summary>
		/// Put 명령을 실행하기 전에 발생합니다.
		/// </summary>
		public event TransferFileInfoEventHandler OnBeginPutFile;
		/// <summary>
		/// Put 작업 도중 block 단위 전송을 할 때 마다 발생합니다.
		/// </summary>
		public event TransferFileInfoEventHandler OnTransferingPutFile;
		/// <summary>
		/// Put 작업이 성공적으로 완료되면 발생합니다.
		/// </summary>
		public event TransferFileInfoEventHandler OnSuccessPutFile;
		/// <summary>
		/// Put 작업이 실패할 경우 발생합니다.
		/// </summary>
		public event TransferFileInfoEventHandler OnFailurePutFile;
		/// <summary>
		/// Put 명령이 완료된 후 발생합니다.
		/// </summary>
		public event TransferFileInfoEventHandler OnEndPutFile;
		/// <summary>
		/// Directory를 변경하기 전에 발생합니다.
		/// </summary>
		public event DirInfoEventHandler OnBeginChangeDir;
		/// <summary>
		/// Directory를 변경한 후에 발생합니다.
		/// </summary>
		public event DirInfoEventHandler OnEndChangeDir;
		/// <summary>
		/// Directory를 만들기 전에 발생합니다.
		/// </summary>
		public event DirInfoEventHandler OnBeginMakeDir;
		/// <summary>
		/// Directory를 만들고 난 후에 발생합니다.
		/// </summary>
		public event DirInfoEventHandler OnEndMakeDir;
		/// <summary>
		/// Directory를 제거하기 전에 발생합니다.
		/// </summary>
		public event DirInfoEventHandler OnBeginRemoveDir;
		/// <summary>
		/// Directory를 제거한 후에 발생합니다.
		/// </summary>
		public event DirInfoEventHandler OnEndRemoveDir;
		/// <summary>
		/// File을 삭제하기 전에 발생합니다.
		/// </summary>
		public event FileInfoEventHandler OnBeginDelFile;
		/// <summary>
		/// File을 삭제한 후에 발생합니다.
		/// </summary>
		public event FileInfoEventHandler OnEndDelFile;
		/// <summary>
		/// File을 이동하기 전에 발생합니다.
		/// </summary>
		public event FileInfoEventHandler OnBeginMoveFile;
		/// <summary>
		/// File을 이동한 후에 발생합니다.
		/// </summary>
		public event FileInfoEventHandler OnEndMoveFile;
		/// <summary>
		/// File 이름을 변경하기 전에 발생합니다.
		/// </summary>
		public event FileInfoEventHandler OnBeginRenameFile;
		/// <summary>
		/// File 이름을 변경한 후에 발생합니다.
		/// </summary>
		public event FileInfoEventHandler OnEndRenameFile;
		/// <summary>
		/// Message가 발생할 때 마다 발생합니다.
		/// </summary>
		public event MessageUpEventHandler OnMessageUp;
		#endregion

		#region Constructor
		/// <summary>
		/// Ftp Client이 base instance를 생성합니다.
		/// </summary>
		public FtpBase()
		{
			_messageList = new ArrayList();
			_clientMode = ClientMode.Active;
			_activeConnectionsCount = 0;
			_isLogMessage = false;
			_remoteHost = string.Empty;
			_remotePort = DEFAULT_REMOTE_PORT;
			_userId = DEFAULT_USER;
			_password = DEFAULT_PASSWORD;
			_isOpenned = false;
		}
		#endregion

		#region Properties
		#region RemoteHost
		/// <summary>
		/// RemoteHost 정보를 구합니다.
		/// </summary>
		public string RemoteHost
		{
			get
			{
				return _remoteHost;
			}
		}
		#endregion

		#region RemotePort
		/// <summary>
		/// Remote port 정보를 구합니다.
		/// </summary>
		public int RemotePort
		{
			get
			{
				return _remotePort;
			}
		}
		#endregion

		#region UserID
		/// <summary>
		/// User ID를 구합니다.
		/// </summary>
		public string UserID
		{
			get
			{
				return _userId;
			}
		}
		#endregion

		#region Password
		/// <summary>
		/// Password를 구합니다.
		/// </summary>
		public string Password
		{
			get
			{
				return _password;
			}
		}
		#endregion

		#region MessageList
		/// <summary>
		/// MessageList를 구합니다.
		/// </summary>
		public ArrayList MessageList
		{
			get
			{
				return _messageList;
			}
		}
		#endregion

		#region IsLogMessage
		/// <summary>
		/// IsLogMessage 남길것인지를 설정합니다.
		/// </summary>
		public bool IsLogMessage
		{
			get
			{
				return _isLogMessage;
			}

			set
			{
				if (value == true)
				{
					if (_messageList == null)
						_messageList = new ArrayList();
				}

				_isLogMessage = value;
			}
		}

		#region IsOpenned
		/// <summary>
		/// RemoteHost와 연결되었는지를 구합니다.
		/// </summary>
		public bool IsOpenned
		{
			get
			{
				return _isOpenned;
			}
		}
		#endregion

		#endregion
		#endregion

		#region Public Methods
		#region Open
		/// <summary>
		/// Ftp Client를 Open합니다.
		/// </summary>
		/// <param name="remoteHost">Remote host의 이름이나 IP address</param>
		public virtual void Open(string remoteHost)
		{
			Open(remoteHost, DEFAULT_REMOTE_PORT, DEFAULT_USER, DEFAULT_PASSWORD, ClientMode.Active);
		}

		/// <summary>
		/// Ftp Client를 Open합니다.
		/// </summary>
		/// <param name="remoteHost">Remote host의 이름이나 IP address</param>
		/// <param name="userId">User ID(default : anonymous)</param>
		/// <param name="password">Password(default : anonymous@anonymous.com)</param>
		public virtual void Open(string remoteHost, string userId, string password)
		{
			Open(remoteHost, DEFAULT_REMOTE_PORT, userId, password, ClientMode.Active);
		}

		/// <summary>
		/// Ftp Client를 Open합니다.
		/// </summary>
		/// <param name="remoteHost">Remote host의 이름이나 IP address</param>
		/// <param name="userId">User ID(default : anonymous)</param>
		/// <param name="password">Password(default : anonymous@anonymous.com)</param>
		/// <param name="clientMode">Client open mode(default : Active)</param>
		public virtual void Open(string remoteHost, string userId, string password, ClientMode mode)
		{
			Open(remoteHost, DEFAULT_REMOTE_PORT, userId, password, mode);
		}

		/// <summary>
		/// Ftp Client를 Open합니다.
		/// </summary>
		/// <param name="remoteHost">Remote host의 이름이나 IP address</param>
		/// <param name="remotePort">Remote port(default : 21)</param>
		/// <param name="userId">User ID(default : anonymous)</param>
		/// <param name="password">Password(default : anonymous@anonymous.com)</param>
		public virtual void Open(string remoteHost, int remotePort, string userId, string password)
		{
			Open(remoteHost, remotePort, userId, password, ClientMode.Active);
		}

		/// <summary>
		/// Ftp Client를 Open합니다.
		/// </summary>
		/// <param name="remoteHost">Remote host의 이름이나 IP address</param>
		/// <param name="remotePort">Remote port(default : 21)</param>
		/// <param name="userId">User ID(default : anonymous)</param>
		/// <param name="password">Password(default : anonymous@anonymous.com)</param>
		/// <param name="clientMode">Client open mode(default : Active)</param>
		public virtual void Open(string remoteHost, int remotePort, string userId, string password, ClientMode clientMode)
		{
			ArrayList tempMessageList = new ArrayList();
			int returnValue;

			_tcpClient = new TcpClient();
			_clientMode = clientMode;
			_remoteHost = remoteHost;
			_remotePort = RemotePort;
			_userId = userId;
			_password = password;

			// As we cannot detect the local address from the TCPClient class, convert "127.0.0.1" and "localhost" to
			// the DNS record of this machine; this will ensure that the connection address and the PORT command address
			// are identical.  This fixes bug 854919.
			if (remoteHost == "localhost" || remoteHost == "127.0.0.1")
			{
				remoteHost = getLocalAddressList()[0].ToString();
			}

			//CONNECT
			try
			{
				if (OnOpenning != null)
					OnOpenning(this, new OpenEventArgs(_remoteHost, _remotePort, _userId, _password, _clientMode));

				_tcpClient.Connect(remoteHost, remotePort);

				tempMessageList = read();
				returnValue = getMessageReturnValue((string)tempMessageList[0]);

				if (returnValue != 220)
				{
					Close();

					throw new Exception((string)tempMessageList[0]);
				}

				//SEND USER
				tempMessageList = SendCommand(COMMAND_USER + " " + userId);
				returnValue = getMessageReturnValue((string)tempMessageList[0]);

				if (!(returnValue == 331 || returnValue == 202))
				{
					Close();

					throw new Exception((string)tempMessageList[0]);
				}

				//SEND PASSWORD
				if (returnValue == 331)
				{
					tempMessageList = SendCommand(COMMAND_PASS + " " + password);
					returnValue = getMessageReturnValue((string)tempMessageList[0]);

					if (!(returnValue == 230 || returnValue == 202))
					{
						Close();

						throw new Exception((string)tempMessageList[0]);
					}
				}

				if (OnOpenned != null)
					OnOpenned(this, new OpenEventArgs(_remoteHost, _remotePort, _userId, _password, _clientMode));

				_isOpenned = true;
			}
			catch (SocketException ex)
			{
				addMessageExceptLastMessage(ex);

				throw new IOException("Could not connect to remote server");
			}
			catch (Exception ex)
			{
				addMessageExceptLastMessage(ex);

				throw new Exception(ex.Message);
			}
		}
		#endregion

		#region Close
		/// <summary>
		/// Ftp Client를 Closing합니다.
		/// </summary>
		public virtual void Close()
		{
			if (OnClosing != null)
				OnClosing(this, new OpenEventArgs(_remoteHost, _remotePort, _userId, _password, _clientMode));

			ArrayList messageList = new ArrayList();

			if (_tcpClient != null)
			{
				messageList = SendCommand("QUIT");

				_tcpClient.Close();
			}

			if (OnClosed != null)
				OnClosed(this, new OpenEventArgs(_remoteHost, _remotePort, _userId, _password, _clientMode));

			_isOpenned = false;
		}
		#endregion

		#region Dir
		/// <summary>
		/// 단순 파일 목록을 구해옵니다.
		/// </summary>
		/// <param name="mask">파일 목록 mask</param>
		/// <returns></returns>
		public ArrayList Dir(string mask)
		{
			ArrayList tmpList = Dir();

			DataTable table = new DataTable();

			table.Columns.Add("Name");

			for (int i = 0; i < tmpList.Count; i++)
			{
				DataRow row = table.NewRow();

				row["Name"] = (string)tmpList[i];
				table.Rows.Add(row);
			}

			DataRow[] rowList = table.Select("Name LIKE '" + mask + "'", "", DataViewRowState.CurrentRows);
			tmpList = new ArrayList();

			for (int i = 0; i < rowList.Length; i++)
			{
				tmpList.Add((string)rowList[i]["Name"]);
			}

			return tmpList;
		}

		/// <summary>
		/// 단순 파일 목록을 구해옵니다.
		/// </summary>
		/// <returns></returns>
		public ArrayList Dir()
		{
			lockTcpClient();

			TcpListener listner = null;
			TcpClient client = null;
			NetworkStream networkStream = null;
			ArrayList tempMessageList = new ArrayList();
			int returnValue = 0;
			string returnValueMessage = "";
			ArrayList fileList = new ArrayList();

			try
			{
				setTransferType(TransferType.ASCII);

				if (_clientMode == ClientMode.Active)
				{
					listner = createDataListner();
					listner.Start();
				}
				else
				{
					client = createDataClient();
				}

				tempMessageList = new ArrayList();
				tempMessageList = SendCommand(COMMAND_NLST);
				returnValue = getMessageReturnValue((string)tempMessageList[0]);

				if (!(returnValue == 150 || returnValue == 125 || returnValue == 550))
				{
					throw new Exception((string)tempMessageList[0]);
				}

				if (returnValue == 550) //No files found
				{
					return fileList;
				}

				if (_clientMode == ClientMode.Active)
				{
					client = listner.AcceptTcpClient();
				}

				try
				{
					networkStream = client.GetStream();

					fileList = readLines(networkStream);

					if (tempMessageList.Count == 1)
					{
						tempMessageList = read();
						returnValue = getMessageReturnValue((string)tempMessageList[0]);
						returnValueMessage = (string)tempMessageList[0];
					}
					else
					{
						returnValue = getMessageReturnValue((string)tempMessageList[1]);
						returnValueMessage = (string)tempMessageList[1];
					}

					if (returnValue != 226)
					{
						throw new Exception(returnValueMessage);
					}
				}
				catch (Exception ex)
				{
					addMessageExceptLastMessage(ex);
				}
				finally
				{
					networkStream.Close();
					client.Close();

					if (_clientMode == ClientMode.Active)
					{
						listner.Stop();
					}
				}
			}
			catch (Exception ex)
			{
				addMessageExceptLastMessage(ex);
			}
			finally
			{
				unlockTcpClient();
			}

			return fileList;
		}
		#endregion

		#region XDir
		/// <summary>
		/// 파일 목록을 날짜, 시간, 크기, 이름 순으로 구해옵니다.
		/// </summary>
		/// <param name="mask">mask 할 filename/file path 또는 기타 정보</param>
		/// <param name="maskedColumn">mask되어 보여질 컬럼</param>
		/// <returns></returns>
		public ArrayList XDir(string mask, XDirColumns maskedColumn)
		{
			string strPath = string.Empty;
			string strFileName = string.Empty;

			if (maskedColumn == XDirColumns.FileName)
			{
				strPath = Path.GetDirectoryName(mask);
				strFileName = Path.GetFileName(mask);

				if (strPath != string.Empty)
				{
					ChangeDir(strPath, false);
					mask = strFileName;
				}
			}

			ArrayList tmpList = XDir();
			DataTable table = new DataTable();

			table.Columns.Add("Name");

			for (int i = 0; i < tmpList.Count; i++)
			{
				DataRow row = table.NewRow();

				row["Name"] = (string)((ArrayList)tmpList[i])[(int)maskedColumn];
				table.Rows.Add(row);
			}

			DataRow[] rowList = table.Select("Name LIKE '" + mask + "'", "", DataViewRowState.CurrentRows);
			ArrayList newList = new ArrayList();

			for (int i = 0; i < rowList.Length; i++)
			{
				for (int j = 0; j < tmpList.Count; j++)
				{
					if ((string)rowList[i]["Name"] == (string)((ArrayList)tmpList[j])[(int)maskedColumn])
					{
						newList.Add((ArrayList)tmpList[j]);
					}
				}
			}

			return newList;
		}

		/// <summary>
		/// 파일 목록을 날짜, 시간, 크기, 이름 순으로 구해옵니다.
		/// </summary>
		/// <returns>2차원 ArrayList : 1차 -> 목록, 2차 -> 날짜, 시간, 크기, 이름</returns>
		public ArrayList XDir()
		{
			lockTcpClient();

			TcpListener listner = null;
			TcpClient client = null;
			NetworkStream networkStream = null;
			ArrayList tempMessageList = new ArrayList();
			int returnValue = 0;
			string returnValueMessage = string.Empty;
			ArrayList filesAndFolderList = new ArrayList();
			ArrayList tmpFilesAndFolderList = new ArrayList();

			try
			{
				setTransferType(TransferType.ASCII);

				if (_clientMode == ClientMode.Active)
				{
					listner = createDataListner();
					listner.Start();
				}
				else
				{
					client = createDataClient();
				}

				tempMessageList = new ArrayList();
				tempMessageList = SendCommand(COMMAND_LIST);
				returnValue = getMessageReturnValue((string)tempMessageList[0]);

				if (!(returnValue == 150 || returnValue == 125 || returnValue == 550))
				{
					throw new Exception((string)tempMessageList[0]);
				}

				if (returnValue == 550) //No files found
				{
					return filesAndFolderList;
				}

				if (_clientMode == ClientMode.Active)
				{
					client = listner.AcceptTcpClient();
				}

				try
				{
					networkStream = client.GetStream();

					tmpFilesAndFolderList = readLines(networkStream);

					if (tempMessageList.Count == 1)
					{
						tempMessageList = read();
						returnValue = getMessageReturnValue((string)tempMessageList[0]);
						returnValueMessage = (string)tempMessageList[0];
					}
					else
					{
						returnValue = getMessageReturnValue((string)tempMessageList[1]);
						returnValueMessage = (string)tempMessageList[1];
					}

					if (returnValue != 226)
					{
						throw new Exception(returnValueMessage);
					}
				}
				catch (Exception ex)
				{
					addMessageExceptLastMessage(ex);
				}
				finally
				{
					networkStream.Close();
					client.Close();

					if (_clientMode == ClientMode.Active)
					{
						listner.Stop();
					}
				}
			}
			catch (Exception ex)
			{
				addMessageExceptLastMessage(ex);
			}
			finally
			{
				unlockTcpClient();

				foreach (string str in tmpFilesAndFolderList)
				{
					filesAndFolderList.Add(getTokens(str, " "));
				}
			}

			return filesAndFolderList;
		}
		#endregion

		#region GetFile
		/// <summary>
		/// File을 가져옵니다.
		/// </summary>
		/// <param name="remoteFileName">가져올 remote filename</param>
		/// <param name="transferType">Transfer type</param>
		protected virtual void GetFile(string remoteFileName, TransferType transferType)
		{
			GetFile(remoteFileName, Path.GetFileName(remoteFileName), transferType);
		}

		/// <summary>
		/// File을 가져옵니다.
		/// </summary>
		/// <param name="remoteFileName">가져올 remote filename</param>
		/// <param name="localFileName">저장하거나 변경할 local filename</param>
		/// <param name="transferType">Transfer type</param>
		protected virtual void GetFile(string remoteFileName, string localFileName, TransferType transferType)
		{
			if (OnBeginGetFile != null)
				OnBeginGetFile(this, new TransferFileEventArgs(localFileName, remoteFileName, transferType, 0, 0));

			long totalBytes = 0;

			// XDir로 file 목록을 읽어와서 remotefile의 크기를 구한다.
			try
			{
				string size = (string)((ArrayList)XDir(remoteFileName, XDirColumns.FileName)[0])[(int)XDirColumns.FileSize];

				totalBytes = long.Parse(size);
			}
			catch (Exception)
			{
			}

			FileStream fsLocalFile = null;
			long receiveBytes = 0;
			bool isSuccess = false;

			try
			{
				fsLocalFile = new FileStream(localFileName, FileMode.Create);

				receiveBytes = GetStream(remoteFileName, fsLocalFile, transferType, totalBytes);

				if (receiveBytes == totalBytes)
				{
					if (OnSuccessGetFile != null)
						OnSuccessGetFile(this,
							new TransferFileEventArgs(localFileName, remoteFileName, transferType, totalBytes, receiveBytes));

					isSuccess = true;
				}
				else
				{
					if (OnFailureGetFile != null)
						OnFailureGetFile(this,
							new TransferFileEventArgs(localFileName, remoteFileName, transferType, totalBytes, receiveBytes));
				}
			}
			catch (Exception ex)
			{
				if (OnFailureGetFile != null)
					OnFailureGetFile(this,
						new TransferFileEventArgs(localFileName, remoteFileName, transferType, totalBytes, receiveBytes));

				addMessageExceptLastMessage(ex);
			}
			finally
			{
				fsLocalFile.Close();
			}

			if (isSuccess == true && OnEndGetFile != null)
				OnEndGetFile(this,
					new TransferFileEventArgs(localFileName, remoteFileName, transferType, totalBytes, receiveBytes));
		}
		#endregion

		#region GetStream
		/// <summary>
		/// Stream 으로 파일을 읽어옵니다.
		/// </summary>
		/// <param name="remoteFileName">Remote filename</param>
		/// <param name="stream">읽고자 하는 Stream</param>
		/// <param name="transferType">Transfer type</param>
		/// <param name="totalBytes">remoteFile의 전체 길이(<seealso cref="XDir"/>로 구할 수 있다.</param>
		public long GetStream(string remoteFileName, Stream stream, TransferType transferType, long totalBytes)
		{
			lockTcpClient();

			long readBytes = 0;

			try
			{
				TcpListener listner = null;
				TcpClient client = null;
				NetworkStream networkStream = null;
				ArrayList tempMessageList = new ArrayList();
				int returnValue = 0;
				string returnValueMessage = string.Empty;

				setTransferType(transferType);

				if (_clientMode == ClientMode.Active)
				{
					listner = createDataListner();
					listner.Start();
				}
				else
				{
					client = createDataClient();
				}

				tempMessageList = new ArrayList();
				tempMessageList = SendCommand(COMMAND_RETRURN + " " + remoteFileName);
				returnValue = getMessageReturnValue((string)tempMessageList[0]);

				if (!(returnValue == 150 || returnValue == 125))
				{
					throw new Exception((string)tempMessageList[0]);
				}

				if (_clientMode == ClientMode.Active)
				{
					client = listner.AcceptTcpClient();
				}

				networkStream = client.GetStream();

				Byte[] buffer = new Byte[BLOCK_SIZE];
				int bytes = 0;
				bool read = true;

				try
				{
					while (read == true)
					{
						if (OnTransferingGetFile != null)
							OnTransferingGetFile(this,
								new TransferFileEventArgs(remoteFileName, remoteFileName, transferType, totalBytes, readBytes));

						bytes = (int)networkStream.Read(buffer, 0, buffer.Length);

						readBytes += bytes;

						stream.Write(buffer, 0, bytes);

						if (bytes == 0)
						{
							read = false;
						}
					}
				}
				catch (Exception ex)
				{
					addMessageExceptLastMessage(ex);
				}
				finally
				{
					networkStream.Close();
					client.Close();

					if (_clientMode == ClientMode.Active)
					{
						listner.Stop();
					}

					if (tempMessageList.Count == 1)
					{
						tempMessageList = this.read();
						returnValue = getMessageReturnValue((string)tempMessageList[0]);
						returnValueMessage = (string)tempMessageList[0];
					}
					else
					{
						returnValue = getMessageReturnValue((string)tempMessageList[1]);
						returnValueMessage = (string)tempMessageList[1];
					}

					if (returnValue != 226)
					{
						throw new Exception(returnValueMessage);
					}
				}
			}
			catch (Exception ex)
			{
				addMessageExceptLastMessage(ex);
			}
			finally
			{
				unlockTcpClient();
			}

			return readBytes;
		}
		#endregion

		#region PutFile
		/// <summary>
		/// File을 전송합니다.
		/// </summary>
		/// <param name="localFileName">전송할 local filename</param>
		/// <param name="transferType">Transfer type</param>
		public virtual void PutFile(string localFileName, TransferType transferType)
		{
			PutFile(localFileName, Path.GetFileName(localFileName), transferType);
		}

		/// <summary>
		/// File을 전송합니다.
		/// </summary>
		/// <param name="localFileName">전송할 local filename</param>
		/// <param name="remoteFileName">변경하거나 저장할 remote filename</param>
		/// <param name="transferType">Transfer type</param>
		public virtual void PutFile(string localFileName, string remoteFileName, TransferType transferType)
		{
			if (OnBeginPutFile != null)
				OnBeginPutFile(this, new TransferFileEventArgs(localFileName, remoteFileName, transferType, 0, 0));

			FileStream fsLocalFile = null;
			long sendBytes = 0;
			long totalBytes = 0;
			bool isSuccess = false;

			try
			{
				fsLocalFile = new FileStream(localFileName, FileMode.Open);

				totalBytes = fsLocalFile.Length;
				sendBytes = SendStream(remoteFileName, fsLocalFile, transferType);

				if (sendBytes == fsLocalFile.Length)
				{
					if (OnSuccessPutFile != null)
						OnSuccessPutFile(this,
							new TransferFileEventArgs(localFileName, remoteFileName, transferType, totalBytes, sendBytes));

					isSuccess = true;
				}
				else
				{
					if (OnFailurePutFile != null)
						OnFailurePutFile(this,
							new TransferFileEventArgs(localFileName, remoteFileName, transferType, totalBytes, sendBytes));
				}
			}
			catch (Exception ex)
			{
				if (OnFailurePutFile != null)
					OnFailurePutFile(this,
						new TransferFileEventArgs(localFileName, remoteFileName, transferType, totalBytes, sendBytes));

				addMessageExceptLastMessage(ex);
			}
			finally
			{
				if (fsLocalFile != null)
					fsLocalFile.Close();
			}

			if (isSuccess == true && OnEndPutFile != null)
				OnEndPutFile(this,
					new TransferFileEventArgs(localFileName, remoteFileName, transferType, totalBytes, sendBytes));
		}
		#endregion

		#region SendStream
		/// <summary>
		/// Stream 으로 파일을 저장 합니다.
		/// </summary>
		/// <param name="remoteFileName">Remote filename</param>
		/// <param name="stream">보내고자 하는 Stream</param>
		/// <param name="transferType">Transfer type</param>
		/// <returns>실제 전송된 stream bytes</returns>
		public long SendStream(string remoteFileName, Stream stream, TransferType transferType)
		{
			lockTcpClient();

			long sendBytes = 0;

			try
			{
				TcpListener listner = null;
				TcpClient client = null;
				NetworkStream networkStream = null;
				ArrayList tempMessageList = new ArrayList();
				int returnValue = 0;
				string returnValueMessage = "";
				tempMessageList = new ArrayList();

				setTransferType(transferType);

				if (_clientMode == ClientMode.Active)
				{
					listner = createDataListner();
					listner.Start();
				}
				else
				{
					client = createDataClient();
				}

				tempMessageList = SendCommand(COMMAND_STORE + " " + remoteFileName);
				returnValue = getMessageReturnValue((string)tempMessageList[0]);

				if (!(returnValue == 150 || returnValue == 125))
				{
					throw new Exception((string)tempMessageList[0]);
				}

				if (_clientMode == ClientMode.Active)
				{
					client = listner.AcceptTcpClient();
				}

				networkStream = client.GetStream();
				Byte[] buffer = new Byte[BLOCK_SIZE];
				int bytes = 0;

				try
				{
					while (sendBytes < stream.Length)
					{
						bytes = (int)stream.Read(buffer, 0, BLOCK_SIZE);

						sendBytes += bytes;

						networkStream.Write(buffer, 0, bytes);

						if (OnTransferingPutFile != null)
							OnTransferingPutFile(this,
								new TransferFileEventArgs(remoteFileName, remoteFileName, transferType, stream.Length, sendBytes));
					}
				}
				catch (Exception ex)
				{
					addMessageExceptLastMessage(ex);
				}
				finally
				{
					networkStream.Close();
					client.Close();

					if (_clientMode == ClientMode.Active)
					{
						listner.Stop();
					}

					if (tempMessageList.Count == 1)
					{
						tempMessageList = read();
						returnValue = getMessageReturnValue((string)tempMessageList[0]);
						returnValueMessage = (string)tempMessageList[0];
					}
					else
					{
						returnValue = getMessageReturnValue((string)tempMessageList[1]);
						returnValueMessage = (string)tempMessageList[1];
					}

					if (returnValue != 226)
					{
						throw new Exception(returnValueMessage);
					}
				}
			}
			catch (Exception ex)
			{
				addMessageExceptLastMessage(ex);
			}
			finally
			{
				unlockTcpClient();
			}

			return sendBytes;
		}
		#endregion

		#region DeleteFile
		/// <summary>
		/// Remote의 file을 삭제합니다.
		/// </summary>
		/// <param name="remoteFileName">삭제할 파일이름</param>
		public virtual void DeleteFile(string remoteFileName)
		{
			string remotePath = Path.GetDirectoryName(remoteFileName);

			if (OnBeginDelFile != null)
				OnBeginDelFile(this, new FileInfoEventArgs(remoteFileName, remotePath));

			bool isSuccess = false;

			lockTcpClient();

			try
			{
				ArrayList tempMessageList = new ArrayList();
				int returnValue = 0;

				tempMessageList = SendCommand(COMMAND_DELETE + " " + remoteFileName);
				returnValue = getMessageReturnValue((string)tempMessageList[0]);

				if (returnValue != 250)
				{
					throw new Exception((string)tempMessageList[0]);
				}

				isSuccess = true;
			}
			catch (Exception ex)
			{
				addMessageExceptLastMessage(ex);
			}
			finally
			{
				unlockTcpClient();
			}

			if (isSuccess == true && OnEndDelFile != null)
				OnEndDelFile(this, new FileInfoEventArgs(remoteFileName, remotePath));
		}
		#endregion

		#region MoveFile
		/// <summary>
		/// Remote의 file을 이동시킨다.
		/// </summary>
		/// <param name="fromRemoteFilePath">이동할 remote [path + ]filename</param>
		/// <param name="toRemoteFilePath">이동시킬 remote [path + ]filename</param>
		public virtual void MoveFile(string fromRemoteFilePath, string toRemoteFilePath)
		{
			if (OnBeginMoveFile != null)
				OnBeginMoveFile(this, new FileInfoEventArgs(fromRemoteFilePath, toRemoteFilePath));

			bool isSuccess = false;
			string fromFileName = Path.GetFileName(fromRemoteFilePath);
			string toFileName = Path.GetFileName(toRemoteFilePath);
			string toFilePath = Path.GetDirectoryName(toRemoteFilePath);

			// path와 filename을 분리시킨것이 같을 경우 filename이 없고 path만 포함된 경우이다.
			if (toFileName == toFilePath)
			{
				toFileName = fromFileName;
			}

			fromRemoteFilePath.Replace(@"\", "/");
			toRemoteFilePath.Replace(@"\", "/");

			isSuccess = RenameFile(fromRemoteFilePath, toFilePath + PATH_DELIMITER + toFileName, false);

			if (isSuccess == true && OnEndMoveFile != null)
				OnEndMoveFile(this, new FileInfoEventArgs(fromRemoteFilePath, toRemoteFilePath));
		}
		#endregion

		#region RenameFile
		/// <summary>
		/// Remote filename/folder name을 변경합니다.
		/// </summary>
		/// <remarks>이름을 변경할 권한이 있어야 합니다.(guest, anoymous는 통상적으로 변경 안됨)</remarks>
		/// <param name="fromRemoteFileName">변경할 remote filename/folder name</param>
		/// <param name="toRemoteFileName">변경될 remote filename/folder name</param>
		/// <param name="useEventHandler">OnBeginRenameFile, OnEndRenameFile을 사용할지 여부를 결정한다.
		/// <br><see cref="MoveFile"/>에서 사용합니다.</br></param>
		/// <returns>성공적으로 변경하였으면 true를 return 합니다.
		/// <br><see cref="MoveFile"/>에서 EventHandler를 위해 사용합니다.</returns>
		public virtual bool RenameFile(string fromRemoteFileName, string toRemoteFileName, bool useEventHandler)
		{
			if (useEventHandler == true && OnBeginRenameFile != null)
				OnBeginRenameFile(this, new FileInfoEventArgs(fromRemoteFileName, toRemoteFileName));

			bool isSuccess = false;

			lockTcpClient();

			try
			{
				ArrayList tempMessageList = new ArrayList();
				int returnValue = 0;

				tempMessageList = SendCommand(COMMAND_RENAMEFROM + " " + fromRemoteFileName);
				returnValue = getMessageReturnValue((string)tempMessageList[0]);

				if (returnValue != 350)
				{
					throw new Exception((string)tempMessageList[0]);
				}

				tempMessageList = SendCommand(COMMAND_RENAMETO + " " + toRemoteFileName);
				returnValue = getMessageReturnValue((string)tempMessageList[0]);

				if (returnValue != 250)
				{
					throw new Exception((string)tempMessageList[0]);
				}

				isSuccess = true;
			}
			catch (Exception ex)
			{
				addMessageExceptLastMessage(ex);
			}
			finally
			{
				unlockTcpClient();
			}

			if (useEventHandler == true && isSuccess == true && OnEndRenameFile != null)
				OnEndRenameFile(this, new FileInfoEventArgs(fromRemoteFileName, toRemoteFileName));

			return isSuccess;
		}

		/// <summary>
		/// Remote filename/folder name을 변경합니다.
		/// </summary>
		/// <remarks>이름을 변경할 권한이 있어야 합니다.(guest, anoymous는 통상적으로 변경 안됨)</remarks>
		/// <param name="fromRemoteFileName">변경할 remote filename/folder name</param>
		/// <param name="toRemoteFileName">변경될 remote filename/folder name</param>
		/// <returns>성공적으로 변경하였으면 true를 return 합니다.
		/// <br><see cref="MoveFile"/>에서 EventHandler를 위해 사용합니다.</returns>
		public virtual bool RenameFile(string fromRemoteFileName, string toRemoteFileName)
		{
			return RenameFile(fromRemoteFileName, toRemoteFileName, true);
		}
		#endregion

		#region ChangeDir
		/// <summary>
		/// 현재 directory 위치를 변경합니다.
		/// </summary>
		/// <param name="remotePath">변경할 remote path</param>
		public virtual void ChangeDir(string remotePath)
		{
			ChangeDir(remotePath, true);
		}

		/// <summary>
		/// 현재 directory 위치를 변경합니다.
		/// </summary>
		/// <param name="remotePath">변경할 remote path</param>
		/// <param name="useEventHandler">OnBeginRenameFile, OnEndRenameFile을 사용할지 여부를 결정한다.
		/// <br><see cref="MoveFile"/>에서 사용합니다.</br></param>
		public virtual void ChangeDir(string remotePath, bool useEventHandler)
		{
			if (useEventHandler == true && OnBeginChangeDir != null)
				OnBeginChangeDir(this, new DirInfoEventArgs(remotePath));

			bool isSuccess = false;

			remotePath.Replace(@"\", "/");

			lockTcpClient();

			try
			{

				ArrayList tempMessageList = new ArrayList();
				int returnValue = 0;

				tempMessageList = SendCommand(COMMAND_CWD + " " + remotePath);
				returnValue = getMessageReturnValue((string)tempMessageList[0]);

				if (returnValue != 250)
				{
					throw new Exception((string)tempMessageList[0]);
				}

				isSuccess = true;
			}
			catch (Exception ex)
			{
				addMessageExceptLastMessage(ex);
			}
			finally
			{
				unlockTcpClient();
			}

			if (useEventHandler == true && isSuccess == true && OnEndChangeDir != null)
				OnEndChangeDir(this, new DirInfoEventArgs(remotePath));
		}
		#endregion

		#region MakeDir
		/// <summary>
		/// 새로운 폴더를 생성합니다.
		/// </summary>
		/// <remarks>다중 폴더 생성("/Folder1/SubFolder1" 일 경우 Folder1, SubFolder1을 둘다 생성하는 것)을 지원하지 않습니다.</remarks>
		/// <param name="folderName">생성할 폴더이름</param>
		public virtual void MakeDir(string folderName)
		{
			if (OnBeginMakeDir != null)
				OnBeginMakeDir(this, new DirInfoEventArgs(folderName));

			bool isSuccess = false;

			lockTcpClient();

			try
			{
				ArrayList tempMessageList = new ArrayList();
				int returnValue = 0;

				tempMessageList = SendCommand(COMMAND_MAKEDIR + " " + folderName);
				returnValue = getMessageReturnValue((string)tempMessageList[0]);

				if (returnValue != 257)
				{
					throw new Exception((string)tempMessageList[0]);
				}

				isSuccess = true;
			}
			catch (Exception ex)
			{
				addMessageExceptLastMessage(ex);
			}
			finally
			{
				unlockTcpClient();
			}

			if (isSuccess == true && OnEndMakeDir != null)
				OnEndMakeDir(this, new DirInfoEventArgs(folderName));
		}
		#endregion

		#region RemoveDir
		/// <summary>
		/// 폴더를 제거한다
		/// </summary>
		/// <param name="folderName">제거할 폴더이름</param>
		public virtual void RemoveDir(string folderName)
		{
			if (OnBeginRemoveDir != null)
				OnBeginRemoveDir(this, new DirInfoEventArgs(folderName));

			bool isSuccess = false;

			folderName.Replace(@"\", "/");

			lockTcpClient();

			try
			{
				ArrayList tempMessageList = new ArrayList();
				int returnValue = 0;

				tempMessageList = SendCommand(COMMAND_REMOVEDIR + " " + folderName);
				returnValue = getMessageReturnValue((string)tempMessageList[0]);

				if (returnValue != 250)
				{
					throw new Exception((string)tempMessageList[0]);
				}

				isSuccess = true;
			}
			catch (Exception ex)
			{
				addMessageExceptLastMessage(ex);
			}
			finally
			{
				unlockTcpClient();
			}

			if (isSuccess == true && OnEndRemoveDir != null)
				OnEndRemoveDir(this, new DirInfoEventArgs(folderName));
		}
		#endregion

		#region SendCommand
		/// <summary>
		/// 해당 명령 조합을 remote에 전달합니다.
		/// </summary>
		/// <param name="command">전달할 명령</param>
		/// <returns></returns>
		public ArrayList SendCommand(string command)
		{
			NetworkStream stream = _tcpClient.GetStream();
			_activeConnectionsCount++;

			Byte[] cmdBytes = Encoding.Default.GetBytes((command + "\r\n").ToCharArray());
			stream.Write(cmdBytes, 0, cmdBytes.Length);

			_activeConnectionsCount--;

			return read();
		}
		#endregion
		#endregion

		#region Private Methods
		private void setTransferType(TransferType transferType)
		{
			switch (transferType)
			{
				case TransferType.ASCII:
					setClientMode("TYPE A");

					break;
				case TransferType.Binary:
					setClientMode("TYPE I");

					break;
				default:
					throw new Exception("Invalid File Transfer Type");
			}
		}

		private void setClientMode(string clientMode)
		{
			lockTcpClient();

			ArrayList tempMessageList = new ArrayList();
			int returnValue = 0;

			tempMessageList = SendCommand(clientMode);
			returnValue = getMessageReturnValue((string)tempMessageList[0]);

			if (returnValue != 200)
			{
				throw new Exception((string)tempMessageList[0]);
			}

			unlockTcpClient();
		}

		private TcpListener createDataListner()
		{
			int port = getPortNumber();
			setDataPort(port);

			IPHostEntry localHost = Dns.Resolve(Dns.GetHostName());
			TcpListener listner = new TcpListener(localHost.AddressList[0], port);

			//TcpListener listner = new TcpListener(port);
			return listner;
		}

		private TcpClient createDataClient()
		{
			int port = getPortNumber();

			//IPEndPoint ep = new IPEndPoint(GetLocalAddressList()[0], port);

			TcpClient client = new TcpClient();

			//client.Connect(ep);
			client.Connect(_remoteHost, port);

			return client;
		}

		private void setDataPort(int portNumber)
		{
			lockTcpClient();

			ArrayList tempMessageList = new ArrayList();
			int returnValue = 0;
			int portHigh = portNumber >> 8;
			int portLow = portNumber & 255;

			tempMessageList = SendCommand(COMMAND_PORT + " "
				+ getLocalAddressList()[0].ToString().Replace(".", ",")
				+ "," + portHigh.ToString() + "," + portLow);

			returnValue = getMessageReturnValue((string)tempMessageList[0]);

			if (returnValue != 200)
			{
				throw new Exception((string)tempMessageList[0]);
			}

			unlockTcpClient();
		}

		private ArrayList read()
		{
			NetworkStream stream = _tcpClient.GetStream();
			ArrayList messageList = new ArrayList();
			ArrayList tempMessage = readLines(stream);
			int tryCount = 0;

			while (tempMessage.Count == 0)
			{
				if (tryCount == 10)
				{
					//					throw new Exception("Server does not return message to the message");
					tempMessage.Add("Server does not return message to the message");

					break;
				}

				Thread.Sleep(1000);
				tryCount++;
				tempMessage = readLines(stream);
			}

			while (((string)tempMessage[tempMessage.Count - 1]).Substring(3, 1) == "-")
			{
				messageList.AddRange(tempMessage);
				tempMessage = readLines(stream);
			}

			messageList.AddRange(tempMessage);

			addMessagesToMessageList(messageList);

			return messageList;
		}

		private ArrayList readLines(NetworkStream stream)
		{
			ArrayList messageList = new ArrayList();
			char[] seperator = { '\n' };
			char[] toRemove = { '\r' };
			Byte[] buffer = new Byte[BLOCK_SIZE];
			int bytes = 0;
			string tmpMes = string.Empty;

			while (stream.DataAvailable)
			{
				bytes = stream.Read(buffer, 0, buffer.Length);
				tmpMes += Encoding.Default.GetString(buffer, 0, bytes);
			}

			string[] mess = tmpMes.Split(seperator);

			for (int i = 0; i < mess.Length; i++)
			{
				if (mess[i].Length > 0)
				{
					messageList.Add(mess[i].Trim(toRemove));
				}
			}

			return messageList;
		}

		private int getMessageReturnValue(string message)
		{
			return int.Parse(message.Substring(0, 3));
		}

		private int getPortNumber()
		{
			lockTcpClient();

			int port = 0;

			switch (_clientMode)
			{
				case ClientMode.Active:
					Random rnd = new Random((int)DateTime.Now.Ticks);
					port = DATA_PORT_RANGE_FROM + rnd.Next(DATA_PORT_RANGE_TO - DATA_PORT_RANGE_FROM);

					break;
				case ClientMode.Passive:
					ArrayList tempMessageList = new ArrayList();
					int returnValue = 0;

					tempMessageList = SendCommand(COMMAND_PASV);
					returnValue = getMessageReturnValue((string)tempMessageList[0]);

					if (returnValue != 227)
					{
						if (((string)tempMessageList[0]).Length > 4)
						{
							throw new Exception((string)tempMessageList[0]);
						}
						else
						{
							throw new Exception((string)tempMessageList[0] + " Passive Mode not implemented");
						}
					}

					string message = (string)tempMessageList[0];
					int index1 = message.IndexOf(",", 0);
					int index2 = message.IndexOf(",", index1 + 1);
					int index3 = message.IndexOf(",", index2 + 1);
					int index4 = message.IndexOf(",", index3 + 1);
					int index5 = message.IndexOf(",", index4 + 1);
					int index6 = message.IndexOf(")", index5 + 1);

					port = 256 * int.Parse(message.Substring(index4 + 1, index5 - index4 - 1)) + int.Parse(message.Substring(index5 + 1, index6 - index5 - 1));

					break;
			}

			unlockTcpClient();

			return port;
		}

		/// <summary>
		/// 마지막 메세지에 exception message가 포함되어 있으면 추가 하지 않는다.
		/// </summary>
		/// <param name="exceptionMessage">exception message</param>
		private void addMessageExceptLastMessage(Exception exceptionMessage)
		{
			if (_messageList.Count > 1 && _messageList.IndexOf(exceptionMessage.Message, _messageList.Count - 2) < 0)
			{
				addMessagesToMessageList(exceptionMessage.Message);
			}
		}

		private void addMessagesToMessageList(string message)
		{
			ArrayList tmpMessage = new ArrayList();

			tmpMessage.Add(message);

			addMessagesToMessageList(tmpMessage);
		}

		private void addMessagesToMessageList(ArrayList messages)
		{
			if (_isLogMessage == true)
			{
				_messageList.AddRange(messages);

				if (OnMessageUp != null)
					OnMessageUp(this, new MessageUpEventArgs(messages));
			}
		}

		private IPAddress[] getLocalAddressList()
		{
			return Dns.Resolve(Dns.GetHostName()).AddressList;
		}

		private void lockTcpClient()
		{
			System.Threading.Monitor.Enter(_tcpClient);
		}

		private void unlockTcpClient()
		{
			System.Threading.Monitor.Exit(_tcpClient);
		}

		private ArrayList getTokens(string text, string delimiter)
		{
			ArrayList tokens = new ArrayList();
			StringBuilder sb = new StringBuilder();
			int nextIdx = 0;

			// 입력되는 text의 format은 아래와 같아서 위치별로 split를 하지 않으면 blank가 있는 파일이름이나 폴더명은 짤린다
			// 08-18-06  02:16PM            267669857 Hynix003.GHS
			// 08-18-06  02:36PM       <DIR>          new folder
			string[] splitText = text.Split(new char[] { ' ' });
			string strDate = splitText[0];
			string strTime = splitText[2];
			string strSize = string.Empty;
			string strName = string.Empty;

			// 다음 string.Empty가 아닌 요소까지 검색
			for (int i = 3; i < splitText.Length; i++)
			{
				if (splitText[i] != string.Empty)
				{
					nextIdx = i + 1;
					strSize = splitText[i];

					break;
				}
			}

			// file 이름은 space가 들어갈 수 있으므로 첫번째 검색 조건 이후로는 space를 모두 연결한다.
			for (int i = nextIdx; i < splitText.Length; i++)
			{
				if (splitText[i] != string.Empty)
				{
					nextIdx = i;

					break;
				}
			}

			for (int i = nextIdx; i < splitText.Length; i++)
			{
				sb.AppendFormat("{0} ", splitText[i]);
			}

			strName = sb.ToString(0, sb.Length - 1);

			tokens.Add(strDate);
			tokens.Add(strTime);
			tokens.Add(strSize);
			tokens.Add(strName);

			return tokens;
		}
		#endregion
	}
}
