/**********************************************************************************************************************/
/*	Name		:	MapFileTransfer.Database.VendorFtpTable
/*	Purpose		:	Vender의 Ftp 정보를 구해오는 class
/*	Creator		:	Kim Ki Won
/*	Create		:	2006년 8월 22일 화요일 오후 11:08:00
/*	Modifier	:	
/*	Update		:	2006년 8월 22일 화요일
/*	Comment		:	
/**********************************************************************************************************************/

using System;
using System.Data;
using System.Text;

using My.Database;
using MapFileTransfer.Common;

namespace MapFileTransfer.Table
{
	/// <summary>
	/// Vender의 Ftp 정보를 구해오는 class
	/// </summary>
	public class VendorFtpTable
	{
		#region Members
		private string _vendor;
		private string _remoteHost;
		private string _userId;
		private string _password;
		#endregion

		#region Constructor
		/// <summary>
		/// Vendor의 Ftp 정보를 구합니다.
		/// </summary>
		public VendorFtpTable()
		{
			if (_vendor == null)
				_vendor = string.Empty;
			
			_remoteHost = string.Empty;
			_userId = string.Empty;
			_password = string.Empty;
		}

		/// <summary>
		/// Vendor의 Ftp 정보를 구합니다.
		/// </summary>
		/// <param name="vendor">Ftp 정보를 구할 vendor</param>
		public VendorFtpTable(string vendor)
			: this()
		{
			_vendor = vendor;
		}
		#endregion

		#region Properties
		/// <summary>
		/// Vendor 정보를 지정합니다.
		/// </summary>
		public string Vendor
		{
			set
			{
				_vendor = value;
			}
		}

		/// <summary>
		/// Vendor Ftp의 Remotehost를 구합니다.
		/// </summary>
		public string RemoteHost
		{
			get
			{
				if (_remoteHost == string.Empty)
					GetFileList();

				return _remoteHost;
			}
		}
		
		/// <summary>
		/// Vendor Ftp의 User ID를 구합니다.
		/// </summary>
		public string UserId
		{
			get
			{
				if (_remoteHost == string.Empty)
					GetFileList();

				return _userId;
			}
		}
		
		/// <summary>
		/// Vendor Ftp이 Password를 구합니다.
		/// </summary>
		public string Password
		{
			get
			{
				if (_remoteHost == string.Empty)
					GetFileList();

				return _password;
			}
		}
		#endregion
        
		#region GetFileList
		/// <summary>
		/// Ftp 정보를 구합니다.
		/// </summary>
		/// <returns>조건에 맞는 row count</returns>
		public int GetFileList()
		{
			DataManipulation dm = new DataManipulation();
			DataTable dataTable = dm.ExecuteQuery(getCommandText());
			int result = -1;

			result = dataTable.Rows.Count;

			if (result > 0)
			{
				_remoteHost = dataTable.Rows[0]["vend_ftp"].ToString();
				_userId = dataTable.Rows[0]["ftp_id"].ToString();
				_password = dataTable.Rows[0]["ftp_pass"].ToString();
			}

			return result;
		}
		#endregion
		
		#region Private methods
		private string getCommandText()
		{
			if (_vendor == string.Empty)
				throw new InvalidOperationException(Constant.VENDOR_DOES_NOT_INPUTED);

			StringBuilder sb = new StringBuilder();
            
			sb.Append("SELECT ");
			sb.Append("	vend_id, ");
			sb.Append("	vend_ftp, ");
			sb.Append("	ftp_id, ");
			sb.Append("	ftp_pass ");
			sb.AppendFormat("	FROM {0} ", Constant.TABLE_VENDOR_FTP);
			sb.AppendFormat("	WHERE vend_id = '{0}' ", _vendor);

			return sb.ToString();
		}
		#endregion		
	}
}
