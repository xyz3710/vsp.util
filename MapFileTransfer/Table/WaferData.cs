/**********************************************************************************************************************/
/*	Name		:	MapFileTransfer.Table.WaferData
/*	Purpose		:	전송할 Wafer file list를 구합니다.
/*	Creator		:	Kim Ki Won
/*	Create		:	2006년 8월 23일 수요일 오후 1:12:36
/*	Modifier	:	
/*	Update		:	2006년 8월 23일 수요일
/*	Comment		:	
/**********************************************************************************************************************/

using System;
using System.Data;
using System.Text;
using System.Collections;

using My.Database;
using MapFileTransfer.Common;

namespace MapFileTransfer.Table
{
	/// <summary>
	/// 전송할 Wafer file list를 구합니다.
	/// </summary>
	public class WaferDataTable
	{
		#region Members
		private string _vendor;
		private ArrayList _fileList;
		#endregion

		#region Constructor
		/// <summary>
		/// 전송할 file list를 구합니다.
		/// </summary>
		public WaferDataTable()
		{
			if (_vendor == null)
				_vendor = string.Empty;

			_fileList = new ArrayList();
		}
		
		/// <summary>
		/// 전송할 file list를 구합니다.
		/// </summary>
		/// <param name="vendor">file list를 구할 vendor</param>
		public WaferDataTable(string vendor)
			: this()
		{
			_vendor = vendor;
		}
		#endregion

		#region Properties
		/// <summary>
		/// Vendor 정보를 지정합니다.
		/// </summary>
		public string Vender
		{
			set
			{
				_vendor = value;
			}
		}
		
		/// <summary>
		/// File list를 구합니다.
		/// </summary>
		/// <return>Hashtable을 포함하는 ArrayList로 key : file_name, value : hynix_lot_no</return>
		public ArrayList FileList
		{
			get
			{
				return GetFileList();
			}
		}
		#endregion
        
		#region GetFileList
		/// <summary>
		/// File list 정보를 구합니다.
		/// </summary>
		/// <returns>Hashtable을 포함하는 ArrayList로 key : file_name, value : hynix_lot_no</returns>
		public ArrayList GetFileList()
		{
			DataManipulation dm = new DataManipulation();
			DataTable dataTable = dm.ExecuteQuery(getCommandText());
			Hashtable dataColumn = new Hashtable();

			for (int i = 0; i < dataTable.Rows.Count; i++)
			{
				dataColumn = new Hashtable();

				dataColumn.Add(
					dataTable.Rows[i]["file_name"].ToString(),
					dataTable.Rows[i]["hynix_lot_no"].ToString());

				_fileList.Add(dataColumn);
			}

			return _fileList;
		}
		#endregion
		
		#region Private methods
		private string getCommandText()
		{
			StringBuilder sb = new StringBuilder();
            
			if (_vendor == string.Empty)
				throw new InvalidOperationException(Constant.VENDOR_DOES_NOT_INPUTED);

			sb.Append("SELECT ");
			sb.Append("	DISTINCT file_name, hynix_lot_no ");
			sb.AppendFormat("	FROM {0} ", Constant.TABLE_TEMPORARY);
			sb.AppendFormat("	WHERE vendor_id = '{0}' ", _vendor);
			sb.Append("		AND trans_flag = 'N' ");

			return sb.ToString();
		}
		#endregion		
	}
}
