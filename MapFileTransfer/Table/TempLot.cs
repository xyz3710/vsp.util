/**********************************************************************************************************************/
/*	Name		:	MapFileTransfer.Database.TempLotTable
/*	Purpose		:	TempLot table을 처리하는 class
/*	Creator		:	Kim Ki Won
/*	Create		:	2006년 8월 22일 화요일 오후 8:52:41
/*	Modifier	:	
/*	Update		:	2006년 8월 22일 화요일
/*	Comment		:	
/**********************************************************************************************************************/

using System;
using System.Text;

using My.Database;
using MapFileTransfer.Common;

namespace MapFileTransfer.Table
{
	/// <summary>
	/// TempLot table을 처리하는 class
	/// </summary>
	public class TempLotTable
	{
		#region Members
		private string _vendor;
		private string _lotId;
		private string _filename;
		#endregion

		#region Constructor
		/// <summary>
		/// TempLotTable instance를 생성합니다.
		/// </summary>
		public TempLotTable()
		{			
			if (_vendor == null)
				_vendor = string.Empty;

			if (_lotId == null)
				_lotId = string.Empty;

			if (_filename == null)
				_filename = string.Empty;
		}

		/// <summary>
		/// TempLotTable instance를 생성합니다.
		/// </summary>
		/// <param name="vender">vender 정보를 입력합니다.</param>
		/// <param name="lotId">lotId 정보를 입력합니다.</param>
		/// <param name="filename">filename 정보를 입력합니다.</param>
		public TempLotTable(string vender, string lotId, string filename)
			: this()
		{
			_vendor = vender;
			_lotId = lotId;
			_filename = filename;
		}
		#endregion

		#region Properties
		/// <summary>
		/// Vender를 설정합니다.
		/// </summary>
		public string Vender
		{
			set
			{
				_vendor = value;
			}
		}

		/// <summary>
		/// LotId를 설정합니다.
		/// </summary>
		public string LotId
		{
			set
			{
				_lotId = value;
			}
		}

		/// <summary>
		/// FileName을 설정합니다.
		/// </summary>
		public string FileName
		{
			set
			{
				_filename = value;
			}
		}       
		#endregion

		#region Update
		/// <summary>
		/// TempLotTable을 Update 합니다.
		/// </summary>
		/// <returns></returns>
		public int Update()
		{
			return new DataManipulation().ExecuteNonQuery(getUpdateCommandText());
		}		
		#endregion
		
		#region Delete
		/// <summary>
		/// TempLotTable을 Delete 합니다.
		/// </summary>
		/// <returns></returns>
		public int Delete()
		{
			return new DataManipulation().ExecuteNonQuery(getDeleteCommandText());
		}
		#endregion
			
		#region Private methods
		private string getUpdateCommandText()
		{
			StringBuilder sb = new StringBuilder();
            
			sb.AppendFormat("UPDATE {0} ", Constant.TABLE_TEMPORARY);
			sb.Append("SET trans_flag = 'Y' ");
			sb.AppendFormat("WHERE vendor_id = '{0}' ", _vendor);
			sb.AppendFormat("	AND hynix_lot_no = '{0}' ", _lotId);
			sb.AppendFormat("	AND file_name = '{0}' ", _filename);
			sb.Append("	AND (trans_flag = ' ' OR trans_flag = 'N') ");

			return sb.ToString();
		}

		private string getDeleteCommandText()
		{
			StringBuilder sb = new StringBuilder();
            
			sb.AppendFormat("DELETE {0} ", Constant.TABLE_TEMPORARY);
			sb.AppendFormat("WHERE vendor_id = '{0}' ", _vendor);
			sb.Append("	AND trans_flag  = 'Y' ");

			return sb.ToString();
		}
		#endregion
	}
}
