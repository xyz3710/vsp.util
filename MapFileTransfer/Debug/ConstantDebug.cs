/**********************************************************************************************************************/
/*	Name		:	My.Debug.ConstantDebug
/*	Purpose		:	상수 목록을 위한 class
/*	Creator		:	Kim Ki Won
/*	Create		:	2006년 8월 22일 화요일 오후 9:36:06
/*	Modifier	:	
/*	Update		:	2006년 8월 22일 화요일
/*	Comment		:	
/**********************************************************************************************************************/

using System;

namespace My.Debug
{
	/// <summary>
	/// 상수 목록 class 입니다.
	/// </summary>
	public class ConstantDebug
	{
		public const string LOG_PATH = @"D:\hicms-p.data\data\wafer\Log\MapFileTransfer.log";
	}
}
