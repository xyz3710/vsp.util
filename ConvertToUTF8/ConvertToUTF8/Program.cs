﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;

namespace ConvertToUTF8
{
	class Program
	{
		static int Main(string[] args)
		{
			if (args.Length <= 1
				|| (args[0].Contains(".") == false && args[0].Contains("\\") == false))
			{
				ShowUsage();

				return 0;
			}

			List<string> files = GetFileListFromArgument(args);
			int count = 0;
			
			foreach (var file in files)
			{
				string filePath = Path.GetFullPath(file);

				var text = File.ReadAllText(filePath, Encoding.Default);
				File.WriteAllText(filePath, text, Encoding.UTF8);

				count++;
			}

			if (count == 0)
			{
				Console.WriteLine("\r\n\tFile(s) does not exist.\r\n");

				return -1;
			}

			Console.WriteLine("\r\n\t{0} file(s) was(were) converted.\r\n", count);
						
			return count;
		}

		private static List<string> GetFileListFromArgument(string[] args)
		{
			List<string> files = new List<string>();
			SearchOption searchOption = SearchOption.TopDirectoryOnly;
			int subDirIndex = -1;

			for (int i = 0; i < args.Length; i++)
			{
				if (args[i].ToLower() == "/s")
				{
					searchOption = SearchOption.AllDirectories;
					subDirIndex = i;

					break;
				}
			}

			for (int i = 1; i < args.Length; i++)
			{
				if (i == subDirIndex)
					continue;

				files.AddRange(Directory.GetFiles(args[0], args[i], searchOption));
			}
			
			return files;
		}

		private static void ShowUsage()
		{
			Console.WriteLine();
			Console.WriteLine("Convert file to UTF8");
			Console.WriteLine();
			Console.WriteLine("Usage : ConvertToUTF8 <directory> <file patterns> /s");
			Console.WriteLine("        /s : check sub directories");
			Console.WriteLine("      ex)");
			Console.WriteLine("        ConvertToUTF8 .\\ \"file name\"");
			Console.WriteLine("        ConvertToUTF8 \"D:\\Target Folder\" file1 file2 file3 ...");
			Console.WriteLine("        ConvertToUTF8 .\\ *.log ???.html");
			Console.WriteLine();
			Console.WriteLine("Return value:");
			Console.WriteLine("\t  0 : argument is empty.");
			Console.WriteLine("\t -1 : file does not exist.");
			Console.WriteLine("\t >0 : converted file counts.");

			Console.WriteLine();
		}
	}
}
