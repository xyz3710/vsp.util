﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Reflection;
using System.Windows.Forms;

namespace LapTimer
{
	public partial class MainForm : Form
	{
		private const string TIME_FORMAT = "HH:mm:ss.ff";
		private const string INTERVAL_FORMAT = @"hh\:mm\:ss\.ff";

		public MainForm()
		{
			InitializeComponent();
		}

		private class LapItem
		{
			public DateTime Start { get; set; }
			public DateTime Lap { get; set; }
			public TimeSpan Interval { get => Lap - Start; }
		}

		private Color ForeColor2 { get => Color.White; }
		private Color BackColor2 { get => Color.Black; }

		private System.Timers.Timer Timer { get; set; }
		private DateTime LapTime { get; set; }
		private DateTime LastLapTime { get; set; }

		protected override bool ProcessCmdKey(ref Message msg, Keys keyData)
		{
			const int WM_KEYDOWN = 0x100;
			const int WM_SYSKEYDOWN = 0x104;

			if ((msg.Msg == WM_KEYDOWN) || (msg.Msg == WM_SYSKEYDOWN))
			{
				switch (keyData)
				{
					case Keys.Enter:
						if (Timer.Enabled == true)
						{
							LapCheck();
							LapControl();
						}
						else
						{
							LapCheck();
						}

						return true;
					case Keys.Enter | Keys.Control:
						ResetControl();

						return true;
					case Keys.Space:
						LapCheck();

						return true;
					case Keys.Q | Keys.Control:
						Close();

						return true;
				}
			}

			return base.ProcessCmdKey(ref msg, keyData);
		}

		protected override void OnLoad(EventArgs e)
		{
			InitControls();

			Timer = new System.Timers.Timer();
			Timer.Elapsed += (sender, ea) =>
			{
				lbLaps.Invoke(new MethodInvoker(() =>
				{
					lbLaps.Text = ea.SignalTime.ToString(TIME_FORMAT);
				}));
				LapTime = ea.SignalTime;
			};

			btnControl.Click += (sender, ea) => { LapControl(); };
			btnReset.Click += (sender, ea) => { ResetControl(); };
			btnLap.Click += (sender, ea) => { LapCheck(); };
			lapChart.Click += (sender, ea) => { lapChart.Series[0].IsValueShownAsLabel = !lapChart.Series[0].IsValueShownAsLabel; };
			lbAverage.Click += (sender, ea) => { lapChart.Series[1].IsValueShownAsLabel = !lapChart.Series[1].IsValueShownAsLabel; };
		}

		protected override void OnShown(EventArgs e)
		{
			lbLaps.Text = DateTime.Now.ToString(TIME_FORMAT);
			ResetControl();
		}

		private void InitControls()
		{
			SetDoubleBuffered(lbLaps);
			SetDoubleBuffered(lbAverage);
			SetDoubleBuffered(lvLaps);

			ForeColor = ForeColor2;
			BackColor = BackColor2;
			btnControl.BackColor = BackColor2;
			btnReset.BackColor = BackColor2;
			btnLap.BackColor = BackColor2;
			lvLaps.ForeColor = ForeColor2;
			lvLaps.BackColor = BackColor2;
			lapChart.ForeColor = ForeColor2;
			lapChart.BackColor = BackColor2;
			lapChart.BorderlineColor = BackColor2;

			var lapSeries = lapChart.Series[0];
			lapSeries.LabelForeColor = ForeColor2;
			lapSeries.LabelBackColor = BackColor2;
			lapSeries.BorderWidth = 4;

			var avgSeries = lapChart.Series[1];
			avgSeries.LabelForeColor = Color.Black;
			avgSeries.LabelBackColor = Color.Orange;
			avgSeries.BorderWidth = 3;

			var lapChartArea = lapChart.ChartAreas[0];

			lapChartArea.Position.Auto = false;
			lapChartArea.Position.Height = 99F;
			lapChartArea.Position.Width = 100F;
			lapChartArea.Position.Y = 1F;
			lapChartArea.BackColor = BackColor2;
			lapChartArea.AxisX.LineColor = ForeColor2;
			lapChartArea.AxisX.LabelStyle.ForeColor = ForeColor2;
			lapChartArea.AxisX.MajorGrid.LineColor = ForeColor2;
			lapChartArea.AxisX.MajorTickMark.LineColor = ForeColor2;
			lapChartArea.AxisX.MinorGrid.LineColor = ForeColor2;
			lapChartArea.AxisX.MinorTickMark.LineColor = ForeColor2;
			lapChartArea.AxisY.LineColor = ForeColor2;
			lapChartArea.AxisY.LabelStyle.ForeColor = ForeColor2;
			lapChartArea.AxisY.MajorGrid.LineColor = ForeColor2;
			lapChartArea.AxisY.MajorTickMark.LineColor = ForeColor2;
			lapChartArea.AxisY.MinorGrid.LineColor = ForeColor2;
			lapChartArea.AxisY.MinorTickMark.LineColor = ForeColor2;

			lbAverage.ForeColor = Color.Orange;
			lbAverage.BackColor = BackColor2;
		}

		private void SetDoubleBuffered(Control control, bool enabled = true)
		{
			if (control == null)
			{
				throw new ArgumentNullException("control");
			}

			var type = control.GetType();
			var objArray = new object[] { true };

			type.InvokeMember("DoubleBuffered", BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.SetProperty, null, control, objArray);
		}

		private void LapControl()
		{
			if (Timer.Enabled == true)
			{
				btnControl.Image = Properties.Resources.Start;
			}
			else
			{
				btnControl.Image = Properties.Resources.Stop;
				LastLapTime = DateTime.Now;
			}

			Timer.Enabled = !Timer.Enabled;
		}

		private void LapCheck()
		{
			if (Timer.Enabled == false)
			{
				LapControl();
				LastLapTime = DateTime.Now;

				return;
			}

			var lapItem = new LapItem { Start = LastLapTime };

			LastLapTime = LapTime;
			lapItem.Lap = LastLapTime;

			var item = new ListViewItem(string.Empty);

			item.Tag = lapItem;
			item.SubItems.Add($"{lvLaps.Items.Count + 1:#,##0}");
			item.SubItems.Add(lapItem.Start.ToString(TIME_FORMAT));
			item.SubItems.Add(lapItem.Lap.ToString(TIME_FORMAT));
			item.SubItems.Add(lapItem.Interval.ToString(INTERVAL_FORMAT, null));
			lvLaps.Invoke(new MethodInvoker(() =>
			{
				lvLaps.Items.Insert(0, item);
			}));
			lvLaps.Refresh();

			var lapSeries = lapChart.Series[0];
			var avgSeries = lapChart.Series[1];
			var source = lvLaps.Items.Cast<ListViewItem>().Select(x => x.Tag as LapItem);
			var average = Math.Round(source.Average(x => x.Interval.TotalMilliseconds) / 1000d, 2);
			var total = TimeSpan.FromTicks(source.Sum(x => x.Interval.Ticks));

			lapSeries.Points.AddY(Math.Round(lapItem.Interval.TotalMilliseconds / 1000d, 2));
			avgSeries.Points.AddY(average);
			lbAverage.Invoke(new MethodInvoker(() =>
			{
				lbAverage.Text = $"{average:#,##0.##} sec";
			}));
			lbTotal.Invoke(new MethodInvoker(() =>
			{
				lbTotal.Text = total.ToString(INTERVAL_FORMAT);
			}));
		}

		private void ResetControl()
		{
			if (Timer.Enabled == true)
			{
				return;
			}
			
			lvLaps.Items.Clear();
			lapChart.Series[0].Points.Clear();
			lapChart.Series[1].Points.Clear();
		}
	}
}
