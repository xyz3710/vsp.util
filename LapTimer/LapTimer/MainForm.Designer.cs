﻿namespace LapTimer
{
	partial class MainForm
	{
		/// <summary>
		/// 필수 디자이너 변수입니다.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// 사용 중인 모든 리소스를 정리합니다.
		/// </summary>
		/// <param name="disposing">관리되는 리소스를 삭제해야 하면 true이고, 그렇지 않으면 false입니다.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form 디자이너에서 생성한 코드

		/// <summary>
		/// 디자이너 지원에 필요한 메서드입니다. 
		/// 이 메서드의 내용을 코드 편집기로 수정하지 마세요.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea1 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
			System.Windows.Forms.DataVisualization.Charting.Series series1 = new System.Windows.Forms.DataVisualization.Charting.Series();
			System.Windows.Forms.DataVisualization.Charting.Series series2 = new System.Windows.Forms.DataVisualization.Charting.Series();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
			this.tlpMain = new System.Windows.Forms.TableLayoutPanel();
			this.lapChart = new System.Windows.Forms.DataVisualization.Charting.Chart();
			this.tlpLeft = new System.Windows.Forms.TableLayoutPanel();
			this.tlpButtons = new System.Windows.Forms.TableLayoutPanel();
			this.btnControl = new System.Windows.Forms.Button();
			this.btnLap = new System.Windows.Forms.Button();
			this.btnReset = new System.Windows.Forms.Button();
			this.lbLaps = new System.Windows.Forms.Label();
			this.lbAverage = new System.Windows.Forms.Label();
			this.lbTotal = new System.Windows.Forms.Label();
			this.lvLaps = new System.Windows.Forms.ListView();
			this.columnHeader1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
			this.chNo = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
			this.chStart = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
			this.chLap = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
			this.chInterval = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
			this.toolTip = new System.Windows.Forms.ToolTip(this.components);
			this.tlpMain.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.lapChart)).BeginInit();
			this.tlpLeft.SuspendLayout();
			this.tlpButtons.SuspendLayout();
			this.SuspendLayout();
			// 
			// tlpMain
			// 
			this.tlpMain.ColumnCount = 2;
			this.tlpMain.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 406F));
			this.tlpMain.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
			this.tlpMain.Controls.Add(this.lapChart, 1, 0);
			this.tlpMain.Controls.Add(this.tlpLeft, 0, 0);
			this.tlpMain.Dock = System.Windows.Forms.DockStyle.Fill;
			this.tlpMain.Location = new System.Drawing.Point(0, 0);
			this.tlpMain.Margin = new System.Windows.Forms.Padding(0);
			this.tlpMain.Name = "tlpMain";
			this.tlpMain.RowCount = 1;
			this.tlpMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 60F));
			this.tlpMain.Size = new System.Drawing.Size(1584, 761);
			this.tlpMain.TabIndex = 1;
			// 
			// lapChart
			// 
			this.lapChart.BorderlineColor = System.Drawing.Color.Black;
			chartArea1.AxisY.LineColor = System.Drawing.Color.Empty;
			chartArea1.AxisY.MajorTickMark.LineColor = System.Drawing.Color.Empty;
			chartArea1.Name = "lapChartArea";
			chartArea1.Position.Auto = false;
			chartArea1.Position.Height = 99F;
			chartArea1.Position.Width = 100F;
			chartArea1.Position.Y = 1F;
			this.lapChart.ChartAreas.Add(chartArea1);
			this.lapChart.Dock = System.Windows.Forms.DockStyle.Fill;
			this.lapChart.Location = new System.Drawing.Point(407, 1);
			this.lapChart.Margin = new System.Windows.Forms.Padding(1);
			this.lapChart.Name = "lapChart";
			series1.BorderWidth = 4;
			series1.ChartArea = "lapChartArea";
			series1.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Spline;
			series1.Font = new System.Drawing.Font("Tahoma", 9F);
			series1.IsValueShownAsLabel = true;
			series1.LabelBackColor = System.Drawing.Color.Black;
			series1.LabelForeColor = System.Drawing.Color.White;
			series1.MarkerStyle = System.Windows.Forms.DataVisualization.Charting.MarkerStyle.Circle;
			series1.Name = "lapSeries";
			series2.BorderWidth = 3;
			series2.ChartArea = "lapChartArea";
			series2.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Spline;
			series2.Font = new System.Drawing.Font("Tahoma", 9F);
			series2.LabelBackColor = System.Drawing.Color.Orange;
			series2.LabelForeColor = System.Drawing.Color.White;
			series2.Name = "avgSeries";
			this.lapChart.Series.Add(series1);
			this.lapChart.Series.Add(series2);
			this.lapChart.Size = new System.Drawing.Size(1176, 759);
			this.lapChart.TabIndex = 2;
			this.lapChart.Text = "n";
			this.toolTip.SetToolTip(this.lapChart, "If you click, toggle label value box.");
			// 
			// tlpLeft
			// 
			this.tlpLeft.ColumnCount = 1;
			this.tlpLeft.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
			this.tlpLeft.Controls.Add(this.tlpButtons, 0, 0);
			this.tlpLeft.Controls.Add(this.lvLaps, 0, 1);
			this.tlpLeft.Dock = System.Windows.Forms.DockStyle.Fill;
			this.tlpLeft.Location = new System.Drawing.Point(0, 0);
			this.tlpLeft.Margin = new System.Windows.Forms.Padding(0);
			this.tlpLeft.Name = "tlpLeft";
			this.tlpLeft.RowCount = 2;
			this.tlpLeft.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 80F));
			this.tlpLeft.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
			this.tlpLeft.Size = new System.Drawing.Size(406, 761);
			this.tlpLeft.TabIndex = 3;
			// 
			// tlpButtons
			// 
			this.tlpButtons.ColumnCount = 4;
			this.tlpButtons.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 18F));
			this.tlpButtons.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 18F));
			this.tlpButtons.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 18F));
			this.tlpButtons.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 46F));
			this.tlpButtons.Controls.Add(this.btnControl, 0, 0);
			this.tlpButtons.Controls.Add(this.btnLap, 2, 0);
			this.tlpButtons.Controls.Add(this.btnReset, 1, 0);
			this.tlpButtons.Controls.Add(this.lbLaps, 0, 1);
			this.tlpButtons.Controls.Add(this.lbAverage, 3, 0);
			this.tlpButtons.Controls.Add(this.lbTotal, 3, 1);
			this.tlpButtons.Dock = System.Windows.Forms.DockStyle.Fill;
			this.tlpButtons.Location = new System.Drawing.Point(0, 0);
			this.tlpButtons.Margin = new System.Windows.Forms.Padding(0);
			this.tlpButtons.Name = "tlpButtons";
			this.tlpButtons.RowCount = 2;
			this.tlpButtons.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
			this.tlpButtons.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
			this.tlpButtons.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
			this.tlpButtons.Size = new System.Drawing.Size(406, 80);
			this.tlpButtons.TabIndex = 0;
			// 
			// btnControl
			// 
			this.btnControl.Dock = System.Windows.Forms.DockStyle.Fill;
			this.btnControl.Image = global::LapTimer.Properties.Resources.Start;
			this.btnControl.Location = new System.Drawing.Point(0, 0);
			this.btnControl.Margin = new System.Windows.Forms.Padding(0);
			this.btnControl.Name = "btnControl";
			this.btnControl.Size = new System.Drawing.Size(73, 40);
			this.btnControl.TabIndex = 0;
			this.toolTip.SetToolTip(this.btnControl, "Start/Stop (Enter)");
			this.btnControl.UseVisualStyleBackColor = true;
			// 
			// btnLap
			// 
			this.btnLap.Dock = System.Windows.Forms.DockStyle.Fill;
			this.btnLap.Image = global::LapTimer.Properties.Resources.Lap;
			this.btnLap.Location = new System.Drawing.Point(146, 0);
			this.btnLap.Margin = new System.Windows.Forms.Padding(0);
			this.btnLap.Name = "btnLap";
			this.btnLap.Size = new System.Drawing.Size(73, 40);
			this.btnLap.TabIndex = 2;
			this.toolTip.SetToolTip(this.btnLap, "Lap (Space)");
			this.btnLap.UseVisualStyleBackColor = true;
			// 
			// btnReset
			// 
			this.btnReset.Dock = System.Windows.Forms.DockStyle.Fill;
			this.btnReset.Image = global::LapTimer.Properties.Resources.Reset;
			this.btnReset.Location = new System.Drawing.Point(73, 0);
			this.btnReset.Margin = new System.Windows.Forms.Padding(0);
			this.btnReset.Name = "btnReset";
			this.btnReset.Size = new System.Drawing.Size(73, 40);
			this.btnReset.TabIndex = 1;
			this.toolTip.SetToolTip(this.btnReset, "Reset (Ctrl+Enter)");
			this.btnReset.UseVisualStyleBackColor = true;
			// 
			// lbLaps
			// 
			this.lbLaps.AutoSize = true;
			this.lbLaps.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.tlpButtons.SetColumnSpan(this.lbLaps, 3);
			this.lbLaps.Dock = System.Windows.Forms.DockStyle.Fill;
			this.lbLaps.Font = new System.Drawing.Font("맑은 고딕", 13F);
			this.lbLaps.Location = new System.Drawing.Point(1, 41);
			this.lbLaps.Margin = new System.Windows.Forms.Padding(1);
			this.lbLaps.Name = "lbLaps";
			this.lbLaps.Size = new System.Drawing.Size(217, 38);
			this.lbLaps.TabIndex = 1;
			this.lbLaps.Text = "00:00:00.00";
			this.lbLaps.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			this.toolTip.SetToolTip(this.lbLaps, "Current Times");
			// 
			// lbAverage
			// 
			this.lbAverage.AutoSize = true;
			this.lbAverage.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.lbAverage.Dock = System.Windows.Forms.DockStyle.Fill;
			this.lbAverage.Font = new System.Drawing.Font("맑은 고딕", 13F);
			this.lbAverage.Location = new System.Drawing.Point(220, 1);
			this.lbAverage.Margin = new System.Windows.Forms.Padding(1);
			this.lbAverage.Name = "lbAverage";
			this.lbAverage.Size = new System.Drawing.Size(185, 38);
			this.lbAverage.TabIndex = 1;
			this.lbAverage.Text = "Average 0.00 Sec";
			this.lbAverage.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			this.toolTip.SetToolTip(this.lbAverage, "Average Seconds(If your click, toggle label value box)");
			// 
			// lbTotal
			// 
			this.lbTotal.AutoSize = true;
			this.lbTotal.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.lbTotal.Dock = System.Windows.Forms.DockStyle.Fill;
			this.lbTotal.Font = new System.Drawing.Font("맑은 고딕", 13F);
			this.lbTotal.Location = new System.Drawing.Point(220, 41);
			this.lbTotal.Margin = new System.Windows.Forms.Padding(1);
			this.lbTotal.Name = "lbTotal";
			this.lbTotal.Size = new System.Drawing.Size(185, 38);
			this.lbTotal.TabIndex = 3;
			this.lbTotal.Text = "Total Intervals";
			this.lbTotal.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			this.toolTip.SetToolTip(this.lbTotal, "Total Intervals");
			// 
			// lvLaps
			// 
			this.lvLaps.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.lvLaps.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader1,
            this.chNo,
            this.chStart,
            this.chLap,
            this.chInterval});
			this.lvLaps.Dock = System.Windows.Forms.DockStyle.Fill;
			this.lvLaps.HideSelection = false;
			this.lvLaps.Location = new System.Drawing.Point(1, 81);
			this.lvLaps.Margin = new System.Windows.Forms.Padding(1);
			this.lvLaps.Name = "lvLaps";
			this.lvLaps.Size = new System.Drawing.Size(404, 679);
			this.lvLaps.TabIndex = 1;
			this.lvLaps.UseCompatibleStateImageBehavior = false;
			this.lvLaps.View = System.Windows.Forms.View.Details;
			// 
			// columnHeader1
			// 
			this.columnHeader1.Width = 0;
			// 
			// chNo
			// 
			this.chNo.Text = "No";
			this.chNo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.chNo.Width = 50;
			// 
			// chStart
			// 
			this.chStart.Text = "Start";
			this.chStart.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
			this.chStart.Width = 110;
			// 
			// chLap
			// 
			this.chLap.Text = "Lap";
			this.chLap.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
			this.chLap.Width = 110;
			// 
			// chInterval
			// 
			this.chInterval.Text = "Interval";
			this.chInterval.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
			this.chInterval.Width = 110;
			// 
			// toolTip
			// 
			this.toolTip.AutomaticDelay = 10;
			this.toolTip.AutoPopDelay = 5000;
			this.toolTip.InitialDelay = 10;
			this.toolTip.ReshowDelay = 2;
			this.toolTip.ShowAlways = true;
			// 
			// MainForm
			// 
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
			this.ClientSize = new System.Drawing.Size(1584, 761);
			this.Controls.Add(this.tlpMain);
			this.Font = new System.Drawing.Font("맑은 고딕", 11F);
			this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
			this.Name = "MainForm";
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Text = "Lap Timer";
			this.tlpMain.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.lapChart)).EndInit();
			this.tlpLeft.ResumeLayout(false);
			this.tlpButtons.ResumeLayout(false);
			this.tlpButtons.PerformLayout();
			this.ResumeLayout(false);

		}

		#endregion

		private System.Windows.Forms.TableLayoutPanel tlpMain;
		private System.Windows.Forms.DataVisualization.Charting.Chart lapChart;
		private System.Windows.Forms.TableLayoutPanel tlpLeft;
		private System.Windows.Forms.TableLayoutPanel tlpButtons;
		private System.Windows.Forms.Button btnControl;
		private System.Windows.Forms.Button btnLap;
		private System.Windows.Forms.Button btnReset;
		private System.Windows.Forms.Label lbLaps;
		private System.Windows.Forms.Label lbAverage;
		private System.Windows.Forms.Label lbTotal;
		private System.Windows.Forms.ListView lvLaps;
		private System.Windows.Forms.ColumnHeader columnHeader1;
		private System.Windows.Forms.ColumnHeader chNo;
		private System.Windows.Forms.ColumnHeader chStart;
		private System.Windows.Forms.ColumnHeader chLap;
		private System.Windows.Forms.ColumnHeader chInterval;
		private System.Windows.Forms.ToolTip toolTip;
	}
}

