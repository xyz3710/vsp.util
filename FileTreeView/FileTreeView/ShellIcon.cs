﻿using System;
using System.Drawing;
using System.Runtime.InteropServices;
using System.IO;

namespace FileTreeView
{
	/// <summary>
	/// Summary description for ShellIcon.  Get a small or large Icon with an easy C# function call
	/// that returns a 32x32 or 16x16 System.Drawing.Icon depending on which function you call
	/// either GetSmallIcon(string fileName) or GetLargeIcon(string fileName)
	/// </summary>
	public class ShellIcon
	{
		#region Internal class
		[Flags]
		internal enum SHGFI : int
		{
			/// <summary>get icon</summary>
			Icon=0x000000100,
			/// <summary>get display name</summary>
			DisplayName=0x000000200,
			/// <summary>get type name</summary>
			TypeName=0x000000400,
			/// <summary>get attributes</summary>
			Attributes=0x000000800,
			/// <summary>get icon location</summary>
			IconLocation=0x000001000,
			/// <summary>return exe type</summary>
			ExeType=0x000002000,
			/// <summary>get system icon index</summary>
			SysIconIndex=0x000004000,
			/// <summary>put a link overlay on icon</summary>
			LinkOverlay=0x000008000,
			/// <summary>show icon in selected state</summary>
			Selected=0x000010000,
			/// <summary>get only specified attributes</summary>
			Attr_Specified=0x000020000,
			/// <summary>get large icon</summary>
			LargeIcon=0x000000000,
			/// <summary>get small icon</summary>
			SmallIcon=0x000000001,
			/// <summary>get open icon</summary>
			OpenIcon=0x000000002,
			/// <summary>get shell size icon</summary>
			ShellIconSize=0x000000004,
			/// <summary>pszPath is a pidl</summary>
			PIDL=0x000000008,
			/// <summary>use passed dwFileAttribute</summary>
			UseFileAttributes=0x000000010,
			/// <summary>apply the appropriate overlays</summary>
			AddOverlays=0x000000020,
			/// <summary>Get the index of the overlay in the upper 8 bits of the iIcon</summary>
			OverlayIndex=0x000000040,
		}

		[StructLayout(LayoutKind.Sequential)]
		internal struct SHFILEINFO
		{
			public IntPtr hIcon;
			public IntPtr iIcon;
			public uint dwAttributes;
			[MarshalAs(UnmanagedType.ByValTStr, SizeConst=260)]
			public string szDisplayName;
			[MarshalAs(UnmanagedType.ByValTStr, SizeConst=80)]
			public string szTypeName;
		};

		internal class Win32
		{
			[DllImport("shell32.dll")]
			public static extern IntPtr SHGetFileInfo(string pszPath, uint dwFileAttributes, ref SHFILEINFO psfi, uint cbSizeFileInfo, uint uFlags);
		}
		#endregion

		#region Constructors
		/// <summary>
		/// 
		/// </summary>
		public ShellIcon()
		{
		}
		#endregion
		
		/// <summary>
		/// 파일의 Small image icon을 구합니다.
		/// </summary>
		/// <param name="fileName">이미지를 구할 파일 이름</param>
		/// <returns>구한 SystemIcon</returns>
		public static Icon GetSmallIcon(string fileName)
		{
			IntPtr hImgSmall; //the handle to the system image list
			SHFILEINFO shinfo = new SHFILEINFO();
			//Use this to get the small Icon
			hImgSmall = Win32.SHGetFileInfo(fileName, 0, ref shinfo, (uint)Marshal.SizeOf(shinfo), (uint)SHGFI.Icon | (uint)SHGFI.SmallIcon);

			//The icon is returned in the hIcon member of the shinfo struct
			return Icon.FromHandle(shinfo.hIcon);
		}

		/// <summary>
		/// 파일의 Large image icon을 구합니다.
		/// </summary>
		/// <param name="fileName">이미지를 구할 파일 이름</param>
		/// <returns>구한 SystemIcon</returns>
		public static Icon GetLargeIcon(string fileName)
		{
			IntPtr hImgLarge; //the handle to the system image list
			SHFILEINFO shinfo = new SHFILEINFO();
			//Use this to get the large Icon
			hImgLarge = Win32.SHGetFileInfo(fileName, 0, ref shinfo, (uint)Marshal.SizeOf(shinfo), (uint)SHGFI.Icon | (uint)SHGFI.LargeIcon);

			//The icon is returned in the hIcon member of the shinfo struct
			return Icon.FromHandle(shinfo.hIcon);
		}

		/// <summary>
		/// 파일의 종류를 구합니다.
		/// </summary>
		/// <param name="fileName">이미지를 구할 파일 이름</param>
		/// <returns>구한 SystemIcon</returns>
		public static string GetFileType(string fileName)
		{
			SHFILEINFO shinfo = new SHFILEINFO();
			Win32.SHGetFileInfo(fileName, 0, ref shinfo, (uint)Marshal.SizeOf(shinfo), (uint)SHGFI.TypeName);

			return shinfo.szTypeName.ToString();
		}

		/// <summary>
		/// 파일의 표시되는 이름을 구합니다.
		/// </summary>
		/// <param name="fileName">이미지를 구할 파일 이름</param>
		/// <returns>구한 SystemIcon</returns>
		public static string GetDisplayName(string fileName)
		{ 
			SHFILEINFO shinfo = new SHFILEINFO();
			Win32.SHGetFileInfo(fileName, 0, ref shinfo, (uint)Marshal.SizeOf(shinfo), (uint)SHGFI.DisplayName);

			return shinfo.szDisplayName.ToString();
		}
	}
}