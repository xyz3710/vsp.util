﻿/**********************************************************************************************************************/
/*	Domain		:	FileTreeView.FileTreeview
/*	Creator		:	KIMKIWON\xyz37(김기원)
/*	Create		:	2010년 3월 30일 화요일 오전 10:30
/*	Purpose		:	
/*--------------------------------------------------------------------------------------------------------------------*/
/*	Modifier	:	
/*	Update		:	
/*	Changes		:	
/*--------------------------------------------------------------------------------------------------------------------*/
/*	Comment		:	
/*--------------------------------------------------------------------------------------------------------------------*/
/*	Reviewer	:	Kim Ki Won
/*	Rev. Date	:	
/**********************************************************************************************************************/

using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Collections;

namespace FileTreeView
{
	/// <summary>
	/// 
	/// </summary>
	class FileTreeview : TreeView
	{
		#region Fields
		private SystemImage _systemImage;
		private bool _showHiddenDirectory;
		private bool _showSystemDirectory;
		#endregion

		/// <summary>
		/// 
		/// </summary>
		public FileTreeview()
		{
			_systemImage = SystemImage.GetInstance();
			ShowDriveOnly();
			//ShowDriveAndTree();
		}

		#region Properties
		public bool ShowHiddenDirectory
		{
			get
			{
				return _showHiddenDirectory;
			}
			set
			{
				_showHiddenDirectory = value;
			}
		}

		public bool ShowSystemDirectory
		{
			get
			{
				return _showSystemDirectory;
			}
			set
			{
				_showSystemDirectory = value;
			}
		}
		#endregion

		private void ShowDriveOnly()
		{
			Cursor saveCursor = Cursor.Current;

			try
			{
				Cursor.Current = Cursors.WaitCursor;

				Nodes.Clear();
				ImageList = _systemImage.SmallImageList;

				foreach (string driveName in Directory.GetLogicalDrives())
				{
					if (Directory.Exists(driveName) == false)
					{
						continue;
					}

					int imageIndex = _systemImage.GetDriveImageIndex(driveName);
					TreeNode newNode = new TreeNode(driveName, imageIndex, imageIndex);
					TreeNodeCollection fileCollection = newNode.Nodes;

					Nodes.Add(newNode);

					if (driveName == @"A:\" || driveName == @"B:\")
					{
						continue;
					}
					else
					{
						AddDirectroyToNode(fileCollection, driveName);
					}
				}
			}
			finally
			{
				Cursor.Current = saveCursor;
			}
		}

		private void ShowDriveAndTree()
		{
			Cursor saveCursor = Cursor.Current;

			try
			{
				Cursor.Current = Cursors.WaitCursor;
				Nodes.Clear();

				ImageList = _systemImage.SmallImageList;

				foreach (string driveName in Directory.GetLogicalDrives())
				{
					if (Directory.Exists(driveName) == false)
					{
						continue;
					}

					int imageIndex = _systemImage.GetDriveImageIndex(driveName);
					TreeNode newNode = new TreeNode(driveName, imageIndex, imageIndex);
					TreeNodeCollection fileCollection = newNode.Nodes;

					Nodes.Add(newNode);

					if (driveName == @"A:\" || driveName == @"B:\")
					{
						continue;
					}
					else
					{
						AddDirectroyToNode(fileCollection, driveName);
					}
				}
			}
			finally
			{
				Cursor.Current = saveCursor;
			}
		}

		/// <summary>
		/// 노드 찾기
		/// </summary>
		/// <param name="nodes"></param>
		/// <param name="subNode"></param>
		/// <returns></returns>
		private bool SearchNode(TreeNodeCollection nodes, TreeNode subNode)
		{
			bool result = true;

			foreach (TreeNode node in nodes)
			{
				if (node.Text == subNode.Text)
				{
					result = false;

					break;
				}
			}

			return result;
		}

		//추가된 노드면 삭제
		private TreeNodeCollection Nodevalue(TreeNodeCollection nodes, TreeNode subNode)
		{
			TreeNodeCollection nodvalue = null;

			foreach (TreeNode node in nodes)
			{
				if (node.Text == subNode.Text)
				{
					node.Nodes.Remove(node);
				}
			}

			return nodvalue;
		}

		private void AddDirectroyToNode(TreeNodeCollection nodes, string driveName)
		{
			DirectoryInfo directoryInfo = new DirectoryInfo(driveName);

			// 전체 directory display
			foreach (DirectoryInfo dirInfo in directoryInfo.GetDirectories("*.*"))
			{
				if ((dirInfo.Attributes & FileAttributes.Directory) != FileAttributes.Directory)
				{
					continue;
				}

				if (ShowHiddenDirectory == false &&
					((dirInfo.Attributes & FileAttributes.Hidden) == FileAttributes.Hidden))
				{
					continue;
				}

				if (ShowSystemDirectory == false &&
					((dirInfo.Attributes & FileAttributes.System) == FileAttributes.System))
				{
					continue;
				}

				int imageIndex = _systemImage.GetSystemImageIndex(dirInfo.FullName);
				TreeNode newNode = new TreeNode(dirInfo.Name, imageIndex, imageIndex);

				Nodevalue(nodes, newNode);
				nodes.Add(InsertNode(newNode, dirInfo.FullName));
			}
		}

		private TreeNode InsertNode(TreeNode node, string dFullname)
		{
			//하위 디렉토리 한번 더 추가
			DirectoryInfo rootDirInfo = new DirectoryInfo(dFullname);

			foreach (DirectoryInfo dirInfo in rootDirInfo.GetDirectories())
			{
				if (dirInfo.Attributes.ToString().StartsWith(FileAttributes.Directory.ToString()))
				{
					int iImageIndex = _systemImage.GetSystemImageIndex(dirInfo.FullName);
					TreeNode childSubNode = new TreeNode(dirInfo.Name, iImageIndex, iImageIndex);

					node.Nodes.Add(childSubNode);
				}
			}

			return node;
		}

		protected override void OnAfterExpand(TreeViewEventArgs e)
		{
			base.OnAfterExpand(e);

			//하위추가
			string l_strChildDir = e.Node.FullPath;
			AddDirectroyToNode(e.Node.Nodes, l_strChildDir);
		}
	}
}
