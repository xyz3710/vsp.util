﻿namespace FileTreeView
{
	/// <summary>
	/// 
	/// </summary>
    partial class WinForm
    {
        /// <summary>
        /// 필수 디자이너 변수입니다.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 사용 중인 모든 리소스를 정리합니다.
        /// </summary>
        /// <param name="disposing">관리되는 리소스를 삭제해야 하면 true이고, 그렇지 않으면 false입니다.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form 디자이너에서 생성한 코드

        /// <summary>
        /// 디자이너 지원에 필요한 메서드입니다.
        /// 이 메서드의 내용을 코드 편집기로 수정하지 마십시오.
        /// </summary>
        private void InitializeComponent()
        {
			this.components = new System.ComponentModel.Container();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(WinForm));
			System.Windows.Forms.TreeNode treeNode1 = new System.Windows.Forms.TreeNode("C:\\", 0, 0);
			System.Windows.Forms.TreeNode treeNode2 = new System.Windows.Forms.TreeNode("D:\\", 1, 1);
			System.Windows.Forms.TreeNode treeNode3 = new System.Windows.Forms.TreeNode("E:\\", 2, 2);
			System.Windows.Forms.TreeNode treeNode4 = new System.Windows.Forms.TreeNode("F:\\", 3, 3);
			System.Windows.Forms.TreeNode treeNode5 = new System.Windows.Forms.TreeNode("Q:\\", 4, 4);
			System.Windows.Forms.TreeNode treeNode6 = new System.Windows.Forms.TreeNode("R:\\", 5, 5);
			System.Windows.Forms.TreeNode treeNode7 = new System.Windows.Forms.TreeNode("S:\\", 6, 6);
			System.Windows.Forms.TreeNode treeNode8 = new System.Windows.Forms.TreeNode("T:\\", 7, 7);
			System.Windows.Forms.TreeNode treeNode9 = new System.Windows.Forms.TreeNode("U:\\", 8, 8);
			System.Windows.Forms.TreeNode treeNode10 = new System.Windows.Forms.TreeNode("V:\\", 9, 9);
			System.Windows.Forms.TreeNode treeNode11 = new System.Windows.Forms.TreeNode("W:\\", 10, 10);
			System.Windows.Forms.TreeNode treeNode12 = new System.Windows.Forms.TreeNode("X:\\", 11, 11);
			System.Windows.Forms.TreeNode treeNode13 = new System.Windows.Forms.TreeNode("Y:\\", 12, 12);
			System.Windows.Forms.TreeNode treeNode14 = new System.Windows.Forms.TreeNode("Z:\\", 13, 13);
			this.menuStrip1 = new System.Windows.Forms.MenuStrip();
			this.파일ToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
			this.종료ToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
			this.프로그램종료ToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
			this.파일ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.종료ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.프로그램종료ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
			this.toolStripButton1 = new System.Windows.Forms.ToolStripButton();
			this.toolStripButton2 = new System.Windows.Forms.ToolStripButton();
			this.toolStripButton3 = new System.Windows.Forms.ToolStripButton();
			this.splitContainer1 = new System.Windows.Forms.SplitContainer();
			this.treeView1 = new FileTreeView.FileTreeview();
			this.listView1 = new FileTreeView.FileListView();
			this.panel1 = new System.Windows.Forms.Panel();
			this.statusStrip1 = new System.Windows.Forms.StatusStrip();
			this.Show_contextMenu = new System.Windows.Forms.ContextMenuStrip(this.components);
			this.큰아이콘ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.큰아이콘ToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
			this.작은아이콘ToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
			this.간단히ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.자세히ToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
			this.menuStrip1.SuspendLayout();
			this.splitContainer1.Panel1.SuspendLayout();
			this.splitContainer1.Panel2.SuspendLayout();
			this.splitContainer1.SuspendLayout();
			this.panel1.SuspendLayout();
			this.Show_contextMenu.SuspendLayout();
			this.SuspendLayout();
			// 
			// menuStrip1
			// 
			this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.파일ToolStripMenuItem1,
            this.종료ToolStripMenuItem1});
			this.menuStrip1.Location = new System.Drawing.Point(0, 0);
			this.menuStrip1.Name = "menuStrip1";
			this.menuStrip1.Size = new System.Drawing.Size(833, 24);
			this.menuStrip1.TabIndex = 0;
			this.menuStrip1.Text = "menuStrip1";
			// 
			// 파일ToolStripMenuItem1
			// 
			this.파일ToolStripMenuItem1.Name = "파일ToolStripMenuItem1";
			this.파일ToolStripMenuItem1.Size = new System.Drawing.Size(43, 20);
			this.파일ToolStripMenuItem1.Text = "파일";
			// 
			// 종료ToolStripMenuItem1
			// 
			this.종료ToolStripMenuItem1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.프로그램종료ToolStripMenuItem1});
			this.종료ToolStripMenuItem1.Name = "종료ToolStripMenuItem1";
			this.종료ToolStripMenuItem1.Size = new System.Drawing.Size(43, 20);
			this.종료ToolStripMenuItem1.Text = "종료";
			// 
			// 프로그램종료ToolStripMenuItem1
			// 
			this.프로그램종료ToolStripMenuItem1.Name = "프로그램종료ToolStripMenuItem1";
			this.프로그램종료ToolStripMenuItem1.Size = new System.Drawing.Size(146, 22);
			this.프로그램종료ToolStripMenuItem1.Text = "프로그램종료";
			this.프로그램종료ToolStripMenuItem1.Click += new System.EventHandler(this.프로그램종료ToolStripMenuItem_Click);
			// 
			// 파일ToolStripMenuItem
			// 
			this.파일ToolStripMenuItem.Name = "파일ToolStripMenuItem";
			this.파일ToolStripMenuItem.Size = new System.Drawing.Size(41, 20);
			this.파일ToolStripMenuItem.Text = "파일";
			// 
			// 종료ToolStripMenuItem
			// 
			this.종료ToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.프로그램종료ToolStripMenuItem});
			this.종료ToolStripMenuItem.Name = "종료ToolStripMenuItem";
			this.종료ToolStripMenuItem.Size = new System.Drawing.Size(41, 20);
			this.종료ToolStripMenuItem.Text = "종료";
			// 
			// 프로그램종료ToolStripMenuItem
			// 
			this.프로그램종료ToolStripMenuItem.Name = "프로그램종료ToolStripMenuItem";
			this.프로그램종료ToolStripMenuItem.Size = new System.Drawing.Size(150, 22);
			this.프로그램종료ToolStripMenuItem.Text = "프로그램 종료";
			this.프로그램종료ToolStripMenuItem.Click += new System.EventHandler(this.프로그램종료ToolStripMenuItem_Click);
			// 
			// toolStripSeparator1
			// 
			this.toolStripSeparator1.Name = "toolStripSeparator1";
			this.toolStripSeparator1.Size = new System.Drawing.Size(6, 25);
			// 
			// toolStripButton1
			// 
			this.toolStripButton1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
			this.toolStripButton1.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton1.Image")));
			this.toolStripButton1.ImageTransparentColor = System.Drawing.Color.Magenta;
			this.toolStripButton1.Name = "toolStripButton1";
			this.toolStripButton1.Size = new System.Drawing.Size(23, 22);
			this.toolStripButton1.Text = "toolStripButton1";
			// 
			// toolStripButton2
			// 
			this.toolStripButton2.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
			this.toolStripButton2.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton2.Image")));
			this.toolStripButton2.ImageTransparentColor = System.Drawing.Color.Magenta;
			this.toolStripButton2.Name = "toolStripButton2";
			this.toolStripButton2.Size = new System.Drawing.Size(23, 22);
			this.toolStripButton2.Text = "toolStripButton2";
			// 
			// toolStripButton3
			// 
			this.toolStripButton3.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
			this.toolStripButton3.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton3.Image")));
			this.toolStripButton3.ImageTransparentColor = System.Drawing.Color.Magenta;
			this.toolStripButton3.Name = "toolStripButton3";
			this.toolStripButton3.Size = new System.Drawing.Size(23, 22);
			this.toolStripButton3.Text = "toolStripButton3";
			// 
			// splitContainer1
			// 
			this.splitContainer1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.splitContainer1.Location = new System.Drawing.Point(0, 24);
			this.splitContainer1.Name = "splitContainer1";
			// 
			// splitContainer1.Panel1
			// 
			this.splitContainer1.Panel1.AutoScroll = true;
			this.splitContainer1.Panel1.Controls.Add(this.treeView1);
			// 
			// splitContainer1.Panel2
			// 
			this.splitContainer1.Panel2.AutoScroll = true;
			this.splitContainer1.Panel2.Controls.Add(this.listView1);
			this.splitContainer1.Panel2.Controls.Add(this.panel1);
			this.splitContainer1.Size = new System.Drawing.Size(833, 564);
			this.splitContainer1.SplitterDistance = 233;
			this.splitContainer1.TabIndex = 5;
			// 
			// treeView1
			// 
			this.treeView1.Cursor = System.Windows.Forms.Cursors.Arrow;
			this.treeView1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.treeView1.ImageIndex = 0;
			this.treeView1.Location = new System.Drawing.Point(0, 0);
			this.treeView1.Name = "treeView1";
			treeNode1.ImageIndex = 0;
			treeNode1.Name = "";
			treeNode1.SelectedImageIndex = 0;
			treeNode1.Text = "C:\\";
			treeNode2.ImageIndex = 1;
			treeNode2.Name = "";
			treeNode2.SelectedImageIndex = 1;
			treeNode2.Text = "D:\\";
			treeNode3.ImageIndex = 2;
			treeNode3.Name = "";
			treeNode3.SelectedImageIndex = 2;
			treeNode3.Text = "E:\\";
			treeNode4.ImageIndex = 3;
			treeNode4.Name = "";
			treeNode4.SelectedImageIndex = 3;
			treeNode4.Text = "F:\\";
			treeNode5.ImageIndex = 4;
			treeNode5.Name = "";
			treeNode5.SelectedImageIndex = 4;
			treeNode5.Text = "Q:\\";
			treeNode6.ImageIndex = 5;
			treeNode6.Name = "";
			treeNode6.SelectedImageIndex = 5;
			treeNode6.Text = "R:\\";
			treeNode7.ImageIndex = 6;
			treeNode7.Name = "";
			treeNode7.SelectedImageIndex = 6;
			treeNode7.Text = "S:\\";
			treeNode8.ImageIndex = 7;
			treeNode8.Name = "";
			treeNode8.SelectedImageIndex = 7;
			treeNode8.Text = "T:\\";
			treeNode9.ImageIndex = 8;
			treeNode9.Name = "";
			treeNode9.SelectedImageIndex = 8;
			treeNode9.Text = "U:\\";
			treeNode10.ImageIndex = 9;
			treeNode10.Name = "";
			treeNode10.SelectedImageIndex = 9;
			treeNode10.Text = "V:\\";
			treeNode11.ImageIndex = 10;
			treeNode11.Name = "";
			treeNode11.SelectedImageIndex = 10;
			treeNode11.Text = "W:\\";
			treeNode12.ImageIndex = 11;
			treeNode12.Name = "";
			treeNode12.SelectedImageIndex = 11;
			treeNode12.Text = "X:\\";
			treeNode13.ImageIndex = 12;
			treeNode13.Name = "";
			treeNode13.SelectedImageIndex = 12;
			treeNode13.Text = "Y:\\";
			treeNode14.ImageIndex = 13;
			treeNode14.Name = "";
			treeNode14.SelectedImageIndex = 13;
			treeNode14.Text = "Z:\\";
			this.treeView1.Nodes.AddRange(new System.Windows.Forms.TreeNode[] {
            treeNode1,
            treeNode2,
            treeNode3,
            treeNode4,
            treeNode5,
            treeNode6,
            treeNode7,
            treeNode8,
            treeNode9,
            treeNode10,
            treeNode11,
            treeNode12,
            treeNode13,
            treeNode14});
			this.treeView1.SelectedImageIndex = 0;
			this.treeView1.ShowHiddenDirectory = false;
			this.treeView1.ShowSystemDirectory = false;
			this.treeView1.Size = new System.Drawing.Size(229, 560);
			this.treeView1.TabIndex = 0;
			this.treeView1.NodeMouseClick += new System.Windows.Forms.TreeNodeMouseClickEventHandler(this.treeView1_NodeMouseClick);
			this.treeView1.AfterExpand += new System.Windows.Forms.TreeViewEventHandler(this.treeView1_AfterExpand_1);
			// 
			// listView1
			// 
			this.listView1.AllowColumnReorder = true;
			this.listView1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.listView1.LabelEdit = true;
			this.listView1.LabelWrap = false;
			this.listView1.Location = new System.Drawing.Point(0, 0);
			this.listView1.MultiSelect = false;
			this.listView1.Name = "listView1";
			this.listView1.Size = new System.Drawing.Size(592, 536);
			this.listView1.TabIndex = 4;
			this.listView1.UseCompatibleStateImageBehavior = false;
			this.listView1.View = System.Windows.Forms.View.Details;
			this.listView1.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.listView1_MouseDoubleClick);
			// 
			// panel1
			// 
			this.panel1.Controls.Add(this.statusStrip1);
			this.panel1.Dock = System.Windows.Forms.DockStyle.Bottom;
			this.panel1.Location = new System.Drawing.Point(0, 536);
			this.panel1.Name = "panel1";
			this.panel1.Size = new System.Drawing.Size(592, 24);
			this.panel1.TabIndex = 0;
			// 
			// statusStrip1
			// 
			this.statusStrip1.Location = new System.Drawing.Point(0, 2);
			this.statusStrip1.Name = "statusStrip1";
			this.statusStrip1.Size = new System.Drawing.Size(592, 22);
			this.statusStrip1.TabIndex = 0;
			this.statusStrip1.Text = "statusStrip1";
			// 
			// Show_contextMenu
			// 
			this.Show_contextMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.큰아이콘ToolStripMenuItem});
			this.Show_contextMenu.Name = "contextMenuStrip1";
			this.Show_contextMenu.Size = new System.Drawing.Size(99, 26);
			// 
			// 큰아이콘ToolStripMenuItem
			// 
			this.큰아이콘ToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.큰아이콘ToolStripMenuItem1,
            this.작은아이콘ToolStripMenuItem1,
            this.간단히ToolStripMenuItem,
            this.자세히ToolStripMenuItem1});
			this.큰아이콘ToolStripMenuItem.Name = "큰아이콘ToolStripMenuItem";
			this.큰아이콘ToolStripMenuItem.Size = new System.Drawing.Size(98, 22);
			this.큰아이콘ToolStripMenuItem.Text = "보기";
			// 
			// 큰아이콘ToolStripMenuItem1
			// 
			this.큰아이콘ToolStripMenuItem1.Name = "큰아이콘ToolStripMenuItem1";
			this.큰아이콘ToolStripMenuItem1.Size = new System.Drawing.Size(138, 22);
			this.큰아이콘ToolStripMenuItem1.Text = "큰 아이콘";
			this.큰아이콘ToolStripMenuItem1.Click += new System.EventHandler(this.큰아이콘ToolStripMenuItem1_Click);
			// 
			// 작은아이콘ToolStripMenuItem1
			// 
			this.작은아이콘ToolStripMenuItem1.Name = "작은아이콘ToolStripMenuItem1";
			this.작은아이콘ToolStripMenuItem1.Size = new System.Drawing.Size(138, 22);
			this.작은아이콘ToolStripMenuItem1.Text = "작은 아이콘";
			this.작은아이콘ToolStripMenuItem1.Click += new System.EventHandler(this.작은아이콘ToolStripMenuItem1_Click);
			// 
			// 간단히ToolStripMenuItem
			// 
			this.간단히ToolStripMenuItem.Name = "간단히ToolStripMenuItem";
			this.간단히ToolStripMenuItem.Size = new System.Drawing.Size(138, 22);
			this.간단히ToolStripMenuItem.Text = "간단히";
			this.간단히ToolStripMenuItem.Click += new System.EventHandler(this.간단히ToolStripMenuItem_Click);
			// 
			// 자세히ToolStripMenuItem1
			// 
			this.자세히ToolStripMenuItem1.Name = "자세히ToolStripMenuItem1";
			this.자세히ToolStripMenuItem1.Size = new System.Drawing.Size(138, 22);
			this.자세히ToolStripMenuItem1.Text = "자세히";
			this.자세히ToolStripMenuItem1.Click += new System.EventHandler(this.자세히ToolStripMenuItem1_Click);
			// 
			// WinForm
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(833, 588);
			this.ContextMenuStrip = this.Show_contextMenu;
			this.Controls.Add(this.splitContainer1);
			this.Controls.Add(this.menuStrip1);
			this.MainMenuStrip = this.menuStrip1;
			this.Name = "WinForm";
			this.Text = "메인화면";
			this.menuStrip1.ResumeLayout(false);
			this.menuStrip1.PerformLayout();
			this.splitContainer1.Panel1.ResumeLayout(false);
			this.splitContainer1.Panel2.ResumeLayout(false);
			this.splitContainer1.ResumeLayout(false);
			this.panel1.ResumeLayout(false);
			this.panel1.PerformLayout();
			this.Show_contextMenu.ResumeLayout(false);
			this.ResumeLayout(false);
			this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem 파일ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 종료ToolStripMenuItem;
        private System.Windows.Forms.ToolStripButton toolStripButton1;
        private System.Windows.Forms.ToolStripButton toolStripButton2;
        private System.Windows.Forms.ToolStripButton toolStripButton3;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripMenuItem 프로그램종료ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 파일ToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem 종료ToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem 프로그램종료ToolStripMenuItem1;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private FileListView listView1;
        private System.Windows.Forms.ContextMenuStrip Show_contextMenu;
        private System.Windows.Forms.ToolStripMenuItem 큰아이콘ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 큰아이콘ToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem 작은아이콘ToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem 간단히ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 자세히ToolStripMenuItem1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private FileTreeview treeView1;
    }
}

