﻿/**********************************************************************************************************************/
/*	Domain		:	FileTreeView.SystemImage
/*	Creator		:	KIMKIWON\xyz37(김기원)
/*	Create		:	2010년 3월 30일 화요일 오전 10:28
/*	Purpose		:	
/*--------------------------------------------------------------------------------------------------------------------*/
/*	Modifier	:	
/*	Update		:	
/*	Changes		:	
/*--------------------------------------------------------------------------------------------------------------------*/
/*	Comment		:	
/*--------------------------------------------------------------------------------------------------------------------*/
/*	Reviewer	:	Kim Ki Won
/*	Rev. Date	:	
/**********************************************************************************************************************/

using System;
using System.Collections.Generic;
using System.Text;
using System.Drawing;
using System.Windows.Forms;
using System.Collections;
using System.IO;

namespace FileTreeView
{
	/// <summary>
	/// 
	/// </summary>
	public class SystemImage
	{
		#region Fields
		private ImageList _smallImageList;
		private ImageList _largeImageList;
		private Dictionary<string, int> _systemIcons;
		#endregion

		#region Use Singleton Pattern
		private static SystemImage _newInstance;

		#region Constructor for Single Ton Pattern
		/// <summary>
		/// SystemImage class의 Single Ton Pattern을 위한 생성자 입니다.
		/// </summary>
		/// <returns></returns>
		private SystemImage()
		{
			_newInstance = null;
			_smallImageList = new ImageList
			{
				ImageSize = new Size(16, 16),
			};
			_largeImageList = new ImageList
			{
				ImageSize = new Size(32, 32),
			};
			_systemIcons = new Dictionary<string, int>();
		}
		#endregion

		#region GetInstance
		/// <summary>
		/// SystemImage class의 Single Ton Pattern을 위한 새 인스턴스를 초기화 합니다.
		/// </summary>
		/// <returns></returns>
		public static SystemImage GetInstance()
		{
			if (_newInstance == null)
			{
				_newInstance = new SystemImage();
			}

			return _newInstance;
		}
		#endregion
		#endregion

		#region Properties
		/// <summary>
		/// SmallImageList를 구하거나 설정합니다.
		/// </summary>
		public ImageList SmallImageList
		{
			get
			{
				return _smallImageList;
			}
			set
			{
				_smallImageList = value;
			}
		}

		/// <summary>
		/// LargeImageList를 구하거나 설정합니다.
		/// </summary>
		public ImageList LargeImageList
		{
			get
			{
				return _largeImageList;
			}
			set
			{
				_largeImageList = value;
			}
		}
		#endregion

		#region Private methods
		private void CollectIconImages(string filePath, string key)
		{
			// _systemIcons에 없으면 추가하고 있으면 return 한다.
			if (_systemIcons.ContainsKey(key) == false)
			{
				Icon smallIcon = ShellIcon.GetSmallIcon(filePath);

				if (smallIcon != null)
				{
#if TEST
					var bmp = smallIcon.ToBitmap();
					bmp.Save($@"j:\ico\{smallIcon.Handle}-small.ico");
#endif
					SmallImageList.Images.Add(ShellIcon.GetSmallIcon(filePath));
				}

				Icon largeIcon = ShellIcon.GetLargeIcon(filePath);

				if (largeIcon != null)
				{
#if TEST
					var bmp = largeIcon.ToBitmap();
					bmp.Save($@"j:\ico\{smallIcon.Handle}-large.ico");
#endif
					LargeImageList.Images.Add(ShellIcon.GetLargeIcon(filePath));
				}

				_systemIcons.Add(key, SmallImageList.Images.Count - 1);
			}
		}
		#endregion

		#region Public methods
		/// <summary>
		/// Directory나 File의 image index를 구합니다.
		/// </summary>
		/// <param name="filePath">구하고자 하는 file의 fullPath</param>
		/// <returns>system image index</returns>
		public int GetSystemImageIndex(string filePath)
		{
			FileAttributes attributes = File.GetAttributes(filePath);
			FileAttributes dirAttr = FileAttributes.Directory;
			string key = string.Empty;

			// Directory icons
			if ((attributes | dirAttr) == dirAttr)
			{
				key = dirAttr.ToString();
			}
			else
			{
				// File icons
				key = Path.GetExtension(filePath);
			}

			CollectIconImages(filePath, key);

			return _systemIcons[key];
		}

		/// <summary>
		/// Drive의 image index를 구합니다.
		/// </summary>
		/// <param name="filePath">구하고자 하는 file의 fullPath</param>
		/// <returns>system image index</returns>
		public int GetDriveImageIndex(string filePath)
		{
			string key = Path.GetPathRoot(filePath);

			CollectIconImages(filePath, key);

			return _systemIcons[key];
		}
		#endregion
	}
}
