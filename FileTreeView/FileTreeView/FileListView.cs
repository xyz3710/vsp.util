﻿/**********************************************************************************************************************/
/*	Domain		:	FileTreeView.FileListView
/*	Creator		:	KIMKIWON\xyz37(김기원)
/*	Create		:	2010년 3월 30일 화요일 오전 10:30
/*	Purpose		:	
/*--------------------------------------------------------------------------------------------------------------------*/
/*	Modifier	:	
/*	Update		:	
/*	Changes		:	
/*--------------------------------------------------------------------------------------------------------------------*/
/*	Comment		:	
/*--------------------------------------------------------------------------------------------------------------------*/
/*	Reviewer	:	Kim Ki Won
/*	Rev. Date	:	
/**********************************************************************************************************************/

using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Drawing;
using System.Collections;
using System.Runtime.InteropServices;

namespace FileTreeView
{
	/// <summary>
	/// 
	/// </summary>
	public class FileListView : ListView
	{
		[DllImport("shell32.dll")]
		private static extern int ShellExecute(int hwnd, string lpOperation, string lpFile, string lpParameters, string lpDirectory, int nShowCmd);

		/// <summary>
		/// 
		/// </summary>
		public const int SW_SHOWNORMAL = 1;

		SystemImage wimage = SystemImage.GetInstance();

		/// <summary>
		/// 
		/// </summary>
		public FileListView()
		{
			SetColumn();
		}

		private void SetColumn()
		{
			MultiSelect = false;

			Clear();
			//listView1.View = View.Details;
			AllowColumnReorder = true;
			LabelEdit = true;
			//listView1.GridLines = true; //그리드 라인
			//listView1.FullRowSelect = true; //Row Full 선택

			ColumnHeader columHeader1 = new ColumnHeader();
			columHeader1.Text = "이름";
			columHeader1.Width = 210;
			ColumnHeader columHeader2 = new ColumnHeader();
			columHeader2.Text = "크기";
			columHeader2.Width = 70;
			columHeader2.TextAlign = HorizontalAlignment.Right;
			ColumnHeader columHeader3 = new ColumnHeader();
			columHeader3.Text = "종류";
			columHeader3.Width = 70;
			columHeader3.TextAlign = HorizontalAlignment.Center;
			ColumnHeader columHeader4 = new ColumnHeader();
			columHeader4.Text = "수정한날짜";
			columHeader4.Width = 100;
			columHeader4.TextAlign = HorizontalAlignment.Center;
			ColumnHeader columHeader5 = new ColumnHeader();
			columHeader5.Text = "속성";
			columHeader5.Width = 70;
			columHeader5.TextAlign = HorizontalAlignment.Center;

			Columns.Add(columHeader1);
			Columns.Add(columHeader2);
			Columns.Add(columHeader3);
			Columns.Add(columHeader4);
			Columns.Add(columHeader5);

			SmallImageList = wimage.SmallImageList;
			LargeImageList = wimage.LargeImageList;
			LargeImageList.ImageSize = new Size(30, 30);
		}

		/// <summary>
		/// 디렉토리 출력
		/// </summary>
		/// <param name="strdriver">상위폴더</param>
		public void ShowDirectoryAndFile(string strdriver)
		{
			this.Cursor = Cursors.WaitCursor;
			Items.Clear();

			WinForm wform = new WinForm();

			if (Directory.Exists(strdriver) == false)
			{
				MessageBox.Show(strdriver + " Error Driver");
				this.Cursor = Cursors.Arrow;
				return;
			}

			//리스트뷰 초기화
			//listView1.Clear();

			DirectoryInfo directoryInfo = new DirectoryInfo(strdriver);

			BeginUpdate();

			//디렉토리 출력
			foreach (DirectoryInfo directoryinfo in directoryInfo.GetDirectories("*"))
			{
				//일반 디렉토리만 표시

				if (directoryinfo.Attributes.ToString().StartsWith(FileAttributes.Directory.ToString()))
				{

					String DisplayName = directoryinfo.Name;
					if (this.View == View.LargeIcon)
					{
						if (DisplayName.Length > 5)
						{
							DisplayName = DisplayName.Substring(0, 5) + "..";
						}
					}
					//ListViewItem Dlistitem = new ListViewItem(directoryinfo.Name);
					ListViewItem Dlistitem = new ListViewItem(DisplayName);
					Dlistitem.Tag = directoryinfo.FullName;
					Dlistitem.SubItems.Add("");
					Dlistitem.SubItems.Add("파일 폴더");
					Dlistitem.SubItems.Add(directoryinfo.LastAccessTime.ToString());
					Dlistitem.SubItems.Add(directoryinfo.Attributes.ToString());
					Dlistitem.ImageIndex = wimage.GetSystemImageIndex(directoryinfo.FullName); //이미지
					Items.Add(Dlistitem);
				}
			}

			//파일출력
			foreach (FileInfo fileinfo in directoryInfo.GetFiles("*.*"))
			{
				String FileName = fileinfo.Name;
				if (this.View == View.LargeIcon)
				{
					if (FileName.Length > 5)
					{
						FileName = FileName.Substring(0, 5) + "..";
					}
				}

				ListViewItem Flistitem = new ListViewItem(FileName);
				Flistitem.Tag = fileinfo.FullName;
				Flistitem.SubItems.Add(String.Format("{0:#,###} KB", fileinfo.Length / 1024));
				Flistitem.SubItems.Add(ShellIcon.GetFileType(fileinfo.FullName));//종류
				Flistitem.SubItems.Add(fileinfo.LastAccessTime.ToString());//마지막수정일
				Flistitem.SubItems.Add(fileinfo.Attributes.ToString());//파일속성
				Flistitem.ImageIndex = wimage.GetSystemImageIndex(fileinfo.FullName); //이미지
				Items.Add(Flistitem);
			}
			EndUpdate();

			this.Cursor = Cursors.Arrow;
		}

		/// <summary>
		/// Raises the <see cref="E:System.Windows.Forms.Control.MouseDoubleClick" /> event.
		/// </summary>
		/// <param name="e">An <see cref="T:System.Windows.Forms.MouseEventArgs" /> that contains the event data.</param>
		protected override void OnMouseDoubleClick(MouseEventArgs e)
		{
			base.OnDoubleClick(e);
			ItemSelect();
		}

		protected override void OnKeyDown(KeyEventArgs e)
		{
			base.OnKeyDown(e);

			if (e.KeyData == Keys.Enter)
			{
				ItemSelect();
			}
		}

		private void ItemSelect()
		{
			if (SelectedItems.Count > 0)
			{
				String strAttributes = File.GetAttributes(SelectedItems[0].Tag.ToString()).ToString();

				if ((strAttributes.Contains(FileAttributes.Directory.ToString())) == true)
				{
					ShowDirectoryAndFile(SelectedItems[0].Tag.ToString());
				}
				else
				{
					string SelectString = SelectedItems[0].Tag.ToString();

					ShellExecute(0, "open", SelectString, null, null, SW_SHOWNORMAL);
				}
			}
		}
	}
}
