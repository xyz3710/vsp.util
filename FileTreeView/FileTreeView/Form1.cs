﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Collections;

using System.Runtime.InteropServices;
using Microsoft.Win32;

namespace FileTreeView
{
    public partial class WinForm : Form
    {
		/// <summary>
		/// 
		/// </summary>
        public WinForm()
        {
            InitializeComponent();
        }

        private void 프로그램종료ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void 작은아이콘ToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            listView1.View = View.SmallIcon;
        }

        private void 큰아이콘ToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            listView1.View = View.LargeIcon;
        }

        private void 간단히ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            listView1.View = View.List;
        }

        private void 자세히ToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            listView1.View = View.Details;
        }
        private void listView1_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            //더블클릭 이벤트
        }

        //트리클릭
        private void treeView1_NodeMouseClick(object sender, TreeNodeMouseClickEventArgs e)
        {
            TreeNode l_tnCurrNode = e.Node;
            listView1.ShowDirectoryAndFile(l_tnCurrNode.FullPath);
        }

        private void treeView1_AfterExpand_1(object sender, TreeViewEventArgs e)
        {
            //노드 확장 이벤트
        }

    

      
    }
}
