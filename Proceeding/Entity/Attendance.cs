﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Proceeding.Entity
{
	/// <summary>
	/// The attendance.
	/// </summary>
	[System.Diagnostics.DebuggerDisplay("Date:{Date}, Attendees:{Attendees}", Name = "Attendance")]
	public class Attendance
	{
		/// <summary>
		/// Gets or sets the date.
		/// </summary>
		public DateTime Date { get; set; }

		/// <summary>
		/// Gets or sets the attendees.
		/// </summary>
		public List<Attendee> Attendees { get; set; }
	}
}
