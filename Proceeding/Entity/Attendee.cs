﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Proceeding.Entity
{
	/// <summary>
	/// The attendee.
	/// </summary>
	[System.Diagnostics.DebuggerDisplay("Title:{Title}, Name:{Name}, RoomNumbers:{RoomNumbers}, SpeakingTime:{SpeakingTime}", Name = "Attendee")]
	public class Attendee
	{
		/// <summary>
		/// Gets or sets the title.
		/// </summary>
		public string Title { get; set; }
		/// <summary>
		/// Gets or sets the name.
		/// </summary>
		public string Name { get; set; }
		/// <summary>
		/// Gets or sets the room numbers.
		/// </summary>
		public string RoomNumbers { get; set; }
		/// <summary>
		/// Gets or sets the speaking time.
		/// </summary>
		public int SpeakingTime { get; set; }
	}
}
