﻿namespace Proceeding
{
	partial class MainForm
	{
		/// <summary>
		/// 필수 디자이너 변수입니다.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// 사용 중인 모든 리소스를 정리합니다.
		/// </summary>
		/// <param name="disposing">관리되는 리소스를 삭제해야 하면 true이고, 그렇지 않으면 false입니다.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form 디자이너에서 생성한 코드

		/// <summary>
		/// 디자이너 지원에 필요한 메서드입니다. 
		/// 이 메서드의 내용을 코드 편집기로 수정하지 마세요.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
			this.tlpMain = new System.Windows.Forms.TableLayoutPanel();
			this.gbRun = new System.Windows.Forms.GroupBox();
			this.tlpProcessed = new System.Windows.Forms.TableLayoutPanel();
			this.pushedButton1 = new ZerOne.Win.Forms.UserControls.PushedButton();
			this.themeButton3 = new ZerOne.Win.Forms.UserControls.ThemeButton();
			this.gbInfo = new System.Windows.Forms.GroupBox();
			this.tlpInfo = new System.Windows.Forms.TableLayoutPanel();
			this.labeledDateTimePicker2 = new ZerOne.Win.Forms.UserControls.LabeledDateTimePicker();
			this.labeledControlBase1 = new ZerOne.Win.Forms.UserControls.LabeledControlBase();
			this.labeledDateTimePicker1 = new ZerOne.Win.Forms.UserControls.LabeledDateTimePicker();
			this.labeledDateTimePicker3 = new ZerOne.Win.Forms.UserControls.LabeledDateTimePicker();
			this.gbLeft = new System.Windows.Forms.GroupBox();
			this.pnlLeft = new System.Windows.Forms.Panel();
			this.tlpListLeft = new System.Windows.Forms.TableLayoutPanel();
			this.themeButton1 = new ZerOne.Win.Forms.UserControls.ThemeButton();
			this.themeButton2 = new ZerOne.Win.Forms.UserControls.ThemeButton();
			this.tlpMain.SuspendLayout();
			this.gbRun.SuspendLayout();
			this.tlpProcessed.SuspendLayout();
			this.gbInfo.SuspendLayout();
			this.tlpInfo.SuspendLayout();
			this.gbLeft.SuspendLayout();
			this.pnlLeft.SuspendLayout();
			this.SuspendLayout();
			// 
			// tlpMain
			// 
			this.tlpMain.ColumnCount = 2;
			this.tlpMain.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
			this.tlpMain.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
			this.tlpMain.Controls.Add(this.gbRun, 1, 0);
			this.tlpMain.Controls.Add(this.gbInfo, 0, 0);
			this.tlpMain.Controls.Add(this.gbLeft, 0, 1);
			this.tlpMain.Dock = System.Windows.Forms.DockStyle.Fill;
			this.tlpMain.Location = new System.Drawing.Point(0, 0);
			this.tlpMain.Margin = new System.Windows.Forms.Padding(0);
			this.tlpMain.Name = "tlpMain";
			this.tlpMain.RowCount = 2;
			this.tlpMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 24.36869F));
			this.tlpMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 75.63131F));
			this.tlpMain.Size = new System.Drawing.Size(935, 926);
			this.tlpMain.TabIndex = 2;
			// 
			// gbRun
			// 
			this.gbRun.Controls.Add(this.tlpProcessed);
			this.gbRun.Dock = System.Windows.Forms.DockStyle.Fill;
			this.gbRun.Location = new System.Drawing.Point(470, 3);
			this.gbRun.Name = "gbRun";
			this.gbRun.Size = new System.Drawing.Size(462, 219);
			this.gbRun.TabIndex = 0;
			this.gbRun.TabStop = false;
			this.gbRun.Text = "회의 도움";
			// 
			// tlpProcessed
			// 
			this.tlpProcessed.ColumnCount = 2;
			this.tlpProcessed.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
			this.tlpProcessed.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
			this.tlpProcessed.Controls.Add(this.pushedButton1, 0, 0);
			this.tlpProcessed.Controls.Add(this.themeButton3, 0, 1);
			this.tlpProcessed.Controls.Add(this.themeButton1, 1, 0);
			this.tlpProcessed.Controls.Add(this.themeButton2, 1, 1);
			this.tlpProcessed.Dock = System.Windows.Forms.DockStyle.Fill;
			this.tlpProcessed.Location = new System.Drawing.Point(3, 23);
			this.tlpProcessed.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
			this.tlpProcessed.Name = "tlpProcessed";
			this.tlpProcessed.RowCount = 2;
			this.tlpProcessed.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
			this.tlpProcessed.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
			this.tlpProcessed.Size = new System.Drawing.Size(456, 193);
			this.tlpProcessed.TabIndex = 0;
			// 
			// pushedButton1
			// 
			this.pushedButton1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(78)))), ((int)(((byte)(135)))), ((int)(((byte)(162)))));
			this.pushedButton1.ForeColor = System.Drawing.Color.White;
			this.pushedButton1.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
			this.pushedButton1.Location = new System.Drawing.Point(3, 3);
			this.pushedButton1.Name = "pushedButton1";
			this.pushedButton1.ParentControl = null;
			this.pushedButton1.PushedColor = System.Drawing.Color.FromArgb(((int)(((byte)(228)))), ((int)(((byte)(63)))), ((int)(((byte)(35)))));
			this.pushedButton1.Size = new System.Drawing.Size(164, 53);
			this.pushedButton1.SuspendPushed = false;
			this.pushedButton1.TabIndex = 1;
			this.pushedButton1.Text = "회의 시작";
			this.pushedButton1.Theme = ZerOne.Win.Forms.UserControls.UserControlThemes.DarkCyan;
			this.pushedButton1.UseVisualStyleBackColor = false;
			// 
			// themeButton3
			// 
			this.themeButton3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(78)))), ((int)(((byte)(135)))), ((int)(((byte)(162)))));
			this.themeButton3.FlatAppearance.BorderSize = 0;
			this.themeButton3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.themeButton3.ForeColor = System.Drawing.Color.White;
			this.themeButton3.Location = new System.Drawing.Point(3, 99);
			this.themeButton3.Name = "themeButton3";
			this.themeButton3.Size = new System.Drawing.Size(137, 53);
			this.themeButton3.TabIndex = 0;
			this.themeButton3.Text = "참석자 목록 복사";
			this.themeButton3.Theme = ZerOne.Win.Forms.UserControls.UserControlThemes.DarkCyan;
			this.themeButton3.UseVisualStyleBackColor = false;
			// 
			// gbInfo
			// 
			this.gbInfo.Controls.Add(this.tlpInfo);
			this.gbInfo.Dock = System.Windows.Forms.DockStyle.Fill;
			this.gbInfo.Location = new System.Drawing.Point(3, 3);
			this.gbInfo.Name = "gbInfo";
			this.gbInfo.Size = new System.Drawing.Size(461, 219);
			this.gbInfo.TabIndex = 1;
			this.gbInfo.TabStop = false;
			this.gbInfo.Text = "회의 정보";
			// 
			// tlpInfo
			// 
			this.tlpInfo.ColumnCount = 1;
			this.tlpInfo.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
			this.tlpInfo.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
			this.tlpInfo.Controls.Add(this.labeledDateTimePicker2, 0, 2);
			this.tlpInfo.Controls.Add(this.labeledControlBase1, 0, 1);
			this.tlpInfo.Controls.Add(this.labeledDateTimePicker1, 0, 0);
			this.tlpInfo.Controls.Add(this.labeledDateTimePicker3, 0, 3);
			this.tlpInfo.Dock = System.Windows.Forms.DockStyle.Fill;
			this.tlpInfo.Location = new System.Drawing.Point(3, 23);
			this.tlpInfo.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
			this.tlpInfo.Name = "tlpInfo";
			this.tlpInfo.RowCount = 4;
			this.tlpInfo.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
			this.tlpInfo.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
			this.tlpInfo.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
			this.tlpInfo.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
			this.tlpInfo.Size = new System.Drawing.Size(455, 193);
			this.tlpInfo.TabIndex = 0;
			// 
			// labeledDateTimePicker2
			// 
			this.labeledDateTimePicker2.BackColor = System.Drawing.Color.White;
			this.labeledDateTimePicker2.DialogResult = System.Windows.Forms.DialogResult.OK;
			this.labeledDateTimePicker2.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
			this.labeledDateTimePicker2.LabelText = "1인당 최대 발언 시간";
			this.labeledDateTimePicker2.LabelWidth = 180;
			this.labeledDateTimePicker2.Location = new System.Drawing.Point(3, 99);
			this.labeledDateTimePicker2.MaxDate = new System.DateTime(9998, 12, 31, 0, 0, 0, 0);
			this.labeledDateTimePicker2.MinDate = new System.DateTime(1753, 1, 1, 0, 0, 0, 0);
			this.labeledDateTimePicker2.Name = "labeledDateTimePicker2";
			this.labeledDateTimePicker2.Size = new System.Drawing.Size(329, 38);
			this.labeledDateTimePicker2.TabIndex = 1;
			this.labeledDateTimePicker2.Theme = ZerOne.Win.Forms.UserControls.UserControlThemes.DarkCyan;
			this.labeledDateTimePicker2.Value = new System.DateTime(2024, 2, 20, 6, 0, 0, 0);
			// 
			// labeledControlBase1
			// 
			this.labeledControlBase1.BackColor = System.Drawing.Color.White;
			this.labeledControlBase1.DialogResult = System.Windows.Forms.DialogResult.OK;
			this.labeledControlBase1.LabelText = "회의 시간";
			this.labeledControlBase1.Location = new System.Drawing.Point(3, 51);
			this.labeledControlBase1.Name = "labeledControlBase1";
			this.labeledControlBase1.Size = new System.Drawing.Size(329, 38);
			this.labeledControlBase1.TabIndex = 0;
			this.labeledControlBase1.Theme = ZerOne.Win.Forms.UserControls.UserControlThemes.DarkCyan;
			// 
			// labeledDateTimePicker1
			// 
			this.labeledDateTimePicker1.BackColor = System.Drawing.Color.White;
			this.labeledDateTimePicker1.DialogResult = System.Windows.Forms.DialogResult.OK;
			this.labeledDateTimePicker1.Format = System.Windows.Forms.DateTimePickerFormat.Long;
			this.labeledDateTimePicker1.LabelText = "회의 날짜";
			this.labeledDateTimePicker1.Location = new System.Drawing.Point(4, 5);
			this.labeledDateTimePicker1.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
			this.labeledDateTimePicker1.MaxDate = new System.DateTime(9998, 12, 31, 0, 0, 0, 0);
			this.labeledDateTimePicker1.MinDate = new System.DateTime(1753, 1, 1, 0, 0, 0, 0);
			this.labeledDateTimePicker1.Name = "labeledDateTimePicker1";
			this.labeledDateTimePicker1.Size = new System.Drawing.Size(328, 38);
			this.labeledDateTimePicker1.TabIndex = 0;
			this.labeledDateTimePicker1.Theme = ZerOne.Win.Forms.UserControls.UserControlThemes.DarkCyan;
			this.labeledDateTimePicker1.Value = new System.DateTime(2024, 2, 20, 20, 34, 13, 985);
			// 
			// labeledDateTimePicker3
			// 
			this.labeledDateTimePicker3.BackColor = System.Drawing.Color.White;
			this.labeledDateTimePicker3.DialogResult = System.Windows.Forms.DialogResult.OK;
			this.labeledDateTimePicker3.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
			this.labeledDateTimePicker3.LabelText = "1인 발언 중간 알림";
			this.labeledDateTimePicker3.LabelWidth = 180;
			this.labeledDateTimePicker3.Location = new System.Drawing.Point(3, 147);
			this.labeledDateTimePicker3.MaxDate = new System.DateTime(9998, 12, 31, 0, 0, 0, 0);
			this.labeledDateTimePicker3.MinDate = new System.DateTime(1753, 1, 1, 0, 0, 0, 0);
			this.labeledDateTimePicker3.Name = "labeledDateTimePicker3";
			this.labeledDateTimePicker3.Size = new System.Drawing.Size(329, 38);
			this.labeledDateTimePicker3.TabIndex = 1;
			this.labeledDateTimePicker3.Theme = ZerOne.Win.Forms.UserControls.UserControlThemes.DarkCyan;
			this.labeledDateTimePicker3.Value = new System.DateTime(2024, 2, 20, 3, 0, 0, 0);
			// 
			// gbLeft
			// 
			this.gbLeft.Controls.Add(this.pnlLeft);
			this.gbLeft.Dock = System.Windows.Forms.DockStyle.Fill;
			this.gbLeft.Location = new System.Drawing.Point(3, 228);
			this.gbLeft.Name = "gbLeft";
			this.gbLeft.Size = new System.Drawing.Size(461, 695);
			this.gbLeft.TabIndex = 2;
			this.gbLeft.TabStop = false;
			this.gbLeft.Text = "참석자 목록";
			// 
			// pnlLeft
			// 
			this.pnlLeft.AutoScroll = true;
			this.pnlLeft.Controls.Add(this.tlpListLeft);
			this.pnlLeft.Dock = System.Windows.Forms.DockStyle.Fill;
			this.pnlLeft.Location = new System.Drawing.Point(3, 23);
			this.pnlLeft.Name = "pnlLeft";
			this.pnlLeft.Size = new System.Drawing.Size(455, 669);
			this.pnlLeft.TabIndex = 0;
			// 
			// tlpListLeft
			// 
			this.tlpListLeft.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
			this.tlpListLeft.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
			this.tlpListLeft.ColumnCount = 1;
			this.tlpListLeft.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
			this.tlpListLeft.Dock = System.Windows.Forms.DockStyle.Top;
			this.tlpListLeft.Location = new System.Drawing.Point(0, 0);
			this.tlpListLeft.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
			this.tlpListLeft.Name = "tlpListLeft";
			this.tlpListLeft.RowCount = 15;
			this.tlpListLeft.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 6.666667F));
			this.tlpListLeft.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 6.666667F));
			this.tlpListLeft.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 6.666667F));
			this.tlpListLeft.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 6.666667F));
			this.tlpListLeft.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 6.666667F));
			this.tlpListLeft.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 6.666667F));
			this.tlpListLeft.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 6.666667F));
			this.tlpListLeft.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 6.666667F));
			this.tlpListLeft.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 6.666667F));
			this.tlpListLeft.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 6.666667F));
			this.tlpListLeft.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 6.666667F));
			this.tlpListLeft.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 6.666667F));
			this.tlpListLeft.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 6.666667F));
			this.tlpListLeft.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 6.666667F));
			this.tlpListLeft.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 6.666667F));
			this.tlpListLeft.Size = new System.Drawing.Size(438, 1240);
			this.tlpListLeft.TabIndex = 0;
			// 
			// themeButton1
			// 
			this.themeButton1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(78)))), ((int)(((byte)(135)))), ((int)(((byte)(162)))));
			this.themeButton1.FlatAppearance.BorderSize = 0;
			this.themeButton1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.themeButton1.ForeColor = System.Drawing.Color.White;
			this.themeButton1.Image = global::Proceeding.Properties.Resources.Settings32;
			this.themeButton1.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
			this.themeButton1.Location = new System.Drawing.Point(231, 3);
			this.themeButton1.Name = "themeButton1";
			this.themeButton1.Size = new System.Drawing.Size(137, 53);
			this.themeButton1.TabIndex = 0;
			this.themeButton1.Text = "환경 설정";
			this.themeButton1.Theme = ZerOne.Win.Forms.UserControls.UserControlThemes.DarkCyan;
			this.themeButton1.UseVisualStyleBackColor = false;
			// 
			// themeButton2
			// 
			this.themeButton2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(78)))), ((int)(((byte)(135)))), ((int)(((byte)(162)))));
			this.themeButton2.FlatAppearance.BorderSize = 0;
			this.themeButton2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.themeButton2.ForeColor = System.Drawing.Color.White;
			this.themeButton2.Image = global::Proceeding.Properties.Resources.Close32;
			this.themeButton2.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
			this.themeButton2.Location = new System.Drawing.Point(231, 99);
			this.themeButton2.Name = "themeButton2";
			this.themeButton2.Size = new System.Drawing.Size(137, 53);
			this.themeButton2.TabIndex = 0;
			this.themeButton2.Text = "종    료";
			this.themeButton2.Theme = ZerOne.Win.Forms.UserControls.UserControlThemes.DarkCyan;
			this.themeButton2.UseVisualStyleBackColor = false;
			// 
			// MainForm
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(935, 926);
			this.Controls.Add(this.tlpMain);
			this.Font = new System.Drawing.Font("맑은 고딕", 11F);
			this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
			this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
			this.Name = "MainForm";
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Text = "회의록";
			this.tlpMain.ResumeLayout(false);
			this.gbRun.ResumeLayout(false);
			this.tlpProcessed.ResumeLayout(false);
			this.gbInfo.ResumeLayout(false);
			this.tlpInfo.ResumeLayout(false);
			this.gbLeft.ResumeLayout(false);
			this.pnlLeft.ResumeLayout(false);
			this.ResumeLayout(false);

		}

		#endregion

		private System.Windows.Forms.TableLayoutPanel tlpMain;
		private System.Windows.Forms.TableLayoutPanel tlpInfo;
		private System.Windows.Forms.TableLayoutPanel tlpProcessed;
		private System.Windows.Forms.TableLayoutPanel tlpListLeft;
		private ZerOne.Win.Forms.UserControls.LabeledDateTimePicker labeledDateTimePicker1;
		private ZerOne.Win.Forms.UserControls.LabeledDateTimePicker labeledDateTimePicker2;
		private ZerOne.Win.Forms.UserControls.LabeledControlBase labeledControlBase1;
		private System.Windows.Forms.GroupBox gbInfo;
		private ZerOne.Win.Forms.UserControls.LabeledDateTimePicker labeledDateTimePicker3;
		private System.Windows.Forms.GroupBox gbRun;
		private ZerOne.Win.Forms.UserControls.PushedButton pushedButton1;
		private ZerOne.Win.Forms.UserControls.ThemeButton themeButton3;
		private ZerOne.Win.Forms.UserControls.ThemeButton themeButton1;
		private ZerOne.Win.Forms.UserControls.ThemeButton themeButton2;
		private System.Windows.Forms.GroupBox gbLeft;
		private System.Windows.Forms.Panel pnlLeft;
	}
}

