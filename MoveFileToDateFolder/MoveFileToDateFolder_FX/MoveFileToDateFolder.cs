using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;
using System.Windows.Forms;

public class MoveFileToDateFolder
{
	private static Regex regex = new Regex(":", RegexOptions.Compiled);
	private const string FOLDER_DATE_FORMAT = "yyyy-MM-dd";

	/// <summary>
	/// Retrieves the picture created datetime WITHOUT loading the whole image.
	/// </summary>
	/// <remarks>https://stackoverflow.com/a/7713780/929740</remarks>
	/// <param name="path"></param>
	/// <returns></returns>
	internal DateTime? GetTakenDateFromImage(string path)
	{
		using (FileStream fs = new FileStream(path, FileMode.Open, FileAccess.Read))
		{
			try
			{
				using (Image image = Image.FromStream(fs, false, false))
				{
					var propItem = image.GetPropertyItem(36867);
					var dateTaken = regex.Replace(Encoding.UTF8.GetString(propItem.Value), "-", 2);

					return DateTime.Parse(dateTaken);
				}
			}
			catch
			{
				return null;
			}
		}
	}

	/// <summary>
	/// Runs the specified base folder.
	/// </summary>
	/// <param name="baseFolder">The base folder.</param>
	/// <param name="searchPatterns">The search pattern.</param>
	/// <param name="searchOption">The search option.</param>
	/// <param name="seperateByTakenDate">if set to <c>true</c> [seperate by taken date].</param>
	/// <returns></returns>
	public int Run(string baseFolder,
		string searchPatterns,
		SearchOption searchOption,
		bool seperateByTakenDate = true,
		Action<string[]> afterGetFilesAction = null,
		Action<string> afterMovedAction = null)
	{
		var files = new List<string>();

		if (searchPatterns.Contains(";") == true)
		{
			foreach (string searchPattern in searchPatterns.Split(';'))
			{
				var tempFiles = Directory.GetFiles(baseFolder, searchPattern, searchOption);

				files.AddRange(tempFiles);

				if (afterGetFilesAction != null)
				{
					try
					{
						afterGetFilesAction(tempFiles);
					}
					catch
					{

					}
				}
			}
		}

		int result = 0;
		string folderName = string.Empty;

		foreach (string file in files)
		{
			if (seperateByTakenDate == true)
			{
				var takenDate = GetTakenDateFromImage(file);

				if (takenDate == null)
				{
					folderName = GetLastWriteTimeDateString(file);
				}
				else
				{
					folderName = takenDate.Value.ToString(FOLDER_DATE_FORMAT);
				}
			}
			else
			{
				folderName = GetLastWriteTimeDateString(file);
			}

			var yearName = folderName.Substring(0, 4);
			var folderPath = $@"{baseFolder}\{yearName}\{folderName}";

			if (Directory.Exists(folderPath) == false)
			{
				Directory.CreateDirectory(folderPath);
			}

			var target = $@"{folderPath}\{Path.GetFileName(file)}";

			if (File.Exists(target) == true)
			{
				target = SeqFilename(folderPath, target);
			}

			Console.WriteLine($@"{folderName}\{yearName}\{Path.GetFileName(file)}");

			try
			{
				File.Move(file, target);

				if (afterMovedAction != null)
				{
					afterMovedAction(target);
				}
			}
			catch (Exception ex)
			{
				Console.WriteLine("\t{0}", ex.Message);
			}

			result++;
		}

		return result;
	}

	private string GetLastWriteTimeDateString(string file)
	{
		var fileInfo = new FileInfo(file);

		return fileInfo.LastWriteTime.ToString(FOLDER_DATE_FORMAT);
	}

	private string SeqFilename(string folderPath, string target)
	{
		var newPath = target;
		var fileNameOnly = Path.GetFileNameWithoutExtension(target);
		var ext = Path.GetExtension(target);
		var count = Directory.GetFiles(folderPath, string.Format("{0}*{1}", fileNameOnly, ext), SearchOption.TopDirectoryOnly).Length;

		if (count > 0)
		{
			newPath = $@"{folderPath}\{string.Format("{0}_{1}", fileNameOnly, count + 1)}{ext}";
		}

		return newPath;
	}
}

public class Test
{
	public static int Main(string[] args)
	{
		int result = -1;

		if (args.Length == 0)
		{
			ShowUsages();

			return result;
		}
		else if (args.Length >= 1 && args.Length <= 3)
		{
			string baseFolder = string.Empty;
			string searchPattern = "*.*";
			SearchOption searchOption = SearchOption.TopDirectoryOnly;

			if (args.Length >= 1 && args.Length <= 3)
			{
				foreach (string arg in args)
				{
					string upperOption = arg.ToUpper();
					int argIndexOf = arg.IndexOf('.');

					if (upperOption[0] == '/' && upperOption[1] == 'S')
					{
						searchOption = SearchOption.AllDirectories;
					}
					else if (baseFolder == string.Empty
						&& (arg.IndexOf(Path.DirectorySeparatorChar) != -1 || arg == "."))
					{
						baseFolder = arg;
					}
					else if (arg.Length > 1 && argIndexOf != -1)
					{
						if (argIndexOf == 0)
						{
							searchPattern = "*" + arg;
						}
						else
						{
							searchPattern = arg;
						}
					}
				}
			}

			if (baseFolder == "." || baseFolder == string.Empty)
			{
				baseFolder = Environment.CurrentDirectory;
			}

			if (Directory.Exists(baseFolder) == false)
			{
				Console.WriteLine("{0}Folder does not exists.", Environment.NewLine);

				return result;
			}

			#region Debug
#if DEBUG
			Console.WriteLine("baseFolder : {0}", baseFolder);
			Console.WriteLine("searchOption : {0}", searchOption);
			Console.WriteLine("searchPattern : {0}", searchPattern);
#endif
			#endregion

			MoveFileToDateFolder moveFileToDateFolder = new MoveFileToDateFolder();

			result = moveFileToDateFolder.Run(baseFolder, searchPattern, searchOption);
		}

		return result;
	}

	private static void ShowUsages()
	{
		string appName = Path.GetFileName(Application.ExecutablePath);

		Console.WriteLine("");
		Console.WriteLine("    Move file to it's own modified date folder name.{0}", Environment.NewLine);
		Console.WriteLine("    {0} <Target Folder> [Search Pattern] [Option]", appName);
		Console.WriteLine("    \tOption : /S : Action to subdirectory{0}", Environment.NewLine);
		Console.WriteLine("    \tex) : {0} . /s", appName);
		Console.WriteLine("    \t      Action to current folder and sub folders");
	}
}
