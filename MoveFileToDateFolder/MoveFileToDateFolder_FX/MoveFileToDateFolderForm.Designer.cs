﻿namespace MoveFileToDateFolderForm
{
	partial class MoveFileToDateFolderForm
	{
		/// <summary>
		/// 필수 디자이너 변수입니다.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// 사용 중인 모든 리소스를 정리합니다.
		/// </summary>
		/// <param name="disposing">관리되는 리소스를 삭제해야 하면 true이고, 그렇지 않으면 false입니다.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form 디자이너에서 생성한 코드

		/// <summary>
		/// 디자이너 지원에 필요한 메서드입니다. 
		/// 이 메서드의 내용을 코드 편집기로 수정하지 마세요.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MoveFileToDateFolderForm));
			this.groupBox1 = new System.Windows.Forms.GroupBox();
			this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
			this.tbPatterns = new System.Windows.Forms.TextBox();
			this.cbCreatedDate = new System.Windows.Forms.CheckBox();
			this.cbSubfolder = new System.Windows.Forms.CheckBox();
			this.tbTargetFolder = new System.Windows.Forms.TextBox();
			this.btnFolder = new System.Windows.Forms.Button();
			this.btnRun = new System.Windows.Forms.Button();
			this.label1 = new System.Windows.Forms.Label();
			this.tlpMain = new System.Windows.Forms.TableLayoutPanel();
			this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
			this.tbTargets = new System.Windows.Forms.TextBox();
			this.tbResults = new System.Windows.Forms.TextBox();
			this.lbFromCount = new System.Windows.Forms.Label();
			this.lbToCount = new System.Windows.Forms.Label();
			this.fbdTarget = new System.Windows.Forms.FolderBrowserDialog();
			this.groupBox1.SuspendLayout();
			this.tableLayoutPanel1.SuspendLayout();
			this.tlpMain.SuspendLayout();
			this.tableLayoutPanel2.SuspendLayout();
			this.SuspendLayout();
			// 
			// groupBox1
			// 
			this.groupBox1.Controls.Add(this.tableLayoutPanel1);
			this.groupBox1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.groupBox1.Location = new System.Drawing.Point(3, 3);
			this.groupBox1.Name = "groupBox1";
			this.groupBox1.Size = new System.Drawing.Size(1180, 99);
			this.groupBox1.TabIndex = 0;
			this.groupBox1.TabStop = false;
			this.groupBox1.Text = "대상 폴더 및 파일";
			// 
			// tableLayoutPanel1
			// 
			this.tableLayoutPanel1.ColumnCount = 5;
			this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 168F));
			this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 106F));
			this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
			this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 120F));
			this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 59F));
			this.tableLayoutPanel1.Controls.Add(this.tbPatterns, 2, 1);
			this.tableLayoutPanel1.Controls.Add(this.cbCreatedDate, 0, 1);
			this.tableLayoutPanel1.Controls.Add(this.cbSubfolder, 0, 0);
			this.tableLayoutPanel1.Controls.Add(this.tbTargetFolder, 1, 0);
			this.tableLayoutPanel1.Controls.Add(this.btnFolder, 4, 0);
			this.tableLayoutPanel1.Controls.Add(this.btnRun, 3, 1);
			this.tableLayoutPanel1.Controls.Add(this.label1, 1, 1);
			this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.tableLayoutPanel1.Location = new System.Drawing.Point(3, 23);
			this.tableLayoutPanel1.Margin = new System.Windows.Forms.Padding(0);
			this.tableLayoutPanel1.Name = "tableLayoutPanel1";
			this.tableLayoutPanel1.RowCount = 2;
			this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
			this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
			this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
			this.tableLayoutPanel1.Size = new System.Drawing.Size(1174, 73);
			this.tableLayoutPanel1.TabIndex = 0;
			// 
			// tbPatterns
			// 
			this.tbPatterns.Dock = System.Windows.Forms.DockStyle.Fill;
			this.tbPatterns.Font = new System.Drawing.Font("맑은 고딕", 12F);
			this.tbPatterns.Location = new System.Drawing.Point(277, 39);
			this.tbPatterns.Name = "tbPatterns";
			this.tbPatterns.Size = new System.Drawing.Size(715, 29);
			this.tbPatterns.TabIndex = 4;
			this.tbPatterns.Text = "*.jpg;*.jpeg;*.png;*.gif;*.tif;*.tiff;*.mov;*.avi;*.heic;*.mp4";
			// 
			// cbCreatedDate
			// 
			this.cbCreatedDate.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
			this.cbCreatedDate.Checked = true;
			this.cbCreatedDate.CheckState = System.Windows.Forms.CheckState.Checked;
			this.cbCreatedDate.Dock = System.Windows.Forms.DockStyle.Fill;
			this.cbCreatedDate.Location = new System.Drawing.Point(3, 39);
			this.cbCreatedDate.Name = "cbCreatedDate";
			this.cbCreatedDate.Padding = new System.Windows.Forms.Padding(11, 0, 11, 0);
			this.cbCreatedDate.Size = new System.Drawing.Size(162, 31);
			this.cbCreatedDate.TabIndex = 3;
			this.cbCreatedDate.Text = "촬영 일자 기준";
			this.cbCreatedDate.UseVisualStyleBackColor = true;
			// 
			// cbSubfolder
			// 
			this.cbSubfolder.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
			this.cbSubfolder.Checked = true;
			this.cbSubfolder.CheckState = System.Windows.Forms.CheckState.Checked;
			this.cbSubfolder.Dock = System.Windows.Forms.DockStyle.Fill;
			this.cbSubfolder.Location = new System.Drawing.Point(3, 3);
			this.cbSubfolder.Name = "cbSubfolder";
			this.cbSubfolder.Padding = new System.Windows.Forms.Padding(11, 0, 11, 0);
			this.cbSubfolder.Size = new System.Drawing.Size(162, 30);
			this.cbSubfolder.TabIndex = 0;
			this.cbSubfolder.Text = "하위 폴더 포함";
			this.cbSubfolder.UseVisualStyleBackColor = true;
			// 
			// tbTargetFolder
			// 
			this.tableLayoutPanel1.SetColumnSpan(this.tbTargetFolder, 3);
			this.tbTargetFolder.Dock = System.Windows.Forms.DockStyle.Fill;
			this.tbTargetFolder.Font = new System.Drawing.Font("맑은 고딕", 12F);
			this.tbTargetFolder.Location = new System.Drawing.Point(171, 3);
			this.tbTargetFolder.Name = "tbTargetFolder";
			this.tbTargetFolder.Size = new System.Drawing.Size(941, 29);
			this.tbTargetFolder.TabIndex = 1;
			// 
			// btnFolder
			// 
			this.btnFolder.Dock = System.Windows.Forms.DockStyle.Fill;
			this.btnFolder.Location = new System.Drawing.Point(1118, 3);
			this.btnFolder.Name = "btnFolder";
			this.btnFolder.Size = new System.Drawing.Size(53, 30);
			this.btnFolder.TabIndex = 2;
			this.btnFolder.Text = "...";
			this.btnFolder.UseVisualStyleBackColor = true;
			// 
			// btnRun
			// 
			this.tableLayoutPanel1.SetColumnSpan(this.btnRun, 2);
			this.btnRun.Location = new System.Drawing.Point(998, 39);
			this.btnRun.Name = "btnRun";
			this.btnRun.Size = new System.Drawing.Size(173, 31);
			this.btnRun.TabIndex = 5;
			this.btnRun.Text = "정    리(&R)";
			this.btnRun.UseVisualStyleBackColor = true;
			// 
			// label1
			// 
			this.label1.AutoSize = true;
			this.label1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.label1.Location = new System.Drawing.Point(171, 36);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(100, 37);
			this.label1.TabIndex = 4;
			this.label1.Text = "검색 패턴 :";
			this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			// 
			// tlpMain
			// 
			this.tlpMain.ColumnCount = 1;
			this.tlpMain.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
			this.tlpMain.Controls.Add(this.groupBox1, 0, 0);
			this.tlpMain.Controls.Add(this.tableLayoutPanel2, 0, 1);
			this.tlpMain.Dock = System.Windows.Forms.DockStyle.Fill;
			this.tlpMain.Location = new System.Drawing.Point(0, 0);
			this.tlpMain.Name = "tlpMain";
			this.tlpMain.RowCount = 2;
			this.tlpMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 105F));
			this.tlpMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
			this.tlpMain.Size = new System.Drawing.Size(1186, 742);
			this.tlpMain.TabIndex = 1;
			// 
			// tableLayoutPanel2
			// 
			this.tableLayoutPanel2.ColumnCount = 2;
			this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
			this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
			this.tableLayoutPanel2.Controls.Add(this.tbTargets, 0, 1);
			this.tableLayoutPanel2.Controls.Add(this.tbResults, 1, 1);
			this.tableLayoutPanel2.Controls.Add(this.lbFromCount, 0, 0);
			this.tableLayoutPanel2.Controls.Add(this.lbToCount, 1, 0);
			this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
			this.tableLayoutPanel2.Location = new System.Drawing.Point(0, 105);
			this.tableLayoutPanel2.Margin = new System.Windows.Forms.Padding(0);
			this.tableLayoutPanel2.Name = "tableLayoutPanel2";
			this.tableLayoutPanel2.RowCount = 2;
			this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 32F));
			this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
			this.tableLayoutPanel2.Size = new System.Drawing.Size(1186, 637);
			this.tableLayoutPanel2.TabIndex = 1;
			// 
			// tbTargets
			// 
			this.tbTargets.Dock = System.Windows.Forms.DockStyle.Fill;
			this.tbTargets.Location = new System.Drawing.Point(3, 35);
			this.tbTargets.Multiline = true;
			this.tbTargets.Name = "tbTargets";
			this.tbTargets.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
			this.tbTargets.Size = new System.Drawing.Size(587, 599);
			this.tbTargets.TabIndex = 0;
			// 
			// tbResults
			// 
			this.tbResults.Dock = System.Windows.Forms.DockStyle.Fill;
			this.tbResults.Location = new System.Drawing.Point(596, 35);
			this.tbResults.Multiline = true;
			this.tbResults.Name = "tbResults";
			this.tbResults.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
			this.tbResults.Size = new System.Drawing.Size(587, 599);
			this.tbResults.TabIndex = 0;
			// 
			// lbFromCount
			// 
			this.lbFromCount.AutoSize = true;
			this.lbFromCount.Dock = System.Windows.Forms.DockStyle.Fill;
			this.lbFromCount.Location = new System.Drawing.Point(3, 0);
			this.lbFromCount.Name = "lbFromCount";
			this.lbFromCount.Size = new System.Drawing.Size(587, 32);
			this.lbFromCount.TabIndex = 1;
			this.lbFromCount.Text = "From: 0건";
			this.lbFromCount.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			// 
			// lbToCount
			// 
			this.lbToCount.AutoSize = true;
			this.lbToCount.Dock = System.Windows.Forms.DockStyle.Fill;
			this.lbToCount.Location = new System.Drawing.Point(596, 0);
			this.lbToCount.Name = "lbToCount";
			this.lbToCount.Size = new System.Drawing.Size(587, 32);
			this.lbToCount.TabIndex = 1;
			this.lbToCount.Text = "To: 0건";
			this.lbToCount.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			// 
			// fbdTarget
			// 
			this.fbdTarget.Description = "정리할 폴더를 선택 합니다.";
			this.fbdTarget.ShowNewFolderButton = false;
			// 
			// MoveFileToDateFolderForm
			// 
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
			this.ClientSize = new System.Drawing.Size(1186, 742);
			this.Controls.Add(this.tlpMain);
			this.Font = new System.Drawing.Font("맑은 고딕", 11F);
			this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
			this.Margin = new System.Windows.Forms.Padding(4, 6, 4, 6);
			this.Name = "MoveFileToDateFolderForm";
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Text = "Move file to it\'s own modified date folder name.";
			this.groupBox1.ResumeLayout(false);
			this.tableLayoutPanel1.ResumeLayout(false);
			this.tableLayoutPanel1.PerformLayout();
			this.tlpMain.ResumeLayout(false);
			this.tableLayoutPanel2.ResumeLayout(false);
			this.tableLayoutPanel2.PerformLayout();
			this.ResumeLayout(false);

		}

		#endregion

		private System.Windows.Forms.GroupBox groupBox1;
		private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
		private System.Windows.Forms.CheckBox cbSubfolder;
		private System.Windows.Forms.TextBox tbTargetFolder;
		private System.Windows.Forms.TableLayoutPanel tlpMain;
		private System.Windows.Forms.Button btnFolder;
		private System.Windows.Forms.CheckBox cbCreatedDate;
		private System.Windows.Forms.Button btnRun;
		private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
		private System.Windows.Forms.TextBox tbTargets;
		private System.Windows.Forms.TextBox tbResults;
		private System.Windows.Forms.FolderBrowserDialog fbdTarget;
		private System.Windows.Forms.TextBox tbPatterns;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Label lbFromCount;
		private System.Windows.Forms.Label lbToCount;
	}
}

