﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MoveFileToDateFolderForm
{
	static class Program
	{
		[System.Runtime.InteropServices.DllImport("kernel32.dll")]
		static extern bool AttachConsole(int dwProcessId);
		private const int ATTACH_PARENT_PROCESS = -1;

		/// <summary>
		/// 해당 응용 프로그램의 주 진입점입니다.
		/// </summary>
		[STAThread]
		static int Main(string[] args)
		{
			int result = -1;

			if (args == null || args.Length == 0)
			{
				InitForm();

				return Return(result);
			}

			// 콘솔로 아웃풋이 나오지만 마지막 실행파일 자신을 이동할 때는 IO 오류가 발생해서 
			// 마지막 return 문처럼 처리를 해야 한다.
			AttachConsole(ATTACH_PARENT_PROCESS);

			if (args.Length == 1)
			{
				ShowUsages();

				return Return(result);
			}

			if (args.Length > 0 && args.Length <= 3)
			{
				string baseFolder = string.Empty;
				string searchPattern = "*.*";
				var searchOption = SearchOption.TopDirectoryOnly;

				if (args.Length > 0 && args.Length <= 3)
				{
					foreach (string arg in args)
					{
						string upperOption = arg.ToUpper();
						int argIndexOf = arg.IndexOf('.');

						if (upperOption[0] == '/' && upperOption[1] == 'S')
						{
							searchOption = SearchOption.AllDirectories;
						}
						else if (baseFolder == string.Empty
							&& (arg.IndexOf(Path.DirectorySeparatorChar) != -1 || arg == "."))
						{
							baseFolder = arg;
						}
						else if (arg.Length > 1 && argIndexOf != -1)
						{
							if (argIndexOf == 0)
							{
								searchPattern = "*" + arg;
							}
							else
							{
								searchPattern = arg;
							}
						}
					}
				}
				else
				{
					ShowUsages();

					return Return(result);
				}

				if (baseFolder == "." || baseFolder == string.Empty)
				{
					baseFolder = Environment.CurrentDirectory;
				}

				if (Directory.Exists(baseFolder) == false)
				{
					Console.WriteLine("{0}Folder does not exists.", Environment.NewLine);

					return Return(result);
				}

				#region Debug
#if DEBUG
				Console.WriteLine("BseFolder    : {0}", baseFolder);
				Console.WriteLine("SearchOption : {0}", searchOption);
				Console.WriteLine("SearchPattern: {0}", searchPattern);
#endif
				#endregion

				MoveFileToDateFolder moveFileToDateFolder = new MoveFileToDateFolder();

				result = moveFileToDateFolder.Run(baseFolder, searchPattern, searchOption);
			}

			try
			{
			}
			finally
			{
				System.Windows.Forms.SendKeys.SendWait("{ENTER}");
				Application.Exit();
			}

			return result;
		}

		private static void ShowUsages()
		{
			string appName = Path.GetFileName(Application.ExecutablePath);

			Console.WriteLine("");
			Console.WriteLine("    Move file to it's own modified date folder name.{0}", Environment.NewLine);
			Console.WriteLine("    {0} <Target Folder> [Search Pattern] [Option]", appName);
			Console.WriteLine("    Search Pattern: *.jpg;*.png");
			Console.WriteLine("    \tOption : /S : Action to subdirectory{0}", Environment.NewLine);
			Console.WriteLine("    \tex) : {0} . /s", appName);
			Console.WriteLine("    \t      Action to current folder and sub folders");
		}

		private static void InitForm()
		{
			Application.EnableVisualStyles();
			Application.SetCompatibleTextRenderingDefault(false);
			Application.Run(new MoveFileToDateFolderForm());
		}

		private static int Return(int result)
		{
			System.Windows.Forms.SendKeys.SendWait("{ENTER}");
			Application.Exit();

			return result;
		}
	}
}
