﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MoveFileToDateFolder
{
	public partial class MoveFileToDateFolderForm : Form
	{
		private int _fromCount;
		private int _toCount;

		public MoveFileToDateFolderForm()
		{
			InitializeComponent();

			tbTargetFolder.Text = Application.StartupPath;
			btnFolder.Click += (sender, e) =>
			{
				//Test();
				SelectFolderAction();
			};
			btnRun.Click += (sender, e) =>
			{
				RunAction();
			};
		}

		private void Test()
		{
			var path = @".\SampleImage.jpg";
			path = @".\Sample_NonTakenDate.AAE";
			var createDate = new MoveFileToDateFolderCore().GetTakenDateFromImage(path);

			System.Diagnostics.Debug.WriteLine(createDate, "MoveFileToDateFolderForm.Test");
		}

		private void SelectFolderAction()
		{
			if (string.IsNullOrEmpty(fbdTarget.SelectedPath) == true)
			{
				fbdTarget.SelectedPath = Application.StartupPath;
			}

			if (fbdTarget.ShowDialog(this) == DialogResult.OK)
			{
				tbTargetFolder.Text = fbdTarget.SelectedPath;
			}
		}

		private void RunAction1()
		{
			var target = @"D:\사진";
			var dirs = Directory.GetDirectories(target);

			foreach (var item in dirs)
			{
				try
				{
					var fileName = "20" + Path.GetFileName(item);
					var yearPath = Path.Combine(Path.GetDirectoryName(item), fileName.Substring(0, 4));
					var newPath = Path.Combine(yearPath, fileName);

					if (Directory.Exists(yearPath) == false)
					{
						Directory.CreateDirectory(yearPath);
					}

					Directory.Move(item, newPath);
				}
				catch (Exception ex)
				{
					Console.WriteLine(ex.Message);
					continue;
				}
			}
		}

		private void RunAction()
		{
			var baseFolder = tbTargetFolder.Text;

			if (Directory.Exists(baseFolder) == false)
			{
				MessageBox.Show(
					"Target folder not exists.",
					Text,
					MessageBoxButtons.OK,
					MessageBoxIcon.Warning);

				return;
			}

			var searchPatterns = tbPatterns.Text;
			var moveFileToDateFolder = new MoveFileToDateFolderCore();
			var searchOption = SearchOption.TopDirectoryOnly;

			if (searchPatterns == string.Empty)
			{
				searchPatterns = "*.jpg";
			}

			if (cbSubfolder.Checked == true)
			{
				searchOption = SearchOption.AllDirectories;
			}

			ResetCount();

			var result = moveFileToDateFolder.Run(baseFolder,
							searchPatterns,
							searchOption,
							cbCreatedDate.Checked,
							InvokeGetFilesAction,
							InvokeMovedAction);

			if (result > 0)
			{
				MessageBox.Show(
					string.Format("{0:#,##0} 건이 이동 되었습니다.", result),
					Text,
					MessageBoxButtons.OK,
					MessageBoxIcon.Information);
			}
		}

		private void ResetCount()
		{
			FromCount = 0;
			ToCount = 0;
		}

		private int FromCount
		{
			get
			{
				return _fromCount;
			}
			set
			{
				_fromCount = value;
				lbFromCount.Invoke(new MethodInvoker(() =>
				{
					lbFromCount.Text = string.Format("From: {0:#,##0} 건", value);
				}));
			}
		}

		private int ToCount
		{
			get
			{
				return _toCount;
			}
			set
			{
				_toCount = value;
				lbToCount.Invoke(new MethodInvoker(() =>
				{
					lbToCount.Text = string.Format("To  : {0:#,##0} 건", value);
				}));
			}
		}

		private string RemoveBaseFolderName(string file)
		{
			return file.Replace(tbTargetFolder.Text + "\\", string.Empty);
		}

		private void InvokeGetFilesAction(string[] files)
		{
			FromCount += files.Length;

			foreach (string file in files)
			{
				tbTargets.Text += string.Format("{0}\r\n", RemoveBaseFolderName(file));
			}

			tbTargetFolder.ScrollToCaret();
		}

		private void InvokeMovedAction(string file)
		{
			ToCount++;
			tbResults.Invoke(new MethodInvoker(() =>
			{
				tbResults.Text += string.Format("{0}\r\n", RemoveBaseFolderName(file));
			}));
			tbTargetFolder.ScrollToCaret();

		}
	}
}
