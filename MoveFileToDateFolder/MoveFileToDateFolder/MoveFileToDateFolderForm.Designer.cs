﻿namespace MoveFileToDateFolder
{
	partial class MoveFileToDateFolderForm
	{
		/// <summary>
		/// 필수 디자이너 변수입니다.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// 사용 중인 모든 리소스를 정리합니다.
		/// </summary>
		/// <param name="disposing">관리되는 리소스를 삭제해야 하면 true이고, 그렇지 않으면 false입니다.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form 디자이너에서 생성한 코드

		/// <summary>
		/// 디자이너 지원에 필요한 메서드입니다. 
		/// 이 메서드의 내용을 코드 편집기로 수정하지 마세요.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MoveFileToDateFolderForm));
			groupBox1 = new GroupBox();
			tableLayoutPanel1 = new TableLayoutPanel();
			tbPatterns = new TextBox();
			cbCreatedDate = new CheckBox();
			cbSubfolder = new CheckBox();
			tbTargetFolder = new TextBox();
			btnFolder = new Button();
			btnRun = new Button();
			label1 = new Label();
			tlpMain = new TableLayoutPanel();
			tableLayoutPanel2 = new TableLayoutPanel();
			tbTargets = new TextBox();
			tbResults = new TextBox();
			lbFromCount = new Label();
			lbToCount = new Label();
			fbdTarget = new FolderBrowserDialog();
			groupBox1.SuspendLayout();
			tableLayoutPanel1.SuspendLayout();
			tlpMain.SuspendLayout();
			tableLayoutPanel2.SuspendLayout();
			SuspendLayout();
			// 
			// groupBox1
			// 
			groupBox1.Controls.Add(tableLayoutPanel1);
			groupBox1.Dock = DockStyle.Fill;
			groupBox1.Location = new Point(3, 3);
			groupBox1.Name = "groupBox1";
			groupBox1.Size = new Size(1180, 99);
			groupBox1.TabIndex = 0;
			groupBox1.TabStop = false;
			groupBox1.Text = "대상 폴더 및 파일";
			// 
			// tableLayoutPanel1
			// 
			tableLayoutPanel1.ColumnCount = 5;
			tableLayoutPanel1.ColumnStyles.Add(new ColumnStyle(SizeType.Absolute, 168F));
			tableLayoutPanel1.ColumnStyles.Add(new ColumnStyle(SizeType.Absolute, 106F));
			tableLayoutPanel1.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 100F));
			tableLayoutPanel1.ColumnStyles.Add(new ColumnStyle(SizeType.Absolute, 120F));
			tableLayoutPanel1.ColumnStyles.Add(new ColumnStyle(SizeType.Absolute, 59F));
			tableLayoutPanel1.Controls.Add(tbPatterns, 2, 1);
			tableLayoutPanel1.Controls.Add(cbCreatedDate, 0, 1);
			tableLayoutPanel1.Controls.Add(cbSubfolder, 0, 0);
			tableLayoutPanel1.Controls.Add(tbTargetFolder, 1, 0);
			tableLayoutPanel1.Controls.Add(btnFolder, 4, 0);
			tableLayoutPanel1.Controls.Add(btnRun, 3, 1);
			tableLayoutPanel1.Controls.Add(label1, 1, 1);
			tableLayoutPanel1.Dock = DockStyle.Fill;
			tableLayoutPanel1.Location = new Point(3, 23);
			tableLayoutPanel1.Margin = new Padding(0);
			tableLayoutPanel1.Name = "tableLayoutPanel1";
			tableLayoutPanel1.RowCount = 2;
			tableLayoutPanel1.RowStyles.Add(new RowStyle(SizeType.Percent, 50F));
			tableLayoutPanel1.RowStyles.Add(new RowStyle(SizeType.Percent, 50F));
			tableLayoutPanel1.RowStyles.Add(new RowStyle(SizeType.Absolute, 20F));
			tableLayoutPanel1.Size = new Size(1174, 73);
			tableLayoutPanel1.TabIndex = 0;
			// 
			// tbPatterns
			// 
			tbPatterns.Dock = DockStyle.Fill;
			tbPatterns.Font = new Font("맑은 고딕", 12F);
			tbPatterns.Location = new Point(277, 39);
			tbPatterns.Name = "tbPatterns";
			tbPatterns.Size = new Size(715, 29);
			tbPatterns.TabIndex = 4;
			tbPatterns.Text = "*.jpg;*.jpeg;*.png;*.gif;*.tif;*.tiff;*.mov;*.avi;*.heic;*.heif;*.mp4";
			// 
			// cbCreatedDate
			// 
			cbCreatedDate.CheckAlign = ContentAlignment.MiddleRight;
			cbCreatedDate.Checked = true;
			cbCreatedDate.CheckState = CheckState.Checked;
			cbCreatedDate.Dock = DockStyle.Fill;
			cbCreatedDate.Location = new Point(3, 39);
			cbCreatedDate.Name = "cbCreatedDate";
			cbCreatedDate.Padding = new Padding(11, 0, 11, 0);
			cbCreatedDate.Size = new Size(162, 31);
			cbCreatedDate.TabIndex = 3;
			cbCreatedDate.Text = "촬영 일자 기준";
			cbCreatedDate.UseVisualStyleBackColor = true;
			// 
			// cbSubfolder
			// 
			cbSubfolder.CheckAlign = ContentAlignment.MiddleRight;
			cbSubfolder.Checked = true;
			cbSubfolder.CheckState = CheckState.Checked;
			cbSubfolder.Dock = DockStyle.Fill;
			cbSubfolder.Location = new Point(3, 3);
			cbSubfolder.Name = "cbSubfolder";
			cbSubfolder.Padding = new Padding(11, 0, 11, 0);
			cbSubfolder.Size = new Size(162, 30);
			cbSubfolder.TabIndex = 0;
			cbSubfolder.Text = "하위 폴더 포함";
			cbSubfolder.UseVisualStyleBackColor = true;
			// 
			// tbTargetFolder
			// 
			tableLayoutPanel1.SetColumnSpan(tbTargetFolder, 3);
			tbTargetFolder.Dock = DockStyle.Fill;
			tbTargetFolder.Font = new Font("맑은 고딕", 12F);
			tbTargetFolder.Location = new Point(171, 3);
			tbTargetFolder.Name = "tbTargetFolder";
			tbTargetFolder.Size = new Size(941, 29);
			tbTargetFolder.TabIndex = 1;
			// 
			// btnFolder
			// 
			btnFolder.Dock = DockStyle.Fill;
			btnFolder.Location = new Point(1118, 3);
			btnFolder.Name = "btnFolder";
			btnFolder.Size = new Size(53, 30);
			btnFolder.TabIndex = 2;
			btnFolder.Text = "...";
			btnFolder.UseVisualStyleBackColor = true;
			// 
			// btnRun
			// 
			tableLayoutPanel1.SetColumnSpan(btnRun, 2);
			btnRun.Location = new Point(998, 39);
			btnRun.Name = "btnRun";
			btnRun.Size = new Size(173, 31);
			btnRun.TabIndex = 5;
			btnRun.Text = "정    리(&R)";
			btnRun.UseVisualStyleBackColor = true;
			// 
			// label1
			// 
			label1.AutoSize = true;
			label1.Dock = DockStyle.Fill;
			label1.Location = new Point(171, 36);
			label1.Name = "label1";
			label1.Size = new Size(100, 37);
			label1.TabIndex = 4;
			label1.Text = "검색 패턴 :";
			label1.TextAlign = ContentAlignment.MiddleRight;
			// 
			// tlpMain
			// 
			tlpMain.ColumnCount = 1;
			tlpMain.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 100F));
			tlpMain.Controls.Add(groupBox1, 0, 0);
			tlpMain.Controls.Add(tableLayoutPanel2, 0, 1);
			tlpMain.Dock = DockStyle.Fill;
			tlpMain.Location = new Point(0, 0);
			tlpMain.Name = "tlpMain";
			tlpMain.RowCount = 2;
			tlpMain.RowStyles.Add(new RowStyle(SizeType.Absolute, 105F));
			tlpMain.RowStyles.Add(new RowStyle(SizeType.Percent, 100F));
			tlpMain.Size = new Size(1186, 742);
			tlpMain.TabIndex = 1;
			// 
			// tableLayoutPanel2
			// 
			tableLayoutPanel2.ColumnCount = 2;
			tableLayoutPanel2.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 50F));
			tableLayoutPanel2.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 50F));
			tableLayoutPanel2.Controls.Add(tbTargets, 0, 1);
			tableLayoutPanel2.Controls.Add(tbResults, 1, 1);
			tableLayoutPanel2.Controls.Add(lbFromCount, 0, 0);
			tableLayoutPanel2.Controls.Add(lbToCount, 1, 0);
			tableLayoutPanel2.Dock = DockStyle.Fill;
			tableLayoutPanel2.Location = new Point(0, 105);
			tableLayoutPanel2.Margin = new Padding(0);
			tableLayoutPanel2.Name = "tableLayoutPanel2";
			tableLayoutPanel2.RowCount = 2;
			tableLayoutPanel2.RowStyles.Add(new RowStyle(SizeType.Absolute, 32F));
			tableLayoutPanel2.RowStyles.Add(new RowStyle(SizeType.Percent, 100F));
			tableLayoutPanel2.Size = new Size(1186, 637);
			tableLayoutPanel2.TabIndex = 1;
			// 
			// tbTargets
			// 
			tbTargets.Dock = DockStyle.Fill;
			tbTargets.Location = new Point(3, 35);
			tbTargets.Multiline = true;
			tbTargets.Name = "tbTargets";
			tbTargets.ScrollBars = ScrollBars.Vertical;
			tbTargets.Size = new Size(587, 599);
			tbTargets.TabIndex = 0;
			// 
			// tbResults
			// 
			tbResults.Dock = DockStyle.Fill;
			tbResults.Location = new Point(596, 35);
			tbResults.Multiline = true;
			tbResults.Name = "tbResults";
			tbResults.ScrollBars = ScrollBars.Vertical;
			tbResults.Size = new Size(587, 599);
			tbResults.TabIndex = 0;
			// 
			// lbFromCount
			// 
			lbFromCount.AutoSize = true;
			lbFromCount.Dock = DockStyle.Fill;
			lbFromCount.Location = new Point(3, 0);
			lbFromCount.Name = "lbFromCount";
			lbFromCount.Size = new Size(587, 32);
			lbFromCount.TabIndex = 1;
			lbFromCount.Text = "From: 0건";
			lbFromCount.TextAlign = ContentAlignment.MiddleRight;
			// 
			// lbToCount
			// 
			lbToCount.AutoSize = true;
			lbToCount.Dock = DockStyle.Fill;
			lbToCount.Location = new Point(596, 0);
			lbToCount.Name = "lbToCount";
			lbToCount.Size = new Size(587, 32);
			lbToCount.TabIndex = 1;
			lbToCount.Text = "To: 0건";
			lbToCount.TextAlign = ContentAlignment.MiddleRight;
			// 
			// fbdTarget
			// 
			fbdTarget.Description = "정리할 폴더를 선택 합니다.";
			fbdTarget.ShowNewFolderButton = false;
			// 
			// MoveFileToDateFolderForm
			// 
			AutoScaleMode = AutoScaleMode.None;
			ClientSize = new Size(1186, 742);
			Controls.Add(tlpMain);
			Font = new Font("맑은 고딕", 11F);
			Icon = (Icon)resources.GetObject("$this.Icon");
			Margin = new Padding(4, 6, 4, 6);
			Name = "MoveFileToDateFolderForm";
			StartPosition = FormStartPosition.CenterScreen;
			Text = "Move file to it's own modified date folder name.";
			groupBox1.ResumeLayout(false);
			tableLayoutPanel1.ResumeLayout(false);
			tableLayoutPanel1.PerformLayout();
			tlpMain.ResumeLayout(false);
			tableLayoutPanel2.ResumeLayout(false);
			tableLayoutPanel2.PerformLayout();
			ResumeLayout(false);
		}

		#endregion

		private System.Windows.Forms.GroupBox groupBox1;
		private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
		private System.Windows.Forms.CheckBox cbSubfolder;
		private System.Windows.Forms.TextBox tbTargetFolder;
		private System.Windows.Forms.TableLayoutPanel tlpMain;
		private System.Windows.Forms.Button btnFolder;
		private System.Windows.Forms.CheckBox cbCreatedDate;
		private System.Windows.Forms.Button btnRun;
		private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
		private System.Windows.Forms.TextBox tbTargets;
		private System.Windows.Forms.TextBox tbResults;
		private System.Windows.Forms.FolderBrowserDialog fbdTarget;
		private System.Windows.Forms.TextBox tbPatterns;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Label lbFromCount;
		private System.Windows.Forms.Label lbToCount;
	}
}

