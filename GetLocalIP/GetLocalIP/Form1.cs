﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Net;
using System.Net.Sockets;

namespace GetLocalIP
{
	public partial class Form1 : Form
	{
		public Form1()
		{
			InitializeComponent();
		}

		private void btnClear_Click(object sender, EventArgs e)
		{
			txtIpAddress.Clear();
		}

		private void BindingIpAddresses(List<string> ipAddress)
		{
			txtIpAddress.Text += string.Format("{0}\r\n", "Binding start".PadLeft(40, '*'));

			foreach (string ipAddr in ipAddress)
				txtIpAddress.Text += string.Format("{0}\r\n", ipAddr);
		}

		private void btnGetIP_Click(object sender, EventArgs e)
		{
			// Get local ip address
			IPHostEntry ipEntry = Dns.GetHostEntry(Dns.GetHostName());
			List<string> ipAddress = new List<string>();

			ipAddress.Add(string.Format("Host Name : {0}", Dns.GetHostName()));

			if (ipEntry.AddressList.Length > 0)
			{
				foreach (IPAddress ipAddr in ipEntry.AddressList)
					ipAddress.Add(ipAddr.ToString());
			}

			BindingIpAddresses(ipAddress);
		}

		private void btnGetIpInVista_Click(object sender, EventArgs e)
		{
			IPHostEntry ipEntry = Dns.GetHostEntry(Dns.GetHostName());
			string localIpAddress = string.Empty;
			List<string> ipAddress = new List<string>();

			ipAddress.Add(string.Format("Host Name : {0}", Dns.GetHostName()));

			foreach (IPAddress ipAddr in ipEntry.AddressList)
			{
				byte ipHeader = ipAddr.GetAddressBytes()[0];

				if (ipHeader == 0 || ipHeader == 127 || ipHeader == 169 || ipAddr.AddressFamily != AddressFamily.InterNetwork)
					continue;

				localIpAddress = ipAddr.ToString();
				ipAddress.Add(ipAddr.ToString());
			}

			BindingIpAddresses(ipAddress);
		}
	}
}