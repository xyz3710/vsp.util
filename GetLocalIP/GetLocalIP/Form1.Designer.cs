﻿namespace GetLocalIP
{
	partial class Form1
	{
		/// <summary>
		/// 필수 디자이너 변수입니다.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// 사용 중인 모든 리소스를 정리합니다.
		/// </summary>
		/// <param name="disposing">관리되는 리소스를 삭제해야 하면 true이고, 그렇지 않으면 false입니다.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form 디자이너에서 생성한 코드

		/// <summary>
		/// 디자이너 지원에 필요한 메서드입니다.
		/// 이 메서드의 내용을 코드 편집기로 수정하지 마십시오.
		/// </summary>
		private void InitializeComponent()
		{
			this.btnGetIP = new System.Windows.Forms.Button();
			this.txtIpAddress = new System.Windows.Forms.TextBox();
			this.tlpMain = new System.Windows.Forms.TableLayoutPanel();
			this.btnGetIpInVista = new System.Windows.Forms.Button();
			this.btnClear = new System.Windows.Forms.Button();
			this.tlpMain.SuspendLayout();
			this.SuspendLayout();
			// 
			// btnGetIP
			// 
			this.btnGetIP.Dock = System.Windows.Forms.DockStyle.Fill;
			this.btnGetIP.Location = new System.Drawing.Point(3, 230);
			this.btnGetIP.Name = "btnGetIP";
			this.btnGetIP.Size = new System.Drawing.Size(442, 22);
			this.btnGetIP.TabIndex = 1;
			this.btnGetIP.Text = "Local IP 구하기";
			this.btnGetIP.UseVisualStyleBackColor = true;
			this.btnGetIP.Click += new System.EventHandler(this.btnGetIP_Click);
			// 
			// txtIpAddress
			// 
			this.txtIpAddress.Dock = System.Windows.Forms.DockStyle.Fill;
			this.txtIpAddress.Location = new System.Drawing.Point(3, 3);
			this.txtIpAddress.Multiline = true;
			this.txtIpAddress.Name = "txtIpAddress";
			this.txtIpAddress.ScrollBars = System.Windows.Forms.ScrollBars.Both;
			this.txtIpAddress.Size = new System.Drawing.Size(442, 193);
			this.txtIpAddress.TabIndex = 0;
			// 
			// tlpMain
			// 
			this.tlpMain.ColumnCount = 1;
			this.tlpMain.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
			this.tlpMain.Controls.Add(this.btnGetIP, 0, 2);
			this.tlpMain.Controls.Add(this.txtIpAddress, 0, 0);
			this.tlpMain.Controls.Add(this.btnGetIpInVista, 0, 3);
			this.tlpMain.Controls.Add(this.btnClear, 0, 1);
			this.tlpMain.Dock = System.Windows.Forms.DockStyle.Fill;
			this.tlpMain.Location = new System.Drawing.Point(0, 0);
			this.tlpMain.Name = "tlpMain";
			this.tlpMain.RowCount = 4;
			this.tlpMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
			this.tlpMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 28F));
			this.tlpMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 28F));
			this.tlpMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 28F));
			this.tlpMain.Size = new System.Drawing.Size(448, 283);
			this.tlpMain.TabIndex = 2;
			// 
			// btnGetIpInVista
			// 
			this.btnGetIpInVista.Dock = System.Windows.Forms.DockStyle.Fill;
			this.btnGetIpInVista.Location = new System.Drawing.Point(3, 258);
			this.btnGetIpInVista.Name = "btnGetIpInVista";
			this.btnGetIpInVista.Size = new System.Drawing.Size(442, 22);
			this.btnGetIpInVista.TabIndex = 2;
			this.btnGetIpInVista.Text = "Vista에서 IP4 구하기";
			this.btnGetIpInVista.UseVisualStyleBackColor = true;
			this.btnGetIpInVista.Click += new System.EventHandler(this.btnGetIpInVista_Click);
			// 
			// btnClear
			// 
			this.btnClear.Dock = System.Windows.Forms.DockStyle.Fill;
			this.btnClear.Location = new System.Drawing.Point(3, 202);
			this.btnClear.Name = "btnClear";
			this.btnClear.Size = new System.Drawing.Size(442, 22);
			this.btnClear.TabIndex = 3;
			this.btnClear.Text = "&Clear";
			this.btnClear.UseVisualStyleBackColor = true;
			this.btnClear.Click += new System.EventHandler(this.btnClear_Click);
			// 
			// Form1
			// 
			this.AcceptButton = this.btnGetIP;
			this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.CancelButton = this.btnClear;
			this.ClientSize = new System.Drawing.Size(448, 283);
			this.Controls.Add(this.tlpMain);
			this.Name = "Form1";
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Text = "Get Local IP address";
			this.tlpMain.ResumeLayout(false);
			this.tlpMain.PerformLayout();
			this.ResumeLayout(false);

		}

		#endregion

		private System.Windows.Forms.Button btnGetIP;
		private System.Windows.Forms.TextBox txtIpAddress;
		private System.Windows.Forms.TableLayoutPanel tlpMain;
		private System.Windows.Forms.Button btnGetIpInVista;
		private System.Windows.Forms.Button btnClear;
	}
}

