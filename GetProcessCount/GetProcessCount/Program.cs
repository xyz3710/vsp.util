﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Diagnostics;

namespace GetProcessCount
{
	class Program
	{
		static int Main(string[] args)
		{
			if (args.Length == 0)
			{
				ShowUsage();

				return 0;
			}

			Process[] neturoProc = Process.GetProcessesByName(args[0]);

			return neturoProc.Length;            	
		}

		private static void ShowUsage()
		{
			Console.WriteLine("GetProcessCount <ProcessName>");
		}
	}
}
