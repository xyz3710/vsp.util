﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Text.RegularExpressions;

namespace CodeRush_ChangeUserName
{
	class Program2
	{
		/// <summary>
		/// 3.x, 2009 vol1 대의 CodeRush 사용자 정보 변경
		/// </summary>
		/// <param name="args"></param>
		static void Main2(string[] args)
		{
			string targetFile = string.Format(@"{0}\{1}", Environment.GetEnvironmentVariable("APPDATA"), @"CodeRush for VS .NET\1.1\Settings\IDE\User Info.ini");
			Regex regex = new Regex(@"(Fields_0=).*(\r\nFields_1=).*(\r\n.*)(\r\nFields_3=).*");
			string context = string.Empty;
			string userName = string.Empty;
			string firstName = string.Empty;
			string lastName = string.Empty;

			if (Environment.GetEnvironmentVariable("USERNAME") == "xyz37")
			{
				userName = "김기원";
				firstName = "Ki Won";
				lastName = "Kim";
			}

			while (userName == string.Empty || firstName == string.Empty || lastName == string.Empty)
			{
				if (userName == string.Empty)
				{
					Console.Write("주석등에서 사용할 사용자 이름을 한글로 입력해 주십시오 : ");
					userName = Console.ReadLine();
				}

				if (firstName == string.Empty)
				{
					Console.Write("주석등에서 사용할 사용자 이름(First Name)을 영문으로 입력해 주십시오 : ");
					firstName = Console.ReadLine();
				}

				if (lastName == string.Empty)
				{
					Console.Write("주석등에서 사용할 사용자 성(Last Name)을 영문으로 입력해 주십시오 : ");
					lastName = Console.ReadLine();
				}
			}

			try
			{
				using (StreamReader sr = File.OpenText(targetFile))
				{
					context = sr.ReadToEnd();
				}
			}
			catch
			{
			}
			
			if (regex.IsMatch(context) == true)
			{
				using (FileStream fs =File.OpenWrite(targetFile))
				{
					try
					{
						string newContext = regex.Replace(context, string.Format("$1{0}$2{1}$3$4{2}", firstName, lastName, userName));

						using (StreamWriter sw = new StreamWriter(fs))
                        {
							sw.Write(newContext);
							fs.SetLength(newContext.Length);
                        }
					}
					catch
					{	
					}					
				}
			}
		}
	}
}
