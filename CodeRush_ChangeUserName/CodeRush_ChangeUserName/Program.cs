﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Text.RegularExpressions;
using System.Xml;

namespace CodeRush_ChangeUserName
{
	class Program
	{
		#region Constants
		private const string NAME_KEY = "Name";
		private const string VALUE_KEY = "Value";
		#endregion

		static void Main(string[] args)
		{
			string targetFile1 = string.Format(@"{0}\{1}", Environment.GetEnvironmentVariable("APPDATA"), @"CodeRush for VS .NET\1.1\Settings.xml\IDE\User Info.xml");
			string targetFile2 = string.Format(@"{0}\{1}", Environment.GetEnvironmentVariable("APPDATA"), @"CodeRush for VS .NET\1.1\Settings.xml\_Scheme_Mine\IDE\User Info.xml");
			string userName = string.Empty;
			string firstName = string.Empty;
			string lastName = string.Empty;

			if (Environment.GetEnvironmentVariable("USERNAME") == "xyz37")
			{
				userName = "김기원";
				firstName = "Ki Won";
				lastName = "Kim";
			}

			while (userName == string.Empty || firstName == string.Empty || lastName == string.Empty)
			{
				if (userName == string.Empty)
				{
					Console.Write("주석등에서 사용할 사용자 이름을 한글로 입력해 주십시오 : ");
					userName = Console.ReadLine();
				}

				if (firstName == string.Empty)
				{
					Console.Write("주석등에서 사용할 사용자 이름(First Name)을 영문으로 입력해 주십시오 : ");
					firstName = Console.ReadLine();
				}

				if (lastName == string.Empty)
				{
					Console.Write("주석등에서 사용할 사용자 성(Last Name)을 영문으로 입력해 주십시오 : ");
					lastName = Console.ReadLine();
				}
			}

			ReplaceSettings(targetFile1, userName, firstName, lastName);
			ReplaceSettings(targetFile2, userName, firstName, lastName);
		}

		private static void ReplaceSettings(string targetFile, string userName, string firstName, string lastName)
		{
			if (File.Exists(targetFile) == false)
				return;
			
			XmlDocument xmlDocument = new XmlDocument();

			xmlDocument.Load(targetFile);

			XmlNode xmlNode = xmlDocument.DocumentElement.SelectSingleNode(string.Format("/{0}/{1}/{2}",
															 "Page",
															 "Language",
															 "Section"));
			if (xmlNode != null)
			{
				Dictionary<string, string> options = new Dictionary<string, string>();
				Dictionary<string, int> index = new Dictionary<string, int>();
				int count = 0;

				foreach (XmlNode optionNode in xmlNode.ChildNodes)
				{
					string key = optionNode.Attributes[NAME_KEY].Value;

					options.Add(key, optionNode.Attributes[VALUE_KEY].Value);
					index.Add(key, count++);
				}

				bool changed = false;

				foreach (string key in options.Keys)
				{
					string value = options[key];
					string replaceKey = string.Format("Fields_{0}", key[key.Length - 1]);

					switch (value)
					{
						case "First Name":
							XmlNode firstNameField = xmlNode.ChildNodes.Item(index[replaceKey]);

							if (firstNameField != null)
								firstNameField.Attributes[VALUE_KEY].Value = firstName;

							changed = true;

							break;
						case "Last Name":
							XmlNode lastNameField = xmlNode.ChildNodes.Item(index[replaceKey]);

							if (lastNameField != null)
								lastNameField.Attributes[VALUE_KEY].Value = lastName;

							changed = true;

							break;
						case "RealUserName":
							XmlNode realUserNameField = xmlNode.ChildNodes.Item(index[replaceKey]);

							if (realUserNameField != null)
								realUserNameField.Attributes[VALUE_KEY].Value = userName;

							changed = true;

							break;
					}
				}

				if (changed == true)
					xmlDocument.Save(targetFile);
			}
		}

	}
}
