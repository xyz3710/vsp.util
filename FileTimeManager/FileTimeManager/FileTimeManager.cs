﻿/**********************************************************************************************************************/
/*	Domain		:	FileManager.FileTimeManager
/*	Creator		:	KIMKIWON\xyz37(김기원)
/*	Create		:	2009년 2월 25일 수요일 오전 11:46
/*	Purpose		:	파일 시간을 관리하는 Utility 입니다.
/*--------------------------------------------------------------------------------------------------------------------*/
/*	Modifier	:	
/*	Update		:	
/*	Changes		:	
/*--------------------------------------------------------------------------------------------------------------------*/
/*	Comment		:	
/*--------------------------------------------------------------------------------------------------------------------*/
/*	Reviewer	:	
/*	Rev. Date	:	
/**********************************************************************************************************************/

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Reflection;

namespace FileManager
{
	public partial class FileTimeManager : Form
	{
		#region Constants
		private const string DATE_FORMAT = "yyyy-MM-dd HH:mm:ss:fff";
		#endregion

		#region Fields
		private string _fileName;
		#endregion

		#region Constructors
		public FileTimeManager()
		{
			InitializeComponent();
		}
		#endregion

		#region Properties
		private string FileName
		{
			get
			{
				return _fileName;
			}
			set
			{
				_fileName = value;
				txtFilename.Text = _fileName;
			}
		}
		#endregion

		#region EventHandlers
		private void FileTimeManager_Shown(object sender, EventArgs e)
		{
			AssemblyName assemblyGetName = Assembly.GetExecutingAssembly().GetName();

			Text = string.Format("{0} Ver. {1}", assemblyGetName.Name, assemblyGetName.Version.ToString());
			chkEnableTimer.Checked = false;
		}
		
		private void FileTimeManager_DragEnter(object sender, DragEventArgs e)
		{
			if (e.Data.GetDataPresent("FileDrop") == true)
				e.Effect = DragDropEffects.Copy;
			else
				e.Effect = DragDropEffects.None;
		}

		private void FileTimeManager_DragDrop(object sender, DragEventArgs e)
		{
			string[] fileNames = e.Data.GetData("FileName") as string[];

			if (fileNames != null && fileNames.Length > 0)
			{
				FileName = Path.GetFullPath(fileNames[0]);

				if (File.Exists(FileName) == false)
					return;

				txtFilename.ForeColor = SystemPens.WindowText.Color;
				txtFilename.Text = FileName;
				DisplayTime(lblCreateTime, File.GetCreationTime(FileName));
				DisplayTime(lblLastWriteTime, File.GetLastWriteTime(FileName));
				DisplayTime(lblLastAccessTime, File.GetLastAccessTime(FileName));
				rbTimeSelector_CheckedChanged(null, new EventArgs());
				StatusLabel.Text = string.Format("Read \"{0}\" file", Path.GetFileName(FileName));
			}
		}

		private void rbTimeSelector_CheckedChanged(object sender, EventArgs e)
		{
			string dateString = DateTime.Now.ToString(DATE_FORMAT);

			if (rbCreateTime.Checked == true)
				dateString = lblCreateTime.Text;
			else if (rbLastWriteTime.Checked == true)
				dateString = lblLastWriteTime.Text;
			else if (rbLastAccessTime.Checked == true)
				dateString = lblLastAccessTime.Text;

			SetNumericControl(DateTime.ParseExact(dateString, DATE_FORMAT, null));
		}

		private void btnChangeTime_Click(object sender, EventArgs e)
		{
			DateTime changedTime = GetDateTimeFromNumericControl();
			string targetTime = string.Empty;

			if (rbCreateTime.Checked == true)
			{
				targetTime = lblCreate.Text;
				File.SetCreationTime(FileName, changedTime);
				DisplayTime(lblCreateTime, changedTime);
			}
			else if (rbLastWriteTime.Checked == true)
			{
				targetTime = lblLastWrite.Text;
				File.SetLastWriteTime(FileName, changedTime);
				DisplayTime(lblLastWriteTime, changedTime);
			}
			else if (rbLastAccessTime.Checked == true)
			{
				targetTime = lblLastAccess.Text;
				File.SetLastAccessTime(FileName, changedTime);
				DisplayTime(lblLastAccessTime, changedTime);
			}

			StatusLabel.Text = string.Format("{0} changed", targetTime);
		}

		private void Timer_Tick(object sender, EventArgs e)
		{
			SetNumericControl(DateTime.Now);
		}

		private void chkEnableTimer_CheckedChanged(object sender, EventArgs e)
		{
			Timer.Enabled = chkEnableTimer.Checked;
		}
		#endregion

		#region Private methods
		private void DisplayTime(Label lblTarget, DateTime dateTime)
		{
			lblTarget.Text = dateTime.ToString(DATE_FORMAT);
			SetNumericControl(dateTime);
		}

		private void SetNumericControl(DateTime dateTime)
		{
			nuYear.Value = dateTime.Year;
			nuMonth.Value = dateTime.Month;
			nuDay.Value = dateTime.Day;
			nuHour.Value = dateTime.Hour;
			nuMinute.Value = dateTime.Minute;
			nuSecond.Value = dateTime.Second;
			nuMiliSec.Value = dateTime.Millisecond;
		}

		private DateTime GetDateTimeFromNumericControl()
		{
			DateTime changedTime = new DateTime((int)nuYear.Value,
									   (int)nuMonth.Value,
									   (int)nuDay.Value,
									   (int)nuHour.Value,
									   (int)nuMinute.Value,
									   (int)nuSecond.Value,
									   (int)nuMiliSec.Value);

			return changedTime;
		}
		#endregion
	}
}
