﻿namespace FileManager
{
	partial class FileTimeManager
	{
		/// <summary>
		/// 필수 디자이너 변수입니다.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// 사용 중인 모든 리소스를 정리합니다.
		/// </summary>
		/// <param name="disposing">관리되는 리소스를 삭제해야 하면 true이고, 그렇지 않으면 false입니다.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form 디자이너에서 생성한 코드

		/// <summary>
		/// 디자이너 지원에 필요한 메서드입니다.
		/// 이 메서드의 내용을 코드 편집기로 수정하지 마십시오.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FileTimeManager));
			this.tlpMain = new System.Windows.Forms.TableLayoutPanel();
			this.tlpTimeInfo = new System.Windows.Forms.TableLayoutPanel();
			this.lblCreate = new System.Windows.Forms.Label();
			this.lblLastWrite = new System.Windows.Forms.Label();
			this.lblLastAccess = new System.Windows.Forms.Label();
			this.lblCreateTime = new System.Windows.Forms.Label();
			this.lblLastWriteTime = new System.Windows.Forms.Label();
			this.lblLastAccessTime = new System.Windows.Forms.Label();
			this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
			this.nuYear = new System.Windows.Forms.NumericUpDown();
			this.nuMonth = new System.Windows.Forms.NumericUpDown();
			this.nuDay = new System.Windows.Forms.NumericUpDown();
			this.nuHour = new System.Windows.Forms.NumericUpDown();
			this.nuMinute = new System.Windows.Forms.NumericUpDown();
			this.nuSecond = new System.Windows.Forms.NumericUpDown();
			this.nuMiliSec = new System.Windows.Forms.NumericUpDown();
			this.tlpFile = new System.Windows.Forms.TableLayoutPanel();
			this.btnChangeTime = new System.Windows.Forms.Button();
			this.txtFilename = new System.Windows.Forms.TextBox();
			this.tlpTimeSelector = new System.Windows.Forms.TableLayoutPanel();
			this.rbCreateTime = new System.Windows.Forms.RadioButton();
			this.rbLastWriteTime = new System.Windows.Forms.RadioButton();
			this.chkEnableTimer = new System.Windows.Forms.CheckBox();
			this.rbLastAccessTime = new System.Windows.Forms.RadioButton();
			this.StatusStrip = new System.Windows.Forms.StatusStrip();
			this.StatusLabel = new System.Windows.Forms.ToolStripStatusLabel();
			this.Timer = new System.Windows.Forms.Timer(this.components);
			this.tlpMain.SuspendLayout();
			this.tlpTimeInfo.SuspendLayout();
			this.tableLayoutPanel1.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.nuYear)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.nuMonth)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.nuDay)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.nuHour)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.nuMinute)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.nuSecond)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.nuMiliSec)).BeginInit();
			this.tlpFile.SuspendLayout();
			this.tlpTimeSelector.SuspendLayout();
			this.StatusStrip.SuspendLayout();
			this.SuspendLayout();
			// 
			// tlpMain
			// 
			this.tlpMain.ColumnCount = 1;
			this.tlpMain.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
			this.tlpMain.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
			this.tlpMain.Controls.Add(this.tlpTimeInfo, 0, 3);
			this.tlpMain.Controls.Add(this.tableLayoutPanel1, 0, 1);
			this.tlpMain.Controls.Add(this.tlpFile, 0, 2);
			this.tlpMain.Controls.Add(this.tlpTimeSelector, 0, 0);
			this.tlpMain.Dock = System.Windows.Forms.DockStyle.Fill;
			this.tlpMain.Location = new System.Drawing.Point(0, 0);
			this.tlpMain.Name = "tlpMain";
			this.tlpMain.Padding = new System.Windows.Forms.Padding(0, 0, 0, 22);
			this.tlpMain.RowCount = 4;
			this.tlpMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 33F));
			this.tlpMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 33F));
			this.tlpMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 33F));
			this.tlpMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
			this.tlpMain.Size = new System.Drawing.Size(506, 245);
			this.tlpMain.TabIndex = 0;
			// 
			// tlpTimeInfo
			// 
			this.tlpTimeInfo.ColumnCount = 2;
			this.tlpTimeInfo.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
			this.tlpTimeInfo.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
			this.tlpTimeInfo.Controls.Add(this.lblCreate, 0, 0);
			this.tlpTimeInfo.Controls.Add(this.lblLastWrite, 0, 1);
			this.tlpTimeInfo.Controls.Add(this.lblLastAccess, 0, 2);
			this.tlpTimeInfo.Controls.Add(this.lblCreateTime, 1, 0);
			this.tlpTimeInfo.Controls.Add(this.lblLastWriteTime, 1, 1);
			this.tlpTimeInfo.Controls.Add(this.lblLastAccessTime, 1, 2);
			this.tlpTimeInfo.Dock = System.Windows.Forms.DockStyle.Fill;
			this.tlpTimeInfo.Location = new System.Drawing.Point(3, 102);
			this.tlpTimeInfo.Name = "tlpTimeInfo";
			this.tlpTimeInfo.RowCount = 3;
			this.tlpTimeInfo.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
			this.tlpTimeInfo.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
			this.tlpTimeInfo.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
			this.tlpTimeInfo.Size = new System.Drawing.Size(500, 118);
			this.tlpTimeInfo.TabIndex = 0;
			// 
			// lblCreate
			// 
			this.lblCreate.AutoSize = true;
			this.lblCreate.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.lblCreate.Dock = System.Windows.Forms.DockStyle.Fill;
			this.lblCreate.Location = new System.Drawing.Point(3, 3);
			this.lblCreate.Margin = new System.Windows.Forms.Padding(3);
			this.lblCreate.Name = "lblCreate";
			this.lblCreate.Size = new System.Drawing.Size(244, 33);
			this.lblCreate.TabIndex = 0;
			this.lblCreate.Text = "Create Time";
			this.lblCreate.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// lblLastWrite
			// 
			this.lblLastWrite.AutoSize = true;
			this.lblLastWrite.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.lblLastWrite.Dock = System.Windows.Forms.DockStyle.Fill;
			this.lblLastWrite.Location = new System.Drawing.Point(3, 42);
			this.lblLastWrite.Margin = new System.Windows.Forms.Padding(3);
			this.lblLastWrite.Name = "lblLastWrite";
			this.lblLastWrite.Size = new System.Drawing.Size(244, 33);
			this.lblLastWrite.TabIndex = 0;
			this.lblLastWrite.Text = "Last Write Time";
			this.lblLastWrite.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// lblLastAccess
			// 
			this.lblLastAccess.AutoSize = true;
			this.lblLastAccess.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.lblLastAccess.Dock = System.Windows.Forms.DockStyle.Fill;
			this.lblLastAccess.Location = new System.Drawing.Point(3, 81);
			this.lblLastAccess.Margin = new System.Windows.Forms.Padding(3);
			this.lblLastAccess.Name = "lblLastAccess";
			this.lblLastAccess.Size = new System.Drawing.Size(244, 34);
			this.lblLastAccess.TabIndex = 0;
			this.lblLastAccess.Text = "Last Access Time";
			this.lblLastAccess.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// lblCreateTime
			// 
			this.lblCreateTime.AutoSize = true;
			this.lblCreateTime.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.lblCreateTime.Dock = System.Windows.Forms.DockStyle.Fill;
			this.lblCreateTime.Location = new System.Drawing.Point(253, 3);
			this.lblCreateTime.Margin = new System.Windows.Forms.Padding(3);
			this.lblCreateTime.Name = "lblCreateTime";
			this.lblCreateTime.Size = new System.Drawing.Size(244, 33);
			this.lblCreateTime.TabIndex = 0;
			this.lblCreateTime.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// lblLastWriteTime
			// 
			this.lblLastWriteTime.AutoSize = true;
			this.lblLastWriteTime.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.lblLastWriteTime.Dock = System.Windows.Forms.DockStyle.Fill;
			this.lblLastWriteTime.Location = new System.Drawing.Point(253, 42);
			this.lblLastWriteTime.Margin = new System.Windows.Forms.Padding(3);
			this.lblLastWriteTime.Name = "lblLastWriteTime";
			this.lblLastWriteTime.Size = new System.Drawing.Size(244, 33);
			this.lblLastWriteTime.TabIndex = 0;
			this.lblLastWriteTime.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// lblLastAccessTime
			// 
			this.lblLastAccessTime.AutoSize = true;
			this.lblLastAccessTime.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.lblLastAccessTime.Dock = System.Windows.Forms.DockStyle.Fill;
			this.lblLastAccessTime.Location = new System.Drawing.Point(253, 81);
			this.lblLastAccessTime.Margin = new System.Windows.Forms.Padding(3);
			this.lblLastAccessTime.Name = "lblLastAccessTime";
			this.lblLastAccessTime.Size = new System.Drawing.Size(244, 34);
			this.lblLastAccessTime.TabIndex = 0;
			this.lblLastAccessTime.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// tableLayoutPanel1
			// 
			this.tableLayoutPanel1.ColumnCount = 7;
			this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 14.28571F));
			this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 14.28571F));
			this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 14.28571F));
			this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 14.28571F));
			this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 14.28571F));
			this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 14.28571F));
			this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 14.28571F));
			this.tableLayoutPanel1.Controls.Add(this.nuYear, 0, 0);
			this.tableLayoutPanel1.Controls.Add(this.nuMonth, 1, 0);
			this.tableLayoutPanel1.Controls.Add(this.nuDay, 2, 0);
			this.tableLayoutPanel1.Controls.Add(this.nuHour, 3, 0);
			this.tableLayoutPanel1.Controls.Add(this.nuMinute, 4, 0);
			this.tableLayoutPanel1.Controls.Add(this.nuSecond, 5, 0);
			this.tableLayoutPanel1.Controls.Add(this.nuMiliSec, 6, 0);
			this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.tableLayoutPanel1.Location = new System.Drawing.Point(3, 33);
			this.tableLayoutPanel1.Margin = new System.Windows.Forms.Padding(3, 0, 3, 0);
			this.tableLayoutPanel1.Name = "tableLayoutPanel1";
			this.tableLayoutPanel1.RowCount = 1;
			this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
			this.tableLayoutPanel1.Size = new System.Drawing.Size(500, 33);
			this.tableLayoutPanel1.TabIndex = 0;
			// 
			// nuYear
			// 
			this.nuYear.Dock = System.Windows.Forms.DockStyle.Fill;
			this.nuYear.Location = new System.Drawing.Point(3, 3);
			this.nuYear.Maximum = new decimal(new int[] {
            3000,
            0,
            0,
            0});
			this.nuYear.Minimum = new decimal(new int[] {
            1900,
            0,
            0,
            0});
			this.nuYear.Name = "nuYear";
			this.nuYear.Size = new System.Drawing.Size(65, 22);
			this.nuYear.TabIndex = 0;
			this.nuYear.Value = new decimal(new int[] {
            1900,
            0,
            0,
            0});
			// 
			// nuMonth
			// 
			this.nuMonth.Dock = System.Windows.Forms.DockStyle.Fill;
			this.nuMonth.Location = new System.Drawing.Point(74, 3);
			this.nuMonth.Maximum = new decimal(new int[] {
            12,
            0,
            0,
            0});
			this.nuMonth.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
			this.nuMonth.Name = "nuMonth";
			this.nuMonth.Size = new System.Drawing.Size(65, 22);
			this.nuMonth.TabIndex = 1;
			this.nuMonth.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
			// 
			// nuDay
			// 
			this.nuDay.Dock = System.Windows.Forms.DockStyle.Fill;
			this.nuDay.Location = new System.Drawing.Point(145, 3);
			this.nuDay.Maximum = new decimal(new int[] {
            31,
            0,
            0,
            0});
			this.nuDay.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
			this.nuDay.Name = "nuDay";
			this.nuDay.Size = new System.Drawing.Size(65, 22);
			this.nuDay.TabIndex = 2;
			this.nuDay.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
			// 
			// nuHour
			// 
			this.nuHour.Dock = System.Windows.Forms.DockStyle.Fill;
			this.nuHour.Location = new System.Drawing.Point(216, 3);
			this.nuHour.Maximum = new decimal(new int[] {
            23,
            0,
            0,
            0});
			this.nuHour.Name = "nuHour";
			this.nuHour.Size = new System.Drawing.Size(65, 22);
			this.nuHour.TabIndex = 3;
			// 
			// nuMinute
			// 
			this.nuMinute.Dock = System.Windows.Forms.DockStyle.Fill;
			this.nuMinute.Location = new System.Drawing.Point(287, 3);
			this.nuMinute.Maximum = new decimal(new int[] {
            59,
            0,
            0,
            0});
			this.nuMinute.Name = "nuMinute";
			this.nuMinute.Size = new System.Drawing.Size(65, 22);
			this.nuMinute.TabIndex = 4;
			// 
			// nuSecond
			// 
			this.nuSecond.Dock = System.Windows.Forms.DockStyle.Fill;
			this.nuSecond.Location = new System.Drawing.Point(358, 3);
			this.nuSecond.Maximum = new decimal(new int[] {
            59,
            0,
            0,
            0});
			this.nuSecond.Name = "nuSecond";
			this.nuSecond.Size = new System.Drawing.Size(65, 22);
			this.nuSecond.TabIndex = 5;
			// 
			// nuMiliSec
			// 
			this.nuMiliSec.Dock = System.Windows.Forms.DockStyle.Fill;
			this.nuMiliSec.Location = new System.Drawing.Point(429, 3);
			this.nuMiliSec.Maximum = new decimal(new int[] {
            999,
            0,
            0,
            0});
			this.nuMiliSec.Name = "nuMiliSec";
			this.nuMiliSec.Size = new System.Drawing.Size(68, 22);
			this.nuMiliSec.TabIndex = 6;
			// 
			// tlpFile
			// 
			this.tlpFile.ColumnCount = 2;
			this.tlpFile.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 77.77778F));
			this.tlpFile.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 22.22222F));
			this.tlpFile.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
			this.tlpFile.Controls.Add(this.btnChangeTime, 1, 0);
			this.tlpFile.Controls.Add(this.txtFilename, 0, 0);
			this.tlpFile.Dock = System.Windows.Forms.DockStyle.Fill;
			this.tlpFile.Location = new System.Drawing.Point(3, 66);
			this.tlpFile.Margin = new System.Windows.Forms.Padding(3, 0, 3, 0);
			this.tlpFile.Name = "tlpFile";
			this.tlpFile.RowCount = 1;
			this.tlpFile.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
			this.tlpFile.Size = new System.Drawing.Size(500, 33);
			this.tlpFile.TabIndex = 0;
			// 
			// btnChangeTime
			// 
			this.btnChangeTime.Dock = System.Windows.Forms.DockStyle.Fill;
			this.btnChangeTime.Location = new System.Drawing.Point(391, 3);
			this.btnChangeTime.Name = "btnChangeTime";
			this.btnChangeTime.Size = new System.Drawing.Size(106, 27);
			this.btnChangeTime.TabIndex = 1;
			this.btnChangeTime.Text = "Time &Change";
			this.btnChangeTime.UseVisualStyleBackColor = true;
			this.btnChangeTime.Click += new System.EventHandler(this.btnChangeTime_Click);
			// 
			// txtFilename
			// 
			this.txtFilename.Dock = System.Windows.Forms.DockStyle.Fill;
			this.txtFilename.ForeColor = System.Drawing.Color.Gray;
			this.txtFilename.Location = new System.Drawing.Point(3, 3);
			this.txtFilename.Name = "txtFilename";
			this.txtFilename.Size = new System.Drawing.Size(382, 22);
			this.txtFilename.TabIndex = 0;
			this.txtFilename.Text = "Target file drag and drop here.";
			// 
			// tlpTimeSelector
			// 
			this.tlpTimeSelector.ColumnCount = 4;
			this.tlpTimeSelector.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
			this.tlpTimeSelector.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33334F));
			this.tlpTimeSelector.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33334F));
			this.tlpTimeSelector.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 71F));
			this.tlpTimeSelector.Controls.Add(this.rbCreateTime, 0, 0);
			this.tlpTimeSelector.Controls.Add(this.rbLastWriteTime, 1, 0);
			this.tlpTimeSelector.Controls.Add(this.chkEnableTimer, 3, 0);
			this.tlpTimeSelector.Controls.Add(this.rbLastAccessTime, 2, 0);
			this.tlpTimeSelector.Dock = System.Windows.Forms.DockStyle.Fill;
			this.tlpTimeSelector.Location = new System.Drawing.Point(3, 0);
			this.tlpTimeSelector.Margin = new System.Windows.Forms.Padding(3, 0, 3, 0);
			this.tlpTimeSelector.Name = "tlpTimeSelector";
			this.tlpTimeSelector.RowCount = 1;
			this.tlpTimeSelector.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
			this.tlpTimeSelector.Size = new System.Drawing.Size(500, 33);
			this.tlpTimeSelector.TabIndex = 0;
			// 
			// rbCreateTime
			// 
			this.rbCreateTime.AutoSize = true;
			this.rbCreateTime.Dock = System.Windows.Forms.DockStyle.Fill;
			this.rbCreateTime.Location = new System.Drawing.Point(3, 3);
			this.rbCreateTime.Name = "rbCreateTime";
			this.rbCreateTime.Padding = new System.Windows.Forms.Padding(7, 0, 0, 0);
			this.rbCreateTime.Size = new System.Drawing.Size(136, 27);
			this.rbCreateTime.TabIndex = 0;
			this.rbCreateTime.TabStop = true;
			this.rbCreateTime.Text = "&Create Time";
			this.rbCreateTime.UseVisualStyleBackColor = false;
			this.rbCreateTime.CheckedChanged += new System.EventHandler(this.rbTimeSelector_CheckedChanged);
			// 
			// rbLastWriteTime
			// 
			this.rbLastWriteTime.AutoSize = true;
			this.rbLastWriteTime.Dock = System.Windows.Forms.DockStyle.Fill;
			this.rbLastWriteTime.Location = new System.Drawing.Point(145, 3);
			this.rbLastWriteTime.Name = "rbLastWriteTime";
			this.rbLastWriteTime.Padding = new System.Windows.Forms.Padding(7, 0, 0, 0);
			this.rbLastWriteTime.Size = new System.Drawing.Size(137, 27);
			this.rbLastWriteTime.TabIndex = 1;
			this.rbLastWriteTime.TabStop = true;
			this.rbLastWriteTime.Text = "Last &Write Time";
			this.rbLastWriteTime.UseVisualStyleBackColor = true;
			this.rbLastWriteTime.CheckedChanged += new System.EventHandler(this.rbTimeSelector_CheckedChanged);
			// 
			// chkEnableTimer
			// 
			this.chkEnableTimer.AutoSize = true;
			this.chkEnableTimer.Checked = true;
			this.chkEnableTimer.CheckState = System.Windows.Forms.CheckState.Checked;
			this.chkEnableTimer.Dock = System.Windows.Forms.DockStyle.Fill;
			this.chkEnableTimer.Location = new System.Drawing.Point(431, 3);
			this.chkEnableTimer.Name = "chkEnableTimer";
			this.chkEnableTimer.Padding = new System.Windows.Forms.Padding(3, 0, 0, 0);
			this.chkEnableTimer.Size = new System.Drawing.Size(66, 27);
			this.chkEnableTimer.TabIndex = 2;
			this.chkEnableTimer.Text = "&Timer";
			this.chkEnableTimer.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			this.chkEnableTimer.UseVisualStyleBackColor = true;
			this.chkEnableTimer.CheckedChanged += new System.EventHandler(this.chkEnableTimer_CheckedChanged);
			// 
			// rbLastAccessTime
			// 
			this.rbLastAccessTime.AutoSize = true;
			this.rbLastAccessTime.Checked = true;
			this.rbLastAccessTime.Dock = System.Windows.Forms.DockStyle.Fill;
			this.rbLastAccessTime.Location = new System.Drawing.Point(288, 3);
			this.rbLastAccessTime.Name = "rbLastAccessTime";
			this.rbLastAccessTime.Padding = new System.Windows.Forms.Padding(5, 0, 0, 0);
			this.rbLastAccessTime.Size = new System.Drawing.Size(137, 27);
			this.rbLastAccessTime.TabIndex = 2;
			this.rbLastAccessTime.TabStop = true;
			this.rbLastAccessTime.Text = "Last &Access Time";
			this.rbLastAccessTime.UseVisualStyleBackColor = true;
			this.rbLastAccessTime.CheckedChanged += new System.EventHandler(this.rbTimeSelector_CheckedChanged);
			// 
			// StatusStrip
			// 
			this.StatusStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.StatusLabel});
			this.StatusStrip.Location = new System.Drawing.Point(0, 223);
			this.StatusStrip.Name = "StatusStrip";
			this.StatusStrip.Size = new System.Drawing.Size(506, 22);
			this.StatusStrip.TabIndex = 1;
			this.StatusStrip.Text = "statusStrip1";
			// 
			// StatusLabel
			// 
			this.StatusLabel.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
			this.StatusLabel.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
			this.StatusLabel.ForeColor = System.Drawing.Color.RoyalBlue;
			this.StatusLabel.Name = "StatusLabel";
			this.StatusLabel.Size = new System.Drawing.Size(491, 17);
			this.StatusLabel.Spring = true;
			this.StatusLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// Timer
			// 
			this.Timer.Enabled = true;
			this.Timer.Tick += new System.EventHandler(this.Timer_Tick);
			// 
			// FileTimeManager
			// 
			this.AllowDrop = true;
			this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 14F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.BackColor = System.Drawing.SystemColors.Control;
			this.ClientSize = new System.Drawing.Size(506, 245);
			this.Controls.Add(this.StatusStrip);
			this.Controls.Add(this.tlpMain);
			this.Font = new System.Drawing.Font("Tahoma", 9F);
			this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
			this.Name = "FileTimeManager";
			this.Opacity = 0.9;
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Text = "Deploy Roleback";
			this.TopMost = true;
			this.Shown += new System.EventHandler(this.FileTimeManager_Shown);
			this.DragDrop += new System.Windows.Forms.DragEventHandler(this.FileTimeManager_DragDrop);
			this.DragEnter += new System.Windows.Forms.DragEventHandler(this.FileTimeManager_DragEnter);
			this.tlpMain.ResumeLayout(false);
			this.tlpTimeInfo.ResumeLayout(false);
			this.tlpTimeInfo.PerformLayout();
			this.tableLayoutPanel1.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.nuYear)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.nuMonth)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.nuDay)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.nuHour)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.nuMinute)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.nuSecond)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.nuMiliSec)).EndInit();
			this.tlpFile.ResumeLayout(false);
			this.tlpFile.PerformLayout();
			this.tlpTimeSelector.ResumeLayout(false);
			this.tlpTimeSelector.PerformLayout();
			this.StatusStrip.ResumeLayout(false);
			this.StatusStrip.PerformLayout();
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.TableLayoutPanel tlpMain;
		private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
		private System.Windows.Forms.TableLayoutPanel tlpTimeInfo;
		private System.Windows.Forms.TableLayoutPanel tlpFile;
		private System.Windows.Forms.NumericUpDown nuYear;
		private System.Windows.Forms.NumericUpDown nuMonth;
		private System.Windows.Forms.NumericUpDown nuDay;
		private System.Windows.Forms.NumericUpDown nuHour;
		private System.Windows.Forms.NumericUpDown nuMinute;
		private System.Windows.Forms.NumericUpDown nuSecond;
		private System.Windows.Forms.NumericUpDown nuMiliSec;
		private System.Windows.Forms.Button btnChangeTime;
		private System.Windows.Forms.TextBox txtFilename;
		private System.Windows.Forms.TableLayoutPanel tlpTimeSelector;
		private System.Windows.Forms.RadioButton rbCreateTime;
		private System.Windows.Forms.RadioButton rbLastWriteTime;
		private System.Windows.Forms.RadioButton rbLastAccessTime;
		private System.Windows.Forms.Label lblCreate;
		private System.Windows.Forms.Label lblLastWrite;
		private System.Windows.Forms.Label lblLastAccess;
		private System.Windows.Forms.Label lblCreateTime;
		private System.Windows.Forms.Label lblLastWriteTime;
		private System.Windows.Forms.Label lblLastAccessTime;
		private System.Windows.Forms.StatusStrip StatusStrip;
		private System.Windows.Forms.ToolStripStatusLabel StatusLabel;
		private System.Windows.Forms.Timer Timer;
		private System.Windows.Forms.CheckBox chkEnableTimer;
	}
}

