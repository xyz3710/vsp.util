﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using System.Diagnostics;

namespace GetAssemblies
{
	static class Program
	{
		/// <summary>
		/// 해당 응용 프로그램의 주 진입점입니다.
		/// </summary>
		[STAThread]
		public static int Main(string[] args)
		{
			if (args.Length == 1)
			{
				if (args[0] == "/?" || args[0] == "-?" || args[0].ToLower() == "/help"|| args[0].ToLower() == "-help")
                	showUsages();
		
				return -1;
			}

			if (args.Length >= 2)
			{
				GetAssemblies ga = parseArguments(args);

				try
				{
					string exceptionMsg = string.Empty;

					if (ga.Check().Rows.Count > 0)
						ga.Copy(out exceptionMsg);

					if (exceptionMsg != string.Empty)
						throw new Exception(exceptionMsg);
				}
				catch (Exception ex)
				{
					Console.WriteLine(ex.Message);
				}
			}
			else
			{
				Application.EnableVisualStyles();
				Application.SetCompatibleTextRenderingDefault(false);
				Application.Run(new GetAssembliesForm());
			}

			return 0;
		}

		private static void showUsages()
		{
			Console.WriteLine();
			Console.WriteLine(" /?, -?, /help, -help : Display this message.");
			Console.WriteLine("");
			Console.WriteLine(" {0} <Source> [Target] <PGroup> [PFilter] [Doc]", Application.ProductName);
			Console.WriteLine("");
			Console.WriteLine(" Source  : Source solution file");
			Console.WriteLine(" Target  : Target output path(default: <Source>\\Bin)");
			Console.WriteLine(" PGroup  : Property Group(Debug, Release, ...)");
			Console.WriteLine(" PFilter : Project Name Filter(with wildcard *, ? case sensitive)");
			Console.WriteLine(" Doc     : true/false (If exists document files(*.xml) then copy too)");
			Console.WriteLine("");
			Console.WriteLine(" Sample : \r\n{0} source:C:\\Test.sln target:C:\\Bin pgroup:debug pfilter:T* doc:true", Application.ProductName);
			Console.WriteLine("  => Any start with \"T\" letter projects files in Debug condition");
			Console.WriteLine("     at C:\\Test.sln solution copy to C:\\Bin with project documents.");
		}

		private static GetAssemblies parseArguments(string[] args)
		{
			GetAssemblies ga = new GetAssemblies();
			string token = string.Empty;
			int seperatePos = -1;

			foreach (string arg in args)
			{
				seperatePos = arg.IndexOf(":", 0);
				
				if (seperatePos > 0)
				{
					token = arg.Substring(0, seperatePos).ToLower();

					if (token == "source")
						ga.SolutionPath = getValue(seperatePos, arg);
					
					if (token == "target")
						ga.TargetPath = getValue(seperatePos, arg);

					if (token == "pgroup")
                    	ga.PropertyGroup = getValue(seperatePos, arg);

					if (token == "pfilter")
						ga.ProjectNameFilter = getValue(seperatePos, arg);

					if (token == "doc")
                    	ga.IsDocCopy = bool.Parse(getValue(seperatePos, arg));
				}
			}

			return ga;
		}

		private static string getValue(int seperatePos, string arg)
		{
			return arg.Substring(seperatePos + 1);
		}
	}
}