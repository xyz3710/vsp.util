using System;
using System.Collections.Generic;
using System.Windows.Forms;
using System.IO;
using System.Collections;
using System.Diagnostics;

namespace GetAssemblies
{
	public class ProjectInfo
	{
		// TODO: <ItemGroup> <Reference Include="~~~" 참조하는 reference도 같이 카피 할 수 있도록 하자!!
		private string _projectName;
		private string _projectFile;
		private string _assemblyName;
		private string _assemblyType;
		private string _solusionPath;
		private ConstructGroup _constructGroup;

		public ProjectInfo(string solusionPath)
		{
			_projectName = string.Empty;
			_projectFile = string.Empty;
			_assemblyName = string.Empty;
			_solusionPath = solusionPath;
			_constructGroup = new ConstructGroup();
		}

		#region Indexer
		private ArrayList _alIndexer = new ArrayList();
		private int _indexerCnt;

		/// <summary>
		/// ProjectInfo Indexer
		/// </summary>
		/// <param name="index">index</param>
		/// <returns>ProjectInfo</returns>
		public ProjectInfo this[int index]
		{
			get
			{
				if (index > -1 && index < _alIndexer.Count)
					return (ProjectInfo)_alIndexer[index];
				else
					return null;
			}
			set
			{
				if (index > -1 && index < _alIndexer.Count)
					_alIndexer[index] = value;
				else if (index == _alIndexer.Count)
				{
					_alIndexer.Add(value);
					_indexerCnt++;
				}
				else
					throw new IndexOutOfRangeException();
			}
		}

		/// <summary>
		/// Indexer Count 
		/// </summary>
		public int IndexerCount
		{
			get
			{
				return _indexerCnt;
			}
		}
		#endregion

		#region Properties
		/// <summary>
		/// ProjectName을(를) 구하거나 설정합니다.
		/// </summary>
		public String ProjectName
		{
			get
			{
				return _projectName;
			}
			set
			{
				_projectName = value;
			}
		}
        
        /// <summary>
        /// ProjectFile을(를) 구하거나 설정합니다.
        /// </summary>
		public string ProjectFile
		{
			get
			{
				return string.Format("{0}{1}", _solusionPath, _projectFile);
			}
			set
			{
				_projectFile = value;
			}
		}
        
		/// <summary>
		/// Project Path를 구합니다.
		/// </summary>
		public string ProjectPath
		{
			get
			{
				return Path.GetDirectoryName(ProjectFile);
			}
		}

        /// <summary>
        /// AssemblyName을(를) 구하거나 설정합니다.
        /// </summary>
		public string AssemblyName
		{
			get
			{
				return string.Format("{0}.{1}", _assemblyName, _assemblyType);
			}
			set
			{
				_assemblyName = value;
			}
		}

		/// <summary>
		/// AssemblyType을(를) 구하거나 설정합니다.
		/// </summary>
		public string AssemblyType
		{
			get
			{
				return _assemblyType;
			}
			set
			{
				_assemblyType = value;
			}
		}

		/// <summary>
        /// SolusionPath을(를) 구하거나 설정합니다.
        /// </summary>
		public string SolusionPath
		{
			get
			{
				return _solusionPath;
			}
			set
			{
				_solusionPath = value;
			}
		}
        
        /// <summary>
        /// PropertyGroup을(를) 구하거나 설정합니다.
        /// </summary>
		public ConstructGroup ConstructGroup
		{
			get
			{
				return _constructGroup;
			}
			set
			{
				_constructGroup = value;
			}
		}
        
        #endregion

		public static ProjectInfo GetSolutionInfo(string solusionPath)
		{
			const string TOKEN_ASSEMBLYNAME = "<AssemblyName>";
			const string TOKEN_OUTPUTTYPE = "<OutputType>";
			string slnPath = string.Format("{0}{1}", Path.GetDirectoryName(solusionPath), Path.DirectorySeparatorChar);

			Dictionary<string, string> prjList = new Dictionary<string, string>();
			StreamReader srSln = null;
			ProjectInfo prjInfo = new ProjectInfo(slnPath);
			ArrayList alData = new ArrayList();
			int infoCnt = 0;

			try
			{
				string readData = string.Empty;

				srSln = new StreamReader(solusionPath);

				while (srSln.EndOfStream == false)
				{
					readData = srSln.ReadLine();

					// Project Keyword가 있으면 처리
					if (readData != null && readData.IndexOf("Project(") == 0 && readData.IndexOf(".csproj") > 0)
					{
						string[] parsedData = readData.Split(new string[] { "Project(", ") = ", ", ", "\"" }, StringSplitOptions.RemoveEmptyEntries);
						ProjectInfo pi = new ProjectInfo(slnPath);

						pi.ProjectName = parsedData[1];
						pi.ProjectFile = parsedData[2];

						prjInfo[infoCnt++] = pi;
					}
				}
			}
			catch (Exception ex)
			{
				throw new Exception(ex.Message);
			}
			finally
			{
				srSln.Close();
			}

			for (int i = 0; i < prjInfo.IndexerCount; i++)
			{
				StreamReader srPrj = null;

				try
				{
					ConstructGroup cGroup = null;
					ConstructGroup cg = null;
					string readData = string.Empty;
					int cGrpCnt = 0;

					srPrj = new StreamReader(prjInfo[i].ProjectFile);

					while (srPrj.EndOfStream == false)
					{
						readData = srPrj.ReadLine();

						// OutputType : WinExe, Exe, Class
						if (readData.IndexOf(TOKEN_OUTPUTTYPE) >= 0)
						{
							string[] parsedData = readData.Split(new string[] { TOKEN_OUTPUTTYPE, "<" }, StringSplitOptions.RemoveEmptyEntries);

							prjInfo[i].AssemblyType = 
								parsedData[1] == "Library" ? "dll" : "exe";

							if (cGroup == null)
								cGroup = new ConstructGroup();

							if (cg == null)
								cg = new ConstructGroup();
						}

						if (readData.IndexOf(TOKEN_ASSEMBLYNAME) >= 0)
						{
							string[] parsedData = readData.Split(new string[] { TOKEN_ASSEMBLYNAME, "<" }, StringSplitOptions.RemoveEmptyEntries);

							prjInfo[i].AssemblyName = parsedData[1];

							if (cGroup == null)
								cGroup = new ConstructGroup();

							if (cg == null)
								cg = new ConstructGroup();
						}

						if (readData.IndexOf("<PropertyGroup Condition") >= 0)
						{
							string[] parsedData = readData.Split(new string[] { "<PropertyGroup Condition=\" '$(Configuration)|$(Platform)' == '", "|" }, 
								StringSplitOptions.RemoveEmptyEntries);

							if (cGroup == null)
								cGroup = new ConstructGroup();

							if (cg == null)
								cg = new ConstructGroup();

							cg.Name = parsedData[1];
						}

						if (readData.IndexOf("<OutputPath>") >= 0)
						{
							string[] parsedData = readData.Split(new string[] { "<OutputPath>", "<" }, StringSplitOptions.RemoveEmptyEntries);

							if (cGroup == null)
								cGroup = new ConstructGroup();

							if (cg == null)
								cg = new ConstructGroup();

							cg.OutputPath = parsedData[1];

							cGroup[cGrpCnt++] = cg;
							cg = null;
						}
					}

					prjInfo[i].ConstructGroup = cGroup;
					cGroup = null;
				}
				catch (Exception ex)
				{
					throw new Exception(ex.Message);
				}
				finally
				{
					srPrj.Close();
				}
			}

			#region Debug print

			debugPrint(prjInfo);
			#endregion

			return prjInfo;
		}

		[Conditional("DEBUG")]
		private static void debugPrint(ProjectInfo prjInfo)
		{
			for (int i = 0; i < prjInfo.IndexerCount; i++)
			{
				Debug.Print("{0}\t{1}", prjInfo[i].ProjectName, prjInfo[i].AssemblyName);

				for (int j = 0; j < prjInfo[i].ConstructGroup.IndexerCount; j++)
				{
					Debug.Print("{0}\t", prjInfo[i].ConstructGroup[j].Name);
					Debug.Print("{0}\t", prjInfo[i].ConstructGroup[j].OutputPath);
				}

				Debug.Print(string.Empty);
				//Console.WriteLine(prjInfo[i].ProjectFile);
			}
		}
	}
}
