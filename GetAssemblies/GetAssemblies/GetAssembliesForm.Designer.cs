﻿namespace GetAssemblies
{
	partial class GetAssembliesForm
	{
		/// <summary>
		/// 필수 디자이너 변수입니다.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// 사용 중인 모든 리소스를 정리합니다.
		/// </summary>
		/// <param name="disposing">관리되는 리소스를 삭제해야 하면 true이고, 그렇지 않으면 false입니다.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form 디자이너에서 생성한 코드

		/// <summary>
		/// 디자이너 지원에 필요한 메서드입니다.
		/// 이 메서드의 내용을 코드 편집기로 수정하지 마십시오.
		/// </summary>
		private void InitializeComponent()
		{
			Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
			Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
			Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
			Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
			Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
			Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
			Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
			Infragistics.Win.Appearance appearance8 = new Infragistics.Win.Appearance();
			Infragistics.Win.Appearance appearance9 = new Infragistics.Win.Appearance();
			Infragistics.Win.Appearance appearance10 = new Infragistics.Win.Appearance();
			Infragistics.Win.Appearance appearance11 = new Infragistics.Win.Appearance();
			Infragistics.Win.Appearance appearance12 = new Infragistics.Win.Appearance();
			this.tblLpBorder = new System.Windows.Forms.TableLayoutPanel();
			this.tblLpTop = new System.Windows.Forms.TableLayoutPanel();
			this.label1 = new System.Windows.Forms.Label();
			this.label2 = new System.Windows.Forms.Label();
			this.txtSource = new System.Windows.Forms.TextBox();
			this.txtTarget = new System.Windows.Forms.TextBox();
			this.btnSource = new System.Windows.Forms.Button();
			this.btnTarget = new System.Windows.Forms.Button();
			this.btnCheck = new System.Windows.Forms.Button();
			this.btnCopy = new System.Windows.Forms.Button();
			this.xGrid = new Infragistics.Win.UltraWinGrid.UltraGrid();
			this.fbdTarget = new System.Windows.Forms.FolderBrowserDialog();
			this.ofdSource = new System.Windows.Forms.OpenFileDialog();
			this.tblLpBorder.SuspendLayout();
			this.tblLpTop.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.xGrid)).BeginInit();
			this.SuspendLayout();
			// 
			// tblLpBorder
			// 
			this.tblLpBorder.ColumnCount = 1;
			this.tblLpBorder.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
			this.tblLpBorder.Controls.Add(this.tblLpTop, 0, 0);
			this.tblLpBorder.Controls.Add(this.xGrid, 0, 1);
			this.tblLpBorder.Dock = System.Windows.Forms.DockStyle.Fill;
			this.tblLpBorder.Location = new System.Drawing.Point(0, 0);
			this.tblLpBorder.Name = "tblLpBorder";
			this.tblLpBorder.RowCount = 2;
			this.tblLpBorder.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 62F));
			this.tblLpBorder.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
			this.tblLpBorder.Size = new System.Drawing.Size(1016, 673);
			this.tblLpBorder.TabIndex = 0;
			// 
			// tblLpTop
			// 
			this.tblLpTop.ColumnCount = 7;
			this.tblLpTop.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 10F));
			this.tblLpTop.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 80F));
			this.tblLpTop.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
			this.tblLpTop.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 120F));
			this.tblLpTop.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
			this.tblLpTop.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 120F));
			this.tblLpTop.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 10F));
			this.tblLpTop.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
			this.tblLpTop.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
			this.tblLpTop.Controls.Add(this.label1, 1, 0);
			this.tblLpTop.Controls.Add(this.label2, 1, 1);
			this.tblLpTop.Controls.Add(this.txtSource, 2, 0);
			this.tblLpTop.Controls.Add(this.txtTarget, 2, 1);
			this.tblLpTop.Controls.Add(this.btnSource, 3, 0);
			this.tblLpTop.Controls.Add(this.btnTarget, 3, 1);
			this.tblLpTop.Controls.Add(this.btnCheck, 5, 0);
			this.tblLpTop.Controls.Add(this.btnCopy, 5, 1);
			this.tblLpTop.Dock = System.Windows.Forms.DockStyle.Fill;
			this.tblLpTop.Location = new System.Drawing.Point(3, 3);
			this.tblLpTop.Name = "tblLpTop";
			this.tblLpTop.RowCount = 2;
			this.tblLpTop.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
			this.tblLpTop.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
			this.tblLpTop.Size = new System.Drawing.Size(1010, 56);
			this.tblLpTop.TabIndex = 0;
			// 
			// label1
			// 
			this.label1.AutoSize = true;
			this.label1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.label1.Location = new System.Drawing.Point(13, 0);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(74, 28);
			this.label1.TabIndex = 0;
			this.label1.Text = "Source :";
			this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// label2
			// 
			this.label2.AutoSize = true;
			this.label2.Dock = System.Windows.Forms.DockStyle.Fill;
			this.label2.Location = new System.Drawing.Point(13, 28);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(74, 28);
			this.label2.TabIndex = 1;
			this.label2.Text = "Target :";
			this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// txtSource
			// 
			this.txtSource.Dock = System.Windows.Forms.DockStyle.Fill;
			this.txtSource.Location = new System.Drawing.Point(93, 3);
			this.txtSource.Name = "txtSource";
			this.txtSource.Size = new System.Drawing.Size(644, 21);
			this.txtSource.TabIndex = 4;
			this.txtSource.TextChanged += new System.EventHandler(this.txtSource_TextChanged);
			// 
			// txtTarget
			// 
			this.txtTarget.Dock = System.Windows.Forms.DockStyle.Fill;
			this.txtTarget.Location = new System.Drawing.Point(93, 31);
			this.txtTarget.Name = "txtTarget";
			this.txtTarget.Size = new System.Drawing.Size(644, 21);
			this.txtTarget.TabIndex = 5;
			// 
			// btnSource
			// 
			this.btnSource.Dock = System.Windows.Forms.DockStyle.Fill;
			this.btnSource.Location = new System.Drawing.Point(743, 3);
			this.btnSource.Name = "btnSource";
			this.btnSource.Size = new System.Drawing.Size(114, 22);
			this.btnSource.TabIndex = 0;
			this.btnSource.Text = "&Solusion File";
			this.btnSource.UseVisualStyleBackColor = true;
			this.btnSource.Click += new System.EventHandler(this.btnSource_Click);
			// 
			// btnTarget
			// 
			this.btnTarget.Dock = System.Windows.Forms.DockStyle.Fill;
			this.btnTarget.Location = new System.Drawing.Point(743, 31);
			this.btnTarget.Name = "btnTarget";
			this.btnTarget.Size = new System.Drawing.Size(114, 22);
			this.btnTarget.TabIndex = 2;
			this.btnTarget.Text = "&Browse";
			this.btnTarget.UseVisualStyleBackColor = true;
			this.btnTarget.Click += new System.EventHandler(this.btnTarget_Click);
			// 
			// btnCheck
			// 
			this.btnCheck.Dock = System.Windows.Forms.DockStyle.Fill;
			this.btnCheck.Location = new System.Drawing.Point(883, 3);
			this.btnCheck.Name = "btnCheck";
			this.btnCheck.Size = new System.Drawing.Size(114, 22);
			this.btnCheck.TabIndex = 1;
			this.btnCheck.Text = "Ch&eck";
			this.btnCheck.UseVisualStyleBackColor = true;
			this.btnCheck.Click += new System.EventHandler(this.btnCheck_Click);
			// 
			// btnCopy
			// 
			this.btnCopy.Dock = System.Windows.Forms.DockStyle.Fill;
			this.btnCopy.Location = new System.Drawing.Point(883, 31);
			this.btnCopy.Name = "btnCopy";
			this.btnCopy.Size = new System.Drawing.Size(114, 22);
			this.btnCopy.TabIndex = 3;
			this.btnCopy.Text = "&Copy";
			this.btnCopy.UseVisualStyleBackColor = true;
			this.btnCopy.Click += new System.EventHandler(this.btnCopy_Click);
			// 
			// xGrid
			// 
			appearance1.BackColor = System.Drawing.SystemColors.Window;
			appearance1.BorderColor = System.Drawing.SystemColors.InactiveCaption;
			this.xGrid.DisplayLayout.Appearance = appearance1;
			this.xGrid.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
			this.xGrid.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
			appearance2.BackColor = System.Drawing.SystemColors.ActiveBorder;
			appearance2.BackColor2 = System.Drawing.SystemColors.ControlDark;
			appearance2.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
			appearance2.BorderColor = System.Drawing.SystemColors.Window;
			this.xGrid.DisplayLayout.GroupByBox.Appearance = appearance2;
			appearance3.ForeColor = System.Drawing.SystemColors.GrayText;
			this.xGrid.DisplayLayout.GroupByBox.BandLabelAppearance = appearance3;
			this.xGrid.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
			appearance4.BackColor = System.Drawing.SystemColors.ControlLightLight;
			appearance4.BackColor2 = System.Drawing.SystemColors.Control;
			appearance4.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
			appearance4.ForeColor = System.Drawing.SystemColors.GrayText;
			this.xGrid.DisplayLayout.GroupByBox.PromptAppearance = appearance4;
			this.xGrid.DisplayLayout.MaxColScrollRegions = 1;
			this.xGrid.DisplayLayout.MaxRowScrollRegions = 1;
			appearance5.BackColor = System.Drawing.SystemColors.Window;
			appearance5.ForeColor = System.Drawing.SystemColors.ControlText;
			this.xGrid.DisplayLayout.Override.ActiveCellAppearance = appearance5;
			appearance6.BackColor = System.Drawing.SystemColors.Highlight;
			appearance6.ForeColor = System.Drawing.SystemColors.HighlightText;
			this.xGrid.DisplayLayout.Override.ActiveRowAppearance = appearance6;
			this.xGrid.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
			this.xGrid.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
			appearance7.BackColor = System.Drawing.SystemColors.Window;
			this.xGrid.DisplayLayout.Override.CardAreaAppearance = appearance7;
			appearance8.BorderColor = System.Drawing.Color.Silver;
			appearance8.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
			this.xGrid.DisplayLayout.Override.CellAppearance = appearance8;
			this.xGrid.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
			this.xGrid.DisplayLayout.Override.CellPadding = 0;
			appearance9.BackColor = System.Drawing.SystemColors.Control;
			appearance9.BackColor2 = System.Drawing.SystemColors.ControlDark;
			appearance9.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
			appearance9.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
			appearance9.BorderColor = System.Drawing.SystemColors.Window;
			this.xGrid.DisplayLayout.Override.GroupByRowAppearance = appearance9;
			appearance10.TextHAlignAsString = "Left";
			this.xGrid.DisplayLayout.Override.HeaderAppearance = appearance10;
			this.xGrid.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
			this.xGrid.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
			appearance11.BackColor = System.Drawing.SystemColors.Window;
			appearance11.BorderColor = System.Drawing.Color.Silver;
			this.xGrid.DisplayLayout.Override.RowAppearance = appearance11;
			this.xGrid.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
			appearance12.BackColor = System.Drawing.SystemColors.ControlLight;
			this.xGrid.DisplayLayout.Override.TemplateAddRowAppearance = appearance12;
			this.xGrid.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
			this.xGrid.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
			this.xGrid.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
			this.xGrid.Dock = System.Windows.Forms.DockStyle.Fill;
			this.xGrid.Location = new System.Drawing.Point(3, 65);
			this.xGrid.Name = "xGrid";
			this.xGrid.Size = new System.Drawing.Size(1010, 605);
			this.xGrid.TabIndex = 0;
			this.xGrid.Text = "ultraGrid1";
			this.xGrid.BeforeRowFilterDropDown += new Infragistics.Win.UltraWinGrid.BeforeRowFilterDropDownEventHandler(this.xGrid_BeforeRowFilterDropDown);
			this.xGrid.InitializeGroupByRow += new Infragistics.Win.UltraWinGrid.InitializeGroupByRowEventHandler(this.xGrid_InitializeGroupByRow);
			this.xGrid.AfterRowFilterChanged += new Infragistics.Win.UltraWinGrid.AfterRowFilterChangedEventHandler(this.xGrid_AfterRowFilterChanged);
			// 
			// ofdSource
			// 
			this.ofdSource.FileName = "openFileDialog1";
			// 
			// GetAssembliesForm
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(1016, 673);
			this.Controls.Add(this.tblLpBorder);
			this.Name = "GetAssembliesForm";
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Text = "Get Assemblies";
			this.Load += new System.EventHandler(this.GetAssembliesForm_Load);
			this.tblLpBorder.ResumeLayout(false);
			this.tblLpTop.ResumeLayout(false);
			this.tblLpTop.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.xGrid)).EndInit();
			this.ResumeLayout(false);

		}

		#endregion

		private System.Windows.Forms.TableLayoutPanel tblLpBorder;
		private System.Windows.Forms.TableLayoutPanel tblLpTop;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.TextBox txtSource;
		private System.Windows.Forms.TextBox txtTarget;
		private System.Windows.Forms.Button btnSource;
		private System.Windows.Forms.Button btnTarget;
		private System.Windows.Forms.Button btnCheck;
		private System.Windows.Forms.Button btnCopy;
		private Infragistics.Win.UltraWinGrid.UltraGrid xGrid;
		private System.Windows.Forms.FolderBrowserDialog fbdTarget;
		private System.Windows.Forms.OpenFileDialog ofdSource;
	}
}

