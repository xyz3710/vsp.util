﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

using System.IO;
using System.Collections;
using System.Diagnostics;
using Infragistics.Win.UltraWinGrid;
using Infragistics.Win;

namespace GetAssemblies
{
	public partial class GetAssembliesForm : Form
	{
		private bool _isFilterd;
		private GetAssemblies _getAsm;

		public GetAssembliesForm()
		{
			_getAsm = new GetAssemblies();

			InitializeComponent();
		}

		private void GetAssembliesForm_Load(object sender, EventArgs e)
		{
			initControl();
		}

		private void initControl()
		{
			btnCheck.Enabled = false;
			btnCopy.Enabled = false;

			txtSource.Text = string.Empty;
			txtTarget.Text = string.Empty;

			xGrid.DataSource = null;
			xGrid.DataBind();
		}
				
		private void xGrid_InitializeGroupByRow(object sender, InitializeGroupByRowEventArgs e)
		{
			e.Row.ExpandAll();
		}

		private void btnSource_Click(object sender, EventArgs e)
		{
			initControl();

			ofdSource.Filter = "솔루션 파일(*.sln)|*.sln|C# 프로젝트파일(*.csproj)|*.csproj";
			ofdSource.DefaultExt = "*.sln";
			ofdSource.FileName = string.Empty;

			DialogResult result = ofdSource.ShowDialog();

			if (result == DialogResult.OK)
			{
				txtSource.Text = ofdSource.FileName;

				txtTarget.Text = string.Format("{0}\\Bin", Path.GetDirectoryName(txtSource.Text));
			}
		}

		private void btnCheck_Click(object sender, EventArgs e)
		{
			_getAsm.SolutionPath = txtSource.Text;
			
			DataTable table = _getAsm.Check();

			xGrid.DataSource = table;

			xGrid.DisplayLayout.Bands[0].Columns["Project Name"].Width = 100;
			xGrid.DisplayLayout.Bands[0].Columns["Assembly Name"].Width = 130;
			xGrid.DisplayLayout.Bands[0].Columns["Project File"].Width = 430;
			xGrid.DisplayLayout.Bands[0].Columns["Property Group"].Width = 100;
			xGrid.DisplayLayout.Bands[0].Columns["Project Name"].Width = 140;
						
			xGrid.DisplayLayout.Bands[0].Columns["Assembly Path"].Hidden = true;
			xGrid.DisplayLayout.Bands[0].Columns["Document Path"].Hidden = true;			

			for (int i = 1; i < table.Columns.Count; i++)
			{
				if (table.Columns[i].Caption == "Result")
					continue;

				table.Columns[i].ReadOnly = true;
			}

			xGrid.DisplayLayout.Bands[0].Columns["Project Name"].AllowRowFiltering = Infragistics.Win.DefaultableBoolean.True;
			xGrid.DisplayLayout.Bands[0].Columns["Project Name"].FilterOperandStyle = FilterOperandStyle.DropDownList;
			xGrid.DisplayLayout.Bands[0].Columns["Project Name"].FilterOperatorDropDownItems = FilterOperatorDropDownItems.Contains;
			xGrid.DisplayLayout.Bands[0].Columns["Project Name"].FilterOperatorDefaultValue = FilterOperatorDefaultValue.Contains;
			xGrid.DisplayLayout.Bands[0].Columns["Property Group"].AllowRowFiltering = Infragistics.Win.DefaultableBoolean.True;
			xGrid.DisplayLayout.Bands[0].Columns["Property Group"].FilterOperandStyle = FilterOperandStyle.DropDownList;

			xGrid.DataBind();

			if ((sender as Button).Enabled == true && xGrid.Rows.Count > 0)
				btnCopy.Enabled = true;
			else
				btnCopy.Enabled = false;
		}

		private void btnTarget_Click(object sender, EventArgs e)
		{
			bool createdCurrent = false;			

			if (txtTarget.Text != string.Empty)
			{
				if (Directory.Exists(txtTarget.Text) == false)
				{
					Directory.CreateDirectory(txtTarget.Text);
					createdCurrent = true;
				}

				fbdTarget.SelectedPath = txtTarget.Text;
			}
			else
				fbdTarget.SelectedPath = Path.GetDirectoryName(txtSource.Text);

			DialogResult result = fbdTarget.ShowDialog();

			if (result == DialogResult.OK)
				txtTarget.Text = string.Format("{0}", fbdTarget.SelectedPath);

			if (result == DialogResult.Cancel && createdCurrent == true)
				Directory.Delete(txtTarget.Text);
		}

		private void btnCopy_Click(object sender, EventArgs e)
		{
			if (_isFilterd == false)
			{
				MessageBox.Show("You have to filter with the [Property Group] column header.", "Property Group", MessageBoxButtons.OK, MessageBoxIcon.Information);

				return;
			}

			_getAsm.TargetPath = txtTarget.Text;
			_getAsm.PropertyGroupCondition = string.Format("[Property Group] {0}", xGrid.DisplayLayout.Bands[0].ColumnFilters["Property Group"].ToString());
			_getAsm.ProjectNameFilterCondition = xGrid.DisplayLayout.Bands[0].ColumnFilters["Project Name"].ToString().Replace("'", "%'").Replace("Contains %'", "[Project Name] LIKE '%");

			string srcFile = string.Empty;
			string srcXml = string.Empty;
			string exceptionMsg = string.Empty;
			bool isSuccess = true;
			int fileCnt = 0;

			foreach (UltraGridRow xRow in xGrid.Rows)
			{
				if (xRow.Cells["Sel"].Value.ToString() == Boolean.TrueString && 
					(xRow.Cells["Exist"].Value.ToString() == Boolean.TrueString || xRow.Cells["Doc"].Value.ToString() == Boolean.TrueString) && 
					// 보여지는 row만 copy 한다
					xRow.VisibleIndex != -1)
				{
					srcFile = xRow.Cells["Assembly Path"].Value.ToString();
					srcXml = xRow.Cells["Document Path"].Value.ToString();

					isSuccess = _getAsm.Copy(srcFile, srcXml, out exceptionMsg);

					xRow.Cells["Result"].Value = true;
					xGrid.Update();
					fileCnt++;
				}
			}

			if (exceptionMsg != string.Empty)
				txtTarget.Text = exceptionMsg;
			
			if (isSuccess == true)
				MessageBox.Show(string.Format("{0} file(s) copy job was successful.", fileCnt), "Job success", 
					MessageBoxButtons.OK, MessageBoxIcon.Information);
			else
				MessageBox.Show(string.Format("{0} file(s) copy success,\r\nbut, job was failed.", fileCnt), "Job failure", 
					MessageBoxButtons.OK, MessageBoxIcon.Stop);
		}

		private void txtSource_TextChanged(object sender, EventArgs e)
		{
			enableButton(sender, btnCheck);
		}
		
		private void enableButton(object sender, object target)
		{
			(target as Button).Enabled = false;

			if ((sender as TextBox).Text != string.Empty)
				if (File.Exists((sender as TextBox).Text) == true)
					(target as Button).Enabled = true;
		}

		private void xGrid_AfterRowFilterChanged(object sender, AfterRowFilterChangedEventArgs e)
		{
			if (e.NewColumnFilter.FilterConditions.Count > 0)
				_isFilterd = true;
		}

		private void xGrid_BeforeRowFilterDropDown(object sender, BeforeRowFilterDropDownEventArgs e)
		{
			if (e.Column.ToString() == "Project Name")
			{
				ValueListItemsCollection valueLists = e.ValueList.ValueListItems;

				for (int i = valueLists.Count - 1; i >= 0; i--)
				{
					// Remove Blanks option from the filter drop down.
					if (valueLists[i].DisplayText == "(Blanks)")
					{
						e.ValueList.ValueListItems.RemoveAt(i);

						continue;
					}

					if (valueLists[i].DisplayText == "(NonBlanks)")
					{
						e.ValueList.ValueListItems.RemoveAt(i);

						continue;
					}
				}
			}

			if (e.Column.ToString() == "Property Group")
			{
				ValueListItemsCollection valueLists = e.ValueList.ValueListItems;

				for (int i = valueLists.Count - 1; i >= 0; i--)
				{
					// Remove Blanks option from the filter drop down.
					if (valueLists[i].DisplayText == "(Blanks)")
					{
						e.ValueList.ValueListItems.RemoveAt(i);

						continue;
					}

					if (valueLists[i].DisplayText == "(NonBlanks)")
					{
						e.ValueList.ValueListItems.RemoveAt(i);

						continue;
					}

					//  Remove Custom option from the filter drop down.
					if (valueLists[i].DisplayText == "(Custom)")
					{
						e.ValueList.ValueListItems.RemoveAt(i);

						continue;
					}

					//  Remove All option from the filter drop down.
					if (valueLists[i].DisplayText == "(All)")
					{
						e.ValueList.ValueListItems.RemoveAt(i);

						continue;
					}
				}
			}
		}
	}
}