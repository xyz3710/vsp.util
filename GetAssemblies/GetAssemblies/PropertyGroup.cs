using System;
using System.Collections.Generic;
using System.Windows.Forms;
using System.IO;
using System.Collections;

namespace GetAssemblies
{
	public class ConstructGroup
	{
		private string _name;
		private string _output;

		#region Constructor
		/// <summary>
		/// PropertyGroup class??instance�??�성?�니??
		/// </summary>
		public ConstructGroup()
		{
			_name = string.Empty;
			_output = string.Empty;
		}
		#endregion

		#region Indexer
		private ArrayList _alIndexer = new ArrayList();
		private int _indexerCnt;

		/// <summary>
		/// PropertyGroup Indexer
		/// </summary>
		/// <param name="index">index</param>
		/// <returns>PropertyGroup</returns>
		public ConstructGroup this[int index]
		{
			get
			{
				if (index > -1 && index < _alIndexer.Count)
					return (ConstructGroup)_alIndexer[index];
				else
					return null;
			}
			set
			{
				if (index > -1 && index < _alIndexer.Count)
					_alIndexer[index] = value;
				else if (index == _alIndexer.Count)
				{
					_alIndexer.Add(value);
					_indexerCnt++;
				}
				else
					throw new IndexOutOfRangeException();
			}
		}

		/// <summary>
		/// Indexer Count 
		/// </summary>
		public int IndexerCount
		{
			get
			{
				return _indexerCnt;
			}
		}
		#endregion

		#region Properties
		/// <summary>
		/// Project property name
		/// </summary>
		public String Name
		{
			get
			{
				return _name;
			}
			set
			{
				_name = value;
			}
		}
        
        /// <summary>
		/// Project property output path
        /// </summary>
		public string OutputPath
		{
			get
			{
				return _output;
			}
			set
			{
				_output = value;
			}
		}
        
        #endregion
        
	}
}
