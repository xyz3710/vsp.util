/**********************************************************************************************************************/
/*	Domain		:	
/*	Creator		:	Kim Ki Won
/*	Create		:	2007년 7월 27일 금요일 오전 11:58:46a
/*	Purpose		:	
/*	Modifier	:	
/*	Update		:	
/*	Changes		:	
/*	Comment		:	source:C:\iDASiT\iDASiT.sln pgroup:debug
/**********************************************************************************************************************/

using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.IO;

namespace GetAssemblies
{
	public class GetAssemblies
	{
		private string _solutionPath;
		private string _targetPath;
		private string _propertyGroup;
		private string _propertyGroupCondition;
		private string _projectNameFilter;
		private string _projectNameFilterCondition;
		private bool _isDocCopy;
		private DataTable _table;

		/// <summary>
		/// Initializes a new instance of the GetAssemblies class.
		/// </summary>
		public GetAssemblies(string sourcePath, string targetPath, string propertyGroup, string projectFilter, bool isDocCopy)
		{
			_solutionPath = sourcePath;
			_targetPath = targetPath;
			_propertyGroup = propertyGroup;
			_projectNameFilter = projectFilter;
			_isDocCopy = isDocCopy;
			_table = null;
		}

		/// <summary>
		/// Initializes a new instance of the GetAssemblies class.
		/// </summary>
		public GetAssemblies()
		{
			_solutionPath = string.Empty;
			_targetPath = string.Empty;
			_propertyGroup = string.Empty;
			_propertyGroupCondition = string.Empty;
			_projectNameFilter = string.Empty;
			_projectNameFilterCondition = string.Empty;
			_isDocCopy = true;
		}

		#region Properties
		/// <summary>
		/// Get or set SolutionPath.
		/// </summary>
		public string SolutionPath
		{
			get
			{
				return _solutionPath;
			}
			set
			{
				_solutionPath = value;
			}
		}
        
        /// <summary>
        /// Get or set TargetPath.
        /// </summary>
		public string TargetPath
		{
			get
			{
				if (_targetPath == string.Empty)
					_targetPath = string.Format(@"{0}\Bin", Path.GetDirectoryName(SolutionPath));

				return _targetPath;
			}
			set
			{
				_targetPath = value;
			}
		}

		/// <summary>
		/// Get or set PropertyGroup.(Debug, Release and so on...)
		/// </summary>
		public string PropertyGroup
		{
			get
			{
				return _propertyGroup;
			}
			set
			{
				_propertyGroup = value;
			}
		}


		/// <summary>
		/// Get or set PropertyGroup.(Debug, Release and so on...)
		/// </summary>
		public string PropertyGroupCondition
		{
			get
			{
				if (_propertyGroupCondition == string.Empty)
                	_propertyGroupCondition = string.Format("[Property Group] = '{0}'", _propertyGroup);

				return _propertyGroupCondition;
			}
			set
			{
				_propertyGroupCondition = value;
			}
		}

		/// <summary>
		/// Get or set ProjectFilter.(It same file filter *, ? ..)
		/// </summary>
		public string ProjectNameFilter
		{
			get
			{
				return _projectNameFilter;
			}
			set
			{
				_projectNameFilter = value;
			}
		}

		/// <summary>
		/// Get or set ProjectFilter.(It same file filter *, ? ..)
		/// </summary>
		public string ProjectNameFilterCondition
		{
			get
			{
				if (_projectNameFilterCondition == string.Empty && _projectNameFilter != string.Empty)
                	_projectNameFilterCondition = string.Format("[Project Name] LIKE '%{0}%'", _projectNameFilter);

				return _projectNameFilterCondition;
			}
			set
			{
				_projectNameFilterCondition = value;
			}
		}
        
        /// <summary>
        /// Get or set IsDocCopy.
        /// </summary>
		public bool IsDocCopy
		{
			get
			{
				return _isDocCopy;
			}
			set
			{
				_isDocCopy = value;
			}
		}
        
        #endregion

		public DataTable Check()
		{
			_table = new DataTable("SolutionInfo");
			ProjectInfo prjInfo = ProjectInfo.GetSolutionInfo(_solutionPath);
			
			_table.Columns.Add("Sel", typeof(bool));
			_table.Columns.Add("Project Name", typeof(string));
			_table.Columns.Add("Project File", typeof(string));
			_table.Columns.Add("Assembly Name", typeof(string));
			_table.Columns.Add("Property Group", typeof(string));
			_table.Columns.Add("Exist", typeof(bool));
			_table.Columns.Add("Doc", typeof(bool));
			_table.Columns.Add("Result", typeof(bool));
			_table.Columns.Add("Assembly Path", typeof(string));
			_table.Columns.Add("Document Path", typeof(string));

			for (int i = 0; i < prjInfo.IndexerCount; i++)
			{
				for (int j = 0; j < prjInfo[i].ConstructGroup.IndexerCount; j++)
				{
					string assemblyPath = string.Format("{0}\\{1}{2}",
						prjInfo[i].ProjectPath, prjInfo[i].ConstructGroup[j].OutputPath, prjInfo[i].AssemblyName);
					string docPath = string.Format("{0}\\{1}{2}.xml",
						prjInfo[i].ProjectPath, prjInfo[i].ConstructGroup[j].OutputPath, Path.GetFileNameWithoutExtension(prjInfo[i].AssemblyName));
					bool exists = false;
					bool existDocs = false;

					exists = File.Exists(assemblyPath);
					existDocs = File.Exists(docPath);

					object[] row = new object[] {
						exists
						,prjInfo[i].ProjectName
						,prjInfo[i].ProjectFile
						,prjInfo[i].AssemblyName
						,prjInfo[i].ConstructGroup[j].Name
						,exists
						,existDocs
						,false
						,assemblyPath
						,docPath
					};

					_table.Rows.Add(row);
				}
			}

			return _table;
		}

		public int Copy(out string exceptionMsg)
		{
			string query = string.Format("{0}{1}", PropertyGroupCondition,
				_projectNameFilter == string.Empty ? string.Empty : string.Format(" AND {0}", ProjectNameFilterCondition));
			DataRow[] rows = _table.Select(query);
			string srcFile = string.Empty;
			string srcXml = string.Empty;
			string destFile = string.Empty;
			string destXml = string.Empty;
			bool isSuccess = true;
			int fileCnt = 0;

			exceptionMsg = string.Empty;

			if (Directory.Exists(TargetPath) == false)
				Directory.CreateDirectory(TargetPath);

			foreach (DataRow dataRow in rows)
			{
				srcFile = dataRow["Assembly Path"].ToString();
				srcXml = dataRow["Document Path"].ToString();

				isSuccess = Copy(srcFile, srcXml, out exceptionMsg);
				fileCnt++;
			}

			if (isSuccess == true)
				Console.WriteLine(string.Format("{0} file(s) copy job was successful.", fileCnt));
			else
				Console.WriteLine(string.Format("{0} file(s) copy success, but, job was failed.", fileCnt));

			return fileCnt;
		}

		public bool Copy(string srcFile, string srcXml, out string exceptionMsg)
		{
			bool result = true;
			string destFile = string.Format("{0}\\{1}", _targetPath, Path.GetFileName(srcFile));
			string destXml = string.Format("{0}\\{1}", _targetPath, Path.GetFileName(srcXml));

			exceptionMsg = string.Empty;

			try
			{
				if (File.Exists(srcFile) == true)
					File.Copy(srcFile, destFile, true);

				if (_isDocCopy == true && File.Exists(srcXml) == true)
					File.Copy(srcXml, destXml, true);
			}
			catch (Exception ex)
			{
				Console.WriteLine(ex.Message);
				exceptionMsg = ex.Message;

				result = false;
			}

			return result;
		}
	}
}
