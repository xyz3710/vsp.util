﻿using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SevenZip;
using System.Threading;
using System.Reflection;

namespace _7ZipHelper
{
	class Program
	{
		private const string ROOT = @"E:\Test\CompressTest";
		private const string OUTPUT1 = @"E:\Test\CompressTest\Extracted1";
		private const string OUTPUT2 = @"E:\Test\CompressTest\Extracted2";
		private const string ZIP1 = @"E:\Test\CompressTest\7zipBasic1.7z";
		private const string ZIP2 = @"E:\Test\CompressTest\7zipBasic2.zip";
		private const string ZIP_MULTI1 = @"E:\Test\CompressTest\7zipMulti.zip";
		private const string FILE1 = @"E:\Test\CompressTest\Data\Data.sdf";
		private const string FILE2 = @"E:\Test\CompressTest\IMG_0220.JPG";
		private const string FOLDER = @"E:\Test\CompressTest\Image";

		static void Main(string[] args)
		{
			var targetRoot = @"E:\Test\CompressTest";
			var subFolder = "Data";

			//subFolder = "ARS_Server_Patch";
			//subFolder = "Image";

			//TestDotNetCompress(targetRoot, subFolder);
			//Test7ZipCompress(targetRoot, subFolder);

			TestProgram();
		}

		private static void SetProgress(SevenZipCompressor zip)
		{
			//zip.Compressing += (sender, e) =>
			//{
			//	Console.WriteLine($"Compressing: {e.PercentDone}%");
			//};
			zip.CompressionFinished += (sender, e) =>
			{
				Console.WriteLine($"\r\nCompressingFinished.\r\n");
			};
			zip.FileCompressionStarted += (sender, e) =>
			{
				Console.Write($"{e.FileName} => {e.PercentDone}%");
			};
			zip.FileCompressionFinished += (sender, e) =>
			{
				Console.WriteLine();
			};
		}

		private static void SetProgress(SevenZipExtractor zip)
		{
			//zip.Extracting += (sender, e) =>
			//{
			//	Console.Write($"\t\t{e.PercentDone}%\r\n");

			zip.ExtractionFinished += (sender, e) =>
			 {
				 Console.WriteLine($"\r\nExtractionFinished.\r\n");
			 };
			zip.FileExtractionStarted += (sender, e) =>
			{
				Console.Write($"{e.PercentDone,3}% => {e.FileInfo.FileName}");
			};
			zip.FileExtractionStarted += (sender, e) =>
			{
				Console.WriteLine();
			};
		}

		private static void TestProgram()
		{
			Console.WriteLine("SevenZipSharp test application.");

			//Console.ReadKey();

			/*
			 You may specify the custom path to 7-zip dll at SevenZipLibraryManager.LibraryFileName 
				or call SevenZipExtractor.SetLibraryPath(@"c:\Program Files\7-Zip\7z.dll");
				or call SevenZipCompressor.SetLibraryPath(@"c:\Program Files\7-Zip\7z.dll");
			 You may check if your library fits your goals with
				(SevenZipExtractor/Compressor.CurrentLibraryFeatures & LibraryFeature.<name>) != 0
			 Internal benchmark:
				var features = SevenZip.SevenZipExtractor.CurrentLibraryFeatures;
				Console.WriteLine(((uint)features).ToString("X6"));
			*/

			#region Temporary test
			//var features = SevenZip.SevenZipExtractor.CurrentLibraryFeatures;
			//Console.WriteLine(((uint)features).ToString("X6"));
			//Console.WriteLine();
			#endregion

			//CompressionVerySimple();

			//ExtractFiles();

			//CompressionsMultiVolumes();

			//ExtractMultiVolumes();

			//Compressions_Features_AppendMode();

			//Compressions_Features_ModifyMode();

			//Extraction_Features_ShowAndCancel();

			//Compressions_Features_ShowAndCancel();

			//MultiThreaded_ExractionTest();

			//MultiThreaded_CompressionsTest();

			//StreamingCompression();

			//StreamingExtraction();

			//CompressStreamManaged();

			//ExtractFileByStream();

			//ExtractFileToDisk();

			//CompressWithFormat_TAR();

			//CompressStreamInAndOut();       // Error 발생 

			CompressFileWithDictionary();       // Error 발생

			#region Toughness test - throws no exceptions and no leaks
			/*
			Console.ReadKey();
			string exeAssembly = Assembly.GetAssembly(typeof(SevenZipExtractor)).FullName;
			AppDomain dom = AppDomain.CreateDomain("Extract");
			for (int i = 0; i < 1000; i++)
			{
				using (SevenZipExtractor tmp = (SevenZipExtractor)dom.CreateInstance(
					exeAssembly, typeof(SevenZipExtractor).FullName,
					false, BindingFlags.CreateInstance, null,
					new object[] { ZIP1 },
					System.Globalization.CultureInfo.CurrentCulture, null, null).Unwrap())
				{
					tmp.ExtractArchive(OUTPUT2);
				}
				Console.Clear();
				Console.WriteLine(i);
			}
			AppDomain.Unload(dom);
			//No errors, no leaks*/
			#endregion

			#region Serialization demo
			/*ArgumentException ex = new ArgumentException("blahblah");
			BinaryFormatter bf = new BinaryFormatter();
			using (MemoryStream ms = new MemoryStream())
			{
				bf.Serialize(ms, ex);
				SevenZipCompressor cmpr = new SevenZipCompressor();
				cmpr.CompressStream(ms, File.Create(@"d:\Temp\test.7z"));
			}
			//*/
			#endregion

			#region Compress with custom parameters demo
			/*var tmp = new SevenZipCompressor();            
			tmp.ArchiveFormat = OutArchiveFormat.Zip;
			tmp.CompressionMethod = CompressionMethod.Deflate;
			tmp.CompressionLevel = CompressionLevel.Ultra;
			//Number of fast bytes
			tmp.CustomParameters.Add("fb", "256");
			//Number of deflate passes
			tmp.CustomParameters.Add("pass", "4");
			//Multi-threading on
			tmp.CustomParameters.Add("mt", "on");
			tmp.ZipEncryptionMethod = ZipEncryptionMethod.AES256;
			tmp.Compressing += (s, e) =>
			{
				Console.Clear();
				Console.WriteLine(String.Format("{0}%", e.PercentDone));
			};
			tmp.CompressDirectory(@"d:\Temp\!Пусто", @"d:\Temp\arch.zip", "test");
			//*/

			/*SevenZipCompressor tmp = new SevenZipCompressor();
			tmp.CompressionMethod = CompressionMethod.Ppmd;
			tmp.CompressionLevel = CompressionLevel.Ultra;
			tmp.EncryptHeadersSevenZip = true;
			tmp.ScanOnlyWritable = true;
			tmp.CompressDirectory(@"d:\Temp\!Пусто", @"d:\Temp\arch.7z", "test");  
			//*/
			#endregion

			#region Sfx demo
			/*var sfx = new SevenZipSfx();
			SevenZipCompressor tmp = new SevenZipCompressor();
			using (MemoryStream ms = new MemoryStream())
			{
				tmp.CompressDirectory(@"d:\Temp\!Пусто", ms);               
				sfx.MakeSfx(ms, @"d:\Temp\test.exe");
			}
			//*/
			#endregion

			#region Lzma Encode/Decode Stream test
			/*using (var output = new FileStream(@"d:\Temp\arch.lzma", FileMode.Create))
			{
				var encoder = new LzmaEncodeStream(output);
				using (var inputSample = new FileStream(@"d:\Temp\tolstoi_lev_voina_i_mir_kniga_1.rtf", FileMode.Open))
				{
					int bufSize = 24576, count;
					byte[] buf = new byte[bufSize];
					while ((count = inputSample.Read(buf, 0, bufSize)) > 0)
					{
						encoder.Write(buf, 0, count);
					}
				}
				encoder.Close();
			}//*/
			/*using (var input = new FileStream(@"d:\Temp\arch.lzma", FileMode.Open))
			{
				var decoder = new LzmaDecodeStream(input);
				using (var output = new FileStream(@"d:\Temp\res.rtf", FileMode.Create))
				{
					int bufSize = 24576, count;
					byte[] buf = new byte[bufSize];
					while ((count = decoder.Read(buf, 0, bufSize)) > 0)
					{
						output.Write(buf, 0, count);
					}
				}
			}//*/
			#endregion
		}

		private static void CompressionVerySimple()
		{
			Console.WriteLine("\r\nCompression very simple.");

			//SevenZipCompressor.SetLibraryPath(@"C:\Program Files\Util\7-Zip\7z.dll");
			var zip = new SevenZipCompressor();

			SetProgress(zip);

			zip.ScanOnlyWritable = true;
			zip.CompressFiles(ZIP1, FILE1);
			zip.CompressDirectory(FOLDER, ZIP2);
		}

		private static void CompressionsMultiVolumes()
		{
			Console.WriteLine("\r\nCompressions multi volumes");

			var zip = new SevenZipCompressor();

			zip.VolumeSize = 5000000;
			zip.CompressDirectory(FOLDER, ZIP_MULTI1);
		}

		private static void ExtractFiles()
		{
			Console.WriteLine("\r\nExtract files");

			using (var zip = new SevenZipExtractor(ZIP2))
			{
				SetProgress(zip);

				for (int i = 0; i < zip.ArchiveFileData.Count; i++)
				{
					zip.ExtractFiles(OUTPUT1, zip.ArchiveFileData[i].Index);
				}

				Console.WriteLine("Extract 1, 3, 5 index of archive.");

				// To extract more than 1 file at a time or when you definitely know which files to extract,	// use something like
				zip.ExtractFiles(OUTPUT2, 1, 3, 5);
			}
		}

		private static void ExtractMultiVolumes()
		{
			Console.WriteLine("\r\nExtract multi volumes");

			//SevenZipExtractor.SetLibraryPath(@"d:\Work\Misc\7zip\9.04\CPP\7zip\Bundles\Format7zF\7z.dll");
			using (var zip = new SevenZipExtractor(ZIP_MULTI1 + ".001"))
			{
				SetProgress(zip);

				if (Directory.Exists(OUTPUT2) == true)
				{
					Directory.Delete(OUTPUT2, true);
				}

				zip.ExtractArchive(OUTPUT2);
			}
		}

		private static void Compressions_Features_AppendMode()
		{
			Console.WriteLine("\r\nCompressions features append mode");

			var zip = new SevenZipCompressor();

			SetProgress(zip);
			zip.CompressionMode = SevenZip.CompressionMode.Append;
			zip.CompressDirectory(FOLDER, ZIP1);
			zip = null;
		}

		private static void Compressions_Features_ModifyMode()
		{
			Console.WriteLine("\r\nCompressions features modify mode");

			var zip = new SevenZipCompressor();

			SetProgress(zip);
			zip.ModifyArchive(ZIP1, new Dictionary<int, string>() { { 0, "xxx0.bat" } });
			//Delete
			zip.ModifyArchive(ZIP1, new Dictionary<int, string>() { { 12, null }, { 1, null } });
		}

		private static void Extraction_Features_ShowAndCancel()
		{
			Console.WriteLine("\r\nExtraction features show and cancel");

			using (var zip = new SevenZipExtractor(ZIP1))
			{
				//SetProgress(zip);

				zip.FileExtractionStarted += (s, e) =>
				{
					if (e.FileInfo.Index == 10)
					{
						e.Cancel = true;
						Console.WriteLine("Cancelled");
					}
					else
					{
						Console.WriteLine(String.Format("{0,3}% => {1}", e.PercentDone, e.FileInfo.FileName));
					}
				};
				zip.FileExists += (o, e) =>
				{
					Console.WriteLine("Warning: file \"" + e.FileName + "\" already exists.");
				};
				zip.ExtractionFinished += (s, e) =>
				{
					Console.WriteLine("Finished!");
				};
				zip.ExtractArchive(OUTPUT1);
			}
		}

		private static void Compressions_Features_ShowAndCancel()
		{
			Console.WriteLine("\r\nCompressions features show and cancel.");

			if (File.Exists(ZIP1) == true)
			{
				File.Delete(ZIP1);
			}

			var zip = new SevenZipCompressor();
			zip.ArchiveFormat = OutArchiveFormat.SevenZip;
			zip.CompressionLevel = SevenZip.CompressionLevel.High;
			zip.CompressionMethod = CompressionMethod.Ppmd;
			zip.FileCompressionStarted += (s, e) =>
			{
				if (e.PercentDone > 50)
				{
					e.Cancel = true;
					Console.WriteLine("Canceled");
				}
				else
				{
					Console.WriteLine(String.Format("{0,3}% => {1}", e.PercentDone, e.FileName));
				}
			};

			zip.FilesFound += (se, ea) =>
			{
				Console.WriteLine("Number of files: " + ea.Value.ToString());
			};

			zip.CompressFiles(ZIP1, FILE1);
			zip.CompressDirectory(FOLDER, ZIP1);
		}

		private static void MultiThreaded_ExractionTest()
		{
			Console.WriteLine("\r\nMulti threaded extraction test.");

			var t1 = new Thread(() =>
						{
							using (var zip = new SevenZipExtractor(ZIP2))
							{
								zip.FileExtractionStarted += (s, e) =>
								{
									Console.WriteLine($"{Thread.CurrentThread.ManagedThreadId} => [{e.PercentDone}%] {e.FileInfo.FileName}");
								};
								zip.ExtractionFinished += (s, e) =>
								{
									Console.WriteLine($"{Thread.CurrentThread.ManagedThreadId} => Finished!");
								};
								zip.ExtractArchive(OUTPUT1);
							}
						});
			var t2 = new Thread(() =>
			{
				using (var zip = new SevenZipExtractor(ZIP2))
				{
					zip.FileExtractionStarted += (s, e) =>
					{
						Console.WriteLine($"{Thread.CurrentThread.ManagedThreadId} => [{e.PercentDone}%] {e.FileInfo.FileName}");
					};
					zip.ExtractionFinished += (s, e) =>
					{
						Console.WriteLine($"{Thread.CurrentThread.ManagedThreadId} => Finished!");
					};
					zip.ExtractArchive(OUTPUT2);
				}
			});
			t1.Start();
			t2.Start();
			t1.Join();
			t2.Join();
		}

		private static void MultiThreaded_CompressionsTest()
		{
			Console.WriteLine("\r\nMulti threaded compressions test.");

			var t1 = new Thread(() =>
			{
				var zip = new SevenZipCompressor();

				zip.FileCompressionStarted += (s, e) =>
					Console.WriteLine($"{Thread.CurrentThread.ManagedThreadId} => [{e.PercentDone}%]");

				zip.CompressDirectory(FOLDER, ZIP1);
			});
			var t2 = new Thread(() =>
			{
				var zip = new SevenZipCompressor();

				zip.FileCompressionStarted += (s, e) =>
					Console.WriteLine($"{Thread.CurrentThread.ManagedThreadId} => [{e.PercentDone}%]");

				zip.CompressDirectory(FOLDER, ZIP2);
			});
			t1.Start();
			t2.Start();
			t1.Join();
			t2.Join();
		}

		private static void StreamingExtraction()
		{
			Console.WriteLine("\r\nStreaming extraction.");

			using (var zip = new SevenZipExtractor(File.OpenRead(ZIP2)))
			{
				zip.FileExtractionStarted += (s, e) =>
				{
					Console.WriteLine(String.Format("[{0}%] {1}", e.PercentDone, e.FileInfo.FileName));
				};
				zip.ExtractionFinished += (s, e) =>
				{
					Console.WriteLine("Finished!");
				};
				zip.ExtractArchive(OUTPUT2);
			}
		}

		private static void StreamingCompression()
		{
			Console.WriteLine("\r\nStreaming compressions.");

			var zip = new SevenZipCompressor();

			zip.FileCompressionStarted += (s, e) =>
			{
				Console.WriteLine(String.Format("[{0}%] {1}", e.PercentDone, e.FileName));
			};
			zip.CompressDirectory(FOLDER, File.Create(ZIP1));
		}

		private static void CompressStreamManaged()
		{
			Console.WriteLine("\r\nCompress stream managed.");

			SevenZipCompressor.CompressStream(File.OpenRead(FILE1), File.Create(ZIP2), null, (o, e) =>
			{
				if (e.PercentDelta > 0)
				{
					Console.WriteLine(e.PercentDone.ToString() + "%");
				}
			});
		}

		private static void ExtractFileByStream()
		{
			Console.WriteLine("\r\nExtract file by stream");

			using (var zip = new SevenZipExtractor(ZIP1))
			{
				zip.FileExtractionStarted += (s, e) =>
				{
					Console.WriteLine(String.Format("[{0}%] {1}", e.PercentDone, e.FileInfo.FileName));
				};
				zip.FileExists += (o, e) =>
				{
					Console.WriteLine("Warning: file \"" + e.FileName + "\" already exists.");
					//e.Overwrite = false;
				};
				zip.ExtractionFinished += (s, e) =>
				{
					Console.WriteLine("Finished!");
				};
				zip.ExtractFile(1, File.Create(FILE2));
			}
		}

		private static void ExtractFileToDisk()
		{
			Console.WriteLine("\r\nExtract file to disk");

			using (var zip = new SevenZipExtractor(ZIP1))
			{
				zip.FileExtractionStarted += (s, e) =>
				{
					Console.WriteLine(String.Format("[{0}%] {1}", e.PercentDone, e.FileInfo.FileName));
				};
				zip.FileExists += (o, e) =>
				{
					Console.WriteLine("Warning: file \"" + e.FileName + "\" already exists.");
					//e.Overwrite = false;
				};
				zip.ExtractionFinished += (s, e) =>
				{
					Console.WriteLine("Finished!");
				};
				zip.ExtractFile(4, File.OpenWrite(FILE2));
			}
		}

		private static void CompressWithFormat_TAR()
		{
			Console.WriteLine("\r\nCompress with file format");

			try
			{
				if (File.Exists(ZIP2) == true)
				{
					File.Delete(ZIP2);
				}
			}
			catch (Exception ex)
			{
				Console.WriteLine(ex.Message);
			}

			var zip = new SevenZipCompressor();

			SetProgress(zip);
			zip.ArchiveFormat = OutArchiveFormat.Tar;
			//zip.CompressFiles(ZIP1, FILE1, FILE2);
			zip.CompressFiles(@"E:\Test\CompressTest\7zipBasic1.tar", FILE1, FILE2);
		}

		private static void CompressStreamInAndOut()
		{
			Console.WriteLine("\r\nCompress by in stream and out stream. but ERROR !!!");

			// Error 발생 
			var zip = new SevenZipCompressor();

			SetProgress(zip);
			zip.CompressStream(
				File.OpenRead(FILE2),
				File.Create(ZIP2));
		}

		private static void CompressFileWithDictionary()
		{
			var tmp = new SevenZipCompressor();
			Dictionary<string, string> fileDict = new Dictionary<string, string>();
			fileDict.Add("IMG_0219.JPG", @"E:\Test\CompressTest\Image\IMG_0219.JPG");
			tmp.FileCompressionStarted += (o, e) =>
			{
				Console.WriteLine(String.Format("[{0}%] {1}", e.PercentDone, e.FileName));
			};
			tmp.CompressFileDictionary(fileDict, @"E:\Test\CompressTest\arch.7z");

			return;
			// 에러 발생
			//Console.WriteLine("\r\nCompress File With Dictionary, but ERROR!!!");

			//var zip = new SevenZipCompressor();

			//SetProgress(zip);
			//Dictionary<string, string> fileDict = new Dictionary<string, string>();
			//fileDict.Add(Path.GetFileName(FILE2), FILE2);
			//zip.FileCompressionStarted += (o, e) =>
			//{
			//	Console.WriteLine(String.Format("[{0}%] {1}", e.PercentDone, e.FileName));
			//};
			//zip.CompressFileDictionary(fileDict, ZIP2);
		}

		/// <summary>
		/// 압축된 이미지는 7zip이나 크기가 차이가 없다.
		/// </summary>
		/// <param name="targetRoot">The target root.</param>
		/// <param name="subFolder">The sub folder.</param>
		private static void TestDotNetCompress(string targetRoot, string subFolder)
		{
			var target = Path.Combine(targetRoot, subFolder);
			string zipFile = Path.Combine(targetRoot, "DotNetCompress.zip");

			DateTime performanceTestStart150135 = DateTime.Now;
			Console.WriteLine(string.Format("Start\t{0}", performanceTestStart150135), "Program.TestDotNetCompress");

			DotNetCompress(target, zipFile);

			Console.WriteLine(string.Format("Stop \t{0}", DateTime.Now - performanceTestStart150135), "Program.TestDotNetCompress");

			DateTime performanceTestStart150141 = DateTime.Now;
			Console.WriteLine(string.Format("Start\t{0}", performanceTestStart150141), "Program.TestDotNetCompress");

			DotNetDeCompress(zipFile, $@"{targetRoot}\x{subFolder}");

			Console.WriteLine(string.Format("Stop \t{0}", DateTime.Now - performanceTestStart150141), "Program.TestDotNetCompress");
		}

		private static void DotNetCompress(string targetRoot, string zipFile)
		{
			if (File.Exists(zipFile) == true)
			{
				File.Delete(zipFile);
			}

			ZipFile.CreateFromDirectory(targetRoot, zipFile);
		}

		private static void DotNetDeCompress(string zipFile, string targetRoot)
		{
			if (Directory.Exists(targetRoot) == true)
			{
				Directory.Delete(targetRoot, true);
			}

			ZipFile.ExtractToDirectory(zipFile, targetRoot);
		}

		private static void Test7ZipCompress(string targetRoot, string subFolder)
		{
			var target = Path.Combine(targetRoot, subFolder);
			string zipFile = Path.Combine(targetRoot, "7zipCompress.zip");

			//SevenZipCompressor.SetLibraryPath(@"C:\Program Files\Util\7-Zip\7z.dll");

			var zip = new SevenZipCompressor();

			zip.FileCompressionStarted += (sender, e) =>
			{
				Console.Write($"{e.FileName} => {e.PercentDone}");
			};
			zip.FileCompressionFinished += (sender, e) =>
			{
				Console.WriteLine();
			};

			//zip.Compressing += (sender, e) =>
			//{
			//	Console.WriteLine("{0}", e.PercentDone);
			//};
			zip.CompressDirectory(target, zipFile);
		}
	}
}
