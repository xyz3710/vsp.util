﻿// ****************************************************************************************************************** //
//	Domain		:	RssDownloader.MainForm
//	Creator		:	DEV\xyz37(Kim Ki Won)
//	Create		:	2019년 12월 26일 목요일 23:38
//	Purpose		:	
// ------------------------------------------------------------------------------------------------------------------ //
//	Modifier	:	
//	Update		:	
//	Changes		:	
// ------------------------------------------------------------------------------------------------------------------ //
//	Comment		:	
// ------------------------------------------------------------------------------------------------------------------ //
//	Reviewer	:	
//	Rev. Date	:	
//	Comment		:	
//  http://minicast.imbc.com/PodCast/pod.aspx?code=1002488100000100000
//	http://enabler.kbs.co.kr/api/podcast_channel/feed.xml?channel_id=R2016-0125
//	https://rss.art19.com/samgukji
//	https://www.listennotes.com/podcasts/%EB%B0%B0%ED%95%9C%EC%84%B1%EC%9D%98-%EA%B3%A0%EC%A0%84%EC%97%B4%EC%A0%84-%EC%A2%85%EC%98%81-imbc-0XUJa1EpOGW/
//	https://github.com/codehollow/FeedReader
// ------------------------------------------------------------------------------------------------------------------ //
//	<copyright file="MainForm.cs" company="(주)가치소프트">
//		Copyright (c) 2019. (주)가치소프트. All rights reserved.
//	</copyright>
// <summary></summary>
// ****************************************************************************************************************** //

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.ServiceModel.Syndication;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;

namespace RssDownloader
{
	public partial class MainForm : Form
	{
		public MainForm()
		{
			InitializeComponent();

			btnQuery.Click += OnQueryClick;
		}

		protected override void OnShown(EventArgs e)
		{
			base.OnShown(e);
			btnQuery.Focus();
		}

		private void OnQueryClick(object sender, EventArgs e)
		{
			//var feed = SyndicationFeed.Load();

			var uri = cboRss.Text;
			PodcastSyndicationFeed feed = null;

			using (var reader = XmlReader.Create(uri))
			{
				feed = SyndicationFeed.Load<PodcastSyndicationFeed>(reader);
			}

			foreach (var item in feed.Items.OrderBy(x => x.PublishDate).ToList())
			{
				var title = item.Title.Text;
				var desc = item.Summary.Text;

				Console.WriteLine($"T:{title}, D:{desc}");
			}
		}
	}
}
