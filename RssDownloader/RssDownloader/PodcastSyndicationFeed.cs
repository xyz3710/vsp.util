﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel.Syndication;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace RssDownloader
{
	public class PodcastSyndicationFeed : SyndicationFeed
	{
		public string Summary { get; set; }

		protected override bool TryParseElement(XmlReader reader, string version)
		{
			if (reader.Name == "itunes:summary")
			{
				//Summary = reader
			}

			return base.TryParseElement(reader, version);
		}
	}
}
