﻿using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Resources;
using System.Diagnostics;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("BatteryChecker")]
[assembly: AssemblyDescription("Battery Checker in Windows XP")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("XYZ37")]
[assembly: AssemblyProduct("BatteryChecker")]
[assembly: AssemblyCopyright("Copyright (C) XYZ37 2009")]
[assembly: AssemblyTrademark("GhostWorker(Kim Ki Won)")]
[assembly: AssemblyCulture("")]

// Setting ComVisible to false makes the types in this assembly not visible 
// to COM components.  If you need to access a type in this assembly from 
// COM, set the ComVisible attribute to true on that type.
[assembly: ComVisible(false)]

// The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly: Guid("ff55455a-143d-48ca-b3b4-770cf9d17475")]

// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version 
//      Build Number
//      Revision
//
[assembly: AssemblyFileVersion("1.0909.0213.0001")]
[assembly: AssemblyVersion("1.0909.0213.0001")]
// Added by AnyFactory.CI.AssemblyInfoTask2 in 2009.09.02 13:20:57
[assembly: Debuggable(DebuggableAttribute.DebuggingModes.EnableEditAndContinue | DebuggableAttribute.DebuggingModes.DisableOptimizations | DebuggableAttribute.DebuggingModes.IgnoreSymbolStoreSequencePoints)]
