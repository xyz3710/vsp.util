﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Threading;
using System.Resources;
using System.IO;
using System.Reflection;

namespace BatteryChecker
{
	public partial class BatteryCheckerForm : Form
	{
		private const string SHOWGRAPH = "그래프 보기(&G)";
		private const string HIDEGRAPH = "그래프 숨기기(&G)";
		private Thread _checker;
		private PowerStatus _powerStatus;
		private bool _loopCondition;
		private Icon _acIcon;
		private Icon _batIcon;
		private Icon _noBatIcon;
		private float _interval;

		private Graphics _pbGrp;
		private SolidBrush _contextBrush;
		private Font _contextFont;

		public BatteryCheckerForm()
		{
			InitializeComponent();
		}

		private void Form1_Load(object sender, EventArgs e)
		{
			Assembly assembly = Assembly.GetExecutingAssembly();
			// 포함시 아이콘 파일 속성에서 Build Action : Embedded Resource 선택
			// namespace.folder명.아이콘 파일
			Stream streamAC = assembly.GetManifestResourceStream("BatteryChecker.Resources.AC.ico");
			Stream streamBat = assembly.GetManifestResourceStream("BatteryChecker.Resources.Bat.ico");
			Stream streamNoBat = assembly.GetManifestResourceStream("BatteryChecker.Resources.NoBat.ico");

			_acIcon = new Icon(streamAC);
			_batIcon = new Icon(streamBat);
			_noBatIcon = new Icon(streamNoBat);

			_pbGrp = pbBattery.CreateGraphics();
			_contextBrush = new SolidBrush(Color.Yellow);
			_contextFont = new Font("Tahoma", 9, FontStyle.Bold);

			_powerStatus = SystemInformation.PowerStatus;

			_interval = 2.5F;
			_checker = new Thread(new ThreadStart(batteryChecker));
			_loopCondition = true;
			_checker.Start();
			_checker.Join(10);

			this.Location = new Point(Screen.PrimaryScreen.WorkingArea.Width - this.Width, 
				Screen.PrimaryScreen.WorkingArea.Height- this.Height);

			niBattery.Visible = true;
		}

		private void batteryChecker()
		{
			int percent;
			string txtPercent;

			while (_loopCondition == true)
			{
				this.Invoke(new MethodInvoker(delegate()
				{
					percent = Convert.ToInt32(_powerStatus.BatteryLifePercent * 100);

					if (percent == 100)
					{
						txtPercent = powerLineStatus() +
							percent.ToString("###\\%");
					}
					else
					{
						txtPercent = powerLineStatus() +
							batteryChargeStatus() + 
							percent.ToString("###\\%");
					}
					
					pbBattery.Value = percent;
					niBattery.BalloonTipText = txtPercent;
					niBattery.Text = txtPercent;
						this.Text = txtPercent;

					drawPercentText(txtPercent);
				}));

				Thread.Sleep((int)_interval * 1000);
			}
		}

		private void drawPercentText(string txtPercent)
		{
			// Draw text
			SizeF size = _pbGrp.MeasureString(txtPercent, _contextFont);

			// clear text
			_pbGrp.Clear(Color.LawnGreen);
			pbBattery.Refresh();
			_pbGrp.DrawString(txtPercent, _contextFont, _contextBrush, 
				((int)this.ClientSize.Width / 2) - ((int)size.Width / 2),
				((int)this.ClientSize.Height / 2) - ((int)size.Height / 2));

			size = SizeF.Empty;
		}

		private void Form1_FormClosing(object sender, FormClosingEventArgs e)
		{
			if (_loopCondition == true)
			{
				e.Cancel = true;
				this.Hide();
			}
		}

		private string powerLineStatus()
		{
			string result = string.Empty;

			switch (_powerStatus.PowerLineStatus)
			{
				case PowerLineStatus.Offline:
					break;
				case PowerLineStatus.Online:
					iconChange(IconState.AC);

					result = "온라인,";

					break;
				case PowerLineStatus.Unknown:
					break;
				default:
					break;
			}

			return result;
		}

		private string batteryChargeStatus()
		{
			string result = string.Empty;
			BatteryChargeStatus current = _powerStatus.BatteryChargeStatus;

			if ((current & BatteryChargeStatus.Charging) == BatteryChargeStatus.Charging)
			{
				result = "충전:";

				iconChange(IconState.AC);
			}
			else
			{
				switch (current)
				{
					case BatteryChargeStatus.Critical:
						result = "방전(위험):";
						iconChange(IconState.Battery);

						break;
					case BatteryChargeStatus.NoSystemBattery:
						result = "배터리없음:";
						iconChange(IconState.Unknown);

						break;
					case BatteryChargeStatus.Unknown:
						result = "모름:";
						iconChange(IconState.Unknown);

						break;
					case BatteryChargeStatus.High:
					case BatteryChargeStatus.Low:
					default:
						result = "방전:";
						iconChange(IconState.Battery);

						break;
				}
			}

			return result;
		}

		private void iconChange(IconState iconState)
		{
			Icon targetIcon = null;

			switch (iconState)
			{
				case IconState.AC:
					targetIcon = _acIcon;

					break;
				case IconState.Battery:
					targetIcon = _batIcon;

					break;
				case IconState.Unknown:
					targetIcon = _noBatIcon;

					break;
			}

			niBattery.Icon = targetIcon;
			this.Icon = targetIcon;
		}

		private void tsmAbout_Click(object sender, EventArgs e)
		{

		}

		private void tsmGraph_Click(object sender, EventArgs e)
		{
			if (tsmGraph.Text.Equals(SHOWGRAPH) == true)
			{
				tsmGraph.Text = HIDEGRAPH;
				this.Show();
			}
			else
			{
				tsmGraph.Text = SHOWGRAPH;
				this.Hide();
			}
		}

		private void tsmTransparent_Click(object sender, EventArgs e)
		{
			if (tsmTransparent.Checked == true)
			{
				this.TransparencyKey = Color.Empty;
				tsmTransparent.Checked = false;
			}
			else
			{
				this.TransparencyKey = Color.LawnGreen;
				tsmTransparent.Checked = true;
			}
		}
		
		private void tsmExit_Click(object sender, EventArgs e)
		{
			_loopCondition = false;
			this.Close();
		}

		private void tscboInterval_SelectedIndexChanged(object sender, EventArgs e)
		{
			if (Microsoft.VisualBasic.Information.IsNumeric(tscboInterval.SelectedItem) == true)
				_interval = float.Parse((string)tscboInterval.SelectedItem);
		}

	}
}