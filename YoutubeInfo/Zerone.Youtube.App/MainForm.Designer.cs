﻿namespace Zerone.Youtube.App
{
	partial class MainForm
	{
		/// <summary>
		///  Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		///  Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		///  Required method for Designer support - do not modify
		///  the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			components = new System.ComponentModel.Container();
			DataGridViewCellStyle dataGridViewCellStyle1 = new DataGridViewCellStyle();
			DataGridViewCellStyle dataGridViewCellStyle2 = new DataGridViewCellStyle();
			DataGridViewCellStyle dataGridViewCellStyle3 = new DataGridViewCellStyle();
			DataGridViewCellStyle dataGridViewCellStyle4 = new DataGridViewCellStyle();
			DataGridViewCellStyle dataGridViewCellStyle5 = new DataGridViewCellStyle();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
			MainMenu = new MenuStrip();
			tsFile = new ToolStripMenuItem();
			tsFileQuit = new ToolStripMenuItem();
			tsSetting = new ToolStripMenuItem();
			tsCurrentTime = new ToolStripMenuItem();
			tlpMain = new TableLayoutPanel();
			tlpButtons = new TableLayoutPanel();
			btnStart = new Button();
			btnStop = new Button();
			tableLayoutPanel1 = new TableLayoutPanel();
			lbNextTime = new Label();
			lbActionPeriod = new Label();
			label1 = new Label();
			label2 = new Label();
			tlpManual = new TableLayoutPanel();
			tableLayoutPanel3 = new TableLayoutPanel();
			label5 = new Label();
			dtpManualTo = new DateTimePicker();
			dtpManualFrom = new DateTimePicker();
			btnManual = new Button();
			tcMain = new TabControl();
			tpLog = new TabPage();
			teLog = new ICSharpCode.TextEditor.TextEditorControl();
			tpDbLog = new TabPage();
			tlpDbLog = new TableLayoutPanel();
			dgvDataLog = new Zuby.ADGV.AdvancedDataGridView();
			chId = new DataGridViewTextBoxColumn();
			chRequestDate = new DataGridViewTextBoxColumn();
			chRequestTime = new DataGridViewTextBoxColumn();
			chRequestActionType = new DataGridViewTextBoxColumn();
			chRequestTable = new DataGridViewTextBoxColumn();
			chRequestQuery = new DataGridViewTextBoxColumn();
			chResponseCount = new DataGridViewTextBoxColumn();
			chResponseResult = new DataGridViewTextBoxColumn();
			chErrorMessage = new DataGridViewTextBoxColumn();
			loggerBindingSource = new BindingSource(components);
			tableLayoutPanel2 = new TableLayoutPanel();
			label4 = new Label();
			dtpLogTo = new DateTimePicker();
			btnQueryLog = new Button();
			dtpLogFrom = new DateTimePicker();
			label3 = new Label();
			MainMenu.SuspendLayout();
			tlpMain.SuspendLayout();
			tlpButtons.SuspendLayout();
			tableLayoutPanel1.SuspendLayout();
			tlpManual.SuspendLayout();
			tableLayoutPanel3.SuspendLayout();
			tcMain.SuspendLayout();
			tpLog.SuspendLayout();
			tpDbLog.SuspendLayout();
			tlpDbLog.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)dgvDataLog).BeginInit();
			((System.ComponentModel.ISupportInitialize)loggerBindingSource).BeginInit();
			tableLayoutPanel2.SuspendLayout();
			SuspendLayout();
			// 
			// MainMenu
			// 
			MainMenu.Font = new Font("맑은 고딕", 11F, FontStyle.Regular, GraphicsUnit.Point);
			MainMenu.Items.AddRange(new ToolStripItem[] { tsFile, tsSetting, tsCurrentTime });
			MainMenu.Location = new Point(0, 0);
			MainMenu.Name = "MainMenu";
			MainMenu.Padding = new Padding(8, 3, 0, 3);
			MainMenu.Size = new Size(1044, 30);
			MainMenu.TabIndex = 0;
			MainMenu.Text = "menuStrip1";
			// 
			// tsFile
			// 
			tsFile.DropDownItems.AddRange(new ToolStripItem[] { tsFileQuit });
			tsFile.Name = "tsFile";
			tsFile.Size = new Size(44, 24);
			tsFile.Text = "&File";
			// 
			// tsFileQuit
			// 
			tsFileQuit.Name = "tsFileQuit";
			tsFileQuit.Size = new Size(108, 24);
			tsFileQuit.Text = "&Quit";
			// 
			// tsSetting
			// 
			tsSetting.Name = "tsSetting";
			tsSetting.ShortcutKeyDisplayString = "Alt+O";
			tsSetting.ShortcutKeys = Keys.Alt | Keys.O;
			tsSetting.Size = new Size(69, 24);
			tsSetting.Text = "&Setting";
			// 
			// tsCurrentTime
			// 
			tsCurrentTime.Alignment = ToolStripItemAlignment.Right;
			tsCurrentTime.Name = "tsCurrentTime";
			tsCurrentTime.Overflow = ToolStripItemOverflow.AsNeeded;
			tsCurrentTime.Size = new Size(86, 24);
			tsCurrentTime.Text = "현재 시간";
			// 
			// tlpMain
			// 
			tlpMain.ColumnCount = 1;
			tlpMain.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 100F));
			tlpMain.Controls.Add(tlpButtons, 0, 0);
			tlpMain.Controls.Add(tcMain, 0, 1);
			tlpMain.Dock = DockStyle.Fill;
			tlpMain.Location = new Point(0, 30);
			tlpMain.Margin = new Padding(4);
			tlpMain.Name = "tlpMain";
			tlpMain.RowCount = 3;
			tlpMain.RowStyles.Add(new RowStyle(SizeType.Absolute, 90F));
			tlpMain.RowStyles.Add(new RowStyle(SizeType.Percent, 100F));
			tlpMain.RowStyles.Add(new RowStyle(SizeType.Absolute, 20F));
			tlpMain.Size = new Size(1044, 911);
			tlpMain.TabIndex = 1;
			// 
			// tlpButtons
			// 
			tlpButtons.ColumnCount = 4;
			tlpButtons.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 27.77778F));
			tlpButtons.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 27.7777786F));
			tlpButtons.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 44.44444F));
			tlpButtons.ColumnStyles.Add(new ColumnStyle(SizeType.Absolute, 283F));
			tlpButtons.Controls.Add(btnStart, 0, 0);
			tlpButtons.Controls.Add(btnStop, 1, 0);
			tlpButtons.Controls.Add(tableLayoutPanel1, 3, 0);
			tlpButtons.Controls.Add(tlpManual, 2, 0);
			tlpButtons.Dock = DockStyle.Fill;
			tlpButtons.Location = new Point(1, 1);
			tlpButtons.Margin = new Padding(1);
			tlpButtons.Name = "tlpButtons";
			tlpButtons.RowCount = 1;
			tlpButtons.RowStyles.Add(new RowStyle(SizeType.Percent, 100F));
			tlpButtons.RowStyles.Add(new RowStyle(SizeType.Absolute, 20F));
			tlpButtons.Size = new Size(1042, 88);
			tlpButtons.TabIndex = 0;
			// 
			// btnStart
			// 
			btnStart.Dock = DockStyle.Fill;
			btnStart.Image = Resource.Player_Play32;
			btnStart.ImageAlign = ContentAlignment.MiddleLeft;
			btnStart.Location = new Point(3, 3);
			btnStart.Name = "btnStart";
			btnStart.Padding = new Padding(30, 0, 0, 0);
			btnStart.Size = new Size(204, 82);
			btnStart.TabIndex = 0;
			btnStart.Text = "&Start";
			btnStart.UseVisualStyleBackColor = true;
			// 
			// btnStop
			// 
			btnStop.Dock = DockStyle.Fill;
			btnStop.Image = Resource.Player_Stop32;
			btnStop.ImageAlign = ContentAlignment.MiddleLeft;
			btnStop.Location = new Point(213, 3);
			btnStop.Name = "btnStop";
			btnStop.Padding = new Padding(30, 0, 0, 0);
			btnStop.Size = new Size(204, 82);
			btnStop.TabIndex = 1;
			btnStop.Text = "S&top";
			btnStop.UseVisualStyleBackColor = true;
			// 
			// tableLayoutPanel1
			// 
			tableLayoutPanel1.CellBorderStyle = TableLayoutPanelCellBorderStyle.Single;
			tableLayoutPanel1.ColumnCount = 2;
			tableLayoutPanel1.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 33.6917572F));
			tableLayoutPanel1.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 66.30824F));
			tableLayoutPanel1.Controls.Add(lbNextTime, 1, 1);
			tableLayoutPanel1.Controls.Add(lbActionPeriod, 1, 0);
			tableLayoutPanel1.Controls.Add(label1, 0, 0);
			tableLayoutPanel1.Controls.Add(label2, 0, 1);
			tableLayoutPanel1.Dock = DockStyle.Fill;
			tableLayoutPanel1.Location = new Point(760, 3);
			tableLayoutPanel1.Name = "tableLayoutPanel1";
			tableLayoutPanel1.RowCount = 2;
			tableLayoutPanel1.RowStyles.Add(new RowStyle(SizeType.Percent, 50F));
			tableLayoutPanel1.RowStyles.Add(new RowStyle(SizeType.Percent, 50F));
			tableLayoutPanel1.RowStyles.Add(new RowStyle(SizeType.Absolute, 20F));
			tableLayoutPanel1.Size = new Size(279, 82);
			tableLayoutPanel1.TabIndex = 3;
			// 
			// lbNextTime
			// 
			lbNextTime.AutoSize = true;
			lbNextTime.Dock = DockStyle.Fill;
			lbNextTime.Location = new Point(97, 41);
			lbNextTime.Name = "lbNextTime";
			lbNextTime.Size = new Size(178, 40);
			lbNextTime.TabIndex = 4;
			lbNextTime.Text = "[지정 간격] 29:29 남음";
			lbNextTime.TextAlign = ContentAlignment.MiddleCenter;
			// 
			// lbActionPeriod
			// 
			lbActionPeriod.AutoSize = true;
			lbActionPeriod.Dock = DockStyle.Fill;
			lbActionPeriod.Location = new Point(97, 1);
			lbActionPeriod.Name = "lbActionPeriod";
			lbActionPeriod.Size = new Size(178, 39);
			lbActionPeriod.TabIndex = 3;
			lbActionPeriod.Text = "작동 시간";
			lbActionPeriod.TextAlign = ContentAlignment.MiddleCenter;
			// 
			// label1
			// 
			label1.AutoSize = true;
			label1.Dock = DockStyle.Fill;
			label1.Location = new Point(4, 1);
			label1.Name = "label1";
			label1.Size = new Size(86, 39);
			label1.TabIndex = 0;
			label1.Text = "작동 시간";
			label1.TextAlign = ContentAlignment.MiddleRight;
			// 
			// label2
			// 
			label2.AutoSize = true;
			label2.Dock = DockStyle.Fill;
			label2.Location = new Point(4, 41);
			label2.Name = "label2";
			label2.Size = new Size(86, 40);
			label2.TabIndex = 0;
			label2.Text = "다음 요청";
			label2.TextAlign = ContentAlignment.MiddleRight;
			// 
			// tlpManual
			// 
			tlpManual.ColumnCount = 1;
			tlpManual.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 100F));
			tlpManual.Controls.Add(tableLayoutPanel3, 0, 0);
			tlpManual.Controls.Add(btnManual, 0, 1);
			tlpManual.Dock = DockStyle.Fill;
			tlpManual.Enabled = false;
			tlpManual.Location = new Point(420, 0);
			tlpManual.Margin = new Padding(0);
			tlpManual.Name = "tlpManual";
			tlpManual.RowCount = 2;
			tlpManual.RowStyles.Add(new RowStyle(SizeType.Absolute, 36F));
			tlpManual.RowStyles.Add(new RowStyle(SizeType.Percent, 100F));
			tlpManual.Size = new Size(337, 88);
			tlpManual.TabIndex = 4;
			// 
			// tableLayoutPanel3
			// 
			tableLayoutPanel3.ColumnCount = 3;
			tableLayoutPanel3.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 50F));
			tableLayoutPanel3.ColumnStyles.Add(new ColumnStyle(SizeType.Absolute, 30F));
			tableLayoutPanel3.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 50F));
			tableLayoutPanel3.ColumnStyles.Add(new ColumnStyle(SizeType.Absolute, 20F));
			tableLayoutPanel3.ColumnStyles.Add(new ColumnStyle(SizeType.Absolute, 20F));
			tableLayoutPanel3.Controls.Add(label5, 1, 0);
			tableLayoutPanel3.Controls.Add(dtpManualTo, 2, 0);
			tableLayoutPanel3.Controls.Add(dtpManualFrom, 0, 0);
			tableLayoutPanel3.Dock = DockStyle.Fill;
			tableLayoutPanel3.Location = new Point(1, 0);
			tableLayoutPanel3.Margin = new Padding(1, 0, 1, 0);
			tableLayoutPanel3.Name = "tableLayoutPanel3";
			tableLayoutPanel3.RowCount = 1;
			tableLayoutPanel3.RowStyles.Add(new RowStyle(SizeType.Percent, 100F));
			tableLayoutPanel3.Size = new Size(335, 36);
			tableLayoutPanel3.TabIndex = 2;
			// 
			// label5
			// 
			label5.AutoSize = true;
			label5.Dock = DockStyle.Fill;
			label5.Location = new Point(155, 0);
			label5.Name = "label5";
			label5.Size = new Size(24, 36);
			label5.TabIndex = 3;
			label5.Text = "~";
			label5.TextAlign = ContentAlignment.MiddleRight;
			// 
			// dtpManualTo
			// 
			dtpManualTo.CustomFormat = "yyyy-MM-dd HH:mm";
			dtpManualTo.Dock = DockStyle.Fill;
			dtpManualTo.Format = DateTimePickerFormat.Custom;
			dtpManualTo.Location = new Point(185, 4);
			dtpManualTo.Margin = new Padding(3, 4, 3, 3);
			dtpManualTo.Name = "dtpManualTo";
			dtpManualTo.ShowUpDown = true;
			dtpManualTo.Size = new Size(147, 27);
			dtpManualTo.TabIndex = 1;
			dtpManualTo.Value = new DateTime(2023, 7, 20, 7, 0, 0, 0);
			// 
			// dtpManualFrom
			// 
			dtpManualFrom.CustomFormat = "yyyy-MM-dd HH:mm";
			dtpManualFrom.Dock = DockStyle.Fill;
			dtpManualFrom.Format = DateTimePickerFormat.Custom;
			dtpManualFrom.Location = new Point(3, 4);
			dtpManualFrom.Margin = new Padding(3, 4, 3, 3);
			dtpManualFrom.Name = "dtpManualFrom";
			dtpManualFrom.ShowUpDown = true;
			dtpManualFrom.Size = new Size(146, 27);
			dtpManualFrom.TabIndex = 0;
			dtpManualFrom.Value = new DateTime(2023, 7, 20, 7, 0, 0, 0);
			// 
			// btnManual
			// 
			btnManual.Dock = DockStyle.Fill;
			btnManual.Image = Resource.Player_Fastforward32;
			btnManual.ImageAlign = ContentAlignment.MiddleLeft;
			btnManual.Location = new Point(3, 39);
			btnManual.Name = "btnManual";
			btnManual.Padding = new Padding(30, 0, 0, 0);
			btnManual.Size = new Size(331, 46);
			btnManual.TabIndex = 3;
			btnManual.Text = "&Manual 요청";
			btnManual.UseVisualStyleBackColor = true;
			// 
			// tcMain
			// 
			tcMain.Controls.Add(tpLog);
			tcMain.Controls.Add(tpDbLog);
			tcMain.Dock = DockStyle.Fill;
			tcMain.Location = new Point(3, 93);
			tcMain.Name = "tcMain";
			tcMain.SelectedIndex = 0;
			tcMain.Size = new Size(1038, 795);
			tcMain.TabIndex = 2;
			// 
			// tpLog
			// 
			tpLog.Controls.Add(teLog);
			tpLog.Location = new Point(4, 29);
			tpLog.Name = "tpLog";
			tpLog.Padding = new Padding(3);
			tpLog.Size = new Size(1030, 762);
			tpLog.TabIndex = 0;
			tpLog.Text = "실시간 로그(Ctrl+1)";
			tpLog.UseVisualStyleBackColor = true;
			// 
			// teLog
			// 
			teLog.Dock = DockStyle.Fill;
			teLog.Highlighting = "SQL";
			teLog.LineViewerStyle = ICSharpCode.TextEditor.Document.LineViewerStyle.FullRow;
			teLog.Location = new Point(3, 3);
			teLog.Name = "teLog";
			teLog.ShowLineNumbers = false;
			teLog.ShowMatchingBracket = false;
			teLog.ShowVRuler = false;
			teLog.Size = new Size(1024, 756);
			teLog.TabIndex = 0;
			// 
			// tpDbLog
			// 
			tpDbLog.Controls.Add(tlpDbLog);
			tpDbLog.Location = new Point(4, 24);
			tpDbLog.Name = "tpDbLog";
			tpDbLog.Padding = new Padding(3);
			tpDbLog.Size = new Size(1030, 767);
			tpDbLog.TabIndex = 1;
			tpDbLog.Text = "데이터 로그(Ctrl+2)";
			tpDbLog.UseVisualStyleBackColor = true;
			// 
			// tlpDbLog
			// 
			tlpDbLog.ColumnCount = 2;
			tlpDbLog.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 50F));
			tlpDbLog.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 50F));
			tlpDbLog.Controls.Add(dgvDataLog, 0, 1);
			tlpDbLog.Controls.Add(tableLayoutPanel2, 0, 0);
			tlpDbLog.Dock = DockStyle.Fill;
			tlpDbLog.Location = new Point(3, 3);
			tlpDbLog.Name = "tlpDbLog";
			tlpDbLog.RowCount = 2;
			tlpDbLog.RowStyles.Add(new RowStyle(SizeType.Absolute, 36F));
			tlpDbLog.RowStyles.Add(new RowStyle(SizeType.Percent, 100F));
			tlpDbLog.Size = new Size(1024, 761);
			tlpDbLog.TabIndex = 0;
			// 
			// dgvDataLog
			// 
			dgvDataLog.AllowUserToAddRows = false;
			dgvDataLog.AllowUserToDeleteRows = false;
			dgvDataLog.AllowUserToOrderColumns = true;
			dgvDataLog.AutoGenerateColumns = false;
			dgvDataLog.ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.AutoSize;
			dgvDataLog.Columns.AddRange(new DataGridViewColumn[] { chId, chRequestDate, chRequestTime, chRequestActionType, chRequestTable, chRequestQuery, chResponseCount, chResponseResult, chErrorMessage });
			tlpDbLog.SetColumnSpan(dgvDataLog, 2);
			dgvDataLog.DataSource = loggerBindingSource;
			dgvDataLog.Dock = DockStyle.Fill;
			dgvDataLog.FilterAndSortEnabled = true;
			dgvDataLog.FilterStringChangedInvokeBeforeDatasourceUpdate = true;
			dgvDataLog.Location = new Point(3, 39);
			dgvDataLog.Name = "dgvDataLog";
			dgvDataLog.ReadOnly = true;
			dgvDataLog.RightToLeft = RightToLeft.No;
			dgvDataLog.RowTemplate.Height = 25;
			dgvDataLog.Size = new Size(1018, 719);
			dgvDataLog.SortStringChangedInvokeBeforeDatasourceUpdate = true;
			dgvDataLog.TabIndex = 0;
			// 
			// chId
			// 
			chId.DataPropertyName = "Id";
			chId.HeaderText = "Id";
			chId.MinimumWidth = 22;
			chId.Name = "chId";
			chId.ReadOnly = true;
			chId.SortMode = DataGridViewColumnSortMode.Programmatic;
			chId.Visible = false;
			// 
			// chRequestDate
			// 
			chRequestDate.DataPropertyName = "RequestDate";
			dataGridViewCellStyle1.Alignment = DataGridViewContentAlignment.MiddleCenter;
			dataGridViewCellStyle1.Format = "d";
			dataGridViewCellStyle1.NullValue = null;
			chRequestDate.DefaultCellStyle = dataGridViewCellStyle1;
			chRequestDate.HeaderText = "요청 일자";
			chRequestDate.MinimumWidth = 22;
			chRequestDate.Name = "chRequestDate";
			chRequestDate.ReadOnly = true;
			chRequestDate.SortMode = DataGridViewColumnSortMode.Programmatic;
			// 
			// chRequestTime
			// 
			chRequestTime.DataPropertyName = "RequestTime";
			dataGridViewCellStyle2.Alignment = DataGridViewContentAlignment.MiddleCenter;
			dataGridViewCellStyle2.Format = "T";
			dataGridViewCellStyle2.NullValue = null;
			chRequestTime.DefaultCellStyle = dataGridViewCellStyle2;
			chRequestTime.HeaderText = "요청 시간";
			chRequestTime.MinimumWidth = 22;
			chRequestTime.Name = "chRequestTime";
			chRequestTime.ReadOnly = true;
			chRequestTime.SortMode = DataGridViewColumnSortMode.Programmatic;
			// 
			// chRequestActionType
			// 
			chRequestActionType.DataPropertyName = "RequestActionType";
			dataGridViewCellStyle3.Alignment = DataGridViewContentAlignment.MiddleCenter;
			chRequestActionType.DefaultCellStyle = dataGridViewCellStyle3;
			chRequestActionType.HeaderText = "요청 타입";
			chRequestActionType.MinimumWidth = 22;
			chRequestActionType.Name = "chRequestActionType";
			chRequestActionType.ReadOnly = true;
			chRequestActionType.SortMode = DataGridViewColumnSortMode.Programmatic;
			// 
			// chRequestTable
			// 
			chRequestTable.DataPropertyName = "RequestTable";
			chRequestTable.HeaderText = "요청 테이블";
			chRequestTable.MinimumWidth = 22;
			chRequestTable.Name = "chRequestTable";
			chRequestTable.ReadOnly = true;
			chRequestTable.SortMode = DataGridViewColumnSortMode.Programmatic;
			chRequestTable.Width = 120;
			// 
			// chRequestQuery
			// 
			chRequestQuery.DataPropertyName = "RequestQuery";
			chRequestQuery.HeaderText = "요청 쿼리";
			chRequestQuery.MinimumWidth = 22;
			chRequestQuery.Name = "chRequestQuery";
			chRequestQuery.ReadOnly = true;
			chRequestQuery.SortMode = DataGridViewColumnSortMode.Programmatic;
			// 
			// chResponseCount
			// 
			chResponseCount.DataPropertyName = "ResponseCount";
			dataGridViewCellStyle4.Alignment = DataGridViewContentAlignment.MiddleRight;
			dataGridViewCellStyle4.Format = "N0";
			dataGridViewCellStyle4.NullValue = null;
			chResponseCount.DefaultCellStyle = dataGridViewCellStyle4;
			chResponseCount.HeaderText = "응답 건수";
			chResponseCount.MinimumWidth = 22;
			chResponseCount.Name = "chResponseCount";
			chResponseCount.ReadOnly = true;
			chResponseCount.SortMode = DataGridViewColumnSortMode.Programmatic;
			// 
			// chResponseResult
			// 
			chResponseResult.DataPropertyName = "ResponseResult";
			dataGridViewCellStyle5.Alignment = DataGridViewContentAlignment.MiddleCenter;
			chResponseResult.DefaultCellStyle = dataGridViewCellStyle5;
			chResponseResult.HeaderText = "응답 결과";
			chResponseResult.MinimumWidth = 22;
			chResponseResult.Name = "chResponseResult";
			chResponseResult.ReadOnly = true;
			chResponseResult.SortMode = DataGridViewColumnSortMode.Programmatic;
			// 
			// chErrorMessage
			// 
			chErrorMessage.DataPropertyName = "ErrorMessage";
			chErrorMessage.HeaderText = "에러 메세지";
			chErrorMessage.MinimumWidth = 22;
			chErrorMessage.Name = "chErrorMessage";
			chErrorMessage.ReadOnly = true;
			chErrorMessage.SortMode = DataGridViewColumnSortMode.Programmatic;
			chErrorMessage.Width = 200;
			// 
			// loggerBindingSource
			// 
			loggerBindingSource.DataSource = typeof(Dac.Logger);
			// 
			// tableLayoutPanel2
			// 
			tableLayoutPanel2.ColumnCount = 6;
			tableLayoutPanel2.ColumnStyles.Add(new ColumnStyle(SizeType.Absolute, 110F));
			tableLayoutPanel2.ColumnStyles.Add(new ColumnStyle(SizeType.Absolute, 120F));
			tableLayoutPanel2.ColumnStyles.Add(new ColumnStyle(SizeType.Absolute, 30F));
			tableLayoutPanel2.ColumnStyles.Add(new ColumnStyle(SizeType.Absolute, 120F));
			tableLayoutPanel2.ColumnStyles.Add(new ColumnStyle(SizeType.Absolute, 20F));
			tableLayoutPanel2.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 100F));
			tableLayoutPanel2.Controls.Add(label4, 2, 0);
			tableLayoutPanel2.Controls.Add(dtpLogTo, 3, 0);
			tableLayoutPanel2.Controls.Add(btnQueryLog, 5, 0);
			tableLayoutPanel2.Controls.Add(dtpLogFrom, 1, 0);
			tableLayoutPanel2.Controls.Add(label3, 0, 0);
			tableLayoutPanel2.Dock = DockStyle.Fill;
			tableLayoutPanel2.Location = new Point(3, 0);
			tableLayoutPanel2.Margin = new Padding(3, 0, 3, 0);
			tableLayoutPanel2.Name = "tableLayoutPanel2";
			tableLayoutPanel2.RowCount = 1;
			tableLayoutPanel2.RowStyles.Add(new RowStyle(SizeType.Percent, 100F));
			tableLayoutPanel2.Size = new Size(506, 36);
			tableLayoutPanel2.TabIndex = 1;
			// 
			// label4
			// 
			label4.AutoSize = true;
			label4.Dock = DockStyle.Fill;
			label4.Location = new Point(233, 0);
			label4.Name = "label4";
			label4.Size = new Size(24, 36);
			label4.TabIndex = 3;
			label4.Text = "~";
			label4.TextAlign = ContentAlignment.MiddleRight;
			// 
			// dtpLogTo
			// 
			dtpLogTo.CustomFormat = "yyyy-MM-dd";
			dtpLogTo.Dock = DockStyle.Fill;
			dtpLogTo.Format = DateTimePickerFormat.Custom;
			dtpLogTo.Location = new Point(263, 4);
			dtpLogTo.Margin = new Padding(3, 4, 3, 3);
			dtpLogTo.Name = "dtpLogTo";
			dtpLogTo.ShowUpDown = true;
			dtpLogTo.Size = new Size(114, 27);
			dtpLogTo.TabIndex = 1;
			dtpLogTo.Value = new DateTime(2023, 7, 20, 7, 0, 0, 0);
			// 
			// btnQueryLog
			// 
			btnQueryLog.Dock = DockStyle.Fill;
			btnQueryLog.Image = Resource.Document_Search24;
			btnQueryLog.Location = new Point(403, 3);
			btnQueryLog.Name = "btnQueryLog";
			btnQueryLog.Size = new Size(100, 30);
			btnQueryLog.TabIndex = 2;
			btnQueryLog.UseVisualStyleBackColor = true;
			// 
			// dtpLogFrom
			// 
			dtpLogFrom.CustomFormat = "yyyy-MM-dd";
			dtpLogFrom.Dock = DockStyle.Fill;
			dtpLogFrom.Format = DateTimePickerFormat.Custom;
			dtpLogFrom.Location = new Point(113, 4);
			dtpLogFrom.Margin = new Padding(3, 4, 3, 3);
			dtpLogFrom.Name = "dtpLogFrom";
			dtpLogFrom.ShowUpDown = true;
			dtpLogFrom.Size = new Size(114, 27);
			dtpLogFrom.TabIndex = 0;
			dtpLogFrom.Value = new DateTime(2023, 7, 20, 7, 0, 0, 0);
			// 
			// label3
			// 
			label3.AutoSize = true;
			label3.Dock = DockStyle.Fill;
			label3.Location = new Point(3, 0);
			label3.Name = "label3";
			label3.Size = new Size(104, 36);
			label3.TabIndex = 0;
			label3.Text = "요청 일자";
			label3.TextAlign = ContentAlignment.MiddleRight;
			// 
			// MainForm
			// 
			AutoScaleDimensions = new SizeF(9F, 20F);
			AutoScaleMode = AutoScaleMode.Font;
			ClientSize = new Size(1044, 941);
			Controls.Add(tlpMain);
			Controls.Add(MainMenu);
			Font = new Font("맑은 고딕", 11F, FontStyle.Regular, GraphicsUnit.Point);
			Icon = (Icon)resources.GetObject("$this.Icon");
			MainMenuStrip = MainMenu;
			Margin = new Padding(4);
			Name = "MainForm";
			StartPosition = FormStartPosition.CenterScreen;
			Text = "CKDBio Veeva Interface Batch Form";
			MainMenu.ResumeLayout(false);
			MainMenu.PerformLayout();
			tlpMain.ResumeLayout(false);
			tlpButtons.ResumeLayout(false);
			tableLayoutPanel1.ResumeLayout(false);
			tableLayoutPanel1.PerformLayout();
			tlpManual.ResumeLayout(false);
			tableLayoutPanel3.ResumeLayout(false);
			tableLayoutPanel3.PerformLayout();
			tcMain.ResumeLayout(false);
			tpLog.ResumeLayout(false);
			tpDbLog.ResumeLayout(false);
			tlpDbLog.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)dgvDataLog).EndInit();
			((System.ComponentModel.ISupportInitialize)loggerBindingSource).EndInit();
			tableLayoutPanel2.ResumeLayout(false);
			tableLayoutPanel2.PerformLayout();
			ResumeLayout(false);
			PerformLayout();
		}

		#endregion

		private MenuStrip MainMenu;
		private TableLayoutPanel tlpMain;
		private ToolStripMenuItem tsFile;
		private ToolStripMenuItem tsFileQuit;
		private ToolStripMenuItem tsSetting;
		private TableLayoutPanel tlpButtons;
		private Button btnStart;
		private Button btnStop;
		private Button btnManual;
		private TabControl tcMain;
		private TabPage tpLog;
		private TabPage tpDbLog;
		private Zuby.ADGV.AdvancedDataGridView dgvDataLog;
		private TableLayoutPanel tableLayoutPanel1;
		private Label label1;
		private Label label2;
		private Label lbActionPeriod;
		private Label lbNextTime;
		private ToolStripMenuItem tsCurrentTime;
		private ICSharpCode.TextEditor.TextEditorControl teLog;
		private TableLayoutPanel tlpDbLog;
		private TableLayoutPanel tableLayoutPanel2;
		private Label label3;
		private DateTimePicker dtpLogFrom;
		private Button btnQueryLog;
		private Label label4;
		private DateTimePicker dtpLogTo;
		private BindingSource loggerBindingSource;
		private DataGridViewTextBoxColumn chId;
		private DataGridViewTextBoxColumn chRequestDate;
		private DataGridViewTextBoxColumn chRequestTime;
		private DataGridViewTextBoxColumn chRequestActionType;
		private DataGridViewTextBoxColumn chRequestTable;
		private DataGridViewTextBoxColumn chRequestQuery;
		private DataGridViewTextBoxColumn chResponseCount;
		private DataGridViewTextBoxColumn chResponseResult;
		private DataGridViewTextBoxColumn chErrorMessage;
		private TableLayoutPanel tableLayoutPanel3;
		private Label label5;
		private DateTimePicker dtpManualTo;
		private DateTimePicker dtpManualFrom;
		private TableLayoutPanel tlpManual;
	}
}