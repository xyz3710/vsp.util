﻿using System;
using System.Drawing.Printing;
using System.Windows.Forms;

using Zerone.Youtube.Environment;
using Zerone.Youtube.App.Common;

using Newtonsoft.Json.Linq;

using static System.Windows.Forms.VisualStyles.VisualStyleElement.StartPanel;

namespace Zerone.Youtube.App
{
	public partial class SettingForm : Form
	{
		/// <summary>
		/// 
		/// </summary>
		public SettingForm()
		{
			InitializeComponent();
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="e"></param>
		protected override void OnLoad(EventArgs e)
		{
			base.OnLoad(e);

			rbInterval.Checked = true;
			rbTime.Checked = false;

			cbActionInTime.CheckedChanged += (sender, ea) =>
			{
				dtpActionFromTime.Enabled = cbActionInTime.Checked;
				dtpActionToTime.Enabled = cbActionInTime.Checked;
			};

			rbInterval.CheckedChanged += (sender, ea) =>
			{
				nudInterval.Enabled = rbInterval.Checked;
				nudPerTime.Enabled = !nudInterval.Enabled;
			};
			rbTime.CheckedChanged += (sender, ea) =>
			{
				nudPerTime.Enabled = rbTime.Checked;
				nudInterval.Enabled = !nudPerTime.Enabled;
			};
			btnSave.Click += (sender, ea) => SaveAction();
			btnClose.Click += (sender, ea) => Close();
		}

		protected override void OnShown(EventArgs e)
		{
			base.OnShown(e);
			LoadAction();
		}

		protected override bool ProcessCmdKey(ref Message msg, Keys keyData)
		{
			const int WM_KEYDOWN = 0x100;
			const int WM_SYSKEYDOWN = 0x104;

			if ((msg.Msg == WM_KEYDOWN) || (msg.Msg == WM_SYSKEYDOWN))
			{
				switch (keyData)
				{
					case Keys.Q | Keys.Control:
						Close();

						return true;
					case Keys.S | Keys.Control:
						SaveAction();

						return true;
				}
			}

			return base.ProcessCmdKey(ref msg, keyData);
		}

		private void SaveAction()
		{
			if (MessageBox.Show(
				"설정을 저장하고 창을 닫겠습니까?",
				Text,
				MessageBoxButtons.YesNo,
				MessageBoxIcon.Question) == DialogResult.No)
			{
				return;
			}

			var actionTags = GetCheckedTypeTags<RadioButton>(tlpAction);
			var actionTypes = GetValueToEnumString<RequestActionTypes>(actionTags);
			var emailActionTags = GetCheckedTypeTags<CheckBox>(tlpEmail);
			var emailActionTypes = GetValueToEnumString<EmailActionTypes>(emailActionTags);
			var restartActionTags = GetCheckedTypeTags<CheckBox>(tlpRestart);
			var restartActionTypes = GetValueToEnumString<DayOfWeek>(restartActionTags);

			var veeva = new Environment.YTInfo
			{
				PlaylistUrl = tbVeevaBaseUrl.Text,
				VideoDetialUrl = tbVeevaQmsBaseUrl.Text,
				VersionUrl = tbVeevaVersionUrl.Text,
				Username = tbVeevaUsername.Text,
				Password = tbVeevaPassword.Text,
				PageSize = (int)nudVeevaPageSize.Value,
			};
			var bioDb = new BioDb
			{
				Host = tbDbHost.Text,
				Port = (int)nudDbPort.Value,
				Username = tbDbUsername.Text,
				Password = tbDbPassword.Text,
			};
			var bioEmail = new BioEmail
			{
				Host = tbEmailHost.Text,
				Port = (int)nudEmailPort.Value,
				Username = tbEmailUsername.Text,
				Password = tbEmailPassword.Text,
				Senders = tbEmailSenders.Text,
				Receivers = tbEmailReceivers.Text,
			};
			var requestAction = new RequestAction
			{
				Type = actionTypes,
				IsActionInTime = cbActionInTime.Checked,
				ActionFromTime = dtpActionFromTime.Text,
				ActionToTime = dtpActionToTime.Text,
				Interval = (int)nudInterval.Value,
				PerTime = (int)nudPerTime.Value,
			};
			var emailAction = new EmailAction
			{
				Types = emailActionTypes,
				MailHeaderPrefix = tbEmailHeaderPrefix.Text,
			};
			var restartAction = new RestartAction
			{
				ActionTime = dtpRestart.Text,
				DayOfWeeks = restartActionTypes,
			};

			Env env;

			if (Env.IsInitialized == false)
			{
				env = Env.Initialize(
					veeva,
					bioDb,
					bioEmail,
					requestAction,
					emailAction,
					restartAction
				);
			}
			else
			{
				env = Env.Instance;
				env.YTInfo = veeva;
				env.BioDb = bioDb;
				env.BioEmail = bioEmail;
				env.RequestAction = requestAction;
				env.EmailAction = emailAction;
				env.RestartAction = restartAction;
			}

			if (Directory.Exists(Constants.DATA_FOLDER) == false)
			{
				Directory.CreateDirectory(Constants.DATA_FOLDER);
			}

			EnvHelper.Save(env, Constants.ENV_PATH);
			DialogResult = DialogResult.OK;
			Close();
		}

		/// <summary>
		/// 지정된 TableLayoutPanel에서 CheckBox 타입 중 선택된 컨트롤의 Tag 값을 읽어 , 로 구분하여 반환
		/// </summary>
		/// <param name="tableLayoutPanel"></param>
		/// <returns></returns>
		private string GetCheckedTypeTags<T>(TableLayoutPanel tableLayoutPanel)
			where T : ButtonBase
		{
			return string.Join(",", tableLayoutPanel
				.Controls.OfType<T>()
				.Where(c =>
				{
					if (c.Tag == null)
					{
						return false;
					}

					switch (c)
					{
						case CheckBox cb:
							return cb.Checked;
						case RadioButton rb:
							return rb.Checked;
						default:
							return false;
					}
				})
				.Select(x => Convert.ToString(x.Tag))
				.OrderBy(x => x));
		}

		/// <summary>
		/// ,로 합쳐진 value를 T 타입의 enum의 값으로 변환 후 enum string을 문자열로 합쳐서 반환
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="values"></param>
		/// <returns></returns>
		private string GetValueToEnumString<T>(string values)
			where T : Enum
		{
			if (values == string.Empty)
			{
				return string.Empty;
			}

			var enumType = typeof(T);

			return string.Join(",",
				values.Split(",")
				.Select(x => Enum.Parse(enumType, x)));
		}

		/// <summary>
		/// Loads the settings.
		/// </summary>
		private void LoadAction()
		{
			if (File.Exists(Constants.ENV_PATH) == false)
			{
				MessageBox.Show(
					$"{Constants.ENV_PATH}\r\n에 저장 파일이 없습니다.\r\n설정 후 저장해 주십시오.",
					Text,
					MessageBoxButtons.OK,
					MessageBoxIcon.Warning);
				return;
			}

			var env = EnvHelper.Load(Constants.ENV_PATH);

			if (env == null)
			{
				MessageBox.Show(
					$"저장 파일에 오류가 있습니다.\r\n설정 후 저장해 주십시오.\r\n{Constants.ENV_PATH}",
					Text,
					MessageBoxButtons.OK,
					MessageBoxIcon.Warning);
				return;
			}

			tbVeevaBaseUrl.Text = env.YTInfo.PlaylistUrl;
			tbVeevaQmsBaseUrl.Text = env.YTInfo.VideoDetialUrl;
			tbVeevaVersionUrl.Text = env.YTInfo.VersionUrl;
			tbVeevaUsername.Text = env.YTInfo.Username;
			tbVeevaPassword.Text = env.YTInfo.Password;
			nudVeevaPageSize.Value = env.YTInfo.PageSize;

			tbDbHost.Text = env.BioDb.Host;
			nudDbPort.Value = env.BioDb.Port;
			tbDbUsername.Text = env.BioDb.Username;
			tbDbPassword.Text = env.BioDb.Password;

			tbEmailHost.Text = env.BioEmail.Host;
			nudEmailPort.Value = env.BioEmail.Port;
			tbEmailUsername.Text = env.BioEmail.Username;
			tbEmailPassword.Text = env.BioEmail.Password;
			tbEmailSenders.Text = env.BioEmail.Senders;
			tbEmailReceivers.Text = env.BioEmail.Receivers;

			cbActionInTime.Checked = env.RequestAction.IsActionInTime;
			dtpActionFromTime.Text = env.RequestAction.ActionFromTime;
			dtpActionToTime.Text = env.RequestAction.ActionToTime;
			SetEnumStringValueToCheckBoxTag<RequestActionTypes, RadioButton>(tlpAction, env.RequestAction.Type);
			rbTime.Checked = !rbInterval.Checked;
			nudInterval.Value = env.RequestAction.Interval;
			nudPerTime.Value = env.RequestAction.PerTime;

			SetEnumStringValueToCheckBoxTag<EmailActionTypes, CheckBox>(tlpEmail, env.EmailAction.Types);
			tbEmailHeaderPrefix.Text = env.EmailAction.MailHeaderPrefix;

			dtpRestart.Text = env.RestartAction.ActionTime;
			SetEnumStringValueToCheckBoxTag<DayOfWeek, CheckBox>(tlpRestart, env.RestartAction.DayOfWeeks);
		}

		/// <summary>
		/// T 타이의 enum의 enumString을 , 로 구분한 뒤 해당 enum의 value 값을 Tag로 갖은 CheckBox를 찾아 Checked = true로 설정
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="enumString"></param>
		private void SetEnumStringValueToCheckBoxTag<T, C>(TableLayoutPanel tableLayoutPanel, string enumString)
			where T : Enum
			where C : ButtonBase
		{
			var clearControls = tableLayoutPanel.Controls.OfType<C>();

			foreach (var c in clearControls)
			{
				switch (c)
				{
					case CheckBox cb:
						cb.Checked = false;
						break;
					case RadioButton rb:
						rb.Checked = false;
						break;
				}
			}

			if (enumString == string.Empty)
			{
				return;
			}

			var enumType = typeof(T);
			var values = enumString.Split(",")
				.Select(x => (int)Enum.Parse(enumType, x));
			var controls = tableLayoutPanel.Controls.OfType<C>()
				.Where(c => c.Tag != null && values.Contains(Convert.ToInt32(c.Tag)));

			foreach (var c in controls)
			{
				switch (c)
				{
					case CheckBox cb:
						cb.Checked = true;
						break;
					case RadioButton rb:
						rb.Checked = true;
						break;
				}
			}
		}
	}
}