﻿using Zerone.Youtube.Dac;
using Zerone.Youtube.Environment;

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Timers;

namespace Zerone.Youtube.App
{
	partial class MainForm
	{
		/// <summary>Processes a command key.</summary>
		/// <param name="msg">A <see cref="System.Windows.Forms.Message" />, passed by reference, that represents the Win32 message to process.</param>
		/// <param name="keyData">One of the <see cref="System.Windows.Forms.Keys" /> values that represents the key to process.</param>
		/// <returns>
		/// <see langword="true" /> if the keystroke was processed and consumed by the control; otherwise, <see langword="false" /> to allow further processing.</returns>
		protected override bool ProcessCmdKey(ref Message msg, Keys keyData)
		{
			const int WM_KEYDOWN = 0x100;
			const int WM_SYSKEYDOWN = 0x104;

			if ((msg.Msg == WM_KEYDOWN) || (msg.Msg == WM_SYSKEYDOWN))
			{
				switch (keyData)
				{
					case Keys.Q | Keys.Control:
						Close();

						return true;
					case Keys.O | Keys.Alt:
						ShowSettingForm();

						return true;
					case Keys.M | Keys.Control:
						btnManual.PerformClick();

						return true;
					case Keys.D1 | Keys.Control:
						tcMain.SelectedIndex = 0;

						return true;
					case Keys.D2 | Keys.Control:
						tcMain.SelectedIndex = 1;

						return true;
					case Keys.D1 | Keys.Shift:
						//TestRestart();

						return true;
				}
			}

			return base.ProcessCmdKey(ref msg, keyData);
		}

		private static void TestRestart()
		{
			Application.Exit();
			Process.Start(Application.ExecutablePath, "RESTART");
			//Application.Restart();
			System.Environment.Exit(0);
		}

		private void OnMainTimerElapsed(object? sender, System.Timers.ElapsedEventArgs e)
		{
			// MainTimer 시간 갱신
			UpdateCurrentTime(e);

			// 재시작 동작 제어
			RestartAction(e.SignalTime);

			if (btnStart.Enabled) // 중지 상태면 SKIP
			{
				return;
			}

			// 작동시간이 설정되어 있는지 확인
			if (Env.Instance.RequestAction.IsActionInTime)
			{
				IsInTimeToAction = e.SignalTime >= ActionFromTime && e.SignalTime <= ActionToTime;
			}
			else
			{
				IsInTimeToAction = true;
			}

			RestSec--;

			if (IsInTimeToAction == false)
			{
				return;
			}

			if (RestSec == 0)
			{
				RequestAction((RequestActionTypes)Enum.Parse<RequestActionTypes>(Env.Instance.RequestAction.Type));
				SetRestTime();
			}

			UpdateRestTime();
		}

		private void UpdateCurrentTime(ElapsedEventArgs e)
		{
			Invoke(new MethodInvoker(() =>
			{
#if DEBUG
				if (tsCurrentTime == null || e == null)
				{
					return;
				}
#endif
				tsCurrentTime.Text = $"[ {e.SignalTime:yyyy-MM-dd HH:mm:ss} ]";
			}));
		}

		private void RestartAction(DateTime signalTime)
		{
			var now = DateTime.Now;

			if (Env.Instance.RestartAction.DayOfWeeks.Contains(now.DayOfWeek.ToString()) == false
				|| RestartTime.Hour != now.Hour
				|| now.Second != 0)
			{
				return;
			}

			if (RestartMin == now.Minute)
			{
				if (Env.Instance.EmailAction.Types.Contains(EmailActionTypes.OnBeforeRestart.ToString()))
				{
					SendBeforeRestartEmail();

					Thread.Sleep(1000);
				}

				Application.Exit();
				Process.Start(Application.ExecutablePath, "RESTART");
				System.Environment.Exit(0);
			}
		}
	}
}
