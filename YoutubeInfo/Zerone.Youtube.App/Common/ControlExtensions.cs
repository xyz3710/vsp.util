﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace System.Windows.Forms
{
	public static class ControlExtensions
	{
		/// <summary>
		/// 
		/// </summary>
		/// <param name="control"></param>
		/// <param name="enabled"></param>
		/// <exception cref="ArgumentNullException"></exception>
		public static void SetDoubleBuffered(this Control control, bool enabled = true)
		{
			if (control == null)
			{
				throw new ArgumentNullException("control");
			}

			Type type = control.GetType();
			object[] objArray = new object[] { true };
			type.InvokeMember("DoubleBuffered", BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.SetProperty, null, control, objArray);
		}
	}
}
