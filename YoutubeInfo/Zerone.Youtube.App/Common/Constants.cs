﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zerone.Youtube.App.Common
{
	/// <summary>
	/// 상수 클래스
	/// </summary>
	internal class Constants
	{
		public static string DATA_FOLDER = Path.Combine(Application.StartupPath, "Data");

		public static readonly string ENV_PATH = Path.Combine(DATA_FOLDER, "__env.json");
	}
}
