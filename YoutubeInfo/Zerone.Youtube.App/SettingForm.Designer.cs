﻿namespace Zerone.Youtube.App
{
	partial class SettingForm
	{
		/// <summary>
		///  Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		///  Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		///  Required method for Designer support - do not modify
		///  the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SettingForm));
			tlpMain = new TableLayoutPanel();
			gbVeeva = new GroupBox();
			tlpVeeva = new TableLayoutPanel();
			label1 = new Label();
			label2 = new Label();
			label3 = new Label();
			label4 = new Label();
			label5 = new Label();
			nudVeevaPageSize = new NumericUpDown();
			tbVeevaBaseUrl = new TextBox();
			tbVeevaVersionUrl = new TextBox();
			tbVeevaUsername = new TextBox();
			tbVeevaPassword = new TextBox();
			tbVeevaQmsBaseUrl = new TextBox();
			label18 = new Label();
			gbDb = new GroupBox();
			tlpDb = new TableLayoutPanel();
			label6 = new Label();
			label7 = new Label();
			label8 = new Label();
			label9 = new Label();
			tbDbHost = new TextBox();
			nudDbPort = new NumericUpDown();
			tbDbUsername = new TextBox();
			tbDbPassword = new TextBox();
			gbEmail = new GroupBox();
			tlpEmailSetting = new TableLayoutPanel();
			label10 = new Label();
			label11 = new Label();
			label12 = new Label();
			label13 = new Label();
			tbEmailHost = new TextBox();
			nudEmailPort = new NumericUpDown();
			label14 = new Label();
			tbEmailPassword = new TextBox();
			label15 = new Label();
			tbEmailUsername = new TextBox();
			tbEmailSenders = new TextBox();
			tbEmailReceivers = new TextBox();
			tlpRight = new TableLayoutPanel();
			gbAction = new GroupBox();
			tlpAction = new TableLayoutPanel();
			rbInterval = new RadioButton();
			nudInterval = new NumericUpDown();
			nudPerTime = new NumericUpDown();
			tlpActionPeriod = new TableLayoutPanel();
			dtpActionFromTime = new DateTimePicker();
			dtpActionToTime = new DateTimePicker();
			cbActionInTime = new CheckBox();
			rbTime = new RadioButton();
			tableLayoutPanel1 = new TableLayoutPanel();
			btnSave = new Button();
			btnClose = new Button();
			gbEmailAction = new GroupBox();
			tlpEmail = new TableLayoutPanel();
			cbEmailOnAfterRestart = new CheckBox();
			cbEmailOnBeforeRestart = new CheckBox();
			cbEmailOnFail = new CheckBox();
			cbEmailOnEmpty = new CheckBox();
			tableLayoutPanel2 = new TableLayoutPanel();
			tbEmailHeaderPrefix = new TextBox();
			label16 = new Label();
			tbRestart = new GroupBox();
			tlpRestart = new TableLayoutPanel();
			checkBox1 = new CheckBox();
			tableLayoutPanel4 = new TableLayoutPanel();
			dtpRestart = new DateTimePicker();
			label17 = new Label();
			checkBox2 = new CheckBox();
			checkBox3 = new CheckBox();
			checkBox4 = new CheckBox();
			checkBox5 = new CheckBox();
			checkBox6 = new CheckBox();
			checkBox7 = new CheckBox();
			tlpMain.SuspendLayout();
			gbVeeva.SuspendLayout();
			tlpVeeva.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)nudVeevaPageSize).BeginInit();
			gbDb.SuspendLayout();
			tlpDb.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)nudDbPort).BeginInit();
			gbEmail.SuspendLayout();
			tlpEmailSetting.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)nudEmailPort).BeginInit();
			tlpRight.SuspendLayout();
			gbAction.SuspendLayout();
			tlpAction.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)nudInterval).BeginInit();
			((System.ComponentModel.ISupportInitialize)nudPerTime).BeginInit();
			tlpActionPeriod.SuspendLayout();
			tableLayoutPanel1.SuspendLayout();
			gbEmailAction.SuspendLayout();
			tlpEmail.SuspendLayout();
			tableLayoutPanel2.SuspendLayout();
			tbRestart.SuspendLayout();
			tlpRestart.SuspendLayout();
			tableLayoutPanel4.SuspendLayout();
			SuspendLayout();
			// 
			// tlpMain
			// 
			tlpMain.ColumnCount = 2;
			tlpMain.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 51.68651F));
			tlpMain.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 48.31349F));
			tlpMain.Controls.Add(gbVeeva, 0, 0);
			tlpMain.Controls.Add(gbDb, 0, 1);
			tlpMain.Controls.Add(gbEmail, 0, 2);
			tlpMain.Controls.Add(tlpRight, 1, 0);
			tlpMain.Dock = DockStyle.Fill;
			tlpMain.Location = new Point(0, 0);
			tlpMain.Margin = new Padding(4);
			tlpMain.Name = "tlpMain";
			tlpMain.RowCount = 3;
			tlpMain.RowStyles.Add(new RowStyle(SizeType.Absolute, 223F));
			tlpMain.RowStyles.Add(new RowStyle(SizeType.Absolute, 162F));
			tlpMain.RowStyles.Add(new RowStyle(SizeType.Absolute, 185F));
			tlpMain.Size = new Size(1116, 628);
			tlpMain.TabIndex = 0;
			// 
			// gbVeeva
			// 
			gbVeeva.Controls.Add(tlpVeeva);
			gbVeeva.Dock = DockStyle.Fill;
			gbVeeva.Location = new Point(4, 4);
			gbVeeva.Margin = new Padding(4);
			gbVeeva.Name = "gbVeeva";
			gbVeeva.Padding = new Padding(4);
			gbVeeva.Size = new Size(568, 215);
			gbVeeva.TabIndex = 0;
			gbVeeva.TabStop = false;
			gbVeeva.Text = "&Veeva";
			// 
			// tlpVeeva
			// 
			tlpVeeva.ColumnCount = 2;
			tlpVeeva.ColumnStyles.Add(new ColumnStyle(SizeType.Absolute, 116F));
			tlpVeeva.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 100F));
			tlpVeeva.Controls.Add(label1, 0, 0);
			tlpVeeva.Controls.Add(label2, 0, 2);
			tlpVeeva.Controls.Add(label3, 0, 3);
			tlpVeeva.Controls.Add(label4, 0, 4);
			tlpVeeva.Controls.Add(label5, 0, 5);
			tlpVeeva.Controls.Add(nudVeevaPageSize, 1, 5);
			tlpVeeva.Controls.Add(tbVeevaBaseUrl, 1, 0);
			tlpVeeva.Controls.Add(tbVeevaVersionUrl, 1, 2);
			tlpVeeva.Controls.Add(tbVeevaUsername, 1, 3);
			tlpVeeva.Controls.Add(tbVeevaPassword, 1, 4);
			tlpVeeva.Controls.Add(tbVeevaQmsBaseUrl, 1, 1);
			tlpVeeva.Controls.Add(label18, 0, 1);
			tlpVeeva.Dock = DockStyle.Fill;
			tlpVeeva.Location = new Point(4, 24);
			tlpVeeva.Margin = new Padding(4);
			tlpVeeva.Name = "tlpVeeva";
			tlpVeeva.RowCount = 6;
			tlpVeeva.RowStyles.Add(new RowStyle(SizeType.Percent, 16.666666F));
			tlpVeeva.RowStyles.Add(new RowStyle(SizeType.Percent, 16.666666F));
			tlpVeeva.RowStyles.Add(new RowStyle(SizeType.Percent, 16.666666F));
			tlpVeeva.RowStyles.Add(new RowStyle(SizeType.Percent, 16.666666F));
			tlpVeeva.RowStyles.Add(new RowStyle(SizeType.Percent, 16.666666F));
			tlpVeeva.RowStyles.Add(new RowStyle(SizeType.Percent, 16.666666F));
			tlpVeeva.Size = new Size(560, 187);
			tlpVeeva.TabIndex = 0;
			// 
			// label1
			// 
			label1.AutoSize = true;
			label1.Dock = DockStyle.Fill;
			label1.Location = new Point(3, 0);
			label1.Name = "label1";
			label1.Size = new Size(110, 31);
			label1.TabIndex = 0;
			label1.Text = "Base Url";
			label1.TextAlign = ContentAlignment.MiddleRight;
			// 
			// label2
			// 
			label2.AutoSize = true;
			label2.Dock = DockStyle.Fill;
			label2.Location = new Point(3, 62);
			label2.Name = "label2";
			label2.Size = new Size(110, 31);
			label2.TabIndex = 0;
			label2.Text = "Version Url";
			label2.TextAlign = ContentAlignment.MiddleRight;
			// 
			// label3
			// 
			label3.AutoSize = true;
			label3.Dock = DockStyle.Fill;
			label3.Location = new Point(3, 93);
			label3.Name = "label3";
			label3.Size = new Size(110, 31);
			label3.TabIndex = 0;
			label3.Text = "User name";
			label3.TextAlign = ContentAlignment.MiddleRight;
			// 
			// label4
			// 
			label4.AutoSize = true;
			label4.Dock = DockStyle.Fill;
			label4.Location = new Point(3, 124);
			label4.Name = "label4";
			label4.Size = new Size(110, 31);
			label4.TabIndex = 0;
			label4.Text = "Password";
			label4.TextAlign = ContentAlignment.MiddleRight;
			// 
			// label5
			// 
			label5.AutoSize = true;
			label5.Dock = DockStyle.Fill;
			label5.Location = new Point(3, 155);
			label5.Name = "label5";
			label5.Size = new Size(110, 32);
			label5.TabIndex = 0;
			label5.Text = "Page Size";
			label5.TextAlign = ContentAlignment.MiddleRight;
			// 
			// nudVeevaPageSize
			// 
			nudVeevaPageSize.Dock = DockStyle.Fill;
			nudVeevaPageSize.Location = new Point(119, 158);
			nudVeevaPageSize.Maximum = new decimal(new int[] { 100000000, 0, 0, 0 });
			nudVeevaPageSize.Minimum = new decimal(new int[] { 10, 0, 0, 0 });
			nudVeevaPageSize.Name = "nudVeevaPageSize";
			nudVeevaPageSize.Size = new Size(438, 27);
			nudVeevaPageSize.TabIndex = 5;
			nudVeevaPageSize.ThousandsSeparator = true;
			nudVeevaPageSize.Value = new decimal(new int[] { 1000, 0, 0, 0 });
			// 
			// tbVeevaBaseUrl
			// 
			tbVeevaBaseUrl.Dock = DockStyle.Fill;
			tbVeevaBaseUrl.Location = new Point(119, 3);
			tbVeevaBaseUrl.Name = "tbVeevaBaseUrl";
			tbVeevaBaseUrl.Size = new Size(438, 27);
			tbVeevaBaseUrl.TabIndex = 0;
			tbVeevaBaseUrl.Text = "https://sb-ckdbio-ckd-bio-sandbox.veevavault.com";
			// 
			// tbVeevaVersionUrl
			// 
			tbVeevaVersionUrl.Dock = DockStyle.Fill;
			tbVeevaVersionUrl.Location = new Point(119, 65);
			tbVeevaVersionUrl.Name = "tbVeevaVersionUrl";
			tbVeevaVersionUrl.Size = new Size(438, 27);
			tbVeevaVersionUrl.TabIndex = 2;
			tbVeevaVersionUrl.Text = "/api/v22.3";
			// 
			// tbVeevaUsername
			// 
			tbVeevaUsername.Dock = DockStyle.Fill;
			tbVeevaUsername.Location = new Point(119, 96);
			tbVeevaUsername.Name = "tbVeevaUsername";
			tbVeevaUsername.Size = new Size(438, 27);
			tbVeevaUsername.TabIndex = 3;
			tbVeevaUsername.Text = "integration.test@sb-ckdbio.com";
			// 
			// tbVeevaPassword
			// 
			tbVeevaPassword.Dock = DockStyle.Fill;
			tbVeevaPassword.Location = new Point(119, 127);
			tbVeevaPassword.Name = "tbVeevaPassword";
			tbVeevaPassword.PasswordChar = '*';
			tbVeevaPassword.Size = new Size(438, 27);
			tbVeevaPassword.TabIndex = 4;
			tbVeevaPassword.Text = "CKDBIO123456!";
			// 
			// tbVeevaQmsBaseUrl
			// 
			tbVeevaQmsBaseUrl.Dock = DockStyle.Fill;
			tbVeevaQmsBaseUrl.Location = new Point(119, 34);
			tbVeevaQmsBaseUrl.Name = "tbVeevaQmsBaseUrl";
			tbVeevaQmsBaseUrl.Size = new Size(438, 27);
			tbVeevaQmsBaseUrl.TabIndex = 1;
			tbVeevaQmsBaseUrl.Text = "https://sb-ckdbio-ckd-bio-qms-sandbox.veevavault.com";
			// 
			// label18
			// 
			label18.AutoSize = true;
			label18.Dock = DockStyle.Fill;
			label18.Location = new Point(3, 31);
			label18.Name = "label18";
			label18.Size = new Size(110, 31);
			label18.TabIndex = 0;
			label18.Text = "Qms Auth. Url";
			label18.TextAlign = ContentAlignment.MiddleRight;
			// 
			// gbDb
			// 
			gbDb.Controls.Add(tlpDb);
			gbDb.Dock = DockStyle.Fill;
			gbDb.Location = new Point(4, 227);
			gbDb.Margin = new Padding(4);
			gbDb.Name = "gbDb";
			gbDb.Padding = new Padding(4);
			gbDb.Size = new Size(568, 154);
			gbDb.TabIndex = 1;
			gbDb.TabStop = false;
			gbDb.Text = "&Database";
			// 
			// tlpDb
			// 
			tlpDb.ColumnCount = 2;
			tlpDb.ColumnStyles.Add(new ColumnStyle(SizeType.Absolute, 116F));
			tlpDb.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 100F));
			tlpDb.Controls.Add(label6, 0, 0);
			tlpDb.Controls.Add(label7, 0, 1);
			tlpDb.Controls.Add(label8, 0, 2);
			tlpDb.Controls.Add(label9, 0, 3);
			tlpDb.Controls.Add(tbDbHost, 1, 0);
			tlpDb.Controls.Add(nudDbPort, 1, 1);
			tlpDb.Controls.Add(tbDbUsername, 1, 2);
			tlpDb.Controls.Add(tbDbPassword, 1, 3);
			tlpDb.Dock = DockStyle.Fill;
			tlpDb.Location = new Point(4, 24);
			tlpDb.Margin = new Padding(4);
			tlpDb.Name = "tlpDb";
			tlpDb.RowCount = 4;
			tlpDb.RowStyles.Add(new RowStyle(SizeType.Percent, 25F));
			tlpDb.RowStyles.Add(new RowStyle(SizeType.Percent, 25F));
			tlpDb.RowStyles.Add(new RowStyle(SizeType.Percent, 25F));
			tlpDb.RowStyles.Add(new RowStyle(SizeType.Percent, 25F));
			tlpDb.Size = new Size(560, 126);
			tlpDb.TabIndex = 0;
			// 
			// label6
			// 
			label6.AutoSize = true;
			label6.Dock = DockStyle.Fill;
			label6.Location = new Point(3, 0);
			label6.Name = "label6";
			label6.Size = new Size(110, 31);
			label6.TabIndex = 0;
			label6.Text = "Host";
			label6.TextAlign = ContentAlignment.MiddleRight;
			// 
			// label7
			// 
			label7.AutoSize = true;
			label7.Dock = DockStyle.Fill;
			label7.Location = new Point(3, 31);
			label7.Name = "label7";
			label7.Size = new Size(110, 31);
			label7.TabIndex = 0;
			label7.Text = "Port";
			label7.TextAlign = ContentAlignment.MiddleRight;
			// 
			// label8
			// 
			label8.AutoSize = true;
			label8.Dock = DockStyle.Fill;
			label8.Location = new Point(3, 62);
			label8.Name = "label8";
			label8.Size = new Size(110, 31);
			label8.TabIndex = 0;
			label8.Text = "User name";
			label8.TextAlign = ContentAlignment.MiddleRight;
			// 
			// label9
			// 
			label9.AutoSize = true;
			label9.Dock = DockStyle.Fill;
			label9.Location = new Point(3, 93);
			label9.Name = "label9";
			label9.Size = new Size(110, 33);
			label9.TabIndex = 0;
			label9.Text = "Password";
			label9.TextAlign = ContentAlignment.MiddleRight;
			// 
			// tbDbHost
			// 
			tbDbHost.Dock = DockStyle.Fill;
			tbDbHost.Location = new Point(119, 3);
			tbDbHost.Name = "tbDbHost";
			tbDbHost.Size = new Size(438, 27);
			tbDbHost.TabIndex = 0;
			tbDbHost.Text = "localhost";
			// 
			// nudDbPort
			// 
			nudDbPort.Dock = DockStyle.Fill;
			nudDbPort.Location = new Point(119, 34);
			nudDbPort.Maximum = new decimal(new int[] { 100000000, 0, 0, 0 });
			nudDbPort.Minimum = new decimal(new int[] { 10, 0, 0, 0 });
			nudDbPort.Name = "nudDbPort";
			nudDbPort.Size = new Size(438, 27);
			nudDbPort.TabIndex = 1;
			nudDbPort.Value = new decimal(new int[] { 1433, 0, 0, 0 });
			// 
			// tbDbUsername
			// 
			tbDbUsername.Dock = DockStyle.Fill;
			tbDbUsername.Location = new Point(119, 65);
			tbDbUsername.Name = "tbDbUsername";
			tbDbUsername.Size = new Size(438, 27);
			tbDbUsername.TabIndex = 2;
			tbDbUsername.Text = "bioif";
			// 
			// tbDbPassword
			// 
			tbDbPassword.Dock = DockStyle.Fill;
			tbDbPassword.Location = new Point(119, 96);
			tbDbPassword.Name = "tbDbPassword";
			tbDbPassword.PasswordChar = '*';
			tbDbPassword.Size = new Size(438, 27);
			tbDbPassword.TabIndex = 3;
			tbDbPassword.Text = "CKDqkdldh07!#";
			// 
			// gbEmail
			// 
			gbEmail.Controls.Add(tlpEmailSetting);
			gbEmail.Dock = DockStyle.Fill;
			gbEmail.Location = new Point(4, 389);
			gbEmail.Margin = new Padding(4);
			gbEmail.Name = "gbEmail";
			gbEmail.Padding = new Padding(4);
			gbEmail.Size = new Size(568, 235);
			gbEmail.TabIndex = 2;
			gbEmail.TabStop = false;
			gbEmail.Text = "&Email 설정";
			// 
			// tlpEmailSetting
			// 
			tlpEmailSetting.ColumnCount = 2;
			tlpEmailSetting.ColumnStyles.Add(new ColumnStyle(SizeType.Absolute, 116F));
			tlpEmailSetting.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 100F));
			tlpEmailSetting.Controls.Add(label10, 0, 0);
			tlpEmailSetting.Controls.Add(label11, 0, 1);
			tlpEmailSetting.Controls.Add(label12, 0, 2);
			tlpEmailSetting.Controls.Add(label13, 0, 3);
			tlpEmailSetting.Controls.Add(tbEmailHost, 1, 0);
			tlpEmailSetting.Controls.Add(nudEmailPort, 1, 1);
			tlpEmailSetting.Controls.Add(label14, 0, 4);
			tlpEmailSetting.Controls.Add(tbEmailPassword, 1, 3);
			tlpEmailSetting.Controls.Add(label15, 0, 5);
			tlpEmailSetting.Controls.Add(tbEmailUsername, 1, 2);
			tlpEmailSetting.Controls.Add(tbEmailSenders, 1, 4);
			tlpEmailSetting.Controls.Add(tbEmailReceivers, 1, 5);
			tlpEmailSetting.Dock = DockStyle.Fill;
			tlpEmailSetting.Location = new Point(4, 24);
			tlpEmailSetting.Margin = new Padding(4);
			tlpEmailSetting.Name = "tlpEmailSetting";
			tlpEmailSetting.RowCount = 6;
			tlpEmailSetting.RowStyles.Add(new RowStyle(SizeType.Percent, 16.6666641F));
			tlpEmailSetting.RowStyles.Add(new RowStyle(SizeType.Percent, 16.6666641F));
			tlpEmailSetting.RowStyles.Add(new RowStyle(SizeType.Percent, 16.6666641F));
			tlpEmailSetting.RowStyles.Add(new RowStyle(SizeType.Percent, 16.6666641F));
			tlpEmailSetting.RowStyles.Add(new RowStyle(SizeType.Percent, 16.6666641F));
			tlpEmailSetting.RowStyles.Add(new RowStyle(SizeType.Percent, 16.6666641F));
			tlpEmailSetting.Size = new Size(560, 207);
			tlpEmailSetting.TabIndex = 0;
			// 
			// label10
			// 
			label10.AutoSize = true;
			label10.Dock = DockStyle.Fill;
			label10.Location = new Point(3, 0);
			label10.Name = "label10";
			label10.Size = new Size(110, 34);
			label10.TabIndex = 0;
			label10.Text = "Host";
			label10.TextAlign = ContentAlignment.MiddleRight;
			// 
			// label11
			// 
			label11.AutoSize = true;
			label11.Dock = DockStyle.Fill;
			label11.Location = new Point(3, 34);
			label11.Name = "label11";
			label11.Size = new Size(110, 34);
			label11.TabIndex = 0;
			label11.Text = "Port";
			label11.TextAlign = ContentAlignment.MiddleRight;
			// 
			// label12
			// 
			label12.AutoSize = true;
			label12.Dock = DockStyle.Fill;
			label12.Location = new Point(3, 68);
			label12.Name = "label12";
			label12.Size = new Size(110, 34);
			label12.TabIndex = 0;
			label12.Text = "User name";
			label12.TextAlign = ContentAlignment.MiddleRight;
			// 
			// label13
			// 
			label13.AutoSize = true;
			label13.Dock = DockStyle.Fill;
			label13.Location = new Point(3, 102);
			label13.Name = "label13";
			label13.Size = new Size(110, 34);
			label13.TabIndex = 0;
			label13.Text = "Password";
			label13.TextAlign = ContentAlignment.MiddleRight;
			// 
			// tbEmailHost
			// 
			tbEmailHost.Dock = DockStyle.Fill;
			tbEmailHost.Location = new Point(119, 3);
			tbEmailHost.Name = "tbEmailHost";
			tbEmailHost.Size = new Size(438, 27);
			tbEmailHost.TabIndex = 0;
			tbEmailHost.Text = "smtp.office365.com";
			// 
			// nudEmailPort
			// 
			nudEmailPort.Dock = DockStyle.Fill;
			nudEmailPort.Location = new Point(119, 37);
			nudEmailPort.Maximum = new decimal(new int[] { 100000000, 0, 0, 0 });
			nudEmailPort.Minimum = new decimal(new int[] { 10, 0, 0, 0 });
			nudEmailPort.Name = "nudEmailPort";
			nudEmailPort.Size = new Size(438, 27);
			nudEmailPort.TabIndex = 1;
			nudEmailPort.Value = new decimal(new int[] { 587, 0, 0, 0 });
			// 
			// label14
			// 
			label14.AutoSize = true;
			label14.Dock = DockStyle.Fill;
			label14.Location = new Point(3, 136);
			label14.Name = "label14";
			label14.Size = new Size(110, 34);
			label14.TabIndex = 0;
			label14.Text = "Senders";
			label14.TextAlign = ContentAlignment.MiddleRight;
			// 
			// tbEmailPassword
			// 
			tbEmailPassword.Dock = DockStyle.Fill;
			tbEmailPassword.Location = new Point(119, 105);
			tbEmailPassword.Name = "tbEmailPassword";
			tbEmailPassword.PasswordChar = '*';
			tbEmailPassword.Size = new Size(438, 27);
			tbEmailPassword.TabIndex = 3;
			tbEmailPassword.Text = "BellCKDBIO123!@#";
			// 
			// label15
			// 
			label15.AutoSize = true;
			label15.Dock = DockStyle.Fill;
			label15.Location = new Point(3, 170);
			label15.Name = "label15";
			label15.Size = new Size(110, 37);
			label15.TabIndex = 0;
			label15.Text = "Receivers";
			label15.TextAlign = ContentAlignment.MiddleRight;
			// 
			// tbEmailUsername
			// 
			tbEmailUsername.Dock = DockStyle.Fill;
			tbEmailUsername.Location = new Point(119, 71);
			tbEmailUsername.Name = "tbEmailUsername";
			tbEmailUsername.Size = new Size(438, 27);
			tbEmailUsername.TabIndex = 2;
			tbEmailUsername.Text = "it@ckdbio.com";
			// 
			// tbEmailSenders
			// 
			tbEmailSenders.Dock = DockStyle.Fill;
			tbEmailSenders.Location = new Point(119, 139);
			tbEmailSenders.Name = "tbEmailSenders";
			tbEmailSenders.Size = new Size(438, 27);
			tbEmailSenders.TabIndex = 4;
			tbEmailSenders.Text = "it@ckdbio.com";
			// 
			// tbEmailReceivers
			// 
			tbEmailReceivers.Dock = DockStyle.Fill;
			tbEmailReceivers.Location = new Point(119, 173);
			tbEmailReceivers.Name = "tbEmailReceivers";
			tbEmailReceivers.Size = new Size(438, 27);
			tbEmailReceivers.TabIndex = 5;
			tbEmailReceivers.Text = "hjjang@bellins.net;ysyang@ckdpharm.com";
			// 
			// tlpRight
			// 
			tlpRight.ColumnCount = 1;
			tlpRight.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 100F));
			tlpRight.Controls.Add(gbAction, 0, 0);
			tlpRight.Controls.Add(tableLayoutPanel1, 0, 3);
			tlpRight.Controls.Add(gbEmailAction, 0, 1);
			tlpRight.Controls.Add(tbRestart, 0, 2);
			tlpRight.Dock = DockStyle.Fill;
			tlpRight.Location = new Point(576, 0);
			tlpRight.Margin = new Padding(0);
			tlpRight.Name = "tlpRight";
			tlpRight.RowCount = 4;
			tlpMain.SetRowSpan(tlpRight, 3);
			tlpRight.RowStyles.Add(new RowStyle(SizeType.Absolute, 190F));
			tlpRight.RowStyles.Add(new RowStyle(SizeType.Absolute, 162F));
			tlpRight.RowStyles.Add(new RowStyle(SizeType.Absolute, 164F));
			tlpRight.RowStyles.Add(new RowStyle());
			tlpRight.Size = new Size(540, 628);
			tlpRight.TabIndex = 6;
			// 
			// gbAction
			// 
			gbAction.Controls.Add(tlpAction);
			gbAction.Dock = DockStyle.Fill;
			gbAction.Location = new Point(4, 4);
			gbAction.Margin = new Padding(4);
			gbAction.Name = "gbAction";
			gbAction.Padding = new Padding(4);
			gbAction.Size = new Size(532, 182);
			gbAction.TabIndex = 3;
			gbAction.TabStop = false;
			gbAction.Text = "작동 조건(&A)";
			// 
			// tlpAction
			// 
			tlpAction.ColumnCount = 2;
			tlpAction.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 50F));
			tlpAction.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 50F));
			tlpAction.Controls.Add(rbInterval, 0, 1);
			tlpAction.Controls.Add(nudInterval, 0, 2);
			tlpAction.Controls.Add(nudPerTime, 1, 2);
			tlpAction.Controls.Add(tlpActionPeriod, 0, 0);
			tlpAction.Controls.Add(rbTime, 1, 1);
			tlpAction.Dock = DockStyle.Fill;
			tlpAction.Location = new Point(4, 24);
			tlpAction.Name = "tlpAction";
			tlpAction.RowCount = 3;
			tlpAction.RowStyles.Add(new RowStyle(SizeType.Percent, 30F));
			tlpAction.RowStyles.Add(new RowStyle(SizeType.Percent, 70F));
			tlpAction.RowStyles.Add(new RowStyle(SizeType.Absolute, 33F));
			tlpAction.Size = new Size(524, 154);
			tlpAction.TabIndex = 0;
			// 
			// rbInterval
			// 
			rbInterval.Appearance = Appearance.Button;
			rbInterval.AutoSize = true;
			rbInterval.Checked = true;
			rbInterval.Dock = DockStyle.Fill;
			rbInterval.Location = new Point(3, 39);
			rbInterval.Name = "rbInterval";
			rbInterval.Size = new Size(256, 78);
			rbInterval.TabIndex = 1;
			rbInterval.TabStop = true;
			rbInterval.Tag = "0";
			rbInterval.Text = "지정 간격\r\n(1 ~ 180분)";
			rbInterval.TextAlign = ContentAlignment.MiddleCenter;
			rbInterval.UseVisualStyleBackColor = true;
			// 
			// nudInterval
			// 
			nudInterval.Dock = DockStyle.Fill;
			nudInterval.Location = new Point(3, 123);
			nudInterval.Maximum = new decimal(new int[] { 180, 0, 0, 0 });
			nudInterval.Minimum = new decimal(new int[] { 1, 0, 0, 0 });
			nudInterval.Name = "nudInterval";
			nudInterval.Size = new Size(256, 27);
			nudInterval.TabIndex = 2;
			nudInterval.Value = new decimal(new int[] { 30, 0, 0, 0 });
			// 
			// nudPerTime
			// 
			nudPerTime.Dock = DockStyle.Fill;
			nudPerTime.Enabled = false;
			nudPerTime.Location = new Point(265, 123);
			nudPerTime.Maximum = new decimal(new int[] { 59, 0, 0, 0 });
			nudPerTime.Name = "nudPerTime";
			nudPerTime.Size = new Size(256, 27);
			nudPerTime.TabIndex = 4;
			nudPerTime.Value = new decimal(new int[] { 30, 0, 0, 0 });
			// 
			// tlpActionPeriod
			// 
			tlpActionPeriod.CellBorderStyle = TableLayoutPanelCellBorderStyle.Single;
			tlpActionPeriod.ColumnCount = 3;
			tlpAction.SetColumnSpan(tlpActionPeriod, 2);
			tlpActionPeriod.ColumnStyles.Add(new ColumnStyle(SizeType.Absolute, 258F));
			tlpActionPeriod.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 50F));
			tlpActionPeriod.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 50F));
			tlpActionPeriod.Controls.Add(dtpActionFromTime, 1, 0);
			tlpActionPeriod.Controls.Add(dtpActionToTime, 2, 0);
			tlpActionPeriod.Controls.Add(cbActionInTime, 0, 0);
			tlpActionPeriod.Dock = DockStyle.Fill;
			tlpActionPeriod.Location = new Point(1, 1);
			tlpActionPeriod.Margin = new Padding(1);
			tlpActionPeriod.Name = "tlpActionPeriod";
			tlpActionPeriod.RowCount = 1;
			tlpActionPeriod.RowStyles.Add(new RowStyle(SizeType.Percent, 100F));
			tlpActionPeriod.Size = new Size(522, 34);
			tlpActionPeriod.TabIndex = 0;
			// 
			// dtpActionFromTime
			// 
			dtpActionFromTime.CustomFormat = "HH:mm";
			dtpActionFromTime.Dock = DockStyle.Fill;
			dtpActionFromTime.Format = DateTimePickerFormat.Custom;
			dtpActionFromTime.Location = new Point(263, 4);
			dtpActionFromTime.Name = "dtpActionFromTime";
			dtpActionFromTime.ShowUpDown = true;
			dtpActionFromTime.Size = new Size(124, 27);
			dtpActionFromTime.TabIndex = 2;
			dtpActionFromTime.Value = new DateTime(2023, 7, 20, 7, 0, 0, 0);
			// 
			// dtpActionToTime
			// 
			dtpActionToTime.CustomFormat = "HH:mm";
			dtpActionToTime.Dock = DockStyle.Fill;
			dtpActionToTime.Format = DateTimePickerFormat.Custom;
			dtpActionToTime.Location = new Point(394, 4);
			dtpActionToTime.Name = "dtpActionToTime";
			dtpActionToTime.ShowUpDown = true;
			dtpActionToTime.Size = new Size(124, 27);
			dtpActionToTime.TabIndex = 2;
			dtpActionToTime.Value = new DateTime(2023, 7, 20, 21, 0, 0, 0);
			// 
			// cbActionInTime
			// 
			cbActionInTime.Appearance = Appearance.Button;
			cbActionInTime.AutoSize = true;
			cbActionInTime.Checked = true;
			cbActionInTime.CheckState = CheckState.Checked;
			cbActionInTime.Dock = DockStyle.Fill;
			cbActionInTime.Location = new Point(2, 2);
			cbActionInTime.Margin = new Padding(1);
			cbActionInTime.Name = "cbActionInTime";
			cbActionInTime.Size = new Size(256, 30);
			cbActionInTime.TabIndex = 1;
			cbActionInTime.Text = "작동 시간";
			cbActionInTime.TextAlign = ContentAlignment.MiddleCenter;
			cbActionInTime.UseVisualStyleBackColor = true;
			// 
			// rbTime
			// 
			rbTime.Appearance = Appearance.Button;
			rbTime.AutoSize = true;
			rbTime.Dock = DockStyle.Fill;
			rbTime.Location = new Point(265, 39);
			rbTime.Name = "rbTime";
			rbTime.Size = new Size(256, 78);
			rbTime.TabIndex = 3;
			rbTime.Tag = "1";
			rbTime.Text = "매시 지정 시간\r\n(0 ~ 59분)";
			rbTime.TextAlign = ContentAlignment.MiddleCenter;
			rbTime.UseVisualStyleBackColor = true;
			// 
			// tableLayoutPanel1
			// 
			tableLayoutPanel1.ColumnCount = 2;
			tableLayoutPanel1.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 50F));
			tableLayoutPanel1.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 50F));
			tableLayoutPanel1.Controls.Add(btnSave, 0, 0);
			tableLayoutPanel1.Controls.Add(btnClose, 1, 0);
			tableLayoutPanel1.Dock = DockStyle.Fill;
			tableLayoutPanel1.Location = new Point(3, 519);
			tableLayoutPanel1.Name = "tableLayoutPanel1";
			tableLayoutPanel1.RowCount = 1;
			tableLayoutPanel1.RowStyles.Add(new RowStyle(SizeType.Percent, 50F));
			tableLayoutPanel1.Size = new Size(534, 106);
			tableLayoutPanel1.TabIndex = 6;
			// 
			// btnSave
			// 
			btnSave.Dock = DockStyle.Fill;
			btnSave.Location = new Point(3, 3);
			btnSave.Name = "btnSave";
			btnSave.Size = new Size(261, 100);
			btnSave.TabIndex = 0;
			btnSave.Text = "&Save\r\n(Ctrl+S)";
			btnSave.UseVisualStyleBackColor = true;
			// 
			// btnClose
			// 
			btnClose.Dock = DockStyle.Fill;
			btnClose.Location = new Point(270, 3);
			btnClose.Name = "btnClose";
			btnClose.Size = new Size(261, 100);
			btnClose.TabIndex = 1;
			btnClose.Text = "&Quit\r\n(Ctrl+Q)";
			btnClose.UseVisualStyleBackColor = true;
			// 
			// gbEmailAction
			// 
			gbEmailAction.Controls.Add(tlpEmail);
			gbEmailAction.Dock = DockStyle.Fill;
			gbEmailAction.Location = new Point(4, 194);
			gbEmailAction.Margin = new Padding(4);
			gbEmailAction.Name = "gbEmailAction";
			gbEmailAction.Padding = new Padding(4);
			gbEmailAction.Size = new Size(532, 154);
			gbEmailAction.TabIndex = 4;
			gbEmailAction.TabStop = false;
			gbEmailAction.Text = "Email 조건(&C)";
			// 
			// tlpEmail
			// 
			tlpEmail.ColumnCount = 2;
			tlpEmail.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 50F));
			tlpEmail.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 50F));
			tlpEmail.Controls.Add(cbEmailOnAfterRestart, 1, 1);
			tlpEmail.Controls.Add(cbEmailOnBeforeRestart, 0, 1);
			tlpEmail.Controls.Add(cbEmailOnFail, 0, 0);
			tlpEmail.Controls.Add(cbEmailOnEmpty, 1, 0);
			tlpEmail.Controls.Add(tableLayoutPanel2, 0, 2);
			tlpEmail.Dock = DockStyle.Fill;
			tlpEmail.Location = new Point(4, 24);
			tlpEmail.Margin = new Padding(4);
			tlpEmail.Name = "tlpEmail";
			tlpEmail.RowCount = 3;
			tlpEmail.RowStyles.Add(new RowStyle(SizeType.Percent, 50F));
			tlpEmail.RowStyles.Add(new RowStyle(SizeType.Percent, 50F));
			tlpEmail.RowStyles.Add(new RowStyle(SizeType.Absolute, 34F));
			tlpEmail.RowStyles.Add(new RowStyle(SizeType.Absolute, 20F));
			tlpEmail.Size = new Size(524, 126);
			tlpEmail.TabIndex = 0;
			// 
			// cbEmailOnAfterRestart
			// 
			cbEmailOnAfterRestart.AutoSize = true;
			cbEmailOnAfterRestart.Checked = true;
			cbEmailOnAfterRestart.CheckState = CheckState.Checked;
			cbEmailOnAfterRestart.Dock = DockStyle.Fill;
			cbEmailOnAfterRestart.Location = new Point(265, 49);
			cbEmailOnAfterRestart.Name = "cbEmailOnAfterRestart";
			cbEmailOnAfterRestart.Padding = new Padding(20, 0, 0, 0);
			cbEmailOnAfterRestart.Size = new Size(256, 40);
			cbEmailOnAfterRestart.TabIndex = 3;
			cbEmailOnAfterRestart.Tag = "4";
			cbEmailOnAfterRestart.Text = "재시작 후";
			cbEmailOnAfterRestart.UseVisualStyleBackColor = true;
			// 
			// cbEmailOnBeforeRestart
			// 
			cbEmailOnBeforeRestart.AutoSize = true;
			cbEmailOnBeforeRestart.Checked = true;
			cbEmailOnBeforeRestart.CheckState = CheckState.Checked;
			cbEmailOnBeforeRestart.Dock = DockStyle.Fill;
			cbEmailOnBeforeRestart.Location = new Point(3, 49);
			cbEmailOnBeforeRestart.Name = "cbEmailOnBeforeRestart";
			cbEmailOnBeforeRestart.Padding = new Padding(20, 0, 0, 0);
			cbEmailOnBeforeRestart.Size = new Size(256, 40);
			cbEmailOnBeforeRestart.TabIndex = 2;
			cbEmailOnBeforeRestart.Tag = "3";
			cbEmailOnBeforeRestart.Text = "재시작 전";
			cbEmailOnBeforeRestart.UseVisualStyleBackColor = true;
			// 
			// cbEmailOnFail
			// 
			cbEmailOnFail.AutoSize = true;
			cbEmailOnFail.Checked = true;
			cbEmailOnFail.CheckState = CheckState.Checked;
			cbEmailOnFail.Dock = DockStyle.Fill;
			cbEmailOnFail.Location = new Point(3, 3);
			cbEmailOnFail.Name = "cbEmailOnFail";
			cbEmailOnFail.Padding = new Padding(20, 0, 0, 0);
			cbEmailOnFail.Size = new Size(256, 40);
			cbEmailOnFail.TabIndex = 0;
			cbEmailOnFail.Tag = "1";
			cbEmailOnFail.Text = "실패 시";
			cbEmailOnFail.UseVisualStyleBackColor = true;
			// 
			// cbEmailOnEmpty
			// 
			cbEmailOnEmpty.AutoSize = true;
			cbEmailOnEmpty.Dock = DockStyle.Fill;
			cbEmailOnEmpty.Location = new Point(265, 3);
			cbEmailOnEmpty.Name = "cbEmailOnEmpty";
			cbEmailOnEmpty.Padding = new Padding(20, 0, 0, 0);
			cbEmailOnEmpty.Size = new Size(256, 40);
			cbEmailOnEmpty.TabIndex = 1;
			cbEmailOnEmpty.Tag = "2";
			cbEmailOnEmpty.Text = "응답이 데이터가 없을 시";
			cbEmailOnEmpty.UseVisualStyleBackColor = true;
			// 
			// tableLayoutPanel2
			// 
			tableLayoutPanel2.ColumnCount = 2;
			tlpEmail.SetColumnSpan(tableLayoutPanel2, 2);
			tableLayoutPanel2.ColumnStyles.Add(new ColumnStyle(SizeType.Absolute, 130F));
			tableLayoutPanel2.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 100F));
			tableLayoutPanel2.Controls.Add(tbEmailHeaderPrefix, 1, 0);
			tableLayoutPanel2.Controls.Add(label16, 0, 0);
			tableLayoutPanel2.Dock = DockStyle.Fill;
			tableLayoutPanel2.Location = new Point(1, 93);
			tableLayoutPanel2.Margin = new Padding(1);
			tableLayoutPanel2.Name = "tableLayoutPanel2";
			tableLayoutPanel2.RowCount = 1;
			tableLayoutPanel2.RowStyles.Add(new RowStyle(SizeType.Percent, 100F));
			tableLayoutPanel2.Size = new Size(522, 32);
			tableLayoutPanel2.TabIndex = 2;
			// 
			// tbEmailHeaderPrefix
			// 
			tbEmailHeaderPrefix.Dock = DockStyle.Fill;
			tbEmailHeaderPrefix.Location = new Point(133, 3);
			tbEmailHeaderPrefix.Name = "tbEmailHeaderPrefix";
			tbEmailHeaderPrefix.Size = new Size(386, 27);
			tbEmailHeaderPrefix.TabIndex = 3;
			// 
			// label16
			// 
			label16.AutoSize = true;
			label16.Dock = DockStyle.Fill;
			label16.Location = new Point(3, 0);
			label16.Name = "label16";
			label16.Size = new Size(124, 32);
			label16.TabIndex = 2;
			label16.Text = "메일 제목 머리말";
			label16.TextAlign = ContentAlignment.MiddleRight;
			// 
			// tbRestart
			// 
			tbRestart.Controls.Add(tlpRestart);
			tbRestart.Dock = DockStyle.Fill;
			tbRestart.Location = new Point(4, 356);
			tbRestart.Margin = new Padding(4);
			tbRestart.Name = "tbRestart";
			tbRestart.Padding = new Padding(4);
			tbRestart.Size = new Size(532, 156);
			tbRestart.TabIndex = 5;
			tbRestart.TabStop = false;
			tbRestart.Text = "재시작 조건(&R)";
			// 
			// tlpRestart
			// 
			tlpRestart.ColumnCount = 2;
			tlpRestart.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 50F));
			tlpRestart.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 50F));
			tlpRestart.Controls.Add(checkBox1, 1, 0);
			tlpRestart.Controls.Add(tableLayoutPanel4, 0, 0);
			tlpRestart.Controls.Add(checkBox2, 0, 1);
			tlpRestart.Controls.Add(checkBox3, 1, 1);
			tlpRestart.Controls.Add(checkBox4, 0, 2);
			tlpRestart.Controls.Add(checkBox5, 1, 2);
			tlpRestart.Controls.Add(checkBox6, 0, 3);
			tlpRestart.Controls.Add(checkBox7, 1, 3);
			tlpRestart.Dock = DockStyle.Fill;
			tlpRestart.Location = new Point(4, 24);
			tlpRestart.Margin = new Padding(4);
			tlpRestart.Name = "tlpRestart";
			tlpRestart.RowCount = 4;
			tlpRestart.RowStyles.Add(new RowStyle(SizeType.Percent, 25F));
			tlpRestart.RowStyles.Add(new RowStyle(SizeType.Percent, 25F));
			tlpRestart.RowStyles.Add(new RowStyle(SizeType.Percent, 25F));
			tlpRestart.RowStyles.Add(new RowStyle(SizeType.Percent, 25F));
			tlpRestart.Size = new Size(524, 128);
			tlpRestart.TabIndex = 0;
			// 
			// checkBox1
			// 
			checkBox1.AutoSize = true;
			checkBox1.Checked = true;
			checkBox1.CheckState = CheckState.Checked;
			checkBox1.Dock = DockStyle.Fill;
			checkBox1.Location = new Point(265, 3);
			checkBox1.Name = "checkBox1";
			checkBox1.Padding = new Padding(20, 0, 0, 0);
			checkBox1.Size = new Size(256, 26);
			checkBox1.TabIndex = 1;
			checkBox1.Tag = "0";
			checkBox1.Text = "일요일";
			checkBox1.UseVisualStyleBackColor = true;
			// 
			// tableLayoutPanel4
			// 
			tableLayoutPanel4.ColumnCount = 2;
			tableLayoutPanel4.ColumnStyles.Add(new ColumnStyle(SizeType.Absolute, 120F));
			tableLayoutPanel4.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 100F));
			tableLayoutPanel4.ColumnStyles.Add(new ColumnStyle(SizeType.Absolute, 20F));
			tableLayoutPanel4.Controls.Add(dtpRestart, 1, 0);
			tableLayoutPanel4.Controls.Add(label17, 0, 0);
			tableLayoutPanel4.Dock = DockStyle.Fill;
			tableLayoutPanel4.Location = new Point(1, 1);
			tableLayoutPanel4.Margin = new Padding(1);
			tableLayoutPanel4.Name = "tableLayoutPanel4";
			tableLayoutPanel4.RowCount = 1;
			tableLayoutPanel4.RowStyles.Add(new RowStyle(SizeType.Percent, 100F));
			tableLayoutPanel4.Size = new Size(260, 30);
			tableLayoutPanel4.TabIndex = 0;
			// 
			// dtpRestart
			// 
			dtpRestart.CustomFormat = "HH:mm";
			dtpRestart.Dock = DockStyle.Fill;
			dtpRestart.Format = DateTimePickerFormat.Custom;
			dtpRestart.Location = new Point(123, 3);
			dtpRestart.Name = "dtpRestart";
			dtpRestart.ShowUpDown = true;
			dtpRestart.Size = new Size(134, 27);
			dtpRestart.TabIndex = 2;
			dtpRestart.Value = new DateTime(2023, 7, 20, 6, 30, 0, 0);
			// 
			// label17
			// 
			label17.AutoSize = true;
			label17.Dock = DockStyle.Fill;
			label17.Location = new Point(3, 0);
			label17.Name = "label17";
			label17.Size = new Size(114, 30);
			label17.TabIndex = 0;
			label17.Text = "매주";
			label17.TextAlign = ContentAlignment.MiddleRight;
			// 
			// checkBox2
			// 
			checkBox2.AutoSize = true;
			checkBox2.Dock = DockStyle.Fill;
			checkBox2.Location = new Point(3, 35);
			checkBox2.Name = "checkBox2";
			checkBox2.Padding = new Padding(20, 0, 0, 0);
			checkBox2.Size = new Size(256, 26);
			checkBox2.TabIndex = 2;
			checkBox2.Tag = "1";
			checkBox2.Text = "월요일";
			checkBox2.UseVisualStyleBackColor = true;
			// 
			// checkBox3
			// 
			checkBox3.AutoSize = true;
			checkBox3.Dock = DockStyle.Fill;
			checkBox3.Location = new Point(265, 35);
			checkBox3.Name = "checkBox3";
			checkBox3.Padding = new Padding(20, 0, 0, 0);
			checkBox3.Size = new Size(256, 26);
			checkBox3.TabIndex = 3;
			checkBox3.Tag = "2";
			checkBox3.Text = "화요일";
			checkBox3.UseVisualStyleBackColor = true;
			// 
			// checkBox4
			// 
			checkBox4.AutoSize = true;
			checkBox4.Dock = DockStyle.Fill;
			checkBox4.Location = new Point(3, 67);
			checkBox4.Name = "checkBox4";
			checkBox4.Padding = new Padding(20, 0, 0, 0);
			checkBox4.Size = new Size(256, 26);
			checkBox4.TabIndex = 4;
			checkBox4.Tag = "3";
			checkBox4.Text = "수요일";
			checkBox4.UseVisualStyleBackColor = true;
			// 
			// checkBox5
			// 
			checkBox5.AutoSize = true;
			checkBox5.Dock = DockStyle.Fill;
			checkBox5.Location = new Point(265, 67);
			checkBox5.Name = "checkBox5";
			checkBox5.Padding = new Padding(20, 0, 0, 0);
			checkBox5.Size = new Size(256, 26);
			checkBox5.TabIndex = 5;
			checkBox5.Tag = "4";
			checkBox5.Text = "목요일";
			checkBox5.UseVisualStyleBackColor = true;
			// 
			// checkBox6
			// 
			checkBox6.AutoSize = true;
			checkBox6.Dock = DockStyle.Fill;
			checkBox6.Location = new Point(3, 99);
			checkBox6.Name = "checkBox6";
			checkBox6.Padding = new Padding(20, 0, 0, 0);
			checkBox6.Size = new Size(256, 26);
			checkBox6.TabIndex = 6;
			checkBox6.Tag = "5";
			checkBox6.Text = "금요일";
			checkBox6.UseVisualStyleBackColor = true;
			// 
			// checkBox7
			// 
			checkBox7.AutoSize = true;
			checkBox7.Dock = DockStyle.Fill;
			checkBox7.Location = new Point(265, 99);
			checkBox7.Name = "checkBox7";
			checkBox7.Padding = new Padding(20, 0, 0, 0);
			checkBox7.Size = new Size(256, 26);
			checkBox7.TabIndex = 7;
			checkBox7.Tag = "6";
			checkBox7.Text = "토요일";
			checkBox7.UseVisualStyleBackColor = true;
			// 
			// SettingForm
			// 
			AutoScaleDimensions = new SizeF(9F, 20F);
			AutoScaleMode = AutoScaleMode.Font;
			ClientSize = new Size(1116, 628);
			Controls.Add(tlpMain);
			Font = new Font("맑은 고딕", 11F, FontStyle.Regular, GraphicsUnit.Point);
			Icon = (Icon)resources.GetObject("$this.Icon");
			Margin = new Padding(4);
			Name = "SettingForm";
			StartPosition = FormStartPosition.CenterParent;
			Text = "환경 설정";
			tlpMain.ResumeLayout(false);
			gbVeeva.ResumeLayout(false);
			tlpVeeva.ResumeLayout(false);
			tlpVeeva.PerformLayout();
			((System.ComponentModel.ISupportInitialize)nudVeevaPageSize).EndInit();
			gbDb.ResumeLayout(false);
			tlpDb.ResumeLayout(false);
			tlpDb.PerformLayout();
			((System.ComponentModel.ISupportInitialize)nudDbPort).EndInit();
			gbEmail.ResumeLayout(false);
			tlpEmailSetting.ResumeLayout(false);
			tlpEmailSetting.PerformLayout();
			((System.ComponentModel.ISupportInitialize)nudEmailPort).EndInit();
			tlpRight.ResumeLayout(false);
			gbAction.ResumeLayout(false);
			tlpAction.ResumeLayout(false);
			tlpAction.PerformLayout();
			((System.ComponentModel.ISupportInitialize)nudInterval).EndInit();
			((System.ComponentModel.ISupportInitialize)nudPerTime).EndInit();
			tlpActionPeriod.ResumeLayout(false);
			tlpActionPeriod.PerformLayout();
			tableLayoutPanel1.ResumeLayout(false);
			gbEmailAction.ResumeLayout(false);
			tlpEmail.ResumeLayout(false);
			tlpEmail.PerformLayout();
			tableLayoutPanel2.ResumeLayout(false);
			tableLayoutPanel2.PerformLayout();
			tbRestart.ResumeLayout(false);
			tlpRestart.ResumeLayout(false);
			tlpRestart.PerformLayout();
			tableLayoutPanel4.ResumeLayout(false);
			tableLayoutPanel4.PerformLayout();
			ResumeLayout(false);
		}

		#endregion

		private TableLayoutPanel tlpMain;
		private GroupBox gbAction;
		private TableLayoutPanel tlpAction;
		private GroupBox gbEmailAction;
		private TableLayoutPanel tlpEmail;
		private GroupBox gbVeeva;
		private TableLayoutPanel tlpVeeva;
		private GroupBox gbDb;
		private TableLayoutPanel tlpDb;
		private GroupBox gbEmail;
		private TableLayoutPanel tlpEmailSetting;
		private TableLayoutPanel tableLayoutPanel1;
		private Button btnSave;
		private Button btnClose;
		private Label label1;
		private RadioButton rbInterval;
		private RadioButton rbTime;
		private Label label2;
		private Label label3;
		private Label label4;
		private Label label5;
		private NumericUpDown nudVeevaPageSize;
		private Label label6;
		private Label label7;
		private Label label8;
		private Label label9;
		private Label label10;
		private Label label11;
		private Label label12;
		private Label label13;
		private Label label14;
		private Label label15;
		private CheckBox cbEmailOnFail;
		private CheckBox cbEmailOnEmpty;
		private NumericUpDown nudInterval;
		private NumericUpDown nudPerTime;
		private TextBox tbVeevaBaseUrl;
		private TextBox tbVeevaVersionUrl;
		private TextBox tbVeevaUsername;
		private TextBox tbVeevaPassword;
		private TextBox tbDbHost;
		private NumericUpDown nudDbPort;
		private TextBox tbDbUsername;
		private TextBox tbDbPassword;
		private TextBox tbEmailHost;
		private NumericUpDown nudEmailPort;
		private TextBox tbEmailPassword;
		private TextBox tbEmailUsername;
		private TextBox tbEmailSenders;
		private TextBox tbEmailReceivers;
		private TableLayoutPanel tableLayoutPanel2;
		private TextBox tbEmailHeaderPrefix;
		private Label label16;
		private DateTimePicker dtpActionFromTime;
		private TableLayoutPanel tlpActionPeriod;
		private DateTimePicker dtpActionToTime;
		private CheckBox cbActionInTime;
		private CheckBox cbEmailOnBeforeRestart;
		private TableLayoutPanel tlpRight;
		private GroupBox tbRestart;
		private TableLayoutPanel tlpRestart;
		private TableLayoutPanel tableLayoutPanel4;
		private DateTimePicker dtpRestart;
		private Label label17;
		private CheckBox checkBox1;
		private CheckBox checkBox2;
		private CheckBox checkBox3;
		private CheckBox checkBox4;
		private CheckBox checkBox5;
		private CheckBox checkBox6;
		private CheckBox checkBox7;
		private CheckBox cbEmailOnAfterRestart;
		private TextBox tbVeevaQmsBaseUrl;
		private Label label18;
	}
}