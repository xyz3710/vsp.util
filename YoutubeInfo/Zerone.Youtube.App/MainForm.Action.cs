﻿
using System;
using System.Collections.Generic;
using System.Linq;

using Zerone.Youtube.Common;

using Zerone.Youtube.Dac;
using Zerone.Youtube.Environment;
using Zerone.Youtube.VeevaRestHelper;

namespace Zerone.Youtube.App
{
	partial class MainForm
	{
		private const string MANUAL_DATE_FORMAT = "yyyy-MM-dd HH:mm";

		private void StartAction()
		{
			btnStart.Enabled = false;
			btnStop.Enabled = true;
			SetRestTime();
			UpdateRestTime();
			WriteLog("배치 작업이 [시작] 되었습니다.", RequestActionTypes.InternalLogic);
			tlpManual.Enabled = false;
			QueryLog();
		}

		private void StopAction()
		{
			btnStart.Enabled = true;
			btnStop.Enabled = false;
			SetRestTime();
			UpdateRestTime();
			WriteLog("배치 작업이 [중지] 되었습니다.", RequestActionTypes.InternalLogic);
			tlpManual.Enabled = true;
			dtpManualFrom.Value = DateTime.ParseExact(DateTime.Now.AddDays(-1).ToString(MANUAL_DATE_FORMAT), MANUAL_DATE_FORMAT, null);
			dtpManualTo.Value = DateTime.Now;
			QueryLog();
		}

		private void ManualAction()
		{
			WriteLog($"{dtpManualFrom.Value:yyyy-MM-dd HH:mm:00} ~ {dtpManualTo.Value:yyyy-MM-dd HH:mm:59}로 [메뉴얼] 요청 되었습니다.", RequestActionTypes.ByManual);
			RequestAction(RequestActionTypes.ByManual);
		}

		private void SendEmail(string subject, string body, bool useBodyHtml = false, Action<string> logAction = null)
		{
			var emailHelper = new EmailHelper(
				Env.Instance.BioEmail.Host,
				Env.Instance.BioEmail.Port,
				Env.Instance.BioEmail.Username,
				Env.Instance.BioEmail.Password,
				Env.Instance.BioEmail.Senders,
				Env.Instance.BioEmail.Receivers
				);

			emailHelper.Send($"{Env.Instance.EmailAction.MailHeaderPrefix} {subject}", body, useBodyHtml, logAction);
		}

		private void SendBeforeRestartEmail()
		{
			var logger = new Logger
			{
				RequestActionType = RequestActionTypes.InternalLogic.ToString(),
				ResponseResult = Logger.RESULT_OK,
				ErrorMessage = "[재시작] 요청 보고",
			};
			WriteLog(logger, "[재시작] 전 이메일 발송");
		}

		private void SendAfterRestartEmail()
		{
			var logger = new Logger
			{
				RequestActionType = RequestActionTypes.InternalLogic.ToString(),
				ResponseResult = Logger.RESULT_OK,
				ErrorMessage = "[재시작] 완료 보고",
			};
			WriteLog(logger, "[재시작] 후 이메일 발송");
		}

		private async void RequestAction(RequestActionTypes requestActionType)
		{
			// Interval: 현재 시간 - Interval 분
			// Time: 현재 시간 - 60분
			// 요청 시간 포멧 "2023-06-12T06:00:00.000Z";
			var now = DateTime.Now;
			var requestAction = Env.Instance.RequestAction;
			var addMin = requestAction.Type == RequestActionTypes.ByInterval.ToString() ? requestAction.Interval : requestAction.PerTime;
			var interval = now.AddMinutes(addMin * -1d);
			var condition = $"{now:yyyy-MM-dd}T{interval:HH:mm:ss}.000Z";
			var conditionTo = string.Empty;

			if (requestActionType == RequestActionTypes.ByManual)
			{
				condition = $"{dtpManualFrom.Value:yyyy-MM-dd}T{dtpManualFrom.Value:HH:mm:00}.000Z";
				conditionTo = $"{dtpManualTo.Value:yyyy-MM-dd}T{dtpManualTo.Value:HH:mm:59}.000Z";
			}

			WriteLog($"[{requestActionType}] 요청을 시작합니다.", requestActionType);

			if (Api.LmsSessionId == string.Empty)
			{
				await LmsAuthenticate();
			}

			if (Api.QmsSessionId == string.Empty)
			{
				await QmsAuthenticate();
			}

			var retrySessionCount = 0;
			var retrySession = false;

			do
			{
				var requirement = Api.Query(Api.GetRequirementQuery(condition, conditionTo));
				retrySession = await ProcessQueryResult(requirement, requestActionType);

				if (retrySession == true)
				{
					WriteLog("[LMS] Session이 만료되어 재시도 합니다.", requestActionType);
					retrySessionCount++;
					Api.ClearLmsSessionId();
					await LmsAuthenticate();
				}
			} while (retrySession == true && retrySessionCount <= 1);

			retrySessionCount = 0;
			retrySession = false;
			do
			{
				var assignment = Api.Query(Api.GetAssignmentQuery(condition, conditionTo));
				retrySession = await ProcessQueryResult(assignment, requestActionType);

				if (retrySession == true)
				{
					WriteLog("[LMS] Session이 만료되어 재시도 합니다.", requestActionType);
					retrySessionCount++;
					Api.ClearLmsSessionId();
					await LmsAuthenticate();
				}
			} while (retrySession == true && retrySessionCount <= 1);

			retrySessionCount = 0;
			retrySession = false;
			do
			{
				var quality = Api.Query(Api.GetQualityQuery(condition, conditionTo));
				retrySession = await ProcessQueryResult(quality, requestActionType);

				if (retrySession == true)
				{
					WriteLog("[QMS] Session이 만료되어 재시도 합니다.", requestActionType);
					retrySessionCount++;
					Api.ClearQmsSessionId();
					await LmsAuthenticate();
				}
			} while (retrySession == true && retrySessionCount <= 1);

			QueryLog();
		}

		private async Task LmsAuthenticate()
		{
			WriteLog($"[LMS] 인증을 시작합니다.", RequestActionTypes.Authenticate);

			var errorMsg = await Api.Authenticate(AuthTypes.Lms);

			if (errorMsg != string.Empty)
			{
				WriteLog(new Logger
				{
					RequestActionType = RequestActionTypes.Authenticate.ToString(),
					ErrorMessage = errorMsg,
					ResponseResult = Logger.RESULT_ER,
				}, "[LMS] 인증 실패");

				Api.ClearLmsSessionId();
			}
		}

		private async Task QmsAuthenticate()
		{
			WriteLog($"[QMS] 인증을 시작합니다.", RequestActionTypes.Authenticate);

			var errorMsg = await Api.Authenticate(AuthTypes.Qms);

			if (errorMsg != string.Empty)
			{
				WriteLog(new Logger
				{
					RequestActionType = RequestActionTypes.Authenticate.ToString(),
					ErrorMessage = errorMsg,
					ResponseResult = Logger.RESULT_ER,
				}, "[QMS] 인증 실패");

				Api.ClearLmsSessionId();
			}
		}

		/// <summary>
		/// 세션이 만료되면 재요청을 하기위해 true를 반환한다.
		/// </summary>
		/// <param name="queryResult"></param>
		/// <param name="requestActionType"></param>
		/// <returns></returns>
		private async Task<bool> ProcessQueryResult(IAsyncEnumerable<QueryResponse> queryResult, RequestActionTypes requestActionType)
		{
			await foreach (var item in queryResult)
			{
				var subject = string.Empty;

				item.Request.RequestActionType = requestActionType.ToString();

				if (item?.Data?.Count == 0 && Env.Instance.EmailAction.Types.Contains(EmailActionTypes.OnEmpty.ToString()))
				{
					subject = $"[{item?.Request.TableName}] 응답 데이터가 [0] 건입니다.";

					WriteLog(item!, subject);

					break;
				}

				if (item?.ErrorMessage == "[Invalid or expired session ID.]")
				{
					return true;
				}

				if (item?.ErrorMessage != string.Empty)
				{
					if (Env.Instance.EmailAction.Types.Contains(EmailActionTypes.OnFail.ToString()))
					{
						subject = $"[{item?.Request.TableName}] 데이터 요청중 에러가 발생했습니다.";
					}

					WriteLog(item!, subject);

					break;
				}

				switch (item!.Request.TableName)
				{
					case TableNames.Requirement:
						Db.Requirements.AddRange(item?.Data?.ToObject<IList<Requirement>>()!);

						break;
					case TableNames.Assignment:
						Db.Assignments.AddRange(item?.Data?.ToObject<IList<Assignment>>()!);

						break;
					case TableNames.ProductQuality:
						Db.ProductQualities.AddRange(item?.Data?.ToObject<IList<ProductQuality>>()!);

						break;
				}

				Db.SaveChanges();
				WriteLog(item!);
			}

			return false;
		}

	}
}
