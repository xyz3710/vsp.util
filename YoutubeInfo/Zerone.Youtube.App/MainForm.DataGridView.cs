﻿using Zerone.Youtube.Dac;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Zerone.Youtube.App
{
	partial class MainForm
	{
		private void InitdataGridView()
		{
			dgvDataLog.SetDoubleBuffered();
			dgvDataLog.SuspendLayout();

			for (var i = 1; i < dgvDataLog.Columns.Count; i++)
			{
				dgvDataLog.ColumnHeadersDefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
			}

			dgvDataLog.Columns[1].Width = 126; // RequestDate
			dgvDataLog.Columns[2].Width = 126; // RequestTime
			dgvDataLog.Columns[3].Width = 126; // RequestActionType
			dgvDataLog.Columns[4].Width = 146; // RequestTable
			dgvDataLog.Columns[5].Width = 232; // RequestQuery
			dgvDataLog.Columns[6].Width = 80;  // ResponseCount
			dgvDataLog.Columns[7].Width = 80;  // ResponseResult
			dgvDataLog.Columns[8].AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells; // ErrorMessage
			dgvDataLog.ResumeLayout();

			dgvDataLog.FilterStringChanged += OnDataLog_FilterStringChanged;
			dgvDataLog.SortStringChanged += OnDataLog_SortStringChanged;
		}

		private List<Logger> LoggerSource = new List<Logger>();

		private async void QueryLog()
		{
			var from = $"{dtpLogFrom.Value:yyyy-MM-dd}";
			var to = $"{dtpLogTo.Value:yyyy-MM-dd}";

			await Task.Run(() =>
			{
				using var loggerReadingContext = CreateDbContext();

				LoggerSource = loggerReadingContext.Loggers
					.Where(x => x.RequestDate.CompareTo(from) >= 0
							 && x.RequestDate.CompareTo(to) <= 0)
					.OrderByDescending(x => x.RequestDate)
					.ThenByDescending(x => x.RequestTime)
					.ToList();
			});

			Invoke(new MethodInvoker(() =>
			{
				dgvDataLog.SuspendLayout();
				dgvDataLog.DataSource = LoggerSource;
				dgvDataLog.ResumeLayout();
			}));
		}

		/// <summary>
		/// https://github.com/davidegironi/advanceddatagridview/issues/24
		/// </summary>
		/// <param name="filter"></param>
		/// <returns></returns>
		private string FilterStringConverter(string filter)
		{
			if (filter == string.Empty)
			{
				return string.Empty;
			}

			// get rid of all the parenthesis 
			filter = filter.Replace("(", "").Replace(")", "");
			// now split the string on the 'and' (each grid column)
			var colFilterList = filter.Split(new string[] { "AND" }, StringSplitOptions.None);
			var newColFilter = "";
			var andOperator = "";

			foreach (var colFilter in colFilterList)
			{
				newColFilter += andOperator;
				// split string on the 'in'
				var temp1 = colFilter.Trim().Split(new string[] { "IN" }, StringSplitOptions.None);
				// get string between square brackets
				var colName = temp1[0].Split('[', ']')[1].Trim();
				// prepare beginning of linq statement
				newColFilter += string.Format("({0} != null && (", colName);

				var orOperator = "";
				var filterValsList = temp1[1].Split(',');

				foreach (var filterVal in filterValsList)
				{
					// remove any single quotes before testing if filter is a num or not
					var cleanFilterVal = filterVal.Replace("'", "").Trim();
					var tempNum = 0d;

					if (Double.TryParse(cleanFilterVal, out tempNum))
					{
						newColFilter += string.Format("{0} {1} = {2}", orOperator, colName, cleanFilterVal.Trim());
					}
					else
					{
						newColFilter += string.Format("{0} {1}.Contains('{2}')", orOperator, colName, cleanFilterVal.Trim());
					}

					orOperator = " OR ";
				}

				newColFilter += "))";
				andOperator = " AND ";
			}

			// replace all single quotes with double quotes
			return newColFilter.Replace("'", "\"");
		}

		private string SortStringConverter(string sortString)
		{
			if (sortString == string.Empty)
			{
				return string.Empty;
			}

			return sortString.Replace("[", "").Replace("]", "");
		}

		private void OnDataLog_SortStringChanged(object? sender, Zuby.ADGV.AdvancedDataGridView.SortEventArgs e)
		{
			try
			{
				if (e.SortString == null)
				{
					e.SortString = "[RequestDate] DESC, [RequestTime] DESC";
				}

				var sortToken = SortStringConverter(e.SortString);

				if (string.IsNullOrEmpty(sortToken) == true)
				{
					dgvDataLog.DataSource = LoggerSource;
				}
				else
				{
					// System.Linq.Dynamic.Core 필요
					dgvDataLog.DataSource = LoggerSource.AsQueryable().OrderBy(sortToken).ToList();
				}
			}
			catch (Exception ex)
			{
				System.Diagnostics.Debug.WriteLine(ex.Message, "MainForm.OnDataLog_SortStringChanged");
			}
		}

		private void OnDataLog_FilterStringChanged(object? sender, Zuby.ADGV.AdvancedDataGridView.FilterEventArgs e)
		{
			try
			{
				var filter = FilterStringConverter(e.FilterString);

				if (string.IsNullOrEmpty(filter) == true)
				{
					dgvDataLog.DataSource = LoggerSource;
				}
				else
				{
					// System.Linq.Dynamic.Core 필요
					dgvDataLog.DataSource = LoggerSource.AsQueryable().Where(filter).ToList();
				}
			}
			catch (Exception ex)
			{
				System.Diagnostics.Debug.WriteLine(ex.Message, "MainForm.OnDataLog_FilterStringChanged");
			}
		}
	}
}
