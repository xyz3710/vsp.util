﻿using System;
using System.Windows.Forms;

using Azure;

using Zerone.Youtube.Environment;
using Zerone.Youtube.App.Common;
using Zerone.Youtube.RestHelper;

using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Microsoft.VisualBasic.Logging;

namespace Zerone.Youtube.App
{
	public partial class MainForm : Form
	{
		/// <summary>
		/// 
		/// </summary>
		public MainForm(bool retartAction = false)
		{
			InitializeComponent();
			teLog.Font = Font;
			InRestartAction = retartAction;
		}

		private System.Timers.Timer MainTimer { get; set; }
		private bool InRestartAction { get; set; }

		private RestHelper Api { get; set; }
		private BioDbContext Db { get; set; }

		protected override void OnLoad(EventArgs e)
		{
			base.OnLoad(e);
			tsSetting.Click += (sender, ea) => ShowSettingForm();
			btnStart.Click += (sender, ea) => StartAction();
			btnStop.Click += (sender, ea) => StopAction();
			btnManual.Click += (sender, ea) => ManualAction();
			btnQueryLog.Click += (sender, ea) => QueryLog();
			InitdataGridView();
			InitMainTimer();
			InitLogCondition();
		}

		protected override void OnShown(EventArgs e)
		{
			base.OnShown(e);
			LoadEnv();

			if (InRestartAction
				&& Env.Instance.EmailAction.Types.Contains(EmailActionTypes.OnAfterRestart.ToString()))
			{
				SendAfterRestartEmail();
			}

			StartAction(); // 시작 상태로 시작
			MainTimer.Start();
			QueryLog();
		}

		protected override void OnFormClosing(FormClosingEventArgs e)
		{
			base.OnFormClosing(e);
			MainTimer.Stop();
		}

		private void InitMainTimer()
		{
			MainTimer = new System.Timers.Timer(1000.0d);
			MainTimer.Elapsed += OnMainTimerElapsed;
		}

		/// <summary>
		/// Shows the setting form.
		/// </summary>
		private void ShowSettingForm()
		{
			using (var settingForm = new SettingForm())
			{
				if (settingForm.ShowDialog(this) == DialogResult.OK)
				{
					var env = EnvHelper.Load(Constants.ENV_PATH);

					if (env == null)
					{
						return;
					}

					Env.Initialize(
						env.YTInfo,
						env.BioDb,
						env.BioEmail,
						env.RequestAction,
						env.EmailAction,
						env.RestartAction);
					SetActionAftetLoadEnv();
				}
			}
		}

		private void WriteLog(QueryResponse response, string emailSubject = "")
		{
			var logger = new Logger
			{
				RequestDate = response.Request.RequestDate,
				RequestTime = response.Request.RequestTime,
				RequestActionType = response.Request.RequestActionType,
				RequestTable = response.Request.TableName.ToString(),
				RequestQuery = response.Request.Condition, // 검색 조건만 전달 한다.
				ResponseCount = response.Total,
				ErrorMessage = response.ErrorMessage,
				ResponseResult = response.ErrorMessage == string.Empty ? Logger.RESULT_OK : Logger.RESULT_ER,
			};

			WriteLog(logger, emailSubject);
		}

		private void WriteLog(string message, RequestActionTypes requestActionType)
		{
			var logger = new Logger
			{
				RequestActionType = requestActionType.ToString(),
				ErrorMessage = message,
			};

			WriteLog(logger);
		}

		private void WriteLog(Logger logger, string emailSubject = "")
		{
			if (emailSubject != string.Empty)
			{
				var errorLog = string.Empty;

				SendEmail(emailSubject, logger.ToString(), logAction: log =>
				{
					errorLog = log;
					WriteLog($"{emailSubject} Email을 발송 중 오류가 발생했습니다. {log}", RequestActionTypes.InternalLogic);
				});

				if (errorLog == string.Empty)
				{
					WriteLog($"{emailSubject} Email을 발송했습니다.", RequestActionTypes.InternalLogic);
				}
			}

			WriteMessageLog(logger.ToString());
			WriteDbLog(logger);
		}

		private void WriteMessageLog(string message)
		{
			Invoke(new MethodInvoker(() =>
			{
				teLog.Text = teLog.Text.Insert(0, $"[{DateTime.Now:MM-dd HH:mm:ss}] {message}\r\n");
				teLog.ActiveTextAreaControl.Caret.Column = 0;
				teLog.Refresh();
			}));
		}

		private void WriteDbLog(Logger logger)
		{
			Db.Loggers.Add(logger);

			try
			{
				Db.SaveChanges();
			}
			catch (Exception ex)
			{
				System.Diagnostics.Debug.WriteLine(ex.Message, "MainForm.WriteDbLog");
				Thread.Sleep(500);
				Db.SaveChanges();
			}
		}

		private void LoadEnv()
		{
			if (File.Exists(Constants.ENV_PATH) == false)
			{
				ShowSettingForm();
			}

			var env = EnvHelper.Load(Constants.ENV_PATH);

			if (env == null)
			{
				MessageBox.Show(
					"설정 파일이 잘못 되었습니다.\r\n환경 설정을 한 뒤 프로그램을 재시작 해주세요.",
					Text,
					MessageBoxButtons.OK,
					MessageBoxIcon.Warning);
				ShowSettingForm();

				return;
			}

			Env.Initialize(env);
			SetActionAftetLoadEnv();
		}

		private void SetActionAftetLoadEnv()
		{
			ActionFromTime = DateTime.ParseExact(Env.Instance.RequestAction.ActionFromTime, "HH:mm", null);
			ActionToTime = DateTime.ParseExact(Env.Instance.RequestAction.ActionToTime, "HH:mm", null);
			lbActionPeriod.Text = $"{Env.Instance.RequestAction.ActionFromTime} ~ {Env.Instance.RequestAction.ActionToTime}";

			SetRestTime();
			UpdateRestTime();

			RestartTime = DateTime.ParseExact(Env.Instance.RestartAction.ActionTime, "HH:mm", null);
			RestartMin = RestartTime.Minute;

			Api?.Dispose();
			Api = new RestHelper(Env.Instance.YTInfo);

			Db?.Dispose();
			Db = CreateDbContext();
		}

		private BioDbContext CreateDbContext()
		{
			Action<string>? logger = null;
#if DEBUG
			//logger = WriteMessageLog;
#endif
			return new BioDbContext(Env.Instance.BioDb.ConnectionString, logger);
		}

		/// <summary>
		/// 작동 시간 내 시간인지
		/// </summary>
		private bool IsInTimeToAction { get; set; } = true;
		/// <summary>
		/// 설정된 작동시간의 From Time을 미리 저장해둔다
		/// </summary>
		private DateTime ActionFromTime { get; set; }
		private DateTime ActionToTime { get; set; }

		private DateTime RestartTime { get; set; }
		private int RestartMin { get; set; }

		/// <summary>
		/// 남은 분
		/// </summary>
		private int RestSec { get; set; } = -1;
		private String GetRestTime() => RestSec == -1 ? string.Empty : $"{(RestSec / 60):00}:{(RestSec % 60):00}";
		private string RestTimeLabelByInterval => $"[지정 간격] {GetRestTime()} 남음";
		private string RestTimeLabelByTime => $"[지정 시간] {GetRestTime()} 남음";

		private void SetRestTime()
		{
			if (Env.Instance.RequestAction.Type == RequestActionTypes.ByInterval.ToString())
			{
				RestSec = Env.Instance.RequestAction.Interval * 60;
			}
			else
			{
				var min = DateTime.Now.Minute;
				var sec = DateTime.Now.Second;

				if (min == Env.Instance.RequestAction.PerTime)
				{
					if (sec == 0) // 이미 실행을 했다면 다음 interval을 계산
					{
						RestSec = (60 - min + Env.Instance.RequestAction.PerTime) * 60;
					}
				}
				else if (min > Env.Instance.RequestAction.PerTime)
				{
					RestSec = (60 - min + Env.Instance.RequestAction.PerTime) * 60;
				}
				else
				{
					RestSec = (Env.Instance.RequestAction.PerTime - min) * 60;
				}

				if (sec != 0)
				{
					RestSec -= 60; // 1분 제거 후
					RestSec += 60 - sec;
				}
			}
		}

		private void UpdateRestTime()
		{
			Invoke(new MethodInvoker(() =>
			{
				lbNextTime.Text = Env.Instance.RequestAction.Type == RequestActionTypes.ByInterval.ToString()
								? RestTimeLabelByInterval : RestTimeLabelByTime;
			}));
		}

		private void InitLogCondition()
		{
			dtpLogFrom.Value = DateTime.ParseExact($"{DateTime.Now:yyyy-MM-01}", "yyyy-MM-dd", null);
			dtpLogTo.Value = DateTime.Now;
			dtpLogFrom.KeyDown += (sender, ea) =>
			{
				if (ea.KeyData == Keys.Enter)
				{
					btnQueryLog.PerformClick();
				}
			};
			dtpLogTo.KeyDown += (sender, ea) =>
			{
				if (ea.KeyData == Keys.Enter)
				{
					btnQueryLog.PerformClick();
				}
			};
		}
	}
}