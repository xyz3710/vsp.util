﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zerone.Youtube.API
{
	/// <summary>
	/// 요청 공통
	/// </summary>
	[System.Diagnostics.DebuggerDisplay("Key:{Key}, Type:{Type}, Part:{Part}", Name = "RequestBase")]
	public abstract class RequestBase
	{
		/// <summary>
		/// 요청 기본 형식
		/// </summary>
		/// <param name="api">Youtube API</param>
		public RequestBase(YoutubeApi api)
		{
			API = api;
			Key = api.Key;
		}

		/// <summary>
		/// Youtube Api
		/// </summary>
		protected YoutubeApi API { get; set; }

		/// <summary>
		/// API Key
		/// </summary>
		protected string Key { get; set; }

		/// <summary>
		/// Type
		/// </summary>
		public string Type { get; set; } = "video";

		/// <summary>
		/// Authentication password
		/// </summary>
		public string Part { get; set; } = "id,snippet";

		/// <summary>
		/// 요청 URL을 반환하도록 합니다.
		/// </summary>
		/// <returns></returns>
		public abstract string GetRequestUrl();
	}
}
