﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zerone.Youtube.API
{
	/// <summary>
	/// 요청 공통
	/// </summary>
	[System.Diagnostics.DebuggerDisplay("Kind:{Kind}, Etag:{Etag}, PageInfo:{PageInfo}", Name = "ResponseBase")]
	public abstract class ResponseBase
	{
		/// <summary>
		/// ChannelId
		/// </summary>
		public string Kind { get; set; } = "";

		/// <summary>
		/// etag
		/// </summary>
		public string Etag { get; set; } = "";

		/// <summary>
		/// Page 정보
		/// </summary>
		public PageInfo PageInfo { get; set; } = new PageInfo();
	}
}
