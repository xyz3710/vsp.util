﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;

using Zerone.Youtube.API;

namespace Zerone.Youtube.API
{
	/// <summary>
	/// Video list Request data
	/// </summary>
	/// <remarks>응답은 <see cref="VideoList"/></remarks>
	[System.Diagnostics.DebuggerDisplay("ChannelId:{ChannelId}, MaxResult:{MaxResult}, Order:{Order}, PageToken:{PageToken}, VideoListUrl:{VideoListUrl}", Name = "VideoListRequest")]
	public class VideoListRequest : RequestBase
	{
		/// <summary>
		/// 비디오 목록 요청 데이터
		/// </summary>
		/// <param name="channelId"></param>
		public VideoListRequest(YoutubeApi api, string channelId)
			: base(api)
		{
			ChannelId = channelId;
		}

		/// <summary>
		/// ChannelId
		/// </summary>
		public string ChannelId { get; set; }

		/// <summary>
		/// 한번에 요청할 데이터 크기 (5 ~ 50)
		/// </summary>
		public int MaxResult { get; set; } = 50;

		/// <summary>
		/// Order: date/title
		/// </summary>
		public string Order { get; set; } = "date";

		/// <summary>
		/// PageToken (지정되면 MaxResult 만큼으로 나뉜 NextPage 값)
		/// </summary>
		public string PageToken { get; set; } = "";

		/// <summary>
		/// Video list Url을 구합니다.
		/// </summary>
		public string VideoListUrl => $"{API.BaseUrl}/{API.VideoList}?key={Key}&type={Type}&part={Part}&channelId={ChannelId}&maxResults={MaxResult}&order={Order}&pageToken={PageToken}";

		/// <summary>
		/// 요청 URL을 반환하도록 합니다.
		/// </summary>
		/// <returns></returns>
		public override string GetRequestUrl() => VideoListUrl;
	}
}