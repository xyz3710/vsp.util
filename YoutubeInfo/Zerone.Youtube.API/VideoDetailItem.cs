﻿using Newtonsoft.Json;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zerone.Youtube.API
{
	/// <summary>
	/// VideoDetailItem
	/// </summary>
	public class VideoDetailItem : ItemBase
	{
		/// <summary>
		/// Video id
		/// </summary>
		//public override string Id { get; set; } = "";
		public string Id { get; set; }
	}
}
