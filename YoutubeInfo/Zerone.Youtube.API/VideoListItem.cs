﻿using Newtonsoft.Json;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zerone.Youtube.API
{
	/// <summary>
	/// VideoListItem
	/// </summary>
	[System.Diagnostics.DebuggerDisplay("Id:{Id}", Name = "VideoListItem")]
	public class VideoListItem : ItemBase
	{
		/// <summary>
		/// Gets or sets the id.
		/// </summary>
		//[JsonProperty("id/videoId")]
		//[JsonIgnore]
		//public override string Id { get; set; } = "";

		[JsonProperty("id")]
		public object Id { get; set; }
	}
}
