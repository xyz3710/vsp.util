﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Zerone.Youtube.API
{
	/// <summary>
	/// videolist/pageinfo
	/// </summary>
	[System.Diagnostics.DebuggerDisplay("TotalResults:{TotalResults}, ResultsPerPage:{ResultsPerPage}", Name = "PageInfo")]
	public class PageInfo
	{
		/// <summary>
		/// 전체 페이지
		/// </summary>
		public int TotalResults { get; set; } = 1;

		/// <summary>
		/// 페이지당 요청 갯수
		/// </summary>
		public int ResultsPerPage { get; set; } = 5;
	}
}