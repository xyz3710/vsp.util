﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zerone.Youtube.API
{
	/// <summary>
	/// 비디오 정보
	/// </summary>
	public class VideoInfo
	{
		/// <summary>
		/// Video ID
		/// </summary>
		public string Id { get; set; } = "";

		/// <summary>
		/// Video Title
		/// </summary>
		public string Title { get; set; } = "";

		/// <summary>
		/// Description
		/// </summary>
		public string Desc { get; set; } = "";

		/// <summary>
		/// NewLine이 제거된 Desc list
		/// </summary>
		//public IList<string> DescList { get; set; } = new List<string>();

		/// <summary>
		/// Thumnail Max Res Url
		/// </summary>
		public string ThumnailMaxResUrl { get; set; } = "";

		/// <summary>
		/// Thumnail Standard Res Url <summary>
		/// </summary>
		public string ThumnailStdResUrl { get; set; } = "";

		/// <summary>
		/// 
		/// </summary>
		public DateTime? PublishedAt { get; set; }

		/// <summary>
		/// Tags
		/// </summary>
		public IList<string> Tags { get; set; } = new List<string>();
	}
}
