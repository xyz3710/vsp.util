﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text.Json.Serialization;
using System.Threading.Channels;

using Zerone.Youtube.API;

namespace Zerone.Youtube.API
{
	/// <summary>
	/// VideoDetail Request data
	/// </summary>
	/// <remarks>응답은 <see cref="VideoDetail"/></remarks>
	[System.Diagnostics.DebuggerDisplay("Id:{Id}, VideoDetailUrl:{VideoDetailUrl}", Name = "VideoDetailRequest")]
	public class VideoDetailRequest : RequestBase
	{
		/// <summary>
		/// 비디오 목록 요청 데이터
		/// </summary>
		/// <param name="videoId">VideoId</param>
		public VideoDetailRequest(YoutubeApi api, string videoId)
			: base(api)
		{
			VideoId = videoId;
		}

		/// <summary>
		/// Video Id
		/// </summary>
		[JsonPropertyName("id")]
		public string VideoId { get; set; }

		/// <summary>
		/// Video detail Url을 구합니다.
		/// </summary>
		public string VideoDetailUrl => $"{API.BaseUrl}/{API.VideoList}?key={Key}&type={Type}&part={Part}&id={VideoId}";

		/// <summary>
		/// 요청 URL을 반환하도록 합니다.
		/// </summary>
		/// <returns></returns>
		public override string GetRequestUrl() => VideoDetailUrl;
	}
}