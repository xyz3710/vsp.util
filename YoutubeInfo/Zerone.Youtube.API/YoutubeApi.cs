﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zerone.Youtube.API
{
	/// <summary>
	/// Youtube Api 용 환경 설정
	/// <see cref="https://developers.google.com/youtube/v3/guides/implementation?hl=ko"/>
	/// </summary>
	public class YoutubeApi
	{
		/// <summary>
		/// Youtube 요청용 객체 생성자
		/// </summary>
		/// <param name="key">인증 키</param>
		public YoutubeApi(string key)
		{
			Key = key;
		}

		/// <summary>
		/// API Key
		/// </summary>
		public string Key { get; set; }

		/// <summary>
		/// BaseUrl
		/// </summary>
		[DefaultValue("https://www.googleapis.com/youtube/v3")]
		public string BaseUrl { get; set; } = "https://www.googleapis.com/youtube/v3";

		/// <summary>
		/// Video list
		/// </summary>
		[DefaultValue("https://www.googleapis.com/youtube/v3/search")]
		public string VideoList { get; set; } = "search";

		/// <summary>
		/// Video detail
		/// </summary>
		[DefaultValue("https://www.googleapis.com/youtube/v3/videos")]
		public string VideoDetail { get; set; } = "videos";
	}
}