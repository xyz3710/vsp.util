﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zerone.Youtube.API
{
	/// <summary>
	/// Error 발생 시 데이터
	/// </summary>
	[System.Diagnostics.DebuggerDisplay("Code:{Code}, Message:{Message}, Status:{Status}, Errors:{Errors}, Details:{Details}", Name = "Error")]
	public class Error
	{
		/// <summary>
		/// Gets or sets the code.
		/// </summary>
		public int Code { get; set; }

		/// <summary>
		/// Gets or sets the message.
		/// </summary>
		public string Message { get; set; } = "";

		/// <summary>
		/// Gets or sets the status.
		/// </summary>
		public string Status { get; set; } = "";

		/// <summary>
		/// Gets or sets the errors.
		/// </summary>
		public IList<ErrorInfo> Errors { get; set; } = new List<ErrorInfo>();

		/// <summary>
		/// Gets or sets the details.
		/// </summary>
		public IList<ErrorDetail> Details { get; set; } = new List<ErrorDetail>();
	}

	/// <summary>
	/// 에러 정보
	/// </summary>
	[System.Diagnostics.DebuggerDisplay("Message:{Message}, Reason:{Reason}, Domain:{Domain}, Location:{Location}, LocationType:{LocationType}", Name = "ErrorInfo")]
	public class ErrorInfo
	{
		/// <summary>
		/// Gets or sets the message.
		/// </summary>
		public string Message { get; set; } = "";

		/// <summary>
		/// Gets or sets the reason.
		/// </summary>
		public string Reason { get; set; } = "";

		/// <summary>
		/// Gets or sets the domain.
		/// </summary>
		public string Domain { get; set; } = "";

		/// <summary>
		/// Gets or sets the location.
		/// </summary>
		public string Location { get; set; } = "";

		/// <summary>
		/// Gets or sets the location type.
		/// </summary>
		public string LocationType { get; set; } = "";
	}

	/// <summary>
	/// The error detail.
	/// </summary>
	[System.Diagnostics.DebuggerDisplay("Type:{Type}, Reason:{Reason}, Domain:{Domain}", Name = "ErrorDetail")]
	public class ErrorDetail
	{
		/// <summary>
		/// Gets or sets the type.
		/// </summary>
		public string Type { get; set; } = "";

		/// <summary>
		/// Gets or sets the reason.
		/// </summary>
		public string Reason { get; set; } = "";

		/// <summary>
		/// Gets or sets the domain.
		/// </summary>
		public string Domain { get; set; } = "";
	}
}
