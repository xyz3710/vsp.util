﻿using Newtonsoft.Json;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zerone.Youtube.API
{
	/// <summary>
	/// items/snippet
	/// </summary>
	[System.Diagnostics.DebuggerDisplay("Title:{Title}, Description:{Description}, Thumbnails:{Thumbnails}, PublishedAt:{PublishedAt}", Name = "Snippet")]
	public class Snippet
	{
		/// <summary>
		/// Title
		/// </summary>
		public string Title { get; set; } = "";

		/// <summary>
		/// 설명 일부
		/// </summary>
		public string Description { get; set; } = "";

		/// <summary>
		/// Thumbnails
		/// </summary>
		public Thumbnails Thumbnails { get; set; } = new Thumbnails();

		/// <summary>
		/// 요청한 Channel Id
		/// </summary>
		public string ChannelId { get; set; } = "";

		/// <summary>
		/// 요청한 Chnnel Title
		/// </summary>
		public string ChannelTitle { get; set; } = "";

		/// <summary>
		/// 발행 일자
		/// </summary>
		public DateTime PublishedAt { get; set; }

		/// <summary>
		/// PublishedAt과 동일한 값
		/// </summary>
		public DateTime PublishTime { get; set; }

		/// <summary>
		/// Live 방송용 contents인지 여부
		/// </summary>
		public string LiveBroadcastContent { get; set; } = "none";

		/*
		 * 아래는 Video Detail 시에만 값이 할당 됨
		 */

		/// <summary>
		/// Video tags
		/// </summary>
		/// <remarks>VideoDetail 시에만 할당됨</remarks>
		public IList<string> Tags { get; set; } = new List<string>();

		/// <summary>
		/// CategoryId
		/// </summary>
		/// <remarks>VideoDetail 시에만 할당됨</remarks>
		public string CategoryId { get; set; } = "";

		/// <summary>
		/// 기본 언어
		/// </summary>
		/// <remarks>VideoDetail 시에만 할당됨</remarks>
		public string DefaultLanguage { get; set; } = "ko";

		/// <summary>
		/// 기본 음성 언어
		/// </summary>
		/// <remarks>VideoDetail 시에만 할당됨</remarks>
		public string DefaultAudioLanguage { get; set; } = "ko";

		/// <summary>
		/// 다국어 값
		/// </summary>
		public Localized Localized { get; set; } = new Localized();
	}

	/// <summary>
	/// Localized
	/// </summary>
	[System.Diagnostics.DebuggerDisplay("Title:{Title}, Description:{Description}", Name = "Localized")]
	public class Localized
	{
		/// <summary>
		/// Title
		/// </summary>
		public string Title { get; set; } = "";

		/// <summary>
		/// 설명 일부
		/// </summary>
		public string Description { get; set; } = "";
	}
}
