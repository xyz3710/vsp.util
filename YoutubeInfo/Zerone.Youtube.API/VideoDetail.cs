﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;

namespace Zerone.Youtube.API
{
	/// <summary>
	/// Playlist Request data
	/// </summary>
	public class VideoDetail : ResponseBase
	{
		/// <summary>
		/// VideoList items
		/// </summary>
		public IList<VideoListItem> Items { get; set; } = new List<VideoListItem>();
	}
}