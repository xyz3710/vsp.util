﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace Zerone.Youtube.API
{
	/// <summary>
	/// Response item 공통
	/// </summary>
	public abstract class ItemBase
	{
		/// <summary>
		/// Kind
		/// </summary>
		public string Kind { get; set; } = "";

		/// <summary>
		/// etag
		/// </summary>
		public string Etag { get; set; } = "";

		/// <summary>
		/// videoList는 객체로, video는 값으로
		/// </summary>
		//[JsonIgnore]
		//public abstract string Id { get; set; }

		/// <summary>
		/// snippet
		/// </summary>
		public Snippet Snippet { get; set; } = new Snippet();
	}
}
