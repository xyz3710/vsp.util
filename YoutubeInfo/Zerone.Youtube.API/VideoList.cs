﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;

namespace Zerone.Youtube.API
{
	/// <summary>
	/// Playlist Request data
	/// </summary>
	[System.Diagnostics.DebuggerDisplay("NextPageToken:{NextPageToken}, RegionCode:{RegionCode}, Items:{Items}", Name = "VideoList")]
	public class VideoList : ResponseBase
	{
		/// <summary>
		/// 다음 페이지가 있을 경우 다음 페이지 토큰
		/// </summary>
		public string NextPageToken { get; set; } = "";

		/// <summary>
		/// 지역 설정
		/// </summary>
		public string RegionCode { get; set; } = "US";

		/// <summary>
		/// VideoList items
		/// </summary>
		public IList<VideoListItem> Items { get; set; } = new List<VideoListItem>();
	}
}