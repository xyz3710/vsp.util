﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Zerone.Youtube.API
{
	/// <summary>
	/// items/snippet/thumbnails
	/// </summary>
	public class Thumbnails
	{
		/// <summary>
		/// Default size info
		/// </summary>
		//[JsonProperty(IsReference = true)]
		public ThumbnailInfo? Default { get; set; }

		/// <summary>
		/// Medium size info
		/// </summary>
		public ThumbnailInfo? Medium { get; set; }

		/// <summary>
		/// High size info
		/// </summary>
		public ThumbnailInfo? High { get; set; }

		/// <summary>
		/// Standard size info
		/// </summary>
		public ThumbnailInfo? Standard { get; set; }

		/// <summary>
		/// 최대 해상도 size info
		/// </summary>
		public ThumbnailInfo? MaxRes { get; set; }
	}

	/// <summary>
	/// items/snippet/thumbnails/default,standard...
	/// </summary>
	[System.Diagnostics.DebuggerDisplay("Url:{Url}, Width:{Width}, Height:{Height}", Name = "ThumbnailInfo")]
	public class ThumbnailInfo
	{
		/// <summary>
		/// Url
		/// </summary>
		public string Url { get; set; } = "";

		/// <summary>
		/// Width
		/// </summary>
		public int Width { get; set; }

		/// <summary>
		/// Height
		/// </summary>
		public int Height { get; set; }
	}
}
