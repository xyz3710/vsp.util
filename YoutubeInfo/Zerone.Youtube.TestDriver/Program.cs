﻿using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Text;
using System.Text.Json.Serialization;
using System.Text.RegularExpressions;

using Google.Apis.Services;
using Google.Apis.YouTube.v3;
using Google.Apis.YouTube.v3.Data;

using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

using Zerone.Youtube.API;
using Zerone.Youtube.Environment;

namespace Zerone.YTInfo.TestDriver
{
	/// <summary>
	/// The program.
	/// </summary>
	internal static class Program
	{
		/// <summary>
		///  The main entry point for the application.
		/// </summary>
		[STAThread]
		static void Main()
		{
			// https://console.cloud.google.com/apis/api/youtube.googleapis.com/credentials?hl=ko&project=youtube-api-zelda3710
			//var key = "AIzaSyC14uzcAbi2MyT7RZTxfKiIpZkZA1DPuUg"; // xyz3710
			// https://console.cloud.google.com/apis/api/youtube.googleapis.com/quotas?hl=ko&authuser=5&project=youtubeapi-test-396502
			var key = "AIzaSyCggkw42tyFrZMALBG-88PKHbInkGnFrXE"; // xyz37.test
			var api = new YoutubeApi(key);
			var channelId = "UC7zaP5OSmmMiby6SoCpQE2A";
			//var rest = new RestHelper();

			//VideoListTest(api, rest);
			//VideoListTest(key, channelId).Wait();

			var vid = "mRWM1auHWgE";
			var path = @".\_Data.json";
			var outPath = @".\_Data_Out.json";

			vid = "";
			var videoInfo = VideoDescTest(key, channelId, vid).Result;

			var json = JsonConvert.SerializeObject(videoInfo, Newtonsoft.Json.Formatting.Indented).Replace("null", "\"null\"");
			EnvHelper.SaveStringCore(path, json);

			DescParse(path, outPath);
			//ParseShrine(outPath);
			//ParseRoot(outPath);
		}

		//private static async void VideoListTest(YoutubeApi api, RestHelper rest)
		//{
		//	var channelId = "UC7zaP5OSmmMiby6SoCpQE2A";
		//	var videoListRequest = new VideoListRequest(api, channelId)
		//	{
		//		PageToken = "",
		//	};
		//	//videoListRequest.Order = "date1";
		//	var request = new QueryRequest(videoListRequest);
		//	var response = await rest.GetQuery(request);
		//}

		private static async Task VideoListTest(string key, string channelId)
		{
			var service = new YouTubeService(new BaseClientService.Initializer()
			{
				ApiKey = key,
				ApplicationName = "YoutubeInfo",
				//ApplicationName = GetType().ToString()
			});

			var videoListRequest = service.Search.List("snippet");
			//searchListRequest.Q = "팬텀"; // Replace with your search term.
			videoListRequest.MaxResults = 50;
			videoListRequest.ChannelId = channelId;
			videoListRequest.Order = SearchResource.ListRequest.OrderEnum.Date;
			videoListRequest.PageToken = string.Empty;

			SearchListResponse? videoListResponse = null;

			List<string> videos = new List<string>();
			List<string> channels = new List<string>();
			List<string> playlists = new List<string>();
			var videoId = "";

			do
			{
				videoListRequest.PageToken = videoListResponse?.NextPageToken;
				videoListResponse = await videoListRequest.ExecuteAsync();

				foreach (var item in videoListResponse.Items)
				{
					switch (item.Id.Kind)
					{
						case "youtube#video":
							videos.Add($"{item.Snippet.Title} ({item.Id.VideoId})");
							videoId = item.Id.VideoId;

							break;
						case "youtube#channel":
							channels.Add($"{item.Snippet.Title} ({item.Id.ChannelId})");

							break;
						case "youtube#playlist":
							playlists.Add($"{item.Snippet.Title} ({item.Id.PlaylistId})");

							break;
					}
				}
			} while (string.IsNullOrEmpty(videoListResponse.NextPageToken) == false);

			Console.WriteLine($"Videos:\n{(string.Join("\n", videos))}\n");
			Console.WriteLine($"Channels:\n{(string.Join("\n", channels))}\n");
			Console.WriteLine($"Playlists:\n{(string.Join("\n", playlists))}\n");

			var videoDetailRequest = service.Videos.List("snippet");

			videoDetailRequest.Id = videoId;

			var videoDetailRespose = await videoDetailRequest.ExecuteAsync();
			var snippet = videoDetailRespose.Items.FirstOrDefault()!.Snippet;
			var desc = snippet.Description;

			Console.WriteLine($"VideoId: {videoId}");
			Console.WriteLine($"Title: {snippet.Title}");
			Console.WriteLine($"{desc}");
			Console.WriteLine("===============================================");
			Console.WriteLine($"{GetLinkedDesc(desc, videoId)}");
			Console.WriteLine($"MaxRes: {snippet.Thumbnails.Maxres.Url}");
			Console.WriteLine($"Tags: {string.Join(", ", snippet.Tags)}");
		}

		private static async Task<List<VideoInfo>> VideoDescTest(string key, string channelId, string vid = "")
		{
			var service = new YouTubeService(new BaseClientService.Initializer()
			{
				ApiKey = key,
				ApplicationName = "YoutubeInfo",
				//ApplicationName = GetType().ToString()
			});

			var videoListRequest = service.Search.List("snippet");
			//searchListRequest.Q = "팬텀"; // Replace with your search term.
			videoListRequest.MaxResults = 50;
			videoListRequest.ChannelId = channelId;
			videoListRequest.Order = SearchResource.ListRequest.OrderEnum.Date;
			videoListRequest.PageToken = string.Empty;

			var videoDetailRequest = service.Videos.List("snippet");

			if (vid != string.Empty)
			{
				await GetVideoDetail(videoDetailRequest, vid);

				return new List<VideoInfo>();
			}

			SearchListResponse? videoListResponse = null;
			var videoListResult = new List<(string Title, string VId)>();

			do
			{
				videoListRequest.PageToken = videoListResponse?.NextPageToken;

				try
				{
					videoListResponse = await videoListRequest.ExecuteAsync();
				}
				catch (Exception ex)
				{
					Console.WriteLine(ex.Message);
				}

				videoListResult.AddRange(videoListResponse!.Items.Select(item => (item.Snippet.Title, item.Id.VideoId)));
				//var item = videoListResponse.Items.Select(item => $"{item.Id.VideoId}: {item.Snippet.Title}");
				//Console.WriteLine($"{videoListResponse.NextPageToken}: {item.First()}");
				//Console.WriteLine(string.Join("\n", item));
			} while (string.IsNullOrEmpty(videoListResponse.NextPageToken) == false);

			var videoList = videoListResult.Where(x => string.IsNullOrEmpty(x.VId) == false).OrderBy(x => x.Title);
			var count = 1;

			foreach (var item in videoList)
			{
				Console.WriteLine($"{count++:000} {item.VId}: {item.Title}");
			}

			var videoIds = videoList.Select(x => x.VId).ToList();
			var videoInfo = new List<VideoInfo>();

			foreach (string videoId in videoIds)
			{
				videoInfo.Add(await GetVideoDetail(videoDetailRequest, videoId));
			}

			return videoInfo;
		}

		private static async Task<VideoInfo> GetVideoDetail(VideosResource.ListRequest videoDetailRequest, string videoId)
		{
			videoDetailRequest.Id = videoId;
			VideoListResponse? videoDetailRespose = null;

			try
			{
				videoDetailRespose = await videoDetailRequest.ExecuteAsync();
			}
			catch (Exception ex)
			{
				Console.WriteLine(ex.Message);
			}

			var snippet = videoDetailRespose!.Items.FirstOrDefault()!.Snippet;

			var videoInfo = new VideoInfo
			{
				Id = videoId,
				Title = snippet.Title,
				Desc = snippet.Description,
				ThumnailMaxResUrl = (snippet.Thumbnails.Maxres ?? snippet.Thumbnails.Standard)?.Url,
				ThumnailStdResUrl = snippet.Thumbnails.Standard.Url,
				PublishedAt = snippet.PublishedAtDateTimeOffset?.DateTime,
				Tags = snippet.Tags,
			};

			var desc = snippet.Description;

			Console.WriteLine($"VideoId: {videoId}");
			Console.WriteLine($"Title: {snippet.Title}");
			Console.WriteLine($"{desc}");
			Console.WriteLine("===============================================");
			Console.WriteLine($"{GetLinkedDesc(desc, videoId)}");
			Console.WriteLine($"MaxRes: {snippet.Thumbnails.Maxres?.Url}");
			Console.WriteLine($"Tags: {string.Join(", ", snippet.Tags)}");

			return videoInfo;
		}

		private static Regex _timeRegEx = new Regex(@"((?<HH>\d{1,2}):)?(?<Min>\d{2}):(?<Sec>\d{2})", RegexOptions.Compiled);

		private static string GetLinkedDesc(string desc, string videoId)
		{
			var repeatDescIdx = desc.IndexOf("## 전체 재생 목록");

			desc = desc.Remove(repeatDescIdx);
			var matches = _timeRegEx.Matches(desc);

			if (matches.Count == 0)
			{
				return desc;
			}

			var times = new Dictionary<string, int>(matches.Count);

			foreach (Match match in matches)
			{
				var hh = (match.Groups["HH"].Success ? Convert.ToInt32(match.Groups["HH"].Value) : 0) * 3600;
				var min = Convert.ToInt32(match.Groups["Min"].Value) * 60;
				var sec = Convert.ToInt32(match.Groups["Sec"].Value);

				times.TryAdd(match.Value, hh + min + sec);
			}

			foreach (var time in times.Keys)
			{
				desc = desc.Replace(time, $"{time}(https://youtu.be/{videoId}?t={times[time]})");
			}

			return desc;
		}


		private static void DescParse(string path, string outPath)
		{
			var videoInfo = EnvHelper.Load<List<VideoInfo>>(path)!;
			var text = new List<string>();

			foreach (var vi in videoInfo)
			{
				var desc = GetLinkedDesc(vi.Desc, vi.Id);
				var title = vi.Title;

				//title = title.Replace("#Tears_of_the_Kingdom ", string.Empty).Substring(0, 3);

				Console.WriteLine($"##### {title}");
				Console.WriteLine(desc);

				vi.Desc = desc.Replace("\n", "\r\n");
				//vi.DescList = desc.Split("\n").ToList();
				text.Add($"##### {title}\r\n{vi.ThumnailMaxResUrl}\r\n{desc}");
			}

			EnvHelper.SaveStringCore(@".\_All.txt", string.Join("\r\n", text));
			EnvHelper.Save(videoInfo, outPath);
		}

		private static Regex _shrineRegEx = new Regex(@"^[\d.:]{1,}\s?(?:(?<Shrine>[\w ]*)\s+\((?<Seq>\d{1,3})\)[\s: ]*(?<Url>[\S-[,]]*)\r\s*(?<Desc>[\w .:?!,]*)\r\s*(?<Loc>[\w .:,-[\d\n]]*)|[\d:]*\((?<Url>\S*)\) (?<Shrine>[\w ]*)\s+\((?<Seq>\d{1,3})\),\s?(?<Desc>[\w ?!,]*)\r\s*(?<Loc>[\w ,]*))",
				RegexOptions.Compiled | RegexOptions.Multiline);
		/* 2개 조건 or
		^[\d.:]{1,}\s?(?<Shrine>[\w ]*)\s?\((?<Seq>\d{1,3})\)[\s: ]*(?<Url>[\S-[,]]*)\r\s*(?<Desc>[\w .:?!,]*)\r\s*(?<Loc>[\w .:,-[\d\n]]*)
		^[\d.:]{1,}\s?[\d:]*\((?<Url>\S*)\) (?<Shrine>[\w ]*)\s?\((?<Seq>\d{1,3})\),\s?(?<Desc>[\w ?!,]*)\r\s*(?<Loc>[\w ,]*)
		 */
		private static void ParseShrine(string path)
		{
			var videoInfo = EnvHelper.Load<List<VideoInfo>>(path)!;
			var list = new List<string>();
			var count = 0;

			foreach (var vi in videoInfo)
			{
				//Console.WriteLine($"##### {vi.Title}");

				//if (vi.Id != "mAhbmFAJcgA")
				//{
				//	continue;
				//}

				var matches = _shrineRegEx.Matches(vi.Desc);

				if (matches.Count == 0)
				{
					continue;
				}

				var seq = 0;
				var shrine = string.Empty;
				var desc = string.Empty;
				var url = string.Empty;
				var loc = string.Empty;

				foreach (Match match in matches)
				{
					if (match.Value.Contains("사당") == false || match.Value.Contains("오픈 못함") == true)
					{
						continue;
					}

					seq = Convert.ToInt32(match.Groups["Seq"].Value);
					shrine = match.Groups["Shrine"].Value;
					desc = match.Groups["Desc"].Value;
					url = match.Groups["Url"].Value;
					loc = match.Groups["Loc"].Value;

					if (desc != string.Empty)
					{
						desc = $", {desc}";
					}

					var text = $"{seq:000}. {shrine}{desc}\r\n     {url}\r\n     {loc}\r\n     {vi.Title}\r\n";

					list.Add(text);

					//if (count != seq)
					{
						Console.WriteLine(text);
					}
				}
			}

			EnvHelper.SaveStringCore(@".\_Shrines.txt", string.Join("\r\n", list), false);
		}

		private static Regex _rootRegEx = new Regex(@"^[\d.:]{1,}\s?(?:(?<Root>[\w ]* 뿌리)[: ]*(?<Url>[\S-[,]]*)|[\d:]*\((?<Url>\S*)\) (?<Root>[\w ]* 뿌리))\r\s*(?<Loc>[\w ㅡ.:,-[\d\n]]*)",
				RegexOptions.Compiled | RegexOptions.Multiline);
		/* 2개 조건 or
		^[\d.:]{1,}\s?(?<Root>[\w ]* 뿌리)[: ]*(?<Url>[\S-[,]]*)\r\s*(?<Loc>[\w ㅡ.:,-[\d\n]]*)
		^[\d.:]{1,}\s?[\d:]*\((?<Url>\S*)\) (?<Root>[\w ]* 뿌리)\r\s*(?<Loc>[\w ㅡ.:,-[\d\n]]*)
		 */

		private static void ParseRoot(string path)
		{
			var videoInfo = EnvHelper.Load<List<VideoInfo>>(path)!;
			var list = new List<string>();
			var count = 1;

			foreach (var vi in videoInfo)
			{
				//Console.WriteLine($"##### {vi.Title}");

				//if (vi.Id != "mAhbmFAJcgA")
				//{
				//	continue;
				//}

				var matches = _rootRegEx.Matches(vi.Desc);

				if (matches.Count == 0)
				{
					continue;
				}

				var root = string.Empty;
				var url = string.Empty;
				var loc = string.Empty;

				foreach (Match match in matches)
				{
					if (match.Value.Contains("뿌리") == false)
					{
						continue;
					}

					root = match.Groups["Root"].Value;
					url = match.Groups["Url"].Value;
					loc = match.Groups["Loc"].Value;

					var text = $"{count++:000}. {root}: {url}\r\n     {loc}\r\n";

					list.Add(text);
					Console.WriteLine(text);
				}
			}

			EnvHelper.SaveStringCore(@".\_Roots.txt", string.Join("\r\n", list), false);
		}
	}
}