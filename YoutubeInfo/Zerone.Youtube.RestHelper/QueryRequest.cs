﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Zerone.Youtube.API;

namespace Zerone.Youtube.RestHelper
{
	/// <summary>
	/// REST API 요청 DB 로깅용 객체
	/// </summary>
	[System.Diagnostics.DebuggerDisplay("Request:{Request}", Name = "QueryRequest")]
	public class QueryRequest
	{
		/// <summary>
		/// Initializes a new instance of the <see cref="QueryRequest"/> class.
		/// </summary>
		/// <param name="request">The request.</param>
		public QueryRequest(RequestBase request)
		{
			Request = request;
		}

		/// <summary>
		/// 요청 일자
		/// </summary>
		public string RequestDate { get; set; } = $"{DateTime.Now:yyyy-MM-dd}";

		/// <summary>
		/// 요청 시간
		/// </summary>
		public string RequestTime { get; set; } = $"{DateTime.Now:HH:mm:ss:fff}";

		/// <summary>
		/// Gets or sets the request.
		/// </summary>
		public RequestBase Request { get; private set; }
	}
}
