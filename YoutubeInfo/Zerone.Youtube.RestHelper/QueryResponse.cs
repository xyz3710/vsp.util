﻿using Newtonsoft.Json.Linq;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zerone.Youtube.RestHelper
{
	/// <summary>
	/// REST API Query 응답
	/// </summary>
	public class QueryResponse
	{
		/// <summary>
		/// 반환된 데이터
		/// </summary>
		public JArray? Data { get; set; }

		/// <summary>
		/// 전체 갯수
		/// </summary>
		public int Total { get; set; }

		/// <summary>
		/// 에러 메세지
		/// </summary>
		public string ErrorMessage { get; set; } = "";

		/// <summary>
		/// 남은 호출 횟수
		/// </summary>
		public int Rest { get; set; }

		/// <summary>
		/// 다음 페이지가 있을 경우 다음 페이지 Url
		/// </summary>
		public string NextPageUrl { get; set; } = "";

		/// <summary>
		/// 요청 쿼리
		/// </summary>
		public QueryRequest? Request { get; set; }
	}
}
