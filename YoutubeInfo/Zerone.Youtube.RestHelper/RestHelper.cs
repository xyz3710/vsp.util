﻿using System;
using System.Net;

using RestSharp;
using RestSharp.Authenticators;

using System.Threading;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Text;
using Zerone.Youtube.API;

namespace Zerone.Youtube.RestHelper
{
	/// <summary>
	/// Veeva REST API를 사용하는 클래스
	/// </summary>
	public class RestHelper : IDisposable
	{
		private const string RESPONSE_STATUS = "responseStatus";
		private const string RESPONSE_MESSAGE = "responseMessage";
		private const string RESPONSE_DETAILS = "responseDetails";
		private const string SUCCESS = "SUCCESS";

		/// <summary>
		/// Initializes a new instance of the <see cref="RestHelper"/> class.
		/// </summary>
		public RestHelper()
		{
			Client = InitRestClient();
		}

		/// <summary>
		/// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
		/// </summary>
		public void Dispose()
		{
			Client?.Dispose();
		}

		/// <summary>
		/// 구성된 RestClient
		/// </summary>
		public RestClient Client { get; private set; }

		/// <summary>
		/// Inits the rest client.
		/// </summary>
		/// <returns>A RestClient.</returns>
		private RestClient InitRestClient()
		{
			var options = new RestClientOptions()
			{
				ThrowOnAnyError = true,
			};
			var client = new RestClient(options, useClientFactory: false);

			client.AddDefaultHeaders(new Dictionary<string, string>() {
				//{"Content-Type", "application/x-www-form-urlencoded" },
				{"Accept", "application/json" },
				{"Accept-Encoding", "gzip, deflate, br" },
			});

			return client;
		}

		/// <summary>
		/// 반환된 json에서 에러 메세지를 구합니다.
		/// </summary>
		/// <param name="json"></param>
		/// <returns></returns>
		public string GetErrorMessage(JObject? json)
		{
			if (json == null)
			{
				return string.Empty;
			}

			var errorMessage = Convert.ToString(json[RESPONSE_MESSAGE]);
			var firstError = json["errors"]!.FirstOrDefault();

			if (firstError != null)
			{
				errorMessage = $"{errorMessage}[{firstError["message"]}]";
			}

			return errorMessage ?? string.Empty;
		}

		/// <summary>
		/// 응답이 성공인지 확인 합니다.
		/// </summary>
		/// <param name="json"></param>
		/// <returns></returns>
		public bool IsSuccess(JObject? json)
		{
			return Convert.ToString(json?[RESPONSE_STATUS]) == SUCCESS;
		}

		/// <summary>
		/// 응답 중 data token을 JArray로 반환하여 구합니다.
		/// </summary>
		/// <param name="resourceUrl">NextPageUrl이나 queryUrl</param>
		/// <param name="request">요청 객체(NextUrl을 지정하면 request.Query는 공백으로 할당 해야 함</param>
		/// <returns>data token의 arrary와 성공하면 responseDetails.total 또는 에러 메세지를 튜플로 반환합니다.</returns>
		public async Task<QueryResponse> GetQuery(QueryRequest request)
		{
			var restRequest = new RestRequest(request.Request.GetRequestUrl(), method: Method.Get);
			var contents = string.Empty;
			var queryResponse = new QueryResponse
			{
				Request = request,
			};

			try
			{
				var response = Client.Get(restRequest);
				//var response = await Task.Run(() =>
				//{
				//	return Client.Get(restRequest);
				//});
				//var response = await Client.GetJsonAsync<VideoListRequest>(request.Request.GetRequestUrl());

				if (response.StatusCode != HttpStatusCode.OK)
				{
					queryResponse.ErrorMessage = $"response not ok. [{response.ErrorMessage}]";

					return queryResponse;
				}

				contents = response.Content ?? string.Empty;
			}
			catch (Exception ex)
			{
				System.Diagnostics.Debug.WriteLine(ex.Message, "RestHelper.GetQuery");
			}

			if (JsonConvert.DeserializeObject(contents) is not JObject json)
			{
				queryResponse.ErrorMessage = "response is not json object.";

				return queryResponse;
			}

			if (IsSuccess(json) == false)
			{
				queryResponse.ErrorMessage = GetErrorMessage(json);

				return queryResponse;
			}

			if (json["data"] is not JArray data)
			{
				queryResponse.ErrorMessage = "json data token not found.";

				return queryResponse;
			}

			queryResponse.Data = data;
			queryResponse.Total = Convert.ToInt32(json[RESPONSE_DETAILS]?["total"] ?? "0");
			queryResponse.NextPageUrl = Convert.ToString(json[RESPONSE_DETAILS]?["next_page"]) ?? string.Empty;

			if (queryResponse.NextPageUrl != string.Empty)
			{
				var pageSize = Convert.ToInt32(json[RESPONSE_DETAILS]?["pagesize"] ?? "0");
				var pageOffset = Convert.ToInt32(json[RESPONSE_DETAILS]?["pageoffset"] ?? "0");
				var remain = (queryResponse.Total - (pageOffset + pageSize));
				var share = remain / pageSize;
				var left = remain % pageSize;

				queryResponse.Rest = share + (left == 0 ? 0 : 1);
			}

			return queryResponse;
		}

		/// <summary>
		/// NextPage가 있다면 모두 요청하여 반환 합니다.
		/// </summary>
		/// <param name="request"></param>
		/// <returns></returns>
		public async IAsyncEnumerable<QueryResponse> Query(QueryRequest request)
		{
			var queryResponse = new QueryResponse
			{
				Request = request,
			};

			do
			{
				if (queryResponse.NextPageUrl == string.Empty)
				{
					//queryResponse = await QueryOnePage(YTInfo.QueryUrl, request);
				}
				else
				{
					//request.Query = string.Empty;
					//queryResponse = await GetPlayList(queryResponse.NextPageUrl, request);
				}
				yield return queryResponse;
			} while (queryResponse.ErrorMessage != string.Empty || queryResponse.NextPageUrl != string.Empty);
		}

	}
}

