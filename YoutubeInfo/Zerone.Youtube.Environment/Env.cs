﻿using System.Reflection.Metadata;

namespace Zerone.Youtube.Environment
{
	/// <summary>
	/// 환경 설정을 관리 합니다.
	/// </summary>
	public class Env
	{
		#region Use Singleton Pattern
		private volatile static Env? _newInstance;

		internal Env() { }

		/// <summary>
		/// Env 클래스의 Single Ton Pattern을 위한 생성자 입니다.
		/// </summary>
		/// <param name="ytInfo"></param>
		private Env(YoutubeApi ytInfo)
		{
			YTInfo = ytInfo;
		}

		/// <summary>
		/// Env 클래스의 Single Ton Pattern을 위한 새 인스턴스를 초기화 합니다.
		/// </summary>
		/// <returns>유일한 Env instance</returns>
		/// <param name="ytInfo"></param>
		[System.Runtime.CompilerServices.MethodImpl(System.Runtime.CompilerServices.MethodImplOptions.Synchronized)]
		public static Env Initialize(YoutubeApi ytInfo)
		{
			if (_newInstance == null)
			{
				_newInstance = new Env(ytInfo);
				IsInitialized = true;
			}

			return _newInstance;
		}

		/// <summary>
		/// Initializes the env.
		/// </summary>
		/// <param name="env">The env.</param>
		/// <returns>An Env.</returns>
		public static Env Initialize(Env env)
		{
			if (_newInstance == null)
			{
				_newInstance = new Env(env.YTInfo);
				IsInitialized = true;
			}

			return _newInstance;
		}

		/// <summary>
		/// Env 클래스의 Single Ton Pattern을 위한 새 인스턴스를 구합니다.
		/// </summary>
		/// <returns>유일한 Env instance</returns>
		public static Env Instance
		{
			get
			{
				lock (typeof(Env))
				{
					if (_newInstance == null)
					{
						throw new InvalidProgramException("You must Initialize first.");
					}
				}

				return _newInstance;
			}
		}

		/// <summary>
		/// 
		/// </summary>
		public static bool IsInitialized { get; private set; }
		#endregion

		/// <summary>
		/// Youtube 관련 환경 설정
		/// </summary>
		public YoutubeApi YTInfo { get; set; }
	}
}