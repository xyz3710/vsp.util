﻿using Newtonsoft.Json;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace Zerone.Youtube.Environment
{
	/// <summary>
	/// 환겅 설정을 저장하고 불러옵니다.
	/// </summary>
	public static class EnvHelper
	{
		/// <summary>
		/// Loads the string core.
		/// </summary>
		/// <param name="filePath">The file path.</param>
		/// <param name="encoding">The encoding.</param>
		/// <returns>A string.</returns>
		public static string LoadStringCore(string filePath, Encoding? encoding = null)
		{
			if (File.Exists(filePath) == false)
			{
				return string.Empty;
			}

			using (var fs = File.Open(filePath, FileMode.Open, FileAccess.Read, FileShare.ReadWrite))
			{
				try
				{
					using (var streamReader = new StreamReader(fs, encoding ?? Encoding.UTF8))
					{
						try
						{
							return streamReader.ReadToEnd();
						}
						finally
						{
							streamReader?.Close();
						}
					}
				}
				finally
				{
					fs?.Close();
				}
			}
		}

		/// <summary>
		/// Saves the string core.
		/// </summary>
		/// <param name="filePath">The file path.</param>
		/// <param name="contents">The contents.</param>
		/// <param name="appendMode">If true, append mode.</param>
		/// <param name="encoding">The encoding.</param>
		public static bool SaveStringCore(string filePath, string contents, bool appendMode = true, Encoding? encoding = null)
		{
			var dir = Path.GetDirectoryName(filePath);

			if (dir != null && Directory.Exists(dir) == false)
			{
				Directory.CreateDirectory(dir);
			}

			var fileMode = appendMode == true || File.Exists(filePath) == false ? FileMode.Create : FileMode.Truncate;

			using (var fs = File.Open(filePath, fileMode, FileAccess.Write, FileShare.ReadWrite))
			{
				try
				{
					using (var streamWriter = new StreamWriter(fs, encoding ?? Encoding.UTF8))
					{
						try
						{
							streamWriter.Write(contents);
							streamWriter.Flush();
							streamWriter.Close();

							return true;
						}
						finally
						{
							streamWriter?.Close();
						}
					}
				}
				finally
				{
					fs?.Close();
				}
			}
		}

		/// <summary>
		/// Loads the class with json file.
		/// </summary>
		/// <param name="filePath">The file path.</param>
		/// <returns>A T.</returns>
		public static T? Load<T>(string filePath)
			where T : class
		{
			var settingString = LoadStringCore(filePath);

			if (settingString == null)
			{
				return default;
			}

			try
			{
				return JsonConvert.DeserializeObject<T>(settingString.Replace("\"null\"", "null"));
			}
			catch { }

			return default;
		}

		/// <summary>
		/// Env를 Serialize 시켜 저장 합니다.
		/// </summary>
		/// <param name="env"></param>
		/// <param name="filePath"></param>
		public static bool Save<T>(T env, string filePath)
		{
			var json = JsonConvert.SerializeObject(env, Newtonsoft.Json.Formatting.Indented).Replace("null", "\"null\"");

			return SaveStringCore(filePath, json);
		}
	}
}
