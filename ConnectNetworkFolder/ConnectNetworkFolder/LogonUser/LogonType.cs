﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConnectNetworkFolder
{
	/// <summary>
	/// LogonUser API에서 사용하는 로그온 타입
	/// <see>ms-help://MS.VSCC.v80/MS.MSDN.v80/MS.WIN32COM.v10.en/secauthn/security/logonuser.htm</see>
	/// </summary>
	public enum LogonType
	{
		/// <summary>
		/// This logon type is intended for users who will be interactively using the computer, 
		/// such as a user being logged on by a terminal server, remote shell, or similar process. <br/>
		/// This logon type has the additional expense of caching logon information for disconnected operations; <br/>
		/// therefore, it is inappropriate for some client/server applications, such as a mail server.
		/// 이 유형은 컴퓨터를 이용해서 상호작용하는 - 예를 들면 터미널 서버, 원격 셸, 혹은 유사한 프로세스 - <br/>
		/// 사용자를 위해서 만들어진 것이다. 이 유형은 접속이 끊어졌을 때의 오퍼레이션을 위해서 로그온 정보를 캐싱하기 때문에 부가적인 비용이 소요된다. <br/>
		/// 그렇기 때문에, 메일 서버와 같은 클라이언트/서버 애플리케이션에는 부적당하다.
		/// </summary>
		LOGON32_LOGON_INTERACTIVE=2,
		/// <summary>
		/// This logon type is intended for high performance servers to authenticate plaintext passwords. <br/>
		/// The LogonUser function does not cache credentials for this logon type.
		/// 이 유형은 plaintext 패스워드로 인증하는 고성능 서버를 위해서 만들어졌다. LogonUser 함수는 이 경우에 Credential을 캐시하지 않는다.
		/// </summary>
		LOGON32_LOGON_NETWORK=3,
		/// <summary>
		/// This logon type is intended for batch servers, where processes may be executing on behalf of a user without their direct intervention. <br/>
		/// This type is also for higher performance servers that process many plaintext authentication attempts at a time, such as mail or Web servers. <br/>
		/// The LogonUser function does not cache credentials for this logon type.
		/// 이 유형은 사용자를 대신해서 프로세스를 처리하는 배치 서버를 위해서 만들어졌다. <br/>
		/// 또 이 유형은 많은 plaintext 인증시도를 하는 고성능 서버가 사용하기 위해서 만들어 진 것으로, 메일 서버나 웹 서버가 바로 그 예이다. <br/>
		/// LogonUser 함수는 이 경우에 사용자의 Credential을 캐싱하지 않는다.
		/// </summary>
		LOGON32_LOGON_BATCH=4,
		/// <summary>
		/// Indicates a service-type logon. The account provided must have the service privilege enabled.
		/// 서비스 타입 로그온을 가리킨다. 계정은 반드시 서비스 권한이 활성화되어 있어야 한다.
		/// </summary>
		LOGON32_LOGON_SERVICE=5,
		/// <summary>
		/// This logon type is for GINA DLLs that log on users who will be interactively using the computer. <br/>
		/// This logon type can generate a unique audit record that shows when the workstation was unlocked.
		/// 이 유형은 GINA(Graphical Identification and Authentication) DLL - Window인증창을 생각하면 된다 - 이 사용한다. <br/>
		/// 이 유형의 로그인은 워크스테이션이 잠금해제되었다는 유니크한 감사 레코드를 생성한다.
		/// </summary>
		LOGON32_LOGON_UNLOCK=7,
		/// <summary>
		/// This logon type preserves the name and password in the authentication package, <br/>
		/// which allows the server to make connections to other network servers while impersonating the client. <br/>
		/// A server can accept plaintext credentials from a client, call LogonUser, <br/>
		/// verify that the user can access the system across the network,  and still communicate with other servers.
		/// <remarks>Windows NT:  This value is not supported.</remarks>
		/// 이 유형은 Impersonating하는 동안 다른 서버로 또 연결할 수 있도록, 인증 패키지에 이름과 패스워드를 저장한다. <br/>
		/// 서버는 클라이언트로부터 plaintext credential을 받을 수 있고, <br/>
		/// LogonUser 함수를 호출해서, 네트워크에 있는 다른 서버로 접속할 수 있는지 확인하고, 다른 서버와 계속 통신할 수 있다. (NT에서는 지원하지 않는다)
		/// </summary>
		LOGON32_LOGON_NETWORK_CLEARTEXT=8,
		/// <summary>
		/// This logon type allows the caller to clone its current token and specify new credentials for outbound connections. <br/>
		/// The new logon session has the same local identifier but uses different credentials for other network connections.<br/>
		/// This logon type is supported only by the LOGON32_PROVIDER_WINNT50 logon provider.
		/// <remarks>Windows NT:  This value is not supported.</remarks>		
		/// 이 유형은 호출자가 현재의 토큰을 복제해서 외부로 나가는 연결에 대해서 새로운 credential을 지정하게 해준다. <br/>
		/// 새로운 로그온 세션은 같은 로컬 식별자를 가지지만, 외부 연결에서는 다른 Credential을 가지게 된다. <br/>
		/// 이 유형은 LOGON32_PROVIDER_WINNT50 프로바이더를 지정했을 때만 지원된다. (NT에서는 지원하지 않는다)
		/// </summary>
		LOGON32_LOGON_NEW_CREDENTIALS=9
	}
}
