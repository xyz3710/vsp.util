﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConnectNetworkFolder
{
	/// <summary>
	/// LogonUser API에서 사용하는 로그온 프로바이더
	/// </summary>
	public enum LogonProvider
	{
		/// <summary>
		/// Use the standard logon provider for the system. The default security provider is negotiate, <br/>
		/// unless you pass NULL for the domain name and the user name is not in UPN format. In this case, the default provider is NTLM.
		/// <remarks>Windows 2000/NT:   The default security provider is NTLM.</remarks>
		/// </summary>
		LOGON32_PROVIDER_DEFAULT=0,
		/// <summary>
		/// Use the Windows NT 3.5 logon provider.
		/// </summary>
		LOGON32_PROVIDER_WINNT35=1,
		/// <summary>
		/// Use the NTLM logon provider.
		/// </summary>
		LOGON32_PROVIDER_WINNT40=2,
		/// <summary>
		/// Use the negotiate logon provider.
		/// <remarks>Windows NT:  This value is not supported.</remarks>
		/// </summary>
		LOGON32_PROVIDER_WINNT50=3
	}
}
