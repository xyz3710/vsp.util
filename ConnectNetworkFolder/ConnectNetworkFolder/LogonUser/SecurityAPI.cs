﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel;
using System.Runtime.InteropServices;

namespace ConnectNetworkFolder
{
	/// <summary>
	/// WIN32 Security 관련 API 들에 대한 모음
	/// </summary>
	public class SecurityAPI
	{
		const int ERROR_INSUFFICIENT_BUFFER = 122;

		[DllImport("advapi32.dll", EntryPoint="LogonUser", SetLastError=true)]
		private extern static bool LogonUserPI(string lpszUsername, string lpszDomain, string lpszPassword, int dwLogonType, int dwLogonProvider, out int phToken);

		/// <summary>
		/// 주어진 사용자 ID로 로그온하여 액세스 토큰을 반환한다.
		/// </summary>
		/// <param name="userName">사용자 ID</param>
		/// <param name="password">암호</param>
		/// <param name="domainName">도메인 이름</param>
		/// <param name="logonType">로그온 종류</param>
		/// <param name="logonProvider">로그온 프로바이더</param>
		/// <returns></returns>
		public static IntPtr LogonUser(string userName, string password, string domainName, LogonType logonType, LogonProvider logonProvider)
		{
			try
			{
				int token = 0;
				bool logonSuccess = LogonUserPI(userName, domainName, password, (int)logonType, (int)logonProvider, out token);

				if (logonSuccess == true)
					return new IntPtr(token);

				int retval = Marshal.GetLastWin32Error();

				throw new Win32Exception(retval);
			}
			catch (Win32Exception ex)
			{
				Console.WriteLine(ex.Message);
			}

			return IntPtr.Zero;
		}
	}
}
