﻿/**********************************************************************************************************************/
/*	Domain		:	ConnectNetworkFolder.Program
/*	Creator		:	KIMKIWON\xyz37
/*	Create		:	2007년 11월 28일 수요일 오후 3:11
/*	Purpose		:	네트웍 폴더 파일 열기
/*	Modifier	:	
/*	Update		:	
/*	Changes		:	
/*	Comment		:	참고 ttp://www.simpleisbest.net/archive/2005/12/07/316.aspx
/**********************************************************************************************************************/

using System;
using System.Collections.Generic;
using System.Text;
using System.Security.Principal;
using System.IO;

namespace ConnectNetworkFolder
{
	class Program
	{
		static void Main(string[] args)
		{
			
			// 현재 쓰레드 계정 정보 표시
			WindowsIdentity identity = WindowsIdentity.GetCurrent();
			Console.WriteLine("Before Impersonation :: Current Thread Account = {0}", identity.Name);

			string user = "pidemo";
			string password = "rwpi0808";
			string domain = ".";
			string path = @"\\RWPIMS\Graphic";

			user = "sharedUser";
			password = "sharedUser";
			domain = ".";		// or "TFS-IDASIT"
			path = @"\\TFS-IDASIT\Framework$";
			
			// Impersonation 수행
			IntPtr token = SecurityAPI.LogonUser(user, password, domain, LogonType.LOGON32_LOGON_NETWORK_CLEARTEXT, LogonProvider.LOGON32_PROVIDER_DEFAULT);
			WindowsImpersonationContext ctx = WindowsIdentity.Impersonate(token);
			// 혹은 다음과 같이 WindowsIdentity 객체를 만들어서 Impersonate 메쏘드를 호출해도 된다.
			// identity = new WindowsIdentity(token);
			// WindowsImpersonationContext ctx = identity.Impersonate();

			// Impersonation 수행 후의 쓰레드 계정 표시
			identity = WindowsIdentity.GetCurrent();
			Console.WriteLine("After Impersonation :: Current Thread Account = {0}", identity.Name);

			// 여기에서 파일 액세스, 네트워크 액세스나 기타 자원을 액세스 한다.
			// 이들 자원에 대한 액세스는 impersonate 된 계정을 사용하여 액세스하게 될 것이다.
			string[] files = Directory.GetFiles(path);

			foreach (string pathFile in files)
				Console.WriteLine(pathFile);

			// 수행한 impersonation을 revoke 한다.
			ctx.Undo();

			// revoke 후에 쓰레드 계정 표시
			identity = WindowsIdentity.GetCurrent();
			Console.WriteLine("After Revoke :: Current Thread Account = {0}", identity.Name);
		}
	}
}
