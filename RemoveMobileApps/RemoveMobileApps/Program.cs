﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Text.RegularExpressions;
using System.Diagnostics;

namespace RemoveMobileApps
{
	[DebuggerDisplay("Key:{Key}, Length:{Length}, Path:{Path}, FileName:{FileName}", Name = "RemoveTarget")]
	class RemoveTarget
	{
		public string Path
		{
			get;
			set;
		}
		public string FileName
		{
			get;
			set;
		}
		public string Key
		{
			get;
			set;
		}
		public int Length
		{
			get;
			set;
		}
	}

	class Dummy
	{
		public string Key
		{
			get;
			set;
		}

		public int Length
		{
			get;
			set;
		}
	}

	class Program
	{
		static void Main(string[] args)
		{
			string path = @"J:\_iTunesMedia\Mobile Applications";

			if (args.Length == 1)
				path = args[0];

			if (Directory.Exists(path) == false)
			{
				Console.WriteLine("Argument directory not exist.");

				return;
			}

			Regex regex = new Regex(@"([\d\w\+\- ]*)( [\d\. ]*)\.ipa", RegexOptions.Compiled);
			var files = Directory.GetFiles(path, "*.ipa");
			var target = files.Select(x => new RemoveTarget
				{
					Path = x,
					FileName = Path.GetFileNameWithoutExtension(x),
					Key = regex.Replace(Path.GetFileName(x), "$1"),
					Length = regex.Replace(Path.GetFileName(x), "$2").Length,
				})
				.ToList();
			var dic = target
				.GroupBy(x => x.Key)
				.Select(x => new Dummy
				{
					Key = x.Key,
					Length = x.Max(c => c.Length),
				})
				.ToDictionary(key => key.Key, sel => sel.Length);
			var dupDic = target		// 길이가 같은데 버전이 높은 파일 이름
				.GroupBy(x => x.Key)
				.Select(x => new
				{
					Key = x.Key,
					Path = x.Max(c => c.Path),
					Count = x.Count(),
				})
				.Where(x => x.Count > 1)
				.ToDictionary(key => key.Key, sel => sel);

			var deleted = target.Where(x =>
			{
				if (dic.ContainsKey(x.Key) == true)
				{
					if (x.Length == dic[x.Key])
						return false;
					else
						return true;
				}

				return false;
			})
			.ToList();

			deleted.AddRange(target.Where(x =>
				{
					if (dupDic.ContainsKey(x.Key) == true)
					{
						if (x.Path == dupDic[x.Key].Path)
							return false;
						else
							return true;
					}

					return false;
				})
			);

			foreach (var item in deleted)
			{
				try
				{
					File.Delete(item.Path);
					Console.WriteLine("Deleted [O] : {0}", item.FileName);
				}
				catch
				{
					Console.WriteLine("Deleted [X] : {0}", item.FileName);

					continue;
				}
			}
		}
	}
}
