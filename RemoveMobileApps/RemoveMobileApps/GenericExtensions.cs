﻿/**********************************************************************************************************************/
/*	Domain		:	System.Collections.Generic.GenericExtensions
/*	Creator		:	KIMKIWON\xyz37(김기원)
/*	Create		:	2011년 4월 26일 화요일 오후 11:22
/*	Purpose		:	Generic collection의 확장 기능을 제공합니다.
/*--------------------------------------------------------------------------------------------------------------------*/
/*	Modifier	:	Kim Ki Won
/*	Update		:	2011년 10월 13일 목요일 오후 10:54
/*	Changes		:	Minus, GetInsertUpdate 확장 기능 추가
/*--------------------------------------------------------------------------------------------------------------------*/
/*	Modifier	:	
/*	Update		:	
/*	Changes		:	
/*--------------------------------------------------------------------------------------------------------------------*/
/*	Comment		:	
/*--------------------------------------------------------------------------------------------------------------------*/
/*	Reviewer	:	Kim Ki Won
/*	Rev. Date	:	2011년 8월 18일 목요일 오후 10:06
/**********************************************************************************************************************/

using System;
using System.Linq;
using System.Text;
using System.Reflection;
using System.Data;
using System.ComponentModel;
using System.Linq.Expressions;

namespace System.Collections.Generic
{
	/// <summary>
	/// Generic collection의 확장 기능을 제공합니다.
	/// </summary>
	public static class GenericExtensions
	{
		#region Minus
		/// <summary>
		/// outer 컬렉션에 없는 inner 컬렉션의 항목만을 구합니다.
		/// </summary>
		/// <typeparam name="T">null이 가능한 구조체 타입입니다.</typeparam>
		/// <param name="inner">기준이 되는 컬렉션입니다.</param>
		/// <param name="outer">비교하려는 컬렉션입니다.</param>
		/// <returns>inner 컬렉션과 작거나 같은 값이 나옵니다.</returns>
		public static IEnumerable<T> Minus<T>(
			this IEnumerable<T> inner,
			IEnumerable<T> outer)
			where T : struct
		{
			return from i in inner
				   from o in
					   (from o in outer
						where o.Equals(i)
						select (T?)o
					   ).DefaultIfEmpty()
				   where o == null
				   select i;
		}

		/// <summary>
		/// outer 컬렉션에 없는 inner 컬렉션의 항목만을 구합니다.
		/// </summary>
		/// <typeparam name="T">클래스 타입입니다.</typeparam>
		/// <typeparam name="TKey">키가되는 타입입니다.</typeparam>
		/// <param name="inner">기준이 되는 컬렉션입니다.</param>
		/// <param name="outer">비교하려는 컬렉션입니다.</param>
		/// <param name="compareKeyExpression">T 타입에서 비교하려는 key의 표현식입니다.</param>
		/// <returns>inner 컬렉션과 작거나 같은 값이 나옵니다.</returns>
		public static IEnumerable<T> Minus<T, TKey>(
			this IEnumerable<T> inner,
			IEnumerable<T> outer,
			Expression<Func<T, TKey>> compareKeyExpression)
			where T : class
		{
			var compiledCondition = compareKeyExpression.Compile();
			return from i in inner
				   from o in
					   (from o in outer
						where compiledCondition(o).Equals(compiledCondition(i))
						select o
					   ).DefaultIfEmpty()
				   where o == null
				   select i;
		}

		/// <summary>
		/// outer 컬렉션에 없는 inner 컬렉션의 항목만을 구합니다.
		/// </summary>
		/// <param name="inner">기준이 되는 문자열 컬렉션입니다.</param>
		/// <param name="outer">비교하려는 문자열 컬렉션입니다.</param>
		/// <returns>inner 컬렉션과 작거나 같은 값이 나옵니다.</returns>
		public static IEnumerable<string> Minus(
			this IEnumerable<string> inner,
			IEnumerable<string> outer)
		{
			return inner.Minus(outer, x => x);
		}
		#endregion
	}
}
