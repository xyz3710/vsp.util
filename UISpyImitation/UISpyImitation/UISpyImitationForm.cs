﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Runtime.InteropServices;

namespace UISpyImitation
{
	public partial class UISpyImitationForm : Form
	{
		#region Invoke method
		[DllImport("user32.dll", EntryPoint="FindWindow", SetLastError=true)]
		private static extern IntPtr FindWindow(string lpClassName, string lpWindowName);

		[DllImport("user32.dll", EntryPoint="FindWindowByCaption", SetLastError=true)]
		private static extern IntPtr FindWindowByCaption(IntPtr ZeroOnly, string lpWindowName);

		[DllImport("user32.dll", CharSet=CharSet.Auto, SetLastError=false)]
		private static extern int SendMessage(IntPtr hWnd, UInt32 Msg, Int32 wParam, Int32 lParam);

		[DllImport("user32.dll", CharSet=CharSet.Auto, SetLastError=false)]
		private static extern int SendMessage(IntPtr hWnd, UInt32 Msg, Int32 wParam, object lParam);

		[DllImport("user32.dll", CharSet=CharSet.Auto, SetLastError=false)]
		private static extern IntPtr SetActiveWindow(IntPtr hWnd);

		[DllImport("user32.dll", SetLastError=true)]
		private static extern IntPtr FindWindowEx(IntPtr hwndParent, IntPtr hwndChildAfter, string lpszClass, string lpszWindow);

		[DllImport("user32.dll", SetLastError=true)]
		private static extern IntPtr FindWindowEx(IntPtr parentHandle, IntPtr childAfter, string className, IntPtr windowTitle);

		[DllImport("user32.dll", EntryPoint="PostMessage", SetLastError=true)]
		private static extern uint PostMessage(IntPtr hwnd, uint wMsg, uint wParam, uint lParam);

		[DllImport("user32.dll", EntryPoint="WindowFromPoint", SetLastError=true)]
		private static extern IntPtr WindowFromPoint(int xPoint, int yPoint);

		[DllImport("user32.dll", CharSet=CharSet.Auto)]
		private static extern int GetClassName(IntPtr hWnd, StringBuilder lpClassName, int nMaxCount);

		[DllImport("user32.dll", CharSet=CharSet.Auto, SetLastError=true)]
		private static extern int GetWindowText(IntPtr hWnd, StringBuilder lpString, int nMaxCount);

		[DllImport("user32.dll", SetLastError=true, CharSet=CharSet.Auto)]
		static extern int GetWindowTextLength(IntPtr hWnd);
		#endregion

		#region Constants
		private const uint WM_GETTEXT = 13;
		private const uint WM_GETTEXTLENGTH = 14;
		#endregion

		public UISpyImitationForm()
		{
			InitializeComponent();
		}

		private void UISpyImitationForm_Load(object sender, EventArgs e)
		{
			Timer timer = new Timer()
			{
				Interval = 50,
			};

			timer.Tick += new EventHandler(timer_Tick);
			timer.Start();
		}

		void timer_Tick(object sender, EventArgs e)
		{
			Point point = Control.MousePosition;
			IntPtr handle = WindowFromPoint(point.X, point.Y);
			StringBuilder className = new StringBuilder(100);
			int res = GetClassName(handle, className, className.Capacity);

			Text = string.Format("{0}, x: {1}, y : {2}", Application.ProductName, point.X, point.Y);
			txtText.Text = GetText(handle);
			txtClassName.Text = className.ToString();
		}

		public string GetText(IntPtr hWnd)
		{
			// Allocate correct string length first
			int length = GetWindowTextLength(hWnd);
			StringBuilder sb = new StringBuilder(length + 1);

			GetWindowText(hWnd, sb, sb.Capacity);

			return sb.ToString();
		}
	}
}
