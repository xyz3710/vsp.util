﻿namespace FolderSynchronizer
{
	partial class FolderSynchronizerForm
	{
		/// <summary>
		/// 필수 디자이너 변수입니다.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// 사용 중인 모든 리소스를 정리합니다.
		/// </summary>
		/// <param name="disposing">관리되는 리소스를 삭제해야 하면 true이고, 그렇지 않으면 false입니다.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form 디자이너에서 생성한 코드

		/// <summary>
		/// 디자이너 지원에 필요한 메서드입니다.
		/// 이 메서드의 내용을 코드 편집기로 수정하지 마십시오.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
			Infragistics.Win.Appearance appearance120 = new Infragistics.Win.Appearance();
			Infragistics.Win.Appearance appearance11 = new Infragistics.Win.Appearance();
			Infragistics.Win.Appearance appearance12 = new Infragistics.Win.Appearance();
			Infragistics.Win.Appearance appearance13 = new Infragistics.Win.Appearance();
			Infragistics.Win.Appearance appearance142 = new Infragistics.Win.Appearance();
			Infragistics.Win.Appearance appearance143 = new Infragistics.Win.Appearance();
			Infragistics.Win.Appearance appearance144 = new Infragistics.Win.Appearance();
			Infragistics.Win.Appearance appearance47 = new Infragistics.Win.Appearance();
			Infragistics.Win.Appearance appearance48 = new Infragistics.Win.Appearance();
			Infragistics.Win.Appearance appearance49 = new Infragistics.Win.Appearance();
			Infragistics.Win.Appearance appearance50 = new Infragistics.Win.Appearance();
			Infragistics.Win.Appearance appearance145 = new Infragistics.Win.Appearance();
			Infragistics.Win.Appearance appearance137 = new Infragistics.Win.Appearance();
			Infragistics.Win.Appearance appearance53 = new Infragistics.Win.Appearance();
			Infragistics.Win.Appearance appearance54 = new Infragistics.Win.Appearance();
			Infragistics.Win.Appearance appearance55 = new Infragistics.Win.Appearance();
			Infragistics.Win.Appearance appearance56 = new Infragistics.Win.Appearance();
			Infragistics.Win.Appearance appearance57 = new Infragistics.Win.Appearance();
			Infragistics.Win.Appearance appearance15 = new Infragistics.Win.Appearance();
			Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
			Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
			Infragistics.Win.Appearance appearance119 = new Infragistics.Win.Appearance();
			Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
			Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
			Infragistics.Win.Appearance appearance10 = new Infragistics.Win.Appearance();
			Infragistics.Win.Appearance appearance138 = new Infragistics.Win.Appearance();
			Infragistics.Win.Appearance appearance139 = new Infragistics.Win.Appearance();
			Infragistics.Win.Appearance appearance140 = new Infragistics.Win.Appearance();
			Infragistics.Win.Appearance appearance31 = new Infragistics.Win.Appearance();
			Infragistics.Win.Appearance appearance32 = new Infragistics.Win.Appearance();
			Infragistics.Win.Appearance appearance33 = new Infragistics.Win.Appearance();
			Infragistics.Win.Appearance appearance34 = new Infragistics.Win.Appearance();
			Infragistics.Win.Appearance appearance141 = new Infragistics.Win.Appearance();
			Infragistics.Win.Appearance appearance130 = new Infragistics.Win.Appearance();
			Infragistics.Win.Appearance appearance37 = new Infragistics.Win.Appearance();
			Infragistics.Win.Appearance appearance38 = new Infragistics.Win.Appearance();
			Infragistics.Win.Appearance appearance39 = new Infragistics.Win.Appearance();
			Infragistics.Win.Appearance appearance40 = new Infragistics.Win.Appearance();
			Infragistics.Win.Appearance appearance41 = new Infragistics.Win.Appearance();
			Infragistics.Win.Appearance appearance14 = new Infragistics.Win.Appearance();
			Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
			Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
			Infragistics.Win.Appearance appearance118 = new Infragistics.Win.Appearance();
			Infragistics.Win.Appearance appearance146 = new Infragistics.Win.Appearance();
			Infragistics.Win.Appearance appearance147 = new Infragistics.Win.Appearance();
			Infragistics.Win.Appearance appearance148 = new Infragistics.Win.Appearance();
			Infragistics.Win.Appearance appearance17 = new Infragistics.Win.Appearance();
			Infragistics.Win.Appearance appearance18 = new Infragistics.Win.Appearance();
			Infragistics.Win.Appearance appearance19 = new Infragistics.Win.Appearance();
			Infragistics.Win.Appearance appearance20 = new Infragistics.Win.Appearance();
			Infragistics.Win.Appearance appearance149 = new Infragistics.Win.Appearance();
			Infragistics.Win.Appearance appearance125 = new Infragistics.Win.Appearance();
			Infragistics.Win.Appearance appearance23 = new Infragistics.Win.Appearance();
			Infragistics.Win.Appearance appearance24 = new Infragistics.Win.Appearance();
			Infragistics.Win.Appearance appearance25 = new Infragistics.Win.Appearance();
			Infragistics.Win.Appearance appearance26 = new Infragistics.Win.Appearance();
			Infragistics.Win.Appearance appearance27 = new Infragistics.Win.Appearance();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FolderSynchronizerForm));
			this.tlpMain = new System.Windows.Forms.TableLayoutPanel();
			this.xgrbDestination = new Infragistics.Win.Misc.UltraGroupBox();
			this.tlpDestination = new System.Windows.Forms.TableLayoutPanel();
			this.tlpToSrc = new System.Windows.Forms.TableLayoutPanel();
			this.xbtnCompareWithSrc = new Infragistics.Win.Misc.UltraButton();
			this.xbtnCopyToSrc = new Infragistics.Win.Misc.UltraButton();
			this.xbtnMoveToSrc = new Infragistics.Win.Misc.UltraButton();
			this.xgridDest = new Infragistics.Win.UltraWinGrid.UltraGrid();
			this.tlpDestFolder = new System.Windows.Forms.TableLayoutPanel();
			this.xbtnDest = new Infragistics.Win.Misc.UltraButton();
			this.xtxtDestFolder = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
			this.xgrbSource = new Infragistics.Win.Misc.UltraGroupBox();
			this.tlpSource = new System.Windows.Forms.TableLayoutPanel();
			this.tlpToDest = new System.Windows.Forms.TableLayoutPanel();
			this.xbtnCompareWithDest = new Infragistics.Win.Misc.UltraButton();
			this.xbtnCopyToDest = new Infragistics.Win.Misc.UltraButton();
			this.xbtnMoveToDesc = new Infragistics.Win.Misc.UltraButton();
			this.xgridSrc = new Infragistics.Win.UltraWinGrid.UltraGrid();
			this.tlpSrcFolder = new System.Windows.Forms.TableLayoutPanel();
			this.xbtnSrc = new Infragistics.Win.Misc.UltraButton();
			this.xtxtSrcFolder = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
			this.fbDialog = new System.Windows.Forms.FolderBrowserDialog();
			this.statusText = new System.Windows.Forms.ToolStripStatusLabel();
			this.statusProgress = new System.Windows.Forms.ToolStripProgressBar();
			this.statusStrip = new System.Windows.Forms.StatusStrip();
			this.splitContainer = new System.Windows.Forms.SplitContainer();
			this.xgrbDiffer = new Infragistics.Win.Misc.UltraGroupBox();
			this.xgridDiffer = new Infragistics.Win.UltraWinGrid.UltraGrid();
			this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
			this.tlpMain.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.xgrbDestination)).BeginInit();
			this.xgrbDestination.SuspendLayout();
			this.tlpDestination.SuspendLayout();
			this.tlpToSrc.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.xgridDest)).BeginInit();
			this.tlpDestFolder.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.xtxtDestFolder)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.xgrbSource)).BeginInit();
			this.xgrbSource.SuspendLayout();
			this.tlpSource.SuspendLayout();
			this.tlpToDest.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.xgridSrc)).BeginInit();
			this.tlpSrcFolder.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.xtxtSrcFolder)).BeginInit();
			this.statusStrip.SuspendLayout();
			this.splitContainer.Panel1.SuspendLayout();
			this.splitContainer.Panel2.SuspendLayout();
			this.splitContainer.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.xgrbDiffer)).BeginInit();
			this.xgrbDiffer.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.xgridDiffer)).BeginInit();
			this.SuspendLayout();
			// 
			// tlpMain
			// 
			this.tlpMain.BackColor = System.Drawing.Color.White;
			this.tlpMain.ColumnCount = 2;
			this.tlpMain.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
			this.tlpMain.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
			this.tlpMain.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
			this.tlpMain.Controls.Add(this.xgrbDestination, 1, 0);
			this.tlpMain.Controls.Add(this.xgrbSource, 0, 0);
			this.tlpMain.Dock = System.Windows.Forms.DockStyle.Fill;
			this.tlpMain.Location = new System.Drawing.Point(0, 0);
			this.tlpMain.Name = "tlpMain";
			this.tlpMain.RowCount = 1;
			this.tlpMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
			this.tlpMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 432F));
			this.tlpMain.Size = new System.Drawing.Size(1012, 432);
			this.tlpMain.TabIndex = 0;
			// 
			// xgrbDestination
			// 
			appearance3.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(189)))), ((int)(((byte)(196)))), ((int)(((byte)(211)))));
			appearance3.TextVAlignAsString = "Middle";
			this.xgrbDestination.Appearance = appearance3;
			this.xgrbDestination.BackColorInternal = System.Drawing.Color.White;
			this.xgrbDestination.ContentPadding.Bottom = 2;
			this.xgrbDestination.ContentPadding.Left = 2;
			this.xgrbDestination.ContentPadding.Right = 2;
			this.xgrbDestination.ContentPadding.Top = 2;
			this.xgrbDestination.Controls.Add(this.tlpDestination);
			this.xgrbDestination.Dock = System.Windows.Forms.DockStyle.Fill;
			appearance120.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(204)))), ((int)(((byte)(222)))), ((int)(((byte)(214)))));
			appearance120.FontData.BoldAsString = "True";
			appearance120.ForeColor = System.Drawing.Color.Black;
			appearance120.ForegroundAlpha = Infragistics.Win.Alpha.Opaque;
			appearance120.TextVAlignAsString = "Top";
			this.xgrbDestination.HeaderAppearance = appearance120;
			this.xgrbDestination.HeaderBorderStyle = Infragistics.Win.UIElementBorderStyle.Rounded4;
			this.xgrbDestination.HeaderPosition = Infragistics.Win.Misc.GroupBoxHeaderPosition.TopInsideBorder;
			this.xgrbDestination.Location = new System.Drawing.Point(506, 3);
			this.xgrbDestination.Margin = new System.Windows.Forms.Padding(0, 3, 3, 0);
			this.xgrbDestination.Name = "xgrbDestination";
			this.xgrbDestination.Size = new System.Drawing.Size(503, 429);
			this.xgrbDestination.TabIndex = 1;
			this.xgrbDestination.Tag = "Compared Destination";
			this.xgrbDestination.Text = "Compared Destination";
			// 
			// tlpDestination
			// 
			this.tlpDestination.ColumnCount = 1;
			this.tlpDestination.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
			this.tlpDestination.Controls.Add(this.tlpToSrc, 0, 2);
			this.tlpDestination.Controls.Add(this.xgridDest, 0, 1);
			this.tlpDestination.Controls.Add(this.tlpDestFolder, 0, 0);
			this.tlpDestination.Dock = System.Windows.Forms.DockStyle.Fill;
			this.tlpDestination.Location = new System.Drawing.Point(5, 27);
			this.tlpDestination.Margin = new System.Windows.Forms.Padding(0);
			this.tlpDestination.Name = "tlpDestination";
			this.tlpDestination.RowCount = 3;
			this.tlpDestination.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 32F));
			this.tlpDestination.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
			this.tlpDestination.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 32F));
			this.tlpDestination.Size = new System.Drawing.Size(493, 397);
			this.tlpDestination.TabIndex = 1;
			// 
			// tlpToSrc
			// 
			this.tlpToSrc.BackColor = System.Drawing.Color.White;
			this.tlpToSrc.ColumnCount = 3;
			this.tlpToSrc.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
			this.tlpToSrc.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33334F));
			this.tlpToSrc.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33334F));
			this.tlpToSrc.Controls.Add(this.xbtnCompareWithSrc, 0, 0);
			this.tlpToSrc.Controls.Add(this.xbtnCopyToSrc, 1, 0);
			this.tlpToSrc.Controls.Add(this.xbtnMoveToSrc, 2, 0);
			this.tlpToSrc.Dock = System.Windows.Forms.DockStyle.Fill;
			this.tlpToSrc.Location = new System.Drawing.Point(0, 365);
			this.tlpToSrc.Margin = new System.Windows.Forms.Padding(0);
			this.tlpToSrc.Name = "tlpToSrc";
			this.tlpToSrc.RowCount = 1;
			this.tlpToSrc.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
			this.tlpToSrc.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 32F));
			this.tlpToSrc.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 32F));
			this.tlpToSrc.Size = new System.Drawing.Size(493, 32);
			this.tlpToSrc.TabIndex = 0;
			// 
			// xbtnCompareWithSrc
			// 
			this.xbtnCompareWithSrc.BackColorInternal = System.Drawing.Color.White;
			this.xbtnCompareWithSrc.ButtonStyle = Infragistics.Win.UIElementButtonStyle.WindowsXPCommandButton;
			this.xbtnCompareWithSrc.Dock = System.Windows.Forms.DockStyle.Fill;
			appearance11.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(204)))), ((int)(((byte)(222)))), ((int)(((byte)(214)))));
			this.xbtnCompareWithSrc.HotTrackAppearance = appearance11;
			this.xbtnCompareWithSrc.Location = new System.Drawing.Point(3, 3);
			this.xbtnCompareWithSrc.Margin = new System.Windows.Forms.Padding(3, 3, 6, 3);
			this.xbtnCompareWithSrc.Name = "xbtnCompareWithSrc";
			this.xbtnCompareWithSrc.Size = new System.Drawing.Size(155, 26);
			this.xbtnCompareWithSrc.TabIndex = 0;
			this.xbtnCompareWithSrc.Text = "&<- Compare";
			this.xbtnCompareWithSrc.UseOsThemes = Infragistics.Win.DefaultableBoolean.False;
			this.xbtnCompareWithSrc.Click += new System.EventHandler(this.xbtnCompareWithSrc_Click);
			// 
			// xbtnCopyToSrc
			// 
			this.xbtnCopyToSrc.BackColorInternal = System.Drawing.Color.White;
			this.xbtnCopyToSrc.ButtonStyle = Infragistics.Win.UIElementButtonStyle.WindowsXPCommandButton;
			this.xbtnCopyToSrc.Dock = System.Windows.Forms.DockStyle.Fill;
			appearance12.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(204)))), ((int)(((byte)(222)))), ((int)(((byte)(214)))));
			this.xbtnCopyToSrc.HotTrackAppearance = appearance12;
			this.xbtnCopyToSrc.Location = new System.Drawing.Point(170, 3);
			this.xbtnCopyToSrc.Margin = new System.Windows.Forms.Padding(6, 3, 6, 3);
			this.xbtnCopyToSrc.Name = "xbtnCopyToSrc";
			this.xbtnCopyToSrc.Size = new System.Drawing.Size(152, 26);
			this.xbtnCopyToSrc.TabIndex = 1;
			this.xbtnCopyToSrc.Text = "<- Co&py";
			this.xbtnCopyToSrc.UseOsThemes = Infragistics.Win.DefaultableBoolean.False;
			this.xbtnCopyToSrc.Click += new System.EventHandler(this.xbtnCopyToSrc_Click);
			// 
			// xbtnMoveToSrc
			// 
			this.xbtnMoveToSrc.BackColorInternal = System.Drawing.Color.White;
			this.xbtnMoveToSrc.ButtonStyle = Infragistics.Win.UIElementButtonStyle.WindowsXPCommandButton;
			this.xbtnMoveToSrc.Dock = System.Windows.Forms.DockStyle.Fill;
			appearance13.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(204)))), ((int)(((byte)(222)))), ((int)(((byte)(214)))));
			this.xbtnMoveToSrc.HotTrackAppearance = appearance13;
			this.xbtnMoveToSrc.Location = new System.Drawing.Point(334, 3);
			this.xbtnMoveToSrc.Margin = new System.Windows.Forms.Padding(6, 3, 3, 3);
			this.xbtnMoveToSrc.Name = "xbtnMoveToSrc";
			this.xbtnMoveToSrc.Size = new System.Drawing.Size(156, 26);
			this.xbtnMoveToSrc.TabIndex = 2;
			this.xbtnMoveToSrc.Text = "<- Mo&ve";
			this.xbtnMoveToSrc.UseOsThemes = Infragistics.Win.DefaultableBoolean.False;
			this.xbtnMoveToSrc.Click += new System.EventHandler(this.xbtnMoveToSrc_Click);
			// 
			// xgridDest
			// 
			appearance142.BackColor = System.Drawing.Color.White;
			appearance142.BorderColor = System.Drawing.Color.Gray;
			this.xgridDest.DisplayLayout.Appearance = appearance142;
			this.xgridDest.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
			this.xgridDest.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
			this.xgridDest.DisplayLayout.GroupByBox.Hidden = true;
			appearance143.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(168)))), ((int)(((byte)(197)))), ((int)(((byte)(183)))));
			appearance143.BorderColor = System.Drawing.Color.Orange;
			this.xgridDest.DisplayLayout.Override.ActiveRowAppearance = appearance143;
			this.xgridDest.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Solid;
			this.xgridDest.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Solid;
			this.xgridDest.DisplayLayout.Override.BorderStyleSpecialRowSeparator = Infragistics.Win.UIElementBorderStyle.None;
			this.xgridDest.DisplayLayout.Override.BorderStyleSummaryFooter = Infragistics.Win.UIElementBorderStyle.Solid;
			this.xgridDest.DisplayLayout.Override.BorderStyleSummaryValue = Infragistics.Win.UIElementBorderStyle.None;
			this.xgridDest.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
			this.xgridDest.DisplayLayout.Override.ColumnAutoSizeMode = Infragistics.Win.UltraWinGrid.ColumnAutoSizeMode.VisibleRows;
			this.xgridDest.DisplayLayout.Override.ColumnSizingArea = Infragistics.Win.UltraWinGrid.ColumnSizingArea.EntireColumn;
			this.xgridDest.DisplayLayout.Override.FixedCellSeparatorColor = System.Drawing.Color.OrangeRed;
			this.xgridDest.DisplayLayout.Override.FixedHeaderIndicator = Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.None;
			appearance144.TextHAlignAsString = "Center";
			appearance144.TextVAlignAsString = "Middle";
			this.xgridDest.DisplayLayout.Override.HeaderAppearance = appearance144;
			this.xgridDest.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
			this.xgridDest.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
			appearance47.BackColor = System.Drawing.Color.LightBlue;
			this.xgridDest.DisplayLayout.Override.HotTrackCellAppearance = appearance47;
			appearance48.BorderColor = System.Drawing.Color.Orange;
			this.xgridDest.DisplayLayout.Override.HotTrackRowAppearance = appearance48;
			appearance49.BackColor = System.Drawing.Color.LightBlue;
			this.xgridDest.DisplayLayout.Override.HotTrackRowCellAppearance = appearance49;
			appearance50.BackColor = System.Drawing.Color.Silver;
			this.xgridDest.DisplayLayout.Override.HotTrackRowSelectorAppearance = appearance50;
			appearance145.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(235)))), ((int)(((byte)(230)))));
			this.xgridDest.DisplayLayout.Override.RowAlternateAppearance = appearance145;
			appearance137.BackColor = System.Drawing.Color.White;
			this.xgridDest.DisplayLayout.Override.RowAppearance = appearance137;
			appearance53.TextHAlignAsString = "Center";
			appearance53.TextVAlignAsString = "Middle";
			this.xgridDest.DisplayLayout.Override.RowSelectorAppearance = appearance53;
			appearance54.TextHAlignAsString = "Center";
			appearance54.TextVAlignAsString = "Middle";
			this.xgridDest.DisplayLayout.Override.RowSelectorHeaderAppearance = appearance54;
			this.xgridDest.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
			this.xgridDest.DisplayLayout.Override.SpecialRowSeparator = Infragistics.Win.UltraWinGrid.SpecialRowSeparator.SummaryRow;
			appearance55.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(220)))), ((int)(((byte)(225)))), ((int)(((byte)(190)))));
			this.xgridDest.DisplayLayout.Override.SpecialRowSeparatorAppearance = appearance55;
			this.xgridDest.DisplayLayout.Override.SpecialRowSeparatorHeight = 3;
			appearance56.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(238)))), ((int)(((byte)(237)))), ((int)(((byte)(218)))));
			appearance56.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(189)))), ((int)(((byte)(196)))), ((int)(((byte)(211)))));
			this.xgridDest.DisplayLayout.Override.SummaryFooterAppearance = appearance56;
			this.xgridDest.DisplayLayout.Override.SummaryFooterCaptionVisible = Infragistics.Win.DefaultableBoolean.False;
			appearance57.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(238)))), ((int)(((byte)(237)))), ((int)(((byte)(218)))));
			appearance57.TextVAlignAsString = "Middle";
			this.xgridDest.DisplayLayout.Override.SummaryValueAppearance = appearance57;
			this.xgridDest.DisplayLayout.UseFixedHeaders = true;
			this.xgridDest.Dock = System.Windows.Forms.DockStyle.Fill;
			this.xgridDest.Location = new System.Drawing.Point(3, 35);
			this.xgridDest.Name = "xgridDest";
			this.xgridDest.Size = new System.Drawing.Size(487, 327);
			this.xgridDest.TabIndex = 0;
			this.xgridDest.Text = "ultraGrid1";
			this.xgridDest.UpdateMode = Infragistics.Win.UltraWinGrid.UpdateMode.OnUpdate;
			this.xgridDest.UseOsThemes = Infragistics.Win.DefaultableBoolean.False;
			this.xgridDest.CellChange += new Infragistics.Win.UltraWinGrid.CellEventHandler(this.Grid_CellChange);
			// 
			// tlpDestFolder
			// 
			this.tlpDestFolder.ColumnCount = 2;
			this.tlpDestFolder.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
			this.tlpDestFolder.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 128F));
			this.tlpDestFolder.Controls.Add(this.xbtnDest, 1, 0);
			this.tlpDestFolder.Controls.Add(this.xtxtDestFolder, 0, 0);
			this.tlpDestFolder.Dock = System.Windows.Forms.DockStyle.Fill;
			this.tlpDestFolder.Location = new System.Drawing.Point(0, 0);
			this.tlpDestFolder.Margin = new System.Windows.Forms.Padding(0);
			this.tlpDestFolder.Name = "tlpDestFolder";
			this.tlpDestFolder.RowCount = 1;
			this.tlpDestFolder.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
			this.tlpDestFolder.Size = new System.Drawing.Size(493, 32);
			this.tlpDestFolder.TabIndex = 1;
			// 
			// xbtnDest
			// 
			this.xbtnDest.BackColorInternal = System.Drawing.Color.White;
			this.xbtnDest.ButtonStyle = Infragistics.Win.UIElementButtonStyle.WindowsXPCommandButton;
			this.xbtnDest.Dock = System.Windows.Forms.DockStyle.Fill;
			appearance15.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(204)))), ((int)(((byte)(222)))), ((int)(((byte)(214)))));
			this.xbtnDest.HotTrackAppearance = appearance15;
			this.xbtnDest.Location = new System.Drawing.Point(368, 2);
			this.xbtnDest.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
			this.xbtnDest.Name = "xbtnDest";
			this.xbtnDest.Size = new System.Drawing.Size(122, 28);
			this.xbtnDest.TabIndex = 1;
			this.xbtnDest.Text = "Select &Destination";
			this.xbtnDest.UseOsThemes = Infragistics.Win.DefaultableBoolean.False;
			this.xbtnDest.Click += new System.EventHandler(this.xbtnDest_Click);
			// 
			// xtxtDestFolder
			// 
			appearance7.BackColor = System.Drawing.Color.White;
			appearance7.TextHAlignAsString = "Left";
			appearance7.TextVAlignAsString = "Middle";
			this.xtxtDestFolder.Appearance = appearance7;
			this.xtxtDestFolder.AutoSize = false;
			this.xtxtDestFolder.BackColor = System.Drawing.Color.White;
			this.xtxtDestFolder.BorderStyle = Infragistics.Win.UIElementBorderStyle.Rounded1;
			this.xtxtDestFolder.Dock = System.Windows.Forms.DockStyle.Fill;
			this.xtxtDestFolder.Location = new System.Drawing.Point(3, 3);
			this.xtxtDestFolder.Name = "xtxtDestFolder";
			this.xtxtDestFolder.Size = new System.Drawing.Size(359, 26);
			this.xtxtDestFolder.TabIndex = 0;
			this.xtxtDestFolder.UseOsThemes = Infragistics.Win.DefaultableBoolean.False;
			// 
			// xgrbSource
			// 
			appearance2.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(189)))), ((int)(((byte)(196)))), ((int)(((byte)(211)))));
			appearance2.TextVAlignAsString = "Middle";
			this.xgrbSource.Appearance = appearance2;
			this.xgrbSource.BackColorInternal = System.Drawing.Color.White;
			this.xgrbSource.ContentPadding.Bottom = 2;
			this.xgrbSource.ContentPadding.Left = 2;
			this.xgrbSource.ContentPadding.Right = 2;
			this.xgrbSource.ContentPadding.Top = 2;
			this.xgrbSource.Controls.Add(this.tlpSource);
			this.xgrbSource.Dock = System.Windows.Forms.DockStyle.Fill;
			appearance119.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(204)))), ((int)(((byte)(222)))), ((int)(((byte)(214)))));
			appearance119.FontData.BoldAsString = "True";
			appearance119.ForeColor = System.Drawing.Color.Black;
			appearance119.ForegroundAlpha = Infragistics.Win.Alpha.Opaque;
			appearance119.TextVAlignAsString = "Top";
			this.xgrbSource.HeaderAppearance = appearance119;
			this.xgrbSource.HeaderBorderStyle = Infragistics.Win.UIElementBorderStyle.Rounded4;
			this.xgrbSource.HeaderPosition = Infragistics.Win.Misc.GroupBoxHeaderPosition.TopInsideBorder;
			this.xgrbSource.Location = new System.Drawing.Point(3, 3);
			this.xgrbSource.Margin = new System.Windows.Forms.Padding(3, 3, 0, 0);
			this.xgrbSource.Name = "xgrbSource";
			this.xgrbSource.Size = new System.Drawing.Size(503, 429);
			this.xgrbSource.TabIndex = 0;
			this.xgrbSource.Tag = "Compared Source";
			this.xgrbSource.Text = "Compared Source";
			// 
			// tlpSource
			// 
			this.tlpSource.ColumnCount = 1;
			this.tlpSource.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
			this.tlpSource.Controls.Add(this.tlpToDest, 0, 2);
			this.tlpSource.Controls.Add(this.xgridSrc, 0, 1);
			this.tlpSource.Controls.Add(this.tlpSrcFolder, 0, 0);
			this.tlpSource.Dock = System.Windows.Forms.DockStyle.Fill;
			this.tlpSource.Location = new System.Drawing.Point(5, 27);
			this.tlpSource.Margin = new System.Windows.Forms.Padding(0);
			this.tlpSource.Name = "tlpSource";
			this.tlpSource.RowCount = 3;
			this.tlpSource.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 32F));
			this.tlpSource.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
			this.tlpSource.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 32F));
			this.tlpSource.Size = new System.Drawing.Size(493, 397);
			this.tlpSource.TabIndex = 0;
			// 
			// tlpToDest
			// 
			this.tlpToDest.BackColor = System.Drawing.Color.White;
			this.tlpToDest.ColumnCount = 3;
			this.tlpToDest.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
			this.tlpToDest.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33334F));
			this.tlpToDest.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33334F));
			this.tlpToDest.Controls.Add(this.xbtnCompareWithDest, 0, 0);
			this.tlpToDest.Controls.Add(this.xbtnCopyToDest, 1, 0);
			this.tlpToDest.Controls.Add(this.xbtnMoveToDesc, 2, 0);
			this.tlpToDest.Dock = System.Windows.Forms.DockStyle.Fill;
			this.tlpToDest.Location = new System.Drawing.Point(0, 365);
			this.tlpToDest.Margin = new System.Windows.Forms.Padding(0);
			this.tlpToDest.Name = "tlpToDest";
			this.tlpToDest.RowCount = 1;
			this.tlpToDest.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
			this.tlpToDest.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 32F));
			this.tlpToDest.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 32F));
			this.tlpToDest.Size = new System.Drawing.Size(493, 32);
			this.tlpToDest.TabIndex = 0;
			// 
			// xbtnCompareWithDest
			// 
			this.xbtnCompareWithDest.BackColorInternal = System.Drawing.Color.White;
			this.xbtnCompareWithDest.ButtonStyle = Infragistics.Win.UIElementButtonStyle.WindowsXPCommandButton;
			this.xbtnCompareWithDest.Dock = System.Windows.Forms.DockStyle.Fill;
			appearance4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(204)))), ((int)(((byte)(222)))), ((int)(((byte)(214)))));
			this.xbtnCompareWithDest.HotTrackAppearance = appearance4;
			this.xbtnCompareWithDest.Location = new System.Drawing.Point(3, 3);
			this.xbtnCompareWithDest.Margin = new System.Windows.Forms.Padding(3, 3, 6, 3);
			this.xbtnCompareWithDest.Name = "xbtnCompareWithDest";
			this.xbtnCompareWithDest.Size = new System.Drawing.Size(155, 26);
			this.xbtnCompareWithDest.TabIndex = 0;
			this.xbtnCompareWithDest.Text = "Compare -&>";
			this.xbtnCompareWithDest.UseOsThemes = Infragistics.Win.DefaultableBoolean.False;
			this.xbtnCompareWithDest.Click += new System.EventHandler(this.xbtnCompareWithDest_Click);
			// 
			// xbtnCopyToDest
			// 
			this.xbtnCopyToDest.BackColorInternal = System.Drawing.Color.White;
			this.xbtnCopyToDest.ButtonStyle = Infragistics.Win.UIElementButtonStyle.WindowsXPCommandButton;
			this.xbtnCopyToDest.Dock = System.Windows.Forms.DockStyle.Fill;
			appearance5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(204)))), ((int)(((byte)(222)))), ((int)(((byte)(214)))));
			this.xbtnCopyToDest.HotTrackAppearance = appearance5;
			this.xbtnCopyToDest.Location = new System.Drawing.Point(170, 3);
			this.xbtnCopyToDest.Margin = new System.Windows.Forms.Padding(6, 3, 6, 3);
			this.xbtnCopyToDest.Name = "xbtnCopyToDest";
			this.xbtnCopyToDest.Size = new System.Drawing.Size(152, 26);
			this.xbtnCopyToDest.TabIndex = 1;
			this.xbtnCopyToDest.Text = "&Copy ->";
			this.xbtnCopyToDest.UseOsThemes = Infragistics.Win.DefaultableBoolean.False;
			this.xbtnCopyToDest.Click += new System.EventHandler(this.xbtnCopyToDest_Click);
			// 
			// xbtnMoveToDesc
			// 
			this.xbtnMoveToDesc.BackColorInternal = System.Drawing.Color.White;
			this.xbtnMoveToDesc.ButtonStyle = Infragistics.Win.UIElementButtonStyle.WindowsXPCommandButton;
			this.xbtnMoveToDesc.Dock = System.Windows.Forms.DockStyle.Fill;
			appearance10.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(204)))), ((int)(((byte)(222)))), ((int)(((byte)(214)))));
			this.xbtnMoveToDesc.HotTrackAppearance = appearance10;
			this.xbtnMoveToDesc.Location = new System.Drawing.Point(334, 3);
			this.xbtnMoveToDesc.Margin = new System.Windows.Forms.Padding(6, 3, 3, 3);
			this.xbtnMoveToDesc.Name = "xbtnMoveToDesc";
			this.xbtnMoveToDesc.Size = new System.Drawing.Size(156, 26);
			this.xbtnMoveToDesc.TabIndex = 2;
			this.xbtnMoveToDesc.Text = "&Move ->";
			this.xbtnMoveToDesc.UseOsThemes = Infragistics.Win.DefaultableBoolean.False;
			this.xbtnMoveToDesc.Click += new System.EventHandler(this.xbtnMoveToDesc_Click);
			// 
			// xgridSrc
			// 
			appearance138.BackColor = System.Drawing.Color.White;
			appearance138.BorderColor = System.Drawing.Color.Gray;
			this.xgridSrc.DisplayLayout.Appearance = appearance138;
			this.xgridSrc.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
			this.xgridSrc.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
			this.xgridSrc.DisplayLayout.GroupByBox.Hidden = true;
			appearance139.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(168)))), ((int)(((byte)(197)))), ((int)(((byte)(183)))));
			appearance139.BorderColor = System.Drawing.Color.Orange;
			this.xgridSrc.DisplayLayout.Override.ActiveRowAppearance = appearance139;
			this.xgridSrc.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Solid;
			this.xgridSrc.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Solid;
			this.xgridSrc.DisplayLayout.Override.BorderStyleSpecialRowSeparator = Infragistics.Win.UIElementBorderStyle.None;
			this.xgridSrc.DisplayLayout.Override.BorderStyleSummaryFooter = Infragistics.Win.UIElementBorderStyle.Solid;
			this.xgridSrc.DisplayLayout.Override.BorderStyleSummaryValue = Infragistics.Win.UIElementBorderStyle.None;
			this.xgridSrc.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
			this.xgridSrc.DisplayLayout.Override.ColumnAutoSizeMode = Infragistics.Win.UltraWinGrid.ColumnAutoSizeMode.VisibleRows;
			this.xgridSrc.DisplayLayout.Override.ColumnSizingArea = Infragistics.Win.UltraWinGrid.ColumnSizingArea.EntireColumn;
			this.xgridSrc.DisplayLayout.Override.FixedCellSeparatorColor = System.Drawing.Color.OrangeRed;
			this.xgridSrc.DisplayLayout.Override.FixedHeaderIndicator = Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.None;
			appearance140.TextHAlignAsString = "Center";
			appearance140.TextVAlignAsString = "Middle";
			this.xgridSrc.DisplayLayout.Override.HeaderAppearance = appearance140;
			this.xgridSrc.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
			this.xgridSrc.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
			appearance31.BackColor = System.Drawing.Color.LightBlue;
			this.xgridSrc.DisplayLayout.Override.HotTrackCellAppearance = appearance31;
			appearance32.BorderColor = System.Drawing.Color.Orange;
			this.xgridSrc.DisplayLayout.Override.HotTrackRowAppearance = appearance32;
			appearance33.BackColor = System.Drawing.Color.LightBlue;
			this.xgridSrc.DisplayLayout.Override.HotTrackRowCellAppearance = appearance33;
			appearance34.BackColor = System.Drawing.Color.Silver;
			this.xgridSrc.DisplayLayout.Override.HotTrackRowSelectorAppearance = appearance34;
			appearance141.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(235)))), ((int)(((byte)(230)))));
			this.xgridSrc.DisplayLayout.Override.RowAlternateAppearance = appearance141;
			appearance130.BackColor = System.Drawing.Color.White;
			this.xgridSrc.DisplayLayout.Override.RowAppearance = appearance130;
			appearance37.TextHAlignAsString = "Center";
			appearance37.TextVAlignAsString = "Middle";
			this.xgridSrc.DisplayLayout.Override.RowSelectorAppearance = appearance37;
			appearance38.TextHAlignAsString = "Center";
			appearance38.TextVAlignAsString = "Middle";
			this.xgridSrc.DisplayLayout.Override.RowSelectorHeaderAppearance = appearance38;
			this.xgridSrc.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
			this.xgridSrc.DisplayLayout.Override.SpecialRowSeparator = Infragistics.Win.UltraWinGrid.SpecialRowSeparator.SummaryRow;
			appearance39.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(220)))), ((int)(((byte)(225)))), ((int)(((byte)(190)))));
			this.xgridSrc.DisplayLayout.Override.SpecialRowSeparatorAppearance = appearance39;
			this.xgridSrc.DisplayLayout.Override.SpecialRowSeparatorHeight = 3;
			appearance40.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(238)))), ((int)(((byte)(237)))), ((int)(((byte)(218)))));
			appearance40.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(189)))), ((int)(((byte)(196)))), ((int)(((byte)(211)))));
			this.xgridSrc.DisplayLayout.Override.SummaryFooterAppearance = appearance40;
			this.xgridSrc.DisplayLayout.Override.SummaryFooterCaptionVisible = Infragistics.Win.DefaultableBoolean.False;
			appearance41.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(238)))), ((int)(((byte)(237)))), ((int)(((byte)(218)))));
			appearance41.TextVAlignAsString = "Middle";
			this.xgridSrc.DisplayLayout.Override.SummaryValueAppearance = appearance41;
			this.xgridSrc.DisplayLayout.UseFixedHeaders = true;
			this.xgridSrc.Dock = System.Windows.Forms.DockStyle.Fill;
			this.xgridSrc.Location = new System.Drawing.Point(3, 35);
			this.xgridSrc.Name = "xgridSrc";
			this.xgridSrc.Size = new System.Drawing.Size(487, 327);
			this.xgridSrc.TabIndex = 0;
			this.xgridSrc.Text = "ultraGrid1";
			this.xgridSrc.UpdateMode = Infragistics.Win.UltraWinGrid.UpdateMode.OnUpdate;
			this.xgridSrc.UseOsThemes = Infragistics.Win.DefaultableBoolean.False;
			this.xgridSrc.CellChange += new Infragistics.Win.UltraWinGrid.CellEventHandler(this.Grid_CellChange);
			// 
			// tlpSrcFolder
			// 
			this.tlpSrcFolder.ColumnCount = 2;
			this.tlpSrcFolder.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
			this.tlpSrcFolder.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 128F));
			this.tlpSrcFolder.Controls.Add(this.xbtnSrc, 1, 0);
			this.tlpSrcFolder.Controls.Add(this.xtxtSrcFolder, 0, 0);
			this.tlpSrcFolder.Dock = System.Windows.Forms.DockStyle.Fill;
			this.tlpSrcFolder.Location = new System.Drawing.Point(0, 0);
			this.tlpSrcFolder.Margin = new System.Windows.Forms.Padding(0);
			this.tlpSrcFolder.Name = "tlpSrcFolder";
			this.tlpSrcFolder.RowCount = 1;
			this.tlpSrcFolder.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
			this.tlpSrcFolder.Size = new System.Drawing.Size(493, 32);
			this.tlpSrcFolder.TabIndex = 1;
			// 
			// xbtnSrc
			// 
			this.xbtnSrc.BackColorInternal = System.Drawing.Color.White;
			this.xbtnSrc.ButtonStyle = Infragistics.Win.UIElementButtonStyle.WindowsXPCommandButton;
			this.xbtnSrc.Dock = System.Windows.Forms.DockStyle.Fill;
			appearance14.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(204)))), ((int)(((byte)(222)))), ((int)(((byte)(214)))));
			this.xbtnSrc.HotTrackAppearance = appearance14;
			this.xbtnSrc.Location = new System.Drawing.Point(368, 2);
			this.xbtnSrc.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
			this.xbtnSrc.Name = "xbtnSrc";
			this.xbtnSrc.Size = new System.Drawing.Size(122, 28);
			this.xbtnSrc.TabIndex = 1;
			this.xbtnSrc.Text = "Select &Source";
			this.xbtnSrc.UseOsThemes = Infragistics.Win.DefaultableBoolean.False;
			this.xbtnSrc.Click += new System.EventHandler(this.xbtnSrc_Click);
			// 
			// xtxtSrcFolder
			// 
			appearance6.BackColor = System.Drawing.Color.White;
			appearance6.TextHAlignAsString = "Left";
			appearance6.TextVAlignAsString = "Middle";
			this.xtxtSrcFolder.Appearance = appearance6;
			this.xtxtSrcFolder.AutoSize = false;
			this.xtxtSrcFolder.BackColor = System.Drawing.Color.White;
			this.xtxtSrcFolder.BorderStyle = Infragistics.Win.UIElementBorderStyle.Rounded1;
			this.xtxtSrcFolder.Dock = System.Windows.Forms.DockStyle.Fill;
			this.xtxtSrcFolder.Location = new System.Drawing.Point(3, 3);
			this.xtxtSrcFolder.Name = "xtxtSrcFolder";
			this.xtxtSrcFolder.Size = new System.Drawing.Size(359, 26);
			this.xtxtSrcFolder.TabIndex = 0;
			this.toolTip1.SetToolTip(this.xtxtSrcFolder, "목록을 보려면 Click하세요");
			this.xtxtSrcFolder.UseOsThemes = Infragistics.Win.DefaultableBoolean.False;
			// 
			// fbDialog
			// 
			this.fbDialog.RootFolder = System.Environment.SpecialFolder.MyComputer;
			// 
			// statusText
			// 
			this.statusText.AutoToolTip = true;
			this.statusText.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
			this.statusText.Name = "statusText";
			this.statusText.Size = new System.Drawing.Size(795, 17);
			this.statusText.Spring = true;
			this.statusText.Text = "Status";
			this.statusText.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// statusProgress
			// 
			this.statusProgress.AutoToolTip = true;
			this.statusProgress.BackColor = System.Drawing.Color.White;
			this.statusProgress.Name = "statusProgress";
			this.statusProgress.Size = new System.Drawing.Size(200, 16);
			this.statusProgress.Step = 1;
			// 
			// statusStrip
			// 
			this.statusStrip.BackColor = System.Drawing.Color.White;
			this.statusStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.statusText,
            this.statusProgress});
			this.statusStrip.Location = new System.Drawing.Point(0, 682);
			this.statusStrip.Name = "statusStrip";
			this.statusStrip.ShowItemToolTips = true;
			this.statusStrip.Size = new System.Drawing.Size(1012, 22);
			this.statusStrip.TabIndex = 6;
			// 
			// splitContainer
			// 
			this.splitContainer.BackColor = System.Drawing.Color.White;
			this.splitContainer.Dock = System.Windows.Forms.DockStyle.Fill;
			this.splitContainer.Location = new System.Drawing.Point(0, 0);
			this.splitContainer.Name = "splitContainer";
			this.splitContainer.Orientation = System.Windows.Forms.Orientation.Horizontal;
			// 
			// splitContainer.Panel1
			// 
			this.splitContainer.Panel1.BackColor = System.Drawing.Color.White;
			this.splitContainer.Panel1.Controls.Add(this.tlpMain);
			this.splitContainer.Panel1MinSize = 340;
			// 
			// splitContainer.Panel2
			// 
			this.splitContainer.Panel2.BackColor = System.Drawing.Color.White;
			this.splitContainer.Panel2.Controls.Add(this.xgrbDiffer);
			this.splitContainer.Panel2.Padding = new System.Windows.Forms.Padding(3);
			this.splitContainer.Size = new System.Drawing.Size(1012, 682);
			this.splitContainer.SplitterDistance = 432;
			this.splitContainer.SplitterWidth = 3;
			this.splitContainer.TabIndex = 1;
			// 
			// xgrbDiffer
			// 
			appearance1.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(189)))), ((int)(((byte)(196)))), ((int)(((byte)(211)))));
			appearance1.TextVAlignAsString = "Middle";
			this.xgrbDiffer.Appearance = appearance1;
			this.xgrbDiffer.BackColorInternal = System.Drawing.Color.White;
			this.xgrbDiffer.ContentPadding.Bottom = 2;
			this.xgrbDiffer.ContentPadding.Left = 2;
			this.xgrbDiffer.ContentPadding.Right = 2;
			this.xgrbDiffer.ContentPadding.Top = 2;
			this.xgrbDiffer.Controls.Add(this.xgridDiffer);
			this.xgrbDiffer.Dock = System.Windows.Forms.DockStyle.Fill;
			appearance118.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(204)))), ((int)(((byte)(222)))), ((int)(((byte)(214)))));
			appearance118.FontData.BoldAsString = "True";
			appearance118.ForeColor = System.Drawing.Color.Black;
			appearance118.ForegroundAlpha = Infragistics.Win.Alpha.Opaque;
			appearance118.TextVAlignAsString = "Top";
			this.xgrbDiffer.HeaderAppearance = appearance118;
			this.xgrbDiffer.HeaderBorderStyle = Infragistics.Win.UIElementBorderStyle.Rounded4;
			this.xgrbDiffer.HeaderPosition = Infragistics.Win.Misc.GroupBoxHeaderPosition.TopInsideBorder;
			this.xgrbDiffer.Location = new System.Drawing.Point(3, 3);
			this.xgrbDiffer.Name = "xgrbDiffer";
			this.xgrbDiffer.Size = new System.Drawing.Size(1006, 241);
			this.xgrbDiffer.TabIndex = 0;
			this.xgrbDiffer.Text = "Different Files";
			// 
			// xgridDiffer
			// 
			appearance146.BackColor = System.Drawing.Color.White;
			appearance146.BorderColor = System.Drawing.Color.Gray;
			this.xgridDiffer.DisplayLayout.Appearance = appearance146;
			this.xgridDiffer.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
			this.xgridDiffer.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
			this.xgridDiffer.DisplayLayout.GroupByBox.Hidden = true;
			appearance147.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(168)))), ((int)(((byte)(197)))), ((int)(((byte)(183)))));
			appearance147.BorderColor = System.Drawing.Color.Orange;
			this.xgridDiffer.DisplayLayout.Override.ActiveRowAppearance = appearance147;
			this.xgridDiffer.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Solid;
			this.xgridDiffer.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Solid;
			this.xgridDiffer.DisplayLayout.Override.BorderStyleSpecialRowSeparator = Infragistics.Win.UIElementBorderStyle.None;
			this.xgridDiffer.DisplayLayout.Override.BorderStyleSummaryFooter = Infragistics.Win.UIElementBorderStyle.Solid;
			this.xgridDiffer.DisplayLayout.Override.BorderStyleSummaryValue = Infragistics.Win.UIElementBorderStyle.None;
			this.xgridDiffer.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
			this.xgridDiffer.DisplayLayout.Override.ColumnAutoSizeMode = Infragistics.Win.UltraWinGrid.ColumnAutoSizeMode.VisibleRows;
			this.xgridDiffer.DisplayLayout.Override.ColumnSizingArea = Infragistics.Win.UltraWinGrid.ColumnSizingArea.EntireColumn;
			this.xgridDiffer.DisplayLayout.Override.FixedCellSeparatorColor = System.Drawing.Color.OrangeRed;
			this.xgridDiffer.DisplayLayout.Override.FixedHeaderIndicator = Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.None;
			appearance148.TextHAlignAsString = "Center";
			appearance148.TextVAlignAsString = "Middle";
			this.xgridDiffer.DisplayLayout.Override.HeaderAppearance = appearance148;
			this.xgridDiffer.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
			this.xgridDiffer.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
			appearance17.BackColor = System.Drawing.Color.LightBlue;
			this.xgridDiffer.DisplayLayout.Override.HotTrackCellAppearance = appearance17;
			appearance18.BorderColor = System.Drawing.Color.Orange;
			this.xgridDiffer.DisplayLayout.Override.HotTrackRowAppearance = appearance18;
			appearance19.BackColor = System.Drawing.Color.LightBlue;
			this.xgridDiffer.DisplayLayout.Override.HotTrackRowCellAppearance = appearance19;
			appearance20.BackColor = System.Drawing.Color.Silver;
			this.xgridDiffer.DisplayLayout.Override.HotTrackRowSelectorAppearance = appearance20;
			appearance149.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(235)))), ((int)(((byte)(230)))));
			this.xgridDiffer.DisplayLayout.Override.RowAlternateAppearance = appearance149;
			appearance125.BackColor = System.Drawing.Color.White;
			this.xgridDiffer.DisplayLayout.Override.RowAppearance = appearance125;
			appearance23.TextHAlignAsString = "Center";
			appearance23.TextVAlignAsString = "Middle";
			this.xgridDiffer.DisplayLayout.Override.RowSelectorAppearance = appearance23;
			appearance24.TextHAlignAsString = "Center";
			appearance24.TextVAlignAsString = "Middle";
			this.xgridDiffer.DisplayLayout.Override.RowSelectorHeaderAppearance = appearance24;
			this.xgridDiffer.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
			this.xgridDiffer.DisplayLayout.Override.SpecialRowSeparator = Infragistics.Win.UltraWinGrid.SpecialRowSeparator.SummaryRow;
			appearance25.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(220)))), ((int)(((byte)(225)))), ((int)(((byte)(190)))));
			this.xgridDiffer.DisplayLayout.Override.SpecialRowSeparatorAppearance = appearance25;
			this.xgridDiffer.DisplayLayout.Override.SpecialRowSeparatorHeight = 3;
			appearance26.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(238)))), ((int)(((byte)(237)))), ((int)(((byte)(218)))));
			appearance26.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(189)))), ((int)(((byte)(196)))), ((int)(((byte)(211)))));
			this.xgridDiffer.DisplayLayout.Override.SummaryFooterAppearance = appearance26;
			this.xgridDiffer.DisplayLayout.Override.SummaryFooterCaptionVisible = Infragistics.Win.DefaultableBoolean.False;
			appearance27.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(238)))), ((int)(((byte)(237)))), ((int)(((byte)(218)))));
			appearance27.TextVAlignAsString = "Middle";
			this.xgridDiffer.DisplayLayout.Override.SummaryValueAppearance = appearance27;
			this.xgridDiffer.DisplayLayout.UseFixedHeaders = true;
			this.xgridDiffer.Dock = System.Windows.Forms.DockStyle.Fill;
			this.xgridDiffer.Location = new System.Drawing.Point(5, 27);
			this.xgridDiffer.Name = "xgridDiffer";
			this.xgridDiffer.Size = new System.Drawing.Size(996, 209);
			this.xgridDiffer.TabIndex = 0;
			this.xgridDiffer.Text = "ultraGrid1";
			this.xgridDiffer.UpdateMode = Infragistics.Win.UltraWinGrid.UpdateMode.OnUpdate;
			this.xgridDiffer.UseOsThemes = Infragistics.Win.DefaultableBoolean.False;
			this.xgridDiffer.CellChange += new Infragistics.Win.UltraWinGrid.CellEventHandler(this.Grid_CellChange);
			// 
			// FolderSynchronizerForm
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(1012, 704);
			this.Controls.Add(this.splitContainer);
			this.Controls.Add(this.statusStrip);
			this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
			this.Name = "FolderSynchronizerForm";
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Text = "Folder Synchronizer";
			this.tlpMain.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.xgrbDestination)).EndInit();
			this.xgrbDestination.ResumeLayout(false);
			this.tlpDestination.ResumeLayout(false);
			this.tlpToSrc.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.xgridDest)).EndInit();
			this.tlpDestFolder.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.xtxtDestFolder)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.xgrbSource)).EndInit();
			this.xgrbSource.ResumeLayout(false);
			this.tlpSource.ResumeLayout(false);
			this.tlpToDest.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.xgridSrc)).EndInit();
			this.tlpSrcFolder.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.xtxtSrcFolder)).EndInit();
			this.statusStrip.ResumeLayout(false);
			this.statusStrip.PerformLayout();
			this.splitContainer.Panel1.ResumeLayout(false);
			this.splitContainer.Panel2.ResumeLayout(false);
			this.splitContainer.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.xgrbDiffer)).EndInit();
			this.xgrbDiffer.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.xgridDiffer)).EndInit();
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.TableLayoutPanel tlpMain;
		private System.Windows.Forms.TableLayoutPanel tlpToDest;
		private System.Windows.Forms.TableLayoutPanel tlpToSrc;
		private System.Windows.Forms.FolderBrowserDialog fbDialog;
		private System.Windows.Forms.StatusStrip statusStrip;
		private System.Windows.Forms.ToolStripStatusLabel statusText;
		private System.Windows.Forms.ToolStripProgressBar statusProgress;
		private System.Windows.Forms.SplitContainer splitContainer;
		private Infragistics.Win.UltraWinGrid.UltraGrid xgridSrc;
		private Infragistics.Win.Misc.UltraButton xbtnSrc;
		private Infragistics.Win.Misc.UltraButton xbtnDest;
		private Infragistics.Win.UltraWinEditors.UltraTextEditor xtxtSrcFolder;
		private Infragistics.Win.UltraWinEditors.UltraTextEditor xtxtDestFolder;
		private Infragistics.Win.UltraWinGrid.UltraGrid xgridDest;
		private Infragistics.Win.UltraWinGrid.UltraGrid xgridDiffer;
		private Infragistics.Win.Misc.UltraButton xbtnCompareWithDest;
		private Infragistics.Win.Misc.UltraButton xbtnMoveToSrc;
		private Infragistics.Win.Misc.UltraButton xbtnCopyToSrc;
		private Infragistics.Win.Misc.UltraButton xbtnCompareWithSrc;
		private Infragistics.Win.Misc.UltraButton xbtnMoveToDesc;
		private Infragistics.Win.Misc.UltraButton xbtnCopyToDest;
		private Infragistics.Win.Misc.UltraGroupBox xgrbSource;
		private Infragistics.Win.Misc.UltraGroupBox xgrbDestination;
		private Infragistics.Win.Misc.UltraGroupBox xgrbDiffer;
		private System.Windows.Forms.TableLayoutPanel tlpSource;
		private System.Windows.Forms.TableLayoutPanel tlpDestination;
		private System.Windows.Forms.TableLayoutPanel tlpDestFolder;
		private System.Windows.Forms.TableLayoutPanel tlpSrcFolder;
		private System.Windows.Forms.ToolTip toolTip1;
	}
}

