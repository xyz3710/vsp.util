﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using iDASiT.Framework.Win.Server;
using iDASiT.Framework.Win;
using iDASiT.Framework.Win.Controls.UltraGridHelper;
using Infragistics.Win.UltraWinGrid;
using System.IO;
using Infragistics.Win;

namespace FolderSynchronizer
{
	public partial class FolderSynchronizerForm : Form
	{
		#region Constants
        private const string SELECT_KEY = "Sel";
		private const string PATH_KEY = "Path";
		private const string FILENAME_KEY = "FileName";
		private const string COUNT = "Count";
		private const string LAST_WRITE_TIME_KEY = "LastWriteTime";
		private const string CREATION_TIME_KEY = "CreationTime";
		private const string FILE_SIZE_KEY = "FileSize";
		private const string SIZE_KEY = "Size";
		private const string FOLDER_NAME_KEY = "FolderName";
		private const string FOLDER_NAME = "Folder Name";
		#endregion

		#region Delegates
		private delegate void SetProgressbarCount(int count);
		private delegate void SetStatusBarText(string text);
		private delegate void AddRowItem(UltraGrid targetGrid, UpdateFileInfo ufi);
		#endregion

		#region Fields
		private string _defaultSearchPatterns;
		private List<UpdateFileInfo> _srcList;
		private List<UpdateFileInfo> _destList;
		private List<UpdateFileInfo> _differs;
		#endregion
		
		#region Constructors
        public FolderSynchronizerForm()
		{
			InitializeComponent();
		}
		#endregion
		
		#region Properties
		private string SourceFolder
		{
			get
			{
				return xtxtSrcFolder.Text;
			}
			set
			{
				xtxtSrcFolder.Text = value;
			}
		}

		private string DestinationFolder
		{
			get
			{
				return xtxtDestFolder.Text;
			}
			set
			{
				xtxtDestFolder.Text = value;
			}
		}

		private string DefaultSearchPatterns
		{
			get
			{
				if (string.IsNullOrEmpty(_defaultSearchPatterns) == true)
					_defaultSearchPatterns = "*.*";

				return _defaultSearchPatterns;
			}
			set
			{
				_defaultSearchPatterns = value;
			}
		}
		#endregion

		#region Form event handler
		protected override void OnShown(EventArgs e)
		{
			base.OnShown(e);

			InitSourceGrid();
			InitDestGrid();
			InitDifferGrid();
			SetStatusBar(string.Empty);
			xtxtSrcFolder.Text = @"F:\VSP";
			xtxtDestFolder.Text = @"F:\vsp2";
		}

		private void xbtnSrc_Click(object sender, EventArgs e)
		{
			if (SourceFolder != string.Empty && xgridSrc.Rows.Count == 0)
			{
				GetSourceList();

				return;
			}
			else
			{
				fbDialog.Description = "Please select compared source folder.";
				fbDialog.ShowNewFolderButton = false;
				fbDialog.SelectedPath = SourceFolder;

				if (fbDialog.ShowDialog() == DialogResult.OK)
				{
					if (fbDialog.SelectedPath != string.Empty)
					{
						SourceFolder = fbDialog.SelectedPath;
						
						if (DestinationFolder == string.Empty)
                        	DestinationFolder = fbDialog.SelectedPath;

						GetSourceList();
					}
				}
			}
		}

		private void xbtnDest_Click(object sender, EventArgs e)
		{
			if (DestinationFolder != string.Empty && xgridDest.Rows.Count == 0)
			{
				GetDestinationList();

				return;
			}
			else
			{
				fbDialog.Description = "Please select compared destination folder.";
				fbDialog.ShowNewFolderButton = false;
				fbDialog.SelectedPath = DestinationFolder;

				if (fbDialog.ShowDialog() == DialogResult.OK)
				{
					if (fbDialog.SelectedPath != string.Empty)
					{
						DestinationFolder = fbDialog.SelectedPath;
						GetDestinationList();
					}
				}
			}
		}

		private void xbtnCompareWithDest_Click(object sender, EventArgs e)
		{
			CompareFiles(true);
		}

		private void xbtnCopyToDest_Click(object sender, EventArgs e)
		{
			CopyFiles(true);
		}

		private void xbtnMoveToDesc_Click(object sender, EventArgs e)
		{
			CopyFiles(true, true);
		}

		private void xbtnCompareWithSrc_Click(object sender, EventArgs e)
		{
			CompareFiles(false);
		}

		private void xbtnCopyToSrc_Click(object sender, EventArgs e)
		{
			CopyFiles(false);
		}

		private void xbtnMoveToSrc_Click(object sender, EventArgs e)
		{
			CopyFiles(false, true);
		}

		private void Grid_CellChange(object sender, CellEventArgs e)
		{
			if (e.Cell.Column.Key == SELECT_KEY)
				e.Cell.Selected = true;

		}
		#endregion

		#region Private methods
		private void GetSourceList()
		{
			if (Directory.Exists(SourceFolder) == false)
			{
				MessageBox.Show("Source folder does not exist.", Text, MessageBoxButtons.OK, MessageBoxIcon.Warning);

				return;
			}

			ClearSourceGrid();
			
			UpdateFileInfoManager updateFileInfoManager = new UpdateFileInfoManager();

			updateFileInfoManager.DefaultSearchPatterns = DefaultSearchPatterns;
			updateFileInfoManager.GetUpdateFileInfoStarted += new UpdateFileInfoManager.GetUpdateFileInfoStartedEventHandler(OnSourceGetUpdateFileInfoStarted);
			updateFileInfoManager.GetUpdateFileInfoProcessing += new UpdateFileInfoManager.GetUpdateFileInfoProcessingEventHandler(OnSourceGetUpdateFileInfoProcessing);
			updateFileInfoManager.GetUpdateFileInfoCompleted += new UpdateFileInfoManager.GetUpdateFileInfoCompletedEventHandler(OnSourceGetUpdateFileInfoCompleted);

			if (_srcList != null)
				_srcList.Clear();
            
			_srcList = updateFileInfoManager.GetUpdateFileInfo(SourceFolder);
			ClearDifferGrid();
		}

		private void GetDestinationList()
		{
			if (Directory.Exists(DestinationFolder) == false)
			{
				MessageBox.Show("Destination folder does not exist.", Text, MessageBoxButtons.OK, MessageBoxIcon.Warning);

				return;
			}

			ClearDestGrid();

			UpdateFileInfoManager updateFileInfoManager = new UpdateFileInfoManager();

			updateFileInfoManager.DefaultSearchPatterns = DefaultSearchPatterns;
			updateFileInfoManager.GetUpdateFileInfoStarted += new UpdateFileInfoManager.GetUpdateFileInfoStartedEventHandler(OnDestinationGetUpdateFileInfoStarted);
			updateFileInfoManager.GetUpdateFileInfoProcessing += new UpdateFileInfoManager.GetUpdateFileInfoProcessingEventHandler(OnDestinationGetUpdateFileInfoProcessing);
			updateFileInfoManager.GetUpdateFileInfoCompleted += new UpdateFileInfoManager.GetUpdateFileInfoCompletedEventHandler(OnDestinationGetUpdateFileInfoCompleted);

			if (_destList != null)
				_destList.Clear();
            
			_destList = updateFileInfoManager.GetUpdateFileInfo(DestinationFolder);
			ClearDifferGrid();
		}

		private void CompareFiles(bool compareWithDest)
		{
			if (_srcList == null || _destList == null)
				return;

			ClearDifferGrid();

			UpdateFileInfoManager updateFileInfoManager = new UpdateFileInfoManager();

			updateFileInfoManager.CompareUpdateFileInfoStarted += new UpdateFileInfoManager.CompareUpdateFileInfoStartedEventHandler(OnCompareUpdateFileInfoStarted);
			updateFileInfoManager.CompareUpdateFileInfoProcessing += new UpdateFileInfoManager.CompareUpdateFileInfoProcessingEventHandler(OnCompareUpdateFileInfoProcessing);
			updateFileInfoManager.CompareUpdateFileInfoCompleted += new UpdateFileInfoManager.CompareUpdateFileInfoCompletedEventHandler(delegate(UpdateFileInfosEventArgs e)
			{
				SetProgressbar(statusProgress.Maximum);
				SetStatusBar(string.Format("Total {0} files were compared, then {1} files are differents.", e.TotalCount, e.UpdateFileInfos.Count));

				if (xgridDiffer.Rows.Count != e.TotalCount)
					// Add된 Row가 없는경우는 Destination 쪽에 하나도 없어서 전체가 한번에 Binding된 경우다.
					UltraGridHelper.DataBind(xgridDiffer, e.UpdateFileInfos);

				SelectComparedGrid(compareWithDest == true ? xgridSrc : xgridDest, e.UpdateFileInfos);
				xgridDiffer.ActiveRow = null;
			});

			if (_differs != null)
				_differs.Clear();
            
			_differs = compareWithDest == true ? 
					updateFileInfoManager.CompareUpdateFileInfo(_srcList, _destList) : 
					updateFileInfoManager.CompareUpdateFileInfo(_destList, _srcList);
		}

		private void SelectComparedGrid(UltraGrid targetGrid, List<UpdateFileInfo> updateFileInfos)
		{
			UnSetGridSelectionAll();

			for (int index = 0; index < targetGrid.Rows.Count; index++)
			{
				foreach (UpdateFileInfo ufi in updateFileInfos)
				{
					string differLogString = GetLogString(ufi);
					string gridLogString = GetLogString(targetGrid.Rows[index]);

					if (gridLogString == differLogString)
					{
						targetGrid.Rows[index].Cells[SELECT_KEY].Value = true;

						break;
					}
				}
			}

			targetGrid.EndUpdate();
		}

		private void SetGridSelection(UltraGrid targetGrid, bool selectionValue)
		{
			targetGrid.BeginUpdate();

			foreach (UltraGridRow xRow in targetGrid.Rows)
				xRow.Cells[SELECT_KEY].Value = selectionValue;

			targetGrid.EndUpdate();
		}

		private void UnSetGridSelectionAll()
		{
			SetGridSelection(xgridSrc, false);
			SetGridSelection(xgridDest, false);
			SetGridSelection(xgridDiffer, false);
		}

		private string GetLogString(UltraGridRow xRow)
		{
			string logString = string.Empty;

			if (xRow != null)
			{
				logString = string.Format(@"{0}\{1}",
								Convert.ToString(xRow.Cells[PATH_KEY].Value),
								Convert.ToString(xRow.Cells[FILENAME_KEY].Value));
			}

			return logString;
		}

		private string GetLogString(UpdateFileInfo updateFileInfo)
		{
			return string.Format(@"{0}\{1}", updateFileInfo.Path, updateFileInfo.Name);
		}

		/// <summary>
		/// Grid상에서 선택된 Path + Filename만을 구합니다.
		/// </summary>
		/// <param name="sourceFolder"></param>
		/// <returns></returns>
		private List<string> GetSelectedFiles(UltraGrid targetGrid, string sourceFolder)
		{
			List<string> selectedFiles = new List<string>();

			foreach (UltraGridRow xRow in UltraGridHelper.GetSelectedRows(targetGrid, SELECT_KEY))
				selectedFiles.Add(string.Format(@"{0}\{1}", Convert.ToString(xRow.Cells[PATH_KEY].Value), Convert.ToString(xRow.Cells[FILENAME_KEY].Value)));

			return selectedFiles;
		}

		private void CopyFiles(bool compareWithDest)
		{
			bool moveAction = false;

			CopyFiles(compareWithDest, moveAction);
		}

        private void CopyFiles(bool compareWithDest, bool moveAction)
		{
			if (_differs == null)
				return;

			string sourceFolder = string.Empty;
			string destFolder = string.Empty;
			List<string> targetFiles = new List<string>();

			if (compareWithDest == true)
			{
				sourceFolder = xtxtSrcFolder.Text;
				destFolder = xtxtDestFolder.Text;
				targetFiles = GetSelectedFiles(xgridSrc, sourceFolder);
			}
			else
			{
				sourceFolder = xtxtDestFolder.Text;
				destFolder = xtxtSrcFolder.Text;
				targetFiles = GetSelectedFiles(xgridDest, destFolder);
			}

			InitProgressbar(targetFiles.Count);
			SetStatusBar(string.Empty);

			for (int index = 0; index < targetFiles.Count; index++)
			{
				try
				{
					string sourceFilePath = string.Format(@"{0}{1}", sourceFolder, targetFiles[index]);
					string destFilePath = string.Format(@"{0}{1}", destFolder, targetFiles[index]);
					string onlyDestFolderName = Path.GetDirectoryName(destFilePath);

					if (File.Exists(sourceFilePath) == false)
						continue;

					if (Directory.Exists(onlyDestFolderName) == false)
						Directory.CreateDirectory(onlyDestFolderName);

					if (moveAction == true)
						File.Move(sourceFilePath, destFilePath);
					else
						File.Copy(sourceFilePath, destFilePath, true);

					string msg = string.Format("Copy ({0}) to ({1}) ", sourceFilePath, destFolder);

					SetStatusBar(msg);
				}
				catch
				{
				}
				finally
				{
					SetProgressbar(index);
				}
			}
		}
		#endregion

		#region Cross Thread Handler
		private void InitProgressbar(int maximum)
		{
			statusProgress.Minimum = 0;
			statusProgress.Maximum = maximum;
		}

		private void SetProgressbar(int count)
		{
			if (InvokeRequired == true)
			{
				SetProgressbarCount setProgressbarCount = new SetProgressbarCount(SetProgressbar);

				Invoke(setProgressbarCount, count);
			}
			else
				statusProgress.Value = count;
		}

		private void SetStatusBar(string text)
		{
			if (InvokeRequired == true)
            {
				SetStatusBarText setStatusBarText = new SetStatusBarText(SetStatusBar);

				Invoke(setStatusBarText, text);
            }
			else
			{
				statusText.Text = text;
				statusStrip.Update();
			}
		}

		private void AddRow(UltraGrid targetGrid, UpdateFileInfo ufi)
		{
			if (targetGrid.InvokeRequired == true)
			{
				AddRowItem addRowItem = new AddRowItem(AddRow);

				Invoke(addRowItem, targetGrid, ufi);
			}
			else
			{
				targetGrid.BeginUpdate();

				Dictionary<string, object> newRowCollection = new Dictionary<string, object>();

				newRowCollection.Add(SELECT_KEY, false);
				newRowCollection.Add(FOLDER_NAME_KEY, Path.GetFileName(ufi.Path));
				newRowCollection.Add(FILENAME_KEY, ufi.Name);
				newRowCollection.Add(PATH_KEY, ufi.Path);
				newRowCollection.Add(LAST_WRITE_TIME_KEY, ufi.LastWriteTime);
				newRowCollection.Add(FILE_SIZE_KEY, ufi.FileSize);
				newRowCollection.Add(CREATION_TIME_KEY, ufi.CreationTime);

				UltraGridHelper.AddNewRow(targetGrid, newRowCollection, false);
				//targetGrid.Update();
				targetGrid.EndUpdate();
			}
		}
		#endregion

		#region Event Handler
		private void OnSourceGetUpdateFileInfoStarted(UpdateFileInfosEventArgs e)
		{
			SetStatusBar(string.Empty);
			InitProgressbar(e.TotalCount);
		}

		private void OnSourceGetUpdateFileInfoProcessing(UpdateFileInfoEventArgs e)
		{
			string log = GetLogString(e.UpdateFileInfo);

			AddRow(xgridSrc, e.UpdateFileInfo);
			SetStatusBar(log);
			SetProgressbar(e.Count);
		}

		private void OnSourceGetUpdateFileInfoCompleted(UpdateFileInfosEventArgs e)
		{
			xgridSrc.ActiveRow = null;
		}

		private void OnDestinationGetUpdateFileInfoStarted(UpdateFileInfosEventArgs e)
		{
			SetStatusBar(string.Empty);
			InitProgressbar(e.TotalCount);
		}

		private void OnDestinationGetUpdateFileInfoProcessing(UpdateFileInfoEventArgs e)
		{
			string log = GetLogString(e.UpdateFileInfo);

			AddRow(xgridDest, e.UpdateFileInfo);
			SetStatusBar(log);
			SetProgressbar(e.Count);
		}

		private void OnDestinationGetUpdateFileInfoCompleted(UpdateFileInfosEventArgs e)
		{
			xgridDest.ActiveRow = null;
		}

		private void OnCompareUpdateFileInfoStarted(UpdateFileInfosEventArgs e)
		{
			SetStatusBar(string.Empty);
			InitProgressbar(e.TotalCount);
		}

		private void OnCompareUpdateFileInfoProcessing(UpdateFileInfoEventArgs e)
		{
			string log = GetLogString(e.UpdateFileInfo);

			AddRow(xgridDiffer, e.UpdateFileInfo);
			SetStatusBar(log);
			SetProgressbar(e.Count);
		}
		#endregion

		#region Grid handling
		private void InitSourceGrid()
		{
			UltraGridHelperColumn xcolSel = new UltraGridHelperColumn(xgridSrc, SELECT_KEY, " ", UltraGridColumnStyle.CheckBox, true);
			UltraGridHelperColumn xcolFolderName = new UltraGridHelperColumn(xgridSrc, FOLDER_NAME_KEY, FOLDER_NAME, 120 );
			UltraGridHelperColumn xcolName = new UltraGridHelperColumn(xgridSrc, FILENAME_KEY, FILENAME_KEY, UltraGridColumnStyle.Text, 120, false);
			UltraGridHelperColumn xcolPath = new UltraGridHelperColumn(xgridSrc, PATH_KEY, PATH_KEY, UltraGridColumnStyle.Text, 250, false);
			UltraGridHelperColumn xcolLastWriteTime = new UltraGridHelperColumn(xgridSrc, LAST_WRITE_TIME_KEY, LAST_WRITE_TIME_KEY, UltraGridColumnStyle.DateTime, false);
			UltraGridHelperColumn xcolCreationTime = new UltraGridHelperColumn(xgridSrc, CREATION_TIME_KEY, CREATION_TIME_KEY, UltraGridColumnStyle.DateTime, false);
			UltraGridHelperColumn xcolFileSize = new UltraGridHelperColumn(xgridSrc, FILE_SIZE_KEY, FILE_SIZE_KEY, UltraGridColumnStyle.Numeric, false);

			#region Summary
			UltraGridBand bandAmount = xgridSrc.DisplayLayout.Bands[0];

			bandAmount.Summaries.Add(SELECT_KEY, SummaryType.Custom, new CustomSummaryCalculator(SELECT_KEY, true), bandAmount.Columns[SELECT_KEY], SummaryPosition.UseSummaryPositionColumn, null);
			bandAmount.Summaries.Add(SIZE_KEY, 
                SummaryType.Custom, 
                new CustomSummaryCalculator(FILE_SIZE_KEY, SELECT_KEY, true, CalculatorType.Sum), 
                bandAmount.Columns[FILE_SIZE_KEY], 
                SummaryPosition.UseSummaryPositionColumn, 
                null);
			bandAmount.Summaries.Add(COUNT, SummaryType.Count, bandAmount.Columns[FILENAME_KEY]);

			bandAmount.Summaries[SELECT_KEY].Appearance.TextHAlign = HAlign.Center;
			bandAmount.Summaries[SIZE_KEY].Appearance.TextHAlign = HAlign.Right;
			bandAmount.Summaries[COUNT].Appearance.TextHAlign = HAlign.Right;

			bandAmount.Summaries[SELECT_KEY].DisplayFormat = "{0}";
			bandAmount.Summaries[SIZE_KEY].DisplayFormat = string.Format("{{0:{0}}}{1}", "#,##0", "bytes");
			bandAmount.Summaries[COUNT].DisplayFormat = string.Format("{0} : {{0:{1}}}", COUNT, "#,##0");

			// Summary Position
			xgridSrc.DisplayLayout.Override.SummaryDisplayArea = SummaryDisplayAreas.BottomFixed;
			#endregion
	
			xcolSel.Fixed = true;
			xcolFolderName.Fixed = true;
			xcolPath.SortIndicator = SortIndicator.Ascending;
			xcolName.Fixed = true;
			xgridSrc.DisplayLayout.AutoFitStyle = AutoFitStyle.None;
			UltraGridHelper.SetGridOptions(xgridSrc, false, false, false, true);
		}

		private void InitDestGrid()
		{
			UltraGridHelperColumn xcolSel = new UltraGridHelperColumn(xgridDest, SELECT_KEY, " ", UltraGridColumnStyle.CheckBox, true);
			UltraGridHelperColumn xcolFolderName = new UltraGridHelperColumn(xgridDest, FOLDER_NAME_KEY, FOLDER_NAME, 120);
			UltraGridHelperColumn xcolName = new UltraGridHelperColumn(xgridDest, FILENAME_KEY, FILENAME_KEY, UltraGridColumnStyle.Text, 120, false);
			UltraGridHelperColumn xcolPath = new UltraGridHelperColumn(xgridDest, PATH_KEY, PATH_KEY, UltraGridColumnStyle.Text, 250, false);
			UltraGridHelperColumn xcolLastWriteTime = new UltraGridHelperColumn(xgridDest, LAST_WRITE_TIME_KEY, LAST_WRITE_TIME_KEY, UltraGridColumnStyle.DateTime, false);
			UltraGridHelperColumn xcolCreationTime = new UltraGridHelperColumn(xgridDest, CREATION_TIME_KEY, CREATION_TIME_KEY, UltraGridColumnStyle.DateTime, false);
			UltraGridHelperColumn xcolFileSize = new UltraGridHelperColumn(xgridDest, FILE_SIZE_KEY, FILE_SIZE_KEY, UltraGridColumnStyle.Numeric, false);

			#region Summary
			UltraGridBand bandAmount = xgridDest.DisplayLayout.Bands[0];

			bandAmount.Summaries.Add(SELECT_KEY, SummaryType.Custom, new CustomSummaryCalculator(SELECT_KEY, true), bandAmount.Columns[SELECT_KEY], SummaryPosition.UseSummaryPositionColumn, null);
			bandAmount.Summaries.Add(SIZE_KEY, 
                SummaryType.Custom, 
                new CustomSummaryCalculator(FILE_SIZE_KEY, SELECT_KEY, true, CalculatorType.Sum), 
                bandAmount.Columns[FILE_SIZE_KEY], 
                SummaryPosition.UseSummaryPositionColumn, 
                null);
			bandAmount.Summaries.Add(COUNT, SummaryType.Count, bandAmount.Columns[FILENAME_KEY]);

			bandAmount.Summaries[SELECT_KEY].Appearance.TextHAlign = HAlign.Center;
			bandAmount.Summaries[SIZE_KEY].Appearance.TextHAlign = HAlign.Right;
			bandAmount.Summaries[COUNT].Appearance.TextHAlign = HAlign.Right;

			bandAmount.Summaries[SELECT_KEY].DisplayFormat = "{0}";
			bandAmount.Summaries[SIZE_KEY].DisplayFormat = string.Format("{{0:{0}}}{1}", "#,##0", "bytes");
			bandAmount.Summaries[COUNT].DisplayFormat = string.Format("{0} : {{0:{1}}}", COUNT, "#,##0");

			// Summary Position
			xgridDest.DisplayLayout.Override.SummaryDisplayArea = SummaryDisplayAreas.BottomFixed;
			#endregion

			xcolSel.Fixed = true;
			xcolFolderName.Fixed = true;
			xcolName.Fixed = true;
			xcolPath.SortIndicator = SortIndicator.Ascending;
			xgridDest.DisplayLayout.AutoFitStyle = AutoFitStyle.None;
			UltraGridHelper.SetGridOptions(xgridDest, false, false, false, true);
		}

		private void InitDifferGrid()
		{
			UltraGridHelperColumn xcolSel = new UltraGridHelperColumn(xgridDiffer, SELECT_KEY, " ", UltraGridColumnStyle.CheckBox, true);
			UltraGridHelperColumn xcolFolderName = new UltraGridHelperColumn(xgridDiffer, FOLDER_NAME_KEY, FOLDER_NAME, 120);
			UltraGridHelperColumn xcolName = new UltraGridHelperColumn(xgridDiffer, FILENAME_KEY, FILENAME_KEY, UltraGridColumnStyle.Text, 120, false);
			UltraGridHelperColumn xcolPath = new UltraGridHelperColumn(xgridDiffer, PATH_KEY, PATH_KEY, UltraGridColumnStyle.Text, 250, false);
			UltraGridHelperColumn xcolLastWriteTime = new UltraGridHelperColumn(xgridDiffer, LAST_WRITE_TIME_KEY, LAST_WRITE_TIME_KEY, UltraGridColumnStyle.DateTime, false);
			UltraGridHelperColumn xcolCreationTime = new UltraGridHelperColumn(xgridDiffer, CREATION_TIME_KEY, CREATION_TIME_KEY, UltraGridColumnStyle.DateTime, false);
			UltraGridHelperColumn xcolFileSize = new UltraGridHelperColumn(xgridDiffer, FILE_SIZE_KEY, FILE_SIZE_KEY, UltraGridColumnStyle.Numeric, false);

			#region Summary
			UltraGridBand bandAmount = xgridDiffer.DisplayLayout.Bands[0];

			bandAmount.Summaries.Add(SELECT_KEY, SummaryType.Custom, new CustomSummaryCalculator(SELECT_KEY, true), bandAmount.Columns[SELECT_KEY], SummaryPosition.UseSummaryPositionColumn, null);
			bandAmount.Summaries.Add(SIZE_KEY, 
                SummaryType.Custom, 
                new CustomSummaryCalculator(FILE_SIZE_KEY, SELECT_KEY, true, CalculatorType.Sum), 
                bandAmount.Columns[FILE_SIZE_KEY], 
                SummaryPosition.UseSummaryPositionColumn, 
                null);
			bandAmount.Summaries.Add(COUNT, SummaryType.Count, bandAmount.Columns[FILENAME_KEY]);

			bandAmount.Summaries[SELECT_KEY].Appearance.TextHAlign = HAlign.Center;
			bandAmount.Summaries[SIZE_KEY].Appearance.TextHAlign = HAlign.Right;
			bandAmount.Summaries[COUNT].Appearance.TextHAlign = HAlign.Right;

			bandAmount.Summaries[SELECT_KEY].DisplayFormat = "{0}";
			bandAmount.Summaries[SIZE_KEY].DisplayFormat = string.Format("{{0:{0}}}{1}", "#,##0", "bytes");
			bandAmount.Summaries[COUNT].DisplayFormat = string.Format("{0} : {{0:{1}}}", COUNT, "#,##0");

			// Summary Position
			xgridDiffer.DisplayLayout.Override.SummaryDisplayArea = SummaryDisplayAreas.BottomFixed;
			#endregion
			
			xcolSel.Fixed = true;
			xcolFolderName.Fixed = true;
			xcolName.Fixed = true;
			xcolPath.SortIndicator = SortIndicator.Ascending;
			xgridDiffer.DisplayLayout.AutoFitStyle = AutoFitStyle.ResizeAllColumns;
			UltraGridHelper.SetGridOptions(xgridDiffer, false, false, false, true);
		}

		private void ClearSourceGrid()
		{
			UltraGridHelper.DataBind(xgridSrc, null);
			InitSourceGrid();
		}

		private void ClearDestGrid()
		{
			UltraGridHelper.DataBind(xgridDest, null);
			InitDestGrid();
		}

		private void ClearDifferGrid()
		{
			UltraGridHelper.DataBind(xgridDiffer, null);
			InitDifferGrid();
		}
		#endregion
	}
}