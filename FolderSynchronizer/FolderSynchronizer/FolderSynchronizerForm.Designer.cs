﻿namespace FolderSynchronizer
{
	partial class FolderSynchronizerForm
	{
		/// <summary>
		/// 필수 디자이너 변수입니다.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// 사용 중인 모든 리소스를 정리합니다.
		/// </summary>
		/// <param name="disposing">관리되는 리소스를 삭제해야 하면 true이고, 그렇지 않으면 false입니다.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form 디자이너에서 생성한 코드

		/// <summary>
		/// 디자이너 지원에 필요한 메서드입니다.
		/// 이 메서드의 내용을 코드 편집기로 수정하지 마십시오.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
			Infragistics.Win.Appearance appearance8 = new Infragistics.Win.Appearance();
			Infragistics.Win.Appearance appearance18 = new Infragistics.Win.Appearance();
			Infragistics.Win.Appearance appearance19 = new Infragistics.Win.Appearance();
			Infragistics.Win.Appearance appearance20 = new Infragistics.Win.Appearance();
			Infragistics.Win.Appearance appearance47 = new Infragistics.Win.Appearance();
			Infragistics.Win.Appearance appearance48 = new Infragistics.Win.Appearance();
			Infragistics.Win.Appearance appearance49 = new Infragistics.Win.Appearance();
			Infragistics.Win.Appearance appearance50 = new Infragistics.Win.Appearance();
			Infragistics.Win.Appearance appearance21 = new Infragistics.Win.Appearance();
			Infragistics.Win.Appearance appearance16 = new Infragistics.Win.Appearance();
			Infragistics.Win.Appearance appearance53 = new Infragistics.Win.Appearance();
			Infragistics.Win.Appearance appearance54 = new Infragistics.Win.Appearance();
			Infragistics.Win.Appearance appearance55 = new Infragistics.Win.Appearance();
			Infragistics.Win.Appearance appearance56 = new Infragistics.Win.Appearance();
			Infragistics.Win.Appearance appearance57 = new Infragistics.Win.Appearance();
			Infragistics.Win.Appearance appearance15 = new Infragistics.Win.Appearance();
			Infragistics.Win.UltraWinToolTip.UltraToolTipInfo ultraToolTipInfo1 = new Infragistics.Win.UltraWinToolTip.UltraToolTipInfo("Select exists in destination files except empty folders.", Infragistics.Win.ToolTipImage.Default, null, Infragistics.Win.DefaultableBoolean.Default);
			Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
			Infragistics.Win.UltraWinToolTip.UltraToolTipInfo ultraToolTipInfo2 = new Infragistics.Win.UltraWinToolTip.UltraToolTipInfo("Please click to  select destination folder.", Infragistics.Win.ToolTipImage.Default, null, Infragistics.Win.DefaultableBoolean.Default);
			Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
			Infragistics.Win.Appearance appearance9 = new Infragistics.Win.Appearance();
			Infragistics.Win.Appearance appearance22 = new Infragistics.Win.Appearance();
			Infragistics.Win.Appearance appearance23 = new Infragistics.Win.Appearance();
			Infragistics.Win.Appearance appearance24 = new Infragistics.Win.Appearance();
			Infragistics.Win.Appearance appearance31 = new Infragistics.Win.Appearance();
			Infragistics.Win.Appearance appearance32 = new Infragistics.Win.Appearance();
			Infragistics.Win.Appearance appearance33 = new Infragistics.Win.Appearance();
			Infragistics.Win.Appearance appearance34 = new Infragistics.Win.Appearance();
			Infragistics.Win.Appearance appearance25 = new Infragistics.Win.Appearance();
			Infragistics.Win.Appearance appearance17 = new Infragistics.Win.Appearance();
			Infragistics.Win.Appearance appearance37 = new Infragistics.Win.Appearance();
			Infragistics.Win.Appearance appearance38 = new Infragistics.Win.Appearance();
			Infragistics.Win.Appearance appearance39 = new Infragistics.Win.Appearance();
			Infragistics.Win.Appearance appearance40 = new Infragistics.Win.Appearance();
			Infragistics.Win.Appearance appearance41 = new Infragistics.Win.Appearance();
			Infragistics.Win.Appearance appearance14 = new Infragistics.Win.Appearance();
			Infragistics.Win.UltraWinToolTip.UltraToolTipInfo ultraToolTipInfo3 = new Infragistics.Win.UltraWinToolTip.UltraToolTipInfo("Select exists in source files except empty folders.", Infragistics.Win.ToolTipImage.Default, null, Infragistics.Win.DefaultableBoolean.Default);
			Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
			Infragistics.Win.UltraWinToolTip.UltraToolTipInfo ultraToolTipInfo4 = new Infragistics.Win.UltraWinToolTip.UltraToolTipInfo("Please click to  select source folder.", Infragistics.Win.ToolTipImage.Default, null, Infragistics.Win.DefaultableBoolean.Default);
			Infragistics.Win.Appearance appearance13 = new Infragistics.Win.Appearance();
			Infragistics.Win.UltraWinToolTip.UltraToolTipInfo ultraToolTipInfo5 = new Infragistics.Win.UltraWinToolTip.UltraToolTipInfo("Move destination folders or files to source.", Infragistics.Win.ToolTipImage.Default, null, Infragistics.Win.DefaultableBoolean.Default);
			Infragistics.Win.Appearance appearance12 = new Infragistics.Win.Appearance();
			Infragistics.Win.UltraWinToolTip.UltraToolTipInfo ultraToolTipInfo6 = new Infragistics.Win.UltraWinToolTip.UltraToolTipInfo("Copy destination folders or files to source.", Infragistics.Win.ToolTipImage.Default, null, Infragistics.Win.DefaultableBoolean.Default);
			Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
			Infragistics.Win.UltraWinToolTip.UltraToolTipInfo ultraToolTipInfo7 = new Infragistics.Win.UltraWinToolTip.UltraToolTipInfo("Compare destination folder with source folder.", Infragistics.Win.ToolTipImage.Default, null, Infragistics.Win.DefaultableBoolean.Default);
			Infragistics.Win.Appearance appearance10 = new Infragistics.Win.Appearance();
			Infragistics.Win.UltraWinToolTip.UltraToolTipInfo ultraToolTipInfo8 = new Infragistics.Win.UltraWinToolTip.UltraToolTipInfo("Move source folders or files to destination.", Infragistics.Win.ToolTipImage.Default, null, Infragistics.Win.DefaultableBoolean.Default);
			Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
			Infragistics.Win.UltraWinToolTip.UltraToolTipInfo ultraToolTipInfo9 = new Infragistics.Win.UltraWinToolTip.UltraToolTipInfo("Copy source folders or files to destination.", Infragistics.Win.ToolTipImage.Default, null, Infragistics.Win.DefaultableBoolean.Default);
			Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
			Infragistics.Win.UltraWinToolTip.UltraToolTipInfo ultraToolTipInfo10 = new Infragistics.Win.UltraWinToolTip.UltraToolTipInfo("Compare source folder with destination folder.", Infragistics.Win.ToolTipImage.Default, null, Infragistics.Win.DefaultableBoolean.Default);
			Infragistics.Win.Appearance appearance11 = new Infragistics.Win.Appearance();
			Infragistics.Win.UltraWinToolTip.UltraToolTipInfo ultraToolTipInfo11 = new Infragistics.Win.UltraWinToolTip.UltraToolTipInfo("Syncrhronize source and destination folders", Infragistics.Win.ToolTipImage.Default, null, Infragistics.Win.DefaultableBoolean.Default);
			Infragistics.Win.UltraWinStatusBar.UltraStatusPanel ultraStatusPanel1 = new Infragistics.Win.UltraWinStatusBar.UltraStatusPanel();
			Infragistics.Win.UltraWinStatusBar.UltraStatusPanel ultraStatusPanel2 = new Infragistics.Win.UltraWinStatusBar.UltraStatusPanel();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FolderSynchronizerForm));
			this.tlpMain = new System.Windows.Forms.TableLayoutPanel();
			this.xgrbDestination = new Infragistics.Win.Misc.UltraGroupBox();
			this.tlpDestination = new System.Windows.Forms.TableLayoutPanel();
			this.xgridDest = new Infragistics.Win.UltraWinGrid.UltraGrid();
			this.tlpDestFolder = new System.Windows.Forms.TableLayoutPanel();
			this.xbtnDest = new Infragistics.Win.Misc.UltraButton();
			this.xtxtDestFolder = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
			this.xgrbSource = new Infragistics.Win.Misc.UltraGroupBox();
			this.tlpSource = new System.Windows.Forms.TableLayoutPanel();
			this.xgridSrc = new Infragistics.Win.UltraWinGrid.UltraGrid();
			this.tlpSrcFolder = new System.Windows.Forms.TableLayoutPanel();
			this.xbtnSrc = new Infragistics.Win.Misc.UltraButton();
			this.xtxtSrcFolder = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
			this.tlpBottom = new System.Windows.Forms.TableLayoutPanel();
			this.xbtnMoveToSrc = new Infragistics.Win.Misc.UltraButton();
			this.xbtnCopyToSrc = new Infragistics.Win.Misc.UltraButton();
			this.xbtnCompareWithSrc = new Infragistics.Win.Misc.UltraButton();
			this.xbtnMoveToDesc = new Infragistics.Win.Misc.UltraButton();
			this.xbtnCopyToDest = new Infragistics.Win.Misc.UltraButton();
			this.xbtnCompareWithDest = new Infragistics.Win.Misc.UltraButton();
			this.xbtnSync = new Infragistics.Win.Misc.UltraButton();
			this.fbDialog = new System.Windows.Forms.FolderBrowserDialog();
			this.xToolTipManager = new Infragistics.Win.UltraWinToolTip.UltraToolTipManager(this.components);
			this.xStatusBar = new Infragistics.Win.UltraWinStatusBar.UltraStatusBar();
			this.tlpMain.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.xgrbDestination)).BeginInit();
			this.xgrbDestination.SuspendLayout();
			this.tlpDestination.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.xgridDest)).BeginInit();
			this.tlpDestFolder.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.xtxtDestFolder)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.xgrbSource)).BeginInit();
			this.xgrbSource.SuspendLayout();
			this.tlpSource.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.xgridSrc)).BeginInit();
			this.tlpSrcFolder.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.xtxtSrcFolder)).BeginInit();
			this.tlpBottom.SuspendLayout();
			this.SuspendLayout();
			// 
			// tlpMain
			// 
			this.tlpMain.BackColor = System.Drawing.Color.White;
			this.tlpMain.ColumnCount = 2;
			this.tlpMain.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
			this.tlpMain.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
			this.tlpMain.Controls.Add(this.xgrbDestination, 1, 0);
			this.tlpMain.Controls.Add(this.xgrbSource, 0, 0);
			this.tlpMain.Controls.Add(this.tlpBottom, 0, 1);
			this.tlpMain.Dock = System.Windows.Forms.DockStyle.Fill;
			this.tlpMain.Location = new System.Drawing.Point(0, 0);
			this.tlpMain.Name = "tlpMain";
			this.tlpMain.Padding = new System.Windows.Forms.Padding(0, 0, 0, 25);
			this.tlpMain.RowCount = 2;
			this.tlpMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
			this.tlpMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 32F));
			this.tlpMain.Size = new System.Drawing.Size(1012, 704);
			this.tlpMain.TabIndex = 0;
			// 
			// xgrbDestination
			// 
			appearance3.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(189)))), ((int)(((byte)(196)))), ((int)(((byte)(211)))));
			appearance3.TextVAlignAsString = "Middle";
			this.xgrbDestination.Appearance = appearance3;
			this.xgrbDestination.BackColorInternal = System.Drawing.Color.White;
			this.xgrbDestination.ContentPadding.Bottom = 2;
			this.xgrbDestination.ContentPadding.Left = 2;
			this.xgrbDestination.ContentPadding.Right = 2;
			this.xgrbDestination.ContentPadding.Top = 2;
			this.xgrbDestination.Controls.Add(this.tlpDestination);
			this.xgrbDestination.Dock = System.Windows.Forms.DockStyle.Fill;
			appearance8.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(185)))), ((int)(((byte)(187)))), ((int)(((byte)(204)))));
			appearance8.FontData.BoldAsString = "True";
			appearance8.ForeColor = System.Drawing.Color.Black;
			appearance8.ForegroundAlpha = Infragistics.Win.Alpha.Opaque;
			appearance8.TextVAlignAsString = "Top";
			this.xgrbDestination.HeaderAppearance = appearance8;
			this.xgrbDestination.HeaderBorderStyle = Infragistics.Win.UIElementBorderStyle.Rounded4;
			this.xgrbDestination.HeaderPosition = Infragistics.Win.Misc.GroupBoxHeaderPosition.TopInsideBorder;
			this.xgrbDestination.Location = new System.Drawing.Point(506, 3);
			this.xgrbDestination.Margin = new System.Windows.Forms.Padding(0, 3, 3, 0);
			this.xgrbDestination.Name = "xgrbDestination";
			this.xgrbDestination.Size = new System.Drawing.Size(503, 644);
			this.xgrbDestination.TabIndex = 1;
			this.xgrbDestination.Tag = "Compared Destination";
			this.xgrbDestination.Text = "Compared Destination";
			// 
			// tlpDestination
			// 
			this.tlpDestination.BackColor = System.Drawing.Color.White;
			this.tlpDestination.ColumnCount = 1;
			this.tlpDestination.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
			this.tlpDestination.Controls.Add(this.xgridDest, 0, 1);
			this.tlpDestination.Controls.Add(this.tlpDestFolder, 0, 0);
			this.tlpDestination.Dock = System.Windows.Forms.DockStyle.Fill;
			this.tlpDestination.Location = new System.Drawing.Point(5, 27);
			this.tlpDestination.Margin = new System.Windows.Forms.Padding(0);
			this.tlpDestination.Name = "tlpDestination";
			this.tlpDestination.RowCount = 2;
			this.tlpDestination.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 32F));
			this.tlpDestination.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
			this.tlpDestination.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
			this.tlpDestination.Size = new System.Drawing.Size(493, 612);
			this.tlpDestination.TabIndex = 1;
			// 
			// xgridDest
			// 
			appearance18.BackColor = System.Drawing.Color.White;
			appearance18.BorderColor = System.Drawing.Color.Gray;
			this.xgridDest.DisplayLayout.Appearance = appearance18;
			this.xgridDest.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
			this.xgridDest.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
			this.xgridDest.DisplayLayout.GroupByBox.Hidden = true;
			appearance19.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(174)))), ((int)(((byte)(176)))), ((int)(((byte)(198)))));
			appearance19.BorderColor = System.Drawing.Color.Orange;
			this.xgridDest.DisplayLayout.Override.ActiveRowAppearance = appearance19;
			this.xgridDest.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Solid;
			this.xgridDest.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Solid;
			this.xgridDest.DisplayLayout.Override.BorderStyleSpecialRowSeparator = Infragistics.Win.UIElementBorderStyle.None;
			this.xgridDest.DisplayLayout.Override.BorderStyleSummaryFooter = Infragistics.Win.UIElementBorderStyle.Solid;
			this.xgridDest.DisplayLayout.Override.BorderStyleSummaryValue = Infragistics.Win.UIElementBorderStyle.None;
			this.xgridDest.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
			this.xgridDest.DisplayLayout.Override.ColumnAutoSizeMode = Infragistics.Win.UltraWinGrid.ColumnAutoSizeMode.VisibleRows;
			this.xgridDest.DisplayLayout.Override.ColumnSizingArea = Infragistics.Win.UltraWinGrid.ColumnSizingArea.EntireColumn;
			this.xgridDest.DisplayLayout.Override.FixedCellSeparatorColor = System.Drawing.Color.OrangeRed;
			this.xgridDest.DisplayLayout.Override.FixedHeaderIndicator = Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.None;
			appearance20.TextHAlignAsString = "Center";
			appearance20.TextVAlignAsString = "Middle";
			this.xgridDest.DisplayLayout.Override.HeaderAppearance = appearance20;
			this.xgridDest.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
			this.xgridDest.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
			appearance47.BackColor = System.Drawing.Color.LightBlue;
			this.xgridDest.DisplayLayout.Override.HotTrackCellAppearance = appearance47;
			appearance48.BorderColor = System.Drawing.Color.Orange;
			this.xgridDest.DisplayLayout.Override.HotTrackRowAppearance = appearance48;
			appearance49.BackColor = System.Drawing.Color.LightBlue;
			this.xgridDest.DisplayLayout.Override.HotTrackRowCellAppearance = appearance49;
			appearance50.BackColor = System.Drawing.Color.Silver;
			this.xgridDest.DisplayLayout.Override.HotTrackRowSelectorAppearance = appearance50;
			appearance21.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(226)))), ((int)(((byte)(244)))));
			this.xgridDest.DisplayLayout.Override.RowAlternateAppearance = appearance21;
			appearance16.BackColor = System.Drawing.Color.White;
			this.xgridDest.DisplayLayout.Override.RowAppearance = appearance16;
			appearance53.TextHAlignAsString = "Center";
			appearance53.TextVAlignAsString = "Middle";
			this.xgridDest.DisplayLayout.Override.RowSelectorAppearance = appearance53;
			appearance54.TextHAlignAsString = "Center";
			appearance54.TextVAlignAsString = "Middle";
			this.xgridDest.DisplayLayout.Override.RowSelectorHeaderAppearance = appearance54;
			this.xgridDest.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
			this.xgridDest.DisplayLayout.Override.SpecialRowSeparator = Infragistics.Win.UltraWinGrid.SpecialRowSeparator.SummaryRow;
			appearance55.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(206)))), ((int)(((byte)(208)))), ((int)(((byte)(226)))));
			this.xgridDest.DisplayLayout.Override.SpecialRowSeparatorAppearance = appearance55;
			this.xgridDest.DisplayLayout.Override.SpecialRowSeparatorHeight = 3;
			appearance56.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(228)))), ((int)(((byte)(229)))), ((int)(((byte)(239)))));
			appearance56.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(189)))), ((int)(((byte)(196)))), ((int)(((byte)(211)))));
			this.xgridDest.DisplayLayout.Override.SummaryFooterAppearance = appearance56;
			this.xgridDest.DisplayLayout.Override.SummaryFooterCaptionVisible = Infragistics.Win.DefaultableBoolean.False;
			appearance57.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(228)))), ((int)(((byte)(229)))), ((int)(((byte)(239)))));
			appearance57.TextVAlignAsString = "Middle";
			this.xgridDest.DisplayLayout.Override.SummaryValueAppearance = appearance57;
			this.xgridDest.DisplayLayout.UseFixedHeaders = true;
			this.xgridDest.Dock = System.Windows.Forms.DockStyle.Fill;
			this.xgridDest.Location = new System.Drawing.Point(3, 35);
			this.xgridDest.Name = "xgridDest";
			this.xgridDest.Size = new System.Drawing.Size(487, 574);
			this.xgridDest.TabIndex = 0;
			this.xgridDest.TabStop = false;
			this.xgridDest.Text = "ultraGrid1";
			this.xgridDest.UpdateMode = Infragistics.Win.UltraWinGrid.UpdateMode.OnUpdate;
			this.xgridDest.UseOsThemes = Infragistics.Win.DefaultableBoolean.False;
			this.xgridDest.CellChange += new Infragistics.Win.UltraWinGrid.CellEventHandler(this.Grid_CellChange);
			// 
			// tlpDestFolder
			// 
			this.tlpDestFolder.BackColor = System.Drawing.Color.White;
			this.tlpDestFolder.ColumnCount = 2;
			this.tlpDestFolder.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
			this.tlpDestFolder.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 128F));
			this.tlpDestFolder.Controls.Add(this.xbtnDest, 1, 0);
			this.tlpDestFolder.Controls.Add(this.xtxtDestFolder, 0, 0);
			this.tlpDestFolder.Dock = System.Windows.Forms.DockStyle.Fill;
			this.tlpDestFolder.Location = new System.Drawing.Point(0, 0);
			this.tlpDestFolder.Margin = new System.Windows.Forms.Padding(0);
			this.tlpDestFolder.Name = "tlpDestFolder";
			this.tlpDestFolder.RowCount = 1;
			this.tlpDestFolder.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
			this.tlpDestFolder.Size = new System.Drawing.Size(493, 32);
			this.tlpDestFolder.TabIndex = 1;
			// 
			// xbtnDest
			// 
			this.xbtnDest.BackColorInternal = System.Drawing.Color.White;
			this.xbtnDest.ButtonStyle = Infragistics.Win.UIElementButtonStyle.WindowsXPCommandButton;
			this.xbtnDest.Dock = System.Windows.Forms.DockStyle.Fill;
			appearance15.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(204)))), ((int)(((byte)(222)))), ((int)(((byte)(214)))));
			this.xbtnDest.HotTrackAppearance = appearance15;
			this.xbtnDest.Location = new System.Drawing.Point(368, 2);
			this.xbtnDest.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
			this.xbtnDest.Name = "xbtnDest";
			this.xbtnDest.Size = new System.Drawing.Size(122, 28);
			this.xStatusBar.SetStatusBarText(this.xbtnDest, "Select exists in destination files except empty folders.");
			this.xbtnDest.TabIndex = 0;
			this.xbtnDest.Text = "Select &Destination";
			ultraToolTipInfo1.ToolTipText = "Select exists in destination files except empty folders.";
			this.xToolTipManager.SetUltraToolTip(this.xbtnDest, ultraToolTipInfo1);
			this.xbtnDest.UseOsThemes = Infragistics.Win.DefaultableBoolean.False;
			this.xbtnDest.Click += new System.EventHandler(this.xbtnDest_Click);
			// 
			// xtxtDestFolder
			// 
			appearance7.BackColor = System.Drawing.Color.White;
			appearance7.TextHAlignAsString = "Left";
			appearance7.TextVAlignAsString = "Middle";
			this.xtxtDestFolder.Appearance = appearance7;
			this.xtxtDestFolder.AutoSize = false;
			this.xtxtDestFolder.BackColor = System.Drawing.Color.White;
			this.xtxtDestFolder.BorderStyle = Infragistics.Win.UIElementBorderStyle.Rounded1;
			this.xtxtDestFolder.Dock = System.Windows.Forms.DockStyle.Fill;
			this.xtxtDestFolder.Location = new System.Drawing.Point(3, 3);
			this.xtxtDestFolder.Name = "xtxtDestFolder";
			this.xtxtDestFolder.Size = new System.Drawing.Size(359, 26);
			this.xStatusBar.SetStatusBarText(this.xtxtDestFolder, "Please click to  select destination folder.");
			this.xtxtDestFolder.TabIndex = 0;
			this.xtxtDestFolder.TabStop = false;
			ultraToolTipInfo2.ToolTipText = "Please click to  select destination folder.";
			this.xToolTipManager.SetUltraToolTip(this.xtxtDestFolder, ultraToolTipInfo2);
			this.xtxtDestFolder.UseOsThemes = Infragistics.Win.DefaultableBoolean.False;
			this.xtxtDestFolder.AfterEnterEditMode += new System.EventHandler(this.xtxtDestFolder_AfterEnterEditMode);
			// 
			// xgrbSource
			// 
			appearance2.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(189)))), ((int)(((byte)(196)))), ((int)(((byte)(211)))));
			appearance2.TextVAlignAsString = "Middle";
			this.xgrbSource.Appearance = appearance2;
			this.xgrbSource.BackColorInternal = System.Drawing.Color.White;
			this.xgrbSource.ContentPadding.Bottom = 2;
			this.xgrbSource.ContentPadding.Left = 2;
			this.xgrbSource.ContentPadding.Right = 2;
			this.xgrbSource.ContentPadding.Top = 2;
			this.xgrbSource.Controls.Add(this.tlpSource);
			this.xgrbSource.Dock = System.Windows.Forms.DockStyle.Fill;
			appearance9.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(185)))), ((int)(((byte)(187)))), ((int)(((byte)(204)))));
			appearance9.FontData.BoldAsString = "True";
			appearance9.ForeColor = System.Drawing.Color.Black;
			appearance9.ForegroundAlpha = Infragistics.Win.Alpha.Opaque;
			appearance9.TextVAlignAsString = "Top";
			this.xgrbSource.HeaderAppearance = appearance9;
			this.xgrbSource.HeaderBorderStyle = Infragistics.Win.UIElementBorderStyle.Rounded4;
			this.xgrbSource.HeaderPosition = Infragistics.Win.Misc.GroupBoxHeaderPosition.TopInsideBorder;
			this.xgrbSource.Location = new System.Drawing.Point(3, 3);
			this.xgrbSource.Margin = new System.Windows.Forms.Padding(3, 3, 0, 0);
			this.xgrbSource.Name = "xgrbSource";
			this.xgrbSource.Size = new System.Drawing.Size(503, 644);
			this.xgrbSource.TabIndex = 0;
			this.xgrbSource.Tag = "Compared Source";
			this.xgrbSource.Text = "Compared Source";
			// 
			// tlpSource
			// 
			this.tlpSource.BackColor = System.Drawing.Color.White;
			this.tlpSource.ColumnCount = 1;
			this.tlpSource.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
			this.tlpSource.Controls.Add(this.xgridSrc, 0, 1);
			this.tlpSource.Controls.Add(this.tlpSrcFolder, 0, 0);
			this.tlpSource.Dock = System.Windows.Forms.DockStyle.Fill;
			this.tlpSource.Location = new System.Drawing.Point(5, 27);
			this.tlpSource.Margin = new System.Windows.Forms.Padding(0);
			this.tlpSource.Name = "tlpSource";
			this.tlpSource.RowCount = 2;
			this.tlpSource.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 32F));
			this.tlpSource.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
			this.tlpSource.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
			this.tlpSource.Size = new System.Drawing.Size(493, 612);
			this.tlpSource.TabIndex = 0;
			// 
			// xgridSrc
			// 
			appearance22.BackColor = System.Drawing.Color.White;
			appearance22.BorderColor = System.Drawing.Color.Gray;
			this.xgridSrc.DisplayLayout.Appearance = appearance22;
			this.xgridSrc.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
			this.xgridSrc.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
			this.xgridSrc.DisplayLayout.GroupByBox.Hidden = true;
			appearance23.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(174)))), ((int)(((byte)(176)))), ((int)(((byte)(198)))));
			appearance23.BorderColor = System.Drawing.Color.Orange;
			this.xgridSrc.DisplayLayout.Override.ActiveRowAppearance = appearance23;
			this.xgridSrc.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Solid;
			this.xgridSrc.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Solid;
			this.xgridSrc.DisplayLayout.Override.BorderStyleSpecialRowSeparator = Infragistics.Win.UIElementBorderStyle.None;
			this.xgridSrc.DisplayLayout.Override.BorderStyleSummaryFooter = Infragistics.Win.UIElementBorderStyle.Solid;
			this.xgridSrc.DisplayLayout.Override.BorderStyleSummaryValue = Infragistics.Win.UIElementBorderStyle.None;
			this.xgridSrc.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
			this.xgridSrc.DisplayLayout.Override.ColumnAutoSizeMode = Infragistics.Win.UltraWinGrid.ColumnAutoSizeMode.VisibleRows;
			this.xgridSrc.DisplayLayout.Override.ColumnSizingArea = Infragistics.Win.UltraWinGrid.ColumnSizingArea.EntireColumn;
			this.xgridSrc.DisplayLayout.Override.FixedCellSeparatorColor = System.Drawing.Color.OrangeRed;
			this.xgridSrc.DisplayLayout.Override.FixedHeaderIndicator = Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.None;
			appearance24.TextHAlignAsString = "Center";
			appearance24.TextVAlignAsString = "Middle";
			this.xgridSrc.DisplayLayout.Override.HeaderAppearance = appearance24;
			this.xgridSrc.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
			this.xgridSrc.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
			appearance31.BackColor = System.Drawing.Color.LightBlue;
			this.xgridSrc.DisplayLayout.Override.HotTrackCellAppearance = appearance31;
			appearance32.BorderColor = System.Drawing.Color.Orange;
			this.xgridSrc.DisplayLayout.Override.HotTrackRowAppearance = appearance32;
			appearance33.BackColor = System.Drawing.Color.LightBlue;
			this.xgridSrc.DisplayLayout.Override.HotTrackRowCellAppearance = appearance33;
			appearance34.BackColor = System.Drawing.Color.Silver;
			this.xgridSrc.DisplayLayout.Override.HotTrackRowSelectorAppearance = appearance34;
			appearance25.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(226)))), ((int)(((byte)(244)))));
			this.xgridSrc.DisplayLayout.Override.RowAlternateAppearance = appearance25;
			appearance17.BackColor = System.Drawing.Color.White;
			this.xgridSrc.DisplayLayout.Override.RowAppearance = appearance17;
			appearance37.TextHAlignAsString = "Center";
			appearance37.TextVAlignAsString = "Middle";
			this.xgridSrc.DisplayLayout.Override.RowSelectorAppearance = appearance37;
			appearance38.TextHAlignAsString = "Center";
			appearance38.TextVAlignAsString = "Middle";
			this.xgridSrc.DisplayLayout.Override.RowSelectorHeaderAppearance = appearance38;
			this.xgridSrc.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
			this.xgridSrc.DisplayLayout.Override.SpecialRowSeparator = Infragistics.Win.UltraWinGrid.SpecialRowSeparator.SummaryRow;
			appearance39.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(206)))), ((int)(((byte)(208)))), ((int)(((byte)(226)))));
			this.xgridSrc.DisplayLayout.Override.SpecialRowSeparatorAppearance = appearance39;
			this.xgridSrc.DisplayLayout.Override.SpecialRowSeparatorHeight = 3;
			appearance40.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(228)))), ((int)(((byte)(229)))), ((int)(((byte)(239)))));
			appearance40.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(189)))), ((int)(((byte)(196)))), ((int)(((byte)(211)))));
			this.xgridSrc.DisplayLayout.Override.SummaryFooterAppearance = appearance40;
			this.xgridSrc.DisplayLayout.Override.SummaryFooterCaptionVisible = Infragistics.Win.DefaultableBoolean.False;
			appearance41.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(228)))), ((int)(((byte)(229)))), ((int)(((byte)(239)))));
			appearance41.TextVAlignAsString = "Middle";
			this.xgridSrc.DisplayLayout.Override.SummaryValueAppearance = appearance41;
			this.xgridSrc.DisplayLayout.UseFixedHeaders = true;
			this.xgridSrc.Dock = System.Windows.Forms.DockStyle.Fill;
			this.xgridSrc.Location = new System.Drawing.Point(3, 35);
			this.xgridSrc.Name = "xgridSrc";
			this.xgridSrc.Size = new System.Drawing.Size(487, 574);
			this.xgridSrc.TabIndex = 0;
			this.xgridSrc.TabStop = false;
			this.xgridSrc.Text = "ultraGrid1";
			this.xgridSrc.UpdateMode = Infragistics.Win.UltraWinGrid.UpdateMode.OnUpdate;
			this.xgridSrc.UseOsThemes = Infragistics.Win.DefaultableBoolean.False;
			this.xgridSrc.CellChange += new Infragistics.Win.UltraWinGrid.CellEventHandler(this.Grid_CellChange);
			// 
			// tlpSrcFolder
			// 
			this.tlpSrcFolder.BackColor = System.Drawing.Color.White;
			this.tlpSrcFolder.ColumnCount = 2;
			this.tlpSrcFolder.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
			this.tlpSrcFolder.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 128F));
			this.tlpSrcFolder.Controls.Add(this.xbtnSrc, 1, 0);
			this.tlpSrcFolder.Controls.Add(this.xtxtSrcFolder, 0, 0);
			this.tlpSrcFolder.Dock = System.Windows.Forms.DockStyle.Fill;
			this.tlpSrcFolder.Location = new System.Drawing.Point(0, 0);
			this.tlpSrcFolder.Margin = new System.Windows.Forms.Padding(0);
			this.tlpSrcFolder.Name = "tlpSrcFolder";
			this.tlpSrcFolder.RowCount = 1;
			this.tlpSrcFolder.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
			this.tlpSrcFolder.Size = new System.Drawing.Size(493, 32);
			this.tlpSrcFolder.TabIndex = 1;
			// 
			// xbtnSrc
			// 
			this.xbtnSrc.BackColorInternal = System.Drawing.Color.White;
			this.xbtnSrc.ButtonStyle = Infragistics.Win.UIElementButtonStyle.WindowsXPCommandButton;
			this.xbtnSrc.Dock = System.Windows.Forms.DockStyle.Fill;
			appearance14.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(204)))), ((int)(((byte)(222)))), ((int)(((byte)(214)))));
			this.xbtnSrc.HotTrackAppearance = appearance14;
			this.xbtnSrc.Location = new System.Drawing.Point(368, 2);
			this.xbtnSrc.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
			this.xbtnSrc.Name = "xbtnSrc";
			this.xbtnSrc.Size = new System.Drawing.Size(122, 28);
			this.xStatusBar.SetStatusBarText(this.xbtnSrc, "Select exists in source files except empty folders.");
			this.xbtnSrc.TabIndex = 0;
			this.xbtnSrc.Text = "Select &Source";
			ultraToolTipInfo3.ToolTipText = "Select exists in source files except empty folders.";
			this.xToolTipManager.SetUltraToolTip(this.xbtnSrc, ultraToolTipInfo3);
			this.xbtnSrc.UseOsThemes = Infragistics.Win.DefaultableBoolean.False;
			this.xbtnSrc.Click += new System.EventHandler(this.xbtnSrc_Click);
			// 
			// xtxtSrcFolder
			// 
			appearance6.BackColor = System.Drawing.Color.White;
			appearance6.TextHAlignAsString = "Left";
			appearance6.TextVAlignAsString = "Middle";
			this.xtxtSrcFolder.Appearance = appearance6;
			this.xtxtSrcFolder.AutoSize = false;
			this.xtxtSrcFolder.BackColor = System.Drawing.Color.White;
			this.xtxtSrcFolder.BorderStyle = Infragistics.Win.UIElementBorderStyle.Rounded1;
			this.xtxtSrcFolder.Dock = System.Windows.Forms.DockStyle.Fill;
			this.xtxtSrcFolder.Location = new System.Drawing.Point(3, 3);
			this.xtxtSrcFolder.Name = "xtxtSrcFolder";
			this.xtxtSrcFolder.Size = new System.Drawing.Size(359, 26);
			this.xStatusBar.SetStatusBarText(this.xtxtSrcFolder, "Please click to  select source folder.");
			this.xtxtSrcFolder.TabIndex = 0;
			this.xtxtSrcFolder.TabStop = false;
			ultraToolTipInfo4.ToolTipText = "Please click to  select source folder.";
			this.xToolTipManager.SetUltraToolTip(this.xtxtSrcFolder, ultraToolTipInfo4);
			this.xtxtSrcFolder.UseOsThemes = Infragistics.Win.DefaultableBoolean.False;
			this.xtxtSrcFolder.AfterEnterEditMode += new System.EventHandler(this.xtxtSrcFolder_AfterEnterEditMode);
			// 
			// tlpBottom
			// 
			this.tlpBottom.BackColor = System.Drawing.Color.White;
			this.tlpBottom.ColumnCount = 7;
			this.tlpMain.SetColumnSpan(this.tlpBottom, 2);
			this.tlpBottom.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 13.04348F));
			this.tlpBottom.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 13.04348F));
			this.tlpBottom.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 13.04348F));
			this.tlpBottom.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 21.73913F));
			this.tlpBottom.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 13.04348F));
			this.tlpBottom.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 13.04348F));
			this.tlpBottom.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 13.04348F));
			this.tlpBottom.Controls.Add(this.xbtnMoveToSrc, 6, 0);
			this.tlpBottom.Controls.Add(this.xbtnCopyToSrc, 5, 0);
			this.tlpBottom.Controls.Add(this.xbtnCompareWithSrc, 4, 0);
			this.tlpBottom.Controls.Add(this.xbtnMoveToDesc, 2, 0);
			this.tlpBottom.Controls.Add(this.xbtnCopyToDest, 1, 0);
			this.tlpBottom.Controls.Add(this.xbtnCompareWithDest, 0, 0);
			this.tlpBottom.Controls.Add(this.xbtnSync, 3, 0);
			this.tlpBottom.Dock = System.Windows.Forms.DockStyle.Fill;
			this.tlpBottom.Location = new System.Drawing.Point(3, 647);
			this.tlpBottom.Margin = new System.Windows.Forms.Padding(3, 0, 3, 0);
			this.tlpBottom.Name = "tlpBottom";
			this.tlpBottom.RowCount = 1;
			this.tlpBottom.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
			this.tlpBottom.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 32F));
			this.tlpBottom.Size = new System.Drawing.Size(1006, 32);
			this.tlpBottom.TabIndex = 2;
			// 
			// xbtnMoveToSrc
			// 
			this.xbtnMoveToSrc.BackColorInternal = System.Drawing.Color.White;
			this.xbtnMoveToSrc.ButtonStyle = Infragistics.Win.UIElementButtonStyle.WindowsXPCommandButton;
			appearance13.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(204)))), ((int)(((byte)(222)))), ((int)(((byte)(214)))));
			this.xbtnMoveToSrc.HotTrackAppearance = appearance13;
			this.xbtnMoveToSrc.Location = new System.Drawing.Point(879, 3);
			this.xbtnMoveToSrc.Margin = new System.Windows.Forms.Padding(6, 3, 6, 3);
			this.xbtnMoveToSrc.Name = "xbtnMoveToSrc";
			this.xbtnMoveToSrc.Size = new System.Drawing.Size(121, 26);
			this.xStatusBar.SetStatusBarText(this.xbtnMoveToSrc, "Move destination folders or files to source.");
			this.xbtnMoveToSrc.TabIndex = 6;
			this.xbtnMoveToSrc.Text = "<- Mo&ve";
			ultraToolTipInfo5.ToolTipText = "Move destination folders or files to source.";
			this.xToolTipManager.SetUltraToolTip(this.xbtnMoveToSrc, ultraToolTipInfo5);
			this.xbtnMoveToSrc.UseOsThemes = Infragistics.Win.DefaultableBoolean.False;
			this.xbtnMoveToSrc.Click += new System.EventHandler(this.xbtnMoveToSrc_Click);
			// 
			// xbtnCopyToSrc
			// 
			this.xbtnCopyToSrc.BackColorInternal = System.Drawing.Color.White;
			this.xbtnCopyToSrc.ButtonStyle = Infragistics.Win.UIElementButtonStyle.WindowsXPCommandButton;
			appearance12.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(204)))), ((int)(((byte)(222)))), ((int)(((byte)(214)))));
			this.xbtnCopyToSrc.HotTrackAppearance = appearance12;
			this.xbtnCopyToSrc.Location = new System.Drawing.Point(748, 3);
			this.xbtnCopyToSrc.Margin = new System.Windows.Forms.Padding(6, 3, 6, 3);
			this.xbtnCopyToSrc.Name = "xbtnCopyToSrc";
			this.xbtnCopyToSrc.Size = new System.Drawing.Size(119, 26);
			this.xStatusBar.SetStatusBarText(this.xbtnCopyToSrc, "Copy destination folders or files to source.");
			this.xbtnCopyToSrc.TabIndex = 5;
			this.xbtnCopyToSrc.Text = "<- Co&py";
			ultraToolTipInfo6.ToolTipText = "Copy destination folders or files to source.";
			this.xToolTipManager.SetUltraToolTip(this.xbtnCopyToSrc, ultraToolTipInfo6);
			this.xbtnCopyToSrc.UseOsThemes = Infragistics.Win.DefaultableBoolean.False;
			this.xbtnCopyToSrc.Click += new System.EventHandler(this.xbtnCopyToSrc_Click);
			// 
			// xbtnCompareWithSrc
			// 
			this.xbtnCompareWithSrc.BackColorInternal = System.Drawing.Color.White;
			this.xbtnCompareWithSrc.ButtonStyle = Infragistics.Win.UIElementButtonStyle.WindowsXPCommandButton;
			appearance1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(204)))), ((int)(((byte)(222)))), ((int)(((byte)(214)))));
			this.xbtnCompareWithSrc.HotTrackAppearance = appearance1;
			this.xbtnCompareWithSrc.Location = new System.Drawing.Point(614, 3);
			this.xbtnCompareWithSrc.Margin = new System.Windows.Forms.Padding(3, 3, 6, 3);
			this.xbtnCompareWithSrc.Name = "xbtnCompareWithSrc";
			this.xbtnCompareWithSrc.Size = new System.Drawing.Size(122, 26);
			this.xStatusBar.SetStatusBarText(this.xbtnCompareWithSrc, "Compare destination folder with source folder.");
			this.xbtnCompareWithSrc.TabIndex = 4;
			this.xbtnCompareWithSrc.Text = "&<- Compare";
			ultraToolTipInfo7.ToolTipText = "Compare destination folder with source folder.";
			this.xToolTipManager.SetUltraToolTip(this.xbtnCompareWithSrc, ultraToolTipInfo7);
			this.xbtnCompareWithSrc.UseOsThemes = Infragistics.Win.DefaultableBoolean.False;
			this.xbtnCompareWithSrc.Click += new System.EventHandler(this.xbtnCompareWithSrc_Click);
			// 
			// xbtnMoveToDesc
			// 
			this.xbtnMoveToDesc.BackColorInternal = System.Drawing.Color.White;
			this.xbtnMoveToDesc.ButtonStyle = Infragistics.Win.UIElementButtonStyle.WindowsXPCommandButton;
			this.xbtnMoveToDesc.Dock = System.Windows.Forms.DockStyle.Fill;
			appearance10.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(204)))), ((int)(((byte)(222)))), ((int)(((byte)(214)))));
			this.xbtnMoveToDesc.HotTrackAppearance = appearance10;
			this.xbtnMoveToDesc.Location = new System.Drawing.Point(268, 3);
			this.xbtnMoveToDesc.Margin = new System.Windows.Forms.Padding(6, 3, 3, 3);
			this.xbtnMoveToDesc.Name = "xbtnMoveToDesc";
			this.xbtnMoveToDesc.Size = new System.Drawing.Size(122, 26);
			this.xStatusBar.SetStatusBarText(this.xbtnMoveToDesc, "Move source folders or files to destination.");
			this.xbtnMoveToDesc.TabIndex = 2;
			this.xbtnMoveToDesc.Text = "&Move ->";
			ultraToolTipInfo8.ToolTipText = "Move source folders or files to destination.";
			this.xToolTipManager.SetUltraToolTip(this.xbtnMoveToDesc, ultraToolTipInfo8);
			this.xbtnMoveToDesc.UseOsThemes = Infragistics.Win.DefaultableBoolean.False;
			this.xbtnMoveToDesc.Click += new System.EventHandler(this.xbtnMoveToDesc_Click);
			// 
			// xbtnCopyToDest
			// 
			this.xbtnCopyToDest.BackColorInternal = System.Drawing.Color.White;
			this.xbtnCopyToDest.ButtonStyle = Infragistics.Win.UIElementButtonStyle.WindowsXPCommandButton;
			this.xbtnCopyToDest.Dock = System.Windows.Forms.DockStyle.Fill;
			appearance5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(204)))), ((int)(((byte)(222)))), ((int)(((byte)(214)))));
			this.xbtnCopyToDest.HotTrackAppearance = appearance5;
			this.xbtnCopyToDest.Location = new System.Drawing.Point(137, 3);
			this.xbtnCopyToDest.Margin = new System.Windows.Forms.Padding(6, 3, 6, 3);
			this.xbtnCopyToDest.Name = "xbtnCopyToDest";
			this.xbtnCopyToDest.Size = new System.Drawing.Size(119, 26);
			this.xStatusBar.SetStatusBarText(this.xbtnCopyToDest, "Copy source folders or files to destination.");
			this.xbtnCopyToDest.TabIndex = 1;
			this.xbtnCopyToDest.Text = "&Copy ->";
			ultraToolTipInfo9.ToolTipText = "Copy source folders or files to destination.";
			this.xToolTipManager.SetUltraToolTip(this.xbtnCopyToDest, ultraToolTipInfo9);
			this.xbtnCopyToDest.UseOsThemes = Infragistics.Win.DefaultableBoolean.False;
			this.xbtnCopyToDest.Click += new System.EventHandler(this.xbtnCopyToDest_Click);
			// 
			// xbtnCompareWithDest
			// 
			this.xbtnCompareWithDest.BackColorInternal = System.Drawing.Color.White;
			this.xbtnCompareWithDest.ButtonStyle = Infragistics.Win.UIElementButtonStyle.WindowsXPCommandButton;
			this.xbtnCompareWithDest.Dock = System.Windows.Forms.DockStyle.Fill;
			appearance4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(204)))), ((int)(((byte)(222)))), ((int)(((byte)(214)))));
			this.xbtnCompareWithDest.HotTrackAppearance = appearance4;
			this.xbtnCompareWithDest.Location = new System.Drawing.Point(6, 3);
			this.xbtnCompareWithDest.Margin = new System.Windows.Forms.Padding(6, 3, 6, 3);
			this.xbtnCompareWithDest.Name = "xbtnCompareWithDest";
			this.xbtnCompareWithDest.Size = new System.Drawing.Size(119, 26);
			this.xStatusBar.SetStatusBarText(this.xbtnCompareWithDest, "Compare source folder with destination folder.");
			this.xbtnCompareWithDest.TabIndex = 0;
			this.xbtnCompareWithDest.Text = "Compare -&>";
			ultraToolTipInfo10.ToolTipText = "Compare source folder with destination folder.";
			this.xToolTipManager.SetUltraToolTip(this.xbtnCompareWithDest, ultraToolTipInfo10);
			this.xbtnCompareWithDest.UseOsThemes = Infragistics.Win.DefaultableBoolean.False;
			this.xbtnCompareWithDest.Click += new System.EventHandler(this.xbtnCompareWithDest_Click);
			// 
			// xbtnSync
			// 
			this.xbtnSync.BackColorInternal = System.Drawing.Color.White;
			this.xbtnSync.ButtonStyle = Infragistics.Win.UIElementButtonStyle.WindowsXPCommandButton;
			this.xbtnSync.Dock = System.Windows.Forms.DockStyle.Fill;
			appearance11.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(204)))), ((int)(((byte)(222)))), ((int)(((byte)(214)))));
			this.xbtnSync.HotTrackAppearance = appearance11;
			this.xbtnSync.ImageSize = new System.Drawing.Size(32, 32);
			this.xbtnSync.Location = new System.Drawing.Point(399, 3);
			this.xbtnSync.Margin = new System.Windows.Forms.Padding(6, 3, 6, 3);
			this.xbtnSync.Name = "xbtnSync";
			this.xbtnSync.Size = new System.Drawing.Size(206, 26);
			this.xStatusBar.SetStatusBarText(this.xbtnSync, "Syncrhronize source and destination folders");
			this.xbtnSync.TabIndex = 3;
			this.xbtnSync.Text = "<- Synchronize ->";
			ultraToolTipInfo11.ToolTipText = "Syncrhronize source and destination folders";
			this.xToolTipManager.SetUltraToolTip(this.xbtnSync, ultraToolTipInfo11);
			this.xbtnSync.UseOsThemes = Infragistics.Win.DefaultableBoolean.False;
			this.xbtnSync.Click += new System.EventHandler(this.xbtnSync_Click);
			// 
			// fbDialog
			// 
			this.fbDialog.RootFolder = System.Environment.SpecialFolder.MyComputer;
			// 
			// xToolTipManager
			// 
			this.xToolTipManager.ContainingControl = this;
			// 
			// xStatusBar
			// 
			this.xStatusBar.BackColor = System.Drawing.Color.White;
			this.xStatusBar.Location = new System.Drawing.Point(0, 681);
			this.xStatusBar.Name = "xStatusBar";
			ultraStatusPanel1.SizingMode = Infragistics.Win.UltraWinStatusBar.PanelSizingMode.Spring;
			ultraStatusPanel1.Style = Infragistics.Win.UltraWinStatusBar.PanelStyle.AutoStatusText;
			ultraStatusPanel2.Style = Infragistics.Win.UltraWinStatusBar.PanelStyle.Progress;
			ultraStatusPanel2.Width = 200;
			this.xStatusBar.Panels.AddRange(new Infragistics.Win.UltraWinStatusBar.UltraStatusPanel[] {
            ultraStatusPanel1,
            ultraStatusPanel2});
			this.xStatusBar.Size = new System.Drawing.Size(1012, 23);
			this.xStatusBar.TabIndex = 7;
			// 
			// FolderSynchronizerForm
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(1012, 704);
			this.Controls.Add(this.xStatusBar);
			this.Controls.Add(this.tlpMain);
			this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
			this.Name = "FolderSynchronizerForm";
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Text = "Folder Synchronizer";
			this.tlpMain.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.xgrbDestination)).EndInit();
			this.xgrbDestination.ResumeLayout(false);
			this.tlpDestination.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.xgridDest)).EndInit();
			this.tlpDestFolder.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.xtxtDestFolder)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.xgrbSource)).EndInit();
			this.xgrbSource.ResumeLayout(false);
			this.tlpSource.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.xgridSrc)).EndInit();
			this.tlpSrcFolder.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.xtxtSrcFolder)).EndInit();
			this.tlpBottom.ResumeLayout(false);
			this.ResumeLayout(false);

		}

		#endregion

		private System.Windows.Forms.TableLayoutPanel tlpMain;
		private System.Windows.Forms.FolderBrowserDialog fbDialog;
		private Infragistics.Win.UltraWinGrid.UltraGrid xgridSrc;
		private Infragistics.Win.Misc.UltraButton xbtnSrc;
		private Infragistics.Win.Misc.UltraButton xbtnDest;
		private Infragistics.Win.UltraWinEditors.UltraTextEditor xtxtSrcFolder;
		private Infragistics.Win.UltraWinEditors.UltraTextEditor xtxtDestFolder;
		private Infragistics.Win.UltraWinGrid.UltraGrid xgridDest;
		private Infragistics.Win.Misc.UltraButton xbtnCompareWithDest;
		private Infragistics.Win.Misc.UltraButton xbtnMoveToSrc;
		private Infragistics.Win.Misc.UltraButton xbtnCopyToSrc;
		private Infragistics.Win.Misc.UltraButton xbtnCompareWithSrc;
		private Infragistics.Win.Misc.UltraButton xbtnMoveToDesc;
		private Infragistics.Win.Misc.UltraButton xbtnCopyToDest;
		private Infragistics.Win.Misc.UltraGroupBox xgrbSource;
		private Infragistics.Win.Misc.UltraGroupBox xgrbDestination;
		private System.Windows.Forms.TableLayoutPanel tlpSource;
		private System.Windows.Forms.TableLayoutPanel tlpDestination;
		private System.Windows.Forms.TableLayoutPanel tlpDestFolder;
		private System.Windows.Forms.TableLayoutPanel tlpSrcFolder;
		private Infragistics.Win.UltraWinToolTip.UltraToolTipManager xToolTipManager;
		private Infragistics.Win.UltraWinStatusBar.UltraStatusBar xStatusBar;
		private System.Windows.Forms.TableLayoutPanel tlpBottom;
		private Infragistics.Win.Misc.UltraButton xbtnSync;
	}
}

