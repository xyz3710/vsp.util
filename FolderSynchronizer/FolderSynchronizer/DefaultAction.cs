﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FolderSynchronizer
{
	internal enum DefaultAction
	{
		/// <summary>
		/// None.
		/// </summary>
		None,
		/// <summary>
		/// Synchronize souce and destination folder.
		/// </summary>
		Synchronize,
		/// <summary>
		/// Copy source to destination folder.
		/// </summary>
		CopySourceToDestination,
		/// <summary>
		/// Copy destination to source folder.
		/// </summary>
		CopyDestinationToSource,
		/// <summary>
		/// Move source to destination folder.
		/// </summary>
		MoveSourceToDestination,
		/// <summary>
		/// Move destination to source folder.
		/// </summary>
		MoveDestinationToSource,
	}
}
