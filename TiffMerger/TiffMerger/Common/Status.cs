/**********************************************************************************************************************/
/*	Name		:	TiffMerger.Common.Status
/*	Purpose		:	Column 상태를 나타냅니다.
/*	Creator		:	Kim Ki Won
/*	Create		:	2006년 10월 4일 수요일 오전 9:42:38
/*	Modifier	:	
/*	Update		:	2006년 10월 4일 수요일
/*	Comment		:	
/**********************************************************************************************************************/

using System;

namespace TiffMerger.Common
{
	/// <summary>
	/// Column 상태를 나타냅니다.
	/// </summary>
	public enum Status
	{
		/// <summary>
		/// Ready
		/// </summary>
		Ready,
		/// <summary>
		/// OK
		/// </summary>
		OK,
		/// <summary>
		/// Miss
		/// </summary>
		Miss,
		/// <summary>
		/// Error
		/// </summary>
		Error,
	}
}
