﻿/**********************************************************************************************************************/
/*	Name		:	TiffMerger.Common.Columns
/*	Purpose		:	Excel DataTable에 대한 Column명을 정의 합니다.
/*	Creator		:	Kim Ki Won
/*	Create		:	2006년 9월 29일 금요일 오전 2:03:43
/*	Modifier	:	
/*	Update		:	2006년 9월 29일 금요일
/*	Comment		:	
/**********************************************************************************************************************/

using System;
using System.Text;

namespace TiffMerger.Common
{
	public enum Columns
	{
		/// <summary>
		/// No.
		/// </summary>
		No = 0,
		/// <summary>
		/// 상태
		/// </summary>
		State,
		/// <summary>
		/// 철ID
		/// </summary>
		Sheaf,
		/// <summary>
		/// 권 수량
		/// </summary>
		BookSum,
		/// <summary>
		/// 시작 페이지
		/// </summary>
		StartPage,
		/// <summary>
		/// Merge될 page 수
		/// </summary>
		Pages,
		/// <summary>
		/// 해당권의 Page 합
		/// </summary>
		PageSum,
		/// <summary>
		/// Merge될 파일이름
		/// </summary>
		Filename,
		/// <summary>
		/// Source 경로
		/// </summary>
		SourcePath,
		/// <summary>
		/// Target 파일 경로(파일 이름 포함)
		/// </summary>
		TargetFilePath,
	}
}
