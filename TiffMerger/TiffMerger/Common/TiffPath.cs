/**********************************************************************************************************************/
/*	Name		:	TiffMerger.TiffPath
/*	Purpose		:	
/*	Creator		:	Kim Ki Won
/*	Create		:	2006년 9월 28일 목요일 오전 12:47:07
/*	Modifier	:	
/*	Update		:	2006년 9월 28일 목요일
/*	Comment		:	
/**********************************************************************************************************************/

using System;

using TiffMerger.Excel;

namespace TiffMerger.Common
{
	/// <summary>
	/// 입력된 값으로 Tif의 경로를 구합니다.
	/// </summary>
	public class TiffPath
	{
		#region Members
		private string _sheaf;
		private string _year;
		private string _roll;
		private string _minRoll;
		private string _maxRoll;
		private string _bookNo;
		private string _bookSubNo;
		private string _filename;
		#endregion

		#region Constructor
		/// <summary>
		/// Initializes a new instance of the TiffPath class.
		/// </summary>
		public TiffPath()
		{
		}

        /// <summary>
		/// Initializes a new instance of the TiffPath class.
		/// </summary>
		/// <param name="sheaf">철ID</param>
		/// <param name="bookSubNo">sub 권번호</param>
		/// <param name="filename">파일 이름(일반적으로 0001.tif)</param>
		/// <param name="excelFilename">엑셀 파일 이름(전체 Roll 번호를 구하기 위해서)</param>
		public TiffPath(TiffPathInfo pathInfo)
			: this(pathInfo.Sheaf, pathInfo.MinRoll, pathInfo.MaxRoll, pathInfo.BookSubNo, pathInfo.FileName)
		{
		}
		
		/// <summary>
		/// Initializes a new instance of the TiffPath class.
		/// </summary>
		/// <param name="sheaf">철ID</param>
		/// <param name="minRoll">minRoll</param>
		/// <param name="maxRoll">maxRoll</param>
		/// <param name="bookSubNo">sub 권번호</param>
		/// <param name="filename">파일 이름(일반적으로 0001.tif)</param>
		public TiffPath(string sheaf, string minRoll, string maxRoll, string bookSubNo, string filename)
		{
			_sheaf = sheaf;
			_minRoll = minRoll;
			_maxRoll = maxRoll;
			_bookSubNo = bookSubNo;
			_filename = filename;

			_roll = string.Empty;
			_bookNo = string.Empty;
		}
		#endregion
		
		#region Properties
		/// <summary>
		/// 철ID를 구하거나 설정합니다.
		/// </summary>
		public String Sheaf
		{
			get
			{
				return _sheaf;
			}
			set
            {
            	_sheaf = value;
            }
		}
        
		/// <summary>
		/// 년도를 구하거나 설정합니다.
		/// </summary>
		public String Year
		{
			get
			{
				_year = _sheaf.Substring(0, 4);

				return _year;
			}
			set
            {
            	_year = value;
            }
		}
        
		/// <summary>
		/// Roll 번호를 구하거나 설정합니다.
		/// </summary>
		public String Roll
		{
			get
			{
				_roll = _sheaf.Substring(4, 4);

				return _roll;
			}
			set
            {
            	_roll = value;
            }
		}
        
		/// <summary>
		/// 최대 Roll 번호를 구하거나 설정합니다.
		/// </summary>
		public String MaxRoll
		{
			get
			{
				return _maxRoll;
			}
			set
            {
            	_maxRoll = value;
            }
		}
        
		/// <summary>
		/// 최소 Roll 번호를 구하거나 설정합니다.
		/// </summary>
		public String MinRoll
		{
			get
			{
				return _minRoll;
			}
			set
            {
            	_minRoll = value;
            }
		}

		/// <summary>
		/// 권 번호를 구하거나 설정합니다.
		/// </summary>
		public String BookNo
		{
			get
			{
				_bookNo = _sheaf.Substring(8, 2);

				return _bookNo;
			}
			set
            {
            	_bookNo = value;
            }
		}
        
		/// <summary>
		/// Sub 권 번호를 구하거나 설정합니다.
		/// </summary>
		public String BookSubNo
		{
			get
			{
				return _bookSubNo;
			}
			set
            {
            	_bookSubNo = value;
            }
		}
        
		/// <summary>
		/// Filename을 구하거나 설정합니다.
		/// </summary>
		public String Filename
		{
			get
			{
				return _filename;
			}
			set
            {
            	_filename = value;
            }
		}

		/// <summary>
		/// 년도부터 시작하는 전체 경로를 구합니다.(filename을 포함합니다.)
		/// <remark>2000\0001-0007\0001\0001_01\0001\0001.tif</remark>
		/// </summary>
		public string FullPath
		{
			get
			{
				string fullPath = 
					string.Format(@"{0}\{1}-{2}\{3}\{4}_{5}\{6}\{7}", 
						Year, MinRoll, MaxRoll, Roll, Roll, BookNo, BookSubNo, Filename);
				
				return fullPath;
			}
		}

		/// <summary>
		/// 년도부터 시작하는 Source의 전체 경로를 구합니다.(filename은 제외 시킵니다.)
		/// <remark>2000\0001-0007\0001\0001(01)</remark>
		/// </summary>
		public string SourceFullPath
		{
			get
			{
				string fullPath = 
					string.Format(@"{0}\{1}-{2}\{3}\{4}({5})", 
						Year, MinRoll, MaxRoll, Roll, Roll, BookNo);					
				
				return fullPath;
			}
		}
		#endregion
	}

	/// <summary>
	/// TiffPath를 구하기 위한 class입니다.
	/// </summary>
	public class TiffPathInfo
	{
		#region Members
		private string _sheaf;
		private string _minRoll;
		private string _maxRoll;
		private string _bookSubNo;
		private string _fileName;
		#endregion

		#region Constructor
		/// <summary>
		/// Initializes a new instance of the TiffPathInfo class.
		/// </summary>
		public TiffPathInfo()
		{
		}

		/// <summary>
		/// Initializes a new instance of the TiffPathInfo class.
		/// </summary>
		/// <param name="sheaf"></param>
		/// <param name="minRoll"></param>
		/// <param name="maxRoll"></param>
		/// <param name="bookSubNo"></param>
		/// <param name="fileName"></param>
		public TiffPathInfo(string sheaf, string minRoll, string maxRoll, string bookSubNo, string fileName)
		{
			_sheaf = sheaf;
			_minRoll = minRoll;
			_maxRoll = maxRoll;
			_bookSubNo = bookSubNo;
			_fileName = fileName;
		}
		#endregion

		#region Properties
		/// <summary>
		/// 철ID를 구하거나 설정합니다.
		/// </summary>
		public String Sheaf
		{
			get
			{
				return _sheaf;
			}
			set
			{
				_sheaf = value;
			}
		}
        
		/// <summary>
		/// MinRoll을 구하거나 설정합니다.
		/// </summary>
		public String MinRoll
		{
			get
			{
				return _minRoll;
			}
			set
			{
				_minRoll = value;
			}
		}
        
		/// <summary>
		/// MaxRoll을 구하거나 설정합니다.
		/// </summary>
		public String MaxRoll
		{
			get
			{
				return _maxRoll;
			}
			set
			{
				_maxRoll = value;
			}
		}
        
		/// <summary>
		/// Sub book No를 구하거나 설정합니다.
		/// </summary>
		public String BookSubNo
		{
			get
			{
				return _bookSubNo;
			}
			set
			{
				_bookSubNo = value;
			}
		}
        
		/// <summary>
		/// Filename을구하거나 설정합니다.
		/// </summary>
		public String FileName
		{
			get
			{
				return _fileName;
			}
			set
			{
				_fileName = value;
			}
		}
		#endregion
	}
}
