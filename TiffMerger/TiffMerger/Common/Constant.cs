﻿/**********************************************************************************************************************/
/*	Name		:	TiffMerger.Common.Constant
/*	Purpose		:	공통으로 사용하는 상수를 정의합니다.
/*	Creator		:	Kim Ki Won
/*	Create		:	2006년 9월 29일 금요일 오전 2:03:43
/*	Modifier	:	
/*	Update		:	2006년 9월 29일 금요일
/*	Comment		:	
/**********************************************************************************************************************/

using System;
using System.Text;

namespace TiffMerger.Common
{
	/// <summary>
	/// 공통으로 사용하는 상수를 정의합니다.
	/// </summary>
	public class Constant
	{
		public const string INIT_SAVE_FOLDER = @"DATA1\DATA1\3930149\";

		/// <summary>
		/// Ready
		/// </summary>
		public const string STATUS_READY = "Ready";
		/// <summary>
		/// OK
		/// </summary>
		public const string STATUS_OK = "OK";
		/// <summary>
		/// Miss
		/// </summary>
		public const string STATUS_MISS = "Miss";
		/// <summary>
		/// Error
		/// </summary>
		public const string STATUS_ERROR = "Error";

		/// <summary>
		/// 첫번째 Sheet이 이름을 지정합니다.
		/// </summary>
		public static string SheetName = "Sheet1";
	}
}
