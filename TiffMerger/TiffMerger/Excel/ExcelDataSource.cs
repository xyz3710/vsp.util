/**********************************************************************************************************************/
/*	Name		:	My.Excel.ExcelDataSource
/*	Purpose		:	엑셀 파일을 DataSet이나 DataTable 형태로 읽어들입니다.
/*	Creator		:	Kim Ki Won
/*	Create		:	2006년 9월 27일 수요일 오후 10:07:39
/*	Modifier	:	
/*	Update		:	2006년 9월 27일 수요일
/*	Comment		:	
/**********************************************************************************************************************/

using System;
using System.IO;
using System.Data;
using System.Data.OleDb;
using System.Text;

namespace TiffMerger.Excel
{
	/// <summary>
	/// Summary description for ExcelDataSource.
	/// </summary>
	public class ExcelDataSource
	{
		#region Members
		private string _fileName;
		private bool _showheader;
		#endregion

		#region Constructor
		/// <summary>
		/// ExcelDataSource instance를 생성합니다.
		/// </summary>
		/// <param name="fileName"></param>
		public ExcelDataSource(string fileName)
		{
			if (File.Exists(fileName) == false)
				throw new FileNotFoundException("Excel file not found!", fileName);

			_fileName = fileName;
		}
		#endregion

		#region GetDataSet
		/// <summary>
		/// 특정 범위를 가지고 엑셀에서 DataSet을 구합니다.
		/// </summary>
		/// <param name="sheet"></param>
		/// <param name="startRange"></param>
		/// <param name="endRange"></param>
		/// <returns></returns>
		public DataSet GetDataSet(string sheet, string startRange, string endRange)
		{
			if (sheet != null && sheet.Length > 0)
			{
				if (startRange != null && startRange.Length > 0)
				{
					if (endRange != null && endRange.Length > 0)
					{
						return getExcelDataSet(String.Format("select * from [{0}${1}:{2}]", sheet, startRange, endRange));
					}
					else
					{
						throw new ArgumentNullException("endRange", "endrange must not be null!");
					}
				}
				else
				{
					throw new ArgumentNullException("startRange", "startrange must not be null!");
				}
			}
			else
			{
				throw new ArgumentNullException("sheet", "sheet must not be null!");
			}
		}

		/// <summary>
		/// 특정 범위 이름을 가지고 엑셀에서 DataSet을 구합니다.
		/// </summary>
		/// <param name="namedRange"></param>
		/// <returns></returns>
		public DataSet GetDataSet(string namedRange)
		{
			if (namedRange != null && namedRange.Length > 0)
			{
				return getExcelDataSet(String.Format("select * from [{0}]", namedRange));
			}
			else
			{
				throw new ArgumentNullException("namedRange", "namedrange must not be null!");
			}
		}
		#endregion

		#region GetSheetDataSet
		/// <summary>
		/// 특정 sheet 이름을 가지고 엑셀에서 DataSet을 구합니다.
		/// </summary>
		/// <param name="sheetName"></param>
		/// <returns></returns>
		public DataSet GetSheetDataSet(string sheetName)
		{
			return GetSheetDataSet(string.Empty, sheetName); 
		}

		/// <summary>
		/// 특정 sheet 이름과 SQL 문으로 가지고 엑셀에서 DataSet을 구합니다.
		/// </summary>
		/// <param name="sql"></param>
		/// <param name="sheetName"></param>
		/// <returns></returns>
		public DataSet GetSheetDataSet(string sql, string sheetName)
		{
			if (sheetName != null && sheetName.Length > 0)
			{
				string query = sql;

				if (query == string.Empty)
					query = string.Format("select * from [{0}$]", sheetName);

				return getExcelDataSet(query);
			}
			else
			{
				throw new ArgumentNullException("sheetName", "sheetname must not be null!");
			}
		}
		#endregion

		#region GetDataTable
		/// <summary>
		/// Query를 가지고 엑셀에서 DataTable을 구합니다.
		/// <remark>테이블명은 Sheet 이름이 되며 [Shht1$]로 표현 하시면 됩니다.</remark>
		/// </summary>
		/// <param name="commandText"></param>
		/// <returns></returns>
		public DataTable GetDataTable(string commandText)
		{
			DataTable dataTable = new DataTable();
			DataSet ds = getExcelDataSet(commandText);

			if (ds.Tables.Count > 0)
				dataTable = ds.Tables[0];

			return dataTable; 
		}
		#endregion
		
		#region Properties
		/// <summary>
		/// 첫 행을 Header로 인식을 할지 여부를 결정합니다.
		/// <remark>다른 method를 사용하기 전에 입력해야 합니다.</remark>
		/// </summary>
		public bool ShowHeader
		{
			get 
			{ 
				return _showheader; 
			}
			set 
			{
				_showheader = value; 
			}
		}

		/// <summary>
		/// OLEDB를 통한 엑셀과의 연결문자를 구합니다.
		/// </summary>
		public string Connection
		{
			get
			{
				StringBuilder sb = new StringBuilder();

				sb.Append("Provider = Microsoft.Jet.OLEDB.4.0; ");
				sb.AppendFormat("Data Source = {0}; ", _fileName);
				sb.AppendFormat("Extended Properties=\"Excel 8.0;HDR={0}\"; ",
					_showheader == true ? "YES" : "NO");

				return sb.ToString(); 
			}
		}
		#endregion

		#region Private Method
		private DataSet getExcelDataSet(string sql)
		{
			DataSet dataset = new DataSet();
			
			try
			{    
				OleDbDataAdapter adapter = new OleDbDataAdapter(sql, Connection);
				
				adapter.Fill(dataset);
			}
			catch(Exception ex)
			{
				throw new Exception(ex.Message);
			}

			return dataset;
		}
		#endregion
	}
}
