/**********************************************************************************************************************/
/*	Name		:	TiffMerger.Excel.ExcelManager
/*	Purpose		:	
/*	Creator		:	Kim Ki Won
/*	Create		:	2006년 9월 27일 수요일 오후 11:10:12
/*	Modifier	:	
/*	Update		:	2006년 9월 27일 수요일
/*	Comment		:	Min, Max roll을 구하는 부분에서 delay가 생겨서 DataTable을 구해올 때 작업한다.
/**********************************************************************************************************************/

using System;
using System.Data;
using System.Text;

using TiffMerger.Common;

namespace TiffMerger.Excel
{
	/// <summary>
	/// ExcelManager에 대한 요약 설명입니다.
	/// </summary>
	public class ExcelManager
	{
		#region Members
		private string _fileName;
		private string _sheetName;
		#endregion

		#region Constructor
		/// <summary>
		/// Initializes a new instance of the ExcelManager class.
		/// </summary>
		/// <param name="fileName"></param>
		public ExcelManager(string fileName)
		{
			_fileName = fileName;
			_sheetName = Constant.SheetName;
		}
		#endregion

		#region GetDataTable
		/// <summary>
		/// DataTable을 구합니다.
		/// </summary>
		/// <returns></returns>
		public DataTable GetDataTable()
		{
			return GetDataTable(string.Empty);
		}

		/// <summary>
		/// DataTable을 구합니다.
		/// </summary>
		/// <param name="totalRoll">전체 roll(0001-0007), 없을 경우는 <see cref="string.Empty"/></param>
		/// <returns></returns>
		public DataTable GetDataTable(string totalRoll)
		{
			ExcelDataSource excelDataSource = new ExcelDataSource(_fileName);

			excelDataSource.ShowHeader = false;

			string commandText = getDataTableCommandText(_sheetName);
			DataTable dataTable = excelDataSource.GetDataTable(commandText);

			return getNewDataTable(dataTable, totalRoll);
		}
		#endregion

		#region GetMaxMinRoll(not used)
		/// <summary>
		/// 최대/최소 롤 번호를 구합니다.
		/// TODO: 속도 문제로 사용하지 않습니다.
		/// </summary>
		/// <param name="minRoll">최소 Roll 번호를 return 구합니다.</param>
		/// <param name="maxRoll">최대 Roll 번호를 return 구합니다.</param>
		public void GetMaxMinRoll(ref string minRoll, ref string maxRoll)
		{
			ExcelDataSource excelDataSource = new ExcelDataSource(_fileName);

			excelDataSource.ShowHeader = false;

			string commandText = getMaxMinSheafCommandText(_sheetName);
			DataTable dataTable = excelDataSource.GetDataTable(commandText);

			if (dataTable != null && dataTable.Rows.Count > 0)
			{
				// sheafID data는 2000000701,  앞 2000 : 년도, 0007 : sheafID, 01 : BookNo
				minRoll = int.Parse(dataTable.Rows[0]["min_sheaf"].ToString().Substring(4, 4)).ToString("0000");
				maxRoll = int.Parse(dataTable.Rows[0]["min_sheaf"].ToString().Substring(4, 4)).ToString("0000");
			}
		}
		#endregion

		#region Private methods
		private string getDataTableCommandText(string sheetName)
		{
			StringBuilder sb = new StringBuilder();

			sb.Append("SELECT ");
			sb.Append("	A.f1 AS sheaf, ");
			sb.Append("	C.min_sheaf AS MinRoll, ");
			sb.Append("	C.max_sheaf AS MaxRoll, ");
			sb.Append("	B.COUNT_f1 AS BookSum, ");
			sb.Append("	A.f2 AS Page, ");
			sb.Append("	B.SUM_f2 AS PageSum, ");
			sb.Append("	A.f3 AS FileName ");
//			sb.Append("	A.f4 AS FilePath ");
			sb.Append("	FROM ");
			sb.AppendFormat("		[{0}$] A, ", sheetName);
			sb.Append("		(SELECT ");
			sb.Append("			X.f1, ");
			sb.Append("			COUNT(X.f1) AS COUNT_f1, ");
			sb.Append("			SUM(X.f2) AS SUM_f2 ");
			sb.AppendFormat("			FROM [{0}$] X ", sheetName);
			sb.Append("			GROUP BY X.f1) B, ");
			sb.Append("		(SELECT ");
			sb.Append("			MAX(f1) AS max_sheaf, ");
			sb.Append("			MIN(f1) AS min_sheaf ");
			sb.AppendFormat("			FROM [{0}$]) C ", sheetName);
			sb.Append("	WHERE ");
//			sb.Append("LEN(MID(A.F3, LEN(A.F3)-2, 3)) <> LEN(A.F3) ");
			sb.Append("		A.f1 = B.f1 ");

			return sb.ToString();
		}

		private string getMaxMinSheafCommandText(string sheetName)
		{
			StringBuilder sb = new StringBuilder();

			sb.Append("SELECT ");
			sb.Append("	MIN(f1) AS min_sheaf, ");
			sb.Append("	MAX(f1) AS max_sheaf ");
			sb.AppendFormat("	FROM [{0}$] ", sheetName);

			return sb.ToString();
		}

		private DataTable getNewDataTable(DataTable dataTable)
		{
			return getNewDataTable(dataTable, string.Empty);
		}

        private DataTable getNewDataTable(DataTable dataTable, string totalRoll)
		{
			DataTable newDataTable = new DataTable();

			newDataTable.Columns.Add("No");
			newDataTable.Columns.Add("상 태");
			newDataTable.Columns.Add("철 ID");
			newDataTable.Columns.Add("권수");
			newDataTable.Columns.Add("시작 No");
			newDataTable.Columns.Add("페이지수");
			newDataTable.Columns.Add("페이지합");
			newDataTable.Columns.Add("파 일 명");
			newDataTable.Columns.Add("소  스  경  로");
			newDataTable.Columns.Add("타  겟  경  로");

			TiffPath tiffPath = new TiffPath();
			object[] row;
			string sheaf = string.Empty;
			string bookSum = string.Empty;
			int startPage = 1;
			string page = string.Empty;
			string pageSum = string.Empty;
			string fileName = string.Empty;
			string targetPath = string.Empty;
			string sourcePath = string.Empty;
			string beforeSheaf = string.Empty;
			string minRoll = string.Empty;
			string maxRoll = string.Empty;
			int bookSubNo = 1;

			for (int i = 0; i < dataTable.Rows.Count; i++)
			{
				sheaf = dataTable.Rows[i]["sheaf"].ToString();
				bookSum = dataTable.Rows[i]["BookSum"].ToString();
				minRoll = dataTable.Rows[i]["minRoll"].ToString().Substring(4, 4);
				maxRoll = dataTable.Rows[i]["maxRoll"].ToString().Substring(4, 4);
				page = int.Parse(dataTable.Rows[i]["page"].ToString()).ToString("000");
				pageSum = int.Parse(dataTable.Rows[i]["pagesum"].ToString()).ToString("000");
				fileName = dataTable.Rows[i]["filename"].ToString();

				// filePath를 구한다.
				// 순차적으로 읽어들이므로 철ID를 비교하여 틀리면 bookCount를 초기화 한다.
				if (bookSubNo == 1)
					beforeSheaf = sheaf;

				if (beforeSheaf != sheaf)
				{
					bookSubNo = 1;
					startPage = 1;
					beforeSheaf = sheaf;
				}

				tiffPath.Sheaf = sheaf;

				if (totalRoll == string.Empty)
				{
					tiffPath.MinRoll = minRoll;
					tiffPath.MaxRoll = maxRoll;
				}
				else
				{
					tiffPath.MinRoll = totalRoll.Substring(0, 4);
					tiffPath.MaxRoll = totalRoll.Substring(5, 4);
				}

				tiffPath.BookSubNo = bookSubNo.ToString("0000");
				tiffPath.Filename = fileName;

				sourcePath = tiffPath.SourceFullPath;
				targetPath = tiffPath.FullPath;

				row = new object[] { (i+1).ToString(),
									Constant.STATUS_READY,
									sheaf,
									bookSum,
									startPage.ToString("000"),
									page,
									pageSum,
									fileName,
									sourcePath,
									targetPath };

				newDataTable.Rows.Add(row);

				bookSubNo++;
				startPage += int.Parse(page);
			}

			return newDataTable; 
		}
		#endregion
	}		
}
