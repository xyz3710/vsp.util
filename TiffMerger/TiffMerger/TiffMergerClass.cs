﻿/**********************************************************************************************************************/
/*	Name		:	TiffMerger.TiffMergerClass
/*	Purpose		:	
/*	Creator		:	Kim Ki Won
/*	Create		:	2006년 10월 5일 목요일 오전 1:49:46
/*	Modifier	:	
/*	Update		:	2006년 10월 5일 목요일
/*	Comment		:	
/**********************************************************************************************************************/

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Collections;

using TiffMerger.Common;
using TiffMerger.Excel;
using TiffMerger.Tiff;

namespace TiffMerger
{
	public partial class TiffMergerClass : Form
	{
		#region Members
		private int _totalMergedFile;
		private ArrayList _buttonState;
		#endregion

		#region Constructor
		public TiffMergerClass()
		{
			InitializeComponent();

			btnCheck.Enabled = false;
			btnRun.Enabled = false;
			btnErrorSelect.Enabled = false;

//			// UNDONE:
//			txtSource.Text = @"D:\샘플\변환전\";
//			txtRoot.Text = @"D:\";
//			txtTarget.Text = @"D:\샘플\변환후\";
//			excelFileDialog.InitialDirectory = @"D:\샘플";
		}
		#endregion

		#region Async Delegate
		/// <summary>
		/// 비동기 대리자를 생성합니다.
		/// </summary>
		private delegate ArrayList CheckPathDelegate();

		/// <summary>
		/// 비동기 대리자를 생성합니다.
		/// </summary>
		private delegate bool TiffMergeDelegate();

		/// <summary>
		/// Cross thread 처리시 Windows Control 때문에 Debug시 오류가 발생한다.
		/// <remarks>MSDN : http://msdn2.microsoft.com/en-us/library/ms171728.aspx</remarks>
		/// </summary>
		/// <param name="checkSelect"></param>
		private delegate int GetTotalMergedFileDelegate(bool checkSelect);
		private delegate void InitCurrentProgressDelegate(int maximum);
		private delegate void InitTotalProgressDelegate(bool checkSelect);
		#endregion

		#region Async Methods
		/// <summary>
		/// checkPath callback 함수로써 비동기 작업이 끝나면 호출됩니다.
		/// </summary>
		/// <param name="asyncResult"></param>
		private void checkPathAsyncCallbackMethod(IAsyncResult asyncResult)
		{
			CheckPathDelegate checkPathDelegate = (CheckPathDelegate)asyncResult.AsyncState;
			ArrayList missingFIles = checkPathDelegate.EndInvoke(asyncResult);

			getButtonState();

			if (missingFIles.Count == 0)
			{
				selectAll();

				MessageBox.Show("검사가 완료 되었습니다.", "파일 검사 성공",
					MessageBoxButtons.OK, MessageBoxIcon.Information);
			}
			else
			{
				btnRun.Enabled = false;

				string[] files = (string[])missingFIles.ToArray(typeof(string));
				StringBuilder msg = new StringBuilder();

				msg.Append("파일이 일치하지 않거나 없습니다.\r\n\r\n");

				for (int i = 0; i < files.Length; i++)
					msg.AppendFormat("{0}\r\n", files[i]);

				MessageBox.Show(msg.ToString(),
					"파일 검사 오류", MessageBoxButtons.OK, MessageBoxIcon.Information);

				unSelectAll();
			}
		}

		/// <summary>
		/// tiffMerge callback 함수로써 비동기 작업이 끝나면 호출됩니다.
		/// </summary>
		/// <param name="asyncResult"></param>
		private void tiffMergeAsyncCallbackMethod(IAsyncResult asyncResult)
		{
			TiffMergeDelegate tiffMergeDelegate = (TiffMergeDelegate)asyncResult.AsyncState;
			bool result = tiffMergeDelegate.EndInvoke(asyncResult);

			if (result == true)
			{
				MessageBox.Show("작업이 완료 되었습니다.", "성공",
					MessageBoxButtons.OK, MessageBoxIcon.Information);
				btnRun.Enabled = false;
			}
			else
				MessageBox.Show("실패 하였습니다.\r\n에러 선택 후 다시 실행해 하시거나 재부팅 후 다시 시도해 주십시오.",
					"실패", MessageBoxButtons.OK, MessageBoxIcon.Error);

			getButtonState();
		}
		#endregion
		
		#region Event Handler
		private void TiffMergerClass_Load(object sender, EventArgs e)
		{
			Merger.Merging += new Merger.MergeProcessHandeler(Merger_Merging);

			mtxtRollNumber.Enabled = false;
		}

		private void Merger_Merging(string mergedFilename, int currentPage)
		{
			lblMergingFilename.Text = mergedFilename;

			pbCurrent.Value = currentPage;
			pbTotal.Value = _totalMergedFile++;
		}

		private void btnSource_Click(object sender, System.EventArgs e)
		{
			sourceFolderBD.Description = "소스 폴더를 선택해 주십시오.\r\n(통상적으로 연도 이전을 선택합니다.)";
			sourceFolderBD.ShowDialog();

			string path = getPath(sourceFolderBD.SelectedPath);

			if (path != string.Empty)
				txtSource.Text = path;

			btnRun.Enabled = false;
		}

		private void chkChangeSourceRollNumber_CheckedChanged(object sender, EventArgs e)
		{
			if (chkChangeSourceRollNumber.Checked == true)
			{
				mtxtRollNumber.Text = string.Empty;
				mtxtRollNumber.Enabled = true;
				mtxtRollNumber.Focus();
			}
			else
				mtxtRollNumber.Enabled = false;
		}

		private void btnRoot_Click(object sender, System.EventArgs e)
		{
			rootFolderBD.Description = "파일이 저장될 Root 경로를 지정해 주십시오.";
			rootFolderBD.ShowDialog();

			string path = getPath(rootFolderBD.SelectedPath);

			if (path != string.Empty)
			{
				txtRoot.Text = path;
				txtTarget.Text = string.Format("{0}{1}", txtRoot.Text, Constant.INIT_SAVE_FOLDER);
			}

			btnRun.Enabled = false;
		}

		private void btnSave_Click(object sender, System.EventArgs e)
		{
			targetFolderBD.Description = "저장 폴더를 지정해 주십시오.\r\n파일 경로와 합쳐진 폴더로 저장됩니다.";
			targetFolderBD.SelectedPath = txtRoot.Text;
			targetFolderBD.ShowDialog();

			string path = getPath(targetFolderBD.SelectedPath);

			if (path != string.Empty)
				txtTarget.Text = path;

			btnRun.Enabled = false;
		}

		private void btnExcel_Click(object sender, System.EventArgs e)
		{
			excelFileDialog.DefaultExt = "*.xls";
			excelFileDialog.Filter = "Microsoft Excel 워크시트 (*.xls)|*.xls";
			excelFileDialog.FilterIndex = 0;
			excelFileDialog.CheckFileExists = true;
			excelFileDialog.CheckPathExists = true;
			// UNDONE: 
			excelFileDialog.InitialDirectory = 
				txtSource.Text == string.Empty ? @"C:\" : txtSource.Text;

			excelFileDialog.ShowDialog();

			string excelFile = excelFileDialog.FileName;

			if (excelFile == string.Empty)
				return;

			initTotalProgressBar(false);
			initCurrentProgressBar(1);

			lblExcel.Text = "엑셀 파일 읽는중...";
			lblExcel.Update();

			lblExcel.Text = Path.GetFileName(excelFile);

			try
			{
				excelBind(excelFile);
				btnCheck.Enabled = true;
				btnRun.Enabled = false;
				lblExcel.Text = Path.GetFileName(excelFile);
			}
			catch (Exception ex)
			{
				MessageBox.Show("사용할 수 있는 엑셀 파일이 아닙니다.\r\n\r\n철ID, 쪽수, 파일명의 컬럼과\r\n" +
					"Sheet1 이라는 sheet 이름을 포함해야 합니다." + "\r\n\r\n" + ex.Message,
					"다른 엑셀 파일을 선택해 주십시오.",
					MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
			}

			btnRun.Enabled = false;
			btnErrorSelect.Enabled = false;

			// 엑셀 파일을 읽은 후에는 소스 경로를 수정할 수 없다.
		}

		private void btnCheck_Click(object sender, System.EventArgs e)
		{
			if (txtSource.Text == string.Empty)
			{
				MessageBox.Show("소스 경로를 선택해야 합니다.");

				return;
			}

			if (txtTarget.Text == string.Empty)
			{
				MessageBox.Show("저장 경로를 선택해야 합니다.");

				return;
			}

			// CheckPathDelegate instance 생성
			CheckPathDelegate checkPathDelegate = new CheckPathDelegate(checkPath);

			// callback method 선언
			AsyncCallback asyncCallback = new AsyncCallback(checkPathAsyncCallbackMethod);

			// 비동기 호출 초기화
			IAsyncResult ar = checkPathDelegate.BeginInvoke(asyncCallback, checkPathDelegate);

			lblMergingFilename.Text = "현재 진행 중인 파일을 보여줍니다.";
			btnErrorSelect.Enabled = true;

			setButtonState();
		}

		private void btnRun_Click(object sender, System.EventArgs e)
		{
			// ProgressDelegate instance 생성
			TiffMergeDelegate tiffMergeDelegate = new TiffMergeDelegate(tifMerge);

			// callback method 선언
			AsyncCallback asyncCallback = new AsyncCallback(tiffMergeAsyncCallbackMethod);

			// 비동기 호출 초기화
			IAsyncResult ar = tiffMergeDelegate.BeginInvoke(asyncCallback, tiffMergeDelegate);

			setButtonState();
		}

		private void btnErrorSelect_Click(object sender, System.EventArgs e)
		{
			int selectedRowsCount = 0;
			unSelectAll();

			for (int i = 0; i < grdExcel.Rows.Count; i++)
			{
				if (getStatus(i) == Status.Error)
				{
					grdExcel.Rows[i].Selected = true;
					selectedRowsCount++;
				}
			}

			if (selectedRowsCount > 0)
				btnRun.Enabled = true;
		}

		private void grdExcel_CellClick(object sender, DataGridViewCellEventArgs e)
		{
			if (e.RowIndex == -1 && e.ColumnIndex == -1)
			{
				// 전체 선택을 할 경우 상태를 Ready 상태로 돌린다.
				for (int i = 0; i < grdExcel.Rows.Count; i++)
					setStatus(i, Status.Ready);
			}
		}
		#endregion

		#region initCurrentProgressBar
		private void initCurrentProgressBar(int maximum)
		{
			if (pbCurrent.InvokeRequired == true)
			{
				InitCurrentProgressDelegate initCurrent = new InitCurrentProgressDelegate(initCurrentProgressBar);

				Invoke(initCurrent, new object[] { maximum });
			}
			else
			{
				pbCurrent.Minimum = 0;
				pbCurrent.Maximum = maximum;
				pbCurrent.Step = 1;
				pbCurrent.Value = pbCurrent.Minimum;
				pbCurrent.Update();
			}
		}
		#endregion

		#region initTotalProgressBar
		/// <summary>
		/// Total progressBar를 초기화 합니다.
		/// </summary>
		/// <param name="checkSelect">선택된것만을 체크할지 결정합니다.</param>
		private void initTotalProgressBar(bool checkSelect)
		{
			if (pbTotal.InvokeRequired == true)
			{
				InitTotalProgressDelegate initTotal = new InitTotalProgressDelegate(initTotalProgressBar);

				Invoke(initTotal, new object[] { checkSelect });
			}
			else
			{
				pbTotal.Minimum = 0;
				pbTotal.Maximum = getTotalMergedFile(checkSelect);
				pbTotal.Step = 1;
				_totalMergedFile = 1;
				pbTotal.Value = pbTotal.Minimum;
				pbTotal.Update();
			}
		}
		#endregion

		#region selectAll
		private void selectAll()
		{
			// 모든 행을 선택합니다.
			grdExcel.SelectAll();
		}
		#endregion

		#region unSelectAll
		private void unSelectAll()
		{
			// 선택된 행이 없도록 합니다.
			grdExcel.MultiSelect = false;
			grdExcel.MultiSelect = true;
		}
		#endregion

		#region getPath
		/// <summary>
		/// 경로뒤에 Path.DirectorySeparatorChar를 붙여줍니다.
		/// </summary>
		/// <param name="path"></param>
		/// <returns></returns>
		private string getPath(string path)
		{
			if (path != string.Empty && 
				path.Substring(path.Length - 1, 1) != Path.DirectorySeparatorChar.ToString())
				path= string.Format("{0}{1}", path, Path.DirectorySeparatorChar);

			return path;
		}
		#endregion

		#region excelBind
		/// <summary>
		/// Grid에 Excel 파일을 Binding 시킵니다.
		/// </summary>
		/// <param name="excelFileName">엑셀 파일 이름</param>
		private void excelBind(string excelFileName)
		{
			string totalRoll = string.Empty;

			if (chkChangeSourceRollNumber.Checked == true && mtxtRollNumber.Text.Length == 9)
				totalRoll = mtxtRollNumber.Text;

			DataTable dataTable = new ExcelManager(excelFileName).GetDataTable(totalRoll);

			grdExcel.DataSource = dataTable;
		}
		#endregion

		#region checkPath
		/// <summary>
		/// 소스경로의 파일들을 비교합니다.
		/// </summary>
		/// <returns>오류가 나거나 없는 파일 목록입니다.</returns>
		private ArrayList checkPath()
		{
			ArrayList missingFiles = new ArrayList();
			string sourcePath = string.Empty;
			string checkedFile = string.Empty;
			int bookCount = 1;			// 권 번호를 counting한다.
			int bookSum = 0;			// 롤에 들어있는 권 수량
			int pageSum = 0;
			int cnt = 1;

			// 검사할 때는 선택된 항목이 없으므로 false를 넘긴다.
			initTotalProgressBar(false);

			// 소스경로를 수정하였으면 다시 binding 한다.
			if (chkChangeSourceRollNumber.Checked == true)
			{
				grdExcel.DataSource = null;
				excelBind(lblExcel.Text);
			}

			for (int i = 0; i < grdExcel.Rows.Count; i++)
			{
				bookSum = int.Parse(grdExcel.Rows[i].Cells[(int)Columns.BookSum].Value.ToString());

				if (bookCount != bookSum)
				{
					bookCount++;

					continue;
				}
				else
					bookCount = 1;

				sourcePath = string.Format("{0}{1}",
					txtSource.Text, grdExcel.Rows[i].Cells[(int)Columns.SourcePath].Value.ToString());
				pageSum = int.Parse(grdExcel.Rows[i].Cells[(int)Columns.PageSum].Value.ToString());
				initCurrentProgressBar(pageSum);

				for (int j = 1; j <= pageSum; j++)
				{
					checkedFile = string.Format(@"{0}\{1:0000}.tif", sourcePath, j);

					lblMergingFilename.Text = checkedFile;
					lblMergingFilename.Update();

					if (File.Exists(checkedFile) == false)
						missingFiles.Add(checkedFile);

					pbTotal.Value = cnt++;
					pbCurrent.Value = j;
				}
			}

			return missingFiles;
		}
		#endregion

		#region makeTargetFolders
		/// <summary>
		/// target Folder를 생성합니다.
		/// </summary>
		/// <returns></returns>
		private bool makeTargetFolders()
		{
			DataTable dataTable = (DataTable)grdExcel.DataSource;
			string targetFolder = string.Empty;
			bool ret = true;

			for (int i = 0; i < dataTable.Rows.Count; i++)
			{
				targetFolder = string.Format("{0}{1}",
					txtTarget.Text, Path.GetDirectoryName(dataTable.Rows[i][(int)Columns.TargetFilePath].ToString()));

				try
				{
					Directory.CreateDirectory(targetFolder);
				}
				catch (Exception)
				{
					ret = false;
				}
			}

			return ret;
		}
		#endregion

		#region getStatus
		private Status getStatus(int rowNo)
		{
			Status status = Status.Ready;
			string caption = grdExcel.Rows[rowNo].Cells[(int)Columns.State].Value.ToString();

			if (caption == Constant.STATUS_READY)
				status = Status.Ready;

			if (caption == Constant.STATUS_OK)
				status = Status.OK;

			if (caption == Constant.STATUS_MISS)
				status = Status.Miss;

			if (caption == Constant.STATUS_ERROR)
				status = Status.Error;

			return status;
		}
		#endregion

		#region setStatus
		private void setStatus(int rowNo, Status status)
		{
			string caption = string.Empty;

			switch (status)
			{
				case Common.Status.Ready:
					caption = Constant.STATUS_READY;

					break;
				case Common.Status.OK:
					caption = Constant.STATUS_OK;

					break;
				case Common.Status.Miss:
					caption = Constant.STATUS_MISS;

					break;
				case Common.Status.Error:
					caption = Constant.STATUS_ERROR;

					break;
			}

			grdExcel.Rows[rowNo].Cells[(int)Columns.State].Value = caption;
		}
		#endregion

		#region getTotalMergedFile
		private int getTotalMergedFile(bool checkSelect)
		{
			int sum = 0;

			if (grdExcel.InvokeRequired == true)
			{
				GetTotalMergedFileDelegate getTotal = new GetTotalMergedFileDelegate(getTotalMergedFile);

				Invoke(getTotal, new object[] { checkSelect });
			}
			else
			{
				for (int i = 0; i < grdExcel.Rows.Count; i++)
				{
					if (checkSelect == true)
					{
						if (grdExcel.Rows[i].Selected == true)
							sum += int.Parse(grdExcel.Rows[i].Cells[(int)Columns.Pages].Value.ToString());
					}
					else
						sum += int.Parse(grdExcel.Rows[i].Cells[(int)Columns.Pages].Value.ToString());
				}
			}
			return sum;
		}
		#endregion

		#region getButtonState
		private void getButtonState()
		{
			foreach (Control control in _buttonState)
			{
				if (control is Button)
					((Button)control).Enabled = true;

				if (control is CheckBox)
					((CheckBox)control).Enabled = true;
			}

			grdExcel.Enabled = true;
		}
		#endregion

		#region setButtonState
		private void setButtonState()
		{
			if (_buttonState == null)
				_buttonState = new ArrayList();

			foreach (object control in Controls)
			{
				if (control is Button)
				{
					Button button = (Button)control;

					_buttonState.Add(button);
					button.Enabled = false;
				}

				if (control is CheckBox)
				{
					CheckBox checkBox = (CheckBox)control;

					_buttonState.Add(checkBox);
					checkBox.Enabled = false;
				}
			}

			grdExcel.Enabled = false;
		}
		#endregion

		#region tifMerge
		private bool tifMerge()
		{
			Merger merger = Merger.GetInstance();
			string sourcePath = string.Empty;
			int startPage = 0;
			int totalPages = 0;
			string mergedFilePath = string.Empty;
			string sourceRoot = getPath(txtSource.Text);
			string targetRoot = getPath(txtTarget.Text);
			string mergedPath = string.Empty;
			bool ret = true;

			// 실행할 때는 선택된 항목이 있으므로 true를 넘긴다.
			initTotalProgressBar(true);

			for (int i = 0; i < grdExcel.Rows.Count; i++)
			{
				if (grdExcel.Rows[i].Selected == false)
					continue;

				sourcePath = 
					string.Format("{0}{1}", sourceRoot,
					grdExcel.Rows[i].Cells[(int)Columns.SourcePath].Value.ToString());
				startPage = int.Parse(grdExcel.Rows[i].Cells[(int)Columns.StartPage].Value.ToString());
				totalPages = int.Parse(grdExcel.Rows[i].Cells[(int)Columns.Pages].Value.ToString());
				mergedFilePath = 
					string.Format("{0}{1}", targetRoot,
					grdExcel.Rows[i].Cells[(int)Columns.TargetFilePath].Value.ToString());
				mergedPath = Path.GetDirectoryName(mergedFilePath);

				try
				{
					Directory.CreateDirectory(mergedPath);
				}
				catch (Exception)
				{
					MessageBox.Show(
						"저장 경로 폴더가 생성되지 않았습니다.\r\n" + mergedPath,
						"폴더가 생성되지 않았습니다.", MessageBoxButtons.OK, MessageBoxIcon.Information);
					setStatus(i, Status.Error);
					ret = false;

					continue;
				}

				try
				{
					initCurrentProgressBar(totalPages);

					ret = merger.TiffMerge(getPath(sourcePath), startPage, totalPages, mergedFilePath);

					if (ret == true)
						setStatus(i, Status.OK);
					else
						setStatus(i, Status.Error);
				}
				catch (Exception)
				{
					setStatus(i, Status.Error);
					ret = false;
				}

				grdExcel.Rows[i].Selected = true;
				grdExcel.Update();
			}

			return ret;
		}
		#endregion

		private void aboutToolStripMenuItem_Click(object sender, EventArgs e)
		{
			new About().ShowDialog(this);
		}
	}
}