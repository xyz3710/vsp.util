﻿namespace TiffMerger
{
	partial class TiffMergerClass
	{
		private System.Windows.Forms.Button btnSource;
		private System.Windows.Forms.Button btnSave;
		private System.Windows.Forms.Button btnRoot;
		private System.Windows.Forms.Button btnExcel;
		private System.Windows.Forms.Button btnCheck;
		private System.Windows.Forms.Button btnRun;
		private System.Windows.Forms.Button btnErrorSelect;
		private System.Windows.Forms.FolderBrowserDialog sourceFolderBD;
		private System.Windows.Forms.Label lblExcel;
		private System.Windows.Forms.FolderBrowserDialog rootFolderBD;
		private System.Windows.Forms.TextBox txtSource;
		private System.Windows.Forms.TextBox txtRoot;
		private System.Windows.Forms.TextBox txtTarget;
		private System.Windows.Forms.FolderBrowserDialog targetFolderBD;
		private System.Windows.Forms.OpenFileDialog excelFileDialog;
		private System.Windows.Forms.ProgressBar pbCurrent;
		private System.Windows.Forms.ProgressBar pbTotal;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.Label lblMergingFilename;
		private System.Windows.Forms.DataGridView grdExcel;
		private System.Windows.Forms.CheckBox chkChangeSourceRollNumber;
		private System.Windows.Forms.MaskedTextBox mtxtRollNumber;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
			System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
			System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
			this.sourceFolderBD = new System.Windows.Forms.FolderBrowserDialog();
			this.txtSource = new System.Windows.Forms.TextBox();
			this.txtRoot = new System.Windows.Forms.TextBox();
			this.txtTarget = new System.Windows.Forms.TextBox();
			this.lblExcel = new System.Windows.Forms.Label();
			this.rootFolderBD = new System.Windows.Forms.FolderBrowserDialog();
			this.targetFolderBD = new System.Windows.Forms.FolderBrowserDialog();
			this.excelFileDialog = new System.Windows.Forms.OpenFileDialog();
			this.pbCurrent = new System.Windows.Forms.ProgressBar();
			this.pbTotal = new System.Windows.Forms.ProgressBar();
			this.label1 = new System.Windows.Forms.Label();
			this.label2 = new System.Windows.Forms.Label();
			this.lblMergingFilename = new System.Windows.Forms.Label();
			this.grdExcel = new System.Windows.Forms.DataGridView();
			this.chkChangeSourceRollNumber = new System.Windows.Forms.CheckBox();
			this.mtxtRollNumber = new System.Windows.Forms.MaskedTextBox();
			this.btnSource = new System.Windows.Forms.Button();
			this.btnSave = new System.Windows.Forms.Button();
			this.btnRoot = new System.Windows.Forms.Button();
			this.btnExcel = new System.Windows.Forms.Button();
			this.btnCheck = new System.Windows.Forms.Button();
			this.btnRun = new System.Windows.Forms.Button();
			this.btnErrorSelect = new System.Windows.Forms.Button();
			this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
			this.aboutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			((System.ComponentModel.ISupportInitialize)(this.grdExcel)).BeginInit();
			this.contextMenuStrip1.SuspendLayout();
			this.SuspendLayout();
			// 
			// txtSource
			// 
			this.txtSource.Anchor = System.Windows.Forms.AnchorStyles.Left;
			this.txtSource.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.txtSource.Location = new System.Drawing.Point(82, 8);
			this.txtSource.Name = "txtSource";
			this.txtSource.Size = new System.Drawing.Size(527, 23);
			this.txtSource.TabIndex = 1;
			this.txtSource.Text = "연도 이전 폴더를 선택해 주십시오.";
			// 
			// txtRoot
			// 
			this.txtRoot.Anchor = System.Windows.Forms.AnchorStyles.Left;
			this.txtRoot.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.txtRoot.Location = new System.Drawing.Point(82, 40);
			this.txtRoot.Name = "txtRoot";
			this.txtRoot.Size = new System.Drawing.Size(757, 23);
			this.txtRoot.TabIndex = 5;
			this.txtRoot.Text = "파일이 저장될 Root 경로를 지정해 주십시오.";
			// 
			// txtTarget
			// 
			this.txtTarget.Anchor = System.Windows.Forms.AnchorStyles.Left;
			this.txtTarget.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.txtTarget.Location = new System.Drawing.Point(82, 72);
			this.txtTarget.Name = "txtTarget";
			this.txtTarget.Size = new System.Drawing.Size(757, 23);
			this.txtTarget.TabIndex = 7;
			this.txtTarget.Text = "저장 폴더를 지정해 주십시오. 아래 파일 경로와 합쳐진 폴더로 저장됩니다.";
			// 
			// lblExcel
			// 
			this.lblExcel.Anchor = System.Windows.Forms.AnchorStyles.Left;
			this.lblExcel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.lblExcel.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.lblExcel.Location = new System.Drawing.Point(82, 104);
			this.lblExcel.Name = "lblExcel";
			this.lblExcel.Size = new System.Drawing.Size(324, 25);
			this.lblExcel.TabIndex = 13;
			this.lblExcel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// pbCurrent
			// 
			this.pbCurrent.Location = new System.Drawing.Point(82, 136);
			this.pbCurrent.Maximum = 1;
			this.pbCurrent.Name = "pbCurrent";
			this.pbCurrent.Size = new System.Drawing.Size(423, 23);
			this.pbCurrent.Step = 1;
			this.pbCurrent.TabIndex = 16;
			// 
			// pbTotal
			// 
			this.pbTotal.Location = new System.Drawing.Point(585, 136);
			this.pbTotal.Name = "pbTotal";
			this.pbTotal.Size = new System.Drawing.Size(423, 23);
			this.pbTotal.Step = 1;
			this.pbTotal.TabIndex = 18;
			// 
			// label1
			// 
			this.label1.Anchor = System.Windows.Forms.AnchorStyles.Left;
			this.label1.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label1.Location = new System.Drawing.Point(8, 136);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(68, 23);
			this.label1.TabIndex = 15;
			this.label1.Text = "현재 >>>";
			this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// label2
			// 
			this.label2.Anchor = System.Windows.Forms.AnchorStyles.Left;
			this.label2.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label2.Location = new System.Drawing.Point(511, 136);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(68, 23);
			this.label2.TabIndex = 17;
			this.label2.Text = "전체 >>>";
			this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// lblMergingFilename
			// 
			this.lblMergingFilename.Anchor = System.Windows.Forms.AnchorStyles.Left;
			this.lblMergingFilename.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.lblMergingFilename.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.lblMergingFilename.Location = new System.Drawing.Point(412, 104);
			this.lblMergingFilename.Name = "lblMergingFilename";
			this.lblMergingFilename.Size = new System.Drawing.Size(596, 25);
			this.lblMergingFilename.TabIndex = 14;
			this.lblMergingFilename.Text = "현재 진행 중인 파일을 보여줍니다.";
			this.lblMergingFilename.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// grdExcel
			// 
			this.grdExcel.AllowUserToAddRows = false;
			this.grdExcel.AllowUserToDeleteRows = false;
			this.grdExcel.AllowUserToResizeRows = false;
			this.grdExcel.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.grdExcel.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
			this.grdExcel.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.grdExcel.ClipboardCopyMode = System.Windows.Forms.DataGridViewClipboardCopyMode.EnableAlwaysIncludeHeaderText;
			dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
			dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
			dataGridViewCellStyle1.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
			dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
			dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
			dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
			this.grdExcel.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
			this.grdExcel.ColumnHeadersHeight = 22;
			this.grdExcel.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
			dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
			dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window;
			dataGridViewCellStyle2.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText;
			dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
			dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
			dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
			this.grdExcel.DefaultCellStyle = dataGridViewCellStyle2;
			this.grdExcel.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
			this.grdExcel.Location = new System.Drawing.Point(8, 168);
			this.grdExcel.Name = "grdExcel";
			this.grdExcel.ReadOnly = true;
			dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
			dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Control;
			dataGridViewCellStyle3.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.WindowText;
			dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
			dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
			dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
			this.grdExcel.RowHeadersDefaultCellStyle = dataGridViewCellStyle3;
			this.grdExcel.RowTemplate.Height = 23;
			this.grdExcel.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
			this.grdExcel.Size = new System.Drawing.Size(1000, 537);
			this.grdExcel.TabIndex = 12;
			this.grdExcel.TabStop = false;
			// 
			// chkChangeSourceRollNumber
			// 
			this.chkChangeSourceRollNumber.Appearance = System.Windows.Forms.Appearance.Button;
			this.chkChangeSourceRollNumber.AutoSize = true;
			this.chkChangeSourceRollNumber.CheckAlign = System.Drawing.ContentAlignment.MiddleCenter;
			this.chkChangeSourceRollNumber.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.chkChangeSourceRollNumber.Location = new System.Drawing.Point(615, 8);
			this.chkChangeSourceRollNumber.Name = "chkChangeSourceRollNumber";
			this.chkChangeSourceRollNumber.Size = new System.Drawing.Size(141, 26);
			this.chkChangeSourceRollNumber.TabIndex = 2;
			this.chkChangeSourceRollNumber.Text = "소스 Roll 경로 수정";
			this.chkChangeSourceRollNumber.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			this.chkChangeSourceRollNumber.UseVisualStyleBackColor = true;
			this.chkChangeSourceRollNumber.CheckedChanged += new System.EventHandler(this.chkChangeSourceRollNumber_CheckedChanged);
			// 
			// mtxtRollNumber
			// 
			this.mtxtRollNumber.AsciiOnly = true;
			this.mtxtRollNumber.Location = new System.Drawing.Point(757, 8);
			this.mtxtRollNumber.Mask = "0000-0000";
			this.mtxtRollNumber.Name = "mtxtRollNumber";
			this.mtxtRollNumber.Size = new System.Drawing.Size(82, 26);
			this.mtxtRollNumber.TabIndex = 3;
			this.mtxtRollNumber.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
			// 
			// btnSource
			// 
			this.btnSource.Anchor = System.Windows.Forms.AnchorStyles.Left;
			this.btnSource.BackColor = System.Drawing.Color.Transparent;
			this.btnSource.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.btnSource.Image = global::TiffMerger.Properties.Resources.btnSource;
			this.btnSource.Location = new System.Drawing.Point(8, 8);
			this.btnSource.Name = "btnSource";
			this.btnSource.Size = new System.Drawing.Size(68, 26);
			this.btnSource.TabIndex = 0;
			this.btnSource.UseVisualStyleBackColor = false;
			this.btnSource.Click += new System.EventHandler(this.btnSource_Click);
			// 
			// btnSave
			// 
			this.btnSave.Anchor = System.Windows.Forms.AnchorStyles.Left;
			this.btnSave.BackColor = System.Drawing.Color.Transparent;
			this.btnSave.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.btnSave.Image = global::TiffMerger.Properties.Resources.btnSave;
			this.btnSave.Location = new System.Drawing.Point(8, 72);
			this.btnSave.Name = "btnSave";
			this.btnSave.Size = new System.Drawing.Size(68, 26);
			this.btnSave.TabIndex = 6;
			this.btnSave.UseVisualStyleBackColor = false;
			this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
			// 
			// btnRoot
			// 
			this.btnRoot.Anchor = System.Windows.Forms.AnchorStyles.Left;
			this.btnRoot.BackColor = System.Drawing.Color.Transparent;
			this.btnRoot.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.btnRoot.Image = global::TiffMerger.Properties.Resources.btnRoot;
			this.btnRoot.Location = new System.Drawing.Point(8, 40);
			this.btnRoot.Name = "btnRoot";
			this.btnRoot.Size = new System.Drawing.Size(68, 26);
			this.btnRoot.TabIndex = 4;
			this.btnRoot.UseVisualStyleBackColor = false;
			this.btnRoot.Click += new System.EventHandler(this.btnRoot_Click);
			// 
			// btnExcel
			// 
			this.btnExcel.Anchor = System.Windows.Forms.AnchorStyles.Left;
			this.btnExcel.BackColor = System.Drawing.Color.Transparent;
			this.btnExcel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.btnExcel.Image = global::TiffMerger.Properties.Resources.btnExcel;
			this.btnExcel.Location = new System.Drawing.Point(8, 104);
			this.btnExcel.Name = "btnExcel";
			this.btnExcel.Size = new System.Drawing.Size(68, 26);
			this.btnExcel.TabIndex = 8;
			this.btnExcel.UseVisualStyleBackColor = false;
			this.btnExcel.Click += new System.EventHandler(this.btnExcel_Click);
			// 
			// btnCheck
			// 
			this.btnCheck.Anchor = System.Windows.Forms.AnchorStyles.Right;
			this.btnCheck.BackColor = System.Drawing.Color.Transparent;
			this.btnCheck.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.btnCheck.Image = global::TiffMerger.Properties.Resources.btnCheck;
			this.btnCheck.Location = new System.Drawing.Point(858, 8);
			this.btnCheck.Name = "btnCheck";
			this.btnCheck.Size = new System.Drawing.Size(68, 26);
			this.btnCheck.TabIndex = 9;
			this.btnCheck.UseVisualStyleBackColor = false;
			this.btnCheck.Click += new System.EventHandler(this.btnCheck_Click);
			// 
			// btnRun
			// 
			this.btnRun.Anchor = System.Windows.Forms.AnchorStyles.Right;
			this.btnRun.BackColor = System.Drawing.Color.Transparent;
			this.btnRun.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.btnRun.Image = global::TiffMerger.Properties.Resources.btnRun;
			this.btnRun.Location = new System.Drawing.Point(858, 40);
			this.btnRun.Name = "btnRun";
			this.btnRun.Size = new System.Drawing.Size(150, 58);
			this.btnRun.TabIndex = 10;
			this.btnRun.UseVisualStyleBackColor = false;
			this.btnRun.Click += new System.EventHandler(this.btnRun_Click);
			// 
			// btnErrorSelect
			// 
			this.btnErrorSelect.Anchor = System.Windows.Forms.AnchorStyles.Right;
			this.btnErrorSelect.BackColor = System.Drawing.Color.Transparent;
			this.btnErrorSelect.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.btnErrorSelect.Image = global::TiffMerger.Properties.Resources.btnErrorSelect;
			this.btnErrorSelect.Location = new System.Drawing.Point(940, 8);
			this.btnErrorSelect.Name = "btnErrorSelect";
			this.btnErrorSelect.Size = new System.Drawing.Size(68, 26);
			this.btnErrorSelect.TabIndex = 11;
			this.btnErrorSelect.UseVisualStyleBackColor = false;
			this.btnErrorSelect.Click += new System.EventHandler(this.btnErrorSelect_Click);
			// 
			// contextMenuStrip1
			// 
			this.contextMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.aboutToolStripMenuItem});
			this.contextMenuStrip1.Name = "contextMenuStrip1";
			this.contextMenuStrip1.Size = new System.Drawing.Size(109, 26);
			// 
			// aboutToolStripMenuItem
			// 
			this.aboutToolStripMenuItem.Name = "aboutToolStripMenuItem";
			this.aboutToolStripMenuItem.Size = new System.Drawing.Size(108, 22);
			this.aboutToolStripMenuItem.Text = "About";
			this.aboutToolStripMenuItem.Click += new System.EventHandler(this.aboutToolStripMenuItem_Click);
			// 
			// TiffMergerClass
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(7, 19);
			this.ClientSize = new System.Drawing.Size(1016, 711);
			this.ContextMenuStrip = this.contextMenuStrip1;
			this.Controls.Add(this.mtxtRollNumber);
			this.Controls.Add(this.chkChangeSourceRollNumber);
			this.Controls.Add(this.grdExcel);
			this.Controls.Add(this.pbTotal);
			this.Controls.Add(this.pbCurrent);
			this.Controls.Add(this.label2);
			this.Controls.Add(this.label1);
			this.Controls.Add(this.lblMergingFilename);
			this.Controls.Add(this.lblExcel);
			this.Controls.Add(this.txtSource);
			this.Controls.Add(this.txtRoot);
			this.Controls.Add(this.txtTarget);
			this.Controls.Add(this.btnSource);
			this.Controls.Add(this.btnSave);
			this.Controls.Add(this.btnRoot);
			this.Controls.Add(this.btnExcel);
			this.Controls.Add(this.btnCheck);
			this.Controls.Add(this.btnRun);
			this.Controls.Add(this.btnErrorSelect);
			this.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.MaximizeBox = false;
			this.Name = "TiffMergerClass";
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Text = "Tiff Merger v1.3 by Kim Ki Won in 2006.10.24";
			this.Load += new System.EventHandler(this.TiffMergerClass_Load);
			((System.ComponentModel.ISupportInitialize)(this.grdExcel)).EndInit();
			this.contextMenuStrip1.ResumeLayout(false);
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
		private System.Windows.Forms.ToolStripMenuItem aboutToolStripMenuItem;

	}
}

