/**********************************************************************************************************************/
/*	Name		:	TiffMerger.Tiff.Merger
/*	Purpose		:	Tiff를 Merge 해서 CompressionCCITT4로 압축된 Multiframe 단일 파일을 만드는 class 입니다.
/*	Creator		:	Kim Ki Won
/*	Create		:	2006년 9월 26일 화요일 오전 9:39:40
/*	Modifier	:	
/*	Update		:	2006년 9월 26일 화요일
/*	Comment		:	다음 예제는 Bitmap 개체(BMP 파일로부터 하나, JPEG 파일로부터 하나, PNG 파일로부터 하나)를 만듭니다. 
/*					MSDN에 나와있는 샘플을 수정하여 여러개의 TIFF 파일을 압축(CompressionCCITT4)된 Multiframe 파일을 만듭니다.
/*					해당 코드에서는 세 이미지 모두를 여러 프레임으로 구성된 단일 TIFF 파일로 저장합니다.
/**********************************************************************************************************************/

using System;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;

namespace TiffMerger.Tiff
{
	/// <summary>
	/// Tiff를 Merge 해서 CompressionCCITT4로 압축된 Multiframe 단일 파일을 만드는 class 입니다.
	/// </summary>
	public class Merger
	{
		/// <summary>
		/// Merge precess를 처리하는 Handler입니다.
		/// </summary>
		/// <param name="mergedFilename">Merge가 일어날 때의 Filename</param>
		/// <param name="currentPage">현재 page</param>
		public delegate void MergeProcessHandeler(string mergedFilename, int currentPage);

		public static event MergeProcessHandeler Merging;

		protected static void OnMerge(string mergedFilename, int currentPage)
		{
			if (Merging != null)
				Merging(mergedFilename, currentPage);
		}
		
		#region Use Singleton Pattern
		private static Merger _newInstance;

		#region Constructor for Single Ton Pattern
		/// <summary>
		/// Constructor for Single Ton Pattern
		/// </summary>
		/// <returns></returns>
		private Merger()
		{
			_newInstance = null;
			
		}
		#endregion

		#region GetInstance
		/// <summary>
		/// Create unique instance
		/// </summary>
		/// <returns></returns>
		public static Merger GetInstance()
		{
			if (_newInstance == null)
				_newInstance = new Merger();

			return _newInstance;
		}
		#endregion
		#endregion
        
		/// <summary>
		/// Tiff를 Merge 해서 CompressionCCITT4로 압축된 Multiframe 단일 파일을 만듭니다.
		/// </summary>
		/// <param name="sourcePath">소스 경로(@\를 포함해야 합니다.)</param>
		/// <param name="startPage">시작 페이지 번호</param>
		/// <param name="totalPages">merge할 페이지 수</param>
		/// <param name="mergedFilename">저장될 파일 이름</param>
		/// <returns>성공 여부를 되돌립니다.</returns>
		public bool TiffMerge(string sourcePath, int startPage, int totalPages, string mergedFilename)
		{
			string firstFilename = string.Format("{0}{1}.tif", sourcePath, startPage.ToString("0000"));
			int currentPage = 1;
			bool ret = true;

			ret = isExist(firstFilename);

			try
			{
				// Get an ImageCodecInfo object that represents the TIFF codec.
				ImageCodecInfo imageCodecInfo = GetEncoderInfo("image/tiff");

				// Create an Encoder object based on the GUID
				// for the SaveFlag parameter category.
				Encoder encSave = Encoder.SaveFlag;
				Encoder encComp = Encoder.Compression;

				// Create an EncoderParameters object.
				// An EncoderParameters object has an array of EncoderParameter
				// objects. In this case, there is only one
				// EncoderParameter object in the array.
				// 압축된 Multiframe으로 만든다.
				EncoderParameters encoderParameters = new EncoderParameters(2);
				EncoderParameter epSave = new EncoderParameter(encSave, (long)EncoderValue.MultiFrame);
				EncoderParameter epComp = new EncoderParameter(encComp, (long)EncoderValue.CompressionCCITT4);

				// Create Bitmap objects.
				Bitmap firstPage = new Bitmap(firstFilename);

				// Save the first page (frame).
				encoderParameters.Param[0] = epSave;
				encoderParameters.Param[1] = epComp;

				firstPage.Save(mergedFilename, imageCodecInfo, encoderParameters);

				// Event 발생
				Merging(firstFilename, currentPage++);

				for (int i = startPage + 1; i < startPage + totalPages; i++)
				{
					string nPageFilename = string.Format("{0}{1}.tif", sourcePath, i.ToString("0000"));
					Bitmap nPage = new Bitmap(nPageFilename);

					ret = isExist(nPageFilename);

					// Save the next page (frame).
					encoderParameters.Param[0] =
						new EncoderParameter(encSave, (long)EncoderValue.FrameDimensionPage);

					firstPage.SaveAdd(nPage, encoderParameters);

					// Event 발생
					Merging(nPageFilename, currentPage++);
					
					nPage.Dispose();
				}

				// Close the multiple-frame file.
				encoderParameters.Param[0] = new EncoderParameter(encSave, (long)EncoderValue.Flush);

				firstPage.SaveAdd(encoderParameters);
				firstPage.Dispose();
				encoderParameters.Dispose();
			}
			catch (Exception)
			{
				ret = false;
			}

			return ret; 
		}

		private bool isExist(string filePath)
		{
			return File.Exists(filePath); 
		}

		private ImageCodecInfo GetEncoderInfo(String mimeType)
		{
			ImageCodecInfo[] encoders = ImageCodecInfo.GetImageEncoders();

			for(int i = 0; i < encoders.Length; ++i)
			{
				if(encoders[i].MimeType == mimeType)
					return encoders[i];
			}

			return null;
		}

		#region compressLastPage(not used)
//		/// <summary>
//		/// single frame 파일로 압축만을 할 때 사용합니다.
//		/// </summary>
//		/// <param name="mergedFilename"></param>
//		private void compressLastPage(string mergedFilename)
//		{
//			string tmpFilename = string.Format("{0}mergedTempFile.tif", _destPath);
//            
//			// compress last file
//			// Create a Bitmap object based on a BMP file.
//			Bitmap lastPage = new Bitmap(mergedFilename);
//			// Get an ImageCodecInfo object that represents the TIFF codec.
//			ImageCodecInfo lastImageCodecInfo = GetEncoderInfo("image/tiff");
//			// Create an Encoder object based on the GUID
//			// for the Compression parameter category.
//			Encoder lastEncoder = Encoder.Compression;
//			// Create an EncoderParameters object.
//			// An EncoderParameters object has an array of EncoderParameter
//			// objects. In this case, there is only one
//			// EncoderParameter object in the array.
//			EncoderParameters lastEncoderParameters = new EncoderParameters(1);
//			// Save the bitmap as a TIFF file with LZW compression.
//			EncoderParameter lastEncoderParameter =
//				new EncoderParameter(lastEncoder, (long)EncoderValue.CompressionLZW);
//            
//			lastEncoderParameters.Param[0] = lastEncoderParameter;
//			lastPage.Save(tmpFilename, lastImageCodecInfo, lastEncoderParameters);
//
//			lastPage.Dispose();
//
//			File.Copy(tmpFilename, mergedFilename, true);
//			File.Delete(tmpFilename);
//		}
		#endregion
	}
}