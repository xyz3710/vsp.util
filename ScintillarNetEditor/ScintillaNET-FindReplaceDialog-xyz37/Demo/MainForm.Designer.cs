﻿namespace Demo
{
	partial class MainForm
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
			this.scintilla1 = new ScintillaNET.Scintilla();
			this.GotoButton = new System.Windows.Forms.Button();
			this.splitContainer1 = new System.Windows.Forms.SplitContainer();
			this.tabControl1 = new System.Windows.Forms.TabControl();
			this.tabPage1 = new System.Windows.Forms.TabPage();
			this.tabPage2 = new System.Windows.Forms.TabPage();
			this.scintilla2 = new ScintillaNET.Scintilla();
			this.findAllResultsPanel1 = new ScintillaNET.FindReplaceHelpers.FindAllResultsPanel();
			this.incrementalSearcher1 = new ScintillaNET.FindReplaceHelpers.IncrementalFinder();
			this.cmMenu = new System.Windows.Forms.ContextMenuStrip(this.components);
			this.tsCopy = new System.Windows.Forms.ToolStripMenuItem();
			this.tsCut = new System.Windows.Forms.ToolStripMenuItem();
			((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
			this.splitContainer1.Panel1.SuspendLayout();
			this.splitContainer1.Panel2.SuspendLayout();
			this.splitContainer1.SuspendLayout();
			this.tabControl1.SuspendLayout();
			this.tabPage1.SuspendLayout();
			this.tabPage2.SuspendLayout();
			this.cmMenu.SuspendLayout();
			this.SuspendLayout();
			// 
			// scintilla1
			// 
			this.scintilla1.AnnotationVisible = ScintillaNET.Annotation.Standard;
			this.scintilla1.AutoCMaxHeight = 9;
			this.scintilla1.AutomaticFold = ((ScintillaNET.AutomaticFold)(((ScintillaNET.AutomaticFold.Show | ScintillaNET.AutomaticFold.Click)
			| ScintillaNET.AutomaticFold.Change)));
			this.scintilla1.BiDirectionality = ScintillaNET.BiDirectionalDisplayType.Disabled;
			this.scintilla1.BufferedDraw = false;
			this.scintilla1.CaretLineBackColor = System.Drawing.Color.Orange;
			this.scintilla1.CaretLineFrame = 2;
			this.scintilla1.CaretLineVisible = true;
			this.scintilla1.CaretLineVisibleAlways = true;
			this.scintilla1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.scintilla1.EdgeColor = System.Drawing.Color.IndianRed;
			this.scintilla1.EdgeColumn = 1;
			this.scintilla1.EdgeMode = ScintillaNET.EdgeMode.MultiLine;
			this.scintilla1.Font = new System.Drawing.Font("D2Coding", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
			this.scintilla1.LexerName = null;
			this.scintilla1.Location = new System.Drawing.Point(4, 3);
			this.scintilla1.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
			this.scintilla1.Name = "scintilla1";
			this.scintilla1.ScrollWidth = 5001;
			this.scintilla1.Size = new System.Drawing.Size(1383, 401);
			this.scintilla1.TabIndents = true;
			this.scintilla1.TabIndex = 0;
			this.scintilla1.Technology = ScintillaNET.Technology.DirectWrite;
			this.scintilla1.Text = resources.GetString("scintilla1.Text");
			this.scintilla1.UseRightToLeftReadingLayout = false;
			this.scintilla1.UseTabs = true;
			this.scintilla1.WrapMode = ScintillaNET.WrapMode.None;
			this.scintilla1.Enter += new System.EventHandler(this.genericScintilla1_Enter);
			this.scintilla1.KeyDown += new System.Windows.Forms.KeyEventHandler(this.genericScintilla_KeyDown);
			// 
			// GotoButton
			// 
			this.GotoButton.Location = new System.Drawing.Point(852, 39);
			this.GotoButton.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
			this.GotoButton.Name = "GotoButton";
			this.GotoButton.Size = new System.Drawing.Size(88, 27);
			this.GotoButton.TabIndex = 2;
			this.GotoButton.Text = "Goto";
			this.GotoButton.UseVisualStyleBackColor = true;
			this.GotoButton.Click += new System.EventHandler(this.GotoButton_Click);
			// 
			// splitContainer1
			// 
			this.splitContainer1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
			| System.Windows.Forms.AnchorStyles.Left)
			| System.Windows.Forms.AnchorStyles.Right)));
			this.splitContainer1.Location = new System.Drawing.Point(14, 39);
			this.splitContainer1.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
			this.splitContainer1.Name = "splitContainer1";
			this.splitContainer1.Orientation = System.Windows.Forms.Orientation.Horizontal;
			// 
			// splitContainer1.Panel1
			// 
			this.splitContainer1.Panel1.Controls.Add(this.tabControl1);
			// 
			// splitContainer1.Panel2
			// 
			this.splitContainer1.Panel2.Controls.Add(this.findAllResultsPanel1);
			this.splitContainer1.Size = new System.Drawing.Size(1399, 719);
			this.splitContainer1.SplitterDistance = 435;
			this.splitContainer1.SplitterWidth = 5;
			this.splitContainer1.TabIndex = 4;
			// 
			// tabControl1
			// 
			this.tabControl1.Controls.Add(this.tabPage1);
			this.tabControl1.Controls.Add(this.tabPage2);
			this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.tabControl1.Location = new System.Drawing.Point(0, 0);
			this.tabControl1.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
			this.tabControl1.Name = "tabControl1";
			this.tabControl1.SelectedIndex = 0;
			this.tabControl1.Size = new System.Drawing.Size(1399, 435);
			this.tabControl1.TabIndex = 5;
			// 
			// tabPage1
			// 
			this.tabPage1.Controls.Add(this.scintilla1);
			this.tabPage1.Location = new System.Drawing.Point(4, 24);
			this.tabPage1.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
			this.tabPage1.Name = "tabPage1";
			this.tabPage1.Padding = new System.Windows.Forms.Padding(4, 3, 4, 3);
			this.tabPage1.Size = new System.Drawing.Size(1391, 407);
			this.tabPage1.TabIndex = 0;
			this.tabPage1.Text = "tabPage1";
			this.tabPage1.UseVisualStyleBackColor = true;
			// 
			// tabPage2
			// 
			this.tabPage2.Controls.Add(this.scintilla2);
			this.tabPage2.Location = new System.Drawing.Point(4, 24);
			this.tabPage2.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
			this.tabPage2.Name = "tabPage2";
			this.tabPage2.Padding = new System.Windows.Forms.Padding(4, 3, 4, 3);
			this.tabPage2.Size = new System.Drawing.Size(1391, 407);
			this.tabPage2.TabIndex = 1;
			this.tabPage2.Text = "tabPage2";
			this.tabPage2.UseVisualStyleBackColor = true;
			// 
			// scintilla2
			// 
			this.scintilla2.AutoCMaxHeight = 9;
			this.scintilla2.BiDirectionality = ScintillaNET.BiDirectionalDisplayType.Disabled;
			this.scintilla2.CaretLineBackColor = System.Drawing.Color.Black;
			this.scintilla2.CaretLineVisible = true;
			this.scintilla2.Dock = System.Windows.Forms.DockStyle.Fill;
			this.scintilla2.Font = new System.Drawing.Font("D2Coding", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
			this.scintilla2.LexerName = null;
			this.scintilla2.Location = new System.Drawing.Point(4, 3);
			this.scintilla2.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
			this.scintilla2.Name = "scintilla2";
			this.scintilla2.ScrollWidth = 5001;
			this.scintilla2.Size = new System.Drawing.Size(1383, 401);
			this.scintilla2.TabIndents = true;
			this.scintilla2.TabIndex = 1;
			this.scintilla2.Text = resources.GetString("scintilla2.Text");
			this.scintilla2.UseRightToLeftReadingLayout = false;
			this.scintilla2.WrapMode = ScintillaNET.WrapMode.None;
			this.scintilla2.Enter += new System.EventHandler(this.genericScintilla1_Enter);
			this.scintilla2.KeyDown += new System.Windows.Forms.KeyEventHandler(this.genericScintilla_KeyDown);
			// 
			// findAllResultsPanel1
			// 
			this.findAllResultsPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.findAllResultsPanel1.Location = new System.Drawing.Point(0, 0);
			this.findAllResultsPanel1.Margin = new System.Windows.Forms.Padding(5, 3, 5, 3);
			this.findAllResultsPanel1.Name = "findAllResultsPanel1";
			this.findAllResultsPanel1.Scintilla = null;
			this.findAllResultsPanel1.Size = new System.Drawing.Size(1399, 279);
			this.findAllResultsPanel1.TabIndex = 3;
			// 
			// incrementalSearcher1
			// 
			this.incrementalSearcher1.AutoPosition = false;
			this.incrementalSearcher1.AutoSize = true;
			this.incrementalSearcher1.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
			this.incrementalSearcher1.BackColor = System.Drawing.Color.Transparent;
			this.incrementalSearcher1.FindReplace = null;
			this.incrementalSearcher1.Location = new System.Drawing.Point(10, 10);
			this.incrementalSearcher1.Margin = new System.Windows.Forms.Padding(0);
			this.incrementalSearcher1.Name = "incrementalSearcher1";
			this.incrementalSearcher1.Scintilla = null;
			this.incrementalSearcher1.Size = new System.Drawing.Size(287, 24);
			this.incrementalSearcher1.TabIndex = 1;
			this.incrementalSearcher1.ToolItem = true;
			// 
			// cmMenu
			// 
			this.cmMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
			this.tsCopy,
			this.tsCut});
			this.cmMenu.Name = "cmMenu";
			this.cmMenu.Size = new System.Drawing.Size(181, 70);
			// 
			// tsCopy
			// 
			this.tsCopy.Name = "tsCopy";
			this.tsCopy.Size = new System.Drawing.Size(180, 22);
			this.tsCopy.Text = "Copy";
			// 
			// tsCut
			// 
			this.tsCut.Name = "tsCut";
			this.tsCut.Size = new System.Drawing.Size(180, 22);
			this.tsCut.Text = "Cut";
			// 
			// MainForm
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(1511, 772);
			this.Controls.Add(this.splitContainer1);
			this.Controls.Add(this.GotoButton);
			this.Controls.Add(this.incrementalSearcher1);
			this.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
			this.Name = "MainForm";
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Text = "Form1";
			this.splitContainer1.Panel1.ResumeLayout(false);
			this.splitContainer1.Panel2.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
			this.splitContainer1.ResumeLayout(false);
			this.tabControl1.ResumeLayout(false);
			this.tabPage1.ResumeLayout(false);
			this.tabPage2.ResumeLayout(false);
			this.cmMenu.ResumeLayout(false);
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private ScintillaNET.Scintilla scintilla1;
		private ScintillaNET.FindReplaceHelpers.IncrementalFinder incrementalSearcher1;
		private System.Windows.Forms.Button GotoButton;
		private ScintillaNET.FindReplaceHelpers.FindAllResultsPanel findAllResultsPanel1;
		private System.Windows.Forms.SplitContainer splitContainer1;
		private System.Windows.Forms.TabControl tabControl1;
		private System.Windows.Forms.TabPage tabPage1;
		private System.Windows.Forms.TabPage tabPage2;
		private ScintillaNET.Scintilla scintilla2;
		private System.Windows.Forms.ContextMenuStrip cmMenu;
		private System.Windows.Forms.ToolStripMenuItem tsCopy;
		private System.Windows.Forms.ToolStripMenuItem tsCut;
	}
}

