﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace ScintillaNET.LexerHelpers
{
	/// <summary>
	/// The lang model wrapper.
	/// </summary>
	[XmlRoot("NotepadPlus")]
	[Serializable]
	[System.Diagnostics.DebuggerDisplay("Languages:{Languages}", Name = "LangModelWrapper")]
	public class LangModelWrapper
	{
		/// <summary>
		/// Initializes a new instance of the <see cref="LangModelWrapper"/> class.
		/// </summary>
		public LangModelWrapper()
		{
			Languages = new List<Language>();
		}

		/// <summary>
		/// Gets or sets the languages.
		/// </summary>
		public List<Language> Languages { get; set; }
	}

	/// <summary>
	/// The language element.
	/// </summary>
	[System.Diagnostics.DebuggerDisplay("Name:{Name}, Ext:{Extensions}, CommentLine:{CommentLine}, CommentStart:{CommentStart}, CommentEnd:{CommentEnd}, Keywords:{Keywords}", Name = "Language")]
	public class Language
	{
		/// <summary>
		/// Initializes a new instance of the <see cref="Language"/> class.
		/// </summary>
		public Language()
		{
			Keywords = new List<Keyword>();
		}

		/// <summary>
		/// Gets or sets the name.
		/// </summary>
		[XmlAttribute("name")]
		public string Name { get; set; }

		/// <summary>
		/// Gets or sets the file extensions.
		/// </summary>
		[XmlAttribute("ext")]
		public string Extensions { get; set; }

		/// <summary>
		/// Gets or sets the comment line characters.
		/// </summary>
		[XmlAttribute("commentLine")]
		public string CommentLine { get; set; }

		/// <summary>
		/// Gets or sets the block comment start characters. for example /*
		/// </summary>
		[XmlAttribute("commentStart")]
		public string CommentStart { get; set; }

		/// <summary>
		/// Gets or sets the block comment end characters. for example */
		/// </summary>
		[XmlAttribute("commentEnd")]
		public string CommentEnd { get; set; }

		/// <summary>
		/// Gets or sets the keywords.
		/// </summary>
		[XmlElement]
		public List<Keyword> Keywords { get; set; }
	}

	/// <summary>
	/// The keywords.
	/// </summary>
	[System.Diagnostics.DebuggerDisplay("Name:{Name}, Value:{Value}", Name = "Keyword")]
	public class Keyword
	{
		/// <summary>
		/// Gets or sets the name.
		/// </summary>
		[XmlAttribute("name")]
		public string Name { get; set; }

		/// <summary>
		/// Gets or sets the values.
		/// </summary>
		[XmlText]
		public string Value { get; set; }
	}
}
