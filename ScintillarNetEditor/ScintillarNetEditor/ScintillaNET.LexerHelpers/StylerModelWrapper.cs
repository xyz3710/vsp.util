﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace ScintillaNET.LexerHelpers
{
	/// <summary>
	/// The style model wrapper.
	/// </summary>
	[XmlRoot("NotepadPlus")]
	[Serializable]
	[System.Diagnostics.DebuggerDisplay("LexerStyles:{LexerStyles}, GlobalStyles:{GlobalStyles}", Name = "StylerModelWrapper")]
	public class StylerModelWrapper
	{
		/// <summary>
		/// Initializes a new instance of the <see cref="StylerModelWrapper"/> class.
		/// </summary>
		public StylerModelWrapper()
		{
			LexerStyles = new List<LexerType>();
			GlobalStyles = new List<LexerStyle>();
		}

		/// <summary>
		/// Gets or sets the lexer types.
		/// </summary>
		public List<LexerType> LexerStyles { get; set; }

		/// <summary>
		/// Gets or sets the global styles.
		/// </summary>
		[XmlArrayItem("WidgetStyle")]
		public List<LexerStyle> GlobalStyles { get; set; }
	}

	/// <summary>
	/// The language element.
	/// </summary>
	[System.Diagnostics.DebuggerDisplay("Name:{Name}, Description:{Description}, Ext:{Extensions}, WordsStyles:{WordsStyles}", Name = "LexerType")]
	public class LexerType
	{
		/// <summary>
		/// Initializes a new instance of the <see cref="LexerType"/> class.
		/// </summary>
		public LexerType()
		{
			WordsStyles = new List<LexerStyle>();
		}

		/// <summary>
		/// Gets or sets the name.
		/// </summary>
		[XmlAttribute("name")]
		public string Name { get; set; }

		/// <summary>
		/// Gets or sets the description.
		/// </summary>
		[XmlAttribute("desc")]
		public string Description { get; set; }

		/// <summary>
		/// Gets or sets the file extensions.
		/// </summary>
		[XmlAttribute("ext")]
		public string Extensions { get; set; }

		/// <summary>
		/// Gets or sets the word styles.
		/// </summary>
		[XmlElement(ElementName = "WordsStyle")]
		public List<LexerStyle> WordsStyles { get; set; }
	}

	/// <summary>
	/// The lexer style.
	/// </summary>
	[System.Diagnostics.DebuggerDisplay("Name:{Name}, StyleId:{StyleId}, ForeColor:{ForeColor}, BackColor:{BackColor}, FontName:{FontName}, FontStyle:{FontStyle}, FontSize:{FontSize}, KeywordClass:{KeywordClass}", Name = "LexerStyle")]
	public class LexerStyle
	{
		/// <summary>
		/// Gets or sets the name.
		/// </summary>
		[XmlAttribute("name")]
		public string Name { get; set; }

		/// <summary>
		/// Gets or sets the style id.
		/// </summary>
		[XmlAttribute("styleID")]
		public int StyleId { get; set; }

		/// <summary>
		/// Gets or sets the foreground color string.
		/// </summary>
		[XmlAttribute("fgColor")]
		public string ForegroundColor { get; set; }

		/// <summary>
		/// Gets the foreground color.
		/// </summary>
		public Color ForeColor
		{
			get
			{
				if (string.IsNullOrEmpty(ForegroundColor) == true)
				{
					return Color.Empty;
				}

				return ColorTranslator.FromHtml($"#{ForegroundColor}");
			}
		}

		/// <summary>
		/// Gets or sets the background color string.
		/// </summary>
		[XmlAttribute("bgColor")]
		public string BackgroundColor { get; set; }

		/// <summary>
		/// Gets the background color.
		/// </summary>
		public Color BackColor
		{
			get
			{
				if (string.IsNullOrEmpty(BackgroundColor) == true)
				{
					return Color.Empty;
				}

				return ColorTranslator.FromHtml($"#{BackgroundColor}");
			}
		}

		/// <summary>
		/// Gets or sets the font name.
		/// </summary>
		[XmlAttribute("fontName")]
		public string FontName { get; set; }

		/// <summary>
		/// Gets or sets the font style.
		/// </summary>
		[XmlAttribute("fontStyle")]
		public int FontStyle { get; set; }

		/// <summary>
		/// Gets the font styles enum.
		/// </summary>
		public FontStyles FontStyles
		{
			get
			{
				return (FontStyles)FontStyle;
			}
		}

		/// <summary>
		/// Gets or sets the font size.
		/// </summary>
		[XmlAttribute("fontSize")]
		public string FontSize { get; set; }

		/// <summary>
		/// Gets or sets the keyword class.
		/// </summary>
		[XmlAttribute("keywordClass")]
		public string KeywordClass { get; set; }
	}

	/// <summary>
	/// The font styles enum.
	/// </summary>
	[Flags]
	public enum FontStyles
	{
		/// <summary>
		/// Normal font
		/// </summary>
		Normal = 0,
		/// <summary>
		/// Bold style font
		/// </summary>
		Bold = 1,
		/// <summary>
		/// Italic style font
		/// </summary>
		Italic = 2,
		/// <summary>
		/// Underline style font
		/// </summary>
		Underline = 4,
	}
}
