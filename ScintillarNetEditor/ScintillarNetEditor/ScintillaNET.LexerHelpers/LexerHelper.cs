﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ScintillaNET.LexerHelpers
{
	/// <summary>
	/// The lexer helper.
	/// </summary>
	public static class LexerHelper
	{
		/// <summary>
		/// Creates the lexer from npp.
		/// </summary>
		/// <param name="scintilla">The scintilla.</param>
		/// <param name="nppReader">The npp reader.</param>
		/// <param name="lexerType">The lexer type.</param>
		/// <param name="useGlobalOverride">If true, use global override.</param>
		/// <returns>A bool.</returns>
		public static bool CreateLexerFromNpp(this Scintilla scintilla, NppReader nppReader, LexerTypes lexerType, bool useGlobalOverride = true)
		{
			try
			{
				scintilla.SetGlobalDefaultStyles(nppReader, useGlobalOverride);
				scintilla.LexerName = lexerType.GetLexerName();
				nppReader.SetFilter(lexerType);

				scintilla.SetLexerStyles(nppReader.StylerModel);
				scintilla.SetLexerStyles(nppReader.ThemeStylerModel);

				var language = nppReader.LangModel.Languages.FirstOrDefault();

				if (language != null)
				{
					for (int i = 0; i < language.Keywords.Count; i++)
					{
						scintilla.SetKeywords(i, language.Keywords[i].Value);
					}
				}

				scintilla.SetFoldProperty(lexerType);
				scintilla.SetFoldingStyle(nppReader, lexerType);


				return true;
			}
			catch
			{
				return false;
			}
		}

		/// <summary>
		/// Sets the global default styles.
		/// </summary>
		/// <param name="scintilla">The scintilla.</param>
		/// <param name="nppReader">The npp reader.</param>
		/// <param name="useGlobalOverride">If true, use global override.</param>
		/// <returns>A bool.</returns>
		public static bool SetGlobalDefaultStyles(this Scintilla scintilla, NppReader nppReader, bool useGlobalOverride)
		{
			try
			{
				scintilla.StyleResetDefault();

				if (useGlobalOverride == true)
				{
					scintilla.SetGlobalStyle(nppReader, "Global override");
				}

				scintilla.SetGlobalStyle(nppReader, "Default Style");
				scintilla.StyleClearAll();

				var skipStyle = new string[] {
					"Global override",
					"Default Style",
					//"Indent guideline style",
					//"Brace highlight style",
					//"Bad brace colour",
					"Current line background colour",
					"Selected text colour",
					"Caret colour",
					"Edge colour",
					//"Line number margin",
					//"Fold",
					"Fold margin",
					//"White space symbol",
					//"Smart HighLighting",
					//"Find Mark Style",
					//"Incremental highlight all",
					//"Tags match highlighting",
					//"Tags attribute",
					//"Active tab focused indicator",
					//"Active tab unfocused indicator",
					//"Active tab text",
					//"Inactive tabs",
				};
				scintilla.SetLexerStyles(nppReader.StylerModel.GlobalStyles
					.Where(x => x.StyleId > 0 && skipStyle.Contains(x.Name) == false));

				if (nppReader.ThemeStylerModel != null)
				{
					scintilla.SetLexerStyles(nppReader.ThemeStylerModel.GlobalStyles
						.Where(x => x.StyleId > 0 && skipStyle.Contains(x.Name) == false));
				}

				//var currentLineBackColor = scintilla.SetGlobalStyle(nppReader, "Current line background colour");
				//scintilla.CaretLineBackColor = currentLineBackColor.BackColor;

				//var selectedTextColor = scintilla.SetGlobalStyle(nppReader, "Selected text colour");
				//scintilla.SetSelectionForeColor(true, selectedTextColor.ForeColor);
				//scintilla.SetSelectionBackColor(true, selectedTextColor.BackColor);

				var caretColor = scintilla.SetGlobalStyle(nppReader, "Caret colour");
				scintilla.CaretForeColor = caretColor.ForeColor;
				scintilla.CaretLineBackColor = caretColor.BackColor;
				//return false;

				//var edgeColor = scintilla.SetGlobalStyle(nppReader, "Edge colour");
				//scintilla.EdgeColor = edgeColor.ForeColor;

				//var foldColor = scintilla.SetGlobalStyle(nppReader, "Fold margin");
				//scintilla.SetFoldMarginHighlightColor(true, foldColor.ForeColor);
				//scintilla.SetFoldMarginColor(true, foldColor.BackColor);

				//var whiteSpaceColor = scintilla.SetGlobalStyle(nppReader, "White space symbol");
				//scintilla.SetWhitespaceForeColor(true, whiteSpaceColor.ForeColor);
				//scintilla.SetWhitespaceBackColor(true, whiteSpaceColor.BackColor);

				return true;
			}
			catch
			{
				return false;
			}
		}

		private static void SetLexerStyles(this Scintilla scintilla, StylerModelWrapper stylerModel)
		{
			var lexerStyles = stylerModel?.LexerStyles?.FirstOrDefault();

			if (lexerStyles == null)
			{
				return;
			}

			scintilla.SetLexerStyles(lexerStyles.WordsStyles);
		}

		private static void SetLexerStyles(this Scintilla scintilla, IEnumerable<LexerStyle> lexerStyles)
		{
			foreach (var lexerStyle in lexerStyles)
			{
				try
				{
					scintilla.SetStyle(lexerStyle);
				}
				catch
				{
				}
			}
		}

		private static LexerStyle GetGlobalStyle(StylerModelWrapper stylerModel, string globalWidgetName)
		{
			return stylerModel?.GlobalStyles.FirstOrDefault(x => x.Name == globalWidgetName);
		}

		private static LexerStyle SetGlobalStyle(this Scintilla scintilla, NppReader nppReader, string globalWidgetName)
		{
			var globalStyle = scintilla.SetStyle(GetGlobalStyle(nppReader.StylerModel, globalWidgetName));

			if (nppReader.ThemeStylerModel != null && nppReader.ThemeStylerModel.GlobalStyles.Count > 0)
			{
				return scintilla.SetStyle(GetGlobalStyle(nppReader.ThemeStylerModel, globalWidgetName));
			}

			return globalStyle;
		}

		/// <summary>
		/// Sets the style.
		/// </summary>
		/// <param name="scintilla">The scintilla.</param>
		/// <param name="style">The lexer style.</param>
		public static LexerStyle SetStyle(this Scintilla scintilla, LexerStyle style)
		{
			if (style == null)
			{
				return style;
			}

			if (style.ForeColor != Color.Empty)
			{
				scintilla.Styles[style.StyleId].ForeColor = style.ForeColor;
			}

			if (style.BackColor != Color.Empty)
			{
				scintilla.Styles[style.StyleId].BackColor = style.BackColor;
			}

			if ((style.FontStyles & FontStyles.Bold) == FontStyles.Bold)
			{
				scintilla.Styles[style.StyleId].Bold = true;
			}

			if ((style.FontStyles & FontStyles.Italic) == FontStyles.Italic)
			{
				scintilla.Styles[style.StyleId].Italic = true;
			}

			if ((style.FontStyles & FontStyles.Underline) == FontStyles.Underline)
			{
				scintilla.Styles[style.StyleId].Underline = true;
			}

			if (style.FontName != string.Empty)
			{
				scintilla.Styles[style.StyleId].Font = style.FontName;
			}

			if (style.FontSize != string.Empty)
			{
				scintilla.Styles[style.StyleId].Size = Convert.ToInt32(style.FontSize);
			}

			return style;
		}

		/// <summary>
		/// Sets the fold style.
		/// </summary>
		/// <param name="scintilla">The scintilla.</param>
		/// <param name="lexerType">The lexer type.</param>
		public static void SetFoldProperty(this Scintilla scintilla, LexerTypes lexerType)
		{
			switch (lexerType)
			{
				case LexerTypes.html:
					scintilla.SetProperty("fold.hypertext.comment", "1");
					scintilla.SetProperty("fold.hypertext.heredoc", "1");
					scintilla.SetProperty("fold.html.preprocessor", "1");
					scintilla.SetProperty("fold.html", "1");

					break;
				case LexerTypes.sql:
					scintilla.SetProperty("fold.sql.at.else", "1");
					scintilla.SetProperty("fold.comment", "1");
					scintilla.SetProperty("sql.backslash.escapes", "1");
					scintilla.SetProperty("lexer.sql.numbersign.comment", "1");
					scintilla.SetProperty("lexer.sql.allow.dotted.word", "1");

					break;
				case LexerTypes.xml:
					scintilla.SetProperty("fold.html", "1");
					scintilla.SetProperty("html.tags.case.sensitive", "1");

					break;
				default:
					scintilla.SetProperty("fold", "1");
					scintilla.SetProperty("fold.compact", "1");
					scintilla.SetProperty("fold.preprocessor", "1");

					break;
			}
		}

		/// <summary>
		/// Sets the folding style.
		/// </summary>
		/// <param name="scintilla">The scintilla.</param>
		/// <param name="nppReader">The npp reader.</param>
		/// <param name="lexerType">The lexer type.</param>
		/// <returns>A bool.</returns>
		public static bool SetFoldingStyle(this Scintilla scintilla, NppReader nppReader, LexerTypes lexerType)
		{
			try
			{
				scintilla.Margins[2].Type = MarginType.Symbol;
				scintilla.Margins[2].Mask = Marker.MaskFolders;
				scintilla.Margins[2].Sensitive = true;
				scintilla.Margins[2].Width = 20;

				var style = GetGlobalStyle(nppReader.ThemeStylerModel, "Fold");

				if (style == null)
				{
					style = GetGlobalStyle(nppReader.StylerModel, "Fold");
				}

				for (int i = 25; i <= 31; i++)
				{
					scintilla.Markers[i].SetForeColor(style.ForeColor);
					scintilla.Markers[i].SetBackColor(style.ForeColor);
				}

				// Configure folding markers with respective symbols
				scintilla.Markers[Marker.Folder].Symbol = MarkerSymbol.BoxPlus;
				scintilla.Markers[Marker.FolderOpen].Symbol = MarkerSymbol.BoxMinus;
				scintilla.Markers[Marker.FolderEnd].Symbol = MarkerSymbol.BoxPlusConnected;
				scintilla.Markers[Marker.FolderMidTail].Symbol = MarkerSymbol.TCorner;
				scintilla.Markers[Marker.FolderOpenMid].Symbol = MarkerSymbol.BoxMinusConnected;
				scintilla.Markers[Marker.FolderSub].Symbol = MarkerSymbol.VLine;
				scintilla.Markers[Marker.FolderTail].Symbol = MarkerSymbol.LCorner;

				// Enable automatic folding
				scintilla.AutomaticFold = (AutomaticFold.Show | AutomaticFold.Click | AutomaticFold.Change);

				return true;
			}
			catch
			{
				return false;
			}
		}
	}
}
