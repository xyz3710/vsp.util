﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ScintillaNET.LexerHelpers
{
	/// <summary>
	/// Scintilla.Net Lexer names gernerated by Scintilla.NET.TestApp in 5.1.5.4
	/// </summary>
	public enum LexerNames
	{
		/// <summary>
		/// a68k
		/// </summary>
		[Description("a68k")]
		a68k,
		/// <summary>
		/// abaqus
		/// </summary>
		[Description("abaqus")]
		abaqus,
		/// <summary>
		/// ada
		/// </summary>
		[Description("ada")]
		ada,
		/// <summary>
		/// apdl
		/// </summary>
		[Description("apdl")]
		apdl,
		/// <summary>
		/// as
		/// </summary>
		[Description("as")]
		as_,
		/// <summary>
		/// asciidoc
		/// </summary>
		[Description("asciidoc")]
		asciidoc,
		/// <summary>
		/// asm
		/// </summary>
		[Description("asm")]
		asm,
		/// <summary>
		/// asn1
		/// </summary>
		[Description("asn1")]
		asn1,
		/// <summary>
		/// asy
		/// </summary>
		[Description("asy")]
		asy,
		/// <summary>
		/// au3
		/// </summary>
		[Description("au3")]
		au3,
		/// <summary>
		/// ave
		/// </summary>
		[Description("ave")]
		ave,
		/// <summary>
		/// avs
		/// </summary>
		[Description("avs")]
		avs,
		/// <summary>
		/// baan
		/// </summary>
		[Description("baan")]
		baan,
		/// <summary>
		/// bash
		/// </summary>
		[Description("bash")]
		bash,
		/// <summary>
		/// batch
		/// </summary>
		[Description("batch")]
		batch,
		/// <summary>
		/// bib
		/// </summary>
		[Description("bib")]
		bib,
		/// <summary>
		/// blitzbasic
		/// </summary>
		[Description("blitzbasic")]
		blitzbasic,
		/// <summary>
		/// bullant
		/// </summary>
		[Description("bullant")]
		bullant,
		/// <summary>
		/// caml
		/// </summary>
		[Description("caml")]
		caml,
		/// <summary>
		/// cil
		/// </summary>
		[Description("cil")]
		cil,
		/// <summary>
		/// clarion
		/// </summary>
		[Description("clarion")]
		clarion,
		/// <summary>
		/// clarionnocase
		/// </summary>
		[Description("clarionnocase")]
		clarionnocase,
		/// <summary>
		/// cmake
		/// </summary>
		[Description("cmake")]
		cmake,
		/// <summary>
		/// COBOL
		/// </summary>
		[Description("COBOL")]
		COBOL,
		/// <summary>
		/// coffeescript
		/// </summary>
		[Description("coffeescript")]
		coffeescript,
		/// <summary>
		/// conf
		/// </summary>
		[Description("conf")]
		conf,
		/// <summary>
		/// cpp
		/// </summary>
		[Description("cpp")]
		cpp,
		/// <summary>
		/// cpp
		/// </summary>
		[Description("cpp")]
		cs,
		/// <summary>
		/// cppnocase
		/// </summary>
		[Description("cppnocase")]
		cppnocase,
		/// <summary>
		/// csound
		/// </summary>
		[Description("csound")]
		csound,
		/// <summary>
		/// css
		/// </summary>
		[Description("css")]
		css,
		/// <summary>
		/// d
		/// </summary>
		[Description("d")]
		d,
		/// <summary>
		/// dataflex
		/// </summary>
		[Description("dataflex")]
		dataflex,
		/// <summary>
		/// diff
		/// </summary>
		[Description("diff")]
		diff,
		/// <summary>
		/// DMAP
		/// </summary>
		[Description("DMAP")]
		DMAP,
		/// <summary>
		/// DMIS
		/// </summary>
		[Description("DMIS")]
		DMIS,
		/// <summary>
		/// ecl
		/// </summary>
		[Description("ecl")]
		ecl,
		/// <summary>
		/// edifact
		/// </summary>
		[Description("edifact")]
		edifact,
		/// <summary>
		/// eiffel
		/// </summary>
		[Description("eiffel")]
		eiffel,
		/// <summary>
		/// eiffelkw
		/// </summary>
		[Description("eiffelkw")]
		eiffelkw,
		/// <summary>
		/// erlang
		/// </summary>
		[Description("erlang")]
		erlang,
		/// <summary>
		/// errorlist
		/// </summary>
		[Description("errorlist")]
		errorlist,
		/// <summary>
		/// escript
		/// </summary>
		[Description("escript")]
		escript,
		/// <summary>
		/// f77
		/// </summary>
		[Description("f77")]
		f77,
		/// <summary>
		/// flagship
		/// </summary>
		[Description("flagship")]
		flagship,
		/// <summary>
		/// forth
		/// </summary>
		[Description("forth")]
		forth,
		/// <summary>
		/// fortran
		/// </summary>
		[Description("fortran")]
		fortran,
		/// <summary>
		/// freebasic
		/// </summary>
		[Description("freebasic")]
		freebasic,
		/// <summary>
		/// fsharp
		/// </summary>
		[Description("fsharp")]
		fsharp,
		/// <summary>
		/// gap
		/// </summary>
		[Description("gap")]
		gap,
		/// <summary>
		/// gdscript
		/// </summary>
		[Description("gdscript")]
		gdscript,
		/// <summary>
		/// gui4cli
		/// </summary>
		[Description("gui4cli")]
		gui4cli,
		/// <summary>
		/// haskell
		/// </summary>
		[Description("haskell")]
		haskell,
		/// <summary>
		/// hollywood
		/// </summary>
		[Description("hollywood")]
		hollywood,
		/// <summary>
		/// hypertext
		/// </summary>
		[Description("hypertext")]
		hypertext,
		/// <summary>
		/// ihex
		/// </summary>
		[Description("ihex")]
		ihex,
		/// <summary>
		/// indent
		/// </summary>
		[Description("indent")]
		indent,
		/// <summary>
		/// inno
		/// </summary>
		[Description("inno")]
		inno,
		/// <summary>
		/// json
		/// </summary>
		[Description("json")]
		json,
		/// <summary>
		/// julia
		/// </summary>
		[Description("julia")]
		julia,
		/// <summary>
		/// kix
		/// </summary>
		[Description("kix")]
		kix,
		/// <summary>
		/// kvirc
		/// </summary>
		[Description("kvirc")]
		kvirc,
		/// <summary>
		/// latex
		/// </summary>
		[Description("latex")]
		latex,
		/// <summary>
		/// lisp
		/// </summary>
		[Description("lisp")]
		lisp,
		/// <summary>
		/// literatehaskell
		/// </summary>
		[Description("literatehaskell")]
		literatehaskell,
		/// <summary>
		/// lot
		/// </summary>
		[Description("lot")]
		lot,
		/// <summary>
		/// lout
		/// </summary>
		[Description("lout")]
		lout,
		/// <summary>
		/// lua
		/// </summary>
		[Description("lua")]
		lua,
		/// <summary>
		/// magiksf
		/// </summary>
		[Description("magiksf")]
		magiksf,
		/// <summary>
		/// makefile
		/// </summary>
		[Description("makefile")]
		makefile,
		/// <summary>
		/// markdown
		/// </summary>
		[Description("markdown")]
		markdown,
		/// <summary>
		/// matlab
		/// </summary>
		[Description("matlab")]
		matlab,
		/// <summary>
		/// maxima
		/// </summary>
		[Description("maxima")]
		maxima,
		/// <summary>
		/// metapost
		/// </summary>
		[Description("metapost")]
		metapost,
		/// <summary>
		/// mmixal
		/// </summary>
		[Description("mmixal")]
		mmixal,
		/// <summary>
		/// modula
		/// </summary>
		[Description("modula")]
		modula,
		/// <summary>
		/// mssql
		/// </summary>
		[Description("mssql")]
		mssql,
		/// <summary>
		/// mysql
		/// </summary>
		[Description("mysql")]
		mysql,
		/// <summary>
		/// nim
		/// </summary>
		[Description("nim")]
		nim,
		/// <summary>
		/// nimrod
		/// </summary>
		[Description("nimrod")]
		nimrod,
		/// <summary>
		/// nncrontab
		/// </summary>
		[Description("nncrontab")]
		nncrontab,
		/// <summary>
		/// nsis
		/// </summary>
		[Description("nsis")]
		nsis,
		/// <summary>
		/// null
		/// </summary>
		[Description("null")]
		null_,
		/// <summary>
		/// octave
		/// </summary>
		[Description("octave")]
		octave,
		/// <summary>
		/// opal
		/// </summary>
		[Description("opal")]
		opal,
		/// <summary>
		/// oscript
		/// </summary>
		[Description("oscript")]
		oscript,
		/// <summary>
		/// pascal
		/// </summary>
		[Description("pascal")]
		pascal,
		/// <summary>
		/// powerbasic
		/// </summary>
		[Description("powerbasic")]
		powerbasic,
		/// <summary>
		/// perl
		/// </summary>
		[Description("perl")]
		perl,
		/// <summary>
		/// phpscript
		/// </summary>
		[Description("phpscript")]
		phpscript,
		/// <summary>
		/// PL/M
		/// </summary>
		[Description("PL/M")]
		PL__M,
		/// <summary>
		/// po
		/// </summary>
		[Description("po")]
		po,
		/// <summary>
		/// pov
		/// </summary>
		[Description("pov")]
		pov,
		/// <summary>
		/// powerpro
		/// </summary>
		[Description("powerpro")]
		powerpro,
		/// <summary>
		/// powershell
		/// </summary>
		[Description("powershell")]
		powershell,
		/// <summary>
		/// abl
		/// </summary>
		[Description("abl")]
		abl,
		/// <summary>
		/// props
		/// </summary>
		[Description("props")]
		props,
		/// <summary>
		/// ps
		/// </summary>
		[Description("ps")]
		ps,
		/// <summary>
		/// purebasic
		/// </summary>
		[Description("purebasic")]
		purebasic,
		/// <summary>
		/// python
		/// </summary>
		[Description("python")]
		python,
		/// <summary>
		/// r
		/// </summary>
		[Description("r")]
		r,
		/// <summary>
		/// raku
		/// </summary>
		[Description("raku")]
		raku,
		/// <summary>
		/// rebol
		/// </summary>
		[Description("rebol")]
		rebol,
		/// <summary>
		/// registry
		/// </summary>
		[Description("registry")]
		registry,
		/// <summary>
		/// ruby
		/// </summary>
		[Description("ruby")]
		ruby,
		/// <summary>
		/// rust
		/// </summary>
		[Description("rust")]
		rust,
		/// <summary>
		/// sas
		/// </summary>
		[Description("sas")]
		sas,
		/// <summary>
		/// scriptol
		/// </summary>
		[Description("scriptol")]
		scriptol,
		/// <summary>
		/// smalltalk
		/// </summary>
		[Description("smalltalk")]
		smalltalk,
		/// <summary>
		/// SML
		/// </summary>
		[Description("SML")]
		SML,
		/// <summary>
		/// sorcins
		/// </summary>
		[Description("sorcins")]
		sorcins,
		/// <summary>
		/// specman
		/// </summary>
		[Description("specman")]
		specman,
		/// <summary>
		/// spice
		/// </summary>
		[Description("spice")]
		spice,
		/// <summary>
		/// sql
		/// </summary>
		[Description("sql")]
		sql,
		/// <summary>
		/// srec
		/// </summary>
		[Description("srec")]
		srec,
		/// <summary>
		/// stata
		/// </summary>
		[Description("stata")]
		stata,
		/// <summary>
		/// fcST
		/// </summary>
		[Description("fcST")]
		fcST,
		/// <summary>
		/// TACL
		/// </summary>
		[Description("TACL")]
		TACL,
		/// <summary>
		/// tads3
		/// </summary>
		[Description("tads3")]
		tads3,
		/// <summary>
		/// TAL
		/// </summary>
		[Description("TAL")]
		TAL,
		/// <summary>
		/// tcl
		/// </summary>
		[Description("tcl")]
		tcl,
		/// <summary>
		/// tcmd
		/// </summary>
		[Description("tcmd")]
		tcmd,
		/// <summary>
		/// tehex
		/// </summary>
		[Description("tehex")]
		tehex,
		/// <summary>
		/// tex
		/// </summary>
		[Description("tex")]
		tex,
		/// <summary>
		/// txt2tags
		/// </summary>
		[Description("txt2tags")]
		txt2tags,
		/// <summary>
		/// vb
		/// </summary>
		[Description("vb")]
		vb,
		/// <summary>
		/// vbscript
		/// </summary>
		[Description("vbscript")]
		vbscript,
		/// <summary>
		/// verilog
		/// </summary>
		[Description("verilog")]
		verilog,
		/// <summary>
		/// vhdl
		/// </summary>
		[Description("vhdl")]
		vhdl,
		/// <summary>
		/// visualprolog
		/// </summary>
		[Description("visualprolog")]
		visualprolog,
		/// <summary>
		/// x12
		/// </summary>
		[Description("x12")]
		x12,
		/// <summary>
		/// xml
		/// </summary>
		[Description("xml")]
		xml,
		/// <summary>
		/// yaml
		/// </summary>
		[Description("yaml")]
		yaml,

	}
}
