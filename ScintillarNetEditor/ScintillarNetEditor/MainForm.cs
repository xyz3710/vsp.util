﻿using ScintillaNET;
using ScintillaNET.LexerHelpers;

using ScintillarNetEditor.Properties;

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml.Serialization;

namespace ScintillarNetEditor
{
	public partial class MainForm : Form
	{
		private CSharpLexer cSharpLexer = new CSharpLexer("class const this int namespace partial public static string using void protected override");

		/// <summary>
		/// default text color of the text area
		/// </summary>
		//private const int FORE_COLOR = 0xB7B7B7;
		private readonly Color BasicForeColor = Color.FromArgb(255, 0xB7, 0xB7, 0xB7);

		/// <summary>
		/// the background color of the text area
		/// </summary>
		//private const int BACK_COLOR = 0x2A211C;
		//private readonly Color BACK_COLOR = Color.FromArgb(255, 0x2A, 0x21, 0x1C);
		private readonly Color BasicBackColor = Color.FromArgb(255, 0x21, 0x21, 0x21);
		private readonly Color BasicLineNumberForeColor = Color.FromArgb(255, 102, 153, 204);
		private readonly Color BasicMarginBackColor = Color.Black;

		/*
		// scintilla.Margins : https://scintilla.org/ScintillaDoc.html#Margins
		// 0: Line Number
		// 1: None Folding Symbol (16px)
		// 2: Folding Symbol
		// 3:
		// 4: Application define text or rtext
		*/
		private const int LINE_NUMBER_MARGIN = 0;
		/// <summary>
		/// change this to whatever margin you want the line numbers to show in
		/// </summary>
		private const int NUMBER_MARGIN = 1;
		/// <summary>
		/// change this to whatever margin you want the bookmarks/breakpoints to show in
		/// </summary>
		private const int BOOKMARK_MARGIN = 2;
		private const int BOOKMARK_MARKER = 2;
		private const int FOLDING_MARGIN = 3;

		/// <summary>
		/// set this true to show circular buttons for code folding (the [+] and [-] buttons on the margin)
		/// </summary>
		private const bool CODEFOLDING_CIRCULAR = true;

		public MainForm()
		{
			InitializeComponent();
		}

		protected override void OnLoad(EventArgs e)
		{
			base.OnLoad(e);
			TopMost = false;

			LoadMainForm();
			ReadNpp();

			scintilla.ZoomChanged += (sender, ea) => SetLineNumber(scintilla);
			scintilla.MarginClick += OnMarginClick;
			scintilla.Zoom = 2;

			SetDefaultCaret(scintilla);
			return;
			scintilla.StyleNeeded += OnStyleNeeded;

			//TestCSharpHighlight();
		}

		protected override void OnShown(EventArgs e)
		{
			base.OnShown(e);
			scintilla.Focus();
		}

		protected override bool ProcessCmdKey(ref Message msg, Keys keyData)
		{
			const int WM_KEYDOWN = 0x100;
			const int WM_SYSKEYDOWN = 0x104;

			if ((msg.Msg == WM_KEYDOWN) || (msg.Msg == WM_SYSKEYDOWN))
			{
				switch (keyData)
				{
					case Keys.Q | Keys.Control:
						Close();

						return true;
					case Keys.Oemtilde | Keys.Shift:
						TopMost = !TopMost;

						return true;
					case Keys.D1 | Keys.Control:
						LoadMainForm();

						return true;
					case Keys.D2 | Keys.Control:
						LoadMainFormDesigner();

						return true;
					case Keys.D3 | Keys.Control:
						LoadLog();

						return true;
					case Keys.D1 | Keys.Shift:
						TestCSharpHighlight();

						return true;
					case Keys.D2 | Keys.Shift:
						TestCustomHighlight();

						return true;
					case Keys.D3 | Keys.Shift:
						ReadNpp();

						return true;
					case Keys.D4 | Keys.Shift:
						GotoDisplayedLine(60);

						return true;
					case Keys.K | Keys.Control:
						ToggledBookmark(scintilla);

						return true;
					case Keys.K | Keys.Alt:
						SetBookmarks(scintilla, 5, 10, 20, 30, 90);

						return true;
					case Keys.K | Keys.Alt | Keys.Shift:
						DeleteBookmarks(scintilla, 10, 20);

						return true;
					case Keys.F2:
						MoveNextBookmark(scintilla);

						return true;
					case Keys.F2 | Keys.Shift:
						MovePreviousBookmark(scintilla);

						return true;
				}
			}

			return base.ProcessCmdKey(ref msg, keyData);
		}

		private void LoadMainForm()
		{
			scintilla.Text = File.ReadAllText(@"..\..\MainForm.cs");
		}

		private void LoadMainFormDesigner()
		{
			scintilla.Text = File.ReadAllText(@"..\..\MainForm.Designer.cs");
		}

		private void LoadLog()
		{
			scintilla.Text = File.ReadAllText(@"..\..\..\log.txt");
		}

		private void cbLexar_SelectedIndexChanged(object sender, EventArgs e)
		{

		}

		private void SetDefaultCaret(Scintilla scintilla)
		{
			scintilla.CaretForeColor = Color.DarkGray;
			scintilla.CaretLineBackColor = Color.Orange;
			scintilla.CaretLineFrame = 2; // 지정하지 않으면 Block이 되고 지정하면 테두리만 있다.
			scintilla.CaretLineVisible = true;
			scintilla.CaretLineVisibleAlways = true;
			scintilla.CaretStyle = CaretStyle.Line;
		}

		private void TestCustomHighlight()
		{
			//scintilla.Lexer = Lexer.Null;

			scintilla.Styles[1].ForeColor = Color.Red;
			scintilla.Styles[2].ForeColor = Color.Blue;

			scintilla.StartStyling(0);
			scintilla.SetStyling(0, 10);
			scintilla.SetStyling(0, 10);
		}

		private void TestCSharpHighlight()
		{
			scintilla.StyleResetDefault();
			scintilla.Styles[Style.Default].Font = "D2Coding";
			scintilla.Styles[Style.Default].Size = 11;
			scintilla.Zoom = 1;
			scintilla.Styles[Style.Default].ForeColor = Color.White;
			scintilla.Styles[Style.Default].BackColor = BasicBackColor;
			//scintilla.Styles[Style.Default].ForeColor = Color.FromArgb(255, 0xFF, 0xFF, 0xFF);
			//scintilla.Styles[Style.Default].BackColor = Color.FromArgb(255, 0x21, 0x21, 0x21);
			scintilla.StyleClearAll();

			scintilla.WrapMode = WrapMode.None;

			// Configure the CPP (C#) lexer styles
			//scintilla.Styles[Style.Cpp.Identifier].ForeColor = Color.FromArgb(255, 0xD0, 0xDA, 0xE2);
			//scintilla.Styles[Style.Cpp.Comment].ForeColor = Color.FromArgb(255, 0xBD, 0x75, 0x8B);
			//scintilla.Styles[Style.Cpp.CommentLine].ForeColor = Color.FromArgb(255, 0x40, 0xBF, 0x57);
			//scintilla.Styles[Style.Cpp.CommentDoc].ForeColor = Color.FromArgb(255, 0x2F, 0xAE, 0x35);
			//scintilla.Styles[Style.Cpp.Number].ForeColor = Color.FromArgb(255, 0xFF, 0xFF, 0x00);
			//scintilla.Styles[Style.Cpp.String].ForeColor = Color.FromArgb(255, 0xFF, 0xFF, 0x00);
			//scintilla.Styles[Style.Cpp.Character].ForeColor = Color.FromArgb(255, 0xE9, 0x54, 0x54);
			//scintilla.Styles[Style.Cpp.Preprocessor].ForeColor = Color.FromArgb(255, 0x8A, 0xAF, 0xEE);
			//scintilla.Styles[Style.Cpp.Operator].ForeColor = Color.FromArgb(255, 0xE0, 0xE0, 0xE0);
			//scintilla.Styles[Style.Cpp.Regex].ForeColor = Color.FromArgb(255, 0xFF, 0x00, 0xFF);
			//scintilla.Styles[Style.Cpp.CommentLineDoc].ForeColor = Color.FromArgb(255, 0x77, 0xA7, 0xDB);
			//scintilla.Styles[Style.Cpp.Word].ForeColor = Color.FromArgb(255, 0x48, 0xA8, 0xEE);
			//scintilla.Styles[Style.Cpp.Word2].ForeColor = Color.FromArgb(255, 0xF9, 0x89, 0x06);
			//scintilla.Styles[Style.Cpp.CommentDocKeyword].ForeColor = Color.FromArgb(255, 0xB3, 0xD9, 0x91);
			//scintilla.Styles[Style.Cpp.CommentDocKeywordError].ForeColor = Color.FromArgb(255, 0xFF, 0x00, 0x00);
			//scintilla.Styles[Style.Cpp.GlobalClass].ForeColor = Color.FromArgb(255, 0x48, 0xA8, 0xEE);

			scintilla.Styles[CSharpLexer.StyleDefault].ForeColor = Color.White;
			scintilla.Styles[CSharpLexer.StyleDefault].BackColor = BasicBackColor;
			scintilla.Styles[CSharpLexer.StyleKeyword].ForeColor = Color.FromArgb(255, 50, 120, 203);
			scintilla.Styles[CSharpLexer.StyleIdentifier].ForeColor = Color.FromArgb(255, 96, 139, 78);
			scintilla.Styles[CSharpLexer.StyleNumber].ForeColor = Color.FromArgb(255, 129, 189, 168);
			scintilla.Styles[CSharpLexer.StyleString].ForeColor = Color.FromArgb(181, 150, 124);

			//scintilla.Styles[Style.LineNumber].ForeColor = LINE_NUMBER_FORE_COLOR;
			//scintilla.Styles[Style.LineNumber].BackColor = BACK_COLOR;

			//scintilla.Lexer = Lexer.Container; // Container로 지정해야 StyleNeeded 가 fire 된다.(기본: Container)
			//scintilla.IndentationGuides = IndentView.LookBoth; // { } 가이드 라인

			scintilla.SetKeywords(0, "class extends implements import interface new case do while else if for in switch throw get set function var try catch finally while with default break continue delete return each const namespace package include use is as instanceof typeof author copy default deprecated eventType example exampleText exception haxe inheritDoc internal link mtasc mxmlc param private return see serial serialData serialField since throws usage version langversion playerversion productversion dynamic private public partial static intrinsic internal native override protected AS3 final super this arguments null Infinity NaN undefined true false abstract as base bool break by byte case catch char checked class const continue decimal default delegate do double descending explicit event extern else enum false finally fixed float for foreach from goto group if implicit in int interface internal into is lock long new null namespace object operator out override orderby params private protected public readonly ref return switch struct sbyte sealed short sizeof stackalloc static string select this throw true try typeof uint ulong unchecked unsafe ushort using var virtual volatile void while where yield");
			scintilla.SetKeywords(1, "void Null ArgumentError arguments Array Boolean Class Date DefinitionError Error EvalError Function int Math Namespace Number Object RangeError ReferenceError RegExp SecurityError String SyntaxError TypeError uint XML XMLList Boolean Byte Char DateTime Decimal Double Int16 Int32 Int64 IntPtr SByte Single UInt16 UInt32 UInt64 UIntPtr Void Path File System Windows Forms ScintillaNET");

			SetDefaultSelectionColor(scintilla);
			// scintilla.Margins : https://scintilla.org/ScintillaDoc.html#Margins
			// 0: Line Number
			// 1: None Folding Symbol (16px)
			// 2: Folding Symbol
			// 3:
			// 4: Application define text or rtext
			//SetLineNumber(scintilla);
			SetDefaultLineNumberMargin(scintilla);
			SetDefaultBookmarkMargin(scintilla);
			SetDefaultCodeFolding(scintilla);
		}

		private void OnStyleNeeded(object sender, StyleNeededEventArgs e)
		{
			var startPos = scintilla.GetEndStyled();
			var endPos = e.Position;

			cSharpLexer.Style(scintilla, startPos, endPos);
		}

		private void SetDefaultSelectionColor(Scintilla scintilla, Color? foreColor = null, Color? backColor = null)
		{
			//Color color = Color.FromArgb(255, 0x11, 0x4D, 0x9C);
			scintilla.SetSelectionForeColor(true, foreColor ?? Color.White);
			scintilla.SetSelectionBackColor(true, backColor ?? Color.FromArgb(255, 102, 153, 204));
		}

		private void SetLineNumber(Scintilla scintilla)
		{
			// Did the number of characters in the line number display change?
			// i.e. nnn VS nn, or nnnn VS nn, etc...
			var maxLineNumberCharLength = scintilla.Lines.Count.ToString().Length;
			//var padding = (int)Math.Max(1.1d * (double)scintilla.Zoom, 1.2d);
			var padding = (int)Math.Max(1.1d * (double)scintilla.Zoom, 1d);

			//if (maxLineNumberCharLength == Convert.ToInt32(scintilla.Tag))
			//{
			//    return;
			//}

			// Calculate the width required to display the last line number
			// and include some padding for good measure.
			scintilla.Margins[LINE_NUMBER_MARGIN].Width = scintilla.TextWidth(Style.LineNumber, new string('9', maxLineNumberCharLength + 1)) + padding;
		}

		private void SetDefaultLineNumberMargin(Scintilla scintilla)
		{
			scintilla.Styles[Style.LineNumber].ForeColor = BasicLineNumberForeColor;
			scintilla.Styles[Style.LineNumber].BackColor = BasicMarginBackColor;
			scintilla.Styles[Style.IndentGuide].ForeColor = BasicForeColor;
			scintilla.Styles[Style.IndentGuide].BackColor = BasicMarginBackColor;
			/*
			var nums = scintilla.Margins[NUMBER_MARGIN];
			nums.Width = 30;
			nums.Type = MarginType.Number;
			//nums.Type = MarginType.Color;
			nums.Sensitive = true;
			nums.Mask = 0;
			nums.BackColor = Color.YellowGreen;
			*/
		}

		private void SetDefaultBookmarkMargin(Scintilla scintilla)
		{
			//scintilla.SetFoldMarginColor(true, BACK_COLOR);

			var margin = scintilla.Margins[BOOKMARK_MARGIN];
			margin.Type = MarginType.Symbol;
			margin.Mask = (1 << BOOKMARK_MARKER);
			margin.Sensitive = true;
			margin.Width = 16;

			var marker = scintilla.Markers[BOOKMARK_MARKER];
			//marker.Symbol = MarkerSymbol.Bookmark; // RoundRect 도 괜찮음
			//marker.SetAlpha(100);
			//marker.SetForeColor(Color.FromArgb(119, 232, 74));
			//marker.SetBackColor(Color.FromArgb(54, 75, 227));
			marker.DefineRgbaImage(Resources.Circle_Marker); // 이미지로 할때는 위 속성 필요 없음
		}

		#region Bookmarks
		private void OnMarginClick(object sender, MarginClickEventArgs e)
		{
			if (e.Margin == BOOKMARK_MARGIN)
			{
				ToggledBookmark(sender as Scintilla, e.Position);
			}
		}

		/// <summary>
		/// Toggleds the bookmark.
		/// </summary>
		/// <param name="scintilla">The scintilla.</param>
		/// <param name="position">The position.</param>
		public void ToggledBookmark(Scintilla scintilla, int? position = null)
		{
			// Do we have a marker for this line?
			const uint mask = (1 << BOOKMARK_MARKER);
			var line = scintilla.Lines[scintilla.LineFromPosition(position ?? scintilla.CurrentPosition)];

			if ((line.MarkerGet() & mask) > 0)
			{
				line.MarkerDelete(BOOKMARK_MARKER); // Remove existing bookmark
			}
			else
			{
				line.MarkerAdd(BOOKMARK_MARKER); // Add bookmark
			}
		}

		/// <summary>
		/// Gets the bookmarks.
		/// </summary>
		/// <param name="scintilla">The scintilla.</param>
		/// <returns></returns>
		public IEnumerable<Line> GetBookmarks(Scintilla scintilla)
		{
			const uint mask = (1 << BOOKMARK_MARKER);

			return scintilla.Lines.Cast<Line>().Where(x => (x.MarkerGet() & mask) > 0);
		}

		/// <summary>
		/// Sets the bookmarks.
		/// </summary>
		/// <param name="scintilla">The scintilla.</param>
		/// <param name="index">The index.</param>
		public void SetBookmarks(Scintilla scintilla, params int[] index)
		{
			foreach (var line in GetLineWithIndex(scintilla, index))
			{
				line.MarkerAdd(BOOKMARK_MARKER);
			}
		}

		/// <summary>
		/// Deletes the bookmarks.
		/// </summary>
		/// <param name="scintilla">The scintilla.</param>
		/// <param name="index">The index.</param>
		public void DeleteBookmarks(Scintilla scintilla, params int[] index)
		{
			foreach (var line in GetLineWithIndex(scintilla, index))
			{
				line.MarkerDelete(BOOKMARK_MARKER);
			}
		}

		private IEnumerable<Line> GetLineWithIndex(Scintilla scintilla, int[] index)
		{
			return scintilla.Lines.Cast<Line>().Where(x => index.Contains(x.Index) == true);
		}

		/// <summary>
		/// Moves the next bookmark.
		/// </summary>
		/// <param name="scintilla">The scintilla.</param>
		public void MoveNextBookmark(Scintilla scintilla)
		{
			var currentPosition = scintilla.CurrentPosition;
			var bookmarkers = GetBookmarks(scintilla);
			var line = bookmarkers.FirstOrDefault(x => x.Position > currentPosition);

			if (line == null)
			{
				line = bookmarkers.FirstOrDefault();
			}

			if (line != null)
			{
				scintilla.GotoPosition(line?.Position ?? -1);
			}
		}

		/// <summary>
		/// Moves the previous bookmark.
		/// </summary>
		/// <param name="scintilla">The scintilla.</param>
		public void MovePreviousBookmark(Scintilla scintilla)
		{
			var currentPosition = scintilla.CurrentPosition;
			var bookmarkers = GetBookmarks(scintilla);
			var line = bookmarkers.LastOrDefault(x => x.Position < currentPosition);

			if (line == null)
			{
				line = bookmarkers.LastOrDefault();
			}

			if (line != null)
			{
				scintilla.GotoPosition(line?.Position ?? -1);
			}
		}
		#endregion

		private void SetDefaultCodeFolding(Scintilla scintilla)
		{
			scintilla.SetFoldMarginColor(true, BasicBackColor);
			scintilla.SetFoldMarginHighlightColor(true, BasicMarginBackColor);

			// Enable code folding
			scintilla.SetProperty("fold", "1");
			scintilla.SetProperty("fold.compact", "1");

			// Configure a margin to display folding symbols
			var margin = scintilla.Margins[FOLDING_MARGIN];
			margin.Type = MarginType.Symbol;
			margin.Mask = Marker.MaskFolders;
			margin.Sensitive = true;
			margin.Width = 16;

			// Set colors for all folding markers
			/*
			for (int i = 25; i <= 31; i++)
			{
				scintilla.Markers[i].SetForeColor(Color.Orange); // styles for [+] and [-]
				scintilla.Markers[i].SetBackColor(Color.Yellow); // styles for [+] and [-]
																 //scintilla.Markers[i].SetForeColor(BACK_COLOR); // styles for [+] and [-]
																 //scintilla.Markers[i].SetBackColor(FORE_COLOR); // styles for [+] and [-]
			}
			*/

			// Configure folding markers with respective symbols
			scintilla.Markers[Marker.Folder].Symbol = CODEFOLDING_CIRCULAR ? MarkerSymbol.CirclePlus : MarkerSymbol.BoxPlus;
			scintilla.Markers[Marker.FolderOpen].Symbol = CODEFOLDING_CIRCULAR ? MarkerSymbol.CircleMinus : MarkerSymbol.BoxMinus;
			scintilla.Markers[Marker.FolderEnd].Symbol = CODEFOLDING_CIRCULAR ? MarkerSymbol.CirclePlusConnected : MarkerSymbol.BoxPlusConnected;
			scintilla.Markers[Marker.FolderMidTail].Symbol = MarkerSymbol.TCorner;
			scintilla.Markers[Marker.FolderOpenMid].Symbol = CODEFOLDING_CIRCULAR ? MarkerSymbol.CircleMinusConnected : MarkerSymbol.BoxMinusConnected;
			scintilla.Markers[Marker.FolderSub].Symbol = MarkerSymbol.VLine;
			scintilla.Markers[Marker.FolderTail].Symbol = MarkerSymbol.LCorner;

			// Enable automatic folding
			scintilla.AutomaticFold = (AutomaticFold.Show | AutomaticFold.Click | AutomaticFold.Change);

		}

		// Shift+D3
		private void ReadNpp()
		{
			var nppPath = @"C:\Program Files\Util\Notepad++";
			var lexerType = LexerTypes.cs;
			var themeName = "_Mine_Deep Black";
			var nppReader = new NppReader(nppPath, themeName);

			//nppReader.GenerateEnumClass("ScintillaNET.LexerHelpers");

			scintilla.CreateLexerFromNpp(nppReader, lexerType);
		}

		private void GotoDisplayedLine(int displayedLine)
		{
			var line = scintilla.Lines[displayedLine - 1];
			var screenCenter = scintilla.LinesOnScreen / 2;

			scintilla.FirstVisibleLine = displayedLine - screenCenter;
			scintilla.GotoPosition(line.Position);
			scintilla.ScrollCaret();
		}
	}
}
