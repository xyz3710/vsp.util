﻿namespace ScintillarNetEditor
{
	partial class MainForm
	{
		/// <summary>
		/// 필수 디자이너 변수입니다.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// 사용 중인 모든 리소스를 정리합니다.
		/// </summary>
		/// <param name="disposing">관리되는 리소스를 삭제해야 하면 true이고, 그렇지 않으면 false입니다.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form 디자이너에서 생성한 코드

		/// <summary>
		/// 디자이너 지원에 필요한 메서드입니다. 
		/// 이 메서드의 내용을 코드 편집기로 수정하지 마세요.
		/// </summary>
		private void InitializeComponent()
		{
			this.scintilla = new ScintillaNET.Scintilla();
			this.tlpMain = new System.Windows.Forms.TableLayoutPanel();
			this.tlpControls = new System.Windows.Forms.TableLayoutPanel();
			this.cbLexar = new System.Windows.Forms.ComboBox();
			this.tlpMain.SuspendLayout();
			this.tlpControls.SuspendLayout();
			this.SuspendLayout();
			// 
			// scintilla
			// 
			this.scintilla.AutoCMaxHeight = 9;
			this.scintilla.BiDirectionality = ScintillaNET.BiDirectionalDisplayType.Disabled;
			this.scintilla.CaretLineBackColor = System.Drawing.Color.Black;
			this.scintilla.CaretLineVisible = true;
			this.scintilla.Dock = System.Windows.Forms.DockStyle.Fill;
			this.scintilla.Font = new System.Drawing.Font("맑은 고딕", 10F);
			this.scintilla.Location = new System.Drawing.Point(3, 39);
			this.scintilla.Name = "scintilla";
			this.scintilla.ScrollWidth = 2680;
			this.scintilla.Size = new System.Drawing.Size(961, 732);
			this.scintilla.TabIndents = true;
			this.scintilla.TabIndex = 0;
			this.scintilla.UseRightToLeftReadingLayout = false;
			// 
			// tlpMain
			// 
			this.tlpMain.ColumnCount = 1;
			this.tlpMain.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
			this.tlpMain.Controls.Add(this.scintilla, 0, 1);
			this.tlpMain.Controls.Add(this.tlpControls, 0, 0);
			this.tlpMain.Dock = System.Windows.Forms.DockStyle.Fill;
			this.tlpMain.Location = new System.Drawing.Point(0, 0);
			this.tlpMain.Name = "tlpMain";
			this.tlpMain.RowCount = 2;
			this.tlpMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 36F));
			this.tlpMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
			this.tlpMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
			this.tlpMain.Size = new System.Drawing.Size(967, 774);
			this.tlpMain.TabIndex = 1;
			// 
			// tlpControls
			// 
			this.tlpControls.ColumnCount = 2;
			this.tlpControls.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
			this.tlpControls.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
			this.tlpControls.Controls.Add(this.cbLexar, 0, 0);
			this.tlpControls.Dock = System.Windows.Forms.DockStyle.Fill;
			this.tlpControls.Location = new System.Drawing.Point(1, 1);
			this.tlpControls.Margin = new System.Windows.Forms.Padding(1);
			this.tlpControls.Name = "tlpControls";
			this.tlpControls.RowCount = 1;
			this.tlpControls.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
			this.tlpControls.Size = new System.Drawing.Size(965, 34);
			this.tlpControls.TabIndex = 1;
			// 
			// cbLexar
			// 
			this.cbLexar.FormattingEnabled = true;
			this.cbLexar.Location = new System.Drawing.Point(3, 3);
			this.cbLexar.Name = "cbLexar";
			this.cbLexar.Size = new System.Drawing.Size(160, 25);
			this.cbLexar.TabIndex = 3;
			this.cbLexar.SelectedIndexChanged += new System.EventHandler(this.cbLexar_SelectedIndexChanged);
			// 
			// MainForm
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
			this.ClientSize = new System.Drawing.Size(967, 774);
			this.Controls.Add(this.tlpMain);
			this.Font = new System.Drawing.Font("맑은 고딕", 10F);
			this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
			this.Name = "MainForm";
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Text = "ScintillaNet Text Editor";
			this.TopMost = true;
			this.tlpMain.ResumeLayout(false);
			this.tlpControls.ResumeLayout(false);
			this.ResumeLayout(false);

		}

		#endregion

		private ScintillaNET.Scintilla scintilla;
		private System.Windows.Forms.TableLayoutPanel tlpMain;
		private System.Windows.Forms.TableLayoutPanel tlpControls;
		private System.Windows.Forms.ComboBox cbLexar;
	}
}

