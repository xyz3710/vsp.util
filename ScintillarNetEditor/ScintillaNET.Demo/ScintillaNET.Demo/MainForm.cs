﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;
using ScintillaNET;
using ScintillaNET.Demo.Utils;

namespace ScintillaNET.Demo
{
	public partial class MainForm : Form
	{
		public MainForm()
		{
			InitializeComponent();
		}

		ScintillaNET.Scintilla scintilla;

		private void MainForm_Load(object sender, EventArgs e)
		{

			// CREATE CONTROL
			scintilla = new ScintillaNET.Scintilla();
			TextPanel.Controls.Add(scintilla);

			// BASIC CONFIG
			scintilla.Dock = System.Windows.Forms.DockStyle.Fill;
			scintilla.TextChanged += (this.OnTextChanged);

			// INITIAL VIEW CONFIG
			scintilla.Font = new Font("D2Coding", 11f);  // not working
			scintilla.WrapMode = WrapMode.None;
			scintilla.IndentationGuides = IndentView.LookBoth;

			// STYLING
			InitColors();
			InitSyntaxColoring();

			// NUMBER MARGIN
			InitNumberMargin();

			// BOOKMARK MARGIN
			InitBookmarkMargin();

			scintilla.ZoomChanged += (s, ea) => SetLineNumber(scintilla);

			// CODE FOLDING MARGIN
			InitCodeFolding();

			// DRAG DROP
			InitDragDropFile();

			// DEFAULT FILE
			LoadDataFromFile("../../MainForm.cs");

			// INIT HOTKEYS
			InitHotkeys();
		}

		private const int LINE_NUMBER_MARGIN = 0;

		private void SetLineNumber(Scintilla scintilla)
		{
			// Did the number of characters in the line number display change?
			// i.e. nnn VS nn, or nnnn VS nn, etc...
			var maxLineNumberCharLength = scintilla.Lines.Count.ToString().Length;
			//var padding = (int)Math.Max(1.1d * (double)scintilla.Zoom, 1.2d);
			var padding = (int)Math.Max(1.1d * (double)scintilla.Zoom, 1d);

			//if (maxLineNumberCharLength == Convert.ToInt32(scintilla.Tag))
			//{
			//    return;
			//}

			// Calculate the width required to display the last line number
			// and include some padding for good measure.
			scintilla.Margins[LINE_NUMBER_MARGIN].Width = scintilla.TextWidth(Style.LineNumber, new string('9', maxLineNumberCharLength + 1)) + padding;
		}

		private void InitColors()
		{
			Color color = Color.FromArgb(255, 0x11, 0x4D, 0x9C);
			scintilla.SetSelectionBackColor(true, color);

		}

		private void InitHotkeys()
		{

			// register the hotkeys with the form
			HotKeyManager.AddHotKey(this, OpenSearch, Keys.F, true);
			HotKeyManager.AddHotKey(this, OpenFindDialog, Keys.F, true, false, true);
			HotKeyManager.AddHotKey(this, OpenReplaceDialog, Keys.R, true);
			HotKeyManager.AddHotKey(this, OpenReplaceDialog, Keys.H, true);
			HotKeyManager.AddHotKey(this, Uppercase, Keys.U, true);
			HotKeyManager.AddHotKey(this, Lowercase, Keys.L, true);
			HotKeyManager.AddHotKey(this, ZoomIn, Keys.Oemplus, true);
			HotKeyManager.AddHotKey(this, ZoomOut, Keys.OemMinus, true);
			HotKeyManager.AddHotKey(this, ZoomDefault, Keys.D0, true);
			HotKeyManager.AddHotKey(this, CloseSearch, Keys.Escape);
			HotKeyManager.AddHotKey(this, Close, Keys.Q, ctrl: true);
			HotKeyManager.AddHotKey(this, InitCodeFolding, Keys.D1, shift: true);

			// remove conflicting hotkeys from scintilla
			scintilla.ClearCmdKey(Keys.Control | Keys.F);
			scintilla.ClearCmdKey(Keys.Control | Keys.R);
			scintilla.ClearCmdKey(Keys.Control | Keys.H);
			scintilla.ClearCmdKey(Keys.Control | Keys.L);
			scintilla.ClearCmdKey(Keys.Control | Keys.U);

		}

		private void InitSyntaxColoring()
		{

			// Configure the default style
			scintilla.StyleResetDefault();
			scintilla.Styles[Style.Default].Font = "D2Coding";
			scintilla.Styles[Style.Default].Size = 11;
			scintilla.Styles[Style.Default].BackColor = Color.FromArgb(255, 0x21, 0x21, 0x21);
			scintilla.Styles[Style.Default].ForeColor = Color.FromArgb(255, 0xFF, 0xFF, 0xFF);
			scintilla.StyleClearAll();

			// Configure the CPP (C#) lexer styles
			scintilla.Styles[Style.Cpp.Identifier].ForeColor = Color.FromArgb(255, 0xD0, 0xDA, 0xE2);
			scintilla.Styles[Style.Cpp.Comment].ForeColor = Color.FromArgb(255, 0xBD, 0x75, 0x8B);
			scintilla.Styles[Style.Cpp.CommentLine].ForeColor = Color.FromArgb(255, 0x40, 0xBF, 0x57);
			scintilla.Styles[Style.Cpp.CommentDoc].ForeColor = Color.FromArgb(255, 0x2F, 0xAE, 0x35);
			scintilla.Styles[Style.Cpp.Number].ForeColor = Color.FromArgb(255, 0xFF, 0xFF, 0x00);
			scintilla.Styles[Style.Cpp.String].ForeColor = Color.FromArgb(255, 0xFF, 0xFF, 0x00);
			scintilla.Styles[Style.Cpp.Character].ForeColor = Color.FromArgb(255, 0xE9, 0x54, 0x54);
			scintilla.Styles[Style.Cpp.Preprocessor].ForeColor = Color.FromArgb(255, 0x8A, 0xAF, 0xEE);
			scintilla.Styles[Style.Cpp.Operator].ForeColor = Color.FromArgb(255, 0xE0, 0xE0, 0xE0);
			scintilla.Styles[Style.Cpp.Regex].ForeColor = Color.FromArgb(255, 0xFF, 0x00, 0xFF);
			scintilla.Styles[Style.Cpp.CommentLineDoc].ForeColor = Color.FromArgb(255, 0x77, 0xA7, 0xDB);
			scintilla.Styles[Style.Cpp.Word].ForeColor = Color.FromArgb(255, 0x48, 0xA8, 0xEE);
			scintilla.Styles[Style.Cpp.Word2].ForeColor = Color.FromArgb(255, 0xF9, 0x89, 0x06);
			scintilla.Styles[Style.Cpp.CommentDocKeyword].ForeColor = Color.FromArgb(255, 0xB3, 0xD9, 0x91);
			scintilla.Styles[Style.Cpp.CommentDocKeywordError].ForeColor = Color.FromArgb(255, 0xFF, 0x00, 0x00);
			scintilla.Styles[Style.Cpp.GlobalClass].ForeColor = Color.FromArgb(255, 0x48, 0xA8, 0xEE);

			scintilla.Lexer = Lexer.Cpp;

			scintilla.SetKeywords(0, "class extends implements import interface new case do while else if for in switch throw get set function var try catch finally while with default break continue delete return each const namespace package include use is as instanceof typeof author copy default deprecated eventType example exampleText exception haxe inheritDoc internal link mtasc mxmlc param private return see serial serialData serialField since throws usage version langversion playerversion productversion dynamic private public partial static intrinsic internal native override protected AS3 final super this arguments null Infinity NaN undefined true false abstract as base bool break by byte case catch char checked class const continue decimal default delegate do double descending explicit event extern else enum false finally fixed float for foreach from goto group if implicit in int interface internal into is lock long new null namespace object operator out override orderby params private protected public readonly ref return switch struct sbyte sealed short sizeof stackalloc static string select this throw true try typeof uint ulong unchecked unsafe ushort using var virtual volatile void while where yield");
			scintilla.SetKeywords(1, "void Null ArgumentError arguments Array Boolean Class Date DefinitionError Error EvalError Function int Math Namespace Number Object RangeError ReferenceError RegExp SecurityError String SyntaxError TypeError uint XML XMLList Boolean Byte Char DateTime Decimal Double Int16 Int32 Int64 IntPtr SByte Single UInt16 UInt32 UInt64 UIntPtr Void Path File System Windows Forms ScintillaNET");

		}

		private void OnTextChanged(object sender, EventArgs e)
		{

		}


		#region Numbers, Bookmarks, Code Folding

		/// <summary>
		/// the background color of the text area
		/// </summary>
		//private const int BACK_COLOR = 0x2A211C;
		private readonly Color BACK_COLOR = Color.FromArgb(255, 0x2A, 0x21, 0x1C);

		/// <summary>
		/// default text color of the text area
		/// </summary>
		//private const int FORE_COLOR = 0xB7B7B7;
		private readonly Color FORE_COLOR = Color.FromArgb(255, 0xB7, 0xB7, 0xB7);

		/// <summary>
		/// change this to whatever margin you want the line numbers to show in
		/// </summary>
		private const int NUMBER_MARGIN = 1;

		/// <summary>
		/// change this to whatever margin you want the bookmarks/breakpoints to show in
		/// </summary>
		private const int BOOKMARK_MARGIN = 2;
		private const int BOOKMARK_MARKER = 2;

		/// <summary>
		/// change this to whatever margin you want the code folding tree (+/-) to show in
		/// </summary>
		private const int FOLDING_MARGIN = 3;

		/// <summary>
		/// set this true to show circular buttons for code folding (the [+] and [-] buttons on the margin)
		/// </summary>
		private const bool CODEFOLDING_CIRCULAR = false;

		private void InitNumberMargin()
		{

			scintilla.Styles[Style.LineNumber].BackColor = BACK_COLOR;
			scintilla.Styles[Style.LineNumber].ForeColor = FORE_COLOR;
			scintilla.Styles[Style.IndentGuide].ForeColor = FORE_COLOR;
			scintilla.Styles[Style.IndentGuide].BackColor = BACK_COLOR;

			var nums = scintilla.Margins[NUMBER_MARGIN];
			nums.Width = 30;
			nums.Type = MarginType.Number;
			nums.Sensitive = true;
			nums.Mask = 0;

			scintilla.MarginClick += TextArea_MarginClick;
		}

		private void InitBookmarkMargin()
		{

			//TextArea.SetFoldMarginColor(true, IntToColor(BACK_COLOR));

			var margin = scintilla.Margins[BOOKMARK_MARGIN];
			margin.Width = 20;
			margin.Sensitive = true;
			margin.Type = MarginType.Symbol;
			margin.Mask = (1 << BOOKMARK_MARKER);
			//margin.Cursor = MarginCursor.Arrow;

			var marker = scintilla.Markers[BOOKMARK_MARKER];
			marker.Symbol = MarkerSymbol.Circle;
			marker.SetBackColor(IntToColor(0xFF003B));
			marker.SetForeColor(IntToColor(0x000000));
			marker.SetAlpha(100);

		}

		private void InitCodeFolding()
		{
			scintilla.SetFoldMarginColor(true, BACK_COLOR);
			scintilla.SetFoldMarginHighlightColor(true, BACK_COLOR);

			// Enable code folding
			scintilla.SetProperty("fold", "1");
			scintilla.SetProperty("fold.compact", "1");

			// Configure a margin to display folding symbols
			scintilla.Margins[FOLDING_MARGIN].Type = MarginType.Symbol;
			scintilla.Margins[FOLDING_MARGIN].Mask = Marker.MaskFolders;
			scintilla.Margins[FOLDING_MARGIN].Sensitive = true;
			scintilla.Margins[FOLDING_MARGIN].Width = 20;

			// Set colors for all folding markers
			for (int i = 25; i <= 31; i++)
			{
				scintilla.Markers[i].SetForeColor(BACK_COLOR); // styles for [+] and [-]
				scintilla.Markers[i].SetBackColor(FORE_COLOR); // styles for [+] and [-]
			}

			// Configure folding markers with respective symbols
			scintilla.Markers[Marker.Folder].Symbol = CODEFOLDING_CIRCULAR ? MarkerSymbol.CirclePlus : MarkerSymbol.BoxPlus;
			scintilla.Markers[Marker.FolderOpen].Symbol = CODEFOLDING_CIRCULAR ? MarkerSymbol.CircleMinus : MarkerSymbol.BoxMinus;
			scintilla.Markers[Marker.FolderEnd].Symbol = CODEFOLDING_CIRCULAR ? MarkerSymbol.CirclePlusConnected : MarkerSymbol.BoxPlusConnected;
			scintilla.Markers[Marker.FolderMidTail].Symbol = MarkerSymbol.TCorner;
			scintilla.Markers[Marker.FolderOpenMid].Symbol = CODEFOLDING_CIRCULAR ? MarkerSymbol.CircleMinusConnected : MarkerSymbol.BoxMinusConnected;
			scintilla.Markers[Marker.FolderSub].Symbol = MarkerSymbol.VLine;
			scintilla.Markers[Marker.FolderTail].Symbol = MarkerSymbol.LCorner;

			// Enable automatic folding
			scintilla.AutomaticFold = (AutomaticFold.Show | AutomaticFold.Click | AutomaticFold.Change);

		}

		private void TextArea_MarginClick(object sender, MarginClickEventArgs e)
		{
			if (e.Margin == BOOKMARK_MARGIN)
			{
				// Do we have a marker for this line?
				const uint mask = (1 << BOOKMARK_MARKER);
				var line = scintilla.Lines[scintilla.LineFromPosition(e.Position)];
				if ((line.MarkerGet() & mask) > 0)
				{
					// Remove existing bookmark
					line.MarkerDelete(BOOKMARK_MARKER);
				}
				else
				{
					// Add bookmark
					line.MarkerAdd(BOOKMARK_MARKER);
				}
			}
		}

		#endregion

		#region Drag & Drop File

		public void InitDragDropFile()
		{

			scintilla.AllowDrop = true;
			scintilla.DragEnter += delegate (object sender, DragEventArgs e)
			{
				if (e.Data.GetDataPresent(DataFormats.FileDrop))
					e.Effect = DragDropEffects.Copy;
				else
					e.Effect = DragDropEffects.None;
			};
			scintilla.DragDrop += delegate (object sender, DragEventArgs e)
			{

				// get file drop
				if (e.Data.GetDataPresent(DataFormats.FileDrop))
				{

					Array a = (Array)e.Data.GetData(DataFormats.FileDrop);
					if (a != null)
					{

						string path = a.GetValue(0).ToString();

						LoadDataFromFile(path);

					}
				}
			};

		}

		private void LoadDataFromFile(string path)
		{
			if (File.Exists(path))
			{
				FileName.Text = Path.GetFileName(path);
				scintilla.Text = File.ReadAllText(path);
			}
		}

		#endregion

		#region Main Menu Commands

		private void openToolStripMenuItem_Click(object sender, EventArgs e)
		{
			if (openFileDialog.ShowDialog() == DialogResult.OK)
			{
				LoadDataFromFile(openFileDialog.FileName);
			}
		}

		private void findToolStripMenuItem_Click(object sender, EventArgs e)
		{
			OpenSearch();
		}

		private void findDialogToolStripMenuItem_Click(object sender, EventArgs e)
		{
			OpenFindDialog();
		}

		private void findAndReplaceToolStripMenuItem_Click(object sender, EventArgs e)
		{
			OpenReplaceDialog();
		}

		private void cutToolStripMenuItem_Click(object sender, EventArgs e)
		{
			scintilla.Cut();
		}

		private void copyToolStripMenuItem_Click(object sender, EventArgs e)
		{
			scintilla.Copy();
		}

		private void pasteToolStripMenuItem_Click(object sender, EventArgs e)
		{
			scintilla.Paste();
		}

		private void selectAllToolStripMenuItem_Click(object sender, EventArgs e)
		{
			scintilla.SelectAll();
		}

		private void selectLineToolStripMenuItem_Click(object sender, EventArgs e)
		{
			Line line = scintilla.Lines[scintilla.CurrentLine];
			scintilla.SetSelection(line.Position + line.Length, line.Position);
		}

		private void clearSelectionToolStripMenuItem_Click(object sender, EventArgs e)
		{
			scintilla.SetEmptySelection(0);
		}

		private void indentSelectionToolStripMenuItem_Click(object sender, EventArgs e)
		{
			Indent();
		}

		private void outdentSelectionToolStripMenuItem_Click(object sender, EventArgs e)
		{
			Outdent();
		}

		private void uppercaseSelectionToolStripMenuItem_Click(object sender, EventArgs e)
		{
			Uppercase();
		}

		private void lowercaseSelectionToolStripMenuItem_Click(object sender, EventArgs e)
		{
			Lowercase();
		}

		private void wordWrapToolStripMenuItem1_Click(object sender, EventArgs e)
		{

			// toggle word wrap
			wordWrapItem.Checked = !wordWrapItem.Checked;
			scintilla.WrapMode = wordWrapItem.Checked ? WrapMode.Word : WrapMode.None;
		}

		private void indentGuidesToolStripMenuItem_Click(object sender, EventArgs e)
		{

			// toggle indent guides
			indentGuidesItem.Checked = !indentGuidesItem.Checked;
			scintilla.IndentationGuides = indentGuidesItem.Checked ? IndentView.LookBoth : IndentView.None;
		}

		private void hiddenCharactersToolStripMenuItem_Click(object sender, EventArgs e)
		{

			// toggle view whitespace
			hiddenCharactersItem.Checked = !hiddenCharactersItem.Checked;
			scintilla.ViewWhitespace = hiddenCharactersItem.Checked ? WhitespaceMode.VisibleAlways : WhitespaceMode.Invisible;
		}

		private void zoomInToolStripMenuItem_Click(object sender, EventArgs e)
		{
			ZoomIn();
		}

		private void zoomOutToolStripMenuItem_Click(object sender, EventArgs e)
		{
			ZoomOut();
		}

		private void zoom100ToolStripMenuItem_Click(object sender, EventArgs e)
		{
			ZoomDefault();
		}

		private void collapseAllToolStripMenuItem_Click(object sender, EventArgs e)
		{
			scintilla.FoldAll(FoldAction.Contract);
		}

		private void expandAllToolStripMenuItem_Click(object sender, EventArgs e)
		{
			scintilla.FoldAll(FoldAction.Expand);
		}


		#endregion

		#region Uppercase / Lowercase

		private void Lowercase()
		{

			// save the selection
			int start = scintilla.SelectionStart;
			int end = scintilla.SelectionEnd;

			// modify the selected text
			scintilla.ReplaceSelection(scintilla.GetTextRange(start, end - start).ToLower());

			// preserve the original selection
			scintilla.SetSelection(start, end);
		}

		private void Uppercase()
		{

			// save the selection
			int start = scintilla.SelectionStart;
			int end = scintilla.SelectionEnd;

			// modify the selected text
			scintilla.ReplaceSelection(scintilla.GetTextRange(start, end - start).ToUpper());

			// preserve the original selection
			scintilla.SetSelection(start, end);
		}

		#endregion

		#region Indent / Outdent

		private void Indent()
		{
			// we use this hack to send "Shift+Tab" to scintilla, since there is no known API to indent,
			// although the indentation function exists. Pressing TAB with the editor focused confirms this.
			GenerateKeystrokes("{TAB}");
		}

		private void Outdent()
		{
			// we use this hack to send "Shift+Tab" to scintilla, since there is no known API to outdent,
			// although the indentation function exists. Pressing Shift+Tab with the editor focused confirms this.
			GenerateKeystrokes("+{TAB}");
		}

		private void GenerateKeystrokes(string keys)
		{
			HotKeyManager.Enable = false;
			scintilla.Focus();
			SendKeys.Send(keys);
			HotKeyManager.Enable = true;
		}

		#endregion

		#region Zoom

		private void ZoomIn()
		{
			scintilla.ZoomIn();
		}

		private void ZoomOut()
		{
			scintilla.ZoomOut();
		}

		private void ZoomDefault()
		{
			scintilla.Zoom = 0;
		}


		#endregion

		#region Quick Search Bar

		bool SearchIsOpen = false;

		private void OpenSearch()
		{

			SearchManager.SearchBox = TxtSearch;
			SearchManager.TextArea = scintilla;

			if (!SearchIsOpen)
			{
				SearchIsOpen = true;
				InvokeIfNeeded(delegate ()
				{
					PanelSearch.Visible = true;
					TxtSearch.Text = SearchManager.LastSearch;
					TxtSearch.Focus();
					TxtSearch.SelectAll();
				});
			}
			else
			{
				InvokeIfNeeded(delegate ()
				{
					TxtSearch.Focus();
					TxtSearch.SelectAll();
				});
			}
		}
		private void CloseSearch()
		{
			if (SearchIsOpen)
			{
				SearchIsOpen = false;
				InvokeIfNeeded(delegate ()
				{
					PanelSearch.Visible = false;
					//CurBrowser.GetBrowser().StopFinding(true);
				});
			}
		}

		private void BtnClearSearch_Click(object sender, EventArgs e)
		{
			CloseSearch();
		}

		private void BtnPrevSearch_Click(object sender, EventArgs e)
		{
			SearchManager.Find(false, false);
		}
		private void BtnNextSearch_Click(object sender, EventArgs e)
		{
			SearchManager.Find(true, false);
		}
		private void TxtSearch_TextChanged(object sender, EventArgs e)
		{
			SearchManager.Find(true, true);
		}

		private void TxtSearch_KeyDown(object sender, KeyEventArgs e)
		{
			if (HotKeyManager.IsHotkey(e, Keys.Enter))
			{
				SearchManager.Find(true, false);
			}
			if (HotKeyManager.IsHotkey(e, Keys.Enter, true) || HotKeyManager.IsHotkey(e, Keys.Enter, false, true))
			{
				SearchManager.Find(false, false);
			}
		}

		#endregion

		#region Find & Replace Dialog

		private void OpenFindDialog()
		{

		}
		private void OpenReplaceDialog()
		{


		}

		#endregion

		#region Utils

		public static Color IntToColor(int rgb)
		{
			return Color.FromArgb(255, (byte)(rgb >> 16), (byte)(rgb >> 8), (byte)rgb);
		}

		public void InvokeIfNeeded(Action action)
		{
			if (this.InvokeRequired)
			{
				this.BeginInvoke(action);
			}
			else
			{
				action.Invoke();
			}
		}

		#endregion




	}
}
