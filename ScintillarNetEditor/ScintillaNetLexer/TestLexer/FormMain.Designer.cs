﻿namespace TestLexer
{
    partial class FormMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormMain));
			this.scintilla = new ScintillaNET.Scintilla();
			this.msMain = new System.Windows.Forms.MenuStrip();
			this.mnuFile = new System.Windows.Forms.ToolStripMenuItem();
			this.mnuOpen = new System.Windows.Forms.ToolStripMenuItem();
			this.mnuTestMarkLoad = new System.Windows.Forms.ToolStripMenuItem();
			this.tsLexer = new System.Windows.Forms.ToolStripComboBox();
			this.odFile = new System.Windows.Forms.OpenFileDialog();
			this.msMain.SuspendLayout();
			this.SuspendLayout();
			// 
			// scintilla
			// 
			this.scintilla.AutoCMaxHeight = 9;
			this.scintilla.BiDirectionality = ScintillaNET.BiDirectionalDisplayType.Disabled;
			this.scintilla.CaretLineBackColor = System.Drawing.Color.Black;
			this.scintilla.CaretLineVisible = true;
			this.scintilla.Dock = System.Windows.Forms.DockStyle.Fill;
			this.scintilla.EdgeColor = System.Drawing.Color.Black;
			this.scintilla.LexerName = null;
			this.scintilla.Location = new System.Drawing.Point(0, 27);
			this.scintilla.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
			this.scintilla.Name = "scintilla";
			this.scintilla.ScrollWidth = 210;
			this.scintilla.Size = new System.Drawing.Size(933, 492);
			this.scintilla.TabIndents = true;
			this.scintilla.TabIndex = 0;
			this.scintilla.Text = resources.GetString("scintilla.Text");
			this.scintilla.UseRightToLeftReadingLayout = false;
			this.scintilla.WrapMode = ScintillaNET.WrapMode.None;
			this.scintilla.LocationChanged += new System.EventHandler(this.Scintilla_LocationChanged);
			this.scintilla.Click += new System.EventHandler(this.Scintilla_LocationChanged);
			// 
			// msMain
			// 
			this.msMain.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mnuFile,
            this.tsLexer});
			this.msMain.Location = new System.Drawing.Point(0, 0);
			this.msMain.Name = "msMain";
			this.msMain.Padding = new System.Windows.Forms.Padding(7, 2, 0, 2);
			this.msMain.Size = new System.Drawing.Size(933, 27);
			this.msMain.TabIndex = 1;
			this.msMain.Text = "menuStrip1";
			// 
			// mnuFile
			// 
			this.mnuFile.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mnuOpen,
            this.mnuTestMarkLoad});
			this.mnuFile.Name = "mnuFile";
			this.mnuFile.Size = new System.Drawing.Size(37, 23);
			this.mnuFile.Text = "File";
			// 
			// mnuOpen
			// 
			this.mnuOpen.Name = "mnuOpen";
			this.mnuOpen.Size = new System.Drawing.Size(153, 22);
			this.mnuOpen.Text = "Open";
			this.mnuOpen.Click += new System.EventHandler(this.mnuOpen_Click);
			// 
			// mnuTestMarkLoad
			// 
			this.mnuTestMarkLoad.Name = "mnuTestMarkLoad";
			this.mnuTestMarkLoad.Size = new System.Drawing.Size(153, 22);
			this.mnuTestMarkLoad.Text = "Test mark load";
			this.mnuTestMarkLoad.Click += new System.EventHandler(this.MnuTestMarkLoad_Click);
			// 
			// tsLexer
			// 
			this.tsLexer.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
			this.tsLexer.Name = "tsLexer";
			this.tsLexer.Size = new System.Drawing.Size(75, 23);
			this.tsLexer.Text = "Lexer";
			// 
			// odFile
			// 
			this.odFile.Filter = "All files|*.*";
			// 
			// FormMain
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(933, 519);
			this.Controls.Add(this.scintilla);
			this.Controls.Add(this.msMain);
			this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
			this.MainMenuStrip = this.msMain;
			this.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
			this.Name = "FormMain";
			this.Text = "A test program for the ScintillaNET lexers © VPKSoft 2020";
			this.msMain.ResumeLayout(false);
			this.msMain.PerformLayout();
			this.ResumeLayout(false);
			this.PerformLayout();

        }

        #endregion

        private ScintillaNET.Scintilla scintilla;
        private System.Windows.Forms.MenuStrip msMain;
        private System.Windows.Forms.ToolStripMenuItem mnuFile;
        private System.Windows.Forms.ToolStripMenuItem mnuOpen;
        private System.Windows.Forms.OpenFileDialog odFile;
        private System.Windows.Forms.ToolStripMenuItem mnuTestMarkLoad;
		private System.Windows.Forms.ToolStripComboBox tsLexer;
	}
}

