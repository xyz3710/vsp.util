﻿/**********************************************************************************************************************/
/*	Domain		:	Utility.GetOrangeTrialKeyForm
/*	Creator		:	KIMKIWON\xyz37(김기원)
/*	Create		:	2009년 6월 18일 목요일 오전 9:03
/*	Purpose		:	오렌지 트라이얼 키를 받아와서 자동으로 등록 한다.
/*--------------------------------------------------------------------------------------------------------------------*/
/*	Modifier	:	
/*	Update		:	
/*	Changes		:	
/*--------------------------------------------------------------------------------------------------------------------*/
/*	Comment		:	
/*--------------------------------------------------------------------------------------------------------------------*/
/*	Reviewer	:	Kim Ki Won
/*	Rev. Date	:	2009년 6월 18일 목요일 오전 9:04
/**********************************************************************************************************************/
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Text.RegularExpressions;
using System.Configuration;
using System.Runtime.InteropServices;

namespace Utility
{
	public partial class GetOrangeTrialKeyForm : Form
	{
		#region Constants
		private const string URL_FORMAT_STRING = @"http://www.warevalley.com/renew/kr/member/_do.asp?Mode=Login&userID={0}&userPass={1}&rPath=http://www.warevalley.com/renew/kr/product/{2}";
		private const string TRIAL_PAGE_NAME = "OrangeTrialKey.asp";
		private const string BUTTON_CLASS_NAME = "Button";
		private const Int32 WM_SYSCOMMAND = 0x112;
		private const Int32 SC_SCREENSAVE = 0xF140;
		private const Int32 BM_CLICK = 0x00F5;
		#endregion

		#region Fields
		private string _findRegex;
		private string _defaultGroupName;
		#endregion

		#region Invoke method
		[DllImport("user32.dll", EntryPoint="FindWindow", SetLastError=true)]
		private static extern IntPtr FindWindow(string lpClassName, string lpWindowName);

		[DllImport("user32.dll", EntryPoint="FindWindowByCaption", SetLastError=true)]
		private static extern IntPtr FindWindowByCaption(IntPtr ZeroOnly, string lpWindowName);

		[DllImport("user32.dll", CharSet=CharSet.Auto, SetLastError=false)]
		private static extern int SendMessage(IntPtr hWnd, UInt32 Msg, Int32 wParam, Int32 lParam);

		[DllImport("user32.dll", CharSet=CharSet.Auto, SetLastError=false)]
		private static extern IntPtr SetActiveWindow(IntPtr hWnd);

		[DllImport("user32.dll", SetLastError=true)]
		private static extern IntPtr FindWindowEx(IntPtr hwndParent, IntPtr hwndChildAfter, string lpszClass, string lpszWindow);

		[DllImport("user32.dll", SetLastError=true)]
		private static extern IntPtr FindWindowEx(IntPtr parentHandle, IntPtr childAfter, string className, IntPtr windowTitle);

		[DllImport("user32.dll")]
		private static extern uint PostMessage(IntPtr hwnd, uint wMsg, uint wParam, uint lParam);
		#endregion

		public GetOrangeTrialKeyForm()
		{
			InitializeComponent();
		}

		private void webBrowser_DocumentCompleted(object sender, WebBrowserDocumentCompletedEventArgs e)
		{
			if (Path.GetFileName(e.Url.AbsolutePath) == TRIAL_PAGE_NAME)
			{
				HtmlDocument htmlDocument = webBrowser.Document;

				if (htmlDocument != null && htmlDocument.Body.InnerText != null)
				{
					Regex regex = new Regex(_findRegex, RegexOptions.Multiline | RegexOptions.Compiled);

					if (regex.IsMatch(htmlDocument.Body.InnerText) == true)
					{
						Match match = regex.Match(htmlDocument.Body.InnerText);
						string trialKey = match.Groups[_defaultGroupName].Value;

						Clipboard.SetData(DataFormats.Text, trialKey);
						Text = "Trial key를 clipboard에 복사했습니다.";
						
						IntPtr registerWindowHandle = FindWindow(null, "Register License Key");
						IntPtr pateButtonHandle = FindWindowEx(registerWindowHandle, IntPtr.Zero, BUTTON_CLASS_NAME, "Paste Key");
						int result = 0;

						if (pateButtonHandle != IntPtr.Zero)
						{
							result = SendMessage(pateButtonHandle, BM_CLICK, 0, 0);

							if (result == 0)
							{
								// Register Button의 Handle을 구하지 못해서
								//IntPtr registerButtonHandle = FindWindowEx(registerWindowHandle, IntPtr.Zero, BUTTON_CLASS_NAME, "Register");

								//result = SendMessage(registerButtonHandle, BM_CLICK, 0, 0);
								// Enter Key를 누른다.
								PostMessage(registerWindowHandle, 0x0100, 0xD, 0x1C001);
								PostMessage(registerWindowHandle, 0x0102, 0xD, 0xC01C001);

								// Tab Key
								//PostMessage(registerWindowHandle, 0x0100, 0x9, 0xF0001);
								//PostMessage(registerWindowHandle, 0x0101, 0x9, 0xC00F0001);

							}
						}

						Close();
					}
				}
			}
		}

		private void GetOrangeTrialKeyForm_Load(object sender, EventArgs e)
		{
			string userId = ConfigurationManager.AppSettings["userId"];
			string password = ConfigurationManager.AppSettings["password"];
			string url = string.Format(URL_FORMAT_STRING, userId, password, TRIAL_PAGE_NAME);

			_findRegex = ConfigurationManager.AppSettings["findRegex"];
			_defaultGroupName = ConfigurationManager.AppSettings["defaultGroupName"];

			webBrowser.Url = new Uri(url, UriKind.Absolute);
			webBrowser.DocumentCompleted +=new WebBrowserDocumentCompletedEventHandler(webBrowser_DocumentCompleted);

			Text = "Warevally Site에 접속 중입니다....";
			// Screen Saver 실행 시키는 메세지 발생(SS가 등록 되어 있어야 한다.
			//Message message = Message.Create(handle, WM_SYSCOMMAND, (IntPtr)SC_SCREENSAVE, IntPtr.Zero);
		}
	}
}
