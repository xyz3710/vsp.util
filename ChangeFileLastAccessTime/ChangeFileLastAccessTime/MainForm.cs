﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;

namespace ChangeFileLastAccessTime
{
	public partial class frmMain : Form
	{
		public frmMain()
		{
			InitializeComponent();
		}

		private string FromPath
		{
			get
			{
				return txtFromPath.Text;
			}
		}

		private string ToPath
		{
			get
			{
				return txtToPath.Text;
			}
		}

		private string FromExt
		{
			get
			{
				return txtFromExt.Text;
			}
		}

		private string ToExt
		{
			get
			{
				return txtToExt.Text;
			}
		}

		private void txtFromPath_Click(object sender, EventArgs e)
		{
			fbdMain.SelectedPath = FromPath;

			if (fbdMain.ShowDialog() == System.Windows.Forms.DialogResult.OK)
				Read(chkFrom, fbdMain.SelectedPath, FromExt);

			for (int i = 0; i < chkFrom.Items.Count; i++)
				chkFrom.SetSelected(i, true);
		}

		private void txtToPath_Click(object sender, EventArgs e)
		{
			fbdMain.SelectedPath = ToPath;

			if (fbdMain.ShowDialog() == System.Windows.Forms.DialogResult.OK)
				Read(chkTo, fbdMain.SelectedPath, ToExt);
		}

		private void btnLoad_Click(object sender, EventArgs e)
		{
			Read(chkFrom, FromPath, FromExt);
			Read(chkTo, ToPath, ToExt);
		}

		private string[] GetFileList(string path, string ext)
		{
			return Directory.GetFiles(path, ext);
		}

		private void Read(CheckedListBox targetListBox, string fromPath, string fromExt)
		{
			targetListBox.Items.AddRange(GetFileList(fromPath, fromExt));
		}

		private void btnStart_Click(object sender, EventArgs e)
		{
			var fromFiles = chkFrom.Items.Cast<string>()
				.Select(item => new
				{
					FileName = Path.GetFileNameWithoutExtension(item).ToLower(),
					FullPath = item
				});
			var toFiles = chkTo.Items.Cast<string>()
				.Select(item => new
				{
					FileName = Path.GetFileNameWithoutExtension(item).ToLower(),
					FullPath = item
				});

			foreach (var item in fromFiles)
			{
				var single = toFiles.Single(to => to.FileName == item.FileName);

				if (single == null)
					continue;

				FileInfo fromInfo = new FileInfo(item.FullPath);

				File.SetCreationTime(single.FullPath, fromInfo.CreationTime);
				File.SetLastWriteTime(single.FullPath, fromInfo.LastWriteTime);
				File.SetLastAccessTime(single.FullPath, fromInfo.LastAccessTime);
			}

			MessageBox.Show(
				"완료되었습니다.",
				Text,
				MessageBoxButtons.OK,
				MessageBoxIcon.Warning);
		}
	}
}
