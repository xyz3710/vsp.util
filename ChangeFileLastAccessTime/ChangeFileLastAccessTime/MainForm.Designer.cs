﻿namespace ChangeFileLastAccessTime
{
	partial class frmMain
	{
		/// <summary>
		/// 필수 디자이너 변수입니다.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// 사용 중인 모든 리소스를 정리합니다.
		/// </summary>
		/// <param name="disposing">관리되는 리소스를 삭제해야 하면 true이고, 그렇지 않으면 false입니다.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form 디자이너에서 생성한 코드

		/// <summary>
		/// 디자이너 지원에 필요한 메서드입니다.
		/// 이 메서드의 내용을 코드 편집기로 수정하지 마십시오.
		/// </summary>
		private void InitializeComponent()
		{
			this.tlpMain = new System.Windows.Forms.TableLayoutPanel();
			this.txtFromPath = new System.Windows.Forms.TextBox();
			this.txtToPath = new System.Windows.Forms.TextBox();
			this.label1 = new System.Windows.Forms.Label();
			this.label2 = new System.Windows.Forms.Label();
			this.chkFrom = new System.Windows.Forms.CheckedListBox();
			this.chkTo = new System.Windows.Forms.CheckedListBox();
			this.txtFromExt = new System.Windows.Forms.TextBox();
			this.txtToExt = new System.Windows.Forms.TextBox();
			this.btnStart = new System.Windows.Forms.Button();
			this.btnLoad = new System.Windows.Forms.Button();
			this.fbdMain = new System.Windows.Forms.FolderBrowserDialog();
			this.tlpMain.SuspendLayout();
			this.SuspendLayout();
			// 
			// tlpMain
			// 
			this.tlpMain.ColumnCount = 6;
			this.tlpMain.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 100F));
			this.tlpMain.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
			this.tlpMain.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 50F));
			this.tlpMain.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 100F));
			this.tlpMain.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
			this.tlpMain.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 50F));
			this.tlpMain.Controls.Add(this.txtFromPath, 1, 0);
			this.tlpMain.Controls.Add(this.txtToPath, 4, 0);
			this.tlpMain.Controls.Add(this.label1, 0, 0);
			this.tlpMain.Controls.Add(this.label2, 3, 0);
			this.tlpMain.Controls.Add(this.chkFrom, 0, 1);
			this.tlpMain.Controls.Add(this.chkTo, 3, 1);
			this.tlpMain.Controls.Add(this.txtFromExt, 2, 0);
			this.tlpMain.Controls.Add(this.txtToExt, 5, 0);
			this.tlpMain.Controls.Add(this.btnStart, 1, 2);
			this.tlpMain.Controls.Add(this.btnLoad, 0, 2);
			this.tlpMain.Dock = System.Windows.Forms.DockStyle.Fill;
			this.tlpMain.Location = new System.Drawing.Point(0, 0);
			this.tlpMain.Name = "tlpMain";
			this.tlpMain.RowCount = 3;
			this.tlpMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 28F));
			this.tlpMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
			this.tlpMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 28F));
			this.tlpMain.Size = new System.Drawing.Size(776, 470);
			this.tlpMain.TabIndex = 0;
			// 
			// txtFromPath
			// 
			this.txtFromPath.DataBindings.Add(new System.Windows.Forms.Binding("Text", global::ChangeFileLastAccessTime.Properties.Settings.Default, "FromPath", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
			this.txtFromPath.Dock = System.Windows.Forms.DockStyle.Fill;
			this.txtFromPath.Location = new System.Drawing.Point(103, 3);
			this.txtFromPath.Name = "txtFromPath";
			this.txtFromPath.Size = new System.Drawing.Size(232, 21);
			this.txtFromPath.TabIndex = 1;
			this.txtFromPath.Text = global::ChangeFileLastAccessTime.Properties.Settings.Default.FromPath;
			this.txtFromPath.Click += new System.EventHandler(this.txtFromPath_Click);
			// 
			// txtToPath
			// 
			this.txtToPath.DataBindings.Add(new System.Windows.Forms.Binding("Text", global::ChangeFileLastAccessTime.Properties.Settings.Default, "ToPath", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
			this.txtToPath.Dock = System.Windows.Forms.DockStyle.Fill;
			this.txtToPath.Location = new System.Drawing.Point(491, 3);
			this.txtToPath.Name = "txtToPath";
			this.txtToPath.Size = new System.Drawing.Size(232, 21);
			this.txtToPath.TabIndex = 1;
			this.txtToPath.Text = global::ChangeFileLastAccessTime.Properties.Settings.Default.ToPath;
			this.txtToPath.Click += new System.EventHandler(this.txtToPath_Click);
			// 
			// label1
			// 
			this.label1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.label1.Location = new System.Drawing.Point(3, 0);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(94, 28);
			this.label1.TabIndex = 2;
			this.label1.Text = "&From Path";
			this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// label2
			// 
			this.label2.Dock = System.Windows.Forms.DockStyle.Fill;
			this.label2.Location = new System.Drawing.Point(391, 0);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(94, 28);
			this.label2.TabIndex = 2;
			this.label2.Text = "&To Path";
			this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// chkFrom
			// 
			this.tlpMain.SetColumnSpan(this.chkFrom, 3);
			this.chkFrom.Dock = System.Windows.Forms.DockStyle.Fill;
			this.chkFrom.FormattingEnabled = true;
			this.chkFrom.Location = new System.Drawing.Point(3, 31);
			this.chkFrom.Name = "chkFrom";
			this.chkFrom.Size = new System.Drawing.Size(382, 408);
			this.chkFrom.TabIndex = 3;
			// 
			// chkTo
			// 
			this.tlpMain.SetColumnSpan(this.chkTo, 3);
			this.chkTo.Dock = System.Windows.Forms.DockStyle.Fill;
			this.chkTo.FormattingEnabled = true;
			this.chkTo.Location = new System.Drawing.Point(391, 31);
			this.chkTo.Name = "chkTo";
			this.chkTo.Size = new System.Drawing.Size(382, 408);
			this.chkTo.TabIndex = 3;
			// 
			// txtFromExt
			// 
			this.txtFromExt.Location = new System.Drawing.Point(341, 3);
			this.txtFromExt.Name = "txtFromExt";
			this.txtFromExt.Size = new System.Drawing.Size(44, 21);
			this.txtFromExt.TabIndex = 4;
			this.txtFromExt.Text = "*.mov";
			// 
			// txtToExt
			// 
			this.txtToExt.Location = new System.Drawing.Point(729, 3);
			this.txtToExt.Name = "txtToExt";
			this.txtToExt.Size = new System.Drawing.Size(44, 21);
			this.txtToExt.TabIndex = 4;
			this.txtToExt.Text = "*.mp4";
			// 
			// btnStart
			// 
			this.tlpMain.SetColumnSpan(this.btnStart, 5);
			this.btnStart.Dock = System.Windows.Forms.DockStyle.Fill;
			this.btnStart.Location = new System.Drawing.Point(103, 445);
			this.btnStart.Name = "btnStart";
			this.btnStart.Size = new System.Drawing.Size(670, 22);
			this.btnStart.TabIndex = 0;
			this.btnStart.Text = "&Start";
			this.btnStart.UseVisualStyleBackColor = true;
			this.btnStart.Click += new System.EventHandler(this.btnStart_Click);
			// 
			// btnLoad
			// 
			this.btnLoad.Dock = System.Windows.Forms.DockStyle.Fill;
			this.btnLoad.Location = new System.Drawing.Point(3, 445);
			this.btnLoad.Name = "btnLoad";
			this.btnLoad.Size = new System.Drawing.Size(94, 22);
			this.btnLoad.TabIndex = 0;
			this.btnLoad.Text = "&Read";
			this.btnLoad.UseVisualStyleBackColor = true;
			this.btnLoad.Click += new System.EventHandler(this.btnLoad_Click);
			// 
			// frmMain
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(776, 470);
			this.Controls.Add(this.tlpMain);
			this.Name = "frmMain";
			this.Text = "다중 파일 변경 일자 변경";
			this.tlpMain.ResumeLayout(false);
			this.tlpMain.PerformLayout();
			this.ResumeLayout(false);

		}

		#endregion

		private System.Windows.Forms.TableLayoutPanel tlpMain;
		private System.Windows.Forms.Button btnStart;
		private System.Windows.Forms.TextBox txtFromPath;
		private System.Windows.Forms.TextBox txtToPath;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.CheckedListBox chkFrom;
		private System.Windows.Forms.CheckedListBox chkTo;
		private System.Windows.Forms.FolderBrowserDialog fbdMain;
		private System.Windows.Forms.TextBox txtFromExt;
		private System.Windows.Forms.TextBox txtToExt;
		private System.Windows.Forms.Button btnLoad;
	}
}

