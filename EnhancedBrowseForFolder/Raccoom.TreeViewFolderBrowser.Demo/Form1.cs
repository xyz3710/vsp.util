// Copyright ?2004 by Christoph Richner. All rights are reserved.
// 
// If you like this code then feel free to go ahead and use it.
// The only thing I ask is that you don't remove or alter my copyright notice.
//
// Your use of this software is entirely at your own risk. I make no claims or
// warrantees about the reliability or fitness of this code for any particular purpose.
// If you make changes or additions to this code please mark your code as being yours.
// 
// website http://raccoom.sytes.net, email microweb@bluewin.ch, msn chrisdarebell@msn.com
 

using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using System.Data;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using Raccoom.Windows.Forms;

namespace Raccoom.Windows.Forms.Demo
{
	/// <summary>
	/// Summary description for Form1.
	/// </summary>
	public class Form1 : System.Windows.Forms.Form
	{
		#region fields
		private TreeViewFolderBrowser treeViewRecursiveChecked;
		private System.Windows.Forms.MainMenu mainMenu1;
		private System.Windows.Forms.MenuItem menuItem1;
		private System.Windows.Forms.MenuItem menuItem2;
		private System.Windows.Forms.MenuItem menuItem3;
		private System.Windows.Forms.MenuItem menuItem4;		
		private System.Windows.Forms.StatusBar statusBar1;
		private System.Windows.Forms.ImageList imageList1;
		private System.Windows.Forms.ToolBar toolBar1;
		private System.Windows.Forms.ToolBarButton toolBarButton1;
		private System.Windows.Forms.ToolBarButton toolBarButton2;
		private System.Windows.Forms.MenuItem menuItem5;
		private System.Windows.Forms.MenuItem menuItem6;
		private NJFLib.Controls.CollapsibleSplitter splitter3;
		private System.Windows.Forms.ListBox listBox1;
		private NJFLib.Controls.CollapsibleSplitter splitter1;
		private System.Windows.Forms.PropertyGrid propertyGrid1;
		private System.Windows.Forms.MenuItem menuItem7;
		private System.Windows.Forms.MenuItem menuItem8;
		private System.Windows.Forms.ToolBarButton toolBarButton3;
		private System.Windows.Forms.ToolBarButton toolBarButton4;
		private System.Windows.Forms.MenuItem menuItem9;
		private System.Windows.Forms.ToolBarButton toolBarButton5;
		private System.Windows.Forms.ToolBarButton toolBarButton6;
		private System.Windows.Forms.ToolBarButton toolBarButton7;
		private System.Windows.Forms.ComboBox _cmbDataProvider;
		private System.Windows.Forms.Label _labelDataProvider;
		private System.ComponentModel.IContainer components;
		#endregion
		
		public Form1()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
		}


		private void ShowSelectedDirs()
		{
			this.listBox1.Items.Clear();
			foreach(string s in treeViewRecursiveChecked.SelectedDirectories)
			{
				this.listBox1.Items.Add(s);
			}	
		}

		private void FillDataProviderCombo()
		{
			this._cmbDataProvider.Items.Clear();
			//
			this._cmbDataProvider.Items.Add(new Raccoom.Windows.Forms.TreeViewFolderBrowserDataProvider());
			Raccoom.Windows.Forms.TreeViewFolderBrowserDataProviderShell32 shell32Provider =new Raccoom.Windows.Forms.TreeViewFolderBrowserDataProviderShell32();
			shell32Provider.EnableContextMenu = true;
			shell32Provider.ShowAllShellObjects = true;
			this._cmbDataProvider.Items.Add(shell32Provider);
			_cmbDataProvider.SelectedIndex = 0;
		}

		#region events
		private void _cmbDataProvider_SelectionChangeCommitted(object sender, System.EventArgs e)
		{
            this.treeViewRecursiveChecked.DataSource = this._cmbDataProvider.SelectedItem as ITreeViewFolderBrowserDataProvider;		
			this.propertyGrid1.Refresh();
			//
			menuItem6_Click(null, null);
		}
		private void treeViewRecursiveChecked_SelectedDirectoriesChanged(object sender, Raccoom.Windows.Forms.SelectedDirectoriesChangedEventArgs e)
		{
			this.statusBar1.Text = e.Path + " is now " + e.CheckState.ToString();
			//				 
			ShowSelectedDirs();	
		}		
		private void treeViewRecursiveChecked_AfterSelect(object sender, System.Windows.Forms.TreeViewEventArgs e)
		{
			TreeNodePath node = e.Node as TreeNodePath;
			if(node!=null) this.statusBar1.Text = node.Path;		
		}
		private void menuItem6_Click(object sender, System.EventArgs e)
		{
			this.statusBar1.Text = "Please wait...";
			this.treeViewRecursiveChecked.Populate();
			this.statusBar1.Text = "";
		}
		private void menuItem2_Click(object sender, System.EventArgs e)
		{
			this.Close();
		}
		private void menuItem4_Click(object sender, System.EventArgs e)
		{
			new Raccoom.Windows.Forms.FormAbout().ShowDialog(this);
		}
		protected override void OnLoad(EventArgs e)
		{
			if(!DesignMode)
			{
				FileStream stream = new System.IO.FileStream("list", FileMode.OpenOrCreate);
				try
				{
					BinaryFormatter binary = new BinaryFormatter();			
					this.treeViewRecursiveChecked.SelectedDirectories = binary.Deserialize(stream) as System.Collections.Specialized.StringCollection;
				} 
				catch{}
				stream.Close();	
				//
				FillDataProviderCombo();
				//
				this.treeViewRecursiveChecked.DataSource = _cmbDataProvider.Items[0] as ITreeViewFolderBrowserDataProvider;
				this.treeViewRecursiveChecked.RootFolder = System.Environment.SpecialFolder.Desktop;
				//
				this.treeViewRecursiveChecked.Populate(System.Environment.SpecialFolder.ApplicationData);
				this.propertyGrid1.SelectedObject = this.treeViewRecursiveChecked;	
				//
				this.Text = Application.CompanyName + " - " + Application.ProductName + " " + Application.ProductVersion.Substring(0,3);				
				//
				ShowSelectedDirs();				
			}
			//
			base.OnLoad (e);
		}
		protected override void OnClosed(EventArgs e)
		{
			FileStream stream = new System.IO.FileStream("list", FileMode.Create);
			BinaryFormatter binary = new BinaryFormatter();			
			binary.Serialize(stream, this.treeViewRecursiveChecked.SelectedDirectories);
			stream.Close();
			//
			base.OnClosed(e);
		}
		private void toolBar1_ButtonClick(object sender, System.Windows.Forms.ToolBarButtonClickEventArgs e)
		{
			if(e.Button.Equals(this.toolBarButton1))
			{
				this.Close();
			}
			else if(e.Button.Equals(this.toolBarButton2))
			{
				menuItem6_Click(this, EventArgs.Empty);
			}
			else if(e.Button.Equals(this.toolBarButton3))
			{
				menuItem7_Click(this, EventArgs.Empty);
			}
			else if(e.Button.Equals(this.toolBarButton5))
			{
				menuItem9_Click(this, EventArgs.Empty);
			}
			else if(e.Button.Equals(this.toolBarButton6))
			{
				menuItem4_Click(this, EventArgs.Empty);
			}
		}
		private void menuItem7_Click(object sender, System.EventArgs e)
		{
			System.Windows.Forms.FolderBrowserDialog myDialog = new FolderBrowserDialog();
			myDialog.RootFolder = treeViewRecursiveChecked.RootFolder;
			if(myDialog.ShowDialog()== DialogResult.Cancel) return;
			//
			;
			if(myDialog.SelectedPath!=null)
			{
				this.treeViewRecursiveChecked.ShowFolder(myDialog.SelectedPath);
			}
		}
		private void menuItem9_Click(object sender, System.EventArgs e)
		{
			foreach(string path in this.treeViewRecursiveChecked.SelectedDirectories)
			{
				this.treeViewRecursiveChecked.ShowFolder(path);
			}
		}
		#endregion

		#region Windows Form Designer generated code
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if (components != null) 
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
			this.treeViewRecursiveChecked = new Raccoom.Windows.Forms.TreeViewFolderBrowser();
			this.mainMenu1 = new System.Windows.Forms.MainMenu(this.components);
			this.menuItem1 = new System.Windows.Forms.MenuItem();
			this.menuItem2 = new System.Windows.Forms.MenuItem();
			this.menuItem5 = new System.Windows.Forms.MenuItem();
			this.menuItem9 = new System.Windows.Forms.MenuItem();
			this.menuItem7 = new System.Windows.Forms.MenuItem();
			this.menuItem8 = new System.Windows.Forms.MenuItem();
			this.menuItem6 = new System.Windows.Forms.MenuItem();
			this.menuItem3 = new System.Windows.Forms.MenuItem();
			this.menuItem4 = new System.Windows.Forms.MenuItem();
			this.statusBar1 = new System.Windows.Forms.StatusBar();
			this.imageList1 = new System.Windows.Forms.ImageList(this.components);
			this.toolBar1 = new System.Windows.Forms.ToolBar();
			this.toolBarButton1 = new System.Windows.Forms.ToolBarButton();
			this.toolBarButton4 = new System.Windows.Forms.ToolBarButton();
			this.toolBarButton3 = new System.Windows.Forms.ToolBarButton();
			this.toolBarButton5 = new System.Windows.Forms.ToolBarButton();
			this.toolBarButton2 = new System.Windows.Forms.ToolBarButton();
			this.toolBarButton7 = new System.Windows.Forms.ToolBarButton();
			this.toolBarButton6 = new System.Windows.Forms.ToolBarButton();
			this.listBox1 = new System.Windows.Forms.ListBox();
			this.propertyGrid1 = new System.Windows.Forms.PropertyGrid();
			this._cmbDataProvider = new System.Windows.Forms.ComboBox();
			this._labelDataProvider = new System.Windows.Forms.Label();
			this.splitter1 = new NJFLib.Controls.CollapsibleSplitter();
			this.splitter3 = new NJFLib.Controls.CollapsibleSplitter();
			this.SuspendLayout();
			// 
			// treeViewRecursiveChecked
			// 
			this.treeViewRecursiveChecked.DataSource = null;
			this.treeViewRecursiveChecked.Dock = System.Windows.Forms.DockStyle.Fill;
			this.treeViewRecursiveChecked.HideSelection = false;
			this.treeViewRecursiveChecked.Location = new System.Drawing.Point(0, 28);
			this.treeViewRecursiveChecked.Name = "treeViewRecursiveChecked";
			this.treeViewRecursiveChecked.SelectedDirectories = ((System.Collections.Specialized.StringCollection)(resources.GetObject("treeViewRecursiveChecked.SelectedDirectories")));
			this.treeViewRecursiveChecked.Size = new System.Drawing.Size(232, 268);
			this.treeViewRecursiveChecked.TabIndex = 0;
			this.treeViewRecursiveChecked.SelectedDirectoriesChanged += new Raccoom.Windows.Forms.SelectedDirectoriesChangedDelegate(this.treeViewRecursiveChecked_SelectedDirectoriesChanged);
			this.treeViewRecursiveChecked.AfterSelect += new System.Windows.Forms.TreeViewEventHandler(this.treeViewRecursiveChecked_AfterSelect);
			// 
			// mainMenu1
			// 
			this.mainMenu1.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
            this.menuItem1,
            this.menuItem5,
            this.menuItem3});
			// 
			// menuItem1
			// 
			this.menuItem1.Index = 0;
			this.menuItem1.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
            this.menuItem2});
			this.menuItem1.Text = "&File";
			// 
			// menuItem2
			// 
			this.menuItem2.Index = 0;
			this.menuItem2.Text = "&Exit";
			this.menuItem2.Click += new System.EventHandler(this.menuItem2_Click);
			// 
			// menuItem5
			// 
			this.menuItem5.Index = 1;
			this.menuItem5.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
            this.menuItem9,
            this.menuItem7,
            this.menuItem8,
            this.menuItem6});
			this.menuItem5.Text = "&View";
			// 
			// menuItem9
			// 
			this.menuItem9.Index = 0;
			this.menuItem9.Text = "Show all selected folders";
			this.menuItem9.Click += new System.EventHandler(this.menuItem9_Click);
			// 
			// menuItem7
			// 
			this.menuItem7.Index = 1;
			this.menuItem7.Text = "Set selected folder...";
			this.menuItem7.Click += new System.EventHandler(this.menuItem7_Click);
			// 
			// menuItem8
			// 
			this.menuItem8.Index = 2;
			this.menuItem8.Text = "-";
			// 
			// menuItem6
			// 
			this.menuItem6.Index = 3;
			this.menuItem6.Shortcut = System.Windows.Forms.Shortcut.F5;
			this.menuItem6.Text = "&Refresh";
			this.menuItem6.Click += new System.EventHandler(this.menuItem6_Click);
			// 
			// menuItem3
			// 
			this.menuItem3.Index = 2;
			this.menuItem3.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
            this.menuItem4});
			this.menuItem3.Text = "?";
			// 
			// menuItem4
			// 
			this.menuItem4.Index = 0;
			this.menuItem4.Text = "&About";
			this.menuItem4.Click += new System.EventHandler(this.menuItem4_Click);
			// 
			// statusBar1
			// 
			this.statusBar1.Location = new System.Drawing.Point(0, 380);
			this.statusBar1.Name = "statusBar1";
			this.statusBar1.Size = new System.Drawing.Size(528, 21);
			this.statusBar1.TabIndex = 7;
			// 
			// imageList1
			// 
			this.imageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList1.ImageStream")));
			this.imageList1.TransparentColor = System.Drawing.Color.Transparent;
			this.imageList1.Images.SetKeyName(0, "");
			this.imageList1.Images.SetKeyName(1, "");
			this.imageList1.Images.SetKeyName(2, "");
			this.imageList1.Images.SetKeyName(3, "");
			this.imageList1.Images.SetKeyName(4, "");
			// 
			// toolBar1
			// 
			this.toolBar1.Appearance = System.Windows.Forms.ToolBarAppearance.Flat;
			this.toolBar1.Buttons.AddRange(new System.Windows.Forms.ToolBarButton[] {
            this.toolBarButton1,
            this.toolBarButton4,
            this.toolBarButton3,
            this.toolBarButton5,
            this.toolBarButton2,
            this.toolBarButton7,
            this.toolBarButton6});
			this.toolBar1.DropDownArrows = true;
			this.toolBar1.ImageList = this.imageList1;
			this.toolBar1.Location = new System.Drawing.Point(0, 0);
			this.toolBar1.Name = "toolBar1";
			this.toolBar1.ShowToolTips = true;
			this.toolBar1.Size = new System.Drawing.Size(528, 28);
			this.toolBar1.TabIndex = 8;
			this.toolBar1.ButtonClick += new System.Windows.Forms.ToolBarButtonClickEventHandler(this.toolBar1_ButtonClick);
			// 
			// toolBarButton1
			// 
			this.toolBarButton1.ImageIndex = 0;
			this.toolBarButton1.Name = "toolBarButton1";
			this.toolBarButton1.ToolTipText = "Exit";
			// 
			// toolBarButton4
			// 
			this.toolBarButton4.Name = "toolBarButton4";
			this.toolBarButton4.Style = System.Windows.Forms.ToolBarButtonStyle.Separator;
			// 
			// toolBarButton3
			// 
			this.toolBarButton3.ImageIndex = 2;
			this.toolBarButton3.Name = "toolBarButton3";
			this.toolBarButton3.ToolTipText = "set selected folder...";
			// 
			// toolBarButton5
			// 
			this.toolBarButton5.ImageIndex = 3;
			this.toolBarButton5.Name = "toolBarButton5";
			this.toolBarButton5.ToolTipText = "Show all selected directories";
			// 
			// toolBarButton2
			// 
			this.toolBarButton2.ImageIndex = 1;
			this.toolBarButton2.Name = "toolBarButton2";
			this.toolBarButton2.ToolTipText = "Refresh";
			// 
			// toolBarButton7
			// 
			this.toolBarButton7.Name = "toolBarButton7";
			this.toolBarButton7.Style = System.Windows.Forms.ToolBarButtonStyle.Separator;
			// 
			// toolBarButton6
			// 
			this.toolBarButton6.ImageIndex = 4;
			this.toolBarButton6.Name = "toolBarButton6";
			this.toolBarButton6.ToolTipText = "About";
			// 
			// listBox1
			// 
			this.listBox1.Dock = System.Windows.Forms.DockStyle.Bottom;
			this.listBox1.ItemHeight = 12;
			this.listBox1.Location = new System.Drawing.Point(0, 304);
			this.listBox1.Name = "listBox1";
			this.listBox1.Size = new System.Drawing.Size(528, 76);
			this.listBox1.TabIndex = 13;
			// 
			// propertyGrid1
			// 
			this.propertyGrid1.Dock = System.Windows.Forms.DockStyle.Right;
			this.propertyGrid1.LineColor = System.Drawing.SystemColors.ScrollBar;
			this.propertyGrid1.Location = new System.Drawing.Point(240, 28);
			this.propertyGrid1.Name = "propertyGrid1";
			this.propertyGrid1.Size = new System.Drawing.Size(288, 268);
			this.propertyGrid1.TabIndex = 15;
			// 
			// _cmbDataProvider
			// 
			this._cmbDataProvider.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this._cmbDataProvider.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this._cmbDataProvider.Location = new System.Drawing.Point(297, 6);
			this._cmbDataProvider.Name = "_cmbDataProvider";
			this._cmbDataProvider.Size = new System.Drawing.Size(221, 20);
			this._cmbDataProvider.TabIndex = 16;
			this._cmbDataProvider.SelectionChangeCommitted += new System.EventHandler(this._cmbDataProvider_SelectionChangeCommitted);
			// 
			// _labelDataProvider
			// 
			this._labelDataProvider.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this._labelDataProvider.Location = new System.Drawing.Point(201, 9);
			this._labelDataProvider.Name = "_labelDataProvider";
			this._labelDataProvider.Size = new System.Drawing.Size(96, 17);
			this._labelDataProvider.TabIndex = 17;
			this._labelDataProvider.Text = "DataProvider:";
			// 
			// splitter1
			// 
			this.splitter1.AnimationDelay = 20;
			this.splitter1.AnimationStep = 20;
			this.splitter1.BorderStyle3D = System.Windows.Forms.Border3DStyle.Flat;
			this.splitter1.ControlToHide = this.propertyGrid1;
			this.splitter1.Dock = System.Windows.Forms.DockStyle.Right;
			this.splitter1.ExpandParentForm = false;
			this.splitter1.Location = new System.Drawing.Point(232, 28);
			this.splitter1.Name = "splitter1";
			this.splitter1.TabIndex = 14;
			this.splitter1.TabStop = false;
			this.splitter1.UseAnimations = false;
			this.splitter1.VisualStyle = NJFLib.Controls.VisualStyles.Mozilla;
			// 
			// splitter3
			// 
			this.splitter3.AnimationDelay = 20;
			this.splitter3.AnimationStep = 20;
			this.splitter3.BorderStyle3D = System.Windows.Forms.Border3DStyle.Flat;
			this.splitter3.ControlToHide = this.listBox1;
			this.splitter3.Dock = System.Windows.Forms.DockStyle.Bottom;
			this.splitter3.ExpandParentForm = false;
			this.splitter3.Location = new System.Drawing.Point(0, 296);
			this.splitter3.Name = "splitter3";
			this.splitter3.TabIndex = 12;
			this.splitter3.TabStop = false;
			this.splitter3.UseAnimations = false;
			this.splitter3.VisualStyle = NJFLib.Controls.VisualStyles.Mozilla;
			// 
			// Form1
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(6, 14);
			this.ClientSize = new System.Drawing.Size(528, 401);
			this.Controls.Add(this._labelDataProvider);
			this.Controls.Add(this._cmbDataProvider);
			this.Controls.Add(this.treeViewRecursiveChecked);
			this.Controls.Add(this.splitter1);
			this.Controls.Add(this.propertyGrid1);
			this.Controls.Add(this.toolBar1);
			this.Controls.Add(this.splitter3);
			this.Controls.Add(this.listBox1);
			this.Controls.Add(this.statusBar1);
			this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
			this.Menu = this.mainMenu1;
			this.Name = "Form1";
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Text = "Form1";
			this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
			this.ResumeLayout(false);
			this.PerformLayout();

		}
		#endregion
		
		/// <summary>
		/// The main entry point for the application.
		/// </summary>
		[STAThread]
		static void Main() 
		{
			Application.EnableVisualStyles();
			Application.DoEvents();
			Application.Run(new Form1());
		}
	}
}
